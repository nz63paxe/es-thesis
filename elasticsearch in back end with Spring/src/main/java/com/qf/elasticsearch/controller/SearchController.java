package com.qf.elasticsearch.controller;

import com.alibaba.fastjson.JSONObject;
import com.qf.elasticsearch.pojo.News;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.FetchSourceFilter;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequestMapping("/search")//类
@RestController
@CrossOrigin(origins = "*")
public class SearchController {

    /**
     * search as your type
     */
    @Resource
    private ElasticsearchTemplate elasticsearchTemplate;
    @RequestMapping
    public Object suggest(String text) {
        // german Completion Suggestion
        CompletionSuggestionBuilder germanCompletionSuggestion = new CompletionSuggestionBuilder("tags")
                .prefix(text).skipDuplicates(true);
        SuggestBuilder suggestBuilder = new SuggestBuilder()
                .addSuggestion("ger_suggestion", germanCompletionSuggestion);
        SearchRequestBuilder searchRequestBuilder = elasticsearchTemplate
                .getClient() //
                .prepareSearch("news")
                .suggest(suggestBuilder)
                .setFetchSource(new String[]{"id"}, new String[]{});
        SearchResponse response = searchRequestBuilder.get();
        Set<String> suggestionWord = new HashSet<>();
        // Get german suggestion
        Suggest.Suggestion gerSuggestion = response.getSuggest().getSuggestion("ger_suggestion");
        generateSuggestion(gerSuggestion, suggestionWord);
        return getResult(suggestionWord);
    }




    /**
     * high light + search bar search
     * @param text
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/execute")//方法
    public Object executeSearch(@RequestParam(required = true) String text,
                                @RequestParam(required = false, defaultValue = "0") Integer page,           //Current page
                                @RequestParam(required = false, defaultValue = "10") Integer size){         // informations pre page
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(new MultiMatchQueryBuilder(text, "title", "content","postcode",
                                "location","organization","contacts","telephone","email"))                  // queried field *8
                .withSourceFilter(new FetchSourceFilter(new String[]{"id", "content", "title", "url","postcode",
                                "location","organization","contacts","telephone","email"}, new String[]{})) //received field *10
                .withIndices("news")
                .withHighlightBuilder(
                        new HighlightBuilder()
                                .field("title")
                                .field("content")
                                .field("postcode")
                                .field("location")
                                .field("organization")
                                .field("contacts")
                                .field("telephone")
                                .field("email")
                                .postTags("</font>")
                                .preTags("<font color='red'>")  //Highlight color
                                .fragmentSize(800000)           //If highlight fields with many words such as text content,
                                .numOfFragments(0))             //Must be configured, otherwise it will cause incomplete highlighting,
                                                                // missing article content, etc.
                .withPageable(PageRequest.of(page, size))
                .build();
        return elasticsearchTemplate.query(searchQuery, new CustomResultExtractor());
    }


    /**
     * high light + Catalog search
     * @param text
     * @param page
     * @param size
     * @return
     */
    @RequestMapping("/catalog")
    public Object catalogSearch(@RequestParam(required = true) String text,
                                @RequestParam(required = false, defaultValue = "0") Integer page,   //Current page
                                @RequestParam(required = false, defaultValue = "10") Integer size) {// informations pre page
        SearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withQuery(new MultiMatchQueryBuilder(text, "catalog"))// queried field*1
                .withSourceFilter(new FetchSourceFilter(new String[]{"id","content", "title", "url","postcode","location","organization","contacts","telephone","email"}, new String[]{}))
                .withIndices("news")
                .withHighlightBuilder(
                        new HighlightBuilder()
                                .field("title")
                                .field("content")
                                .postTags("</font>")
                                .preTags("<font color='red'>")
                                .fragmentSize(800000)           //If highlight fields with many words such as text content,
                                .numOfFragments(0))             //Must be configured, otherwise it will cause incomplete highlighting,
                                                                // missing article content, etc.
                .withPageable(PageRequest.of(page, size))
                .build();

        return elasticsearchTemplate.query(searchQuery, new CustomResultExtractor());
    }

    /**
     *  execute results of high light
     */
    public class CustomResultExtractor implements ResultsExtractor<List<News>> {
        @Override
        public List<News> extract(SearchResponse response) {
            return StreamSupport
                    .stream(response.getHits().spliterator(), true)
                    .map(this::searchHitToMyClass)
                    .collect(Collectors.toList());
        }
        private News searchHitToMyClass(SearchHit searchHit) {
            News news = JSONObject.parseObject(searchHit.getSourceAsString(), News.class);
            Map<String, HighlightField> highlightFieldMap = searchHit.getHighlightFields();
                 // content-field,The reason for judging is because some content may not have given keywords
            if(highlightFieldMap.containsKey("content")) {
                Text[] texts = searchHit.getHighlightFields().get("content").fragments();
                 //Get all the highlighted sentences in the content
                StringBuffer content = new StringBuffer();
                 /*Determine if it is empty, otherwise the first result of your match has
				no highlighted content, then a null pointer exception will be reported*/
                for(Text text : texts) {
                    if(null != text) {
                        content.append(text.toString());
                    }
                }
                news.setContent(content.toString());
            }
            //title-field
            if(highlightFieldMap.containsKey("title")) {
                Text[] texts = searchHit.getHighlightFields().get("title").fragments();

                StringBuffer title = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        title.append(text.toString());
                    }
                }
                news.setTitle(title.toString());
            }
            //postcode-field
            if(highlightFieldMap.containsKey("postcode")) {
                Text[] texts = searchHit.getHighlightFields().get("postcode").fragments();

                StringBuffer postcode = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        postcode.append(text.toString());
                    }
                }
                news.setPostcode(postcode.toString());
            }
            //location-field
            if(highlightFieldMap.containsKey("location")) {
                Text[] texts = searchHit.getHighlightFields().get("location").fragments();

                StringBuffer location = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        location.append(text.toString());
                    }
                }
                news.setLocation(location.toString());
            }
            //organization-field
            if(highlightFieldMap.containsKey("organization")) {
                Text[] texts = searchHit.getHighlightFields().get("organization").fragments();

                StringBuffer organization = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        organization.append(text.toString());
                    }
                }
                news.setOrganization(organization.toString());
            }
            //contacts-field
            if(highlightFieldMap.containsKey("contacts")) {
                Text[] texts = searchHit.getHighlightFields().get("contacts").fragments();

                StringBuffer contacts = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        contacts.append(text.toString());
                    }
                }
                news.setContacts(contacts.toString());
            }
            //telephone-field
            if(highlightFieldMap.containsKey("telephone")) {
                Text[] texts = searchHit.getHighlightFields().get("telephone").fragments();

                StringBuffer telephone = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        telephone.append(text.toString());
                    }
                }
                news.setTelephone(telephone.toString());
            }
            //email-field
            if(highlightFieldMap.containsKey("email")) {
                Text[] texts = searchHit.getHighlightFields().get("email").fragments();

                StringBuffer email = new StringBuffer();

                for(Text text : texts) {
                    if(null != text) {
                        email.append(text.toString());
                    }
                }
                news.setEmail(email.toString());
            }
            return news;
        }
    }

    /**
     * Searched suggestions
     * @param suggestion
     */
    public void generateSuggestion(Suggest.Suggestion suggestion, Set<String> suggestionWord) {
        /* The suggestion.getEntries() method gets the corresponding suggestions, because in the case of word segmentation,
         there may be multiple suggestions/
        */
        List<?> entries = suggestion.getEntries();
        if(entries.size() > 0) {
            for(Object obj : entries) {
                // In fact, the type of obj found is CompletionSuggestion.Entry
                if(obj instanceof CompletionSuggestion.Entry) {
                    CompletionSuggestion.Entry entry = (CompletionSuggestion.Entry)obj;
                    // Get specific option
                    List<CompletionSuggestion.Entry.Option> optionsList = entry.getOptions();
                    if(optionsList.size() > 0) {
                        for(CompletionSuggestion.Entry.Option op : optionsList) {
                            Text text = op.getText();
                            suggestionWord.add(text.toString());
                        }
                    }
                }
            }
        }
    }

    /**
     * @param set
     * @return
     * It is used to store all the suggestions. The reason for using the data type List<Map<String, String>>
     * is because the data format received by ElementUI in autocomplete is [{"value":"abc"}, {"value" :"xyz"}]
     */

    public List<Map<String, String>> getResult(Set<String> set) {
        List<Map<String, String>> resultList = new LinkedList<>();
        set.forEach(suggestion -> {
            Map<String, String> map = new HashMap<>();
            map.put("value", suggestion);
            resultList.add(map);
        });
        return resultList;
    }
}

