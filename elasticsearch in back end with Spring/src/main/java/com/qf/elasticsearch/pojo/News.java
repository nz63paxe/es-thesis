package com.qf.elasticsearch.pojo;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "news", type = "_doc")
public class News {
    //Create the entity class News, corresponding to the fields in the data table
    private Integer id;              //<!--id -->
    private String url;              //<!--url -->
    private String content;          //<!--content -->
    private String title;            //<!--title -->
    private String postcode;         //<!--postcode -->
    private String location;         //<!--location -->
    private String organization;     //<!--organization -->
    private String contacts;         // <!--contacts -->
    private String telephone;        // <!--telephone -->
    private String email;            //<!--email -->
    private String catalog;          //<!--catalog -->
        //<!--more for get set methods  -->


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) { this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }


}