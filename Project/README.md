## Data Preperation
- 1. Import the "news.sql" file into MySQL on  your Computer
- 2. There is a logstash-mysql.conf file in the bin folder in the Logstash 7.4.2 folder.Modify your MySQL username and password in this logstash-mysql.conf file.
## Kibana for Analyzer and Mapping 
- 1. Run kibana.bat file in bin folder in the kibana-7.4.2-windows-x86_64 folder, And add **Custom Analyzer** and** Mapping** in Dev Tools of 5601 port as follows.

**Custom Analyzer:** 
```
PUT news
{
  "settings": {
    "number_of_shards" :   2,
    "number_of_replicas" : 1,
    "analysis": {
      "filter": {
        "german_stop": {
          "type":       "stop",
          "stopwords":  "_german_"
        },
        "german_keywords": {
          "type":       "keyword_marker",
          "keywords":   ["Beispiel"]
        },
        "german_stemmer": {
          "type":       "stemmer",
          "language":   "light_german"
        }
      },
      "analyzer": {
        "rebuilt_german": {
          "tokenizer":  "standard",
          "filter": [
            "lowercase",
            "german_stop",
            "german_keywords",
            "german_normalization",
            "german_stemmer"
          ]
        }
      }
    }
  }
}
```
**Mapping:**

```
PUT news/_mapping
{
  "dynamic": false,
  "properties": {
    "id": {
      "type": "long"
    },
    "title": {
      "type": "text",
      "analyzer": "rebuilt_german"
    },
    "content": {
      "type": "text",
      "analyzer": "rebuilt_german"
    },
    "tags": {
      "type": "completion",
      "analyzer": "rebuilt_german"
    },
    "postcode":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },
    "organization":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },    
    "location":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },    
    "contacts":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },    
    "telephone":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },    
    "email":{
      "type": "text",
      "analyzer": "rebuilt_german"
    },
    "catalog":{
      "type": "text",
      "analyzer": "rebuilt_german"
    }
  }
}
```

- 2. In windows system, run CMD commands under the bin folder in Logstash 7.4.2 folder and execute "`logstash -f logstash-mysql.conf`" commands in CMD. Then Logstash collects and uploads the Data to Elasticsearch.

## Elasticsearch

-   **Master Node:** Run elasticsearch.bat file in bin folder in the elasticsearch-7.4.2-node-master folder. And configuration file  elasticsearch.yml stored in config folder
-   **Data Node:** Then run elasticsearch.bat file in bin folder in the elasticsearch-7.4.2-node-slave1 folder. And configuration file  elasticsearch.yml stored in config folder

## SpringBoot in IDEA
-  Run the SpringBoot project(in "elasticsearch in back end with Spring" folder) als Backend.

## Vue in IDEA
-  Run the Vue project(in "front end with Vue" folder) als Frontend on port 8080.





















