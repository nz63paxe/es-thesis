"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _operators = require("rxjs/operators");

var _utils = require("../../../../src/core/utils");

var _authentication = require("./authentication");

var _config = require("./config");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Represents Security Plugin instance that will be managed by the Kibana plugin system.
 */
class Plugin {
  constructor(initializerContext) {
    this.initializerContext = initializerContext;

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "clusterClient", void 0);

    _defineProperty(this, "legacyAPI", void 0);

    _defineProperty(this, "getLegacyAPI", () => {
      if (!this.legacyAPI) {
        throw new Error('Legacy API is not registered!');
      }

      return this.legacyAPI;
    });

    this.logger = this.initializerContext.logger.get();
  }

  async setup(core) {
    const config = await (0, _config.createConfig$)(this.initializerContext, core.http.isTlsEnabled).pipe((0, _operators.first)()).toPromise();
    this.clusterClient = core.elasticsearch.createClient('security', {
      plugins: [require('../../../legacy/server/lib/esjs_shield_plugin')]
    });
    return (0, _utils.deepFreeze)({
      registerLegacyAPI: legacyAPI => this.legacyAPI = legacyAPI,
      authc: await (0, _authentication.setupAuthentication)({
        core,
        config,
        clusterClient: this.clusterClient,
        loggers: this.initializerContext.logger,
        getLegacyAPI: this.getLegacyAPI
      }),
      // We should stop exposing this config as soon as only new platform plugin consumes it. The only
      // exception may be `sessionTimeout` as other parts of the app may want to know it.
      config: {
        sessionTimeout: config.sessionTimeout,
        secureCookies: config.secureCookies,
        cookieName: config.cookieName,
        authc: {
          providers: config.authc.providers
        }
      }
    });
  }

  start() {
    this.logger.debug('Starting plugin');
  }

  stop() {
    this.logger.debug('Stopping plugin');

    if (this.clusterClient) {
      this.clusterClient.close();
      this.clusterClient = undefined;
    }
  }

}

exports.Plugin = Plugin;