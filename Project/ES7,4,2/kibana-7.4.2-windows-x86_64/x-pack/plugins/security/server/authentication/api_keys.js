"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.APIKeys = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Represents the options to create an APIKey class instance that will be
 * shared between functions (create, invalidate, etc).
 */

/**
 * Represents the params for creating an API key
 */

/**
 * Represents the params for invalidating an API key
 */

/**
 * The return value when creating an API key in Elasticsearch. The API key returned by this API
 * can then be used by sending a request with a Authorization header with a value having the
 * prefix ApiKey `{token}` where token is id and api_key joined by a colon `{id}:{api_key}` and
 * then encoded to base64.
 */

/**
 * The return value when invalidating an API key in Elasticsearch.
 */

/**
 * Class responsible for managing Elasticsearch API keys.
 */
class APIKeys {
  constructor({
    logger,
    clusterClient,
    isSecurityFeatureDisabled
  }) {
    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "clusterClient", void 0);

    _defineProperty(this, "isSecurityFeatureDisabled", void 0);

    this.logger = logger;
    this.clusterClient = clusterClient;
    this.isSecurityFeatureDisabled = isSecurityFeatureDisabled;
  }
  /**
   * Tries to create an API key for the current user.
   * @param request Request instance.
   * @param params The params to create an API key
   */


  async create(request, params) {
    if (this.isSecurityFeatureDisabled()) {
      return null;
    }

    this.logger.debug('Trying to create an API key'); // User needs `manage_api_key` privilege to use this API

    let result;

    try {
      result = await this.clusterClient.asScoped(request).callAsCurrentUser('shield.createAPIKey', {
        body: params
      });
      this.logger.debug('API key was created successfully');
    } catch (e) {
      this.logger.error(`Failed to create API key: ${e.message}`);
      throw e;
    }

    return result;
  }
  /**
   * Tries to invalidate an API key.
   * @param request Request instance.
   * @param params The params to invalidate an API key.
   */


  async invalidate(request, params) {
    if (this.isSecurityFeatureDisabled()) {
      return null;
    }

    this.logger.debug('Trying to invalidate an API key'); // User needs `manage_api_key` privilege to use this API

    let result;

    try {
      result = await this.clusterClient.asScoped(request).callAsCurrentUser('shield.invalidateAPIKey', {
        body: {
          id: params.id
        }
      });
      this.logger.debug('API key was invalidated successfully');
    } catch (e) {
      this.logger.error(`Failed to invalidate API key: ${e.message}`);
      throw e;
    }

    return result;
  }

}

exports.APIKeys = APIKeys;