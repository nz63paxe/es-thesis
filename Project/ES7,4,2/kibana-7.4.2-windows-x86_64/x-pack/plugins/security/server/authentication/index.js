"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupAuthentication = setupAuthentication;
Object.defineProperty(exports, "Authenticator", {
  enumerable: true,
  get: function () {
    return _authenticator.Authenticator;
  }
});
Object.defineProperty(exports, "ProviderLoginAttempt", {
  enumerable: true,
  get: function () {
    return _authenticator.ProviderLoginAttempt;
  }
});
Object.defineProperty(exports, "CreateAPIKeyResult", {
  enumerable: true,
  get: function () {
    return _api_keys.CreateAPIKeyResult;
  }
});
Object.defineProperty(exports, "canRedirectRequest", {
  enumerable: true,
  get: function () {
    return _can_redirect_request.canRedirectRequest;
  }
});
Object.defineProperty(exports, "AuthenticationResult", {
  enumerable: true,
  get: function () {
    return _authentication_result.AuthenticationResult;
  }
});
Object.defineProperty(exports, "DeauthenticationResult", {
  enumerable: true,
  get: function () {
    return _deauthentication_result.DeauthenticationResult;
  }
});
Object.defineProperty(exports, "OIDCAuthenticationFlow", {
  enumerable: true,
  get: function () {
    return _providers.OIDCAuthenticationFlow;
  }
});

var _errors = require("../errors");

var _authenticator = require("./authenticator");

var _api_keys = require("./api_keys");

var _can_redirect_request = require("./can_redirect_request");

var _authentication_result = require("./authentication_result");

var _deauthentication_result = require("./deauthentication_result");

var _providers = require("./providers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function setupAuthentication({
  core,
  clusterClient,
  config,
  loggers,
  getLegacyAPI
}) {
  const authLogger = loggers.get('authentication');

  const isSecurityFeatureDisabled = () => {
    const xpackInfo = getLegacyAPI().xpackInfo;
    return xpackInfo.isAvailable() && !xpackInfo.feature('security').isEnabled();
  };
  /**
   * Retrieves server protocol name/host name/port and merges it with `xpack.security.public` config
   * to construct a server base URL (deprecated, used by the SAML provider only).
   */


  const getServerBaseURL = () => {
    const serverConfig = { ...getLegacyAPI().serverConfig,
      ...config.public
    };
    return `${serverConfig.protocol}://${serverConfig.hostname}:${serverConfig.port}`;
  };
  /**
   * Retrieves currently authenticated user associated with the specified request.
   * @param request
   */


  const getCurrentUser = async request => {
    if (isSecurityFeatureDisabled()) {
      return null;
    }

    return await clusterClient.asScoped(request).callAsCurrentUser('shield.authenticate');
  };

  const authenticator = new _authenticator.Authenticator({
    clusterClient,
    basePath: core.http.basePath,
    config: {
      sessionTimeout: config.sessionTimeout,
      authc: config.authc
    },
    isSystemAPIRequest: request => getLegacyAPI().isSystemAPIRequest(request),
    getServerBaseURL,
    loggers,
    sessionStorageFactory: await core.http.createCookieSessionStorageFactory({
      encryptionKey: config.encryptionKey,
      isSecure: config.secureCookies,
      name: config.cookieName,
      validate: sessionValue => !(sessionValue.expires && sessionValue.expires < Date.now())
    })
  });
  authLogger.debug('Successfully initialized authenticator.');
  core.http.registerAuth(async (request, response, t) => {
    // If security is disabled continue with no user credentials and delete the client cookie as well.
    if (isSecurityFeatureDisabled()) {
      return t.authenticated();
    }

    let authenticationResult;

    try {
      authenticationResult = await authenticator.authenticate(request);
    } catch (err) {
      authLogger.error(err);
      return response.internalError();
    }

    if (authenticationResult.succeeded()) {
      return t.authenticated({
        state: authenticationResult.user,
        requestHeaders: authenticationResult.authHeaders,
        responseHeaders: authenticationResult.authResponseHeaders
      });
    }

    if (authenticationResult.redirected()) {
      // Some authentication mechanisms may require user to be redirected to another location to
      // initiate or complete authentication flow. It can be Kibana own login page for basic
      // authentication (username and password) or arbitrary external page managed by 3rd party
      // Identity Provider for SSO authentication mechanisms. Authentication provider is the one who
      // decides what location user should be redirected to.
      return response.redirected({
        headers: {
          location: authenticationResult.redirectURL
        }
      });
    }

    if (authenticationResult.failed()) {
      authLogger.info(`Authentication attempt failed: ${authenticationResult.error.message}`);
      const error = authenticationResult.error; // proxy Elasticsearch "native" errors

      const statusCode = (0, _errors.getErrorStatusCode)(error);

      if (typeof statusCode === 'number') {
        return response.customError({
          body: error,
          statusCode,
          headers: authenticationResult.authResponseHeaders
        });
      }

      return response.unauthorized({
        headers: authenticationResult.authResponseHeaders
      });
    }

    authLogger.debug('Could not handle authentication attempt');
    return response.unauthorized({
      headers: authenticationResult.authResponseHeaders
    });
  });
  authLogger.debug('Successfully registered core authentication handler.');
  const apiKeys = new _api_keys.APIKeys({
    clusterClient,
    logger: loggers.get('api-key'),
    isSecurityFeatureDisabled
  });
  return {
    login: authenticator.login.bind(authenticator),
    logout: authenticator.logout.bind(authenticator),
    getCurrentUser,
    createAPIKey: (request, params) => apiKeys.create(request, params),
    invalidateAPIKey: (request, params) => apiKeys.invalidate(request, params),
    isAuthenticated: async request => {
      try {
        await getCurrentUser(request);
      } catch (err) {
        // Don't swallow server errors.
        if ((0, _errors.getErrorStatusCode)(err) !== 401) {
          throw err;
        }

        return false;
      }

      return true;
    }
  };
}