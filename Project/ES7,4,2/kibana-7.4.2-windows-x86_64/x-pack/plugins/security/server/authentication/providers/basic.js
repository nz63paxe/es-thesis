"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BasicAuthenticationProvider = void 0;

var _can_redirect_request = require("../can_redirect_request");

var _authentication_result = require("../authentication_result");

var _deauthentication_result = require("../deauthentication_result");

var _base = require("./base");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Provider that supports request authentication via Basic HTTP Authentication.
 */
class BasicAuthenticationProvider extends _base.BaseAuthenticationProvider {
  /**
   * Performs initial login request using username and password.
   * @param request Request instance.
   * @param attempt User credentials.
   * @param [state] Optional state object associated with the provider.
   */
  async login(request, {
    username,
    password
  }, state) {
    this.logger.debug('Trying to perform a login.');
    const authHeaders = {
      authorization: `Basic ${Buffer.from(`${username}:${password}`).toString('base64')}`
    };

    try {
      const user = await this.getUser(request, authHeaders);
      this.logger.debug('Login has been successfully performed.');
      return _authentication_result.AuthenticationResult.succeeded(user, {
        authHeaders,
        state: authHeaders
      });
    } catch (err) {
      this.logger.debug(`Failed to perform a login: ${err.message}`);
      return _authentication_result.AuthenticationResult.failed(err);
    }
  }
  /**
   * Performs request authentication using Basic HTTP Authentication.
   * @param request Request instance.
   * @param [state] Optional state object associated with the provider.
   */


  async authenticate(request, state) {
    this.logger.debug(`Trying to authenticate user request to ${request.url.path}.`); // try header-based auth

    const {
      authenticationResult: headerAuthResult,
      headerNotRecognized
    } = await this.authenticateViaHeader(request);

    if (headerNotRecognized) {
      return headerAuthResult;
    }

    let authenticationResult = headerAuthResult;

    if (authenticationResult.notHandled() && state) {
      authenticationResult = await this.authenticateViaState(request, state);
    } else if (authenticationResult.notHandled() && (0, _can_redirect_request.canRedirectRequest)(request)) {
      // If we couldn't handle authentication let's redirect user to the login page.
      const nextURL = encodeURIComponent(`${this.options.basePath.get(request)}${request.url.path}`);
      authenticationResult = _authentication_result.AuthenticationResult.redirectTo(`${this.options.basePath.get(request)}/login?next=${nextURL}`);
    }

    return authenticationResult;
  }
  /**
   * Redirects user to the login page preserving query string parameters.
   * @param request Request instance.
   */


  async logout(request) {
    // Query string may contain the path where logout has been called or
    // logout reason that login page may need to know.
    const queryString = request.url.search || `?msg=LOGGED_OUT`;
    return _deauthentication_result.DeauthenticationResult.redirectTo(`${this.options.basePath.get(request)}/login${queryString}`);
  }
  /**
   * Validates whether request contains `Basic ***` Authorization header and just passes it
   * forward to Elasticsearch backend.
   * @param request Request instance.
   */


  async authenticateViaHeader(request) {
    this.logger.debug('Trying to authenticate via header.');
    const authorization = request.headers.authorization;

    if (!authorization || typeof authorization !== 'string') {
      this.logger.debug('Authorization header is not presented.');
      return {
        authenticationResult: _authentication_result.AuthenticationResult.notHandled()
      };
    }

    const authenticationSchema = authorization.split(/\s+/)[0];

    if (authenticationSchema.toLowerCase() !== 'basic') {
      this.logger.debug(`Unsupported authentication schema: ${authenticationSchema}`);
      return {
        authenticationResult: _authentication_result.AuthenticationResult.notHandled(),
        headerNotRecognized: true
      };
    }

    try {
      const user = await this.getUser(request);
      this.logger.debug('Request has been authenticated via header.');
      return {
        authenticationResult: _authentication_result.AuthenticationResult.succeeded(user)
      };
    } catch (err) {
      this.logger.debug(`Failed to authenticate request via header: ${err.message}`);
      return {
        authenticationResult: _authentication_result.AuthenticationResult.failed(err)
      };
    }
  }
  /**
   * Tries to extract authorization header from the state and adds it to the request before
   * it's forwarded to Elasticsearch backend.
   * @param request Request instance.
   * @param state State value previously stored by the provider.
   */


  async authenticateViaState(request, {
    authorization
  }) {
    this.logger.debug('Trying to authenticate via state.');

    if (!authorization) {
      this.logger.debug('Access token is not found in state.');
      return _authentication_result.AuthenticationResult.notHandled();
    }

    try {
      const authHeaders = {
        authorization
      };
      const user = await this.getUser(request, authHeaders);
      this.logger.debug('Request has been authenticated via state.');
      return _authentication_result.AuthenticationResult.succeeded(user, {
        authHeaders
      });
    } catch (err) {
      this.logger.debug(`Failed to authenticate request via state: ${err.message}`);
      return _authentication_result.AuthenticationResult.failed(err);
    }
  }

}

exports.BasicAuthenticationProvider = BasicAuthenticationProvider;