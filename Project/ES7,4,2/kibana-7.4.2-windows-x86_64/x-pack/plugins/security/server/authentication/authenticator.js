"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Authenticator = void 0;

var _server = require("../../../../../src/core/server");

var _errors = require("../errors");

var _providers = require("./providers");

var _authentication_result = require("./authentication_result");

var _deauthentication_result = require("./deauthentication_result");

var _tokens = require("./tokens");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Mapping between provider key defined in the config and authentication
// provider class that can handle specific authentication mechanism.
const providerMap = new Map([['basic', _providers.BasicAuthenticationProvider], ['kerberos', _providers.KerberosAuthenticationProvider], ['saml', _providers.SAMLAuthenticationProvider], ['token', _providers.TokenAuthenticationProvider], ['oidc', _providers.OIDCAuthenticationProvider], ['pki', _providers.PKIAuthenticationProvider]]);

function assertRequest(request) {
  if (!(request instanceof _server.KibanaRequest)) {
    throw new Error(`Request should be a valid "KibanaRequest" instance, was [${typeof request}].`);
  }
}

function assertLoginAttempt(attempt) {
  if (!attempt || !attempt.provider || typeof attempt.provider !== 'string') {
    throw new Error('Login attempt should be an object with non-empty "provider" property.');
  }
}
/**
 * Instantiates authentication provider based on the provider key from config.
 * @param providerType Provider type key.
 * @param options Options to pass to provider's constructor.
 * @param providerSpecificOptions Options that are specific to {@param providerType}.
 */


function instantiateProvider(providerType, options, providerSpecificOptions) {
  const ProviderClassName = providerMap.get(providerType);

  if (!ProviderClassName) {
    throw new Error(`Unsupported authentication provider name: ${providerType}.`);
  }

  return new ProviderClassName(options, providerSpecificOptions);
}
/**
 * Authenticator is responsible for authentication of the request using chain of
 * authentication providers. The chain is essentially a prioritized list of configured
 * providers (typically of various types). The order of the list determines the order in
 * which the providers will be consulted. During the authentication process, Authenticator
 * will try to authenticate the request via one provider at a time. Once one of the
 * providers successfully authenticates the request, the authentication is considered
 * to be successful and the authenticated user will be associated with the request.
 * If provider cannot authenticate the request, the next in line provider in the chain
 * will be used. If all providers in the chain could not authenticate the request,
 * the authentication is then considered to be unsuccessful and an authentication error
 * will be returned.
 */


class Authenticator {
  /**
   * List of configured and instantiated authentication providers.
   */

  /**
   * Session duration in ms. If `null` session will stay active until the browser is closed.
   */

  /**
   * Internal authenticator logger.
   */

  /**
   * Instantiates Authenticator and bootstrap configured providers.
   * @param options Authenticator options.
   */
  constructor(options) {
    this.options = options;

    _defineProperty(this, "providers", void 0);

    _defineProperty(this, "ttl", null);

    _defineProperty(this, "logger", void 0);

    this.logger = options.loggers.get('authenticator');
    const providerCommonOptions = {
      client: this.options.clusterClient,
      basePath: this.options.basePath,
      tokens: new _tokens.Tokens({
        client: this.options.clusterClient,
        logger: this.options.loggers.get('tokens')
      }),
      getServerBaseURL: this.options.getServerBaseURL
    };
    const authProviders = this.options.config.authc.providers;

    if (authProviders.length === 0) {
      throw new Error('No authentication provider is configured. Verify `xpack.security.authc.providers` config value.');
    }

    this.providers = new Map(authProviders.map(providerType => {
      const providerSpecificOptions = this.options.config.authc.hasOwnProperty(providerType) ? this.options.config.authc[providerType] : undefined;
      return [providerType, instantiateProvider(providerType, Object.freeze({ ...providerCommonOptions,
        logger: options.loggers.get(providerType)
      }), providerSpecificOptions)];
    }));
    this.ttl = this.options.config.sessionTimeout;
  }
  /**
   * Performs the initial login request using the provider login attempt description.
   * @param request Request instance.
   * @param attempt Login attempt description.
   */


  async login(request, attempt) {
    assertRequest(request);
    assertLoginAttempt(attempt); // If there is an attempt to login with a provider that isn't enabled, we should fail.

    const provider = this.providers.get(attempt.provider);

    if (provider === undefined) {
      this.logger.debug(`Login attempt for provider "${attempt.provider}" is detected, but it isn't enabled.`);
      return _authentication_result.AuthenticationResult.notHandled();
    }

    this.logger.debug(`Performing login using "${attempt.provider}" provider.`);
    const sessionStorage = this.options.sessionStorageFactory.asScoped(request); // If we detect an existing session that belongs to a different provider than the one requested
    // to perform a login we should clear such session.

    let existingSession = attempt.stateless ? null : await this.getSessionValue(sessionStorage);

    if (existingSession && existingSession.provider !== attempt.provider) {
      this.logger.debug(`Clearing existing session of another ("${existingSession.provider}") provider.`);
      sessionStorage.clear();
      existingSession = null;
    }

    const authenticationResult = await provider.login(request, attempt.value, existingSession && existingSession.state); // There are two possible cases when we'd want to clear existing state:
    // 1. If provider owned the state (e.g. intermediate state used for multi step login), but failed
    // to login, that likely means that state is not valid anymore and we should clear it.
    // 2. Also provider can specifically ask to clear state by setting it to `null` even if
    // authentication attempt didn't fail (e.g. custom realm could "pin" client/request identity to
    // a server-side only session established during multi step login that relied on intermediate
    // client-side state which isn't needed anymore).

    const shouldClearSession = authenticationResult.shouldClearState() || authenticationResult.failed() && (0, _errors.getErrorStatusCode)(authenticationResult.error) === 401;

    if (existingSession && shouldClearSession) {
      sessionStorage.clear();
    } else if (!attempt.stateless && authenticationResult.shouldUpdateState()) {
      sessionStorage.set({
        state: authenticationResult.state,
        provider: attempt.provider,
        expires: this.ttl && Date.now() + this.ttl
      });
    }

    return authenticationResult;
  }
  /**
   * Performs request authentication using configured chain of authentication providers.
   * @param request Request instance.
   */


  async authenticate(request) {
    assertRequest(request);
    const sessionStorage = this.options.sessionStorageFactory.asScoped(request);
    const existingSession = await this.getSessionValue(sessionStorage);

    let authenticationResult = _authentication_result.AuthenticationResult.notHandled();

    for (const [providerType, provider] of this.providerIterator(existingSession)) {
      // Check if current session has been set by this provider.
      const ownsSession = existingSession && existingSession.provider === providerType;
      authenticationResult = await provider.authenticate(request, ownsSession ? existingSession.state : null);
      this.updateSessionValue(sessionStorage, {
        providerType,
        isSystemAPIRequest: this.options.isSystemAPIRequest(request),
        authenticationResult,
        existingSession: ownsSession ? existingSession : null
      });

      if (authenticationResult.failed() || authenticationResult.succeeded() || authenticationResult.redirected()) {
        return authenticationResult;
      }
    }

    return authenticationResult;
  }
  /**
   * Deauthenticates current request.
   * @param request Request instance.
   */


  async logout(request) {
    assertRequest(request);
    const sessionStorage = this.options.sessionStorageFactory.asScoped(request);
    const sessionValue = await this.getSessionValue(sessionStorage);

    if (sessionValue) {
      sessionStorage.clear();
      return this.providers.get(sessionValue.provider).logout(request, sessionValue.state);
    } // Normally when there is no active session in Kibana, `logout` method shouldn't do anything
    // and user will eventually be redirected to the home page to log in. But if SAML is supported there
    // is a special case when logout is initiated by the IdP or another SP, then IdP will request _every_
    // SP associated with the current user session to do the logout. So if Kibana (without active session)
    // receives such a request it shouldn't redirect user to the home page, but rather redirect back to IdP
    // with correct logout response and only Elasticsearch knows how to do that.


    if ((0, _providers.isSAMLRequestQuery)(request.query) && this.providers.has('saml')) {
      return this.providers.get('saml').logout(request);
    }

    return _deauthentication_result.DeauthenticationResult.notHandled();
  }
  /**
   * Returns provider iterator where providers are sorted in the order of priority (based on the session ownership).
   * @param sessionValue Current session value.
   */


  *providerIterator(sessionValue) {
    // If there is no session to predict which provider to use first, let's use the order
    // providers are configured in. Otherwise return provider that owns session first, and only then the rest
    // of providers.
    if (!sessionValue) {
      yield* this.providers;
    } else {
      yield [sessionValue.provider, this.providers.get(sessionValue.provider)];

      for (const [providerType, provider] of this.providers) {
        if (providerType !== sessionValue.provider) {
          yield [providerType, provider];
        }
      }
    }
  }
  /**
   * Extracts session value for the specified request. Under the hood it can
   * clear session if it belongs to the provider that is not available.
   * @param sessionStorage Session storage instance.
   */


  async getSessionValue(sessionStorage) {
    let sessionValue = await sessionStorage.get(); // If for some reason we have a session stored for the provider that is not available
    // (e.g. when user was logged in with one provider, but then configuration has changed
    // and that provider is no longer available), then we should clear session entirely.

    if (sessionValue && !this.providers.has(sessionValue.provider)) {
      sessionStorage.clear();
      sessionValue = null;
    }

    return sessionValue;
  }

  updateSessionValue(sessionStorage, {
    providerType,
    authenticationResult,
    existingSession,
    isSystemAPIRequest
  }) {
    if (!existingSession && !authenticationResult.shouldUpdateState()) {
      return;
    } // If authentication succeeds or requires redirect we should automatically extend existing user session,
    // unless authentication has been triggered by a system API request. In case provider explicitly returns new
    // state we should store it in the session regardless of whether it's a system API request or not.


    const sessionCanBeUpdated = (authenticationResult.succeeded() || authenticationResult.redirected()) && (authenticationResult.shouldUpdateState() || !isSystemAPIRequest); // If provider owned the session, but failed to authenticate anyway, that likely means that
    // session is not valid and we should clear it. Also provider can specifically ask to clear
    // session by setting it to `null` even if authentication attempt didn't fail.

    if (authenticationResult.shouldClearState() || authenticationResult.failed() && (0, _errors.getErrorStatusCode)(authenticationResult.error) === 401) {
      sessionStorage.clear();
    } else if (sessionCanBeUpdated) {
      sessionStorage.set({
        state: authenticationResult.shouldUpdateState() ? authenticationResult.state : existingSession.state,
        provider: providerType,
        expires: this.ttl && Date.now() + this.ttl
      });
    }
  }

}

exports.Authenticator = Authenticator;