"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "PluginSetupContract", {
  enumerable: true,
  get: function () {
    return _plugin.PluginSetupContract;
  }
});
Object.defineProperty(exports, "wrapError", {
  enumerable: true,
  get: function () {
    return _errors.wrapError;
  }
});
Object.defineProperty(exports, "canRedirectRequest", {
  enumerable: true,
  get: function () {
    return _authentication.canRedirectRequest;
  }
});
Object.defineProperty(exports, "AuthenticationResult", {
  enumerable: true,
  get: function () {
    return _authentication.AuthenticationResult;
  }
});
Object.defineProperty(exports, "DeauthenticationResult", {
  enumerable: true,
  get: function () {
    return _authentication.DeauthenticationResult;
  }
});
Object.defineProperty(exports, "OIDCAuthenticationFlow", {
  enumerable: true,
  get: function () {
    return _authentication.OIDCAuthenticationFlow;
  }
});
Object.defineProperty(exports, "CreateAPIKeyResult", {
  enumerable: true,
  get: function () {
    return _authentication.CreateAPIKeyResult;
  }
});
exports.plugin = exports.config = void 0;

var _config = require("./config");

var _plugin = require("./plugin");

var _errors = require("./errors");

var _authentication = require("./authentication");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// These exports are part of public Security plugin contract, any change in signature of exported
// functions or removal of exports should be considered as a breaking change. Ideally we should
// reduce number of such exports to zero and provide everything we want to expose via Setup/Start
// run-time contracts.
const config = {
  schema: _config.ConfigSchema
};
exports.config = config;

const plugin = initializerContext => new _plugin.Plugin(initializerContext);

exports.plugin = plugin;