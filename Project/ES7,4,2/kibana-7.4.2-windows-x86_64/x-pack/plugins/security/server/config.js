"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createConfig$ = createConfig$;
exports.ConfigSchema = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _operators = require("rxjs/operators");

var _configSchema = require("@kbn/config-schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const providerOptionsSchema = (providerType, optionsSchema) => _configSchema.schema.conditional(_configSchema.schema.siblingRef('providers'), _configSchema.schema.arrayOf(_configSchema.schema.string(), {
  validate: providers => !providers.includes(providerType) ? 'error' : undefined
}), optionsSchema, _configSchema.schema.never());

const ConfigSchema = _configSchema.schema.object({
  cookieName: _configSchema.schema.string({
    defaultValue: 'sid'
  }),
  encryptionKey: _configSchema.schema.conditional(_configSchema.schema.contextRef('dist'), true, _configSchema.schema.maybe(_configSchema.schema.string({
    minLength: 32
  })), _configSchema.schema.string({
    minLength: 32,
    defaultValue: 'a'.repeat(32)
  })),
  sessionTimeout: _configSchema.schema.oneOf([_configSchema.schema.number(), _configSchema.schema.literal(null)], {
    defaultValue: null
  }),
  secureCookies: _configSchema.schema.boolean({
    defaultValue: false
  }),
  public: _configSchema.schema.object({
    protocol: _configSchema.schema.maybe(_configSchema.schema.oneOf([_configSchema.schema.literal('http'), _configSchema.schema.literal('https')])),
    hostname: _configSchema.schema.maybe(_configSchema.schema.string({
      hostname: true
    })),
    port: _configSchema.schema.maybe(_configSchema.schema.number({
      min: 0,
      max: 65535
    }))
  }),
  authc: _configSchema.schema.object({
    providers: _configSchema.schema.arrayOf(_configSchema.schema.string(), {
      defaultValue: ['basic'],
      minSize: 1
    }),
    oidc: providerOptionsSchema('oidc', _configSchema.schema.maybe(_configSchema.schema.object({
      realm: _configSchema.schema.string()
    }))),
    saml: providerOptionsSchema('saml', _configSchema.schema.maybe(_configSchema.schema.object({
      realm: _configSchema.schema.maybe(_configSchema.schema.string())
    })))
  })
}, // This option should be removed as soon as we entirely migrate config from legacy Security plugin.
{
  allowUnknowns: true
}); // HACK: Since new platform doesn't support config deprecation transformations yet (e.g. `rename`), we have to handle
// them manually here for the time being. Legacy platform config will log corresponding deprecation warnings.


exports.ConfigSchema = ConfigSchema;
const origValidate = ConfigSchema.validate;

ConfigSchema.validate = (value, context, namespace) => {
  // Rename deprecated `xpack.security.authProviders` to `xpack.security.authc.providers`.
  if (value && value.authProviders) {
    value.authc = { ...(value.authc || {}),
      providers: value.authProviders
    };
    delete value.authProviders;
  }

  return origValidate.call(ConfigSchema, value, context, namespace);
};

function createConfig$(context, isTLSEnabled) {
  return context.config.create().pipe((0, _operators.map)(config => {
    const logger = context.logger.get('config');
    let encryptionKey = config.encryptionKey;

    if (encryptionKey === undefined) {
      logger.warn('Generating a random key for xpack.security.encryptionKey. To prevent sessions from being invalidated on ' + 'restart, please set xpack.security.encryptionKey in kibana.yml');
      encryptionKey = _crypto.default.randomBytes(16).toString('hex');
    }

    let secureCookies = config.secureCookies;

    if (!isTLSEnabled) {
      if (secureCookies) {
        logger.warn('Using secure cookies, but SSL is not enabled inside Kibana. SSL must be configured outside of Kibana to ' + 'function properly.');
      } else {
        logger.warn('Session cookies will be transmitted over insecure connections. This is not recommended.');
      }
    } else if (!secureCookies) {
      secureCookies = true;
    }

    return { ...config,
      encryptionKey,
      secureCookies
    };
  }));
}