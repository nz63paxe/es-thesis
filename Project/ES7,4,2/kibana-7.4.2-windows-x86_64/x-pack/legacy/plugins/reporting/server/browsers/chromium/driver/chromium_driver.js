"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeadlessChromiumDriver = void 0;

var _url = require("url");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const WAIT_FOR_DELAY_MS = 100;

class HeadlessChromiumDriver {
  constructor(page) {
    _defineProperty(this, "page", void 0);

    this.page = page;
  }

  async open(url, {
    conditionalHeaders,
    waitForSelector
  }, logger) {
    logger.info(`opening url ${url}`);
    await this.page.setRequestInterception(true);
    let interceptedCount = 0;
    this.page.on('request', interceptedRequest => {
      let interceptedUrl = interceptedRequest.url();
      let isData = false;

      if (interceptedUrl.startsWith('file://')) {
        logger.error(`Got bad URL: "${interceptedUrl}", closing browser.`);
        interceptedRequest.abort('blockedbyclient');
        this.page.browser().close();
        throw new Error(`Received disallowed outgoing URL: "${interceptedUrl}", exiting`);
      }

      if (this._shouldUseCustomHeaders(conditionalHeaders.conditions, interceptedUrl)) {
        logger.debug(`Using custom headers for ${interceptedUrl}`);
        interceptedRequest.continue({
          headers: { ...interceptedRequest.headers(),
            ...conditionalHeaders.headers
          }
        });
      } else {
        if (interceptedUrl.startsWith('data:')) {
          // `data:image/xyz;base64` can be very long URLs
          interceptedUrl = interceptedUrl.substring(0, 100) + '[truncated]';
          isData = true;
        }

        logger.debug(`No custom headers for ${interceptedUrl}`);
        interceptedRequest.continue();
      }

      interceptedCount = interceptedCount + (isData ? 0 : 1);
    }); // Even though 3xx redirects go through our request
    // handler, we should probably inspect responses just to
    // avoid being bamboozled by some malicious request

    this.page.on('response', interceptedResponse => {
      const interceptedUrl = interceptedResponse.url();

      if (interceptedUrl.startsWith('file://')) {
        logger.error(`Got disallowed URL "${interceptedUrl}", closing browser.`);
        this.page.browser().close();
        throw new Error(`Received disallowed URL in response: ${interceptedUrl}`);
      }
    });
    await this.page.goto(url, {
      waitUntil: 'domcontentloaded'
    });
    await this.waitForSelector(waitForSelector, {}, logger);
    logger.info(`handled ${interceptedCount} page requests`);
  }

  async screenshot(elementPosition) {
    let clip;

    if (elementPosition) {
      const {
        boundingClientRect,
        scroll = {
          x: 0,
          y: 0
        }
      } = elementPosition;
      clip = {
        x: boundingClientRect.left + scroll.x,
        y: boundingClientRect.top + scroll.y,
        height: boundingClientRect.height,
        width: boundingClientRect.width
      };
    }

    const screenshot = await this.page.screenshot({
      clip
    });
    return screenshot.toString('base64');
  }

  async evaluate({
    fn,
    args = []
  }) {
    const result = await this.page.evaluate(fn, ...args);
    return result;
  }

  async waitForSelector(selector, opts = {}, logger) {
    const {
      silent = false
    } = opts;
    logger.debug(`waitForSelector ${selector}`);
    let resp;

    try {
      resp = await this.page.waitFor(selector);
    } catch (err) {
      if (!silent) {
        // Provide some troubleshooting info to see if we're on the login page,
        // "Kibana could not load correctly", etc
        logger.error(`waitForSelector ${selector} failed on ${this.page.url()}`);
        const pageText = await this.evaluate({
          fn: () => document.querySelector('body').innerText,
          args: []
        });
        logger.debug(`Page plain text: ${pageText.replace(/\n/g, '\\n')}`); // replace newline with escaped for single log line
      }

      throw err;
    }

    logger.debug(`waitForSelector ${selector} resolved`);
    return resp;
  }

  async waitFor({
    fn,
    args,
    toEqual
  }) {
    while (true) {
      const result = await this.evaluate({
        fn,
        args
      });

      if (result === toEqual) {
        return;
      }

      await new Promise(r => setTimeout(r, WAIT_FOR_DELAY_MS));
    }
  }

  async setViewport({
    width,
    height,
    zoom
  }, logger) {
    logger.debug(`Setting viewport to width: ${width}, height: ${height}, zoom: ${zoom}`);
    await this.page.setViewport({
      width: Math.floor(width / zoom),
      height: Math.floor(height / zoom),
      deviceScaleFactor: zoom,
      isMobile: false
    });
  }

  _shouldUseCustomHeaders(conditions, url) {
    const {
      hostname,
      protocol,
      port,
      pathname
    } = (0, _url.parse)(url);

    if (pathname === undefined) {
      // There's a discrepancy between the NodeJS docs and the typescript types. NodeJS docs
      // just say 'string' and the typescript types say 'string | undefined'. We haven't hit a
      // situation where it's undefined but here's an explicit Error if we do.
      throw new Error(`pathname is undefined, don't know how to proceed`);
    }

    return hostname === conditions.hostname && protocol === `${conditions.protocol}:` && this._shouldUseCustomHeadersForPort(conditions, port) && pathname.startsWith(`${conditions.basePath}/`);
  }

  _shouldUseCustomHeadersForPort(conditions, port) {
    if (conditions.protocol === 'http' && conditions.port === 80) {
      return port === undefined || port === null || port === '' || port === conditions.port.toString();
    }

    if (conditions.protocol === 'https' && conditions.port === 443) {
      return port === undefined || port === null || port === '' || port === conditions.port.toString();
    }

    return port === conditions.port.toString();
  }

}

exports.HeadlessChromiumDriver = HeadlessChromiumDriver;