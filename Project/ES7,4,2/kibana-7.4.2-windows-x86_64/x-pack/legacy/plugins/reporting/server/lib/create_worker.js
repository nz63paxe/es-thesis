"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createWorkerFactory = void 0;

var _constants = require("../../common/constants");

var _esqueue = require("./esqueue");

var _level_logger = require("./level_logger");

var _once_per_server = require("./once_per_server");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped dependency
function createWorkerFn(server) {
  const config = server.config();
  const queueConfig = config.get('xpack.reporting.queue');
  const kibanaName = config.get('server.name');
  const kibanaId = config.get('server.uuid');
  const exportTypesRegistry = server.plugins.reporting.exportTypesRegistry;

  const logger = _level_logger.LevelLogger.createForServer(server, [_constants.PLUGIN_ID, 'queue-worker']); // Once more document types are added, this will need to be passed in


  return function createWorker(queue) {
    // export type / execute job map
    const jobExectors = new Map();

    for (const exportType of exportTypesRegistry.getAll()) {
      const executeJob = exportType.executeJobFactory(server);
      jobExectors.set(exportType.jobType, executeJob);
    }

    const workerFn = (job, jobdoc, cancellationToken) => {
      // pass the work to the jobExecutor
      const jobExecutor = jobExectors.get(job._source.jobtype);

      if (!jobExecutor) {
        throw new Error(`Unable to find a job executor for the claimed job: [${job._id}]`);
      }

      return jobExecutor(job._id, jobdoc, cancellationToken);
    };

    const workerOptions = {
      kibanaName,
      kibanaId,
      interval: queueConfig.pollInterval,
      intervalErrorMultiplier: queueConfig.pollIntervalErrorMultiplier
    };
    const worker = queue.registerWorker(_constants.PLUGIN_ID, workerFn, workerOptions);
    worker.on(_esqueue.events.EVENT_WORKER_COMPLETE, res => {
      logger.debug(`Worker completed: (${res.job.id})`);
    });
    worker.on(_esqueue.events.EVENT_WORKER_JOB_EXECUTION_ERROR, res => {
      logger.debug(`Worker error: (${res.job.id})`);
    });
    worker.on(_esqueue.events.EVENT_WORKER_JOB_TIMEOUT, res => {
      logger.debug(`Job timeout exceeded: (${res.job.id})`);
    });
  };
}

const createWorkerFactory = (0, _once_per_server.oncePerServer)(createWorkerFn);
exports.createWorkerFactory = createWorkerFactory;