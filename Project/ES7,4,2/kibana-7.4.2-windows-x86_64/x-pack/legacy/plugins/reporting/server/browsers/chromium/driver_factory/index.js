"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeadlessChromiumDriverFactory = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _os = _interopRequireDefault(require("os"));

var _path = _interopRequireDefault(require("path"));

var _rimraf = _interopRequireDefault(require("rimraf"));

var Rx = _interopRequireWildcard(require("rxjs"));

var _operators = require("rxjs/operators");

var _puppeteer = require("../puppeteer");

var _driver = require("../driver");

var _args = require("./args");

var _safe_child_process = require("../../safe_child_process");

var _paths = require("../paths");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const compactWhitespace = str => {
  return str.replace(/\s+/, ' ');
};

class HeadlessChromiumDriverFactory {
  constructor(binaryPath, logger, browserConfig, queueTimeout) {
    _defineProperty(this, "binaryPath", void 0);

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "browserConfig", void 0);

    _defineProperty(this, "queueTimeout", void 0);

    _defineProperty(this, "type", 'chromium');

    this.binaryPath = binaryPath;
    this.browserConfig = browserConfig;
    this.queueTimeout = queueTimeout;
    this.logger = logger;
  }

  test({
    viewport,
    browserTimezone
  }, logger) {
    const userDataDir = _fs.default.mkdtempSync(_path.default.join(_os.default.tmpdir(), 'chromium-'));

    const chromiumArgs = (0, _args.args)({
      userDataDir,
      viewport,
      verboseLogging: true,
      disableSandbox: this.browserConfig.disableSandbox,
      proxyConfig: this.browserConfig.proxy
    });
    return (0, _puppeteer.puppeteerLaunch)({
      userDataDir,
      executablePath: this.binaryPath,
      ignoreHTTPSErrors: true,
      args: chromiumArgs,
      env: {
        TZ: browserTimezone
      }
    }).catch(error => {
      logger.warning(`The Reporting plugin encountered issues launching Chromium in a self-test. You may have trouble generating reports: [${error}]`);
      logger.warning(`See Chromium's log output at "${(0, _paths.getChromeLogLocation)(this.binaryPath)}"`);
      return null;
    });
  }

  create({
    viewport,
    browserTimezone
  }) {
    return Rx.Observable.create(async observer => {
      this.logger.debug(`Creating browser driver factory`);

      const userDataDir = _fs.default.mkdtempSync(_path.default.join(_os.default.tmpdir(), 'chromium-'));

      const chromiumArgs = (0, _args.args)({
        userDataDir,
        viewport,
        verboseLogging: this.logger.isVerbose,
        disableSandbox: this.browserConfig.disableSandbox,
        proxyConfig: this.browserConfig.proxy
      });
      let browser;
      let page;

      try {
        browser = await (0, _puppeteer.puppeteerLaunch)({
          pipe: !this.browserConfig.inspect,
          userDataDir,
          executablePath: this.binaryPath,
          ignoreHTTPSErrors: true,
          args: chromiumArgs,
          env: {
            TZ: browserTimezone
          }
        });
        page = await browser.newPage(); // All navigation/waitFor methods default to 30 seconds,
        // which can cause the job to fail even if we bump timeouts in
        // the config. Help alleviate errors like
        // "TimeoutError: waiting for selector ".application" failed: timeout 30000ms exceeded"

        page.setDefaultTimeout(this.queueTimeout);
        this.logger.debug(`Browser driver factory created`);
      } catch (err) {
        observer.error(new Error(`Error spawning Chromium browser: [${err}]`));
        throw err;
      }

      const childProcess = {
        async kill() {
          await browser.close();
        }

      };
      const {
        terminate$
      } = (0, _safe_child_process.safeChildProcess)(this.logger, childProcess); // this is adding unsubscribe logic to our observer
      // so that if our observer unsubscribes, we terminate our child-process

      observer.add(() => {
        this.logger.debug(`The browser process observer has unsubscribed. Closing the browser...`);
        childProcess.kill(); // ignore async
      }); // make the observer subscribe to terminate$

      observer.add(terminate$.pipe((0, _operators.tap)(signal => {
        this.logger.debug(`Observer got signal: ${signal}`);
      }), (0, _operators.ignoreElements)()).subscribe(observer)); // Register with a few useful puppeteer event handlers:
      // https://pptr.dev/#?product=Puppeteer&version=v1.10.0&show=api-event-error
      // https://github.com/GoogleChrome/puppeteer/blob/master/docs/api.md#class-page

      const stderr$ = Rx.fromEvent(page, 'console').pipe((0, _operators.filter)(line => line._type === 'error'), (0, _operators.map)(line => line._text), (0, _operators.share)());
      const [consoleMessage$, message$] = (0, _operators.partition)(msg => !!msg.match(/\[\d+\/\d+.\d+:\w+:CONSOLE\(\d+\)\]/))(stderr$);
      const driver$ = Rx.of(new _driver.HeadlessChromiumDriver(page));
      const processError$ = Rx.fromEvent(page, 'error').pipe((0, _operators.mergeMap)(err => Rx.throwError(new Error(`Unable to spawn Chromium: [${err}]`))));
      const processPageError$ = Rx.fromEvent(page, 'pageerror').pipe((0, _operators.mergeMap)(err => Rx.throwError(new Error(`Uncaught exception within the page: [${err}]`))));
      const processRequestFailed$ = Rx.fromEvent(page, 'requestfailed').pipe((0, _operators.mergeMap)(req => {
        const failure = req.failure && req.failure();

        if (failure) {
          return Rx.throwError(new Error(`Request to [${req.url()}] failed! [${failure.errorText}]`));
        }

        return Rx.throwError(new Error(`Unknown failure! [${JSON.stringify(req)}]`));
      }));
      const processExit$ = Rx.fromEvent(browser, 'disconnected').pipe((0, _operators.mergeMap)(code => Rx.throwError(new Error(`Chromium exited with: [${JSON.stringify({
        code
      })}]`))));
      const nssError$ = message$.pipe((0, _operators.filter)(line => line.includes('error while loading shared libraries: libnss3.so')), (0, _operators.mergeMap)(() => Rx.throwError(new Error(`You must install nss for Reporting to work`))));
      const fontError$ = message$.pipe((0, _operators.filter)(line => line.includes('Check failed: InitDefaultFont(). Could not find the default font')), (0, _operators.mergeMap)(() => Rx.throwError(new Error('You must install freetype and ttf-font for Reporting to work'))));
      const noUsableSandbox$ = message$.pipe((0, _operators.filter)(line => line.includes('No usable sandbox! Update your kernel')), (0, _operators.mergeMap)(() => Rx.throwError(new Error(compactWhitespace(`
          Unable to use Chromium sandbox. This can be disabled at your own risk with
          'xpack.reporting.capture.browser.chromium.disableSandbox'
        `)))));
      const exit$ = Rx.merge(processError$, processPageError$, processRequestFailed$, processExit$, nssError$, fontError$, noUsableSandbox$);
      observer.next({
        driver$,
        consoleMessage$,
        message$,
        exit$
      });
      const factoryLogger = this.logger.clone(['chromium-driver-factory']); // unsubscribe logic makes a best-effort attempt to delete the user data directory used by chromium

      observer.add(() => {
        factoryLogger.debug(`deleting chromium user data directory at [${userDataDir}]`); // the unsubscribe function isn't `async` so we're going to make our best effort at
        // deleting the userDataDir and if it fails log an error.

        (0, _rimraf.default)(userDataDir, err => {
          if (err) {
            return factoryLogger.error(`error deleting user data directory at [${userDataDir}]: [${err}]`);
          }
        });
      });
    });
  }

}

exports.HeadlessChromiumDriverFactory = HeadlessChromiumDriverFactory;