"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateConfig = validateConfig;

var _crypto = _interopRequireDefault(require("crypto"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function validateConfig(config, logger) {
  const encryptionKey = config.get('xpack.reporting.encryptionKey');

  if (encryptionKey == null) {
    logger.warning(`Generating a random key for xpack.reporting.encryptionKey. To prevent pending reports from failing on restart, please set ` + `xpack.reporting.encryptionKey in kibana.yml`);
    config.set('xpack.reporting.encryptionKey', _crypto.default.randomBytes(16).toString('hex'));
  }
}