"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.runValidations = runValidations;

var _validate_browser = require("./validate_browser");

var _validate_config = require("./validate_config");

var _validate_max_content_length = require("./validate_max_content_length");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function runValidations(server, config, logger, browserFactory) {
  try {
    await Promise.all([(0, _validate_browser.validateBrowser)(server, browserFactory, logger), (0, _validate_config.validateConfig)(config, logger), (0, _validate_max_content_length.validateMaxContentLength)(server, logger)]);
    logger.debug(`Reporting plugin self-check ok!`);
  } catch (err) {
    logger.warning(`Reporting plugin self-check failed. Please check the Kibana Reporting settings. ${err}`);
  }
}