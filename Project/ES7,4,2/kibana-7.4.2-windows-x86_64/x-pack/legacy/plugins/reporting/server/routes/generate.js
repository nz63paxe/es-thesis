"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerGenerate = registerGenerate;

var _boom = _interopRequireDefault(require("boom"));

var _risonNode = _interopRequireDefault(require("rison-node"));

var _constants = require("../../common/constants");

var _route_config_factories = require("./lib/route_config_factories");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const BASE_GENERATE = `${_constants.API_BASE_URL}/generate`;

function registerGenerate(server, handler, handleError) {
  const getRouteConfig = (0, _route_config_factories.getRouteConfigFactoryReportingPre)(server); // generate report

  server.route({
    path: `${BASE_GENERATE}/{exportType}`,
    method: 'POST',
    config: getRouteConfig(request => request.params.exportType),
    handler: async (request, h) => {
      const {
        exportType
      } = request.params;
      let response;

      try {
        // @ts-ignore
        const jobParams = _risonNode.default.decode(request.query.jobParams);

        response = await handler(exportType, jobParams, request, h);
      } catch (err) {
        throw handleError(exportType, err);
      }

      return response;
    }
  }); // show error about GET method to user

  server.route({
    path: `${BASE_GENERATE}/{p*}`,
    method: 'GET',
    config: getRouteConfig(),
    handler: () => {
      const err = _boom.default.methodNotAllowed('GET is not allowed');

      err.output.headers.allow = 'POST';
      return err;
    }
  });
}