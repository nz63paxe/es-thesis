"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerJobs = registerJobs;

var _boom = _interopRequireDefault(require("boom"));

var _constants = require("../../common/constants");

var _jobs_query = require("../lib/jobs_query");

var _job_response_handler = require("./lib/job_response_handler");

var _route_config_factories = require("./lib/route_config_factories");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
const MAIN_ENTRY = `${_constants.API_BASE_URL}/jobs`;

function registerJobs(server) {
  const jobsQuery = (0, _jobs_query.jobsQueryFactory)(server);
  const getRouteConfig = (0, _route_config_factories.getRouteConfigFactoryManagementPre)(server);
  const getRouteConfigDownload = (0, _route_config_factories.getRouteConfigFactoryDownloadPre)(server); // list jobs in the queue, paginated

  server.route({
    path: `${MAIN_ENTRY}/list`,
    method: 'GET',
    config: getRouteConfig(),
    handler: request => {
      // @ts-ignore
      const page = parseInt(request.query.page, 10) || 0; // @ts-ignore

      const size = Math.min(100, parseInt(request.query.size, 10) || 10); // @ts-ignore

      const jobIds = request.query.ids ? request.query.ids.split(',') : null;
      const results = jobsQuery.list(request.pre.management.jobTypes, request.pre.user, page, size, jobIds);
      return results;
    }
  }); // return the count of all jobs in the queue

  server.route({
    path: `${MAIN_ENTRY}/count`,
    method: 'GET',
    config: getRouteConfig(),
    handler: request => {
      const results = jobsQuery.count(request.pre.management.jobTypes, request.pre.user);
      return results;
    }
  }); // return the raw output from a job

  server.route({
    path: `${MAIN_ENTRY}/output/{docId}`,
    method: 'GET',
    config: getRouteConfig(),
    handler: request => {
      const {
        docId
      } = request.params;
      return jobsQuery.get(request.pre.user, docId, {
        includeContent: true
      }).then(doc => {
        const job = doc._source;

        if (!job) {
          throw _boom.default.notFound();
        }

        const {
          jobtype: jobType
        } = job;

        if (!request.pre.management.jobTypes.includes(jobType)) {
          throw _boom.default.unauthorized(`Sorry, you are not authorized to download ${jobType} reports`);
        }

        return job.output;
      });
    }
  }); // return some info about the job

  server.route({
    path: `${MAIN_ENTRY}/info/{docId}`,
    method: 'GET',
    config: getRouteConfig(),
    handler: request => {
      const {
        docId
      } = request.params;
      return jobsQuery.get(request.pre.user, docId).then(doc => {
        const job = doc._source;

        if (!job) {
          throw _boom.default.notFound();
        }

        const {
          jobtype: jobType,
          payload
        } = job;

        if (!request.pre.management.jobTypes.includes(jobType)) {
          throw _boom.default.unauthorized(`Sorry, you are not authorized to view ${jobType} info`);
        }

        return { ...doc._source,
          payload: { ...payload,
            headers: undefined
          }
        };
      });
    }
  }); // trigger a download of the output from a job

  const jobResponseHandler = (0, _job_response_handler.jobResponseHandlerFactory)(server);
  server.route({
    path: `${MAIN_ENTRY}/download/{docId}`,
    method: 'GET',
    config: getRouteConfigDownload(),
    handler: async (request, h) => {
      const {
        docId
      } = request.params;
      let response = await jobResponseHandler(request.pre.management.jobTypes, request.pre.user, h, {
        docId
      });
      const {
        statusCode
      } = response;

      if (statusCode !== 200) {
        const logLevel = statusCode === 500 ? 'error' : 'debug';
        server.log([logLevel, 'reporting', 'download'], `Report ${docId} has non-OK status: [${statusCode}] Reason: [${JSON.stringify(response.source)}]`);
      }

      if (!response.isBoom) {
        response = response.header('accept-ranges', 'none');
      }

      return response;
    }
  });
}