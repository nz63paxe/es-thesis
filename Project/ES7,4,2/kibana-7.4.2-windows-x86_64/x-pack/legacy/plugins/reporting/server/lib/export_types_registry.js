"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.exportTypesRegistryFactory = void 0;

var _path = require("path");

var _glob = _interopRequireDefault(require("glob"));

var _constants = require("../../common/constants");

var _once_per_server = require("./once_per_server");

var _level_logger = require("./level_logger");

var _export_types_registry = require("../../common/export_types_registry");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untype module
function scan(pattern) {
  return new Promise((resolve, reject) => {
    (0, _glob.default)(pattern, {}, (err, files) => {
      if (err) {
        return reject(err);
      }

      resolve(files);
    });
  });
}

const pattern = (0, _path.resolve)(__dirname, '../../export_types/*/server/index.[jt]s');

async function exportTypesRegistryFn(server) {
  const logger = _level_logger.LevelLogger.createForServer(server, [_constants.PLUGIN_ID, 'exportTypes']);

  const exportTypesRegistry = new _export_types_registry.ExportTypesRegistry();
  const files = await scan(pattern);
  files.forEach(file => {
    logger.debug(`Found exportType at ${file}`);

    const {
      register
    } = require(file); // eslint-disable-line @typescript-eslint/no-var-requires


    register(exportTypesRegistry);
  });
  return exportTypesRegistry;
}

const exportTypesRegistryFactory = (0, _once_per_server.oncePerServer)(exportTypesRegistryFn);
exports.exportTypesRegistryFactory = exportTypesRegistryFactory;