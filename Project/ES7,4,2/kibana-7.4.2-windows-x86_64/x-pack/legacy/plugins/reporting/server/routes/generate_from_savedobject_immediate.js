"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerGenerateCsvFromSavedObjectImmediate = registerGenerateCsvFromSavedObjectImmediate;

var _constants = require("../../common/constants");

var _csv_from_savedobject = require("../../export_types/csv_from_savedobject");

var _route_config_factories = require("./lib/route_config_factories");

var _get_job_params_from_request = require("../../export_types/csv_from_savedobject/server/lib/get_job_params_from_request");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * This function registers API Endpoints for immediate Reporting jobs. The API inputs are:
 * - saved object type and ID
 * - time range and time zone
 * - application state:
 *     - filters
 *     - query bar
 *     - local (transient) changes the user made to the saved object
 */
function registerGenerateCsvFromSavedObjectImmediate(server, parentLogger) {
  const routeOptions = (0, _route_config_factories.getRouteOptions)(server);
  /*
   * CSV export with the `immediate` option does not queue a job with Reporting's ESQueue to run the job async. Instead, this does:
   *  - re-use the createJob function to build up es query config
   *  - re-use the executeJob function to run the scan and scroll queries and capture the entire CSV in a result object.
   */

  server.route({
    path: `${_constants.API_BASE_GENERATE_V1}/immediate/csv/saved-object/{savedObjectType}:{savedObjectId}`,
    method: 'POST',
    options: routeOptions,
    handler: async (request, h) => {
      const logger = parentLogger.clone(['savedobject-csv']);
      const jobParams = (0, _get_job_params_from_request.getJobParamsFromRequest)(request, {
        isImmediate: true
      });
      const createJobFn = (0, _csv_from_savedobject.createJobFactory)(server);
      const executeJobFn = (0, _csv_from_savedobject.executeJobFactory)(server);
      const jobDocPayload = await createJobFn(jobParams, request.headers, request);
      const {
        content_type: jobOutputContentType,
        content: jobOutputContent,
        size: jobOutputSize
      } = await executeJobFn(null, jobDocPayload, request);
      logger.info(`Job output size: ${jobOutputSize} bytes`);
      /*
       * ESQueue worker function defaults `content` to null, even if the
       * executeJob returned undefined.
       *
       * This converts null to undefined so the value can be sent to h.response()
       */

      if (jobOutputContent === null) {
        logger.warn('CSV Job Execution created empty content result');
      }

      const response = h.response(jobOutputContent ? jobOutputContent : undefined).type(jobOutputContentType); // Set header for buffer download, not streaming

      const {
        isBoom
      } = response;

      if (isBoom == null) {
        response.header('accept-ranges', 'none');
      }

      return response;
    }
  });
}