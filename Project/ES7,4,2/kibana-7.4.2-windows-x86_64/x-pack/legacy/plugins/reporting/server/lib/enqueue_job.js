"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enqueueJobFactory = void 0;

var _lodash = require("lodash");

var _esqueue = require("./esqueue");

var _once_per_server = require("./once_per_server");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function enqueueJobFn(server) {
  const jobQueue = server.plugins.reporting.queue;
  const config = server.config();
  const queueConfig = config.get('xpack.reporting.queue');
  const browserType = config.get('xpack.reporting.capture.browser.type');
  const exportTypesRegistry = server.plugins.reporting.exportTypesRegistry;
  return async function enqueueJob(parentLogger, exportTypeId, jobParams, user, headers, request) {
    const logger = parentLogger.clone(['queue-job']);
    const exportType = exportTypesRegistry.getById(exportTypeId);
    const createJob = exportType.createJobFactory(server);
    const payload = await createJob(jobParams, headers, request);
    const options = {
      timeout: queueConfig.timeout,
      created_by: (0, _lodash.get)(user, 'username', false),
      browser_type: browserType
    };
    return new Promise((resolve, reject) => {
      const job = jobQueue.addJob(exportType.jobType, payload, options);
      job.on(_esqueue.events.EVENT_JOB_CREATED, createdJob => {
        if (createdJob.id === job.id) {
          logger.info(`Successfully queued job: ${createdJob.id}`);
          resolve(job);
        }
      });
      job.on(_esqueue.events.EVENT_JOB_CREATE_ERROR, reject);
    });
  };
}

const enqueueJobFactory = (0, _once_per_server.oncePerServer)(enqueueJobFn);
exports.enqueueJobFactory = enqueueJobFactory;