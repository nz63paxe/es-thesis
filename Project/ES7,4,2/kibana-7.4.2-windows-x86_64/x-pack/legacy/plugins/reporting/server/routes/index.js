"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerRoutes = registerRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _constants = require("../../common/constants");

var _enqueue_job = require("../lib/enqueue_job");

var _generate = require("./generate");

var _generate_from_savedobject = require("./generate_from_savedobject");

var _generate_from_savedobject_immediate = require("./generate_from_savedobject_immediate");

var _jobs = require("./jobs");

var _legacy = require("./legacy");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerRoutes(server, logger) {
  const config = server.config();
  const DOWNLOAD_BASE_URL = config.get('server.basePath') + `${_constants.API_BASE_URL}/jobs/download`;
  const {
    errors: esErrors
  } = server.plugins.elasticsearch.getCluster('admin');
  const enqueueJob = (0, _enqueue_job.enqueueJobFactory)(server);
  /*
   * Generates enqueued job details to use in responses
   */

  async function handler(exportTypeId, jobParams, request, h) {
    // @ts-ignore
    const user = request.pre.user;
    const headers = request.headers;
    const job = await enqueueJob(logger, exportTypeId, jobParams, user, headers, request); // return the queue's job information

    const jobJson = job.toJSON();
    return h.response({
      path: `${DOWNLOAD_BASE_URL}/${jobJson.id}`,
      job: jobJson
    }).type('application/json');
  }

  function handleError(exportTypeId, err) {
    if (err instanceof esErrors['401']) {
      return _boom.default.unauthorized(`Sorry, you aren't authenticated`);
    }

    if (err instanceof esErrors['403']) {
      return _boom.default.forbidden(`Sorry, you are not authorized to create ${exportTypeId} reports`);
    }

    if (err instanceof esErrors['404']) {
      return _boom.default.boomify(err, {
        statusCode: 404
      });
    }

    return err;
  }

  (0, _generate.registerGenerate)(server, handler, handleError);
  (0, _legacy.registerLegacy)(server, handler, handleError); // Register beta panel-action download-related API's

  if (config.get('xpack.reporting.csv.enablePanelActionDownload')) {
    (0, _generate_from_savedobject.registerGenerateCsvFromSavedObject)(server, handler, handleError);
    (0, _generate_from_savedobject_immediate.registerGenerateCsvFromSavedObjectImmediate)(server, logger);
  }

  (0, _jobs.registerJobs)(server);
}