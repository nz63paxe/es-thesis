"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getReportingUsageCollector = getReportingUsageCollector;

var _constants = require("../../../monitoring/common/constants");

var _constants2 = require("../../common/constants");

var _get_reporting_usage = require("./get_reporting_usage");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped module

/*
 * @param {Object} server
 * @return {Object} kibana usage stats type collection object
 */
function getReportingUsageCollector(server, isReady) {
  const {
    collectorSet
  } = server.usage;
  return collectorSet.makeUsageCollector({
    type: _constants2.KIBANA_REPORTING_TYPE,
    isReady,
    fetch: callCluster => (0, _get_reporting_usage.getReportingUsage)(server, callCluster),

    /*
     * Format the response data into a model for internal upload
     * 1. Make this data part of the "kibana_stats" type
     * 2. Organize the payload in the usage.xpack.reporting namespace of the data payload
     */
    formatForBulkUpload: result => {
      return {
        type: _constants.KIBANA_STATS_TYPE_MONITORING,
        payload: {
          usage: {
            xpack: {
              reporting: result
            }
          }
        }
      };
    }
  });
}