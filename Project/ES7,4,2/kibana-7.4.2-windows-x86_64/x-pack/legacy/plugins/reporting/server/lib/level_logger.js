"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LevelLogger = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class LevelLogger {
  static createForServer(server, tags, isVerbose = false) {
    const serverLog = (tgs, msg) => server.log(tgs, msg);

    return new LevelLogger(serverLog, tags, isVerbose);
  }

  constructor(logger, tags, isVerbose) {
    _defineProperty(this, "_logger", void 0);

    _defineProperty(this, "_tags", void 0);

    _defineProperty(this, "_isVerbose", void 0);

    _defineProperty(this, "warn", void 0);

    this._logger = logger;
    this._tags = tags;
    this._isVerbose = isVerbose;
    /*
     * This shortcut provides maintenance convenience: Reporting code has been
     * using both .warn and .warning
     */

    this.warn = this.warning.bind(this);
  }

  error(msg, tags = []) {
    this._logger([...this._tags, ...tags, 'error'], msg);
  }

  warning(msg, tags = []) {
    this._logger([...this._tags, ...tags, 'warning'], msg);
  }

  debug(msg, tags = []) {
    this._logger([...this._tags, ...tags, 'debug'], msg);
  }

  info(msg, tags = []) {
    this._logger([...this._tags, ...tags, 'info'], msg);
  }

  get isVerbose() {
    return this._isVerbose;
  }

  clone(tags) {
    return new LevelLogger(this._logger, [...this._tags, ...tags], this._isVerbose);
  }

}

exports.LevelLogger = LevelLogger;