"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CancellationToken = void 0;

var _lodash = require("lodash");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class CancellationToken {
  constructor() {
    _defineProperty(this, "isCancelled", void 0);

    _defineProperty(this, "_callbacks", void 0);

    _defineProperty(this, "on", callback => {
      if (!(0, _lodash.isFunction)(callback)) {
        throw new Error('Expected callback to be a function');
      }

      if (this.isCancelled) {
        callback();
        return;
      }

      this._callbacks.push(callback);
    });

    _defineProperty(this, "cancel", () => {
      this.isCancelled = true;

      this._callbacks.forEach(callback => callback());
    });

    this.isCancelled = false;
    this._callbacks = [];
  }

}

exports.CancellationToken = CancellationToken;