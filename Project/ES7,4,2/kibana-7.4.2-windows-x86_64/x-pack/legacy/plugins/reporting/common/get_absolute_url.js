"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAbsoluteUrlFactory = void 0;

var _url = _interopRequireDefault(require("url"));

var _once_per_server = require("../server/lib/once_per_server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function getAbsoluteUrlFn(server) {
  const config = server.config();
  return function getAbsoluteUrl({
    basePath = config.get('server.basePath'),
    hash = '',
    path = '/app/kibana',
    search = ''
  } = {}) {
    return _url.default.format({
      protocol: config.get('xpack.reporting.kibanaServer.protocol') || server.info.protocol,
      hostname: config.get('xpack.reporting.kibanaServer.hostname') || config.get('server.host'),
      port: config.get('xpack.reporting.kibanaServer.port') || config.get('server.port'),
      pathname: basePath + path,
      hash,
      search
    });
  };
}

const getAbsoluteUrlFactory = (0, _once_per_server.oncePerServer)(getAbsoluteUrlFn);
exports.getAbsoluteUrlFactory = getAbsoluteUrlFactory;