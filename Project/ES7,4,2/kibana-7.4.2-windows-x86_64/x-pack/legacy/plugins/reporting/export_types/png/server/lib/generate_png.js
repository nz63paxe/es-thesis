"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generatePngObservableFactory = void 0;

var Rx = _interopRequireWildcard(require("rxjs"));

var _operators = require("rxjs/operators");

var _once_per_server = require("../../../../server/lib/once_per_server");

var _screenshots = require("../../../common/lib/screenshots");

var _preserve_layout = require("../../../common/layouts/preserve_layout");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function generatePngObservableFn(server) {
  const screenshotsObservable = (0, _screenshots.screenshotsObservableFactory)(server);
  const captureConcurrency = 1; // prettier-ignore

  const createPngWithScreenshots = async ({
    urlScreenshots
  }) => {
    if (urlScreenshots.length !== 1) {
      throw new Error(`Expected there to be 1 URL screenshot, but there are ${urlScreenshots.length}`);
    }

    if (urlScreenshots[0].screenshots.length !== 1) {
      throw new Error(`Expected there to be 1 screenshot, but there are ${urlScreenshots[0].screenshots.length}`);
    }

    return urlScreenshots[0].screenshots[0].base64EncodedData;
  };

  return function generatePngObservable(logger, url, browserTimezone, conditionalHeaders, layoutParams) {
    if (!layoutParams || !layoutParams.dimensions) {
      throw new Error(`LayoutParams.Dimensions is undefined.`);
    }

    const layout = new _preserve_layout.PreserveLayout(layoutParams.dimensions);
    const screenshots$ = Rx.of(url).pipe((0, _operators.mergeMap)(iUrl => screenshotsObservable({
      logger,
      url: iUrl,
      conditionalHeaders,
      layout,
      browserTimezone
    }), (jUrl, screenshot) => screenshot, captureConcurrency));
    return screenshots$.pipe((0, _operators.toArray)(), (0, _operators.mergeMap)(urlScreenshots => createPngWithScreenshots({
      urlScreenshots
    })));
  };
}

const generatePngObservableFactory = (0, _once_per_server.oncePerServer)(generatePngObservableFn);
exports.generatePngObservableFactory = generatePngObservableFactory;