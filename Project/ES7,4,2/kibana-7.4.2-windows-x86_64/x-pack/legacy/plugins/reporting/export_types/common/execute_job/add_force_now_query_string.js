"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addForceNowQuerystring = void 0;

var _url = _interopRequireDefault(require("url"));

var _get_absolute_url = require("../../../common/get_absolute_url");

var _validate_urls = require("../../../common/validate_urls");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getSavedObjectAbsoluteUrl(job, relativeUrl, server) {
  const getAbsoluteUrl = (0, _get_absolute_url.getAbsoluteUrlFactory)(server);

  const {
    pathname: path,
    hash,
    search
  } = _url.default.parse(relativeUrl);

  return getAbsoluteUrl({
    basePath: job.basePath,
    path,
    hash,
    search
  });
}

const addForceNowQuerystring = async ({
  job,
  conditionalHeaders,
  logo,
  server
}) => {
  // if no URLS then its from PNG which should only have one so put it in the array and process as PDF does
  if (!job.urls) {
    if (!job.relativeUrl) {
      throw new Error(`Unable to generate report. Url is not defined.`);
    }

    (0, _validate_urls.validateUrls)([job.relativeUrl]);
    job.urls = [getSavedObjectAbsoluteUrl(job, job.relativeUrl, server)];
  }

  const urls = job.urls.map(jobUrl => {
    if (!job.forceNow) {
      return jobUrl;
    }

    const parsed = _url.default.parse(jobUrl, true);

    const hash = _url.default.parse(parsed.hash.replace(/^#/, ''), true);

    const transformedHash = _url.default.format({
      pathname: hash.pathname,
      query: { ...hash.query,
        forceNow: job.forceNow
      }
    });

    return _url.default.format({ ...parsed,
      hash: transformedHash
    });
  });
  return {
    job,
    conditionalHeaders,
    logo,
    urls,
    server
  };
};

exports.addForceNowQuerystring = addForceNowQuerystring;