"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.injectCustomCss = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _util = require("util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const fsp = {
  readFile: (0, _util.promisify)(_fs.default.readFile)
};

const injectCustomCss = async (browser, layout, logger) => {
  logger.debug('injecting custom css');
  const filePath = layout.getCssOverridesPath();
  const buffer = await fsp.readFile(filePath);
  await browser.evaluate({
    fn: css => {
      const node = document.createElement('style');
      node.type = 'text/css';
      node.innerHTML = css; // eslint-disable-line no-unsanitized/property

      document.getElementsByTagName('head')[0].appendChild(node);
    },
    args: [buffer.toString()]
  });
};

exports.injectCustomCss = injectCustomCss;