"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.decryptJobHeaders = void 0;

var _crypto = require("../../../server/lib/crypto");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
const decryptJobHeaders = async ({
  job,
  server
}) => {
  const crypto = (0, _crypto.cryptoFactory)(server);
  const decryptedHeaders = await crypto.decrypt(job.headers);
  return {
    job,
    decryptedHeaders,
    server
  };
};

exports.decryptJobHeaders = decryptJobHeaders;