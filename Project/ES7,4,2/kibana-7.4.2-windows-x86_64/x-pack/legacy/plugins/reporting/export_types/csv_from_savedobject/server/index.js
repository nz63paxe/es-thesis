"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.register = register;

var _constants = require("../../../common/constants");

var _metadata = require("../metadata");

var _create_job = require("./create_job");

var _execute_job = require("./execute_job");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function register(registry) {
  registry.register({ ..._metadata.metadata,
    jobType: _constants.CSV_FROM_SAVEDOBJECT_JOB_TYPE,
    jobContentExtension: 'csv',
    createJobFactory: _create_job.createJobFactory,
    executeJobFactory: _execute_job.executeJobFactory,
    validLicenses: ['trial', 'basic', 'standard', 'gold', 'platinum']
  });
}