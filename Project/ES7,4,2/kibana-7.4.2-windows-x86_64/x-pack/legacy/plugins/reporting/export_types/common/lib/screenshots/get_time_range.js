"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTimeRange = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getTimeRange = async (browser, layout, logger) => {
  logger.debug('getting timeRange');
  const timeRange = await browser.evaluate({
    fn: (fromAttribute, toAttribute) => {
      const fromElement = document.querySelector(`[${fromAttribute}]`);
      const toElement = document.querySelector(`[${toAttribute}]`);

      if (!fromElement || !toElement) {
        return null;
      }

      const from = fromElement.getAttribute(fromAttribute);
      const to = toElement.getAttribute(toAttribute);

      if (!to || !from) {
        return null;
      }

      return {
        from,
        to
      };
    },
    args: [layout.selectors.timefilterFromAttribute, layout.selectors.timefilterToAttribute]
  });

  if (timeRange) {
    logger.debug(`timeRange from ${timeRange.from} to ${timeRange.to}`);
  } else {
    logger.debug('no timeRange');
  }

  return timeRange;
};

exports.getTimeRange = getTimeRange;