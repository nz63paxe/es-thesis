"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.openUrl = void 0;

var _constants = require("../../constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const openUrl = async (browser, url, conditionalHeaders, logger) => {
  await browser.open(url, {
    conditionalHeaders,
    waitForSelector: _constants.WAITFOR_SELECTOR
  }, logger);
};

exports.openUrl = openUrl;