"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.waitForElementsToBeInDOM = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const waitForElementsToBeInDOM = async (browser, itemsCount, layout, logger) => {
  logger.debug(`waiting for ${itemsCount} rendered elements to be in the DOM`);
  await browser.waitFor({
    fn: selector => {
      return document.querySelectorAll(selector).length;
    },
    args: [layout.selectors.renderComplete],
    toEqual: itemsCount
  });
  logger.info(`found ${itemsCount} rendered elements in the DOM`);
  return itemsCount;
};

exports.waitForElementsToBeInDOM = waitForElementsToBeInDOM;