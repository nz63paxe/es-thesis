"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getElementPositionAndAttributes = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getElementPositionAndAttributes = async (browser, layout) => {
  const elementsPositionAndAttributes = await browser.evaluate({
    fn: (selector, attributes) => {
      const elements = document.querySelectorAll(selector); // NodeList isn't an array, just an iterator, unable to use .map/.forEach

      const results = [];

      for (let i = 0; i < elements.length; i++) {
        const element = elements[i];
        const boundingClientRect = element.getBoundingClientRect();
        results.push({
          position: {
            boundingClientRect: {
              // modern browsers support x/y, but older ones don't
              top: boundingClientRect.y || boundingClientRect.top,
              left: boundingClientRect.x || boundingClientRect.left,
              width: boundingClientRect.width,
              height: boundingClientRect.height
            },
            scroll: {
              x: window.scrollX,
              y: window.scrollY
            }
          },
          attributes: Object.keys(attributes).reduce((result, key) => {
            const attribute = attributes[key];
            result[key] = element.getAttribute(attribute);
            return result;
          }, {})
        });
      }

      return results;
    },
    args: [layout.selectors.screenshot, {
      title: 'data-title',
      description: 'data-description'
    }]
  });
  return elementsPositionAndAttributes;
};

exports.getElementPositionAndAttributes = getElementPositionAndAttributes;