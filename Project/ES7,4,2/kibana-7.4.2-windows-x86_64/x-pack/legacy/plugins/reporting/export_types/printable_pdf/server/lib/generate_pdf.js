"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generatePdfObservableFactory = void 0;

var Rx = _interopRequireWildcard(require("rxjs"));

var _operators = require("rxjs/operators");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _lodash = require("lodash");

var _pdf = require("./pdf");

var _once_per_server = require("../../../../server/lib/once_per_server");

var _screenshots = require("../../../common/lib/screenshots");

var _layouts = require("../../../common/layouts");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped module
const getTimeRange = urlScreenshots => {
  const grouped = (0, _lodash.groupBy)(urlScreenshots.map(u => u.timeRange));
  const values = Object.values(grouped);

  if (values.length === 1) {
    return values[0][0];
  }

  return null;
};

const formatDate = (date, timezone) => {
  return _momentTimezone.default.tz(date, timezone).format('llll');
};

function generatePdfObservableFn(server) {
  const screenshotsObservable = (0, _screenshots.screenshotsObservableFactory)(server);
  const captureConcurrency = 1;
  return function generatePdfObservable(logger, title, urls, browserTimezone, conditionalHeaders, layoutParams, logo) {
    const layout = (0, _layouts.createLayout)(server, layoutParams);
    const screenshots$ = Rx.from(urls).pipe((0, _operators.mergeMap)(url => screenshotsObservable({
      logger,
      url,
      conditionalHeaders,
      layout,
      browserTimezone
    }), captureConcurrency));
    return screenshots$.pipe((0, _operators.toArray)(), (0, _operators.mergeMap)(async urlScreenshots => {
      const pdfOutput = _pdf.pdf.create(layout, logo);

      if (title) {
        const timeRange = getTimeRange(urlScreenshots);
        title += timeRange ? ` — ${formatDate(timeRange.from, browserTimezone)} to ${formatDate(timeRange.to, browserTimezone)}` : '';
        pdfOutput.setTitle(title);
      }

      urlScreenshots.forEach(({
        screenshots
      }) => {
        screenshots.forEach(screenshot => {
          pdfOutput.addImage(screenshot.base64EncodedData, {
            title: screenshot.title,
            description: screenshot.description
          });
        });
      });
      pdfOutput.generate();
      const buffer = await pdfOutput.getBuffer();
      return buffer;
    }));
  };
}

const generatePdfObservableFactory = (0, _once_per_server.oncePerServer)(generatePdfObservableFn);
exports.generatePdfObservableFactory = generatePdfObservableFactory;