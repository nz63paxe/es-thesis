"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.screenshotsObservableFactory = screenshotsObservableFactory;

var Rx = _interopRequireWildcard(require("rxjs"));

var _operators = require("rxjs/operators");

var _check_for_toast = require("./check_for_toast");

var _inject_css = require("./inject_css");

var _open_url = require("./open_url");

var _wait_for_render = require("./wait_for_render");

var _get_number_of_items = require("./get_number_of_items");

var _wait_for_dom_elements = require("./wait_for_dom_elements");

var _get_time_range = require("./get_time_range");

var _get_element_position_data = require("./get_element_position_data");

var _get_screenshots = require("./get_screenshots");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function screenshotsObservableFactory(server) {
  const browserDriverFactory = server.plugins.reporting.browserDriverFactory; // prettier-ignore

  const config = server.config();
  const captureConfig = config.get('xpack.reporting.capture');
  return function screenshotsObservable({
    logger,
    url,
    conditionalHeaders,
    layout,
    browserTimezone
  }) {
    const create$ = browserDriverFactory.create({
      viewport: layout.getBrowserViewport(),
      browserTimezone
    });
    return create$.pipe((0, _operators.mergeMap)(({
      driver$,
      exit$,
      message$,
      consoleMessage$
    }) => {
      message$.subscribe(line => {
        logger.debug(line, ['browser']);
      });
      consoleMessage$.subscribe(line => {
        logger.debug(line, ['browserConsole']);
      });
      const screenshot$ = driver$.pipe((0, _operators.mergeMap)(browser => (0, _open_url.openUrl)(browser, url, conditionalHeaders, logger), browser => browser), (0, _operators.mergeMap)(browser => {
        logger.debug('waiting for elements or items count attribute; or not found to interrupt'); // the dashboard is using the `itemsCountAttribute` attribute to let us
        // know how many items to expect since gridster incrementally adds panels
        // we have to use this hint to wait for all of them

        const renderSuccess = browser.waitForSelector(`${layout.selectors.renderComplete},[${layout.selectors.itemsCountAttribute}]`, {}, logger);
        const renderError = (0, _check_for_toast.checkForToastMessage)(browser, layout, logger);
        return Rx.race(Rx.from(renderSuccess), Rx.from(renderError));
      }, browser => browser), (0, _operators.mergeMap)(browser => (0, _get_number_of_items.getNumberOfItems)(browser, layout, logger), (browser, itemsCount) => ({
        browser,
        itemsCount
      })), (0, _operators.mergeMap)(async ({
        browser,
        itemsCount
      }) => {
        logger.debug('setting viewport');
        const viewport = layout.getViewport(itemsCount);
        return await browser.setViewport(viewport, logger);
      }, ({
        browser,
        itemsCount
      }) => ({
        browser,
        itemsCount
      })), (0, _operators.mergeMap)(({
        browser,
        itemsCount
      }) => (0, _wait_for_dom_elements.waitForElementsToBeInDOM)(browser, itemsCount, layout, logger), ({
        browser,
        itemsCount
      }) => ({
        browser,
        itemsCount
      })), (0, _operators.mergeMap)(({
        browser
      }) => {
        // Waiting till _after_ elements have rendered before injecting our CSS
        // allows for them to be displayed properly in many cases
        return (0, _inject_css.injectCustomCss)(browser, layout, logger);
      }, ({
        browser
      }) => ({
        browser
      })), (0, _operators.mergeMap)(async ({
        browser
      }) => {
        if (layout.positionElements) {
          // position panel elements for print layout
          return await layout.positionElements(browser, logger);
        }
      }, ({
        browser
      }) => browser), (0, _operators.mergeMap)(browser => {
        return (0, _wait_for_render.waitForRenderComplete)(captureConfig, browser, layout, logger);
      }, browser => browser), (0, _operators.mergeMap)(browser => (0, _get_time_range.getTimeRange)(browser, layout, logger), (browser, timeRange) => ({
        browser,
        timeRange
      })), (0, _operators.mergeMap)(({
        browser
      }) => (0, _get_element_position_data.getElementPositionAndAttributes)(browser, layout), ({
        browser,
        timeRange
      }, elementsPositionAndAttributes) => ({
        browser,
        timeRange,
        elementsPositionAndAttributes
      })), (0, _operators.mergeMap)(({
        browser,
        elementsPositionAndAttributes
      }) => {
        return (0, _get_screenshots.getScreenshots)({
          browser,
          elementsPositionAndAttributes,
          logger
        });
      }, ({
        timeRange
      }, screenshots) => ({
        timeRange,
        screenshots
      })));
      return Rx.race(screenshot$, exit$);
    }), (0, _operators.first)());
  };
}