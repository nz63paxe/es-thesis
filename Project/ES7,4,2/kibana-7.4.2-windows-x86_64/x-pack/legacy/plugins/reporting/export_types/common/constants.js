"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WAITFOR_SELECTOR = exports.LayoutTypes = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const LayoutTypes = {
  PRESERVE_LAYOUT: 'preserve_layout',
  PRINT: 'print'
};
exports.LayoutTypes = LayoutTypes;
const WAITFOR_SELECTOR = '.application';
exports.WAITFOR_SELECTOR = WAITFOR_SELECTOR;