"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkForToastMessage = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const checkForToastMessage = async (browser, layout, logger) => {
  return await browser.waitForSelector(layout.selectors.toastHeader, {
    silent: true
  }, logger).then(async () => {
    // Check for a toast message on the page. If there is one, capture the
    // message and throw an error, to fail the screenshot.
    const toastHeaderText = await browser.evaluate({
      fn: selector => {
        const nodeList = document.querySelectorAll(selector);
        return nodeList.item(0).innerText;
      },
      args: [layout.selectors.toastHeader]
    }); // Log an error to track the event in kibana server logs

    logger.error(_i18n.i18n.translate('xpack.reporting.exportTypes.printablePdf.screenshots.unexpectedErrorMessage', {
      defaultMessage: 'Encountered an unexpected message on the page: {toastHeaderText}',
      values: {
        toastHeaderText
      }
    })); // Throw an error to fail the screenshot job with a message

    throw new Error(_i18n.i18n.translate('xpack.reporting.exportTypes.printablePdf.screenshots.unexpectedErrorMessage', {
      defaultMessage: 'Encountered an unexpected message on the page: {toastHeaderText}',
      values: {
        toastHeaderText
      }
    }));
  });
};

exports.checkForToastMessage = checkForToastMessage;