"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getScreenshots = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getAsyncDurationLogger = logger => {
  return async (description, promise) => {
    const start = Date.now();
    const result = await promise;
    logger.debug(`${description} took ${Date.now() - start}ms`);
    return result;
  };
};

const getScreenshots = async ({
  browser,
  elementsPositionAndAttributes,
  logger
}) => {
  logger.info(`taking screenshots`);
  const asyncDurationLogger = getAsyncDurationLogger(logger);
  const screenshots = [];

  for (let i = 0; i < elementsPositionAndAttributes.length; i++) {
    const item = elementsPositionAndAttributes[i];
    const base64EncodedData = await asyncDurationLogger(`screenshot #${i + 1}`, browser.screenshot(item.position));
    screenshots.push({
      base64EncodedData,
      title: item.attributes.title,
      description: item.attributes.description
    });
  }

  logger.info(`screenshots taken: ${screenshots.length}`);
  return screenshots;
};

exports.getScreenshots = getScreenshots;