"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jobQueueClient = void 0;

var _kfetch = require("ui/kfetch");

var _system_api = require("ui/system_api");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var API_BASE_URL = '/api/reporting/jobs';

var JobQueueClient =
/*#__PURE__*/
function () {
  function JobQueueClient() {
    _classCallCheck(this, JobQueueClient);

    _defineProperty(this, "list", function () {
      var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var jobIds = arguments.length > 1 ? arguments[1] : undefined;
      var query = {
        page: page
      };

      if (jobIds && jobIds.length > 0) {
        // Only getting the first 10, to prevent URL overflows
        query.ids = jobIds.slice(0, 10).join(',');
      }

      return (0, _kfetch.kfetch)({
        method: 'GET',
        pathname: "".concat(API_BASE_URL, "/list"),
        query: query,
        headers: (0, _system_api.addSystemApiHeader)({})
      });
    });
  }

  _createClass(JobQueueClient, [{
    key: "total",
    value: function total() {
      return (0, _kfetch.kfetch)({
        method: 'GET',
        pathname: "".concat(API_BASE_URL, "/count"),
        headers: (0, _system_api.addSystemApiHeader)({})
      });
    }
  }, {
    key: "getContent",
    value: function getContent(jobId) {
      return (0, _kfetch.kfetch)({
        method: 'GET',
        pathname: "".concat(API_BASE_URL, "/output/").concat(jobId),
        headers: (0, _system_api.addSystemApiHeader)({})
      });
    }
  }, {
    key: "getInfo",
    value: function getInfo(jobId) {
      return (0, _kfetch.kfetch)({
        method: 'GET',
        pathname: "".concat(API_BASE_URL, "/info/").concat(jobId),
        headers: (0, _system_api.addSystemApiHeader)({})
      });
    }
  }]);

  return JobQueueClient;
}();

var jobQueueClient = new JobQueueClient();
exports.jobQueueClient = jobQueueClient;