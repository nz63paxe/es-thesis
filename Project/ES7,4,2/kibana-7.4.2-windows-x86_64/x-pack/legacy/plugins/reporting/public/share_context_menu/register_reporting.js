"use strict";

var _i18n = require("@kbn/i18n");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _xpack_info = require("plugins/xpack_main/services/xpack_info");

var _react = _interopRequireDefault(require("react"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _share_action_registry = require("ui/share/share_action_registry");

var _state_hashing = require("ui/state_management/state_hashing");

var _screen_capture_panel_content = require("../components/screen_capture_panel_content");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function reportingProvider(dashboardConfig) {
  var getShareActions = function getShareActions(_ref) {
    var objectType = _ref.objectType,
        objectId = _ref.objectId,
        getUnhashableStates = _ref.getUnhashableStates,
        sharingData = _ref.sharingData,
        isDirty = _ref.isDirty,
        onClose = _ref.onClose;

    if (!['dashboard', 'visualization'].includes(objectType)) {
      return [];
    } // Dashboard only mode does not currently support reporting
    // https://github.com/elastic/kibana/issues/18286


    if (objectType === 'dashboard' && dashboardConfig.getHideWriteControls()) {
      return [];
    }

    var getReportingJobParams = function getReportingJobParams() {
      // Replace hashes with original RISON values.
      var unhashedUrl = (0, _state_hashing.unhashUrl)(window.location.href, getUnhashableStates());
      var relativeUrl = unhashedUrl.replace(window.location.origin + _chrome.default.getBasePath(), '');
      var browserTimezone = _chrome.default.getUiSettingsClient().get('dateFormat:tz') === 'Browser' ? _momentTimezone.default.tz.guess() : _chrome.default.getUiSettingsClient().get('dateFormat:tz');
      return _objectSpread({}, sharingData, {
        objectType: objectType,
        browserTimezone: browserTimezone,
        relativeUrls: [relativeUrl]
      });
    };

    var getPngJobParams = function getPngJobParams() {
      // Replace hashes with original RISON values.
      var unhashedUrl = (0, _state_hashing.unhashUrl)(window.location.href, getUnhashableStates());
      var relativeUrl = unhashedUrl.replace(window.location.origin + _chrome.default.getBasePath(), '');
      var browserTimezone = _chrome.default.getUiSettingsClient().get('dateFormat:tz') === 'Browser' ? _momentTimezone.default.tz.guess() : _chrome.default.getUiSettingsClient().get('dateFormat:tz');
      return _objectSpread({}, sharingData, {
        objectType: objectType,
        browserTimezone: browserTimezone,
        relativeUrl: relativeUrl
      });
    };

    var shareActions = [];

    if (_xpack_info.xpackInfo.get('features.reporting.printablePdf.showLinks', false)) {
      var _shareMenuItem;

      var panelTitle = _i18n.i18n.translate('xpack.reporting.shareContextMenu.pdfReportsButtonLabel', {
        defaultMessage: 'PDF Reports'
      });

      shareActions.push({
        shareMenuItem: (_shareMenuItem = {
          name: panelTitle,
          icon: 'document',
          toolTipContent: _xpack_info.xpackInfo.get('features.reporting.printablePdf.message'),
          disabled: !_xpack_info.xpackInfo.get('features.reporting.printablePdf.enableLinks', false) ? true : false
        }, _defineProperty(_shareMenuItem, 'data-test-subj', 'pdfReportMenuItem'), _defineProperty(_shareMenuItem, "sortOrder", 10), _shareMenuItem),
        panel: {
          title: panelTitle,
          content: _react.default.createElement(_screen_capture_panel_content.ScreenCapturePanelContent, {
            reportType: "printablePdf",
            objectType: objectType,
            objectId: objectId,
            getJobParams: getReportingJobParams,
            isDirty: isDirty,
            onClose: onClose
          })
        }
      });
    }

    if (_xpack_info.xpackInfo.get('features.reporting.png.showLinks', false)) {
      var _shareMenuItem2;

      var _panelTitle = 'PNG Reports';
      shareActions.push({
        shareMenuItem: (_shareMenuItem2 = {
          name: _panelTitle,
          icon: 'document',
          toolTipContent: _xpack_info.xpackInfo.get('features.reporting.png.message'),
          disabled: !_xpack_info.xpackInfo.get('features.reporting.png.enableLinks', false) ? true : false
        }, _defineProperty(_shareMenuItem2, 'data-test-subj', 'pngReportMenuItem'), _defineProperty(_shareMenuItem2, "sortOrder", 10), _shareMenuItem2),
        panel: {
          title: _panelTitle,
          content: _react.default.createElement(_screen_capture_panel_content.ScreenCapturePanelContent, {
            reportType: "png",
            objectType: objectType,
            objectId: objectId,
            getJobParams: getPngJobParams,
            isDirty: isDirty,
            onClose: onClose
          })
        }
      });
    }

    return shareActions;
  };

  return {
    id: 'screenCaptureReports',
    getShareActions: getShareActions
  };
}

_share_action_registry.ShareContextMenuExtensionsRegistryProvider.register(reportingProvider);