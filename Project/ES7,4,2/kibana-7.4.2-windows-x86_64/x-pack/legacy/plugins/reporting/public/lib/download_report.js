"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.downloadReport = downloadReport;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function downloadReport(jobId) {
  var apiBaseUrl = _chrome.default.addBasePath(_constants.API_BASE_URL);

  var downloadLink = "".concat(apiBaseUrl, "/jobs/download/").concat(jobId);
  window.open(downloadLink);
}