"use strict";

var _react = _interopRequireDefault(require("react"));

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _i18n = require("@kbn/i18n");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _kfetch = require("ui/kfetch");

var _notify = require("ui/notify");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _eui = require("@elastic/eui");

var _public = require("../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public");

var _legacy = require("../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public/legacy");

var _embeddable = require("../../../../../../src/legacy/core_plugins/kibana/public/discover/embeddable");

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var API_BASE_URL = "".concat(_constants.API_BASE_URL_V1, "/generate/immediate/csv/saved-object");
var CSV_REPORTING_ACTION = 'downloadCsvReport';

function isSavedSearchEmbeddable(embeddable) {
  return embeddable.type === _embeddable.SEARCH_EMBEDDABLE_TYPE;
}

var GetCsvReportPanelAction =
/*#__PURE__*/
function (_Action) {
  _inherits(GetCsvReportPanelAction, _Action);

  function GetCsvReportPanelAction() {
    var _this;

    _classCallCheck(this, GetCsvReportPanelAction);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(GetCsvReportPanelAction).call(this, CSV_REPORTING_ACTION));

    _defineProperty(_assertThisInitialized(_this), "isDownloading", void 0);

    _defineProperty(_assertThisInitialized(_this), "type", CSV_REPORTING_ACTION);

    _defineProperty(_assertThisInitialized(_this), "isCompatible",
    /*#__PURE__*/
    function () {
      var _ref = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(context) {
        var enablePanelActionDownload, embeddable;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                enablePanelActionDownload = _chrome.default.getInjected('enablePanelActionDownload');

                if (enablePanelActionDownload) {
                  _context.next = 3;
                  break;
                }

                return _context.abrupt("return", false);

              case 3:
                embeddable = context.embeddable;
                return _context.abrupt("return", embeddable.getInput().viewMode !== _public.ViewMode.EDIT && embeddable.type === 'search');

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      return function (_x) {
        return _ref.apply(this, arguments);
      };
    }());

    _defineProperty(_assertThisInitialized(_this), "execute",
    /*#__PURE__*/
    function () {
      var _ref2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(context) {
        var embeddable, _embeddable$getInput, _embeddable$getInput$, to, from, searchEmbeddable, searchRequestBody, state, kibanaTimezone, id, filename, timezone, fromTime, toTime, body;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                embeddable = context.embeddable;

                if (isSavedSearchEmbeddable(embeddable)) {
                  _context2.next = 3;
                  break;
                }

                throw new _public.IncompatibleActionError();

              case 3:
                if (!_this.isDownloading) {
                  _context2.next = 5;
                  break;
                }

                return _context2.abrupt("return");

              case 5:
                _embeddable$getInput = embeddable.getInput(), _embeddable$getInput$ = _embeddable$getInput.timeRange, to = _embeddable$getInput$.to, from = _embeddable$getInput$.from;
                searchEmbeddable = embeddable;
                _context2.next = 9;
                return _this.getSearchRequestBody({
                  searchEmbeddable: searchEmbeddable
                });

              case 9:
                searchRequestBody = _context2.sent;
                state = _.pick(searchRequestBody, ['sort', 'docvalue_fields', 'query']);
                kibanaTimezone = _chrome.default.getUiSettingsClient().get('dateFormat:tz');
                id = "search:".concat(embeddable.getSavedSearch().id);
                filename = embeddable.getTitle();
                timezone = kibanaTimezone === 'Browser' ? _momentTimezone.default.tz.guess() : kibanaTimezone;
                fromTime = _datemath.default.parse(from);
                toTime = _datemath.default.parse(to);

                if (!(!fromTime || !toTime)) {
                  _context2.next = 19;
                  break;
                }

                return _context2.abrupt("return", _this.onGenerationFail(new Error("Invalid time range: From: ".concat(fromTime, ", To: ").concat(toTime))));

              case 19:
                body = JSON.stringify({
                  timerange: {
                    min: fromTime.format(),
                    max: toTime.format(),
                    timezone: timezone
                  },
                  state: state
                });
                _this.isDownloading = true;

                _notify.toastNotifications.addSuccess({
                  title: _i18n.i18n.translate('xpack.reporting.dashboard.csvDownloadStartedTitle', {
                    defaultMessage: "CSV Download Started"
                  }),
                  text: _i18n.i18n.translate('xpack.reporting.dashboard.csvDownloadStartedMessage', {
                    defaultMessage: "Your CSV will download momentarily."
                  }),
                  'data-test-subj': 'csvDownloadStarted'
                });

                _context2.next = 24;
                return (0, _kfetch.kfetch)({
                  method: 'POST',
                  pathname: "".concat(API_BASE_URL, "/").concat(id),
                  body: body
                }).then(function (rawResponse) {
                  _this.isDownloading = false;
                  var download = "".concat(filename, ".csv");
                  var blob = new Blob([rawResponse], {
                    type: 'text/csv;charset=utf-8;'
                  }); // Hack for IE11 Support

                  if (window.navigator.msSaveOrOpenBlob) {
                    return window.navigator.msSaveOrOpenBlob(blob, download);
                  }

                  var a = window.document.createElement('a');
                  var downloadObject = window.URL.createObjectURL(blob);
                  a.href = downloadObject;
                  a.download = download;
                  document.body.appendChild(a);
                  a.click();
                  window.URL.revokeObjectURL(downloadObject);
                  document.body.removeChild(a);
                }).catch(_this.onGenerationFail.bind(_assertThisInitialized(_this)));

              case 24:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x2) {
        return _ref2.apply(this, arguments);
      };
    }());

    _this.isDownloading = false;
    return _this;
  }

  _createClass(GetCsvReportPanelAction, [{
    key: "getIcon",
    value: function getIcon() {
      return _react.default.createElement(_eui.EuiIcon, {
        type: "document"
      });
    }
  }, {
    key: "getDisplayName",
    value: function getDisplayName() {
      return _i18n.i18n.translate('xpack.reporting.dashboard.downloadCsvPanelTitle', {
        defaultMessage: 'Download CSV'
      });
    }
  }, {
    key: "getSearchRequestBody",
    value: function () {
      var _getSearchRequestBody = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(_ref3) {
        var searchEmbeddable, adapters;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                searchEmbeddable = _ref3.searchEmbeddable;
                adapters = searchEmbeddable.getInspectorAdapters();

                if (adapters) {
                  _context3.next = 4;
                  break;
                }

                return _context3.abrupt("return", {});

              case 4:
                if (!(adapters.requests.requests.length === 0)) {
                  _context3.next = 6;
                  break;
                }

                return _context3.abrupt("return", {});

              case 6:
                return _context3.abrupt("return", searchEmbeddable.getSavedSearch().searchSource.getSearchRequestBody());

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getSearchRequestBody(_x3) {
        return _getSearchRequestBody.apply(this, arguments);
      }

      return getSearchRequestBody;
    }()
  }, {
    key: "onGenerationFail",
    value: function onGenerationFail(error) {
      this.isDownloading = false;

      _notify.toastNotifications.addDanger({
        title: _i18n.i18n.translate('xpack.reporting.dashboard.failedCsvDownloadTitle', {
          defaultMessage: "CSV download failed"
        }),
        text: _i18n.i18n.translate('xpack.reporting.dashboard.failedCsvDownloadMessage', {
          defaultMessage: "We couldn't generate your CSV at this time."
        }),
        'data-test-subj': 'downloadCsvFail'
      });
    }
  }]);

  return GetCsvReportPanelAction;
}(_public.Action);

var action = new GetCsvReportPanelAction();

_legacy.setup.registerAction(action);

_legacy.setup.attachAction(_public.CONTEXT_MENU_TRIGGER, action.id);