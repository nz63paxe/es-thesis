"use strict";

var _i18n = require("@kbn/i18n");

var _xpack_info = require("plugins/xpack_main/services/xpack_info");

var _react = _interopRequireDefault(require("react"));

var _share_action_registry = require("ui/share/share_action_registry");

var _reporting_panel_content = require("../components/reporting_panel_content");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function reportingProvider() {
  var getShareActions = function getShareActions(_ref) {
    var objectType = _ref.objectType,
        objectId = _ref.objectId,
        sharingData = _ref.sharingData,
        isDirty = _ref.isDirty,
        onClose = _ref.onClose;

    if ('search' !== objectType) {
      return [];
    }

    var getJobParams = function getJobParams() {
      return _objectSpread({}, sharingData, {
        type: objectType
      });
    };

    var shareActions = [];

    if (_xpack_info.xpackInfo.get('features.reporting.csv.showLinks', false)) {
      var panelTitle = _i18n.i18n.translate('xpack.reporting.shareContextMenu.csvReportsButtonLabel', {
        defaultMessage: 'CSV Reports'
      });

      shareActions.push({
        shareMenuItem: _defineProperty({
          name: panelTitle,
          icon: 'document',
          toolTipContent: _xpack_info.xpackInfo.get('features.reporting.csv.message'),
          disabled: !_xpack_info.xpackInfo.get('features.reporting.csv.enableLinks', false) ? true : false
        }, 'data-test-subj', 'csvReportMenuItem'),
        panel: {
          title: panelTitle,
          content: _react.default.createElement(_reporting_panel_content.ReportingPanelContent, {
            reportType: "csv",
            layoutId: undefined,
            objectType: objectType,
            objectId: objectId,
            getJobParams: getJobParams,
            isDirty: isDirty,
            onClose: onClose
          })
        }
      });
    }

    return shareActions;
  };

  return {
    id: 'csvReports',
    getShareActions: getShareActions
  };
}

_share_action_registry.ShareContextMenuExtensionsRegistryProvider.register(reportingProvider);