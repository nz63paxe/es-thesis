"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reportingClient = void 0;

var _kfetch = require("ui/kfetch");

var _risonNode = _interopRequireDefault(require("rison-node"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _query_string = require("ui/utils/query_string");

var _job_completion_notifications = require("./job_completion_notifications");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var API_BASE_URL = '/api/reporting/generate';

var ReportingClient = function ReportingClient() {
  _classCallCheck(this, ReportingClient);

  _defineProperty(this, "getReportingJobPath", function (exportType, jobParams) {
    return "".concat(_chrome.default.addBasePath(API_BASE_URL), "/").concat(exportType, "?").concat(_query_string.QueryString.param('jobParams', _risonNode.default.encode(jobParams)));
  });

  _defineProperty(this, "createReportingJob",
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(exportType, jobParams) {
      var query, resp;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              query = {
                jobParams: _risonNode.default.encode(jobParams)
              };
              _context.next = 3;
              return (0, _kfetch.kfetch)({
                method: 'POST',
                pathname: "".concat(API_BASE_URL, "/").concat(exportType),
                query: query
              });

            case 3:
              resp = _context.sent;

              _job_completion_notifications.jobCompletionNotifications.add(resp.job.id);

              return _context.abrupt("return", resp);

            case 6:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
};

var reportingClient = new ReportingClient();
exports.reportingClient = reportingClient;