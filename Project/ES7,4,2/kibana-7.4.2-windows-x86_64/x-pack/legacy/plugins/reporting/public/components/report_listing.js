"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReportListing = void 0;

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

var _lodash = require("lodash");

var _react2 = _interopRequireWildcard(require("react"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _notify = require("ui/notify");

var _eui = require("@elastic/eui");

var _poller = require("../../../../common/poller");

var _job_statuses = require("../constants/job_statuses");

var _download_report = require("../lib/download_report");

var _job_queue_client = require("../lib/job_queue_client");

var _report_error_button = require("./report_error_button");

var _report_info_button = require("./report_info_button");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var jobStatusLabelsMap = new Map([[_job_statuses.JobStatuses.PENDING, _i18n.i18n.translate('xpack.reporting.jobStatuses.pendingText', {
  defaultMessage: 'Pending'
})], [_job_statuses.JobStatuses.PROCESSING, _i18n.i18n.translate('xpack.reporting.jobStatuses.processingText', {
  defaultMessage: 'Processing'
})], [_job_statuses.JobStatuses.COMPLETED, _i18n.i18n.translate('xpack.reporting.jobStatuses.completedText', {
  defaultMessage: 'Completed'
})], [_job_statuses.JobStatuses.FAILED, _i18n.i18n.translate('xpack.reporting.jobStatuses.failedText', {
  defaultMessage: 'Failed'
})], [_job_statuses.JobStatuses.CANCELLED, _i18n.i18n.translate('xpack.reporting.jobStatuses.cancelledText', {
  defaultMessage: 'Cancelled'
})]]);

var ReportListingUi =
/*#__PURE__*/
function (_Component) {
  _inherits(ReportListingUi, _Component);

  function ReportListingUi(props) {
    var _this;

    _classCallCheck(this, ReportListingUi);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ReportListingUi).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "mounted", void 0);

    _defineProperty(_assertThisInitialized(_this), "poller", void 0);

    _defineProperty(_assertThisInitialized(_this), "isInitialJobsFetch", void 0);

    _defineProperty(_assertThisInitialized(_this), "renderDownloadButton", function (record) {
      if (record.status !== _job_statuses.JobStatuses.COMPLETED) {
        return;
      }

      var intl = _this.props.intl;

      var button = _react2.default.createElement(_eui.EuiButtonIcon, {
        onClick: function onClick() {
          return (0, _download_report.downloadReport)(record.id);
        },
        iconType: "importAction",
        "aria-label": intl.formatMessage({
          id: 'xpack.reporting.listing.table.downloadReportAriaLabel',
          defaultMessage: 'Download report'
        })
      });

      if (record.csv_contains_formulas) {
        return _react2.default.createElement(_eui.EuiToolTip, {
          position: "top",
          content: intl.formatMessage({
            id: 'xpack.reporting.listing.table.csvContainsFormulas',
            defaultMessage: 'Your CSV contains characters which spreadsheet applications can interpret as formulas.'
          })
        }, button);
      }

      if (record.max_size_reached) {
        return _react2.default.createElement(_eui.EuiToolTip, {
          position: "top",
          content: intl.formatMessage({
            id: 'xpack.reporting.listing.table.maxSizeReachedTooltip',
            defaultMessage: 'Max size reached, contains partial data.'
          })
        }, button);
      }

      return button;
    });

    _defineProperty(_assertThisInitialized(_this), "renderReportErrorButton", function (record) {
      if (record.status !== _job_statuses.JobStatuses.FAILED) {
        return;
      }

      return _react2.default.createElement(_report_error_button.ReportErrorButton, {
        jobId: record.id
      });
    });

    _defineProperty(_assertThisInitialized(_this), "renderInfoButton", function (record) {
      return _react2.default.createElement(_report_info_button.ReportInfoButton, {
        jobId: record.id
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTableChange", function (_ref) {
      var page = _ref.page;
      var pageIndex = page.index;

      _this.setState(function () {
        return {
          page: pageIndex
        };
      }, _this.fetchJobs);
    });

    _defineProperty(_assertThisInitialized(_this), "fetchJobs",
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var jobs, total;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // avoid page flicker when poller is updating table - only display loading screen on first load
              if (_this.isInitialJobsFetch) {
                _this.setState(function () {
                  return {
                    isLoading: true
                  };
                });
              }

              _context.prev = 1;
              _context.next = 4;
              return _job_queue_client.jobQueueClient.list(_this.state.page);

            case 4:
              jobs = _context.sent;
              _context.next = 7;
              return _job_queue_client.jobQueueClient.total();

            case 7:
              total = _context.sent;
              _this.isInitialJobsFetch = false;
              _context.next = 20;
              break;

            case 11:
              _context.prev = 11;
              _context.t0 = _context["catch"](1);

              if (_this.licenseAllowsToShowThisPage()) {
                _context.next = 17;
                break;
              }

              _notify.toastNotifications.addDanger(_this.props.badLicenseMessage);

              _this.props.redirect('/management');

              return _context.abrupt("return");

            case 17:
              if (_context.t0.res.status !== 401 && _context.t0.res.status !== 403) {
                _notify.toastNotifications.addDanger(_context.t0.res.statusText || _this.props.intl.formatMessage({
                  id: 'xpack.reporting.listing.table.requestFailedErrorMessage',
                  defaultMessage: 'Request failed'
                }));
              }

              if (_this.mounted) {
                _this.setState(function () {
                  return {
                    isLoading: false,
                    jobs: [],
                    total: 0
                  };
                });
              }

              return _context.abrupt("return");

            case 20:
              if (_this.mounted) {
                _this.setState(function () {
                  return {
                    isLoading: false,
                    total: total,
                    jobs: jobs.map(function (job) {
                      var source = job._source;
                      return {
                        id: job._id,
                        type: source.jobtype,
                        object_type: source.payload.type,
                        object_title: source.payload.title,
                        created_by: source.created_by,
                        created_at: source.created_at,
                        started_at: source.started_at,
                        completed_at: source.completed_at,
                        status: source.status,
                        statusLabel: jobStatusLabelsMap.get(source.status) || source.status,
                        max_size_reached: source.output ? source.output.max_size_reached : false,
                        attempts: source.attempts,
                        max_attempts: source.max_attempts,
                        csv_contains_formulas: (0, _lodash.get)(source, 'output.csv_contains_formulas')
                      };
                    })
                  };
                });
              }

            case 21:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[1, 11]]);
    })));

    _defineProperty(_assertThisInitialized(_this), "licenseAllowsToShowThisPage", function () {
      return _this.props.showLinks && _this.props.enableLinks;
    });

    _this.state = {
      page: 0,
      total: 0,
      jobs: [],
      isLoading: false
    };
    _this.isInitialJobsFetch = true;
    return _this;
  }

  _createClass(ReportListingUi, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(_eui.EuiPageContent, {
        horizontalPosition: "center",
        className: "euiPageBody--restrictWidth-default"
      }, _react2.default.createElement(_eui.EuiTitle, null, _react2.default.createElement("h1", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.reporting.listing.reportstitle",
        defaultMessage: "Reports"
      }))), _react2.default.createElement(_eui.EuiText, {
        color: "subdued",
        size: "s"
      }, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.reporting.listing.reports.subtitle",
        defaultMessage: "Find reports generated in Kibana applications here"
      }))), _react2.default.createElement(_eui.EuiSpacer, null), this.renderTable());
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.mounted = false;
      this.poller.stop();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.mounted = true;

      var _chrome$getInjected = _chrome.default.getInjected('reportingPollConfig'),
          jobsRefresh = _chrome$getInjected.jobsRefresh;

      this.poller = new _poller.Poller({
        functionToPoll: function functionToPoll() {
          return _this2.fetchJobs();
        },
        pollFrequencyInMillis: jobsRefresh.interval,
        trailing: false,
        continuePollingOnError: true,
        pollFrequencyErrorMultiplier: jobsRefresh.intervalErrorMultiplier
      });
      this.poller.start();
    }
  }, {
    key: "renderTable",
    value: function renderTable() {
      var _this3 = this;

      var intl = this.props.intl;
      var tableColumns = [{
        field: 'object_title',
        name: intl.formatMessage({
          id: 'xpack.reporting.listing.tableColumns.reportTitle',
          defaultMessage: 'Report'
        }),
        render: function render(objectTitle, record) {
          return _react2.default.createElement("div", null, _react2.default.createElement("div", null, objectTitle), _react2.default.createElement(_eui.EuiText, {
            size: "s"
          }, _react2.default.createElement(_eui.EuiTextColor, {
            color: "subdued"
          }, record.object_type)));
        }
      }, {
        field: 'created_at',
        name: intl.formatMessage({
          id: 'xpack.reporting.listing.tableColumns.createdAtTitle',
          defaultMessage: 'Created at'
        }),
        render: function render(createdAt, record) {
          if (record.created_by) {
            return _react2.default.createElement("div", null, _react2.default.createElement("div", null, _this3.formatDate(createdAt)), _react2.default.createElement("span", null, record.created_by));
          }

          return _this3.formatDate(createdAt);
        }
      }, {
        field: 'status',
        name: intl.formatMessage({
          id: 'xpack.reporting.listing.tableColumns.statusTitle',
          defaultMessage: 'Status'
        }),
        render: function render(status, record) {
          if (status === 'pending') {
            return _react2.default.createElement("div", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.reporting.listing.tableValue.createdAtDetail.pendingStatusReachedText",
              defaultMessage: "Pending - waiting for job to be processed"
            }));
          }

          var maxSizeReached;

          if (record.max_size_reached) {
            maxSizeReached = _react2.default.createElement("span", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.reporting.listing.tableValue.createdAtDetail.maxSizeReachedText",
              defaultMessage: " - Max size reached"
            }));
          }

          var statusTimestamp;

          if (status === _job_statuses.JobStatuses.PROCESSING && record.started_at) {
            statusTimestamp = _this3.formatDate(record.started_at);
          } else if (record.completed_at && (status === _job_statuses.JobStatuses.COMPLETED || status === _job_statuses.JobStatuses.FAILED)) {
            statusTimestamp = _this3.formatDate(record.completed_at);
          }

          var statusLabel = jobStatusLabelsMap.get(status) || status;

          if (status === _job_statuses.JobStatuses.PROCESSING) {
            statusLabel = statusLabel + " (attempt ".concat(record.attempts, " of ").concat(record.max_attempts, ")");
          }

          if (statusTimestamp) {
            return _react2.default.createElement("div", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.reporting.listing.tableValue.createdAtDetail.statusTimestampText",
              defaultMessage: "{statusLabel} at {statusTimestamp}",
              values: {
                statusLabel: statusLabel,
                statusTimestamp: _react2.default.createElement("span", {
                  className: "eui-textNoWrap"
                }, statusTimestamp)
              }
            }), maxSizeReached);
          } // unknown status


          return _react2.default.createElement("div", null, statusLabel, maxSizeReached);
        }
      }, {
        name: intl.formatMessage({
          id: 'xpack.reporting.listing.tableColumns.actionsTitle',
          defaultMessage: 'Actions'
        }),
        actions: [{
          render: function render(record) {
            return _react2.default.createElement("div", null, _this3.renderDownloadButton(record), _this3.renderReportErrorButton(record), _this3.renderInfoButton(record));
          }
        }]
      }];
      var pagination = {
        pageIndex: this.state.page,
        pageSize: 10,
        totalItemCount: this.state.total,
        hidePerPageOptions: true
      };
      return _react2.default.createElement(_eui.EuiBasicTable, {
        itemId: 'id',
        items: this.state.jobs,
        loading: this.state.isLoading,
        columns: tableColumns,
        noItemsMessage: this.state.isLoading ? intl.formatMessage({
          id: 'xpack.reporting.listing.table.loadingReportsDescription',
          defaultMessage: 'Loading reports'
        }) : intl.formatMessage({
          id: 'xpack.reporting.listing.table.noCreatedReportsDescription',
          defaultMessage: 'No reports have been created'
        }),
        pagination: pagination,
        onChange: this.onTableChange,
        "data-test-subj": "reportJobListing"
      });
    }
  }, {
    key: "formatDate",
    value: function formatDate(timestamp) {
      try {
        return (0, _moment.default)(timestamp).format('YYYY-MM-DD @ hh:mm A');
      } catch (error) {
        // ignore parse error and display unformatted value
        return timestamp;
      }
    }
  }]);

  return ReportListingUi;
}(_react2.Component);

var ReportListing = (0, _react.injectI18n)(ReportListingUi);
exports.ReportListing = ReportListing;