"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _task_manager = require("./task_manager");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Plugin {
  constructor(initializerContext) {
    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "taskManager", void 0);

    this.logger = initializerContext.logger.get();
  } // TODO: Make asynchronous like new platform


  setup(core, {
    config,
    serializer,
    elasticsearch,
    savedObjects
  }) {
    const {
      callWithInternalUser
    } = elasticsearch.getCluster('admin');
    const savedObjectsRepository = savedObjects.getSavedObjectsRepository(callWithInternalUser, ['task']);
    const taskManager = new _task_manager.TaskManager({
      config,
      savedObjectsRepository,
      serializer,
      callWithInternalUser,
      logger: this.logger
    });
    this.taskManager = taskManager;
    return {
      fetch: (...args) => taskManager.fetch(...args),
      remove: (...args) => taskManager.remove(...args),
      schedule: (...args) => taskManager.schedule(...args),
      addMiddleware: (...args) => taskManager.addMiddleware(...args),
      registerTaskDefinitions: (...args) => taskManager.registerTaskDefinitions(...args)
    };
  }

  start() {
    if (this.taskManager) {
      this.taskManager.start();
    }
  }

  stop() {
    if (this.taskManager) {
      this.taskManager.stop();
    }
  }

}

exports.Plugin = Plugin;