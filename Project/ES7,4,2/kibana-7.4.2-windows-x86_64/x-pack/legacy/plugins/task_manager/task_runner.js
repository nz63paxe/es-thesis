"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskManagerRunner = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _intervals = require("./lib/intervals");

var _task = require("./task");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const defaultBackoffPerFailure = 5 * 60 * 1000;

/**
 * Runs a background task, ensures that errors are properly handled,
 * allows for cancellation.
 *
 * @export
 * @class TaskManagerRunner
 * @implements {TaskRunner}
 */
class TaskManagerRunner {
  /**
   * Creates an instance of TaskManagerRunner.
   * @param {Opts} opts
   * @prop {Logger} logger - The task manager logger
   * @prop {TaskDefinition} definition - The definition of the task being run
   * @prop {ConcreteTaskInstance} instance - The record describing this particular task instance
   * @prop {Updatable} store - The store used to read / write tasks instance info
   * @prop {BeforeRunFunction} beforeRun - A function that adjusts the run context prior to running the task
   * @memberof TaskManagerRunner
   */
  constructor(opts) {
    _defineProperty(this, "task", void 0);

    _defineProperty(this, "instance", void 0);

    _defineProperty(this, "definitions", void 0);

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "store", void 0);

    _defineProperty(this, "beforeRun", void 0);

    this.instance = sanitizeInstance(opts.instance);
    this.definitions = opts.definitions;
    this.logger = opts.logger;
    this.store = opts.store;
    this.beforeRun = opts.beforeRun;
  }
  /**
   * Gets how many workers are occupied by this task instance.
   * Per Joi validation logic, this will return a number >= 1
   */


  get numWorkers() {
    return this.definition.numWorkers;
  }
  /**
   * Gets the id of this task instance.
   */


  get id() {
    return this.instance.id;
  }
  /**
   * Gets the task type of this task instance.
   */


  get taskType() {
    return this.instance.taskType;
  }
  /**
   * Gets the task defintion from the dictionary.
   */


  get definition() {
    return this.definitions[this.taskType];
  }
  /**
   * Gets whether or not this task has run longer than its expiration setting allows.
   */


  get isExpired() {
    return (0, _intervals.intervalFromDate)(this.instance.startedAt, this.definition.timeout) < new Date();
  }
  /**
   * Returns a log-friendly representation of this task.
   */


  toString() {
    return `${this.taskType} "${this.id}"`;
  }
  /**
   * Runs the task, handling the task result, errors, etc, rescheduling if need
   * be. NOTE: the time of applying the middleware's beforeRun is incorporated
   * into the total timeout time the task in configured with. We may decide to
   * start the timer after beforeRun resolves
   *
   * @returns {Promise<RunResult>}
   */


  async run() {
    this.logger.debug(`Running task ${this}`);
    const modifiedContext = await this.beforeRun({
      taskInstance: this.instance
    });

    try {
      this.task = this.definition.createTaskRunner(modifiedContext);
      const result = await this.task.run();
      const validatedResult = this.validateResult(result);
      return this.processResult(validatedResult);
    } catch (err) {
      this.logger.error(`Task ${this} failed: ${err}`); // in error scenario, we can not get the RunResult
      // re-use modifiedContext's state, which is correct as of beforeRun

      return this.processResult({
        error: err,
        state: modifiedContext.taskInstance.state
      });
    }
  }
  /**
   * Attempts to claim exclusive rights to run the task. If the attempt fails
   * with a 409 (http conflict), we assume another Kibana instance beat us to the punch.
   *
   * @returns {Promise<boolean>}
   */


  async claimOwnership() {
    const VERSION_CONFLICT_STATUS = 409;
    const attempts = this.instance.attempts + 1;
    const now = new Date();

    try {
      this.instance = await this.store.update({ ...this.instance,
        status: 'running',
        startedAt: now,
        attempts,
        retryAt: this.instance.interval ? (0, _intervals.intervalFromNow)(this.definition.timeout) : this.getRetryDelay({
          attempts,
          // Fake an error. This allows retry logic when tasks keep timing out
          // and lets us set a proper "retryAt" value each time.
          error: new Error('Task timeout'),
          addDuration: this.definition.timeout
        })
      });
      return true;
    } catch (error) {
      if (error.statusCode !== VERSION_CONFLICT_STATUS) {
        throw error;
      }
    }

    return false;
  }
  /**
   * Attempts to cancel the task.
   *
   * @returns {Promise<void>}
   */


  async cancel() {
    const {
      task
    } = this;

    if (task && task.cancel) {
      this.task = undefined;
      return task.cancel();
    }

    this.logger.warn(`The task ${this} is not cancellable.`);
  }

  validateResult(result) {
    const {
      error
    } = _joi.default.validate(result, _task.validateRunResult);

    if (error) {
      this.logger.warn(`Invalid task result for ${this}: ${error.message}`);
    }

    return result || {
      state: {}
    };
  }

  async processResultForRecurringTask(result) {
    // recurring task: update the task instance
    const startedAt = this.instance.startedAt;
    const state = result.state || this.instance.state || {};
    let status = this.getInstanceStatus();
    let runAt;

    if (status === 'failed') {
      // task run errored, keep the same runAt
      runAt = this.instance.runAt;
    } else if (result.runAt) {
      runAt = result.runAt;
    } else if (result.error) {
      // when result.error is truthy, then we're retrying because it failed
      const newRunAt = this.instance.interval ? (0, _intervals.intervalFromDate)(startedAt, this.instance.interval) : this.getRetryDelay({
        attempts: this.instance.attempts,
        error: result.error
      });

      if (!newRunAt) {
        status = 'failed';
        runAt = this.instance.runAt;
      } else {
        runAt = newRunAt;
      }
    } else {
      runAt = (0, _intervals.intervalFromDate)(startedAt, this.instance.interval);
    }

    await this.store.update({ ...this.instance,
      runAt,
      state,
      status,
      startedAt: null,
      retryAt: null,
      attempts: result.error ? this.instance.attempts : 0
    });
    return result;
  }

  async processResultWhenDone(result) {
    // not a recurring task: clean up by removing the task instance from store
    try {
      await this.store.remove(this.instance.id);
    } catch (err) {
      if (err.statusCode === 404) {
        this.logger.warn(`Task cleanup of ${this} failed in processing. Was remove called twice?`);
      } else {
        throw err;
      }
    }

    return result;
  }

  async processResult(result) {
    if (result.runAt || this.instance.interval || result.error) {
      await this.processResultForRecurringTask(result);
    } else {
      await this.processResultWhenDone(result);
    }

    return result;
  }

  getInstanceStatus() {
    if (this.instance.interval) {
      return 'idle';
    }

    const maxAttempts = this.definition.maxAttempts || this.store.maxAttempts;
    return this.instance.attempts < maxAttempts ? 'idle' : 'failed';
  }

  getRetryDelay({
    error,
    attempts,
    addDuration
  }) {
    let result = null; // Use custom retry logic, if any, otherwise we'll use the default logic

    const retry = this.definition.getRetry ? this.definition.getRetry(attempts, error) : true;

    if (retry instanceof Date) {
      result = retry;
    } else if (retry === true) {
      result = new Date(Date.now() + attempts * defaultBackoffPerFailure);
    } // Add a duration to the result


    if (addDuration && result) {
      result = (0, _intervals.intervalFromDate)(result, addDuration);
    }

    return result;
  }

}

exports.TaskManagerRunner = TaskManagerRunner;

function sanitizeInstance(instance) {
  return { ...instance,
    params: instance.params || {},
    state: instance.state || {}
  };
}