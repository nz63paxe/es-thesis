"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskPoller = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * This module contains the logic for polling the task manager index for new work.
 */

/**
 * Performs work on a scheduled interval, logging any errors. This waits for work to complete
 * (or error) prior to attempting another run.
 */
class TaskPoller {
  /**
   * Constructs a new TaskPoller.
   *
   * @param opts
   * @prop {number} pollInterval - How often, in milliseconds, we will run the work function
   * @prop {Logger} logger - The task manager logger
   * @prop {WorkFn} work - An empty, asynchronous function that performs the desired work
   */
  constructor(opts) {
    _defineProperty(this, "isStarted", false);

    _defineProperty(this, "isWorking", false);

    _defineProperty(this, "timeout", void 0);

    _defineProperty(this, "pollInterval", void 0);

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "work", void 0);

    this.pollInterval = opts.pollInterval;
    this.logger = opts.logger;
    this.work = opts.work;
  }
  /**
   * Starts the poller. If the poller is already running, this has no effect.
   */


  async start() {
    if (this.isStarted) {
      return;
    }

    this.isStarted = true;

    const poll = async () => {
      await this.attemptWork();

      if (this.isStarted) {
        this.timeout = setTimeout(poll, this.pollInterval);
      }
    };

    poll();
  }
  /**
   * Stops the poller.
   */


  stop() {
    this.isStarted = false;
    clearTimeout(this.timeout);
    this.timeout = undefined;
  }
  /**
   * Runs the work function. If the work function is currently running,
   * this has no effect.
   */


  async attemptWork() {
    if (!this.isStarted || this.isWorking) {
      return;
    }

    this.isWorking = true;

    try {
      await this.work();
    } catch (err) {
      this.logger.error(`Failed to poll for work: ${err}`);
    } finally {
      this.isWorking = false;
    }
  }

}

exports.TaskPoller = TaskPoller;