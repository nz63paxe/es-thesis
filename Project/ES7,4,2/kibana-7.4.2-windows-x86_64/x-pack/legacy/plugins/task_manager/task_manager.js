"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskManager = void 0;

var _fill_pool = require("./lib/fill_pool");

var _middleware = require("./lib/middleware");

var _sanitize_task_definitions = require("./lib/sanitize_task_definitions");

var _task_poller = require("./task_poller");

var _task_pool = require("./task_pool");

var _task_runner = require("./task_runner");

var _task_store = require("./task_store");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * The TaskManager is the public interface into the task manager system. This glues together
 * all of the disparate modules in one integration point. The task manager operates in two different ways:
 *
 * - pre-init, it allows middleware registration, but disallows task manipulation
 * - post-init, it disallows middleware registration, but allows task manipulation
 *
 * Due to its complexity, this is mostly tested by integration tests (see readme).
 */

/**
 * The public interface into the task manager system.
 */
class TaskManager {
  /**
   * Initializes the task manager, preventing any further addition of middleware,
   * enabling the task manipulation methods, and beginning the background polling
   * mechanism.
   */
  constructor(opts) {
    _defineProperty(this, "isStarted", false);

    _defineProperty(this, "maxWorkers", void 0);

    _defineProperty(this, "overrideNumWorkers", void 0);

    _defineProperty(this, "pollerInterval", void 0);

    _defineProperty(this, "definitions", void 0);

    _defineProperty(this, "store", void 0);

    _defineProperty(this, "poller", void 0);

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "pool", void 0);

    _defineProperty(this, "startQueue", []);

    _defineProperty(this, "middleware", {
      beforeSave: async saveOpts => saveOpts,
      beforeRun: async runOpts => runOpts
    });

    this.maxWorkers = opts.config.get('xpack.task_manager.max_workers');
    this.overrideNumWorkers = opts.config.get('xpack.task_manager.override_num_workers');
    this.pollerInterval = opts.config.get('xpack.task_manager.poll_interval');
    this.definitions = {};
    this.logger = opts.logger;
    /* Kibana UUID needs to be pulled live (not cached), as it takes a long time
     * to initialize, and can change after startup */

    const store = new _task_store.TaskStore({
      serializer: opts.serializer,
      savedObjectsRepository: opts.savedObjectsRepository,
      callCluster: opts.callWithInternalUser,
      index: opts.config.get('xpack.task_manager.index'),
      maxAttempts: opts.config.get('xpack.task_manager.max_attempts'),
      definitions: this.definitions
    });
    const pool = new _task_pool.TaskPool({
      logger: this.logger,
      maxWorkers: this.maxWorkers
    });

    const createRunner = instance => new _task_runner.TaskManagerRunner({
      logger: this.logger,
      instance,
      store,
      definitions: this.definitions,
      beforeRun: this.middleware.beforeRun
    });

    const poller = new _task_poller.TaskPoller({
      logger: this.logger,
      pollInterval: opts.config.get('xpack.task_manager.poll_interval'),

      work() {
        return (0, _fill_pool.fillPool)(pool.run, store.fetchAvailableTasks, createRunner);
      }

    });
    this.pool = pool;
    this.store = store;
    this.poller = poller;
  }
  /**
   * Starts up the task manager and starts picking up tasks.
   */


  start() {
    this.isStarted = true; // Some calls are waiting until task manager is started

    this.startQueue.forEach(fn => fn());
    this.startQueue = [];

    const startPoller = async () => {
      try {
        await this.poller.start();
      } catch (err) {
        // FIXME: check the type of error to make sure it's actually an ES error
        this.logger.warn(`PollError ${err.message}`); // rety again to initialize store and poller, using the timing of
        // task_manager's configurable poll interval

        const retryInterval = this.pollerInterval;
        setTimeout(() => startPoller(), retryInterval);
      }
    };

    startPoller();
  }

  async waitUntilStarted() {
    if (!this.isStarted) {
      await new Promise(resolve => {
        this.startQueue.push(resolve);
      });
    }
  }
  /**
   * Stops the task manager and cancels running tasks.
   */


  stop() {
    this.poller.stop();
    this.pool.cancelRunningTasks();
  }
  /**
   * Method for allowing consumers to register task definitions into the system.
   * @param taskDefinitions - The Kibana task definitions dictionary
   */


  registerTaskDefinitions(taskDefinitions) {
    this.assertUninitialized('register task definitions');
    const duplicate = Object.keys(taskDefinitions).find(k => !!this.definitions[k]);

    if (duplicate) {
      throw new Error(`Task ${duplicate} is already defined!`);
    }

    try {
      const sanitized = (0, _sanitize_task_definitions.sanitizeTaskDefinitions)(taskDefinitions, this.maxWorkers, this.overrideNumWorkers);
      Object.assign(this.definitions, sanitized);
    } catch (e) {
      this.logger.error('Could not sanitize task definitions');
    }
  }
  /**
   * Adds middleware to the task manager, such as adding security layers, loggers, etc.
   *
   * @param {Middleware} middleware - The middlware being added.
   */


  addMiddleware(middleware) {
    this.assertUninitialized('add middleware');
    const prevMiddleWare = this.middleware;
    this.middleware = (0, _middleware.addMiddlewareToChain)(prevMiddleWare, middleware);
  }
  /**
   * Schedules a task.
   *
   * @param task - The task being scheduled.
   * @returns {Promise<ConcreteTaskInstance>}
   */


  async schedule(taskInstance, options) {
    await this.waitUntilStarted();
    const {
      taskInstance: modifiedTask
    } = await this.middleware.beforeSave({ ...options,
      taskInstance
    });
    const result = await this.store.schedule(modifiedTask);
    this.poller.attemptWork();
    return result;
  }
  /**
   * Fetches a paginatable list of scheduled tasks.
   *
   * @param opts - The query options used to filter tasks
   * @returns {Promise<FetchResult>}
   */


  async fetch(opts) {
    await this.waitUntilStarted();
    return this.store.fetch(opts);
  }
  /**
   * Removes the specified task from the index.
   *
   * @param {string} id
   * @returns {Promise<RemoveResult>}
   */


  async remove(id) {
    await this.waitUntilStarted();
    return this.store.remove(id);
  }
  /**
   * Ensures task manager IS NOT already initialized
   *
   * @param {string} message shown if task manager is already initialized
   * @returns void
   */


  assertUninitialized(message) {
    if (this.isStarted) {
      throw new Error(`Cannot ${message} after the task manager is initialized!`);
    }
  }

}

exports.TaskManager = TaskManager;