"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mockLogger = mockLogger;
exports.resolvable = resolvable;
exports.sleep = sleep;

var _sinon = _interopRequireDefault(require("sinon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * A handful of helper functions for testing the task manager.
 */
// Caching this here to avoid setTimeout mocking affecting our tests.
const nativeTimeout = setTimeout;
/**
 * Creates a mock task manager Logger.
 */

function mockLogger() {
  return {
    info: _sinon.default.stub(),
    debug: _sinon.default.stub(),
    warn: _sinon.default.stub(),
    error: _sinon.default.stub()
  };
}

/**
 * Creates a promise which can be resolved externally, useful for
 * coordinating async tests.
 */
function resolvable() {
  let resolve;
  const result = new Promise(r => resolve = r);

  result.resolve = () => nativeTimeout(resolve, 0);

  return result;
}
/**
 * A simple helper for waiting a specified number of milliseconds.
 *
 * @param {number} ms
 */


async function sleep(ms) {
  return new Promise(r => nativeTimeout(r, ms));
}