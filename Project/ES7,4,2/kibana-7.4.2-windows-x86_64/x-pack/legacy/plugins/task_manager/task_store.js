"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskStore = void 0;

var _lodash = require("lodash");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Wraps an elasticsearch connection and provides a task manager-specific
 * interface into the index.
 */
class TaskStore {
  /**
   * Constructs a new TaskStore.
   * @param {StoreOpts} opts
   * @prop {CallCluster} callCluster - The elastic search connection
   * @prop {string} index - The name of the task manager index
   * @prop {number} maxAttempts - The maximum number of attempts before a task will be abandoned
   * @prop {TaskDefinition} definition - The definition of the task being run
   * @prop {serializer} - The saved object serializer
   * @prop {savedObjectsRepository} - An instance to the saved objects repository
   */
  constructor(opts) {
    _defineProperty(this, "maxAttempts", void 0);

    _defineProperty(this, "index", void 0);

    _defineProperty(this, "callCluster", void 0);

    _defineProperty(this, "definitions", void 0);

    _defineProperty(this, "savedObjectsRepository", void 0);

    _defineProperty(this, "serializer", void 0);

    this.callCluster = opts.callCluster;
    this.index = opts.index;
    this.maxAttempts = opts.maxAttempts;
    this.definitions = opts.definitions;
    this.serializer = opts.serializer;
    this.savedObjectsRepository = opts.savedObjectsRepository;
    this.fetchAvailableTasks = this.fetchAvailableTasks.bind(this);
  }
  /**
   * Schedules a task.
   *
   * @param task - The task being scheduled.
   */


  async schedule(taskInstance) {
    if (!this.definitions[taskInstance.taskType]) {
      throw new Error(`Unsupported task type "${taskInstance.taskType}". Supported types are ${Object.keys(this.definitions).join(', ')}`);
    }

    const savedObject = await this.savedObjectsRepository.create('task', taskInstanceToAttributes(taskInstance), {
      id: taskInstance.id
    });
    return savedObjectToConcreteTaskInstance(savedObject);
  }
  /**
   * Fetches a paginatable list of scheduled tasks.
   *
   * @param opts - The query options used to filter tasks
   */


  async fetch(opts = {}) {
    const sort = paginatableSort(opts.sort);
    return this.search({
      sort,
      search_after: opts.searchAfter,
      query: opts.query
    });
  }
  /**
   * Fetches tasks from the index, which are ready to be run.
   * - runAt is now or past
   * - id is not currently running in this instance of Kibana
   * - has a type that is in our task definitions
   *
   * @param {TaskQuery} query
   * @prop {string[]} types - Task types to be queried
   * @prop {number} size - The number of task instances to retrieve
   * @returns {Promise<ConcreteTaskInstance[]>}
   */


  async fetchAvailableTasks() {
    const {
      docs
    } = await this.search({
      query: {
        bool: {
          must: [// Either a task with idle status and runAt <= now or
          // status running with a retryAt <= now.
          {
            bool: {
              should: [{
                bool: {
                  must: [{
                    term: {
                      'task.status': 'idle'
                    }
                  }, {
                    range: {
                      'task.runAt': {
                        lte: 'now'
                      }
                    }
                  }]
                }
              }, {
                bool: {
                  must: [{
                    term: {
                      'task.status': 'running'
                    }
                  }, {
                    range: {
                      'task.retryAt': {
                        lte: 'now'
                      }
                    }
                  }]
                }
              }]
            }
          }, // Either task has an interval or the attempts < the maximum configured
          {
            bool: {
              should: [{
                exists: {
                  field: 'task.interval'
                }
              }, ...Object.entries(this.definitions).map(([type, definition]) => ({
                bool: {
                  must: [{
                    term: {
                      'task.taskType': type
                    }
                  }, {
                    range: {
                      'task.attempts': {
                        lt: definition.maxAttempts || this.maxAttempts
                      }
                    }
                  }]
                }
              }))]
            }
          }]
        }
      },
      size: 10,
      sort: {
        _script: {
          type: 'number',
          order: 'asc',
          script: {
            lang: 'expression',
            source: `doc['task.retryAt'].value || doc['task.runAt'].value`
          }
        }
      },
      seq_no_primary_term: true
    });
    return docs;
  }
  /**
   * Updates the specified doc in the index, returning the doc
   * with its version up to date.
   *
   * @param {TaskDoc} doc
   * @returns {Promise<TaskDoc>}
   */


  async update(doc) {
    const updatedSavedObject = await this.savedObjectsRepository.update('task', doc.id, taskInstanceToAttributes(doc), {
      version: doc.version
    });
    return savedObjectToConcreteTaskInstance(updatedSavedObject);
  }
  /**
   * Removes the specified task from the index.
   *
   * @param {string} id
   * @returns {Promise<void>}
   */


  async remove(id) {
    await this.savedObjectsRepository.delete('task', id);
  }

  async search(opts = {}) {
    const originalQuery = opts.query;
    const queryOnlyTasks = {
      term: {
        type: 'task'
      }
    };
    const query = originalQuery ? {
      bool: {
        must: [queryOnlyTasks, originalQuery]
      }
    } : queryOnlyTasks;
    const result = await this.callCluster('search', {
      index: this.index,
      ignoreUnavailable: true,
      body: { ...opts,
        query
      }
    });
    const rawDocs = result.hits.hits;
    return {
      docs: rawDocs.map(doc => this.serializer.rawToSavedObject(doc)).map(doc => (0, _lodash.omit)(doc, 'namespace')).map(savedObjectToConcreteTaskInstance),
      searchAfter: rawDocs.length && rawDocs[rawDocs.length - 1].sort || []
    };
  }

}

exports.TaskStore = TaskStore;

function paginatableSort(sort = []) {
  const sortById = {
    _id: 'desc'
  };

  if (!sort.length) {
    return [{
      'task.runAt': 'asc'
    }, sortById];
  }

  if (sort.find(({
    _id
  }) => !!_id)) {
    return sort;
  }

  return [...sort, sortById];
}

function taskInstanceToAttributes(doc) {
  return { ...(0, _lodash.omit)(doc, 'id', 'version'),
    params: JSON.stringify(doc.params || {}),
    state: JSON.stringify(doc.state || {}),
    attempts: doc.attempts || 0,
    scheduledAt: (doc.scheduledAt || new Date()).toISOString(),
    startedAt: doc.startedAt && doc.startedAt.toISOString() || null,
    retryAt: doc.retryAt && doc.retryAt.toISOString() || null,
    runAt: (doc.runAt || new Date()).toISOString(),
    status: doc.status || 'idle'
  };
}

function savedObjectToConcreteTaskInstance(savedObject) {
  return { ...savedObject.attributes,
    id: savedObject.id,
    version: savedObject.version,
    scheduledAt: new Date(savedObject.attributes.scheduledAt),
    runAt: new Date(savedObject.attributes.runAt),
    startedAt: savedObject.attributes.startedAt && new Date(savedObject.attributes.startedAt),
    retryAt: savedObject.attributes.retryAt && new Date(savedObject.attributes.retryAt),
    state: parseJSONField(savedObject.attributes.state, 'state', savedObject.id),
    params: parseJSONField(savedObject.attributes.params, 'params', savedObject.id)
  };
}

function parseJSONField(json, fieldName, id) {
  try {
    return json ? JSON.parse(json) : {};
  } catch (error) {
    throw new Error(`Task "${id}"'s ${fieldName} field has invalid JSON: ${json}`);
  }
}