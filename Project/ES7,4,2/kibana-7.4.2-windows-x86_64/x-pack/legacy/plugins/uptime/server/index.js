"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "initServerWithKibana", {
  enumerable: true,
  get: function () {
    return _kibana.initServerWithKibana;
  }
});
Object.defineProperty(exports, "KibanaServer", {
  enumerable: true,
  get: function () {
    return _kibana.KibanaServer;
  }
});

var _kibana = require("./kibana.index");