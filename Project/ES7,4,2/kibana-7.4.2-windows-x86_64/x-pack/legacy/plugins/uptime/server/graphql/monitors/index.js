"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createMonitorsResolvers", {
  enumerable: true,
  get: function () {
    return _resolvers.createMonitorsResolvers;
  }
});
Object.defineProperty(exports, "monitorsSchema", {
  enumerable: true,
  get: function () {
    return _schema.monitorsSchema;
  }
});

var _resolvers = require("./resolvers");

var _schema = require("./schema.gql");