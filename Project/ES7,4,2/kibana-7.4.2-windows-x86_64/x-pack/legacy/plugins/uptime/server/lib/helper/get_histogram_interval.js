"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getHistogramInterval = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _constants = require("../../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getHistogramInterval = (dateRangeStart, dateRangeEnd, bucketCount) => {
  const from = _datemath.default.parse(dateRangeStart);

  const to = _datemath.default.parse(dateRangeEnd);

  if (from === undefined) {
    throw Error('Invalid dateRangeStart value');
  }

  if (to === undefined) {
    throw Error('Invalid dateRangeEnd value');
  }

  return `${Math.round((to.valueOf() - from.valueOf()) / (bucketCount || _constants.QUERY.DEFAULT_BUCKET_COUNT))}ms`;
};

exports.getHistogramInterval = getHistogramInterval;