"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMonitorStatesResolvers = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createMonitorStatesResolvers = libs => {
  return {
    Query: {
      async getMonitorStates(resolver, {
        dateRangeStart,
        dateRangeEnd,
        filters
      }, {
        req
      }) {
        const [// TODO: rely on new summaries adapter function once continuous data frame is available
        // summaries,
        totalSummaryCount, legacySummaries] = await Promise.all([// TODO: rely on new summaries adapter function once continuous data frame is available
        // libs.monitorStates.getMonitorStates(req, pageIndex, pageSize, sortField, sortDirection),
        libs.pings.getDocCount(req), libs.monitorStates.legacyGetMonitorStates(req, dateRangeStart, dateRangeEnd, filters)]);
        return {
          summaries: legacySummaries,
          totalSummaryCount
        };
      },

      async getStatesIndexStatus(resolver, {}, {
        req
      }) {
        return await libs.monitorStates.statesIndexExists(req);
      }

    }
  };
};

exports.createMonitorStatesResolvers = createMonitorStatesResolvers;