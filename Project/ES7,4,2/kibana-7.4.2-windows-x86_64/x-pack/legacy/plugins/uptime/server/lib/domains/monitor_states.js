"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMMonitorStatesDomain = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class UMMonitorStatesDomain {
  constructor(adapter, libs) {
    this.adapter = adapter;
    this.adapter = adapter;
  }

  async getMonitorStates(request, pageIndex, pageSize, sortField, sortDirection) {
    return this.adapter.getMonitorStates(request, pageIndex, pageSize, sortField, sortDirection);
  }

  async statesIndexExists(request) {
    return this.adapter.statesIndexExists(request);
  }

  async legacyGetMonitorStates(request, dateRangeStart, dateRangeEnd, filters) {
    return this.adapter.legacyGetMonitorStates(request, dateRangeStart, dateRangeEnd, filters);
  }

}

exports.UMMonitorStatesDomain = UMMonitorStatesDomain;