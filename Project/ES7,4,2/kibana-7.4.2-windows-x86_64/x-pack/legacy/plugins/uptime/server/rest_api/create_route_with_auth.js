"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createRouteWithAuth = void 0;

var _boom = _interopRequireDefault(require("boom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createRouteWithAuth = (libs, routeCreator) => {
  const restRoute = routeCreator(libs);
  const {
    handler,
    method,
    path,
    options
  } = restRoute;

  const authHandler = async (request, h) => {
    if (libs.auth.requestIsValid(request)) {
      return await handler(request, h);
    }

    return _boom.default.badRequest();
  };

  return {
    method,
    path,
    options,
    handler: authHandler
  };
};

exports.createRouteWithAuth = createRouteWithAuth;