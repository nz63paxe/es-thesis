"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElasticsearchPingsAdapter = void 0;

var _lodash = require("lodash");

var _moment = _interopRequireDefault(require("moment"));

var _constants = require("../../../../common/constants");

var _helper = require("../../helper");

var _get_histogram_interval = require("../../helper/get_histogram_interval");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ElasticsearchPingsAdapter {
  constructor(database) {
    _defineProperty(this, "database", void 0);

    this.database = database;
  }
  /**
   * Fetches ping documents from ES
   * @param request Kibana server request
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param monitorId optional limit by monitorId
   * @param status optional limit by check statuses
   * @param sort optional sort by timestamp
   * @param size optional limit query size
   */


  async getAll(request, dateRangeStart, dateRangeEnd, monitorId, status, sort = 'desc', size, location) {
    const sortParam = {
      sort: [{
        '@timestamp': {
          order: sort
        }
      }]
    };
    const sizeParam = size ? {
      size
    } : undefined;
    const filter = [{
      range: {
        '@timestamp': {
          gte: dateRangeStart,
          lte: dateRangeEnd
        }
      }
    }];

    if (monitorId) {
      filter.push({
        term: {
          'monitor.id': monitorId
        }
      });
    }

    if (status) {
      filter.push({
        term: {
          'monitor.status': status
        }
      });
    }

    let postFilterClause = {};

    if (location) {
      postFilterClause = {
        post_filter: {
          term: {
            'observer.geo.name': location
          }
        }
      };
    }

    const queryContext = {
      bool: {
        filter
      }
    };
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: { ...queryContext
        },
        ...sortParam,
        ...sizeParam,
        aggregations: {
          locations: {
            terms: {
              field: 'observer.geo.name',
              missing: 'N/A',
              size: 1000
            }
          }
        },
        ...postFilterClause
      }
    };
    const {
      hits: {
        hits,
        total
      },
      aggregations: aggs
    } = await this.database.search(request, params);
    const locations = (0, _lodash.get)(aggs, 'locations', {
      buckets: [{
        key: 'N/A',
        doc_count: 0
      }]
    });
    const pings = hits.map(({
      _id,
      _source
    }) => {
      const timestamp = _source['@timestamp']; // Calculate here the length of the content string in bytes, this is easier than in client JS, where
      // we don't have access to Buffer.byteLength. There are some hacky ways to do this in the
      // client but this is cleaner.

      const httpBody = (0, _lodash.get)(_source, 'http.response.body');

      if (httpBody && httpBody.content) {
        httpBody.content_bytes = Buffer.byteLength(httpBody.content);
      }

      return {
        id: _id,
        timestamp,
        ..._source
      };
    });
    const results = {
      total: total.value,
      locations: locations.buckets.map(bucket => bucket.key),
      pings
    };
    return results;
  }
  /**
   * Fetch data to populate monitor status bar.
   * @param request Kibana server request
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param monitorId optional limit to monitorId
   */


  async getLatestMonitorDocs(request, dateRangeStart, dateRangeEnd, monitorId, location) {
    // TODO: Write tests for this function
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: {
          bool: {
            filter: [{
              range: {
                '@timestamp': {
                  gte: dateRangeStart,
                  lte: dateRangeEnd
                }
              }
            }, ...(monitorId ? [{
              term: {
                'monitor.id': monitorId
              }
            }] : []), ...(location ? [{
              term: {
                'observer.geo.name': location
              }
            }] : [])]
          }
        },
        size: 0,
        aggs: {
          by_id: {
            terms: {
              field: 'monitor.id',
              size: 1000
            },
            aggs: {
              latest: {
                top_hits: {
                  size: 1,
                  sort: {
                    '@timestamp': {
                      order: 'desc'
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    const buckets = (0, _lodash.get)(result, 'aggregations.by_id.buckets', []); // @ts-ignore TODO fix destructuring implicit any

    return buckets.map(({
      latest: {
        hits: {
          hits
        }
      }
    }) => {
      const timestamp = hits[0]._source[`@timestamp`];
      const momentTs = (0, _moment.default)(timestamp);
      const millisFromNow = (0, _moment.default)().diff(momentTs);
      return { ...hits[0]._source,
        timestamp,
        millisFromNow
      };
    });
  }
  /**
   * Gets data used for a composite histogram for the currently-running monitors.
   * @param request Kibana server request
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param filters user-defined filters
   */


  async getPingHistogram(request, dateRangeStart, dateRangeEnd, filters, monitorId) {
    const {
      statusFilter,
      query
    } = (0, _helper.getFilteredQueryAndStatusFilter)(dateRangeStart, dateRangeEnd, filters);
    const combinedQuery = !monitorId ? query : {
      bool: {
        filter: [{
          match: {
            'monitor.id': monitorId
          }
        }, query]
      }
    };
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: combinedQuery,
        size: 0,
        aggs: {
          timeseries: {
            date_histogram: {
              field: '@timestamp',
              fixed_interval: (0, _get_histogram_interval.getHistogramInterval)(dateRangeStart, dateRangeEnd)
            },
            aggs: {
              down: {
                filter: {
                  term: {
                    'monitor.status': 'down'
                  }
                }
              },
              up: {
                filter: {
                  term: {
                    'monitor.status': 'up'
                  }
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    const buckets = (0, _lodash.get)(result, 'aggregations.timeseries.buckets', []);
    const mappedBuckets = buckets.map(bucket => {
      const key = (0, _lodash.get)(bucket, 'key');
      const downCount = (0, _lodash.get)(bucket, 'down.doc_count');
      const upCount = (0, _lodash.get)(bucket, 'up.doc_count');
      return {
        key,
        downCount: statusFilter && statusFilter !== 'down' ? 0 : downCount,
        upCount: statusFilter && statusFilter !== 'up' ? 0 : upCount,
        y: 1
      };
    });
    return (0, _helper.formatEsBucketsForHistogram)(mappedBuckets);
  }
  /**
   * Count the number of documents in heartbeat indices
   * @param request Kibana server request
   */


  async getDocCount(request) {
    const {
      count
    } = await this.database.count(request, {
      index: _constants.INDEX_NAMES.HEARTBEAT
    });
    return {
      count
    };
  }

}

exports.ElasticsearchPingsAdapter = ElasticsearchPingsAdapter;