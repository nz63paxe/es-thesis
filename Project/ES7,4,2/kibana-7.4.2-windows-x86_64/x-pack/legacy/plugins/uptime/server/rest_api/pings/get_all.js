"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createGetAllRoute = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createGetAllRoute = libs => ({
  method: 'GET',
  path: '/api/uptime/pings',
  options: {
    validate: {
      query: _joi.default.object({
        dateRangeStart: _joi.default.number().required(),
        dateRangeEnd: _joi.default.number().required(),
        monitorId: _joi.default.string(),
        size: _joi.default.number(),
        sort: _joi.default.string(),
        status: _joi.default.string()
      })
    },
    tags: ['access:uptime']
  },
  handler: async request => {
    const {
      size,
      sort,
      dateRangeStart,
      dateRangeEnd,
      monitorId,
      status
    } = request.query;
    return await libs.pings.getAll(request, dateRangeStart, dateRangeEnd, monitorId, status, sort, size);
  }
});

exports.createGetAllRoute = createGetAllRoute;