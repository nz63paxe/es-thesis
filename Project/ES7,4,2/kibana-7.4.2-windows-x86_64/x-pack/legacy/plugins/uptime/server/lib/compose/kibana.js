"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

var _auth = require("../adapters/auth");

var _kibana_database_adapter = require("../adapters/database/kibana_database_adapter");

var _framework = require("../adapters/framework");

var _monitors = require("../adapters/monitors");

var _pings = require("../adapters/pings");

var _domains = require("../domains");

var _monitor_states = require("../domains/monitor_states");

var _monitor_states2 = require("../adapters/monitor_states");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function compose(hapiServer) {
  const framework = new _framework.UMKibanaBackendFrameworkAdapter(hapiServer);
  const database = new _kibana_database_adapter.UMKibanaDatabaseAdapter(hapiServer.plugins.elasticsearch);
  const pingsDomain = new _domains.UMPingsDomain(new _pings.ElasticsearchPingsAdapter(database), {});
  const authDomain = new _domains.UMAuthDomain(new _auth.UMXPackAuthAdapter(hapiServer.plugins.xpack_main), {});
  const monitorsDomain = new _domains.UMMonitorsDomain(new _monitors.ElasticsearchMonitorsAdapter(database), {});
  const monitorStatesDomain = new _monitor_states.UMMonitorStatesDomain(new _monitor_states2.ElasticsearchMonitorStatesAdapter(database), {});
  const domainLibs = {
    auth: authDomain,
    monitors: monitorsDomain,
    monitorStates: monitorStatesDomain,
    pings: pingsDomain
  };
  const libs = {
    framework,
    database,
    ...domainLibs
  };
  return libs;
}