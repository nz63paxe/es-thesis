"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMXPackAuthAdapter = void 0;

var _constants = require("../../../../common/constants");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// look at index-management for guidance, subscribe to licensecheckerresultsgenerator
// then check the license status
class UMXPackAuthAdapter {
  constructor(xpack) {
    this.xpack = xpack;

    _defineProperty(this, "xpackLicenseStatus", void 0);

    _defineProperty(this, "getLicenseType", () => this.xpackLicenseStatus.licenseType || null);

    _defineProperty(this, "licenseIsActive", () => this.xpackLicenseStatus.isActive || false);

    _defineProperty(this, "registerLicenseCheck", () => this.xpack.info.feature(_constants.PLUGIN.ID).registerLicenseCheckResultsGenerator(this.updateLicenseInfo));

    _defineProperty(this, "updateLicenseInfo", xpackLicenseStatus => {
      this.xpackLicenseStatus = {
        isActive: xpackLicenseStatus.license.isActive(),
        licenseType: xpackLicenseStatus.license.getType()
      };
    });

    this.xpack = xpack;
    this.xpackLicenseStatus = {
      isActive: null,
      licenseType: null
    };
    this.xpack.status.once('green', this.registerLicenseCheck);
  }

}

exports.UMXPackAuthAdapter = UMXPackAuthAdapter;