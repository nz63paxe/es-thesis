"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaTelemetryAdapter = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// seconds in an hour
const BUCKET_SIZE = 3600; // take buckets in the last day

const BUCKET_NUMBER = 24;

class KibanaTelemetryAdapter {
  static initUsageCollector(server) {
    const {
      collectorSet
    } = server.usage;
    return collectorSet.makeUsageCollector({
      type: 'uptime',
      fetch: async () => {
        const report = this.getReport();
        return {
          last_24_hours: {
            hits: { ...report
            }
          }
        };
      },
      isReady: () => true
    });
  }

  static countOverview() {
    const bucket = this.getBucketToIncrement();
    this.collector[bucket].overview_page += 1;
  }

  static countMonitor() {
    const bucket = this.getBucketToIncrement();
    this.collector[bucket].monitor_page += 1;
  }

  static getReport() {
    const minBucket = this.getCollectorWindow();
    Object.keys(this.collector).map(key => parseInt(key, 10)).filter(key => key < minBucket).forEach(oldBucket => {
      delete this.collector[oldBucket];
    });
    return Object.values(this.collector).reduce((acc, cum) => ({
      overview_page: acc.overview_page + cum.overview_page,
      monitor_page: acc.monitor_page + cum.monitor_page
    }), {
      overview_page: 0,
      monitor_page: 0
    });
  }

  static getBucket() {
    const nowInSeconds = Math.round(Date.now() / 1000);
    return nowInSeconds - nowInSeconds % BUCKET_SIZE;
  }

  static getBucketToIncrement() {
    const bucketId = this.getBucket();

    if (!this.collector[bucketId]) {
      this.collector[bucketId] = {
        overview_page: 0,
        monitor_page: 0
      };
    }

    return bucketId;
  }

  static getCollectorWindow() {
    return this.getBucket() - BUCKET_SIZE * (BUCKET_NUMBER - 1);
  }

}

exports.KibanaTelemetryAdapter = KibanaTelemetryAdapter;

_defineProperty(KibanaTelemetryAdapter, "collector", {});