"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMKibanaDatabaseAdapter = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class UMKibanaDatabaseAdapter {
  constructor(kbnElasticsearch) {
    _defineProperty(this, "elasticsearch", void 0);

    this.elasticsearch = kbnElasticsearch.getCluster('data');
  }

  async search(request, params) {
    return this.elasticsearch.callWithRequest(request, 'search', params);
  }

  async count(request, params) {
    return this.elasticsearch.callWithRequest(request, 'count', params);
  }

  async head(request, params) {
    return this.elasticsearch.callWithRequest(request, 'indices.exists', params);
  }

}

exports.UMKibanaDatabaseAdapter = UMKibanaDatabaseAdapter;