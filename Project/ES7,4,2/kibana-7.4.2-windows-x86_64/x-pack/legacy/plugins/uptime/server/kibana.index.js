"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initServerWithKibana = void 0;

var _i18n = require("@kbn/i18n");

var _constants = require("../common/constants");

var _telemetry = require("./lib/adapters/telemetry");

var _kibana = require("./lib/compose/kibana");

var _uptime_server = require("./uptime_server");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const initServerWithKibana = server => {
  const libs = (0, _kibana.compose)(server);
  server.usage.collectorSet.register(_telemetry.KibanaTelemetryAdapter.initUsageCollector(server));
  (0, _uptime_server.initUptimeServer)(libs);
  const xpackMainPlugin = server.plugins.xpack_main;
  xpackMainPlugin.registerFeature({
    id: _constants.PLUGIN.ID,
    name: _i18n.i18n.translate('xpack.uptime.featureRegistry.uptimeFeatureName', {
      defaultMessage: 'Uptime'
    }),
    navLinkId: _constants.PLUGIN.ID,
    icon: 'uptimeApp',
    app: ['uptime', 'kibana'],
    catalogue: ['uptime'],
    privileges: {
      all: {
        api: ['uptime'],
        savedObject: {
          all: [],
          read: []
        },
        ui: ['save']
      },
      read: {
        api: ['uptime'],
        savedObject: {
          all: [],
          read: []
        },
        ui: []
      }
    }
  });
};

exports.initServerWithKibana = initServerWithKibana;