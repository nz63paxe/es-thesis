"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MemoryPingsAdapter = void 0;

var _lodash = require("lodash");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const sortPings = sort => sort === 'asc' ? (a, b) => Date.parse(a.timestamp) > Date.parse(b.timestamp) ? 1 : 0 : (a, b) => Date.parse(a.timestamp) > Date.parse(b.timestamp) ? 0 : 1;

class MemoryPingsAdapter {
  constructor(pingsDB) {
    _defineProperty(this, "pingsDB", void 0);

    this.pingsDB = pingsDB;
  }

  async getAll(request, dateRangeStart, dateRangeEnd, monitorId, status, sort, size) {
    let pings = this.pingsDB;

    if (monitorId) {
      pings = pings.filter(ping => ping.monitor && ping.monitor.id === monitorId);
    }

    const locations = this.pingsDB.map(ping => {
      return (0, _lodash.get)(ping, 'observer.geo.name');
    }).filter(location => !location) || [];
    size = size ? size : 10;
    return {
      total: size,
      pings: (0, _lodash.take)(sort ? pings.sort(sortPings(sort)) : pings, size),
      locations
    };
  } // TODO: implement


  getLatestMonitorDocs(request, dateRangeStart, dateRangeEnd, monitorId) {
    throw new Error('Method not implemented.');
  } // TODO: implement


  getPingHistogram(request, dateRangeStart, dateRangeEnd, filters, monitorId) {
    throw new Error('Method not implemented.');
  } // TODO: implement


  getDocCount(request) {
    throw new Error('Method not implemented.');
  }

}

exports.MemoryPingsAdapter = MemoryPingsAdapter;