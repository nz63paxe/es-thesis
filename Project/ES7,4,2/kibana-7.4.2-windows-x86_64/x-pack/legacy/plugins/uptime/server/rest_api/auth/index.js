"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createIsValidRoute", {
  enumerable: true,
  get: function () {
    return _is_valid.createIsValidRoute;
  }
});

var _is_valid = require("./is_valid");