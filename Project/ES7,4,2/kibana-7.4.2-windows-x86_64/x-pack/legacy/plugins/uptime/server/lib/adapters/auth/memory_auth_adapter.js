"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMMemoryAuthAdapter = void 0;

var _lodash = require("lodash");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class UMMemoryAuthAdapter {
  constructor(xpack) {
    this.xpack = xpack;

    _defineProperty(this, "getLicenseType", () => (0, _lodash.get)(this.xpack, 'info.license.type', null));

    _defineProperty(this, "licenseIsActive", () => this.xpack.info.license.isActive);

    this.xpack = xpack;
  }

}

exports.UMMemoryAuthAdapter = UMMemoryAuthAdapter;