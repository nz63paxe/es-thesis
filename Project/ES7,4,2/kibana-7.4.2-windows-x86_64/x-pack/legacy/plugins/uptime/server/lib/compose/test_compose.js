"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

var _auth = require("../adapters/auth");

var _test_backend_framework_adapter = require("../adapters/framework/test_backend_framework_adapter");

var _monitors = require("../adapters/monitors");

var _memory_pings_adapter = require("../adapters/pings/memory_pings_adapter");

var _domains = require("../domains");

var _monitor_states = require("../adapters/monitor_states");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function compose(server) {
  const framework = new _test_backend_framework_adapter.UMTestBackendFrameworkAdapter(server);
  const pingsDomain = new _domains.UMPingsDomain(new _memory_pings_adapter.MemoryPingsAdapter(server.pingsDB || []), framework);
  const authDomain = new _domains.UMAuthDomain(new _auth.UMMemoryAuthAdapter(server.xpack), framework);
  const monitorsDomain = new _domains.UMMonitorsDomain(new _monitors.UMMemoryMonitorsAdapter(server.pingsDB || []), framework);
  const monitorStatesDomain = new _domains.UMMonitorStatesDomain(new _monitor_states.UMMemoryMonitorStatesAdapter(), framework);
  const libs = {
    auth: authDomain,
    framework,
    pings: pingsDomain,
    monitors: monitorsDomain,
    monitorStates: monitorStatesDomain
  };
  return libs;
}