"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMTestBackendFrameworkAdapter = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class UMTestBackendFrameworkAdapter {
  constructor(server) {
    _defineProperty(this, "server", void 0);

    this.server = server;
  }

  registerRoute(route) {
    const {
      config,
      method,
      path,
      handler
    } = route;
    this.server.route({
      config,
      handler,
      method,
      path
    });
  }

  registerGraphQLEndpoint(routePath, schema) {
    this.server.register({
      options: {
        schema
      },
      path: routePath
    });
  }

}

exports.UMTestBackendFrameworkAdapter = UMTestBackendFrameworkAdapter;