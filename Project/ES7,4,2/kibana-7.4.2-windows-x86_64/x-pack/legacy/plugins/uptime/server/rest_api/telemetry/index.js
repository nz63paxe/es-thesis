"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createLogMonitorPageRoute", {
  enumerable: true,
  get: function () {
    return _log_monitor_page.createLogMonitorPageRoute;
  }
});
Object.defineProperty(exports, "createLogOverviewPageRoute", {
  enumerable: true,
  get: function () {
    return _log_overview_page.createLogOverviewPageRoute;
  }
});

var _log_monitor_page = require("./log_monitor_page");

var _log_overview_page = require("./log_overview_page");