"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMKibanaBackendFrameworkAdapter = void 0;

var _apollo_framework_adapter = require("./apollo_framework_adapter");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class UMKibanaBackendFrameworkAdapter {
  constructor(hapiServer) {
    _defineProperty(this, "server", void 0);

    this.server = hapiServer;
  }

  registerRoute(route) {
    this.server.route(route);
  }

  registerGraphQLEndpoint(routePath, schema) {
    this.server.register({
      options: {
        graphQLOptions: req => ({
          context: {
            req
          },
          schema
        }),
        path: routePath,
        route: {
          tags: ['access:uptime']
        }
      },
      plugin: _apollo_framework_adapter.uptimeGraphQLHapiPlugin
    });
  }

}

exports.UMKibanaBackendFrameworkAdapter = UMKibanaBackendFrameworkAdapter;