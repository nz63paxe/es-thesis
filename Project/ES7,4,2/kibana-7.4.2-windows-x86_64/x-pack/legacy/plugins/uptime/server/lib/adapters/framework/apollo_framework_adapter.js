"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uptimeGraphQLHapiPlugin = void 0;

var _apolloServerCore = require("apollo-server-core");

var _graphql = require("../../../graphql");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const uptimeGraphQLHapiPlugin = {
  name: 'uptimeGraphQL',
  register: (server, options) => {
    server.route({
      options: options.route,
      handler: async (request, h) => {
        try {
          const {
            method
          } = request;
          const query = method === 'post' ? request.payload : request.query;
          const graphQLResponse = await (0, _apolloServerCore.runHttpQuery)([request], {
            method: method.toUpperCase(),
            options: options.graphQLOptions,
            query
          });
          return h.response(graphQLResponse).type('application/json');
        } catch (error) {
          if (error.isGraphQLError === true) {
            return h.response(error.message).code(error.statusCode).type('application/json');
          }

          return h.response(error).type('application/json');
        }
      },
      method: ['get', 'post'],
      path: options.path || _graphql.DEFAULT_GRAPHQL_PATH,
      vhost: options.vhost || undefined
    });
  }
};
exports.uptimeGraphQLHapiPlugin = uptimeGraphQLHapiPlugin;