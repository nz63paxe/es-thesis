"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElasticsearchMonitorsAdapter = void 0;

var _lodash = require("lodash");

var _constants = require("../../../../common/constants");

var _helper = require("../../helper");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const formatStatusBuckets = (time, buckets, docCount) => {
  let up = null;
  let down = null;
  buckets.forEach(bucket => {
    if (bucket.key === 'up') {
      up = bucket.doc_count;
    } else if (bucket.key === 'down') {
      down = bucket.doc_count;
    }
  });
  return {
    x: time,
    up,
    down,
    total: docCount
  };
};

class ElasticsearchMonitorsAdapter {
  constructor(database) {
    this.database = database;
    this.database = database;
  }
  /**
   * Fetches data used to populate monitor charts
   * @param request Kibana request
   * @param monitorId ID value for the selected monitor
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   */


  async getMonitorChartsData(request, monitorId, dateRangeStart, dateRangeEnd, location) {
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: {
          bool: {
            filter: [{
              range: {
                '@timestamp': {
                  gte: dateRangeStart,
                  lte: dateRangeEnd
                }
              }
            }, {
              term: {
                'monitor.id': monitorId
              }
            }, // if location is truthy, add it as a filter. otherwise add nothing
            ...(!!location ? [{
              term: {
                'observer.geo.name': location
              }
            }] : [])]
          }
        },
        size: 0,
        aggs: {
          timeseries: {
            date_histogram: {
              field: '@timestamp',
              fixed_interval: (0, _helper.getHistogramInterval)(dateRangeStart, dateRangeEnd)
            },
            aggs: {
              location: {
                terms: {
                  field: 'observer.geo.name',
                  missing: 'N/A'
                },
                aggs: {
                  status: {
                    terms: {
                      field: 'monitor.status',
                      size: 2,
                      shard_size: 2
                    }
                  },
                  duration: {
                    stats: {
                      field: 'monitor.duration.us'
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    const dateBuckets = (0, _helper.dropLatestBucket)((0, _lodash.get)(result, 'aggregations.timeseries.buckets', []));
    /**
     * The code below is responsible for formatting the aggregation data we fetched above in a way
     * that the chart components used by the client understands.
     * There are five required values. Two are lists of points that conform to a simple (x,y) structure.
     *
     * The third list is for an area chart expressing a range, and it requires an (x,y,y0) structure,
     * where y0 is the min value for the point and y is the max.
     *
     * Additionally, we supply the maximum value for duration and status, so the corresponding charts know
     * what the domain size should be.
     */

    const monitorChartsData = {
      locationDurationLines: [],
      status: [],
      durationMaxValue: 0,
      statusMaxCount: 0
    };
    const linesByLocation = {};
    dateBuckets.forEach(dateBucket => {
      const x = (0, _lodash.get)(dateBucket, 'key');
      const docCount = (0, _lodash.get)(dateBucket, 'doc_count', 0);
      dateBucket.location.buckets.forEach(locationBucket => {
        const locationName = locationBucket.key;
        let ldl = (0, _lodash.get)(linesByLocation, locationName);

        if (!ldl) {
          ldl = {
            name: locationName,
            line: []
          };
          linesByLocation[locationName] = ldl;
          monitorChartsData.locationDurationLines.push(ldl);
        }

        ldl.line.push({
          x,
          y: (0, _lodash.get)(locationBucket, 'duration.avg', null)
        });
      });
      monitorChartsData.status.push(formatStatusBuckets(x, (0, _lodash.get)(dateBucket, 'status.buckets', []), docCount));
    });
    return monitorChartsData;
  }
  /**
   * Provides a count of the current monitors
   * @param request Kibana request
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param filters filters defined by client
   */


  async getSnapshotCount(request, dateRangeStart, dateRangeEnd, filters) {
    const {
      statusFilter,
      query
    } = (0, _helper.getFilteredQueryAndStatusFilter)(dateRangeStart, dateRangeEnd, filters);
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: {
          bool: {
            filter: [{
              exists: {
                field: 'summary.up'
              }
            }, query]
          }
        },
        size: 0,
        aggs: {
          ids: {
            composite: {
              sources: [{
                id: {
                  terms: {
                    field: 'monitor.id'
                  }
                }
              }, {
                location: {
                  terms: {
                    field: 'observer.geo.name',
                    missing_bucket: true
                  }
                }
              }],
              size: _constants.QUERY.DEFAULT_AGGS_CAP
            },
            aggs: {
              latest: {
                top_hits: {
                  sort: [{
                    '@timestamp': {
                      order: 'desc'
                    }
                  }],
                  _source: {
                    includes: ['summary.*', 'monitor.id', '@timestamp', 'observer.geo.name']
                  },
                  size: 1
                }
              }
            }
          }
        }
      }
    };
    let searchAfter = null;
    const summaryByIdLocation = {};

    do {
      if (searchAfter) {
        (0, _lodash.set)(params, 'body.aggs.ids.composite.after', searchAfter);
      }

      const queryResult = await this.database.search(request, params);
      const idBuckets = (0, _lodash.get)(queryResult, 'aggregations.ids.buckets', []);
      idBuckets.forEach(bucket => {
        // We only get the latest doc
        const source = (0, _lodash.get)(bucket, 'latest.hits.hits[0]._source');
        const {
          summary: {
            up,
            down
          },
          monitor: {
            id
          }
        } = source;
        const timestamp = (0, _lodash.get)(source, '@timestamp', 0);
        const location = (0, _lodash.get)(source, 'observer.geo.name', '');
        let idSummary = summaryByIdLocation[id];

        if (!idSummary) {
          idSummary = {};
          summaryByIdLocation[id] = idSummary;
        }

        const locationSummary = idSummary[location];

        if (!locationSummary || locationSummary.timestamp < timestamp) {
          idSummary[location] = {
            timestamp,
            up,
            down
          };
        }
      });
      searchAfter = (0, _lodash.get)(queryResult, 'aggregations.ids.after_key');
    } while (searchAfter);

    let up = 0;
    let mixed = 0;
    let down = 0;

    for (const id in summaryByIdLocation) {
      if (!summaryByIdLocation.hasOwnProperty(id)) {
        continue;
      }

      const locationInfo = summaryByIdLocation[id];
      const {
        up: locationUp,
        down: locationDown
      } = (0, _lodash.reduce)(locationInfo, (acc, value, key) => {
        acc.up += value.up;
        acc.down += value.down;
        return acc;
      }, {
        up: 0,
        down: 0
      });

      if (locationDown === 0) {
        up++;
      } else if (locationUp > 0) {
        mixed++;
      } else {
        down++;
      }
    }

    const result = {
      up,
      down,
      mixed,
      total: up + down + mixed
    };

    if (statusFilter) {
      for (const status in result) {
        if (status !== 'total' && status !== statusFilter) {
          result[status] = 0;
        }
      }
    }

    return result;
  }
  /**
   * Fetch the latest status for a monitors list
   * @param request Kibana request
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param filters filters defined by client
   */


  async getMonitors(request, dateRangeStart, dateRangeEnd, filters) {
    const {
      statusFilter,
      query
    } = (0, _helper.getFilteredQueryAndStatusFilter)(dateRangeStart, dateRangeEnd, filters);
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query,
        size: 0,
        aggs: {
          hosts: {
            composite: {
              sources: [{
                id: {
                  terms: {
                    field: 'monitor.id'
                  }
                }
              }, {
                url: {
                  terms: {
                    field: 'url.full'
                  }
                }
              }, {
                location: {
                  terms: {
                    field: 'observer.geo.name',
                    missing_bucket: true
                  }
                }
              }],
              size: 40
            },
            aggs: {
              latest: {
                top_hits: {
                  sort: [{
                    '@timestamp': {
                      order: 'desc'
                    }
                  }],
                  size: 1
                }
              },
              histogram: {
                date_histogram: {
                  field: '@timestamp',
                  fixed_interval: (0, _helper.getHistogramInterval)(dateRangeStart, dateRangeEnd),
                  missing: 0
                },
                aggs: {
                  status: {
                    terms: {
                      field: 'monitor.status',
                      size: 2,
                      shard_size: 2
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const queryResult = await this.database.search(request, params);
    const aggBuckets = (0, _lodash.get)(queryResult, 'aggregations.hosts.buckets', []);
    const latestMonitors = aggBuckets.filter(bucket => statusFilter && (0, _lodash.get)(bucket, 'latest.hits.hits[0]._source.monitor.status', undefined) === statusFilter || !statusFilter).map(bucket => {
      const key = (0, _lodash.get)(bucket, 'key.id');
      const url = (0, _lodash.get)(bucket, 'key.url', null);
      const upSeries = [];
      const downSeries = [];
      const histogramBuckets = (0, _lodash.get)(bucket, 'histogram.buckets', []);
      const ping = (0, _lodash.get)(bucket, 'latest.hits.hits[0]._source');
      const timestamp = (0, _lodash.get)(bucket, 'latest.hits.hits[0]._source.@timestamp');
      histogramBuckets.forEach(histogramBucket => {
        const status = (0, _lodash.get)(histogramBucket, 'status.buckets', []); // @ts-ignore TODO update typings and remove this comment

        const up = status.find(f => f.key === 'up'); // @ts-ignore TODO update typings and remove this comment

        const down = status.find(f => f.key === 'down'); // @ts-ignore TODO update typings and remove this comment

        upSeries.push({
          x: histogramBucket.key,
          y: up ? up.doc_count : null
        }); // @ts-ignore TODO update typings and remove this comment

        downSeries.push({
          x: histogramBucket.key,
          y: down ? down.doc_count : null
        });
      });
      return {
        id: {
          key,
          url
        },
        ping: { ...ping,
          timestamp
        },
        upSeries,
        downSeries
      };
    });
    return latestMonitors;
  }
  /**
   * Fetch options for the filter bar.
   * @param request Kibana request object
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   */


  async getFilterBar(request, dateRangeStart, dateRangeEnd) {
    const fields = {
      ids: 'monitor.id',
      schemes: 'monitor.type',
      names: 'monitor.name',
      urls: 'url.full',
      ports: 'url.port',
      locations: 'observer.geo.name'
    };
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        size: 0,
        query: {
          range: {
            '@timestamp': {
              gte: dateRangeStart,
              lte: dateRangeEnd
            }
          }
        },
        aggs: Object.values(fields).reduce((acc, field) => {
          acc[field] = {
            terms: {
              field,
              size: 20
            }
          };
          return acc;
        }, {})
      }
    };
    const {
      aggregations
    } = await this.database.search(request, params);
    return Object.keys(fields).reduce((acc, field) => {
      const bucketName = fields[field];
      acc[field] = aggregations[bucketName].buckets.filter(b => !!b.key).map(b => b.key);
      return acc;
    }, {});
  }
  /**
   * Fetch summaries of recent errors for monitors.
   * @example getErrorsList({}, 'now-15m', 'now', '{bool: { must: [{'term': {'monitor.status': {value: 'down'}}}]}})
   * @param request Request to send ES
   * @param dateRangeStart timestamp bounds
   * @param dateRangeEnd timestamp bounds
   * @param filters any filters specified on the client
   */


  async getErrorsList(request, dateRangeStart, dateRangeEnd, filters) {
    const statusDown = {
      term: {
        'monitor.status': {
          value: 'down'
        }
      }
    };
    const query = (0, _helper.getFilteredQuery)(dateRangeStart, dateRangeEnd, filters);

    if ((0, _lodash.get)(query, 'bool.filter', undefined)) {
      query.bool.filter.push(statusDown);
    } else {
      (0, _lodash.set)(query, 'bool.filter', [statusDown]);
    }

    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query,
        size: 0,
        aggs: {
          errors: {
            composite: {
              sources: [{
                id: {
                  terms: {
                    field: 'monitor.id'
                  }
                }
              }, {
                error_type: {
                  terms: {
                    field: 'error.type'
                  }
                }
              }, {
                location: {
                  terms: {
                    field: 'observer.geo.name',
                    missing_bucket: true
                  }
                }
              }],
              size: 50
            },
            aggs: {
              latest: {
                top_hits: {
                  sort: [{
                    '@timestamp': {
                      order: 'desc'
                    }
                  }],
                  size: 1
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    const buckets = (0, _lodash.get)(result, 'aggregations.errors.buckets', []);
    const errorsList = [];
    buckets.forEach(bucket => {
      const count = (0, _lodash.get)(bucket, 'doc_count', 0);
      const monitorId = (0, _lodash.get)(bucket, 'key.id', null);
      const errorType = (0, _lodash.get)(bucket, 'key.error_type', null);
      const location = (0, _lodash.get)(bucket, 'key.location', null);
      const source = (0, _lodash.get)(bucket, 'latest.hits.hits[0]._source', null);
      const errorMessage = (0, _lodash.get)(source, 'error.message', null);
      const statusCode = (0, _lodash.get)(source, 'http.response.status_code', null);
      const timestamp = (0, _lodash.get)(source, '@timestamp', null);
      const name = (0, _lodash.get)(source, 'monitor.name', null);
      errorsList.push({
        count,
        latestMessage: errorMessage,
        location,
        monitorId,
        name: name === '' ? null : name,
        statusCode,
        timestamp,
        type: errorType || ''
      });
    });
    return errorsList.sort(({
      count: A
    }, {
      count: B
    }) => B - A);
  }
  /**
   * Fetch data for the monitor page title.
   * @param request Kibana server request
   * @param monitorId the ID to query
   */


  async getMonitorPageTitle(request, monitorId) {
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: {
          bool: {
            filter: {
              term: {
                'monitor.id': monitorId
              }
            }
          }
        },
        sort: [{
          '@timestamp': {
            order: 'desc'
          }
        }],
        size: 1
      }
    };
    const result = await this.database.search(request, params);
    const pageTitle = (0, _lodash.get)(result, 'hits.hits[0]._source', null);

    if (pageTitle === null) {
      return null;
    }

    return {
      id: (0, _lodash.get)(pageTitle, 'monitor.id', null) || monitorId,
      url: (0, _lodash.get)(pageTitle, 'url.full', null),
      name: (0, _lodash.get)(pageTitle, 'monitor.name', null)
    };
  }

}

exports.ElasticsearchMonitorsAdapter = ElasticsearchMonitorsAdapter;