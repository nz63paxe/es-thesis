"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  restApiRoutes: true,
  createRouteWithAuth: true
};
Object.defineProperty(exports, "createRouteWithAuth", {
  enumerable: true,
  get: function () {
    return _create_route_with_auth.createRouteWithAuth;
  }
});
exports.restApiRoutes = void 0;

var _auth = require("./auth");

var _pings = require("./pings");

var _telemetry = require("./telemetry");

var _types = require("./types");

Object.keys(_types).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _types[key];
    }
  });
});

var _create_route_with_auth = require("./create_route_with_auth");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const restApiRoutes = [_auth.createIsValidRoute, _pings.createGetAllRoute, _telemetry.createLogMonitorPageRoute, _telemetry.createLogOverviewPageRoute];
exports.restApiRoutes = restApiRoutes;