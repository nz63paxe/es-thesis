"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFilteredQuery = void 0;

var _lodash = require("lodash");

var _constants = require("../../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns true if the given clause is not targeting a monitor's status.
 * @param clause the object containing filter criteria
 */
const isNotStatusClause = clause => !(0, _lodash.get)(clause, ['match', 'monitor.status']);
/**
 * This function identifies and filters cases where the status filter is nested within a list of `must` or `should` clauses.
 * If the supplied value is not an array, the original query is returned.
 * @param query the query to filter
 */


const removeNestedStatusQuery = query => Array.isArray(query) ? query.map(clauses => {
  if (clauses.bool && clauses.bool.must) {
    clauses.bool.must = (0, _lodash.get)(clauses, 'bool.must', []).filter(isNotStatusClause);
  }

  if (clauses.bool && clauses.bool.should) {
    clauses.bool.should = (0, _lodash.get)(clauses, 'bool.should', []).filter(isNotStatusClause);
  }

  return clauses;
}) : query;
/**
 * The purpose of this function is to return a filter query without a clause
 * targeting monitor.status. This is useful because a number of our queries rely
 * on post-processing to filter on status.
 * @param dateRangeStart the beginning of the range
 * @param dateRangeEnd the end of the range
 * @param filters any additional filter clauses
 */


const getFilteredQuery = (dateRangeStart, dateRangeEnd, filters) => {
  let filtersObj; // TODO: handle bad JSON gracefully

  if (typeof filters === 'string') {
    filtersObj = JSON.parse(filters);
  } else {
    filtersObj = filters;
  }

  if ((0, _lodash.get)(filtersObj, 'bool.must', undefined)) {
    const userFilters = (0, _lodash.get)(filtersObj, 'bool.must', []).map(filter => filter.simple_query_string ? {
      simple_query_string: { ...filter.simple_query_string,
        fields: _constants.QUERY.SIMPLE_QUERY_STRING_FIELDS
      }
    } : filter);
    delete filtersObj.bool.must;
    filtersObj.bool.filter = [...removeNestedStatusQuery(userFilters)];
  }

  const query = { ...filtersObj
  };
  const rangeSection = {
    range: {
      '@timestamp': {
        gte: dateRangeStart,
        lte: dateRangeEnd
      }
    }
  };

  if ((0, _lodash.get)(query, 'bool.filter', undefined)) {
    query.bool.filter.push(rangeSection);
  } else {
    (0, _lodash.set)(query, 'bool.filter', [rangeSection]);
  }

  return query;
};

exports.getFilteredQuery = getFilteredQuery;