"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMAuthDomain = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const supportedLicenses = ['basic', 'standard', 'gold', 'platinum', 'trial'];

class UMAuthDomain {
  constructor(adapter, libs) {
    this.adapter = adapter;
    this.adapter = adapter;
  }

  requestIsValid(request) {
    const license = this.adapter.getLicenseType();

    if (license === null) {
      throw _boom.default.badRequest('Missing license information');
    }

    if (!supportedLicenses.some(licenseType => licenseType === license)) {
      throw _boom.default.forbidden('License not supported');
    }

    if (this.adapter.licenseIsActive() === false) {
      throw _boom.default.forbidden('License not active');
    }

    return this.checkRequest(request);
  }

  checkRequest(request) {
    const authenticated = (0, _lodash.get)(request, 'auth.isAuthenticated', null);

    if (authenticated === null) {
      throw _boom.default.forbidden('Missing authentication');
    }

    return authenticated;
  }

}

exports.UMAuthDomain = UMAuthDomain;