"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElasticsearchMonitorStatesAdapter = void 0;

var _lodash = require("lodash");

var _constants = require("../../../../common/constants");

var _helper = require("../../helper");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const checksSortBy = check => [(0, _lodash.get)(check, 'observer.geo.name'), (0, _lodash.get)(check, 'monitor.ip')];

class ElasticsearchMonitorStatesAdapter {
  constructor(database) {
    this.database = database;
    this.database = database;
  } // This query returns the most recent check groups for a given
  // monitor ID.


  async runLegacyMonitorStatesRecentCheckGroupsQuery(request, query, searchAfter, size = 50) {
    const checkGroupsById = new Map();
    let afterKey = searchAfter;
    const body = {
      query: {
        bool: {
          filter: [{
            // We check for summary.up to ensure that the check group
            // is complete. Summary fields are only present on
            // completed check groups.
            exists: {
              field: 'summary.up'
            }
          }, query]
        }
      },
      sort: [{
        '@timestamp': 'desc'
      }],
      size: 0,
      aggs: {
        monitors: {
          composite: {
            /**
             * The goal here is to fetch more than enough check groups to reach the target
             * amount in one query.
             *
             * For larger cardinalities, we can only count on being able to fetch max bucket
             * size, so we will have to run this query multiple times.
             *
             * Multiplying `size` by 2 assumes that there will be less than three locations
             * for the deployment, if needed the query will be run subsequently.
             */
            size: Math.min(size * 2, _constants.QUERY.DEFAULT_AGGS_CAP),
            sources: [{
              monitor_id: {
                terms: {
                  field: 'monitor.id'
                }
              }
            }, {
              location: {
                terms: {
                  field: 'observer.geo.name',
                  missing_bucket: true
                }
              }
            }]
          },
          aggs: {
            top: {
              top_hits: {
                sort: [{
                  '@timestamp': 'desc'
                }],
                _source: {
                  includes: ['monitor.check_group', '@timestamp']
                },
                size: 1
              }
            }
          }
        }
      }
    };

    if (afterKey) {
      (0, _lodash.set)(body, 'aggs.monitors.composite.after', afterKey);
    }

    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body
    };
    const result = await this.database.search(request, params);
    afterKey = (0, _lodash.get)(result, 'aggregations.monitors.after_key', null);
    (0, _lodash.get)(result, 'aggregations.monitors.buckets', []).forEach(bucket => {
      const id = (0, _lodash.get)(bucket, 'key.monitor_id');
      const checkGroup = (0, _lodash.get)(bucket, 'top.hits.hits[0]._source.monitor.check_group');
      const value = checkGroupsById.get(id);

      if (!value) {
        checkGroupsById.set(id, [checkGroup]);
      } else if (value.indexOf(checkGroup) < 0) {
        checkGroupsById.set(id, [...value, checkGroup]);
      }
    });
    return {
      checkGroups: (0, _lodash.flatten)(Array.from(checkGroupsById.values())),
      afterKey
    };
  }

  async runLegacyMonitorStatesQuery(request, dateRangeStart, dateRangeEnd, filters, searchAfter, size = 50) {
    size = Math.min(size, _constants.QUERY.DEFAULT_AGGS_CAP);
    const {
      query,
      statusFilter
    } = (0, _helper.getFilteredQueryAndStatusFilter)(dateRangeStart, dateRangeEnd, filters); // First we fetch the most recent check groups for this query
    // This is a critical performance optimization.
    // Without this the expensive scripted_metric agg below will run
    // over large numbers of documents.
    // It only really needs to run over the latest complete check group for each
    // agent.

    const {
      checkGroups,
      afterKey
    } = await this.runLegacyMonitorStatesRecentCheckGroupsQuery(request, query, searchAfter, size);
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        query: {
          bool: {
            filter: [{
              terms: {
                'monitor.check_group': checkGroups
              }
            }, // Even though this work is already done when calculating the groups
            // this helps the planner
            query]
          }
        },
        sort: [{
          '@timestamp': 'desc'
        }],
        size: 0,
        aggs: {
          monitors: {
            composite: {
              size,
              sources: [{
                monitor_id: {
                  terms: {
                    field: 'monitor.id'
                  }
                }
              }]
            },
            aggregations: {
              state: {
                scripted_metric: {
                  init_script: `
                    // Globals are values that should be identical across all docs
                    // We can cheat a bit by always overwriting these and make the
                    // assumption that there is no variation in these across checks
                    state.globals = new HashMap();
                    // Here we store stuff broken out by agent.id and monitor.id
                    // This should correspond to a unique check.
                    state.checksByAgentIdIP = new HashMap();
                `,
                  map_script: `
                    Map curCheck = new HashMap();
                    String agentId = doc["agent.id"][0];
                    String ip = null;
                    if (doc["monitor.ip"].length > 0) {
                      ip = doc["monitor.ip"][0];
                    }
                    String agentIdIP = agentId + "-" + (ip == null ? "" : ip.toString());
                    def ts = doc["@timestamp"][0].toInstant().toEpochMilli();
                    
                    def lastCheck = state.checksByAgentIdIP[agentId];
                    Instant lastTs = lastCheck != null ? lastCheck["@timestamp"] : null;
                    if (lastTs != null && lastTs > ts) {
                      return;
                    }
                    
                    curCheck.put("@timestamp", ts);
                    
                    Map agent = new HashMap();
                    agent.id = agentId;
                    curCheck.put("agent", agent);
                    
                    if (state.globals.url == null) {
                      Map url = new HashMap();
                      Collection fields = ["full", "original", "scheme", "username", "password", "domain", "port", "path", "query", "fragment"];
                      for (field in fields) {
                        String docPath = "url." + field;
                        def val = doc[docPath];
                        if (!val.isEmpty()) {
                          url[field] = val[0];
                        }
                      }
                      state.globals.url = url;
                    }
                    
                    Map monitor = new HashMap();
                    monitor.status = doc["monitor.status"][0];
                    monitor.ip = ip;
                    if (!doc["monitor.name"].isEmpty()) {
                      String monitorName = doc["monitor.name"][0];
                      if (monitor.name != "") {
                        monitor.name = monitorName;
                      }
                    }
                    curCheck.monitor = monitor;
                    
                    if (curCheck.observer == null) {
                      curCheck.observer = new HashMap();
                    }
                    if (curCheck.observer.geo == null) {
                      curCheck.observer.geo = new HashMap();
                    }
                    if (!doc["observer.geo.name"].isEmpty()) {
                      curCheck.observer.geo.name = doc["observer.geo.name"][0];
                    }
                    if (!doc["observer.geo.location"].isEmpty()) {
                      curCheck.observer.geo.location = doc["observer.geo.location"][0];
                    }
                    if (!doc["kubernetes.pod.uid"].isEmpty() && curCheck.kubernetes == null) {
                      curCheck.kubernetes = new HashMap();
                      curCheck.kubernetes.pod = new HashMap();
                      curCheck.kubernetes.pod.uid = doc["kubernetes.pod.uid"][0];
                    }
                    if (!doc["container.id"].isEmpty() && curCheck.container == null) {
                      curCheck.container = new HashMap();
                      curCheck.container.id = doc["container.id"][0];
                    }
                    
                    state.checksByAgentIdIP[agentIdIP] = curCheck;
                `,
                  combine_script: 'return state;',
                  reduce_script: `
                  // The final document
                  Map result = new HashMap();
                  
                  Map checks = new HashMap();
                  Instant maxTs = Instant.ofEpochMilli(0);
                  Collection ips = new HashSet();
                  Collection geoNames = new HashSet();
                  Collection podUids = new HashSet();
                  Collection containerIds = new HashSet();
                  String name = null; 
                  for (state in states) {
                    result.putAll(state.globals);
                    for (entry in state.checksByAgentIdIP.entrySet()) {
                      def agentIdIP = entry.getKey();
                      def check = entry.getValue();
                      def lastBestCheck = checks.get(agentIdIP);
                      def checkTs = Instant.ofEpochMilli(check.get("@timestamp"));
                  
                      if (maxTs.isBefore(checkTs)) { maxTs = checkTs}
                  
                      if (lastBestCheck == null || lastBestCheck.get("@timestamp") < checkTs) {
                        check["@timestamp"] = check["@timestamp"];
                        checks[agentIdIP] = check
                      }

                      if (check.monitor.name != null && check.monitor.name != "") {
                        name = check.monitor.name;
                      }

                      ips.add(check.monitor.ip);
                      if (check.observer != null && check.observer.geo != null && check.observer.geo.name != null) {
                        geoNames.add(check.observer.geo.name);
                      }
                      if (check.kubernetes != null && check.kubernetes.pod != null) {
                        podUids.add(check.kubernetes.pod.uid);
                      }
                      if (check.container != null) {
                        containerIds.add(check.container.id);
                      }
                    }
                  }
                  
                  // We just use the values so we can store these as nested docs
                  result.checks = checks.values();
                  result.put("@timestamp", maxTs);
                  
                  
                  Map summary = new HashMap();
                  summary.up = checks.entrySet().stream().filter(c -> c.getValue().monitor.status == "up").count();
                  summary.down = checks.size() - summary.up;
                  result.summary = summary;
                  
                  Map monitor = new HashMap();
                  monitor.ip = ips;
                  monitor.name = name;
                  monitor.status = summary.down > 0 ? (summary.up > 0 ? "mixed": "down") : "up";
                  result.monitor = monitor;
                  
                  Map observer = new HashMap();
                  Map geo = new HashMap();
                  observer.geo = geo;
                  geo.name = geoNames;
                  result.observer = observer;
                  
                  if (!podUids.isEmpty()) {
                    result.kubernetes = new HashMap();
                    result.kubernetes.pod = new HashMap();
                    result.kubernetes.pod.uid = podUids;
                  }

                  if (!containerIds.isEmpty()) {
                    result.container = new HashMap();
                    result.container.id = containerIds;
                  }

                  return result;
                `
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    return {
      afterKey,
      result,
      statusFilter
    };
  }

  getMonitorBuckets(queryResult, statusFilter) {
    let monitors = (0, _lodash.get)(queryResult, 'aggregations.monitors.buckets', []);

    if (statusFilter) {
      monitors = monitors.filter(monitor => (0, _lodash.get)(monitor, 'state.value.monitor.status') === statusFilter);
    }

    return monitors;
  }

  async legacyGetMonitorStates(request, dateRangeStart, dateRangeEnd, filters) {
    const monitors = [];
    let searchAfter = null;

    do {
      const {
        result,
        statusFilter,
        afterKey
      } = await this.runLegacyMonitorStatesQuery(request, dateRangeStart, dateRangeEnd, filters, searchAfter);
      monitors.push(...this.getMonitorBuckets(result, statusFilter));
      searchAfter = afterKey;
    } while (searchAfter !== null && monitors.length < _constants.STATES.LEGACY_STATES_QUERY_SIZE);

    const monitorIds = [];
    const summaries = monitors.map(monitor => {
      const monitorId = (0, _lodash.get)(monitor, 'key.monitor_id');
      monitorIds.push(monitorId);
      let state = (0, _lodash.get)(monitor, 'state.value');
      state = { ...state,
        timestamp: state['@timestamp']
      };
      const {
        checks
      } = state;

      if (checks) {
        state.checks = (0, _lodash.sortBy)(checks, checksSortBy);
        state.checks = state.checks.map(check => ({ ...check,
          timestamp: check['@timestamp']
        }));
      } else {
        state.checks = [];
      }

      return {
        monitor_id: monitorId,
        state
      };
    });
    const histogramMap = await this.getHistogramForMonitors(request, dateRangeStart, dateRangeEnd, monitorIds);
    return summaries.map(summary => ({ ...summary,
      histogram: histogramMap[summary.monitor_id]
    }));
  }

  async getMonitorStates(request, pageIndex, pageSize, sortField, sortDirection) {
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT_STATES,
      body: {
        from: pageIndex * pageSize,
        size: pageSize
      }
    };

    if (sortField) {
      (0, _lodash.set)(params, 'body.sort', [{
        [sortField]: {
          order: sortDirection || 'asc'
        }
      }]);
    }

    const result = await this.database.search(request, params);
    const hits = (0, _lodash.get)(result, 'hits.hits', []);
    const monitorIds = [];
    const monitorStates = hits.map(({
      _source
    }) => {
      const {
        monitor_id
      } = _source;
      monitorIds.push(monitor_id);
      const sourceState = (0, _lodash.get)(_source, 'state');
      const state = { ...sourceState,
        timestamp: sourceState['@timestamp']
      };

      if (state.checks) {
        state.checks = (0, _lodash.sortBy)(state.checks, checksSortBy).map(check => ({ ...check,
          timestamp: check['@timestamp']
        }));
      } else {
        state.checks = [];
      }

      return {
        monitor_id,
        state
      };
    });
    const histogramMap = await this.getHistogramForMonitors(request, 'now-15m', 'now', monitorIds);
    return monitorStates.map(monitorState => ({ ...monitorState,
      histogram: histogramMap[monitorState.monitor_id]
    }));
  }

  async getHistogramForMonitors(request, dateRangeStart, dateRangeEnd, monitorIds) {
    const params = {
      index: _constants.INDEX_NAMES.HEARTBEAT,
      body: {
        size: 0,
        query: {
          bool: {
            filter: [{
              terms: {
                'monitor.id': monitorIds
              }
            }, {
              range: {
                '@timestamp': {
                  gte: dateRangeStart,
                  lte: dateRangeEnd
                }
              }
            }]
          }
        },
        aggs: {
          by_id: {
            terms: {
              field: 'monitor.id',
              size: _constants.STATES.LEGACY_STATES_QUERY_SIZE
            },
            aggs: {
              histogram: {
                date_histogram: {
                  field: '@timestamp',
                  fixed_interval: (0, _helper.getHistogramInterval)(dateRangeStart, dateRangeEnd),
                  missing: 0
                },
                aggs: {
                  status: {
                    terms: {
                      field: 'monitor.status',
                      size: 2,
                      shard_size: 2
                    }
                  }
                }
              }
            }
          }
        }
      }
    };
    const result = await this.database.search(request, params);
    const buckets = (0, _lodash.get)(result, 'aggregations.by_id.buckets', []);
    return buckets.reduce((map, item) => {
      const points = (0, _lodash.get)(item, 'histogram.buckets', []).map(histogram => {
        const status = (0, _lodash.get)(histogram, 'status.buckets', []).reduce((statuses, bucket) => {
          if (bucket.key === 'up') {
            statuses.up = bucket.doc_count;
          } else if (bucket.key === 'down') {
            statuses.down = bucket.doc_count;
          }

          return statuses;
        }, {
          up: 0,
          down: 0
        });
        return {
          timestamp: histogram.key,
          ...status
        };
      });
      map[item.key] = {
        count: item.doc_count,
        points
      };
      return map;
    }, {});
  }

  async statesIndexExists(request) {
    // TODO: adapt this to the states index in future release
    const {
      _shards: {
        total
      },
      count
    } = await this.database.count(request, {
      index: _constants.INDEX_NAMES.HEARTBEAT
    });
    return {
      indexExists: total > 0,
      docCount: {
        count
      }
    };
  }

}

exports.ElasticsearchMonitorStatesAdapter = ElasticsearchMonitorStatesAdapter;