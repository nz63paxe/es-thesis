"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLogOverviewPageRoute = void 0;

var _telemetry = require("../../lib/adapters/telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createLogOverviewPageRoute = () => ({
  method: 'POST',
  path: '/api/uptime/logOverview',
  handler: async (request, h) => {
    await _telemetry.KibanaTelemetryAdapter.countOverview();
    return h.response().code(200);
  },
  options: {
    tags: ['access:uptime']
  }
});

exports.createLogOverviewPageRoute = createLogOverviewPageRoute;