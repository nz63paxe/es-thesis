"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMMemoryMonitorStatesAdapter = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * This class will be implemented for server-side tests.
 */
class UMMemoryMonitorStatesAdapter {
  async legacyGetMonitorStates(request, dateRangeStart, dateRangeEnd, filters) {
    throw new Error('Method not implemented.');
  }

  async getMonitorStates(request, pageIndex, pageSize, sortField, sortDirection) {
    throw new Error('Method not implemented.');
  }

  async statesIndexExists(request) {
    throw new Error('Method not implemented.');
  }

}

exports.UMMemoryMonitorStatesAdapter = UMMemoryMonitorStatesAdapter;