"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createGetAllRoute", {
  enumerable: true,
  get: function () {
    return _get_all.createGetAllRoute;
  }
});

var _get_all = require("./get_all");