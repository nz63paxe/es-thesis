"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PLUGIN = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const PLUGIN = {
  ID: 'uptime',
  ROUTER_BASE_NAME: '/app/uptime#/',
  LOCAL_STORAGE_KEY: 'xpack.uptime'
};
exports.PLUGIN = PLUGIN;