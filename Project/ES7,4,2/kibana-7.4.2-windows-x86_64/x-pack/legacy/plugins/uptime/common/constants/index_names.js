"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.INDEX_NAMES = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const INDEX_NAMES = {
  HEARTBEAT: 'heartbeat*',
  HEARTBEAT_STATES: 'heartbeat-states-7*'
};
exports.INDEX_NAMES = INDEX_NAMES;