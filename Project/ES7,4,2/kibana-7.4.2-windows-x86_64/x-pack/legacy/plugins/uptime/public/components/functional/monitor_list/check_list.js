"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CheckList = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _lodash = require("lodash");

var _monitor_list_status_column = require("./monitor_list_status_column");

var _location_link = require("./location_link");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CheckList = function CheckList(_ref) {
  var checks = _ref.checks;
  return _react.default.createElement(_eui.EuiFlexGrid, {
    columns: 3,
    gutterSize: "l",
    style: {
      paddingLeft: '40px'
    }
  }, checks.map(function (check) {
    var location = (0, _lodash.get)(check, 'observer.geo.name', null);
    var agentId = (0, _lodash.get)(check, 'agent.id', 'null');
    var key = location + agentId + check.monitor.ip;
    return _react.default.createElement(_react.Fragment, {
      key: key
    }, _react.default.createElement(_eui.EuiFlexItem, {
      grow: 4
    }, _react.default.createElement(_monitor_list_status_column.MonitorListStatusColumn, {
      status: check.monitor.status,
      timestamp: check.timestamp
    })), _react.default.createElement(_eui.EuiFlexItem, {
      grow: 4
    }, _react.default.createElement(_location_link.LocationLink, {
      location: location
    })), _react.default.createElement(_eui.EuiFlexItem, {
      grow: 4
    }, _react.default.createElement(_eui.EuiText, {
      color: "secondary",
      size: "s"
    }, check.monitor.ip)));
  }));
};

exports.CheckList = CheckList;