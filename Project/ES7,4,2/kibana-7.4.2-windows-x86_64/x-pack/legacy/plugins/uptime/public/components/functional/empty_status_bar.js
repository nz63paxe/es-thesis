"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmptyStatusBar = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var EmptyStatusBar = function EmptyStatusBar(_ref) {
  var message = _ref.message,
      monitorId = _ref.monitorId;
  return _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "l"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, !message ? _i18n.i18n.translate('xpack.uptime.emptyStatusBar.defaultMessage', {
    defaultMessage: 'No data found for monitor id {monitorId}',
    description: 'This is the default message we display in a status bar when there is no data available for an uptime monitor.',
    values: {
      monitorId: monitorId
    }
  }) : message)));
};

exports.EmptyStatusBar = EmptyStatusBar;