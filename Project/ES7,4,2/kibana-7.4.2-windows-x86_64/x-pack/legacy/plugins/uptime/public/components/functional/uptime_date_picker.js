"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UptimeDatePicker = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _hooks = require("../../hooks");

var _constants = require("../../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var UptimeDatePicker = function UptimeDatePicker(props) {
  var refreshApp = props.refreshApp;

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 2),
      getUrlParams = _useUrlParams2[0],
      updateUrl = _useUrlParams2[1];

  var _getUrlParams = getUrlParams(),
      autorefreshInterval = _getUrlParams.autorefreshInterval,
      autorefreshIsPaused = _getUrlParams.autorefreshIsPaused,
      dateRangeStart = _getUrlParams.dateRangeStart,
      dateRangeEnd = _getUrlParams.dateRangeEnd;

  return _react.default.createElement(_eui.EuiSuperDatePicker, {
    start: dateRangeStart,
    end: dateRangeEnd,
    commonlyUsedRanges: _constants.CLIENT_DEFAULTS.COMMONLY_USED_DATE_RANGES,
    isPaused: autorefreshIsPaused,
    refreshInterval: autorefreshInterval,
    onTimeChange: function onTimeChange(_ref) {
      var start = _ref.start,
          end = _ref.end;
      updateUrl({
        dateRangeStart: start,
        dateRangeEnd: end
      });
      refreshApp();
    } // @ts-ignore onRefresh is not defined on EuiSuperDatePicker's type yet
    ,
    onRefresh: refreshApp,
    onRefreshChange: function onRefreshChange(_ref2) {
      var isPaused = _ref2.isPaused,
          refreshInterval = _ref2.refreshInterval;
      updateUrl({
        autorefreshInterval: refreshInterval === undefined ? autorefreshInterval : refreshInterval,
        autorefreshPaused: isPaused
      });
    }
  });
};

exports.UptimeDatePicker = UptimeDatePicker;