"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SnapshotLoading = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore missing typings for EuiStat
var SnapshotLoading = function SnapshotLoading() {
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: 4
  }, _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react2.default.createElement("h5", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.snapshot.endpointStatusLoadingTitle",
    defaultMessage: "Current status"
  }))), _react2.default.createElement(_eui.EuiPanel, {
    paddingSize: "s",
    style: {
      height: 170
    }
  }, _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiSpacer, {
    size: "s"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceEvenly",
    gutterSize: "s"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.upDescription', {
      defaultMessage: 'Up'
    }),
    textAlign: "center",
    title: "-",
    titleColor: "secondary"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.downDescription', {
      defaultMessage: 'Down'
    }),
    textAlign: "center",
    title: "-",
    titleColor: "danger"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.totalDescription', {
      defaultMessage: 'Total'
    }),
    textAlign: "center",
    title: "-",
    titleColor: "subdued"
  })))))))));
};

exports.SnapshotLoading = SnapshotLoading;