"use strict";

var _i18n = require("@kbn/i18n");

var _feature_catalogue = require("ui/registry/feature_catalogue");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
_feature_catalogue.FeatureCatalogueRegistryProvider.register(function () {
  return {
    id: 'uptime',
    title: _i18n.i18n.translate('xpack.uptime.uptimeFeatureCatalogueTitle', {
      defaultMessage: 'Uptime'
    }),
    description: _i18n.i18n.translate('xpack.uptime.featureCatalogueDescription', {
      defaultMessage: 'Perform endpoint health checks and uptime monitoring.'
    }),
    icon: 'uptimeApp',
    path: "uptime#/",
    showOnHomePage: true,
    category: _feature_catalogue.FeatureCatalogueCategory.DATA
  };
});