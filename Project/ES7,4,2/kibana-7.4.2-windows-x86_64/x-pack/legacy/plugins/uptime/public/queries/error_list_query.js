"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.errorListQuery = exports.errorListQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var errorListQueryString = "\nquery ErrorList($dateRangeStart: String!, $dateRangeEnd: String!, $filters: String) {\n  errorList: getErrorsList(\n    dateRangeStart: $dateRangeStart\n    dateRangeEnd: $dateRangeEnd\n    filters: $filters\n  ) {\n    count\n    latestMessage\n    location\n    monitorId\n    name\n    statusCode\n    timestamp\n    type\n  }\n}\n";
exports.errorListQueryString = errorListQueryString;
var errorListQuery = (0, _graphqlTag.default)(_templateObject(), errorListQueryString);
exports.errorListQuery = errorListQuery;