"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUrlParams = void 0;

var _querystring = _interopRequireDefault(require("querystring"));

var _react = require("react");

var _contexts = require("../contexts");

var _helper = require("../lib/helper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var useUrlParams = function useUrlParams() {
  var refreshContext = (0, _react.useContext)(_contexts.UptimeRefreshContext);

  var getUrlParams = function getUrlParams() {
    var search;

    if (refreshContext.location) {
      search = refreshContext.location.search;
    }

    var params = search ? _objectSpread({}, _querystring.default.parse(search[0] === '?' ? search.slice(1) : search)) : {};
    return (0, _helper.getSupportedUrlParams)(params);
  };

  var updateUrlParams = function updateUrlParams(updatedParams) {
    if (refreshContext.history && refreshContext.location) {
      var history = refreshContext.history,
          _refreshContext$locat = refreshContext.location,
          pathname = _refreshContext$locat.pathname,
          search = _refreshContext$locat.search;

      var currentParams = _querystring.default.parse(search[0] === '?' ? search.slice(1) : search);

      history.push({
        pathname: pathname,
        search: _querystring.default.stringify(_objectSpread({}, currentParams, {}, updatedParams))
      });
    }
  };

  return [getUrlParams, updateUrlParams];
};

exports.useUrlParams = useUrlParams;