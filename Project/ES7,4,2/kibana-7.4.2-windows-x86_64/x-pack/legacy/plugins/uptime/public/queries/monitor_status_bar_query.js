"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.monitorStatusBarQuery = exports.monitorStatusBarQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var monitorStatusBarQueryString = "\nquery MonitorStatus($dateRangeStart: String!, $dateRangeEnd: String!, $monitorId: String, $location: String) {\n  monitorStatus: getLatestMonitors(\n    dateRangeStart: $dateRangeStart\n    dateRangeEnd: $dateRangeEnd\n    monitorId: $monitorId\n    location: $location\n  ) {\n    timestamp\n    millisFromNow\n    monitor {\n      status\n      duration {\n        us\n      }\n    }\n    observer {\n      geo {\n        name\n      }\n    }\n    url {\n      full\n    }\n  }\n}\n";
exports.monitorStatusBarQueryString = monitorStatusBarQueryString;
var monitorStatusBarQuery = (0, _graphqlTag.default)(_templateObject(), monitorStatusBarQueryString);
exports.monitorStatusBarQuery = monitorStatusBarQuery;