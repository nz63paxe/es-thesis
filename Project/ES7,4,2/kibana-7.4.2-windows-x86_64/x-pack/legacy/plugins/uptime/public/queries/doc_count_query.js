"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.docCountQuery = exports.docCountQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var docCountQueryString = "\nquery GetStateIndexStatus {\n  statesIndexStatus: getStatesIndexStatus {\n    docCount {\n      count\n    }\n    indexExists\n  }\n}\n";
exports.docCountQueryString = docCountQueryString;
var docCountQuery = (0, _graphqlTag.default)(_templateObject(), docCountQueryString);
exports.docCountQuery = docCountQuery;