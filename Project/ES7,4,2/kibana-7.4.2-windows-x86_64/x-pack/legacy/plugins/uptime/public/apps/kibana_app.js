"use strict";

var _kibana_compose = require("../lib/compose/kibana_compose");

var _start_app = require("./start_app");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
(0, _start_app.startApp)((0, _kibana_compose.compose)());