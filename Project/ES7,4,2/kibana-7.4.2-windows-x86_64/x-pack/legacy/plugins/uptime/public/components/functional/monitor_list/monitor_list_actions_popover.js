"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorListActionsPopover = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _integration_group = require("../integration_group");

var _contexts = require("../../../contexts");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var MonitorListActionsPopover = function MonitorListActionsPopover(_ref) {
  var summary = _ref.summary;
  var popoverId = "".concat(summary.monitor_id, "_popover");

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      popoverIsVisible = _useState2[0],
      setPopoverIsVisible = _useState2[1];

  var _useContext = (0, _react.useContext)(_contexts.UptimeSettingsContext),
      basePath = _useContext.basePath,
      dateRangeStart = _useContext.dateRangeStart,
      dateRangeEnd = _useContext.dateRangeEnd,
      isApmAvailable = _useContext.isApmAvailable,
      isInfraAvailable = _useContext.isInfraAvailable,
      isLogsAvailable = _useContext.isLogsAvailable;

  var monitorUrl = (0, _lodash.get)(summary, 'state.url.full', undefined);
  return _react.default.createElement(_eui.EuiPopover, {
    button: _react.default.createElement(_eui.EuiButtonIcon, {
      "aria-label": _i18n.i18n.translate('xpack.uptime.monitorList.observabilityIntegrationsColumn.popoverIconButton.ariaLabel', {
        defaultMessage: 'Opens integrations popover for monitor with url {monitorUrl}',
        description: 'A message explaining that this button opens a popover with links to other apps for a given monitor',
        values: {
          monitorUrl: monitorUrl
        }
      }),
      color: "subdued",
      iconType: "boxesHorizontal",
      onClick: function onClick() {
        return setPopoverIsVisible(true);
      }
    }),
    closePopover: function closePopover() {
      return setPopoverIsVisible(false);
    },
    id: popoverId,
    isOpen: popoverIsVisible
  }, _react.default.createElement(_integration_group.IntegrationGroup, {
    basePath: basePath,
    dateRangeStart: dateRangeStart,
    dateRangeEnd: dateRangeEnd,
    isApmAvailable: isApmAvailable,
    isInfraAvailable: isInfraAvailable,
    isLogsAvailable: isLogsAvailable,
    summary: summary
  }));
};

exports.MonitorListActionsPopover = MonitorListActionsPopover;