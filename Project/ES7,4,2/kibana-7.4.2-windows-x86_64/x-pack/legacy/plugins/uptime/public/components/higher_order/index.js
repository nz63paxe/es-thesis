"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "UptimeGraphQLQueryProps", {
  enumerable: true,
  get: function get() {
    return _uptime_graphql_query.UptimeGraphQLQueryProps;
  }
});
Object.defineProperty(exports, "withUptimeGraphQL", {
  enumerable: true,
  get: function get() {
    return _uptime_graphql_query.withUptimeGraphQL;
  }
});

var _uptime_graphql_query = require("./uptime_graphql_query");