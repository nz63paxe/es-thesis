"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FilterBarLoading = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore No typings for EuiSearchBar
var searchBox = {
  placeholder: _i18n.i18n.translate('xpack.uptime.filterBar.loadingMessage', {
    defaultMessage: 'Loading…'
  })
};
/**
 * This component provides a visual placeholder while the FilterBar is loading.
 * The onChange prop is required, so we provide an empty function to suppress the warning.
 */

var FilterBarLoading = function FilterBarLoading() {
  return _react.default.createElement(_eui.EuiSearchBar, {
    box: searchBox,
    onChange: function onChange() {
      /* */
    }
  });
};

exports.FilterBarLoading = FilterBarLoading;