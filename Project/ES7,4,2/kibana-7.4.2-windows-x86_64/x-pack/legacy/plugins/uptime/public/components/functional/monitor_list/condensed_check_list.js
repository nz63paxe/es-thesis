"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CondensedCheckList = void 0;

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _monitor_list_status_column = require("./monitor_list_status_column");

var _location_link = require("./location_link");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getBadgeColor = function getBadgeColor(status, successColor, dangerColor) {
  switch (status) {
    case 'up':
      return successColor;

    case 'down':
      return dangerColor;

    case 'mixed':
      return 'secondary';

    default:
      return undefined;
  }
};

var getHealthColor = function getHealthColor(dangerColor, status, successColor) {
  switch (status) {
    case 'up':
      return successColor;

    case 'down':
      return dangerColor;

    default:
      return 'primary';
  }
};

var CondensedCheckList = function CondensedCheckList(_ref) {
  var condensedChecks = _ref.condensedChecks,
      dangerColor = _ref.dangerColor,
      successColor = _ref.successColor;
  return _react.default.createElement(_eui.EuiFlexGrid, {
    columns: 3,
    style: {
      paddingLeft: '40px'
    }
  }, condensedChecks.map(function (_ref2) {
    var childStatuses = _ref2.childStatuses,
        location = _ref2.location,
        status = _ref2.status,
        timestamp = _ref2.timestamp;
    return _react.default.createElement(_react.default.Fragment, {
      key: location || 'null'
    }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_monitor_list_status_column.MonitorListStatusColumn, {
      status: status,
      timestamp: timestamp
    })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_location_link.LocationLink, {
      location: location
    })))), _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_eui.EuiToolTip, {
      position: "right",
      title: "Check statuses",
      content: childStatuses.map(function (_ref3) {
        var checkStatus = _ref3.status,
            ip = _ref3.ip,
            condensedTimestamp = _ref3.timestamp;
        return ip ? _react.default.createElement(_eui.EuiFlexGroup, {
          key: ip
        }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiHealth, {
          color: getHealthColor(successColor, checkStatus, dangerColor)
        })), _react.default.createElement(_eui.EuiFlexItem, null, ip), _react.default.createElement(_eui.EuiFlexItem, null, (0, _moment.default)(parseInt(condensedTimestamp, 10)).fromNow())) : null;
      })
    }, _react.default.createElement(_eui.EuiBadge, {
      color: getBadgeColor(status, successColor, dangerColor)
    }, "".concat(childStatuses.length, " checks")))));
  }));
};

exports.CondensedCheckList = CondensedCheckList;