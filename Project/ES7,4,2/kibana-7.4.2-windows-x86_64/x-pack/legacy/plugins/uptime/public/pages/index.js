"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "MonitorPage", {
  enumerable: true,
  get: function get() {
    return _monitor.MonitorPage;
  }
});
Object.defineProperty(exports, "OverviewPage", {
  enumerable: true,
  get: function get() {
    return _overview.OverviewPage;
  }
});

var _monitor = require("./monitor");

var _overview = require("./overview");