"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UptimeApp = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui = require("@elastic/eui");

var _eui_theme_dark = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_dark.json"));

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _reactRouterDom = require("react-router-dom");

var _capabilities = require("ui/capabilities");

var _i18n2 = require("ui/i18n");

var _pages = require("./pages");

var _contexts = require("./contexts");

var _uptime_date_picker = require("./components/functional/uptime_date_picker");

var _hooks = require("./hooks");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Application = function Application(props) {
  var basePath = props.basePath,
      client = props.client,
      darkMode = props.darkMode,
      isApmAvailable = props.isApmAvailable,
      isInfraAvailable = props.isInfraAvailable,
      isLogsAvailable = props.isLogsAvailable,
      logMonitorPageLoad = props.logMonitorPageLoad,
      logOverviewPageLoad = props.logOverviewPageLoad,
      renderGlobalHelpControls = props.renderGlobalHelpControls,
      routerBasename = props.routerBasename,
      setBreadcrumbs = props.setBreadcrumbs,
      setBadge = props.setBadge;
  var colors;

  if (darkMode) {
    colors = {
      danger: _eui_theme_dark.default.euiColorDanger,
      mean: _eui_theme_dark.default.euiColorPrimary,
      range: _eui_theme_dark.default.euiFocusBackgroundColor,
      success: _eui_theme_dark.default.euiColorSuccess,
      warning: _eui_theme_dark.default.euiColorWarning
    };
  } else {
    colors = {
      danger: _eui_theme_light.default.euiColorDanger,
      mean: _eui_theme_light.default.euiColorPrimary,
      range: _eui_theme_light.default.euiFocusBackgroundColor,
      success: _eui_theme_light.default.euiColorSuccess,
      warning: _eui_theme_light.default.euiColorWarning
    };
  }

  var _useState = (0, _react.useState)(Date.now()),
      _useState2 = _slicedToArray(_useState, 2),
      lastRefresh = _useState2[0],
      setLastRefresh = _useState2[1];

  var _useState3 = (0, _react.useState)(undefined),
      _useState4 = _slicedToArray(_useState3, 2),
      headingText = _useState4[0],
      setHeadingText = _useState4[1];

  (0, _react.useEffect)(function () {
    renderGlobalHelpControls();
    setBadge(!_capabilities.capabilities.get().uptime.save ? {
      text: _i18n.i18n.translate('xpack.uptime.badge.readOnly.text', {
        defaultMessage: 'Read only'
      }),
      tooltip: _i18n.i18n.translate('xpack.uptime.badge.readOnly.tooltip', {
        defaultMessage: 'Unable to save'
      }),
      iconType: 'glasses'
    } : undefined);
  }, []);

  var refreshApp = function refreshApp() {
    setLastRefresh(Date.now());
  };

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 1),
      getUrlParams = _useUrlParams2[0];

  var initializeSettingsContextValues = function initializeSettingsContextValues() {
    var _getUrlParams = getUrlParams(),
        autorefreshInterval = _getUrlParams.autorefreshInterval,
        autorefreshIsPaused = _getUrlParams.autorefreshIsPaused,
        dateRangeStart = _getUrlParams.dateRangeStart,
        dateRangeEnd = _getUrlParams.dateRangeEnd;

    var absoluteStartDate = _datemath.default.parse(dateRangeStart);

    var absoluteEndDate = _datemath.default.parse(dateRangeEnd);

    return {
      // TODO: extract these values to dedicated (and more sensible) constants
      absoluteStartDate: absoluteStartDate ? absoluteStartDate.valueOf() : 0,
      absoluteEndDate: absoluteEndDate ? absoluteEndDate.valueOf() : 1,
      autorefreshInterval: autorefreshInterval,
      autorefreshIsPaused: autorefreshIsPaused,
      basePath: basePath,
      colors: colors,
      dateRangeStart: dateRangeStart,
      dateRangeEnd: dateRangeEnd,
      isApmAvailable: isApmAvailable,
      isInfraAvailable: isInfraAvailable,
      isLogsAvailable: isLogsAvailable,
      refreshApp: refreshApp,
      setHeadingText: setHeadingText
    };
  };

  return _react.default.createElement(_i18n2.I18nContext, null, _react.default.createElement(_reactRouterDom.BrowserRouter, {
    basename: routerBasename
  }, _react.default.createElement(_reactRouterDom.Route, {
    path: "/",
    render: function render(rootRouteProps) {
      return _react.default.createElement(_reactApollo.ApolloProvider, {
        client: client
      }, _react.default.createElement(_contexts.UptimeRefreshContext.Provider, {
        value: _objectSpread({
          lastRefresh: lastRefresh
        }, rootRouteProps)
      }, _react.default.createElement(_contexts.UptimeSettingsContext.Provider, {
        value: initializeSettingsContextValues()
      }, _react.default.createElement(_eui.EuiPage, {
        className: "app-wrapper-panel ",
        "data-test-subj": "uptimeApp"
      }, _react.default.createElement("div", null, _react.default.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        justifyContent: "spaceBetween",
        gutterSize: "s"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h1", null, headingText))), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_uptime_date_picker.UptimeDatePicker, _extends({
        refreshApp: refreshApp
      }, rootRouteProps)))), _react.default.createElement(_eui.EuiSpacer, {
        size: "s"
      }), _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
        exact: true,
        path: "/",
        render: function render(routerProps) {
          return _react.default.createElement(_pages.OverviewPage, _extends({
            basePath: basePath,
            logOverviewPageLoad: logOverviewPageLoad,
            setBreadcrumbs: setBreadcrumbs
          }, routerProps));
        }
      }), _react.default.createElement(_reactRouterDom.Route, {
        path: "/monitor/:monitorId/:location?",
        render: function render(routerProps) {
          return _react.default.createElement(_pages.MonitorPage, _extends({
            logMonitorPageLoad: logMonitorPageLoad,
            query: client.query,
            setBreadcrumbs: setBreadcrumbs
          }, routerProps));
        }
      })))))));
    }
  })));
};

var UptimeApp = function UptimeApp(props) {
  return _react.default.createElement(Application, props);
};

exports.UptimeApp = UptimeApp;