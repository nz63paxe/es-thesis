"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "getTelemetryMonitorPageLogger", {
  enumerable: true,
  get: function get() {
    return _log_monitor.getTelemetryMonitorPageLogger;
  }
});
Object.defineProperty(exports, "getTelemetryOverviewPageLogger", {
  enumerable: true,
  get: function get() {
    return _log_overview.getTelemetryOverviewPageLogger;
  }
});

var _log_monitor = require("./log_monitor");

var _log_overview = require("./log_overview");