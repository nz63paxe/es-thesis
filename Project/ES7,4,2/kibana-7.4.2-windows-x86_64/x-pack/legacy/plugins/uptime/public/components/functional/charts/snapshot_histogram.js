"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SnapshotHistogram = exports.SnapshotHistogramComponent = void 0;

var _charts = require("@elastic/charts");

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _get_colors_map = require("./get_colors_map");

var _helper = require("../../../lib/helper");

var _higher_order = require("../../higher_order");

var _snapshot_histogram_query = require("../../../queries/snapshot_histogram_query");

var _chart_wrapper = require("./chart_wrapper");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SnapshotHistogramComponent = function SnapshotHistogramComponent(_ref) {
  var absoluteStartDate = _ref.absoluteStartDate,
      absoluteEndDate = _ref.absoluteEndDate,
      dangerColor = _ref.dangerColor,
      successColor = _ref.successColor,
      data = _ref.data,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? false : _ref$loading,
      height = _ref.height;
  if (!data || !data.histogram)
    /**
     * TODO: the Fragment, EuiTitle, and EuiPanel should be extractec to a dumb component
     * that we can reuse in the subsequent return statement at the bottom of this function.
     */
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("h5", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.uptime.snapshot.pingsOverTimeTitle",
      defaultMessage: "Pings over time"
    }))), _react.default.createElement(_eui.EuiPanel, {
      paddingSize: "s",
      style: {
        height: 170
      }
    }, _react.default.createElement(_eui.EuiEmptyPrompt, {
      title: _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h5", null, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.uptime.snapshot.noDataTitle",
        defaultMessage: "No histogram data available"
      }))),
      body: _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.uptime.snapshot.noDataDescription",
        defaultMessage: "Sorry, there is no data available for the histogram"
      }))
    })));
  var histogram = data.histogram;

  var downMonitorsName = _i18n.i18n.translate('xpack.uptime.snapshotHistogram.downMonitorsId', {
    defaultMessage: 'Down Monitors'
  });

  var downSpecId = (0, _charts.getSpecId)(downMonitorsName);

  var upMonitorsId = _i18n.i18n.translate('xpack.uptime.snapshotHistogram.series.upLabel', {
    defaultMessage: 'Up'
  });

  var upSpecId = (0, _charts.getSpecId)(upMonitorsId);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiPanel, {
    paddingSize: "m"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h5", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.uptime.snapshot.pingsOverTimeTitle",
    defaultMessage: "Pings over time"
  }))), _react.default.createElement(_chart_wrapper.ChartWrapper, {
    height: height,
    loading: loading
  }, _react.default.createElement(_charts.Chart, null, _react.default.createElement(_charts.Settings, {
    xDomain: {
      min: absoluteStartDate,
      max: absoluteEndDate
    },
    showLegend: false
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)(_i18n.i18n.translate('xpack.uptime.snapshotHistogram.xAxisId', {
      defaultMessage: 'Snapshot X Axis'
    })),
    position: _charts.Position.Bottom,
    showOverlappingTicks: false,
    tickFormat: (0, _charts.timeFormatter)((0, _helper.getChartDateLabel)(absoluteStartDate, absoluteEndDate))
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)(_i18n.i18n.translate('xpack.uptime.snapshotHistogram.yAxisId', {
      defaultMessage: 'Snapshot Y Axis'
    })),
    position: "left",
    title: _i18n.i18n.translate('xpack.uptime.snapshotHistogram.yAxis.title', {
      defaultMessage: 'Pings',
      description: 'The label on the y-axis of a chart that displays the number of times Heartbeat has pinged a set of services/websites.'
    })
  }), _react.default.createElement(_charts.BarSeries, {
    customSeriesColors: (0, _get_colors_map.getColorsMap)(successColor, upSpecId),
    data: histogram.map(function (_ref2) {
      var x = _ref2.x,
          upCount = _ref2.upCount;
      return [x, upCount || 0];
    }),
    id: upSpecId,
    name: upMonitorsId,
    stackAccessors: [0],
    timeZone: "local",
    xAccessor: 0,
    xScaleType: "time",
    yAccessors: [1],
    yScaleType: "linear"
  }), _react.default.createElement(_charts.BarSeries, {
    customSeriesColors: (0, _get_colors_map.getColorsMap)(dangerColor, downSpecId),
    data: histogram.map(function (_ref3) {
      var x = _ref3.x,
          downCount = _ref3.downCount;
      return [x, downCount || 0];
    }),
    id: downSpecId,
    name: _i18n.i18n.translate('xpack.uptime.snapshotHistogram.series.downLabel', {
      defaultMessage: 'Down'
    }),
    stackAccessors: [0],
    timeZone: "local",
    xAccessor: 0,
    xScaleType: "time",
    yAccessors: [1],
    yScaleType: "linear"
  })))));
};

exports.SnapshotHistogramComponent = SnapshotHistogramComponent;
var SnapshotHistogram = (0, _higher_order.withUptimeGraphQL)(SnapshotHistogramComponent, _snapshot_histogram_query.snapshotHistogramQuery);
exports.SnapshotHistogram = SnapshotHistogram;