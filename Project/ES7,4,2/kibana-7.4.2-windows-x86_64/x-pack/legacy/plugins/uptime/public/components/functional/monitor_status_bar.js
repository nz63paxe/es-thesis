"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorStatusBar = exports.MonitorStatusBarComponent = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _lodash = require("lodash");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _empty_status_bar = require("./empty_status_bar");

var _helper = require("../../lib/helper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MonitorStatusBarComponent = function MonitorStatusBarComponent(_ref) {
  var data = _ref.data,
      monitorId = _ref.monitorId;

  if (data && data.monitorStatus && data.monitorStatus.length) {
    var _data$monitorStatus$ = data.monitorStatus[0],
        monitor = _data$monitorStatus$.monitor,
        timestamp = _data$monitorStatus$.timestamp;
    var duration = (0, _lodash.get)(monitor, 'duration.us', undefined);
    var status = (0, _lodash.get)(monitor, 'status', 'down');
    var full = (0, _lodash.get)(data.monitorStatus[0], 'url.full');
    return _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiFlexGroup, {
      gutterSize: "l"
    }, _react2.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react2.default.createElement(_eui.EuiHealth, {
      "aria-label": _i18n.i18n.translate('xpack.uptime.monitorStatusBar.healthStatusMessageAriaLabel', {
        defaultMessage: 'Monitor status'
      }),
      color: status === 'up' ? 'success' : 'danger',
      style: {
        lineHeight: 'inherit'
      }
    }, status === 'up' ? _i18n.i18n.translate('xpack.uptime.monitorStatusBar.healthStatusMessage.upLabel', {
      defaultMessage: 'Up'
    }) : _i18n.i18n.translate('xpack.uptime.monitorStatusBar.healthStatusMessage.downLabel', {
      defaultMessage: 'Down'
    }))), _react2.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react2.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react2.default.createElement(_eui.EuiLink, {
      "aria-label": _i18n.i18n.translate('xpack.uptime.monitorStatusBar.monitorUrlLinkAriaLabel', {
        defaultMessage: 'Monitor URL link'
      }),
      href: full,
      target: "_blank"
    }, full))), !!duration && _react2.default.createElement(_eui.EuiFlexItem, {
      "aria-label": _i18n.i18n.translate('xpack.uptime.monitorStatusBar.durationTextAriaLabel', {
        defaultMessage: 'Monitor duration in milliseconds'
      }),
      grow: false
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.uptime.monitorStatusBar.healthStatus.durationInMillisecondsMessage",
      values: {
        duration: (0, _helper.convertMicrosecondsToMilliseconds)(duration)
      },
      defaultMessage: "{duration}ms",
      description: "The 'ms' is an abbreviation for 'milliseconds'."
    })), _react2.default.createElement(_eui.EuiFlexItem, {
      "aria-label": _i18n.i18n.translate('xpack.uptime.monitorStatusBar.timestampFromNowTextAriaLabel', {
        defaultMessage: 'Time since last check'
      }),
      grow: false
    }, (0, _moment.default)(new Date(timestamp).valueOf()).fromNow())));
  }

  return _react2.default.createElement(_empty_status_bar.EmptyStatusBar, {
    message: _i18n.i18n.translate('xpack.uptime.monitorStatusBar.loadingMessage', {
      defaultMessage: 'Loading…'
    }),
    monitorId: monitorId
  });
};

exports.MonitorStatusBarComponent = MonitorStatusBarComponent;
var MonitorStatusBar = (0, _higher_order.withUptimeGraphQL)(MonitorStatusBarComponent, _queries.monitorStatusBarQuery);
exports.MonitorStatusBar = MonitorStatusBar;