"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.monitorPageTitleQuery = exports.monitorPageTitleQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var monitorPageTitleQueryString = "\nquery MonitorPageTitle($monitorId: String!) {\n  monitorPageTitle: getMonitorPageTitle(monitorId: $monitorId) {\n    id\n    url\n    name\n  }\n}";
exports.monitorPageTitleQueryString = monitorPageTitleQueryString;
var monitorPageTitleQuery = (0, _graphqlTag.default)(_templateObject(), monitorPageTitleQueryString);
exports.monitorPageTitleQuery = monitorPageTitleQuery;