"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMonitorPageBreadcrumb = exports.getOverviewPageBreadcrumbs = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var makeOverviewBreadcrumb = function makeOverviewBreadcrumb(search) {
  return {
    text: _i18n.i18n.translate('xpack.uptime.breadcrumbs.overviewBreadcrumbText', {
      defaultMessage: 'Uptime'
    }),
    href: "#/".concat(search ? search : '')
  };
};

var getOverviewPageBreadcrumbs = function getOverviewPageBreadcrumbs(search) {
  return [makeOverviewBreadcrumb(search)];
};

exports.getOverviewPageBreadcrumbs = getOverviewPageBreadcrumbs;

var getMonitorPageBreadcrumb = function getMonitorPageBreadcrumb(name, search) {
  return [makeOverviewBreadcrumb(search), {
    text: name
  }];
};

exports.getMonitorPageBreadcrumb = getMonitorPageBreadcrumb;