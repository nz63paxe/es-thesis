"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorBarSeries = void 0;

var _charts = require("@elastic/charts");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _get_colors_map = require("./get_colors_map");

var _helper = require("../../../lib/helper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * There is a specific focus on the monitor's down count, the up series is not shown,
 * so we will only render the series component if there are down counts for the selected monitor.
 * @param props - the values for the monitor this chart visualizes
 */
var MonitorBarSeries = function MonitorBarSeries(_ref) {
  var absoluteStartDate = _ref.absoluteStartDate,
      absoluteEndDate = _ref.absoluteEndDate,
      dangerColor = _ref.dangerColor,
      histogramSeries = _ref.histogramSeries;
  var id = (0, _charts.getSpecId)('downSeries');
  return (0, _helper.seriesHasDownValues)(histogramSeries) ? _react.default.createElement("div", {
    style: {
      height: 50,
      width: '100%'
    }
  }, _react.default.createElement(_charts.Chart, null, _react.default.createElement(_charts.Settings, {
    xDomain: {
      min: absoluteStartDate,
      max: absoluteEndDate
    }
  }), _react.default.createElement(_charts.Axis, {
    hide: true,
    id: (0, _charts.getAxisId)('bottom'),
    position: _charts.Position.Bottom,
    tickFormat: (0, _charts.timeFormatter)((0, _helper.getChartDateLabel)(absoluteStartDate, absoluteEndDate))
  }), _react.default.createElement(_charts.BarSeries, {
    customSeriesColors: (0, _get_colors_map.getColorsMap)(dangerColor, id),
    data: (histogramSeries || []).map(function (_ref2) {
      var timestamp = _ref2.timestamp,
          down = _ref2.down;
      return [timestamp, down];
    }),
    id: id,
    name: _i18n.i18n.translate('xpack.uptime.monitorList.downLineSeries.downLabel', {
      defaultMessage: 'Down checks'
    }),
    timeZone: "local",
    xAccessor: 0,
    xScaleType: _charts.ScaleType.Time,
    yAccessors: [1],
    yScaleType: _charts.ScaleType.Linear
  }))) : null;
};

exports.MonitorBarSeries = MonitorBarSeries;