"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.monitorChartsQuery = exports.monitorChartsQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var monitorChartsQueryString = "\nquery MonitorCharts($dateRangeStart: String!, $dateRangeEnd: String!, $monitorId: String!, $location: String) {\n  monitorChartsData: getMonitorChartsData(\n    monitorId: $monitorId\n    dateRangeStart: $dateRangeStart\n    dateRangeEnd: $dateRangeEnd\n    location: $location\n  ) {\n    locationDurationLines {\n      name \n      line {\n        x\n        y\n      }\n    }\n    status {\n      x\n      up\n      down\n      total\n    }\n    statusMaxCount\n    durationMaxValue\n  }\n}\n";
exports.monitorChartsQueryString = monitorChartsQueryString;
var monitorChartsQuery = (0, _graphqlTag.default)(_templateObject(), monitorChartsQueryString);
exports.monitorChartsQuery = monitorChartsQuery;