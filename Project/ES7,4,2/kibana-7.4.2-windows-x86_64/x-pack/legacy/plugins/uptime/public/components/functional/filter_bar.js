"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FilterBar = exports.FilterBarComponent = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _filter_bar_loading = require("./filter_bar_loading");

var _search_schema = require("./search_schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SEARCH_THRESHOLD = 2;

var FilterBarComponent = function FilterBarComponent(_ref) {
  var currentQuery = _ref.currentQuery,
      data = _ref.data,
      error = _ref.error,
      updateQuery = _ref.updateQuery;

  if (!data || !data.filterBar) {
    return _react2.default.createElement(_filter_bar_loading.FilterBarLoading, null);
  }

  var _data$filterBar = data.filterBar,
      ids = _data$filterBar.ids,
      locations = _data$filterBar.locations,
      names = _data$filterBar.names,
      ports = _data$filterBar.ports,
      schemes = _data$filterBar.schemes,
      urls = _data$filterBar.urls; // TODO: add a factory function + type for these filter options

  var filters = [{
    type: 'field_value_toggle_group',
    field: 'monitor.status',
    items: [{
      value: 'up',
      name: _i18n.i18n.translate('xpack.uptime.filterBar.filterUpLabel', {
        defaultMessage: 'Up'
      })
    }, {
      value: 'down',
      name: _i18n.i18n.translate('xpack.uptime.filterBar.filterDownLabel', {
        defaultMessage: 'Down'
      })
    }]
  }, {
    type: 'field_value_selection',
    field: 'observer.geo.name',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.location.name', {
      defaultMessage: 'Location',
      description: 'A label applied to a button that lets users filter monitors by their location.'
    }),
    options: locations ? locations.map(function (location) {
      return {
        value: location,
        view: location
      };
    }) : []
  }, {
    type: 'field_value_selection',
    field: 'monitor.id',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.idLabel', {
      defaultMessage: 'ID'
    }),
    multiSelect: false,
    options: ids ? ids.map(function (id) {
      return {
        value: id,
        view: id
      };
    }) : [],
    searchThreshold: SEARCH_THRESHOLD
  }, {
    type: 'field_value_selection',
    field: 'monitor.name',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.nameLabel', {
      defaultMessage: 'Name'
    }),
    multiSelect: false,
    options: names ? names.map(function (nameValue) {
      return {
        value: nameValue,
        view: nameValue
      };
    }) : [],
    searchThreshold: SEARCH_THRESHOLD
  }, {
    type: 'field_value_selection',
    field: 'url.full',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.urlLabel', {
      defaultMessage: 'URL'
    }),
    multiSelect: false,
    options: urls ? urls.map(function (url) {
      return {
        value: url,
        view: url
      };
    }) : [],
    searchThreshold: SEARCH_THRESHOLD
  }, {
    type: 'field_value_selection',
    field: 'url.port',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.portLabel', {
      defaultMessage: 'Port'
    }),
    multiSelect: false,
    options: ports ? ports.map(function (portValue) {
      return {
        value: portValue,
        view: portValue
      };
    }) : [],
    searchThreshold: SEARCH_THRESHOLD
  }, {
    type: 'field_value_selection',
    field: 'monitor.type',
    name: _i18n.i18n.translate('xpack.uptime.filterBar.options.schemeLabel', {
      defaultMessage: 'Scheme'
    }),
    multiSelect: false,
    options: schemes ? schemes.map(function (schemeValue) {
      return {
        value: schemeValue,
        view: schemeValue
      };
    }) : [],
    searchThreshold: SEARCH_THRESHOLD
  }];
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column",
    gutterSize: "none"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement("div", {
    "data-test-subj": "xpack.uptime.filterBar"
  }, _react2.default.createElement(_eui.EuiSearchBar, {
    box: {
      incremental: false,
      placeholder: currentQuery
    },
    className: "euiFlexGroup--gutterSmall",
    onChange: updateQuery,
    filters: filters,
    query: error || currentQuery === '' ? undefined : currentQuery,
    schema: _search_schema.filterBarSearchSchema
  }))), !!error && _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiCallOut, {
    title: error.name || '',
    color: "danger",
    iconType: "cross"
  }, _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.filterBar.errorCalloutMessage",
    defaultMessage: "{codeBlock} cannot be parsed",
    values: {
      codeBlock: _react2.default.createElement(_eui.EuiCode, null, currentQuery)
    }
  }))), !!error.message && _react2.default.createElement(_eui.EuiFlexItem, null, error.message)))));
};

exports.FilterBarComponent = FilterBarComponent;
var FilterBar = (0, _higher_order.withUptimeGraphQL)(FilterBarComponent, _queries.filterBarQuery);
exports.FilterBar = FilterBar;