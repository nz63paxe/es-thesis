"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorPageTitle = exports.MonitorPageTitleComponent = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MonitorPageTitleComponent = function MonitorPageTitleComponent(_ref) {
  var data = _ref.data;
  return data && data.monitorPageTitle ? _react.default.createElement(_eui.EuiTitle, {
    size: "xxs"
  }, _react.default.createElement(_eui.EuiTextColor, {
    color: "subdued"
  }, _react.default.createElement("h1", {
    "data-test-subj": "monitor-page-title"
  }, data.monitorPageTitle.id))) : _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: "xl"
  });
};

exports.MonitorPageTitleComponent = MonitorPageTitleComponent;
var MonitorPageTitle = (0, _higher_order.withUptimeGraphQL)(MonitorPageTitleComponent, _queries.monitorPageTitleQuery);
exports.MonitorPageTitle = MonitorPageTitle;