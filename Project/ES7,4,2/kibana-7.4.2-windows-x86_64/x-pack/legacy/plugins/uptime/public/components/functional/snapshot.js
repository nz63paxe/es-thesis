"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Snapshot = exports.SnapshotComponent = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _snapshot_loading = require("./snapshot_loading");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * This component visualizes a KPI and histogram chart to help users quickly
 * glean the status of their uptime environment.
 * @param props the props required by the component
 */
var SnapshotComponent = function SnapshotComponent(_ref) {
  var data = _ref.data;
  return data && data.snapshot ? _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react2.default.createElement("h5", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.snapshot.endpointStatusTitle",
    defaultMessage: "Current status"
  }))), _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column",
    gutterSize: "m"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiSpacer, {
    size: "xs"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceEvenly",
    gutterSize: "s"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.upDescription', {
      defaultMessage: 'Up'
    }),
    textAlign: "center",
    title: data.snapshot.counts.up,
    titleColor: "secondary"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.downDescription', {
      defaultMessage: 'Down'
    }),
    textAlign: "center",
    title: data.snapshot.counts.down,
    titleColor: "danger"
  })), data.snapshot.counts.mixed > 0 ? _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.mixedDescription', {
      defaultMessage: 'Mixed'
    }),
    textAlign: "center",
    title: data.snapshot.counts.mixed,
    titleColor: "subdued"
  })) : null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiStat, {
    description: _i18n.i18n.translate('xpack.uptime.snapshot.stats.totalDescription', {
      defaultMessage: 'Total'
    }),
    textAlign: "center",
    title: data.snapshot.counts.total,
    titleColor: "subdued"
  }))))))) : _react2.default.createElement(_snapshot_loading.SnapshotLoading, null);
};
/**
 * This component visualizes a KPI and histogram chart to help users quickly
 * glean the status of their uptime environment.
 */


exports.SnapshotComponent = SnapshotComponent;
var Snapshot = (0, _higher_order.withUptimeGraphQL)(SnapshotComponent, _queries.snapshotQuery);
exports.Snapshot = Snapshot;