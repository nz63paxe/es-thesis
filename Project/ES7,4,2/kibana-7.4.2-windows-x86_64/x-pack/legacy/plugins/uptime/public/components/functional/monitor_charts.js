"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorCharts = exports.MonitorChartsComponent = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _charts = require("./charts");

var _contexts = require("../../contexts");

var _snapshot_histogram = require("./charts/snapshot_histogram");

var _hooks = require("../../hooks");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var MonitorChartsComponent = function MonitorChartsComponent(_ref) {
  var data = _ref.data,
      mean = _ref.mean,
      range = _ref.range,
      monitorId = _ref.monitorId,
      dateRangeStart = _ref.dateRangeStart,
      dateRangeEnd = _ref.dateRangeEnd,
      loading = _ref.loading;

  if (data && data.monitorChartsData) {
    var locationDurationLines = data.monitorChartsData.locationDurationLines;

    var _useContext = (0, _react.useContext)(_contexts.UptimeSettingsContext),
        colors = _useContext.colors;

    var _useUrlParams = (0, _hooks.useUrlParams)(),
        _useUrlParams2 = _slicedToArray(_useUrlParams, 1),
        getUrlParams = _useUrlParams2[0];

    var _getUrlParams = getUrlParams(),
        absoluteDateRangeStart = _getUrlParams.absoluteDateRangeStart,
        absoluteDateRangeEnd = _getUrlParams.absoluteDateRangeEnd;

    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_charts.DurationChart, {
      locationDurationLines: locationDurationLines,
      meanColor: mean,
      rangeColor: range,
      loading: loading
    })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_snapshot_histogram.SnapshotHistogram, {
      absoluteStartDate: absoluteDateRangeStart,
      absoluteEndDate: absoluteDateRangeEnd,
      successColor: colors.success,
      dangerColor: colors.danger,
      variables: {
        dateRangeStart: dateRangeStart,
        dateRangeEnd: dateRangeEnd,
        monitorId: monitorId
      },
      height: "400px"
    }))));
  }

  return _react.default.createElement(_react.Fragment, null, _i18n.i18n.translate('xpack.uptime.monitorCharts.loadingMessage', {
    defaultMessage: 'Loading…'
  }));
};

exports.MonitorChartsComponent = MonitorChartsComponent;
var MonitorCharts = (0, _higher_order.withUptimeGraphQL)(MonitorChartsComponent, _queries.monitorChartsQuery);
exports.MonitorCharts = MonitorCharts;