"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UptimeRefreshContext = void 0;

var _react = require("react");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var defaultContext = {
  lastRefresh: 0,
  history: undefined,
  location: undefined
};
var UptimeRefreshContext = (0, _react.createContext)(defaultContext);
exports.UptimeRefreshContext = UptimeRefreshContext;