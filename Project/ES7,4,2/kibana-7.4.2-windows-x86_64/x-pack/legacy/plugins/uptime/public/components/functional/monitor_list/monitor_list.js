"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorList = exports.MonitorListComponent = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _react2 = _interopRequireWildcard(require("react"));

var _higher_order = require("../../higher_order");

var _monitor_states_query = require("../../../queries/monitor_states_query");

var _monitor_list_status_column = require("./monitor_list_status_column");

var _format_error_list = require("../../../lib/helper/format_error_list");

var _monitor_list_drawer = require("./monitor_list_drawer");

var _constants = require("../../../../common/constants");

var _charts = require("../charts");

var _monitor_page_link = require("../monitor_page_link");

var _monitor_list_actions_popover = require("./monitor_list_actions_popover");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var MonitorListComponent = function MonitorListComponent(props) {
  var absoluteStartDate = props.absoluteStartDate,
      absoluteEndDate = props.absoluteEndDate,
      dangerColor = props.dangerColor,
      successColor = props.successColor,
      data = props.data,
      errors = props.errors,
      linkParameters = props.linkParameters,
      loading = props.loading;

  var _useState = (0, _react2.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      drawerIds = _useState2[0],
      updateDrawerIds = _useState2[1];

  var items = (0, _lodash.get)(data, 'monitorStates.summaries', []); // TODO: use with pagination
  // const count = get<number>(data, 'monitorStates.totalSummaryCount.count', 0);
  // TODO: reintegrate pagination in future release
  // const pagination: Pagination = {
  //   pageIndex,
  //   pageSize,
  //   pageSizeOptions: [5, 10, 20, 50],
  //   totalItemCount: count,
  //   hidePerPageOptions: false,
  // };
  // TODO: reintegrate sorting in future release
  // const sorting = {
  //   sort: {
  //     field: sortField,
  //     direction: sortDirection,
  //   },
  // };

  return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react2.default.createElement("h5", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.monitorList.monitoringStatusTitle",
    defaultMessage: "Monitor status"
  }))), _react2.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react2.default.createElement(_eui.EuiBasicTable, {
    error: errors ? (0, _format_error_list.formatUptimeGraphQLErrorList)(errors) : errors // Only set loading to true when there are no items present to prevent the bug outlined in
    // in https://github.com/elastic/eui/issues/2393 . Once that is fixed we can simply set the value here to
    // loading={loading}
    ,
    loading: loading && (!items || items.length < 1),
    isExpandable: true,
    hasActions: true,
    itemId: "monitor_id",
    itemIdToExpandedRowMap: drawerIds.reduce(function (map, id) {
      return _objectSpread({}, map, _defineProperty({}, id, _react2.default.createElement(_monitor_list_drawer.MonitorListDrawer, {
        condensedCheckLimit: _constants.CLIENT_DEFAULTS.CONDENSED_CHECK_LIMIT,
        summary: items ? items.find(function (_ref) {
          var monitorId = _ref.monitor_id;
          return monitorId === id;
        }) : undefined,
        successColor: successColor,
        dangerColor: dangerColor
      })));
    }, {}),
    items: items // TODO: not needed without sorting and pagination
    // onChange={onChange}
    ,
    noItemsMessage: _i18n.i18n.translate('xpack.uptime.monitorList.noItemMessage', {
      defaultMessage: 'No uptime monitors found',
      description: 'This message is shown if the monitors table is rendered but has no items.'
    }) // TODO: reintegrate pagination in future release
    // pagination={pagination}
    // TODO: reintegrate sorting in future release
    // sorting={sorting}
    ,
    columns: [{
      align: 'left',
      field: 'state.monitor.status',
      name: _i18n.i18n.translate('xpack.uptime.monitorList.statusColumnLabel', {
        defaultMessage: 'Status'
      }),
      render: function render(status, _ref2) {
        var timestamp = _ref2.state.timestamp;
        return _react2.default.createElement(_monitor_list_status_column.MonitorListStatusColumn, {
          status: status,
          timestamp: timestamp
        });
      }
    }, {
      align: 'left',
      field: 'state.monitor.name',
      name: _i18n.i18n.translate('xpack.uptime.monitorList.nameColumnLabel', {
        defaultMessage: 'Name'
      }),
      render: function render(name, summary) {
        return _react2.default.createElement(_monitor_page_link.MonitorPageLink, {
          id: summary.monitor_id,
          linkParameters: linkParameters,
          location: undefined
        }, name ? name : "Unnamed - ".concat(summary.monitor_id));
      },
      sortable: true
    }, {
      align: 'left',
      field: 'state.url.full',
      name: _i18n.i18n.translate('xpack.uptime.monitorList.urlColumnLabel', {
        defaultMessage: 'URL'
      }),
      render: function render(url, summary) {
        return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiLink, {
          href: url,
          target: "_blank",
          color: "text"
        }, url, " ", _react2.default.createElement(_eui.EuiIcon, {
          size: "s",
          type: "popout",
          color: "subbdued"
        })));
      },
      sortable: true
    }, {
      field: 'histogram.points',
      name: _i18n.i18n.translate('xpack.uptime.monitorList.monitorHistoryColumnLabel', {
        defaultMessage: 'Downtime history'
      }),
      mobileOptions: {
        show: false
      },
      render: function render(histogramSeries) {
        return _react2.default.createElement(_charts.MonitorBarSeries, {
          absoluteStartDate: absoluteStartDate,
          absoluteEndDate: absoluteEndDate,
          dangerColor: dangerColor,
          histogramSeries: histogramSeries
        });
      }
    }, {
      id: 'actions',
      align: 'right',
      field: 'state',
      hasActions: true,
      mobileOptions: {
        header: false
      },
      name: _i18n.i18n.translate('xpack.uptime.monitorList.observabilityIntegrationsColumnLabel', {
        defaultMessage: 'Integrations',
        description: 'The heading column of some action buttons that will take users to other Obsevability apps'
      }),
      render: function render(state, summary) {
        return _react2.default.createElement(_monitor_list_actions_popover.MonitorListActionsPopover, {
          summary: summary
        });
      }
    }, {
      align: 'left',
      field: 'monitor_id',
      name: '',
      sortable: true,
      width: '40px',
      isExpander: true,
      render: function render(id) {
        return _react2.default.createElement(_eui.EuiButtonIcon, {
          "aria-label": _i18n.i18n.translate('xpack.uptime.monitorList.expandDrawerButton.ariaLabel', {
            defaultMessage: 'Expand row for monitor with ID {id}',
            description: 'The user can click a button on this table and expand further details.',
            values: {
              id: id
            }
          }),
          iconType: drawerIds.find(function (item) {
            return item === id;
          }) ? 'arrowUp' : 'arrowDown',
          onClick: function onClick() {
            if (drawerIds.find(function (i) {
              return id === i;
            })) {
              updateDrawerIds(drawerIds.filter(function (p) {
                return p !== id;
              }));
            } else {
              updateDrawerIds([].concat(_toConsumableArray(drawerIds), [id]));
            }
          }
        });
      }
    }]
  })));
};

exports.MonitorListComponent = MonitorListComponent;
var MonitorList = (0, _higher_order.withUptimeGraphQL)(MonitorListComponent, _monitor_states_query.monitorStatesQuery);
exports.MonitorList = MonitorList;