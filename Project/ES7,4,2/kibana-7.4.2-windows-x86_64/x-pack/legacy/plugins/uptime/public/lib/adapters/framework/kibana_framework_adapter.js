"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UMKibanaFrameworkAdapter = void 0;

var _reactDom = _interopRequireWildcard(require("react-dom"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _documentation_links = require("ui/documentation_links");

var _constants = require("../../../../common/constants");

var _kibana_global_help = require("./kibana_global_help");

var _telemetry = require("../telemetry");

var _capabilities_adapter = require("./capabilities_adapter");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var UMKibanaFrameworkAdapter = function UMKibanaFrameworkAdapter(uiRoutes) {
  var _this = this;

  _classCallCheck(this, UMKibanaFrameworkAdapter);

  _defineProperty(this, "uiRoutes", void 0);

  _defineProperty(this, "xsrfHeader", void 0);

  _defineProperty(this, "uriPath", void 0);

  _defineProperty(this, "render", function (renderComponent, createGraphQLClient) {
    var route = {
      controllerAs: 'uptime',
      // @ts-ignore angular
      controller: function controller($scope, $route, config, $location, $window) {
        var graphQLClient = createGraphQLClient(_this.uriPath, _this.xsrfHeader);
        $scope.$$postDigest(function () {
          var elem = document.getElementById('uptimeReactRoot'); // configure breadcrumbs

          var kibanaBreadcrumbs = [];

          _chrome.default.breadcrumbs.get$().subscribe(function (breadcrumbs) {
            kibanaBreadcrumbs = breadcrumbs;
          }); // set up route with current base path


          var basePath = _chrome.default.getBasePath();

          var routerBasename = basePath.endsWith('/') ? "".concat(basePath, "/").concat(_constants.PLUGIN.ROUTER_BASE_NAME) : basePath + _constants.PLUGIN.ROUTER_BASE_NAME;
          /**
           * TODO: this is a redirect hack to deal with a problem that largely
           * in testing but rarely occurs in the real world, where the specified
           * URL contains `.../app/uptime{SOME_URL_PARAM_TEXT}#` instead of
           * a path like `.../app/uptime#{SOME_URL_PARAM_TEXT}`.
           *
           * This redirect will almost never be triggered in practice, but it makes more
           * sense to include it here rather than altering the existing testing
           * infrastructure underlying the rest of Kibana.
           *
           * We welcome a more permanent solution that will result in the deletion of the
           * block below.
           */

          if ($location.absUrl().indexOf(_constants.PLUGIN.ROUTER_BASE_NAME) === -1) {
            $window.location.replace(routerBasename);
          } // determine whether dark mode is enabled


          var darkMode = config.get('theme:darkMode', false) || false;
          /**
           * We pass this global help setup as a prop to the app, because for
           * localization it's necessary to have the provider mounted before
           * we can render our help links, as they rely on i18n.
           */

          var renderGlobalHelpControls = function renderGlobalHelpControls() {
            return (// render Uptime feedback link in global help menu
              _chrome.default.helpExtension.set(function (element) {
                _reactDom.default.render((0, _kibana_global_help.renderUptimeKibanaGlobalHelp)(_documentation_links.ELASTIC_WEBSITE_URL, _documentation_links.DOC_LINK_VERSION), element);

                return function () {
                  return _reactDom.default.unmountComponentAtNode(element);
                };
              })
            );
          };
          /**
           * These values will let Uptime know if the integrated solutions
           * are available. If any/all of them are unavaialble, we should not show
           * links/integrations to those apps.
           */


          var _getIntegratedAppAvai = (0, _capabilities_adapter.getIntegratedAppAvailability)(_constants.INTEGRATED_SOLUTIONS),
              isApmAvailable = _getIntegratedAppAvai.apm,
              isInfraAvailable = _getIntegratedAppAvai.infrastructure,
              isLogsAvailable = _getIntegratedAppAvai.logs;

          _reactDom.default.render(renderComponent({
            basePath: basePath,
            client: graphQLClient,
            darkMode: darkMode,
            isApmAvailable: isApmAvailable,
            isInfraAvailable: isInfraAvailable,
            isLogsAvailable: isLogsAvailable,
            kibanaBreadcrumbs: kibanaBreadcrumbs,
            logMonitorPageLoad: (0, _telemetry.getTelemetryMonitorPageLogger)(_this.xsrfHeader, basePath),
            logOverviewPageLoad: (0, _telemetry.getTelemetryOverviewPageLogger)(_this.xsrfHeader, basePath),
            renderGlobalHelpControls: renderGlobalHelpControls,
            routerBasename: routerBasename,
            setBadge: _chrome.default.badge.set,
            setBreadcrumbs: _chrome.default.breadcrumbs.set
          }), elem);

          _this.manageAngularLifecycle($scope, $route, elem);
        });
      },
      template: '<uptime-app section="kibana" id="uptimeReactRoot" class="app-wrapper-panel"></uptime-app>'
    };

    _this.uiRoutes.enable(); // TODO: hack to refer all routes to same endpoint, use a more proper way of achieving this


    _this.uiRoutes.otherwise(route);
  });

  _defineProperty(this, "manageAngularLifecycle", function ($scope, $route, elem) {
    var lastRoute = $route.current;
    var deregister = $scope.$on('$locationChangeSuccess', function () {
      var currentRoute = $route.current;

      if (lastRoute.$$route && lastRoute.$$route.template === currentRoute.$$route.template) {
        $route.current = lastRoute;
      }
    });
    $scope.$on('$destroy', function () {
      deregister();
      (0, _reactDom.unmountComponentAtNode)(elem);
    });
  });

  this.uiRoutes = uiRoutes;
  this.xsrfHeader = _chrome.default.getXsrfToken();
  this.uriPath = "".concat(_chrome.default.getBasePath(), "/api/uptime/graphql");
}
/**
 * This function will acquire all the existing data from Kibana
 * services and persisted state expected by the plugin's props
 * interface. It then renders the plugin.
 */
;

exports.UMKibanaFrameworkAdapter = UMKibanaFrameworkAdapter;