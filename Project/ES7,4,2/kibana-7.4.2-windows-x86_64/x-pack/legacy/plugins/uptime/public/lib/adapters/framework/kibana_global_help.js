"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderUptimeKibanaGlobalHelp = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var renderUptimeKibanaGlobalHelp = function renderUptimeKibanaGlobalHelp(docsSiteUrl, docLinkVersion) {
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiLink, {
    "aria-label": _i18n.i18n.translate('xpack.uptime.header.docsLinkAriaLabel', {
      defaultMessage: 'Go to Uptime documentation'
    }),
    href: "".concat(docsSiteUrl, "guide/en/kibana/").concat(docLinkVersion, "/xpack-uptime.html"),
    target: "_blank"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.header.documentationLinkText",
    defaultMessage: "Uptime Docs",
    description: "The link will navigate users to the Uptime UI documentation pages."
  }))), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiLink, {
    "aria-label": _i18n.i18n.translate('xpack.uptime.header.helpLinkAriaLabel', {
      defaultMessage: 'Go to our discuss page'
    }),
    href: "https://discuss.elastic.co/c/uptime",
    target: "_blank"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.header.helpLinkText",
    defaultMessage: "Give Uptime feedback",
    description: "The link is to a support form called 'Discuss', where users can submit feedback."
  }))));
};

exports.renderUptimeKibanaGlobalHelp = renderUptimeKibanaGlobalHelp;