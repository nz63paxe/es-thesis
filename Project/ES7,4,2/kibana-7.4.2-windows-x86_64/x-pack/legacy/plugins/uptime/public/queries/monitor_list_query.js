"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.monitorListQuery = exports.monitorListQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var monitorListQueryString = "\n  query MonitorList($dateRangeStart: String!, $dateRangeEnd: String!, $filters: String) {\n    monitorStatus: getMonitors(\n      dateRangeStart: $dateRangeStart\n      dateRangeEnd: $dateRangeEnd\n      filters: $filters\n    ) {\n      monitors {\n        id {\n          key\n          url\n        }\n        ping {\n          timestamp\n          container {\n            id\n          }\n          kubernetes {\n            pod {\n              uid\n            }\n          }\n          monitor {\n            duration {\n              us\n            }\n            id\n            ip\n            name\n            status\n          }\n          observer {\n            geo {\n              location\n              name\n            }\n          }\n          url {\n            domain\n            full\n          }\n        }\n        upSeries {\n          x\n          y\n        }\n        downSeries {\n          x\n          y\n        }\n      }\n    }\n  }\n";
exports.monitorListQueryString = monitorListQueryString;
var monitorListQuery = (0, _graphqlTag.default)(_templateObject(), monitorListQueryString);
exports.monitorListQuery = monitorListQuery;