"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PingList = exports.PingListComponent = exports.toggleDetails = exports.BaseLocationOptions = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _lodash = require("lodash");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireWildcard(require("react"));

var _helper = require("../../lib/helper");

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _location_name = require("./location_name");

var _expanded_row = require("./ping_list/expanded_row");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var BaseLocationOptions = [{
  label: 'All',
  value: 'All'
}];
exports.BaseLocationOptions = BaseLocationOptions;

var toggleDetails = function toggleDetails(ping, itemIdToExpandedRowMap, setItemIdToExpandedRowMap) {
  // If the user has clicked on the expanded map, close all expanded rows.
  if (itemIdToExpandedRowMap[ping.id]) {
    setItemIdToExpandedRowMap({});
    return;
  } // Otherwise expand this row


  var newItemIdToExpandedRowMap = {};
  newItemIdToExpandedRowMap[ping.id] = _react2.default.createElement(_expanded_row.PingListExpandedRowComponent, {
    ping: ping
  });
  setItemIdToExpandedRowMap(newItemIdToExpandedRowMap);
};

exports.toggleDetails = toggleDetails;

var PingListComponent = function PingListComponent(_ref) {
  var data = _ref.data,
      loading = _ref.loading,
      onPageCountChange = _ref.onPageCountChange,
      onSelectedLocationChange = _ref.onSelectedLocationChange,
      onSelectedStatusChange = _ref.onSelectedStatusChange,
      onUpdateApp = _ref.onUpdateApp,
      pageSize = _ref.pageSize,
      selectedOption = _ref.selectedOption,
      selectedLocation = _ref.selectedLocation;

  var _useState = (0, _react2.useState)({}),
      _useState2 = _slicedToArray(_useState, 2),
      itemIdToExpandedRowMap = _useState2[0],
      setItemIdToExpandedRowMap = _useState2[1];

  var statusOptions = [{
    label: _i18n.i18n.translate('xpack.uptime.pingList.statusOptions.allStatusOptionLabel', {
      defaultMessage: 'All'
    }),
    value: ''
  }, {
    label: _i18n.i18n.translate('xpack.uptime.pingList.statusOptions.upStatusOptionLabel', {
      defaultMessage: 'Up'
    }),
    value: 'up'
  }, {
    label: _i18n.i18n.translate('xpack.uptime.pingList.statusOptions.downStatusOptionLabel', {
      defaultMessage: 'Down'
    }),
    value: 'down'
  }];
  var locations = (0, _lodash.get)(data, 'allPings.locations');
  var locationOptions = !locations ? BaseLocationOptions : BaseLocationOptions.concat(locations.map(function (name) {
    return {
      label: name,
      value: name
    };
  }));
  var columns = [{
    field: 'monitor.status',
    name: _i18n.i18n.translate('xpack.uptime.pingList.statusColumnLabel', {
      defaultMessage: 'Status'
    }),
    render: function render(pingStatus, item) {
      return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiHealth, {
        color: pingStatus === 'up' ? 'success' : 'danger'
      }, pingStatus === 'up' ? _i18n.i18n.translate('xpack.uptime.pingList.statusColumnHealthUpLabel', {
        defaultMessage: 'Up'
      }) : _i18n.i18n.translate('xpack.uptime.pingList.statusColumnHealthDownLabel', {
        defaultMessage: 'Down'
      })), _react2.default.createElement(_eui.EuiText, {
        size: "xs",
        color: "subdued"
      }, _i18n.i18n.translate('xpack.uptime.pingList.recencyMessage', {
        values: {
          fromNow: (0, _moment.default)(item.timestamp).fromNow()
        },
        defaultMessage: 'Checked {fromNow}',
        description: 'A string used to inform our users how long ago Heartbeat pinged the selected host.'
      })));
    }
  }, {
    align: 'left',
    dataType: 'number',
    field: 'observer.geo.name',
    name: _i18n.i18n.translate('xpack.uptime.pingList.locationNameColumnLabel', {
      defaultMessage: 'Location'
    }),
    render: function render(location) {
      return _react2.default.createElement(_location_name.LocationName, {
        location: location
      });
    }
  }, {
    align: 'left',
    dataType: 'number',
    field: 'monitor.ip',
    name: _i18n.i18n.translate('xpack.uptime.pingList.ipAddressColumnLabel', {
      defaultMessage: 'IP'
    })
  }, {
    align: 'right',
    field: 'monitor.duration.us',
    name: _i18n.i18n.translate('xpack.uptime.pingList.durationMsColumnLabel', {
      defaultMessage: 'Duration'
    }),
    render: function render(duration) {
      return _i18n.i18n.translate('xpack.uptime.pingList.durationMsColumnFormatting', {
        values: {
          millis: (0, _helper.convertMicrosecondsToMilliseconds)(duration)
        },
        defaultMessage: '{millis} ms'
      });
    }
  }, {
    align: 'left',
    field: 'error.type',
    name: _i18n.i18n.translate('xpack.uptime.pingList.errorTypeColumnLabel', {
      defaultMessage: 'Error type'
    })
  }];
  (0, _react2.useEffect)(function () {
    onUpdateApp();
  }, [selectedOption]);
  var pings = [];

  if (data && data.allPings && data.allPings.pings) {
    pings = data.allPings.pings;
    var hasStatus = pings.reduce(function (hasHttpStatus, currentPing) {
      return hasHttpStatus || !!(0, _lodash.get)(currentPing, 'http.response.status_code');
    }, false);

    if (hasStatus) {
      columns.push({
        field: 'http.response.status_code',
        // @ts-ignore "align" property missing on type definition for column type
        align: 'right',
        name: _i18n.i18n.translate('xpack.uptime.pingList.responseCodeColumnLabel', {
          defaultMessage: 'Response code'
        }),
        render: function render(statusCode) {
          return _react2.default.createElement(_eui.EuiBadge, null, statusCode);
        }
      });
    }
  }

  columns.push({
    align: 'right',
    width: '40px',
    isExpander: true,
    render: function render(item) {
      return _react2.default.createElement(_eui.EuiButtonIcon, {
        onClick: function onClick() {
          return toggleDetails(item, itemIdToExpandedRowMap, setItemIdToExpandedRowMap);
        },
        "aria-label": itemIdToExpandedRowMap[item.id] ? _i18n.i18n.translate('xpack.uptime.pingList.collapseRow', {
          defaultMessage: 'Collapse'
        }) : _i18n.i18n.translate('xpack.uptime.pingList.expandRow', {
          defaultMessage: 'Expand'
        }),
        iconType: itemIdToExpandedRowMap[item.id] ? 'arrowUp' : 'arrowDown'
      });
    }
  });
  var pagination = {
    initialPageSize: 20,
    pageIndex: 0,
    pageSize: pageSize,
    pageSizeOptions: [5, 10, 20, 50, 100],

    /**
     * we're not currently supporting pagination in this component
     * so the first page is the only page
     */
    totalItemCount: pageSize
  };
  return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react2.default.createElement("h4", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.pingList.checkHistoryTitle",
    defaultMessage: "History"
  }))), _react2.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, {
    style: {
      minWidth: 200
    }
  }, _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFormRow, {
    label: "Status",
    "aria-label": _i18n.i18n.translate('xpack.uptime.pingList.statusLabel', {
      defaultMessage: 'Status'
    })
  }, _react2.default.createElement(_eui.EuiComboBox, {
    isClearable: false,
    singleSelection: {
      asPlainText: true
    },
    selectedOptions: [statusOptions.find(function (_ref2) {
      var value = _ref2.value;
      return value === selectedOption;
    }) || statusOptions[2]],
    options: statusOptions,
    "aria-label": _i18n.i18n.translate('xpack.uptime.pingList.statusLabel', {
      defaultMessage: 'Status'
    }),
    onChange: function onChange(selectedOptions) {
      if (typeof selectedOptions[0].value === 'string') {
        onSelectedStatusChange( // @ts-ignore it's definitely a string
        selectedOptions[0].value !== '' ? selectedOptions[0].value : null);
      }
    }
  }))), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFormRow, {
    label: "Location",
    "aria-label": _i18n.i18n.translate('xpack.uptime.pingList.locationLabel', {
      defaultMessage: 'Location'
    })
  }, _react2.default.createElement(_eui.EuiComboBox, {
    isClearable: false,
    singleSelection: {
      asPlainText: true
    },
    selectedOptions: selectedLocation,
    options: locationOptions,
    "aria-label": _i18n.i18n.translate('xpack.uptime.pingList.locationLabel', {
      defaultMessage: 'Location'
    }),
    onChange: function onChange(selectedOptions) {
      onSelectedLocationChange(selectedOptions);
    }
  })))))))), _react2.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react2.default.createElement(_eui.EuiBasicTable, {
    loading: loading,
    columns: columns,
    items: pings,
    itemId: "id",
    itemIdToExpandedRowMap: itemIdToExpandedRowMap,
    pagination: pagination,
    onChange: function onChange(_ref3) {
      var size = _ref3.page.size;
      return onPageCountChange(size);
    }
  })));
};

exports.PingListComponent = PingListComponent;
var PingList = (0, _higher_order.withUptimeGraphQL)(PingListComponent, _queries.pingsQuery);
exports.PingList = PingList;