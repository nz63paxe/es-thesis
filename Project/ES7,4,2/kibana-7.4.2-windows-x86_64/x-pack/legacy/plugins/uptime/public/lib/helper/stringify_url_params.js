"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stringifyUrlParams = void 0;

var _querystring = _interopRequireDefault(require("querystring"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var stringifyUrlParams = function stringifyUrlParams(params) {
  return "?".concat(_querystring.default.stringify(params));
};

exports.stringifyUrlParams = stringifyUrlParams;