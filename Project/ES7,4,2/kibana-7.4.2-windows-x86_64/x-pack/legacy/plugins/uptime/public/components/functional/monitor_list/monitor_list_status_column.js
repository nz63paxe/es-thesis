"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorListStatusColumn = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _parse_timestamp = require("./parse_timestamp");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getHealthColor = function getHealthColor(status) {
  switch (status) {
    case 'up':
      return 'success';

    case 'down':
      return 'danger';

    case 'mixed':
      return 'warning';

    default:
      return '';
  }
};

var getHealthMessage = function getHealthMessage(status) {
  switch (status) {
    case 'up':
      return _i18n.i18n.translate('xpack.uptime.monitorList.statusColumn.upLabel', {
        defaultMessage: 'Up'
      });

    case 'down':
      return _i18n.i18n.translate('xpack.uptime.monitorList.statusColumn.downLabel', {
        defaultMessage: 'Down'
      });

    case 'mixed':
      return _i18n.i18n.translate('xpack.uptime.monitorList.statusColumn.mixedLabel', {
        defaultMessage: 'Mixed'
      });

    default:
      return null;
  }
};

var MonitorListStatusColumn = function MonitorListStatusColumn(_ref) {
  var status = _ref.status,
      tsString = _ref.timestamp;
  var timestamp = (0, _parse_timestamp.parseTimestamp)(tsString);
  return _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiHealth, {
    color: getHealthColor(status),
    style: {
      display: 'block'
    }
  }, getHealthMessage(status)), _react.default.createElement(_eui.EuiToolTip, {
    content: _react.default.createElement(_eui.EuiText, {
      color: "ghost",
      size: "xs"
    }, timestamp.toLocaleString())
  }, _react.default.createElement(_eui.EuiText, {
    size: "xs",
    color: "subdued"
  }, timestamp.fromNow()))));
};

exports.MonitorListStatusColumn = MonitorListStatusColumn;