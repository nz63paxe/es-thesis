"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "useUrlParams", {
  enumerable: true,
  get: function get() {
    return _use_url_params.useUrlParams;
  }
});

var _use_url_params = require("./use_url_params");