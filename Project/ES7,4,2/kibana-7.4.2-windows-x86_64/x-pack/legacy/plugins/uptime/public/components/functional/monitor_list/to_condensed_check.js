"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toCondensedCheck = void 0;

var _lodash = require("lodash");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var inferCondensedFields = function inferCondensedFields(check, currentStatus, currentTimestamp) {
  var condensedStatus = check.status,
      timestamp = check.timestamp;

  if (condensedStatus !== currentStatus && condensedStatus !== 'mixed') {
    check.status = 'mixed';
  }

  if (timestamp < currentTimestamp) {
    check.timestamp = currentTimestamp;
  }
};

var toCondensedCheck = function toCondensedCheck(checks) {
  var condensedChecks = new Map();
  checks.forEach(function (check) {
    var location = (0, _lodash.get)(check, 'observer.geo.name', null);
    var _check$monitor = check.monitor,
        ip = _check$monitor.ip,
        status = _check$monitor.status,
        timestamp = check.timestamp;
    var condensedCheck;

    if (condensedCheck = condensedChecks.get(location)) {
      condensedCheck.childStatuses.push({
        ip: ip,
        status: status,
        timestamp: timestamp
      });
      inferCondensedFields(condensedCheck, status, timestamp);
    } else {
      condensedChecks.set(location, {
        childStatuses: [{
          ip: ip,
          status: status,
          timestamp: timestamp
        }],
        location: location,
        status: status,
        timestamp: timestamp
      });
    }
  });
  return Array.from(condensedChecks.values());
};

exports.toCondensedCheck = toCondensedCheck;