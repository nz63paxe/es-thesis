"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.snapshotQuery = exports.snapshotQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var snapshotQueryString = "\nquery Snapshot(\n  $dateRangeStart: String!\n  $dateRangeEnd: String!\n  $filters: String\n) {\n  snapshot: getSnapshot(\n    dateRangeStart: $dateRangeStart\n    dateRangeEnd: $dateRangeEnd\n    filters: $filters\n  ) {\n    counts {\n      down\n      mixed\n      up\n      total\n    }\n  }\n}\n";
exports.snapshotQueryString = snapshotQueryString;
var snapshotQuery = (0, _graphqlTag.default)(_templateObject(), snapshotQueryString);
exports.snapshotQuery = snapshotQuery;