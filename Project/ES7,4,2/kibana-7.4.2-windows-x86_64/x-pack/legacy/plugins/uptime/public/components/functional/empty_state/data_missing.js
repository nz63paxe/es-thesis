"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataMissing = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DataMissing = function DataMissing(_ref) {
  var basePath = _ref.basePath,
      headingMessage = _ref.headingMessage;
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "center"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiSpacer, {
    size: "xs"
  }), _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiEmptyPrompt, {
    iconType: "uptimeApp",
    title: _react2.default.createElement(_eui.EuiTitle, {
      size: "l"
    }, _react2.default.createElement("h3", null, headingMessage)),
    body: _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.uptime.emptyState.configureHeartbeatToGetStartedMessage",
      defaultMessage: "{configureHeartbeatLink} to start collecting uptime data.",
      values: {
        configureHeartbeatLink: _react2.default.createElement(_eui.EuiLink, {
          target: "_blank",
          href: "".concat(basePath, "/app/kibana#/home/tutorial/uptimeMonitors")
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.uptime.emptyState.configureHeartbeatLinkText",
          defaultMessage: "Configure Heartbeat"
        }))
      }
    }))
  }))));
};

exports.DataMissing = DataMissing;