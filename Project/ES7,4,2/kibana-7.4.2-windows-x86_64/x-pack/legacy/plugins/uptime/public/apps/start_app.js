"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startApp = startApp;

require("react-vis/dist/style.css");

require("ui/angular-bootstrap");

require("ui/autoload/all");

require("ui/autoload/styles");

require("ui/courier");

require("ui/persisted_log");

require("uiExports/autocompleteProviders");

var _apollo_client_adapter = require("../lib/adapters/framework/apollo_client_adapter");

var _uptime_app = require("../uptime_app");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function startApp(_x) {
  return _startApp.apply(this, arguments);
}

function _startApp() {
  _startApp = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(libs) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            libs.framework.render(_uptime_app.UptimeApp, _apollo_client_adapter.createApolloClient);

          case 1:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _startApp.apply(this, arguments);
}