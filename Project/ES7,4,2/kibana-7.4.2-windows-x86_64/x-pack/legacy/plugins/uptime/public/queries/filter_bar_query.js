"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterBarQuery = exports.filterBarQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var filterBarQueryString = "\nquery FilterBar($dateRangeStart: String!, $dateRangeEnd: String!) {\n  filterBar: getFilterBar(dateRangeStart: $dateRangeStart, dateRangeEnd: $dateRangeEnd) {\n    ids\n    locations\n    names\n    ports\n    schemes\n    urls\n  }\n}\n";
exports.filterBarQueryString = filterBarQueryString;
var filterBarQuery = (0, _graphqlTag.default)(_templateObject(), filterBarQueryString);
exports.filterBarQuery = filterBarQuery;