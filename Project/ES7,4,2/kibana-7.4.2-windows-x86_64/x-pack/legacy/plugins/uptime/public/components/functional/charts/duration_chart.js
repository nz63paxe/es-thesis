"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DurationChart = void 0;

var _charts = require("@elastic/charts");

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _helper = require("../../../lib/helper");

var _get_colors_map = require("./get_colors_map");

var _chart_wrapper = require("./chart_wrapper");

var _hooks = require("../../../hooks");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * This chart is intended to visualize monitor duration performance over time to
 * the users in a helpful way. Its x-axis is based on a timeseries, the y-axis is in
 * milliseconds.
 * @param props The props required for this component to render properly
 */
var DurationChart = function DurationChart(_ref) {
  var locationDurationLines = _ref.locationDurationLines,
      meanColor = _ref.meanColor,
      loading = _ref.loading;

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 1),
      getUrlParams = _useUrlParams2[0];

  var _getUrlParams = getUrlParams(),
      min = _getUrlParams.absoluteDateRangeStart,
      max = _getUrlParams.absoluteDateRangeEnd; // this id is used for the line chart representing the average duration length


  var averageSpecId = (0, _charts.getSpecId)('average-');
  var lineSeries = locationDurationLines.map(function (line) {
    var locationSpecId = (0, _charts.getSpecId)('loc-avg' + line.name);
    return _react.default.createElement(_charts.LineSeries, {
      curve: _charts.CurveType.CURVE_MONOTONE_X,
      customSeriesColors: (0, _get_colors_map.getColorsMap)(meanColor, averageSpecId),
      data: line.line.map(function (_ref2) {
        var x = _ref2.x,
            y = _ref2.y;
        return [x || 0, (0, _helper.convertMicrosecondsToMilliseconds)(y)];
      }),
      id: locationSpecId,
      key: "locline-".concat(line.name),
      name: line.name,
      xAccessor: 0,
      xScaleType: _charts.ScaleType.Time,
      yAccessors: [1],
      yScaleToDataExtent: false,
      yScaleType: _charts.ScaleType.Linear
    });
  });
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiPanel, {
    paddingSize: "m"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h4", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.uptime.monitorCharts.monitorDuration.titleLabel",
    defaultMessage: "Monitor duration",
    description: "The 'ms' is an abbreviation for milliseconds."
  }))), _react.default.createElement(_chart_wrapper.ChartWrapper, {
    height: "400px",
    loading: loading
  }, _react.default.createElement(_charts.Chart, null, _react.default.createElement(_charts.Settings, {
    xDomain: {
      min: min,
      max: max
    },
    showLegend: true,
    legendPosition: _charts.Position.Bottom
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)('bottom'),
    position: _charts.Position.Bottom,
    showOverlappingTicks: true,
    tickFormat: (0, _charts.timeFormatter)((0, _helper.getChartDateLabel)(min, max)),
    title: _i18n.i18n.translate('xpack.uptime.monitorCharts.durationChart.bottomAxis.title', {
      defaultMessage: 'Timestamp'
    })
  }), _react.default.createElement(_charts.Axis, {
    domain: {
      min: 0
    },
    id: (0, _charts.getAxisId)('left'),
    position: _charts.Position.Left,
    tickFormat: function tickFormat(d) {
      return Number(d).toFixed(0);
    },
    title: _i18n.i18n.translate('xpack.uptime.monitorCharts.durationChart.leftAxis.title', {
      defaultMessage: 'Duration ms'
    })
  }), lineSeries))));
};

exports.DurationChart = DurationChart;