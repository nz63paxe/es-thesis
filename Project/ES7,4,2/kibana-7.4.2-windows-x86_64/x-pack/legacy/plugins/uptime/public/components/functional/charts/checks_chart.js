"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ChecksChart = void 0;

var _charts = require("@elastic/charts");

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _helper = require("../../../lib/helper");

var _get_colors_map = require("./get_colors_map");

var _hooks = require("../../../hooks");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/**
 * Renders a chart that displays the total count of up/down status checks over time
 * as a stacked area chart.
 * @param props The props values required by this component.
 */
var ChecksChart = function ChecksChart(_ref) {
  var dangerColor = _ref.dangerColor,
      status = _ref.status,
      successColor = _ref.successColor;
  var upSeriesSpecId = (0, _charts.getSpecId)('Up');
  var downSeriesSpecId = (0, _charts.getSpecId)('Down');

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 1),
      getUrlParams = _useUrlParams2[0];

  var _getUrlParams = getUrlParams(),
      min = _getUrlParams.absoluteDateRangeStart,
      max = _getUrlParams.absoluteDateRangeEnd;

  var upString = _i18n.i18n.translate('xpack.uptime.monitorCharts.checkStatus.series.upCountLabel', {
    defaultMessage: 'Up count'
  });

  var downString = _i18n.i18n.translate('xpack.uptime.monitorCharts.checkStatus.series.downCountLabel', {
    defaultMessage: 'Down count'
  });

  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h4", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.uptime.monitorCharts.checkStatus.title",
    defaultMessage: "Check status"
  }))), _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(_charts.Chart, null, _react.default.createElement(_charts.Settings, {
    xDomain: {
      min: min,
      max: max
    },
    showLegend: false
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)('checksBottom'),
    position: _charts.Position.Bottom,
    showOverlappingTicks: true,
    tickFormat: (0, _charts.timeFormatter)((0, _helper.getChartDateLabel)(min, max)),
    title: _i18n.i18n.translate('xpack.uptime.monitorChart.checksChart.bottomAxis.title', {
      defaultMessage: 'Timestamp',
      description: 'The heading of the x-axis of a chart of timeseries data.'
    })
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)('left'),
    position: _charts.Position.Left,
    tickFormat: function tickFormat(d) {
      return Number(d).toFixed(0);
    },
    title: _i18n.i18n.translate('xpack.uptime.monitorChart.checksChart.leftAxis.title', {
      defaultMessage: 'Number of checks',
      description: 'The heading of the y-axis of a chart of timeseries data'
    })
  }), _react.default.createElement(_charts.AreaSeries, {
    customSeriesColors: (0, _get_colors_map.getColorsMap)(successColor, upSeriesSpecId),
    data: status.map(function (_ref2) {
      var x = _ref2.x,
          up = _ref2.up;
      return _defineProperty({
        x: x
      }, upString, up || 0);
    }),
    id: upSeriesSpecId,
    stackAccessors: ['x'],
    timeZone: "local",
    xAccessor: "x",
    xScaleType: _charts.ScaleType.Time,
    yAccessors: [upString],
    yScaleType: _charts.ScaleType.Linear
  }), _react.default.createElement(_charts.AreaSeries, {
    customSeriesColors: (0, _get_colors_map.getColorsMap)(dangerColor, downSeriesSpecId),
    data: status.map(function (_ref4) {
      var x = _ref4.x,
          down = _ref4.down;
      return _defineProperty({
        x: x
      }, downString, down || 0);
    }),
    id: downSeriesSpecId,
    stackAccessors: ['x'],
    timeZone: "local",
    xAccessor: "x",
    xScaleType: _charts.ScaleType.Time,
    yAccessors: [downString],
    yScaleType: _charts.ScaleType.Linear
  }))));
};

exports.ChecksChart = ChecksChart;