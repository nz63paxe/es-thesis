"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

var _routes = _interopRequireDefault(require("ui/routes"));

var _kibana_framework_adapter = require("../adapters/framework/kibana_framework_adapter");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function compose() {
  var libs = {
    framework: new _kibana_framework_adapter.UMKibanaFrameworkAdapter(_routes.default)
  };
  return libs;
}