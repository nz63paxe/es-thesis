"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorPage = void 0;

var _eui = require("@elastic/eui");

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

var _react = _interopRequireWildcard(require("react"));

var _breadcrumbs = require("../breadcrumbs");

var _functional = require("../components/functional");

var _contexts = require("../contexts");

var _hooks = require("../hooks");

var _stringify_url_params = require("../lib/helper/stringify_url_params");

var _ping_list = require("../components/functional/ping_list");

var _public = require("../../../infra/public");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n        query MonitorPageTitle($monitorId: String!) {\n          monitorPageTitle: getMonitorPageTitle(monitorId: $monitorId) {\n            id\n            url\n            name\n          }\n        }\n      "]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var MonitorPage = function MonitorPage(_ref) {
  var logMonitorPageLoad = _ref.logMonitorPageLoad,
      query = _ref.query,
      setBreadcrumbs = _ref.setBreadcrumbs,
      match = _ref.match;
  // decode 64 base string, it was decoded to make it a valid url, since monitor id can be a url
  var monitorId = atob(match.params.monitorId);

  var _useState = (0, _react.useState)(10),
      _useState2 = _slicedToArray(_useState, 2),
      pingListPageCount = _useState2[0],
      setPingListPageCount = _useState2[1];

  var _useContext = (0, _react.useContext)(_contexts.UptimeSettingsContext),
      colors = _useContext.colors,
      refreshApp = _useContext.refreshApp,
      setHeadingText = _useContext.setHeadingText;

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 2),
      getUrlParams = _useUrlParams2[0],
      updateUrlParams = _useUrlParams2[1];

  var _getUrlParams = getUrlParams(),
      absoluteDateRangeStart = _getUrlParams.absoluteDateRangeStart,
      absoluteDateRangeEnd = _getUrlParams.absoluteDateRangeEnd,
      params = _objectWithoutProperties(_getUrlParams, ["absoluteDateRangeStart", "absoluteDateRangeEnd"]);

  var dateRangeStart = params.dateRangeStart,
      dateRangeEnd = params.dateRangeEnd,
      selectedPingStatus = params.selectedPingStatus;
  (0, _react.useEffect)(function () {
    query({
      query: (0, _graphqlTag.default)(_templateObject()),
      variables: {
        monitorId: monitorId
      }
    }).then(function (result) {
      var _result$data$monitorP = result.data.monitorPageTitle,
          name = _result$data$monitorP.name,
          url = _result$data$monitorP.url,
          id = _result$data$monitorP.id;
      var heading = name || url || id;
      setBreadcrumbs((0, _breadcrumbs.getMonitorPageBreadcrumb)(heading, (0, _stringify_url_params.stringifyUrlParams)(params)));

      if (setHeadingText) {
        setHeadingText(heading);
      }
    });
  }, [params]);

  var _useState3 = (0, _react.useState)(_ping_list.BaseLocationOptions),
      _useState4 = _slicedToArray(_useState3, 2),
      selectedLocation = _useState4[0],
      setSelectedLocation = _useState4[1];

  var selLocationVal = selectedLocation[0].value === 'All' ? null : selectedLocation[0].value;
  var sharedVariables = {
    dateRangeStart: dateRangeStart,
    dateRangeEnd: dateRangeEnd,
    location: selLocationVal,
    monitorId: monitorId
  };
  (0, _react.useEffect)(function () {
    logMonitorPageLoad();
  }, []);
  (0, _public.useTrackPageview)({
    app: 'uptime',
    path: 'monitor'
  });
  (0, _public.useTrackPageview)({
    app: 'uptime',
    path: 'monitor',
    delay: 15000
  });
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_functional.MonitorPageTitle, {
    monitorId: monitorId,
    variables: {
      monitorId: monitorId
    }
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_functional.MonitorStatusBar, {
    monitorId: monitorId,
    variables: sharedVariables
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_functional.MonitorCharts, _extends({}, colors, {
    monitorId: monitorId,
    variables: sharedVariables,
    dateRangeStart: dateRangeStart,
    dateRangeEnd: dateRangeEnd
  })), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_functional.PingList, {
    onPageCountChange: setPingListPageCount,
    onSelectedLocationChange: setSelectedLocation,
    onSelectedStatusChange: function onSelectedStatusChange(selectedStatus) {
      return updateUrlParams({
        selectedPingStatus: selectedStatus || ''
      });
    },
    onUpdateApp: refreshApp,
    pageSize: pingListPageCount,
    selectedOption: selectedPingStatus,
    selectedLocation: selectedLocation,
    variables: _objectSpread({}, sharedVariables, {
      size: pingListPageCount,
      status: selectedPingStatus
    })
  }));
};

exports.MonitorPage = MonitorPage;