"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.snapshotHistogramQuery = exports.snapshotHistogramQueryString = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var snapshotHistogramQueryString = "\n  query SnapshotHistogram(\n    $dateRangeStart: String!\n    $dateRangeEnd: String!\n    $filters: String\n    $monitorId: String\n  ) {\n    histogram: getSnapshotHistogram(\n      dateRangeStart: $dateRangeStart\n      dateRangeEnd: $dateRangeEnd\n      filters: $filters\n      monitorId: $monitorId\n    ) {\n      upCount\n        downCount\n        x\n        x0\n        y\n    }\n  }\n";
exports.snapshotHistogramQueryString = snapshotHistogramQueryString;
var snapshotHistogramQuery = (0, _graphqlTag.default)(_templateObject(), snapshotHistogramQueryString);
exports.snapshotHistogramQuery = snapshotHistogramQuery;