"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorList = exports.ErrorListComponent = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireWildcard(require("react"));

var _higher_order = require("../higher_order");

var _queries = require("../../queries");

var _monitor_page_link = require("./monitor_page_link");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ErrorListComponent = function ErrorListComponent(_ref) {
  var data = _ref.data,
      linkParameters = _ref.linkParameters,
      loading = _ref.loading;
  return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react2.default.createElement("h5", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.uptime.errorList.title",
    defaultMessage: "Errors"
  }))), _react2.default.createElement(_eui.EuiPanel, {
    paddingSize: "s"
  }, _react2.default.createElement(_eui.EuiInMemoryTable, {
    loading: loading,
    items: data && data.errorList || undefined,
    columns: [{
      field: 'count',
      width: '200px',
      name: _i18n.i18n.translate('xpack.uptime.errorList.CountColumnLabel', {
        defaultMessage: 'Frequency'
      }),
      render: function render(count, item) {
        return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiText, {
          size: "s"
        }, _react2.default.createElement(_eui.EuiTextColor, {
          color: "danger"
        }, count), " errors"), _react2.default.createElement(_eui.EuiText, {
          size: "xs",
          color: "subdued"
        }, "Latest was ", (0, _moment.default)(item.timestamp).fromNow()));
      }
    }, {
      field: 'type',
      name: _i18n.i18n.translate('xpack.uptime.errorList.errorTypeColumnLabel', {
        defaultMessage: 'Error type'
      })
    }, {
      field: 'monitorId',
      name: _i18n.i18n.translate('xpack.uptime.errorList.monitorIdColumnLabel', {
        defaultMessage: 'Monitor ID'
      }),
      render: function render(id, _ref2) {
        var name = _ref2.name,
            location = _ref2.location;
        return _react2.default.createElement(_monitor_page_link.MonitorPageLink, {
          id: id,
          location: location || undefined,
          linkParameters: linkParameters
        }, name || id);
      },
      width: '12.5%'
    }, {
      field: 'location',
      name: _i18n.i18n.translate('xpack.uptime.errorList.location', {
        defaultMessage: 'Location',
        description: "The heading of a column that displays the location of a Heartbeat instance's host machine."
      }),
      width: '12.5%'
    }, {
      field: 'statusCode',
      name: _i18n.i18n.translate('xpack.uptime.errorList.statusCodeColumnLabel', {
        defaultMessage: 'Status code'
      }),
      render: function render(statusCode) {
        return statusCode ? _react2.default.createElement(_eui.EuiBadge, null, statusCode) : null;
      }
    }, {
      field: 'latestMessage',
      name: _i18n.i18n.translate('xpack.uptime.errorList.latestMessageColumnLabel', {
        defaultMessage: 'Latest message'
      }),
      width: '40%',
      render: function render(message) {
        return _react2.default.createElement("div", null, // TODO: remove this ignore when prop is defined on type
        // @ts-ignore size is not currently defined on the type for EuiCodeBlock
        _react2.default.createElement(_eui.EuiCodeBlock, {
          transparentBackground: true,
          size: "xs",
          paddingSize: "none"
        }, message));
      }
    }],
    pagination: {
      initialPageSize: 10,
      pageSizeOptions: [5, 10, 20, 50]
    }
  })));
};

exports.ErrorListComponent = ErrorListComponent;
var ErrorList = (0, _higher_order.withUptimeGraphQL)(ErrorListComponent, _queries.errorListQuery);
exports.ErrorList = ErrorList;