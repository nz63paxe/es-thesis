"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonitorListDrawer = void 0;

var _react = _interopRequireDefault(require("react"));

var _check_list = require("./check_list");

var _to_condensed_check = require("./to_condensed_check");

var _condensed_check_list = require("./condensed_check_list");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * The elements shown when the user expands the monitor list rows.
 */
var MonitorListDrawer = function MonitorListDrawer(_ref) {
  var condensedCheckLimit = _ref.condensedCheckLimit,
      dangerColor = _ref.dangerColor,
      successColor = _ref.successColor,
      summary = _ref.summary;

  if (!summary || !summary.state.checks) {
    return null;
  }

  if (summary.state.checks.length < condensedCheckLimit) {
    return _react.default.createElement(_check_list.CheckList, {
      checks: summary.state.checks
    });
  } else {
    return _react.default.createElement(_condensed_check_list.CondensedCheckList, {
      condensedChecks: (0, _to_condensed_check.toCondensedCheck)(summary.state.checks),
      dangerColor: dangerColor,
      successColor: successColor
    });
  }
};

exports.MonitorListDrawer = MonitorListDrawer;