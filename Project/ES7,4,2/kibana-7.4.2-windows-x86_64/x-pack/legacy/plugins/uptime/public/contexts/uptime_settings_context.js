"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UptimeSettingsContext = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react = require("react");

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AUTOREFRESH_IS_PAUSED = _constants.CONTEXT_DEFAULTS.AUTOREFRESH_IS_PAUSED,
    AUTOREFRESH_INTERVAL = _constants.CONTEXT_DEFAULTS.AUTOREFRESH_INTERVAL,
    BASE_PATH = _constants.CONTEXT_DEFAULTS.BASE_PATH,
    DATE_RANGE_START = _constants.CONTEXT_DEFAULTS.DATE_RANGE_START,
    DATE_RANGE_END = _constants.CONTEXT_DEFAULTS.DATE_RANGE_END;

var parsedStart = _datemath.default.parse(DATE_RANGE_START);

var parsedEnd = _datemath.default.parse(DATE_RANGE_END);

var DEFAULT_ABSOLUTE_START_DATE = parsedStart ? parsedStart.valueOf() : 0;
var DEFAULT_ABSOLUTE_END_DATE = parsedEnd ? parsedEnd.valueOf() : 1;
/**
 * These are default values for the context. These defaults are typically
 * overwritten by the Uptime App upon its invocation.
 */

var defaultContext = {
  absoluteStartDate: DEFAULT_ABSOLUTE_START_DATE,
  absoluteEndDate: DEFAULT_ABSOLUTE_END_DATE,
  autorefreshIsPaused: AUTOREFRESH_IS_PAUSED,
  autorefreshInterval: AUTOREFRESH_INTERVAL,
  basePath: BASE_PATH,
  colors: {
    danger: _eui_theme_light.default.euiColorDanger,
    mean: _eui_theme_light.default.euiColorPrimary,
    range: _eui_theme_light.default.euiFocusBackgroundColor,
    success: _eui_theme_light.default.euiColorSuccess,
    warning: _eui_theme_light.default.euiColorWarning
  },
  dateRangeStart: DATE_RANGE_START,
  dateRangeEnd: DATE_RANGE_END,
  isApmAvailable: true,
  isInfraAvailable: true,
  isLogsAvailable: true,
  refreshApp: function refreshApp() {
    throw new Error('App refresh was not initialized, set it when you invoke the context');
  },
  setHeadingText: function setHeadingText() {
    throw new Error('setHeadingText was not initialized on UMSettingsContext.');
  }
};
var UptimeSettingsContext = (0, _react.createContext)(defaultContext);
exports.UptimeSettingsContext = UptimeSettingsContext;