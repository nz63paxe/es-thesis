"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverviewPage = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _breadcrumbs = require("../breadcrumbs");

var _functional = require("../components/functional");

var _contexts = require("../contexts");

var _hooks = require("../hooks");

var _stringify_url_params = require("../lib/helper/stringify_url_params");

var _public = require("../../../infra/public");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var OverviewPage = function OverviewPage(_ref) {
  var basePath = _ref.basePath,
      logOverviewPageLoad = _ref.logOverviewPageLoad,
      setBreadcrumbs = _ref.setBreadcrumbs;

  var _useContext = (0, _react.useContext)(_contexts.UptimeSettingsContext),
      colors = _useContext.colors,
      refreshApp = _useContext.refreshApp,
      setHeadingText = _useContext.setHeadingText;

  var _useUrlParams = (0, _hooks.useUrlParams)(),
      _useUrlParams2 = _slicedToArray(_useUrlParams, 2),
      getUrlParams = _useUrlParams2[0],
      updateUrl = _useUrlParams2[1];

  var _getUrlParams = getUrlParams(),
      absoluteDateRangeStart = _getUrlParams.absoluteDateRangeStart,
      absoluteDateRangeEnd = _getUrlParams.absoluteDateRangeEnd,
      params = _objectWithoutProperties(_getUrlParams, ["absoluteDateRangeStart", "absoluteDateRangeEnd"]);

  var dateRangeStart = params.dateRangeStart,
      dateRangeEnd = params.dateRangeEnd,
      search = params.search;
  (0, _react.useEffect)(function () {
    setBreadcrumbs((0, _breadcrumbs.getOverviewPageBreadcrumbs)());
    logOverviewPageLoad();

    if (setHeadingText) {
      setHeadingText(_i18n.i18n.translate('xpack.uptime.overviewPage.headerText', {
        defaultMessage: 'Overview',
        description: "The text that will be displayed in the app's heading when the Overview page loads."
      }));
    }
  }, []);
  (0, _public.useTrackPageview)({
    app: 'uptime',
    path: 'overview'
  });
  (0, _public.useTrackPageview)({
    app: 'uptime',
    path: 'overview',
    delay: 15000
  });
  var filterQueryString = search || '';
  var error;
  var filters;

  try {
    // toESQuery will throw errors
    if (filterQueryString) {
      filters = JSON.stringify(_eui.EuiSearchBar.Query.toESQuery(filterQueryString));
    }
  } catch (e) {
    error = e;
  }

  var sharedProps = {
    dateRangeStart: dateRangeStart,
    dateRangeEnd: dateRangeEnd,
    filters: filters
  };

  var updateQuery = function updateQuery(_ref2) {
    var queryText = _ref2.queryText;
    updateUrl({
      search: queryText || ''
    });
    refreshApp();
  };

  var linkParameters = (0, _stringify_url_params.stringifyUrlParams)(params); // TODO: reintroduce for pagination and sorting
  // const onMonitorListChange = ({ page: { index, size }, sort: { field, direction } }: Criteria) => {
  //   updateUrl({
  //     monitorListPageIndex: index,
  //     monitorListPageSize: size,
  //     monitorListSortDirection: direction,
  //     monitorListSortField: field,
  //   });
  // };

  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_functional.EmptyState, {
    basePath: basePath,
    implementsCustomErrorState: true,
    variables: {}
  }, _react.default.createElement(_functional.FilterBar, {
    currentQuery: filterQueryString,
    error: error,
    updateQuery: updateQuery,
    variables: sharedProps
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: 4
  }, _react.default.createElement(_functional.Snapshot, {
    variables: sharedProps
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: 8
  }, _react.default.createElement(_functional.SnapshotHistogram, {
    absoluteStartDate: absoluteDateRangeStart,
    absoluteEndDate: absoluteDateRangeEnd,
    successColor: colors.success,
    dangerColor: colors.danger,
    variables: sharedProps,
    height: "120px"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_functional.MonitorList, {
    absoluteStartDate: absoluteDateRangeStart,
    absoluteEndDate: absoluteDateRangeEnd,
    dangerColor: colors.danger,
    implementsCustomErrorState: true,
    linkParameters: linkParameters,
    successColor: colors.success // TODO: reintegrate pagination in future release
    // pageIndex={monitorListPageIndex}
    // pageSize={monitorListPageSize}
    // TODO: reintegrate sorting in future release
    // sortDirection={monitorListSortDirection}
    // sortField={monitorListSortField}
    // TODO: reintroduce for pagination and sorting
    // onChange={onMonitorListChange}
    ,
    variables: _objectSpread({}, sharedProps)
  })));
};

exports.OverviewPage = OverviewPage;