"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuggestionItem = void 0;

var _eui = require("@elastic/eui");

var _polished = require("polished");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  flex: 3 0 0;\n  p {\n    display: inline;\n    span {\n      font-family: ", ";\n    }\n  }\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  flex: 2 0 0;\n  font-family: ", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  color: ", ";\n  flex: 0 0 auto;\n  justify-content: center;\n  width: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  cursor: pointer;\n  display: flex;\n  flex-direction: row;\n  height: ", ";\n  padding: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: row;\n  font-size: ", ";\n  height: ", ";\n  white-space: nowrap;\n  background-color: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SuggestionItem =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SuggestionItem, _React$Component);

  function SuggestionItem() {
    _classCallCheck(this, SuggestionItem);

    return _possibleConstructorReturn(this, _getPrototypeOf(SuggestionItem).apply(this, arguments));
  }

  _createClass(SuggestionItem, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          isSelected = _this$props.isSelected,
          onClick = _this$props.onClick,
          onMouseEnter = _this$props.onMouseEnter,
          suggestion = _this$props.suggestion;
      return _react.default.createElement(SuggestionItemContainer, {
        isSelected: isSelected,
        onClick: onClick,
        onMouseEnter: onMouseEnter
      }, _react.default.createElement(SuggestionItemIconField, {
        suggestionType: suggestion.type
      }, _react.default.createElement(_eui.EuiIcon, {
        type: getEuiIconType(suggestion.type)
      })), _react.default.createElement(SuggestionItemTextField, null, suggestion.text), _react.default.createElement(SuggestionItemDescriptionField, null, suggestion.description));
    }
  }]);

  return SuggestionItem;
}(_react.default.Component);

exports.SuggestionItem = SuggestionItem;

_defineProperty(SuggestionItem, "defaultProps", {
  isSelected: false
});

var SuggestionItemContainer = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.default.euiFontSizeS;
}, function (props) {
  return props.theme.eui.default.euiSizeXl;
}, function (props) {
  return props.isSelected ? props.theme.eui.default.euiColorLightestShade : 'transparent';
});

var SuggestionItemField = _styledComponents.default.div(_templateObject2(), function (props) {
  return props.theme.eui.default.euiSizeXl;
}, function (props) {
  return props.theme.eui.default.euiSizeXs;
});

var SuggestionItemIconField = SuggestionItemField.extend(_templateObject3(), function (props) {
  return (0, _polished.tint)(0.1, getEuiIconColor(props.theme, props.suggestionType));
}, function (props) {
  return getEuiIconColor(props.theme, props.suggestionType);
}, function (props) {
  return props.theme.eui.default.euiSizeXl;
});
var SuggestionItemTextField = SuggestionItemField.extend(_templateObject4(), function (props) {
  return props.theme.eui.default.euiCodeFontFamily;
});
var SuggestionItemDescriptionField = SuggestionItemField.extend(_templateObject5(), function (props) {
  return props.theme.eui.default.euiCodeFontFamily;
});

var getEuiIconType = function getEuiIconType(suggestionType) {
  switch (suggestionType) {
    case 'field':
      return 'kqlField';

    case 'value':
      return 'kqlValue';

    case 'recentSearch':
      return 'search';

    case 'conjunction':
      return 'kqlSelector';

    case 'operator':
      return 'kqlOperand';

    default:
      return 'empty';
  }
};

var getEuiIconColor = function getEuiIconColor(theme, suggestionType) {
  switch (suggestionType) {
    case 'field':
      return theme.eui.default.euiColorVis7;

    case 'value':
      return theme.eui.default.euiColorVis0;

    case 'operator':
      return theme.eui.default.euiColorVis1;

    case 'conjunction':
      return theme.eui.default.euiColorVis2;

    case 'recentSearch':
    default:
      return theme.eui.default.euiColorMediumShade;
  }
};