"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InitialWalkthroughPage = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _no_data = require("../../../components/layouts/no_data");

var _walkthrough = require("../../../components/layouts/walkthrough");

var _child_routes = require("../../../components/navigation/child_routes");

var _connected_link = require("../../../components/navigation/connected_link");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var InitialWalkthroughPageComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(InitialWalkthroughPageComponent, _Component);

  function InitialWalkthroughPageComponent() {
    _classCallCheck(this, InitialWalkthroughPageComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(InitialWalkthroughPageComponent).apply(this, arguments));
  }

  _createClass(InitialWalkthroughPageComponent, [{
    key: "render",
    value: function render() {
      var intl = this.props.intl;

      if (this.props.location.pathname === '/walkthrough/initial') {
        return _react2.default.createElement(_no_data.NoDataLayout, {
          title: _react2.default.createElement(_eui.EuiFlexGroup, {
            alignItems: "center",
            gutterSize: "m"
          }, _react2.default.createElement(_eui.EuiFlexItem, {
            grow: false
          }, 'Beats central management '), _react2.default.createElement(_eui.EuiFlexItem, {
            grow: false
          }, _react2.default.createElement(_eui.EuiBetaBadge, {
            label: _i18n.i18n.translate('xpack.beatsManagement.walkthrough.initial.betaBadgeText', {
              defaultMessage: 'Beta'
            })
          }))),
          actionSection: _react2.default.createElement(_connected_link.ConnectedLink, {
            path: "/walkthrough/initial/beat"
          }, _react2.default.createElement(_eui.EuiButton, {
            color: "primary",
            fill: true
          }, _react2.default.createElement(_react.FormattedMessage, {
            id: "xpack.beatsManagement.enrollBeat.enrollBeatButtonLabel",
            defaultMessage: "Enroll Beat"
          }), ' '))
        }, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.beatsManagement.enrollBeat.beatsCentralManagementDescription",
          defaultMessage: "Manage your configurations in a central location."
        })));
      }

      return _react2.default.createElement(_walkthrough.WalkthroughLayout, {
        title: intl.formatMessage({
          id: 'xpack.beatsManagement.enrollBeat.getStartedBeatsCentralManagementTitle',
          defaultMessage: 'Get started with Beats central management'
        }),
        walkthroughSteps: [{
          id: '/walkthrough/initial/beat',
          name: intl.formatMessage({
            id: 'xpack.beatsManagement.enrollBeat.enrollBeatStepLabel',
            defaultMessage: 'Enroll Beat'
          })
        }, {
          id: '/walkthrough/initial/tag',
          name: intl.formatMessage({
            id: 'xpack.beatsManagement.enrollBeat.createTagStepLabel',
            defaultMessage: 'Create tag'
          })
        }, {
          id: '/walkthrough/initial/finish',
          name: intl.formatMessage({
            id: 'xpack.beatsManagement.enrollBeat.finishStepLabel',
            defaultMessage: 'Finish'
          })
        }],
        goTo: function goTo() {// FIXME implament goto
        },
        activePath: this.props.location.pathname
      }, _react2.default.createElement(_child_routes.ChildRoutes, _extends({
        routes: this.props.routes
      }, this.props)));
    }
  }]);

  return InitialWalkthroughPageComponent;
}(_react2.Component);

var InitialWalkthroughPage = (0, _react.injectI18n)(InitialWalkthroughPageComponent);
exports.InitialWalkthroughPage = InitialWalkthroughPage;