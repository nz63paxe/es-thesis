"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CheckupControls = exports.CheckupControlsUI = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _types = require("../../types");

var _filter_bar = require("./filter_bar");

var _group_by_bar = require("./group_by_bar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CheckupControlsUI = function CheckupControlsUI(_ref) {
  var allDeprecations = _ref.allDeprecations,
      loadingState = _ref.loadingState,
      loadData = _ref.loadData,
      currentFilter = _ref.currentFilter,
      onFilterChange = _ref.onFilterChange,
      search = _ref.search,
      onSearchChange = _ref.onSearchChange,
      availableGroupByOptions = _ref.availableGroupByOptions,
      currentGroupBy = _ref.currentGroupBy,
      onGroupByChange = _ref.onGroupByChange,
      intl = _ref.intl;
  return _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    wrap: true,
    responsive: false
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: true
  }, _react.default.createElement(_eui.EuiFieldSearch, {
    "aria-label": "Filter",
    placeholder: intl.formatMessage({
      id: 'xpack.upgradeAssistant.checkupTab.controls.searchBarPlaceholder',
      defaultMessage: 'Filter'
    }),
    value: search,
    onChange: function onChange(e) {
      return onSearchChange(e.target.value);
    }
  })), _react.default.createElement(_filter_bar.FilterBar, {
    allDeprecations: allDeprecations,
    currentFilter: currentFilter,
    onFilterChange: onFilterChange
  }), _react.default.createElement(_group_by_bar.GroupByBar, {
    availableGroupByOptions: availableGroupByOptions,
    currentGroupBy: currentGroupBy,
    onGroupByChange: onGroupByChange
  }), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    onClick: loadData,
    iconType: "refresh",
    isLoading: loadingState === _types.LoadingState.Loading
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.upgradeAssistant.checkupTab.controls.refreshButtonLabel",
    defaultMessage: "Refresh"
  }))));
};

exports.CheckupControlsUI = CheckupControlsUI;
var CheckupControls = (0, _react2.injectI18n)(CheckupControlsUI);
exports.CheckupControls = CheckupControls;