"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RootComponent = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _version = require("../common/version");

var _tabs = require("./components/tabs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RootComponent = function RootComponent() {
  return _react.default.createElement("div", {
    "data-test-subj": "upgradeAssistantRoot"
  }, _react.default.createElement(_eui.EuiPageHeader, null, _react.default.createElement(_eui.EuiPageHeaderSection, null, _react.default.createElement(_eui.EuiTitle, {
    size: "l"
  }, _react.default.createElement("h1", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.upgradeAssistant.appTitle",
    defaultMessage: "{version} Upgrade Assistant",
    values: {
      version: "".concat(_version.NEXT_MAJOR_VERSION, ".0")
    }
  }))))), _react.default.createElement(_tabs.UpgradeAssistantTabs, null));
};

exports.RootComponent = RootComponent;