"use strict";

var _i18n = require("@kbn/i18n");

var _i18n2 = require("ui/i18n");

var _management = require("ui/management");

var _modules = require("ui/modules");

var _routes = _interopRequireDefault(require("ui/routes"));

var _version = require("../common/version");

var _app = require("./app");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
// @ts-ignore
var BASE_PATH = "/management/elasticsearch/upgrade_assistant";

function startApp() {
  _management.management.getSection('elasticsearch').register('upgrade_assistant', {
    visible: true,
    display: _i18n.i18n.translate('xpack.upgradeAssistant.appTitle', {
      defaultMessage: '{version} Upgrade Assistant',
      values: {
        version: "".concat(_version.NEXT_MAJOR_VERSION, ".0")
      }
    }),
    order: 100,
    url: "#".concat(BASE_PATH)
  });

  _modules.uiModules.get('kibana').directive('upgradeAssistant', function (reactDirective) {
    return reactDirective((0, _i18n2.wrapInI18nContext)(_app.RootComponent));
  });

  _routes.default.when("".concat(BASE_PATH, "/:view?"), {
    template: '<kbn-management-app section="elasticsearch/upgrade_assistant"><upgrade-assistant /></kbn-management-app>'
  });
}

startApp();