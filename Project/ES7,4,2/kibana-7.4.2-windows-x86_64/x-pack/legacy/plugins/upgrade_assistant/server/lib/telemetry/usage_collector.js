"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchUpgradeAssistantMetrics = fetchUpgradeAssistantMetrics;
exports.makeUpgradeAssistantUsageCollector = makeUpgradeAssistantUsageCollector;

var _lodash = require("lodash");

var _types = require("../../../common/types");

var _es_deprecation_logging_apis = require("../es_deprecation_logging_apis");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getSavedObjectAttributesFromRepo(savedObjectsRepository, docType, docID) {
  try {
    return (await savedObjectsRepository.get(docType, docID)).attributes;
  } catch (e) {
    return null;
  }
}

async function getDeprecationLoggingStatusValue(callCluster) {
  try {
    const loggerDeprecationCallResult = await callCluster('cluster.getSettings', {
      includeDefaults: true
    });
    return (0, _es_deprecation_logging_apis.isDeprecationLoggingEnabled)(loggerDeprecationCallResult);
  } catch (e) {
    return false;
  }
}

async function fetchUpgradeAssistantMetrics(callCluster, server) {
  const {
    getSavedObjectsRepository
  } = server.savedObjects;
  const savedObjectsRepository = getSavedObjectsRepository(callCluster);
  const upgradeAssistantSOAttributes = await getSavedObjectAttributesFromRepo(savedObjectsRepository, _types.UPGRADE_ASSISTANT_TYPE, _types.UPGRADE_ASSISTANT_DOC_ID);
  const deprecationLoggingStatusValue = await getDeprecationLoggingStatusValue(callCluster);

  const getTelemetrySavedObject = upgradeAssistantTelemetrySavedObjectAttrs => {
    const defaultTelemetrySavedObject = {
      ui_open: {
        overview: 0,
        cluster: 0,
        indices: 0
      },
      ui_reindex: {
        close: 0,
        open: 0,
        start: 0,
        stop: 0
      }
    };

    if (!upgradeAssistantTelemetrySavedObjectAttrs) {
      return defaultTelemetrySavedObject;
    }

    const upgradeAssistantTelemetrySOAttrsKeys = Object.keys(upgradeAssistantTelemetrySavedObjectAttrs);
    const telemetryObj = defaultTelemetrySavedObject;
    upgradeAssistantTelemetrySOAttrsKeys.forEach(key => {
      (0, _lodash.set)(telemetryObj, key, upgradeAssistantTelemetrySavedObjectAttrs[key]);
    });
    return telemetryObj;
  };

  return { ...getTelemetrySavedObject(upgradeAssistantSOAttributes),
    features: {
      deprecation_logging: {
        enabled: deprecationLoggingStatusValue
      }
    }
  };
}

function makeUpgradeAssistantUsageCollector(server) {
  const kbnServer = server;
  const upgradeAssistantUsageCollector = kbnServer.usage.collectorSet.makeUsageCollector({
    type: _types.UPGRADE_ASSISTANT_TYPE,
    isReady: () => true,
    fetch: async callCluster => fetchUpgradeAssistantMetrics(callCluster, server)
  });
  kbnServer.usage.collectorSet.register(upgradeAssistantUsageCollector);
}