"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerQueryDefaultFieldRoutes = registerQueryDefaultFieldRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _joi = _interopRequireDefault(require("joi"));

var _es_version_precheck = require("../lib/es_version_precheck");

var _query_default_field = require("../lib/query_default_field");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Adds routes for detecting and fixing 6.x Metricbeat indices that need the
 * `index.query.default_field` index setting added.
 *
 * @param server
 */
function registerQueryDefaultFieldRoutes(server) {
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');
  server.route({
    path: '/api/upgrade_assistant/add_query_default_field/{indexName}',
    method: 'POST',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck],
      validate: {
        params: _joi.default.object({
          indexName: _joi.default.string().required()
        }),
        payload: _joi.default.object({
          fieldTypes: _joi.default.array().items(_joi.default.string()).required(),
          otherFields: _joi.default.array().items(_joi.default.string())
        })
      }
    },

    async handler(request) {
      try {
        const {
          indexName
        } = request.params;
        const {
          fieldTypes,
          otherFields
        } = request.payload;
        return await (0, _query_default_field.addDefaultField)(callWithRequest, request, indexName, new Set(fieldTypes), otherFields ? new Set(otherFields) : undefined);
      } catch (e) {
        if (e.status === 403) {
          return _boom.default.forbidden(e.message);
        }

        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
}