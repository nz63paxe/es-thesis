"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "makeUpgradeAssistantUsageCollector", {
  enumerable: true,
  get: function () {
    return _usage_collector.makeUpgradeAssistantUsageCollector;
  }
});

var _usage_collector = require("./usage_collector");