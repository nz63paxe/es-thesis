"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerReindexWorker = registerReindexWorker;
exports.registerReindexIndicesRoutes = registerReindexIndicesRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _lodash = require("lodash");

var _types = require("../../common/types");

var _es_version_precheck = require("../lib/es_version_precheck");

var _reindexing = require("../lib/reindexing");

var _reindex_actions = require("../lib/reindexing/reindex_actions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerReindexWorker(server, credentialStore) {
  const {
    callWithRequest,
    callWithInternalUser
  } = server.plugins.elasticsearch.getCluster('admin');
  const xpackInfo = server.plugins.xpack_main.info;
  const savedObjectsRepository = server.savedObjects.getSavedObjectsRepository(callWithInternalUser);
  const savedObjectsClient = new server.savedObjects.SavedObjectsClient(savedObjectsRepository); // Cannot pass server.log directly because it's value changes during startup (?).
  // Use this function to proxy through.

  const log = (tags, data, timestamp) => server.log(tags, data, timestamp);

  const worker = new _reindexing.ReindexWorker(savedObjectsClient, credentialStore, callWithRequest, callWithInternalUser, xpackInfo, log, (0, _lodash.get)(server, 'plugins.apm_oss.indexPatterns', [])); // Wait for ES connection before starting the polling loop.

  server.plugins.elasticsearch.waitUntilReady().then(() => {
    worker.start();
    server.events.on('stop', () => worker.stop());
  });
  return worker;
}

function registerReindexIndicesRoutes(server, worker, credentialStore) {
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');
  const xpackInfo = server.plugins.xpack_main.info;
  const apmIndexPatterns = (0, _lodash.get)(server, 'plugins.apm_oss.indexPatterns', []);
  const BASE_PATH = '/api/upgrade_assistant/reindex'; // Start reindex for an index

  server.route({
    path: `${BASE_PATH}/{indexName}`,
    method: 'POST',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck]
    },

    async handler(request) {
      const client = request.getSavedObjectsClient();
      const {
        indexName
      } = request.params;
      const callCluster = callWithRequest.bind(null, request);
      const reindexActions = (0, _reindex_actions.reindexActionsFactory)(client, callCluster);
      const reindexService = (0, _reindexing.reindexServiceFactory)(callCluster, xpackInfo, reindexActions, apmIndexPatterns, server.log);

      try {
        if (!(await reindexService.hasRequiredPrivileges(indexName))) {
          throw _boom.default.forbidden(`You do not have adequate privileges to reindex this index.`);
        }

        const existingOp = await reindexService.findReindexOperation(indexName); // If the reindexOp already exists and it's paused, resume it. Otherwise create a new one.

        const reindexOp = existingOp && existingOp.attributes.status === _types.ReindexStatus.paused ? await reindexService.resumeReindexOperation(indexName) : await reindexService.createReindexOperation(indexName); // Add users credentials for the worker to use

        credentialStore.set(reindexOp, request.headers); // Kick the worker on this node to immediately pickup the new reindex operation.

        worker.forceRefresh();
        return reindexOp.attributes;
      } catch (e) {
        if (!e.isBoom) {
          return _boom.default.boomify(e, {
            statusCode: 500
          });
        }

        return e;
      }
    }

  }); // Get status

  server.route({
    path: `${BASE_PATH}/{indexName}`,
    method: 'GET',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck]
    },

    async handler(request) {
      const client = request.getSavedObjectsClient();
      const {
        indexName
      } = request.params;
      const callCluster = callWithRequest.bind(null, request);
      const reindexActions = (0, _reindex_actions.reindexActionsFactory)(client, callCluster);
      const reindexService = (0, _reindexing.reindexServiceFactory)(callCluster, xpackInfo, reindexActions, apmIndexPatterns, server.log);

      try {
        const hasRequiredPrivileges = await reindexService.hasRequiredPrivileges(indexName);
        const reindexOp = await reindexService.findReindexOperation(indexName); // If the user doesn't have privileges than querying for warnings is going to fail.

        const warnings = hasRequiredPrivileges ? await reindexService.detectReindexWarnings(indexName) : [];
        const indexGroup = reindexService.getIndexGroup(indexName);
        return {
          reindexOp: reindexOp ? reindexOp.attributes : null,
          warnings,
          indexGroup,
          hasRequiredPrivileges
        };
      } catch (e) {
        if (!e.isBoom) {
          return _boom.default.boomify(e, {
            statusCode: 500
          });
        }

        return e;
      }
    }

  }); // Cancel reindex

  server.route({
    path: `${BASE_PATH}/{indexName}/cancel`,
    method: 'POST',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck]
    },

    async handler(request) {
      const client = request.getSavedObjectsClient();
      const {
        indexName
      } = request.params;
      const callCluster = callWithRequest.bind(null, request);
      const reindexActions = (0, _reindex_actions.reindexActionsFactory)(client, callCluster);
      const reindexService = (0, _reindexing.reindexServiceFactory)(callCluster, xpackInfo, reindexActions, apmIndexPatterns, server.log);

      try {
        await reindexService.cancelReindexing(indexName);
        return {
          acknowledged: true
        };
      } catch (e) {
        if (!e.isBoom) {
          return _boom.default.boomify(e, {
            statusCode: 500
          });
        }

        return e;
      }
    }

  });
}