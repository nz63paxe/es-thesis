"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerTelemetryRoutes = registerTelemetryRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _joi = _interopRequireDefault(require("joi"));

var _es_ui_open_apis = require("../lib/telemetry/es_ui_open_apis");

var _es_ui_reindex_apis = require("../lib/telemetry/es_ui_reindex_apis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerTelemetryRoutes(server) {
  server.route({
    path: '/api/upgrade_assistant/telemetry/ui_open',
    method: 'PUT',
    options: {
      validate: {
        payload: _joi.default.object({
          overview: _joi.default.boolean().default(false),
          cluster: _joi.default.boolean().default(false),
          indices: _joi.default.boolean().default(false)
        })
      }
    },

    async handler(request) {
      try {
        return await (0, _es_ui_open_apis.upsertUIOpenOption)(server, request);
      } catch (e) {
        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
  server.route({
    path: '/api/upgrade_assistant/telemetry/ui_reindex',
    method: 'PUT',
    options: {
      validate: {
        payload: _joi.default.object({
          close: _joi.default.boolean().default(false),
          open: _joi.default.boolean().default(false),
          start: _joi.default.boolean().default(false),
          stop: _joi.default.boolean().default(false)
        })
      }
    },

    async handler(request) {
      try {
        return await (0, _es_ui_reindex_apis.upsertUIReindexOption)(server, request);
      } catch (e) {
        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
}