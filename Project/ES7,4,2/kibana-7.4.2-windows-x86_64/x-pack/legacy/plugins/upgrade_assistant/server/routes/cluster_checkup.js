"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerClusterCheckupRoutes = registerClusterCheckupRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _lodash = _interopRequireDefault(require("lodash"));

var _es_migration_apis = require("../lib/es_migration_apis");

var _es_version_precheck = require("../lib/es_version_precheck");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerClusterCheckupRoutes(server) {
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');

  const isCloudEnabled = _lodash.default.get(server.plugins, 'cloud.config.isCloudEnabled', false);

  const apmIndexPatterns = _lodash.default.get(server, 'plugins.apm_oss.indexPatterns', []);

  server.route({
    path: '/api/upgrade_assistant/status',
    method: 'GET',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck]
    },

    async handler(request) {
      try {
        return await (0, _es_migration_apis.getUpgradeAssistantStatus)(callWithRequest, request, isCloudEnabled, apmIndexPatterns);
      } catch (e) {
        if (e.status === 403) {
          return _boom.default.forbidden(e.message);
        }

        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
}