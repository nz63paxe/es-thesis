"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.upsertUIReindexOption = upsertUIReindexOption;

var _types = require("../../../common/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function incrementUIReindexOptionCounter(server, uiOpenOptionCounter) {
  const {
    getSavedObjectsRepository
  } = server.savedObjects;
  const {
    callWithInternalUser
  } = server.plugins.elasticsearch.getCluster('admin');
  const internalRepository = getSavedObjectsRepository(callWithInternalUser);
  await internalRepository.incrementCounter(_types.UPGRADE_ASSISTANT_TYPE, _types.UPGRADE_ASSISTANT_DOC_ID, `ui_reindex.${uiOpenOptionCounter}`);
}

async function upsertUIReindexOption(server, req) {
  const {
    close,
    open,
    start,
    stop
  } = req.payload;

  if (close) {
    await incrementUIReindexOptionCounter(server, 'close');
  }

  if (open) {
    await incrementUIReindexOptionCounter(server, 'open');
  }

  if (start) {
    await incrementUIReindexOptionCounter(server, 'start');
  }

  if (stop) {
    await incrementUIReindexOptionCounter(server, 'stop');
  }

  return {
    close,
    open,
    start,
    stop
  };
}