"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerDeprecationLoggingRoutes = registerDeprecationLoggingRoutes;

var _boom = _interopRequireDefault(require("boom"));

var _joi = _interopRequireDefault(require("joi"));

var _es_deprecation_logging_apis = require("../lib/es_deprecation_logging_apis");

var _es_version_precheck = require("../lib/es_version_precheck");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerDeprecationLoggingRoutes(server) {
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');
  server.route({
    path: '/api/upgrade_assistant/deprecation_logging',
    method: 'GET',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck]
    },

    async handler(request) {
      try {
        return await (0, _es_deprecation_logging_apis.getDeprecationLoggingStatus)(callWithRequest, request);
      } catch (e) {
        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
  server.route({
    path: '/api/upgrade_assistant/deprecation_logging',
    method: 'PUT',
    options: {
      pre: [_es_version_precheck.EsVersionPrecheck],
      validate: {
        payload: _joi.default.object({
          isEnabled: _joi.default.boolean()
        })
      }
    },

    async handler(request) {
      try {
        const {
          isEnabled
        } = request.payload;
        return await (0, _es_deprecation_logging_apis.setDeprecationLogging)(callWithRequest, request, isEnabled);
      } catch (e) {
        return _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    }

  });
}