"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initServer = initServer;

var _credential_store = require("./lib/reindexing/credential_store");

var _telemetry = require("./lib/telemetry");

var _cluster_checkup = require("./routes/cluster_checkup");

var _deprecation_logging = require("./routes/deprecation_logging");

var _query_default_field = require("./routes/query_default_field");

var _reindex_indices = require("./routes/reindex_indices");

var _telemetry2 = require("./routes/telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initServer(server) {
  (0, _cluster_checkup.registerClusterCheckupRoutes)(server);
  (0, _deprecation_logging.registerDeprecationLoggingRoutes)(server);
  (0, _query_default_field.registerQueryDefaultFieldRoutes)(server); // The ReindexWorker uses a map of request headers that contain the authentication credentials
  // for a given reindex. We cannot currently store these in an the .kibana index b/c we do not
  // want to expose these credentials to any unauthenticated users. We also want to avoid any need
  // to add a user for a special index just for upgrading. This in-memory cache allows us to
  // process jobs without the browser staying on the page, but will require that jobs go into
  // a paused state if no Kibana nodes have the required credentials.

  const credentialStore = (0, _credential_store.credentialStoreFactory)();
  const worker = (0, _reindex_indices.registerReindexWorker)(server, credentialStore);
  (0, _reindex_indices.registerReindexIndicesRoutes)(server, worker, credentialStore); // Bootstrap the needed routes and the collector for the telemetry

  (0, _telemetry2.registerTelemetryRoutes)(server);
  (0, _telemetry.makeUpgradeAssistantUsageCollector)(server);
}