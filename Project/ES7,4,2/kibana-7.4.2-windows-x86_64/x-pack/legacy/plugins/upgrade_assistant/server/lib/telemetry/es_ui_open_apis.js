"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.upsertUIOpenOption = upsertUIOpenOption;

var _types = require("../../../common/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function incrementUIOpenOptionCounter(server, uiOpenOptionCounter) {
  const {
    getSavedObjectsRepository
  } = server.savedObjects;
  const {
    callWithInternalUser
  } = server.plugins.elasticsearch.getCluster('admin');
  const internalRepository = getSavedObjectsRepository(callWithInternalUser);
  await internalRepository.incrementCounter(_types.UPGRADE_ASSISTANT_TYPE, _types.UPGRADE_ASSISTANT_DOC_ID, `ui_open.${uiOpenOptionCounter}`);
}

async function upsertUIOpenOption(server, req) {
  const {
    overview,
    cluster,
    indices
  } = req.payload;

  if (overview) {
    await incrementUIOpenOptionCounter(server, 'overview');
  }

  if (cluster) {
    await incrementUIOpenOptionCounter(server, 'cluster');
  }

  if (indices) {
    await incrementUIOpenOptionCounter(server, 'indices');
  }

  return {
    overview,
    cluster,
    indices
  };
}