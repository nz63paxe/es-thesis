"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.upgradeAssistant = upgradeAssistant;

var _joi = _interopRequireDefault(require("joi"));

var _path = require("path");

var _mappings = _interopRequireDefault(require("./mappings.json"));

var _server = require("./server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function upgradeAssistant(kibana) {
  return new kibana.Plugin({
    id: 'upgrade_assistant',
    configPrefix: 'xpack.upgrade_assistant',
    require: ['elasticsearch', 'xpack_main'],
    uiExports: {
      managementSections: ['plugins/upgrade_assistant'],
      savedObjectSchemas: {
        'upgrade-assistant-reindex-operation': {
          isNamespaceAgnostic: true
        },
        'upgrade-assistant-telemetry': {
          isNamespaceAgnostic: true
        }
      },
      styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),
      mappings: _mappings.default
    },
    publicDir: (0, _path.resolve)(__dirname, 'public'),

    config() {
      return _joi.default.object({
        enabled: _joi.default.boolean().default(true)
      }).default();
    },

    init(server) {
      // Add server routes and initialize the plugin here
      (0, _server.initServer)(server);
    }

  });
}