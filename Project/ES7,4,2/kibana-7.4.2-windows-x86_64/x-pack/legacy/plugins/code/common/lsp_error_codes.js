"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LanguageServerStartFailed = exports.LanguageDisabled = exports.LanguageServerNotInstalled = exports.UnknownFileLanguage = exports.InternalError = exports.RequestCancelled = exports.UnknownErrorCode = exports.ServerNotInitialized = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ServerNotInitialized = -32002;
exports.ServerNotInitialized = ServerNotInitialized;
const UnknownErrorCode = -32001;
exports.UnknownErrorCode = UnknownErrorCode;
const RequestCancelled = -32800;
exports.RequestCancelled = RequestCancelled;
const InternalError = -32603;
exports.InternalError = InternalError;
const UnknownFileLanguage = -42404;
exports.UnknownFileLanguage = UnknownFileLanguage;
const LanguageServerNotInstalled = -42403;
exports.LanguageServerNotInstalled = LanguageServerNotInstalled;
const LanguageDisabled = -42402;
exports.LanguageDisabled = LanguageDisabled;
const LanguageServerStartFailed = -42405;
exports.LanguageServerStartFailed = LanguageServerStartFailed;