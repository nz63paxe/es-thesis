"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.REPO_FILE_STATUS_SEVERITY = exports.CTA = exports.RepoFileStatusText = exports.LangServerType = exports.Severity = exports.RepoFileStatus = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let RepoFileStatus;
exports.RepoFileStatus = RepoFileStatus;

(function (RepoFileStatus) {
  RepoFileStatus["LANG_SERVER_IS_INITIALIZING"] = "Language server is initializing.";
  RepoFileStatus["LANG_SERVER_INITIALIZED"] = "Language server initialized.";
  RepoFileStatus["INDEXING"] = "Indexing in progress";
  RepoFileStatus["FILE_NOT_SUPPORTED"] = "Current file is not of a supported language.";
  RepoFileStatus["REVISION_NOT_INDEXED"] = "Current revision is not indexed.";
  RepoFileStatus["LANG_SERVER_NOT_INSTALLED"] = "Install additional language server to support current file.";
  RepoFileStatus["FILE_IS_TOO_BIG"] = "Current file is too big.";
})(RepoFileStatus || (exports.RepoFileStatus = RepoFileStatus = {}));

let Severity;
exports.Severity = Severity;

(function (Severity) {
  Severity[Severity["NONE"] = 0] = "NONE";
  Severity[Severity["NOTICE"] = 1] = "NOTICE";
  Severity[Severity["WARNING"] = 2] = "WARNING";
  Severity[Severity["ERROR"] = 3] = "ERROR";
})(Severity || (exports.Severity = Severity = {}));

let LangServerType;
exports.LangServerType = LangServerType;

(function (LangServerType) {
  LangServerType["NONE"] = "Current file is not supported by any language server";
  LangServerType["GENERIC"] = "Current file is only covered by generic language server";
  LangServerType["DEDICATED"] = "Current file is covered by dedicated language server";
})(LangServerType || (exports.LangServerType = LangServerType = {}));

const RepoFileStatusText = {
  [RepoFileStatus.LANG_SERVER_IS_INITIALIZING]: _i18n.i18n.translate('xpack.code.repoFileStatus.langugageServerIsInitializitingMessage', {
    defaultMessage: 'Language server is initializing.'
  }),
  [RepoFileStatus.LANG_SERVER_INITIALIZED]: _i18n.i18n.translate('xpack.code.repoFileStatus.languageServerInitializedMessage', {
    defaultMessage: 'Language server initialized.'
  }),
  [RepoFileStatus.INDEXING]: _i18n.i18n.translate('xpack.code.repoFileStatus.IndexingInProgressMessage', {
    defaultMessage: 'Indexing in progress.'
  }),
  [RepoFileStatus.FILE_NOT_SUPPORTED]: _i18n.i18n.translate('xpack.code.repoFileStatus.fileNotSupportedMessage', {
    defaultMessage: 'Current file is not of a supported language.'
  }),
  [RepoFileStatus.REVISION_NOT_INDEXED]: _i18n.i18n.translate('xpack.code.repoFileStatus.revisionNotIndexedMessage', {
    defaultMessage: 'Current revision is not indexed.'
  }),
  [RepoFileStatus.LANG_SERVER_NOT_INSTALLED]: _i18n.i18n.translate('xpack.code.repoFileStatus.langServerNotInstalledMessage', {
    defaultMessage: 'Install additional language server to support current file.'
  }),
  [RepoFileStatus.FILE_IS_TOO_BIG]: _i18n.i18n.translate('xpack.code.repoFileStatus.fileIsTooBigMessage', {
    defaultMessage: 'Current file is too big.'
  }),
  [LangServerType.NONE]: _i18n.i18n.translate('xpack.code.repoFileStatus.langserverType.noneMessage', {
    defaultMessage: 'Current file is not supported by any language server.'
  }),
  [LangServerType.GENERIC]: _i18n.i18n.translate('xpack.code.repoFileStatus.langserverType.genericMessage', {
    defaultMessage: 'Current file is only covered by generic language server.'
  }),
  [LangServerType.DEDICATED]: _i18n.i18n.translate('xpack.code.repoFileStatus.langserverType.dedicatedMessage', {
    defaultMessage: 'Current file is covered by dedicated language server.'
  })
};
exports.RepoFileStatusText = RepoFileStatusText;
let CTA;
exports.CTA = CTA;

(function (CTA) {
  CTA[CTA["SWITCH_TO_HEAD"] = 0] = "SWITCH_TO_HEAD";
  CTA[CTA["GOTO_LANG_MANAGE_PAGE"] = 1] = "GOTO_LANG_MANAGE_PAGE";
})(CTA || (exports.CTA = CTA = {}));

const REPO_FILE_STATUS_SEVERITY = {
  [RepoFileStatus.LANG_SERVER_IS_INITIALIZING]: {
    severity: Severity.NOTICE
  },
  [RepoFileStatus.INDEXING]: {
    severity: Severity.NOTICE
  },
  [RepoFileStatus.FILE_NOT_SUPPORTED]: {
    severity: Severity.NOTICE
  },
  [LangServerType.GENERIC]: {
    severity: Severity.NOTICE
  },
  [RepoFileStatus.REVISION_NOT_INDEXED]: {
    severity: Severity.WARNING,
    fix: CTA.SWITCH_TO_HEAD
  },
  [RepoFileStatus.LANG_SERVER_NOT_INSTALLED]: {
    severity: Severity.WARNING,
    fix: CTA.GOTO_LANG_MANAGE_PAGE
  },
  [RepoFileStatus.FILE_IS_TOO_BIG]: {
    severity: Severity.NOTICE
  }
};
exports.REPO_FILE_STATUS_SEVERITY = REPO_FILE_STATUS_SEVERITY;