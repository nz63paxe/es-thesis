"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LineMapper = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LineMapper {
  constructor(content) {
    _defineProperty(this, "lines", void 0);

    _defineProperty(this, "acc", void 0);

    this.lines = content.split('\n');
    this.acc = [0];
    this.getLocation = this.getLocation.bind(this);

    for (let i = 0; i < this.lines.length - 1; i++) {
      this.acc[i + 1] = this.acc[i] + this.lines[i].length + 1;
    }
  }

  getLocation(offset) {
    let line = _lodash.default.sortedIndex(this.acc, offset);

    if (offset !== this.acc[line]) {
      line -= 1;
    }

    const column = offset - this.acc[line];
    return {
      line,
      column,
      offset
    };
  }

  getLines() {
    return this.lines;
  }

}

exports.LineMapper = LineMapper;