"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "TextDocumentMethods", {
  enumerable: true,
  get: function () {
    return _text_document_methods.TextDocumentMethods;
  }
});
exports.LspRestClient = void 0;

var _new_platform = require("ui/new_platform");

var _jsonrpc = require("./jsonrpc");

var _text_document_methods = require("./text_document_methods");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LspRestClient {
  constructor(baseUri) {
    _defineProperty(this, "baseUri", void 0);

    this.baseUri = baseUri;
  }

  async sendRequest(method, params, signal) {
    try {
      const response = await _new_platform.npStart.core.http.post(`${this.baseUri}/${method}`, {
        body: JSON.stringify(params),
        signal
      });
      return response;
    } catch (e) {
      let error = e;

      if (error.body && error.body.error) {
        error = error.body.error;
      }

      throw new _jsonrpc.ResponseError(error.code, error.message, error.data);
    }
  }

}

exports.LspRestClient = LspRestClient;