"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiffKind = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let DiffKind;
exports.DiffKind = DiffKind;

(function (DiffKind) {
  DiffKind["ADDED"] = "ADDED";
  DiffKind["DELETED"] = "DELETED";
  DiffKind["MODIFIED"] = "MODIFIED";
  DiffKind["RENAMED"] = "RENAMED";
})(DiffKind || (exports.DiffKind = DiffKind = {}));