"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CTAGS_SUPPORT_LANGS = exports.LanguageServerStatus = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let LanguageServerStatus;
exports.LanguageServerStatus = LanguageServerStatus;

(function (LanguageServerStatus) {
  LanguageServerStatus[LanguageServerStatus["NOT_INSTALLED"] = 0] = "NOT_INSTALLED";
  LanguageServerStatus[LanguageServerStatus["INSTALLING"] = 1] = "INSTALLING";
  LanguageServerStatus[LanguageServerStatus["READY"] = 2] = "READY";
  LanguageServerStatus[LanguageServerStatus["RUNNING"] = 3] = "RUNNING";
})(LanguageServerStatus || (exports.LanguageServerStatus = LanguageServerStatus = {}));

const CTAGS_SUPPORT_LANGS = ['c', 'cpp', 'clojure', 'csharp', 'css', 'go', 'html', 'ini', 'lua', 'json', 'objective-c', 'pascal', 'perl', 'php', 'python', 'r', 'ruby', 'rust', 'scheme', 'shell', 'sql', 'tcl', 'typescript', 'java', 'javascript'];
exports.CTAGS_SUPPORT_LANGS = CTAGS_SUPPORT_LANGS;