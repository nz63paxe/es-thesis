"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryUtils = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _gitUrlParse = _interopRequireDefault(require("git-url-parse"));

var _path = _interopRequireDefault(require("path"));

var _model = require("../model");

var _uri_util = require("./uri_util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class RepositoryUtils {
  // visible for tests
  // the max length of the hash part used to normalize the repository URI
  // as the normalized uri is used to create the name of code indices, and a valid index name cannot be longer than 255,
  // see https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html
  // the utf8 encoded max normalized name length cannot be greater than (255 - len(index_prefix) - len(index_suffix))
  // the index prefixes currently in use are '.code-document-', '.code-reference-', '.code-symbol-',
  // the index suffix currently in use is `-${version}`,
  // and also leave some room for future extensions
  // Generate a Repository instance by parsing repository remote url
  static buildRepository(remoteUrl) {
    const repo = (0, _gitUrlParse.default)(remoteUrl.trim());
    let host = repo.source ? repo.source : '';

    if (repo.port !== null) {
      host = host + ':' + repo.port;
    } // Remove the leading `/` and tailing `.git` if necessary.


    const pathname = repo.pathname && repo.git_suffix ? repo.pathname.substr(1, repo.pathname.length - 5) : repo.pathname.substr(1); // The pathname should look like `a/b/c` now.

    const segs = pathname.split('/').filter(s => s.length > 0);
    const org = segs.length >= 2 ? segs.slice(0, segs.length - 1).join('_') : '_';
    const name = segs.length >= 1 ? segs[segs.length - 1] : '_';
    const uri = host ? `${host}/${org}/${name}` : repo.full_name;
    return {
      uri,
      url: repo.href,
      name,
      org,
      protocol: repo.protocol
    };
  } // From uri 'origin/org/name' to 'org'


  static orgNameFromUri(repoUri) {
    const segs = repoUri.split('/');

    if (segs && segs.length === 3) {
      return segs[1];
    }

    throw new Error('Invalid repository uri.');
  } // From uri 'origin/org/name' to 'name'


  static repoNameFromUri(repoUri) {
    const segs = repoUri.split('/');

    if (segs && segs.length === 3) {
      return segs[2];
    }

    throw new Error('Invalid repository uri.');
  } // From uri 'origin/org/name' to 'org/name'


  static repoFullNameFromUri(repoUri) {
    const segs = repoUri.split('/');

    if (segs && segs.length === 3) {
      return segs[1] + '/' + segs[2];
    }

    throw new Error('Invalid repository uri.');
  } // Return the local data path of a given repository.


  static repositoryLocalPath(repoPath, repoUri) {
    return _path.default.join(repoPath, repoUri);
  }

  static normalizeRepoUriToIndexName(repoUri) {
    // the return value should be <capped readable repository name>-<hash after lowercased uri>
    // the repoUri is encoded in case there is non-ascii character
    const lcRepoUri = encodeURI(repoUri).toLowerCase(); // Following the unit test here in Elasticsearch repository:
    // https://github.com/elastic/elasticsearch/blob/c75773745cd048cd81a58c7d8a74272b45a25cc6/server/src/test/java/org/elasticsearch/cluster/metadata/MetaDataCreateIndexServiceTests.java#L404
    // the hash is calculated from the lowercased repoUri to make it case insensitive

    const hash = _crypto.default.createHash('md5').update(lcRepoUri).digest('hex').substring(0, RepositoryUtils.MAX_HASH_LEN); // Invalid chars in index can be found here:
    // https://github.com/elastic/elasticsearch/blob/237650e9c054149fd08213b38a81a3666c1868e5/server/src/main/java/org/elasticsearch/common/Strings.java#L376


    const normalizedUri = lcRepoUri.replace(/[/\\?%*:|"<> ,]/g, '-');
    const cappedNormalizedUri = normalizedUri.substr(0, RepositoryUtils.MAX_NORMALIZED_URI_LEN); // Elasticsearch index name is case insensitive

    return `${cappedNormalizedUri}-${hash}`.toLowerCase();
  }

  static locationToUrl(loc) {
    const url = (0, _uri_util.parseLspUrl)(loc.uri);
    const {
      repoUri,
      file,
      revision
    } = url;

    if (repoUri && file && revision) {
      return (0, _uri_util.toCanonicalUrl)({
        repoUri,
        file,
        revision,
        position: loc.range.start
      });
    }

    return '';
  }

  static getAllFiles(fileTree) {
    if (!fileTree) {
      return [];
    }

    let result = [];

    switch (fileTree.type) {
      case _model.FileTreeItemType.File:
        result.push(fileTree.path);
        break;

      case _model.FileTreeItemType.Directory:
        for (const node of fileTree.children) {
          result = result.concat(RepositoryUtils.getAllFiles(node));
        }

        break;

      default:
        break;
    }

    return result;
  }

  static hasFullyCloned(cloneProgress) {
    return !!cloneProgress && cloneProgress.isCloned !== undefined && cloneProgress.isCloned;
  }

}

exports.RepositoryUtils = RepositoryUtils;

_defineProperty(RepositoryUtils, "MAX_HASH_LEN", 8);

_defineProperty(RepositoryUtils, "MAX_NORMALIZED_NAME_LEN", 200);

_defineProperty(RepositoryUtils, "MAX_NORMALIZED_URI_LEN", RepositoryUtils.MAX_NORMALIZED_NAME_LEN - RepositoryUtils.MAX_HASH_LEN - 1);