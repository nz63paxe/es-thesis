"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseSchema = parseSchema;
exports.parseGoto = parseGoto;
exports.parseLspUrl = parseLspUrl;
exports.toRepoName = toRepoName;
exports.toRepoNameWithOrg = toRepoNameWithOrg;
exports.toCanonicalUrl = toCanonicalUrl;
exports.encodeRevisionString = exports.decodeRevisionString = void 0;

var _pathToRegexp = _interopRequireDefault(require("path-to-regexp"));

var _routes = require("../public/components/routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const mainRe = (0, _pathToRegexp.default)(_routes.MAIN);
const mainRootRe = (0, _pathToRegexp.default)(_routes.MAIN_ROOT);

function parseSchema(url) {
  let [schema, uri] = url.toString().split('//');

  if (!uri) {
    uri = schema; // @ts-ignore

    schema = undefined;
  }

  if (!uri.startsWith('/')) {
    uri = '/' + uri;
  }

  return {
    uri,
    schema
  };
}

function parseGoto(goto) {
  const regex = /L(\d+)(:\d+)?$/;
  const m = regex.exec(goto);

  if (m) {
    const line = parseInt(m[1], 10);
    let character = 0;

    if (m[2]) {
      character = parseInt(m[2].substring(1), 10);
    }

    return {
      line,
      character
    };
  }
}

function parseLspUrl(url) {
  const {
    schema,
    uri
  } = parseSchema(url.toString());
  const mainParsed = mainRe.exec(uri);
  const mainRootParsed = mainRootRe.exec(uri);

  if (mainParsed) {
    const [resource, org, repo, pathType, revision, file, goto] = mainParsed.slice(1);
    let position;

    if (goto) {
      position = parseGoto(goto);
    }

    return {
      uri: uri.replace(goto, ''),
      repoUri: `${resource}/${org}/${repo}`,
      pathType,
      revision,
      file,
      schema,
      position
    };
  } else if (mainRootParsed) {
    const [resource, org, repo, pathType, revision] = mainRootParsed.slice(1);
    return {
      uri,
      repoUri: `${resource}/${org}/${repo}`,
      pathType,
      revision,
      schema
    };
  } else {
    throw new Error('invalid url ' + url);
  }
}
/*
 * From RepositoryUri to repository name.
 * e.g. github.com/elastic/elasticsearch -> elasticsearch
 */


function toRepoName(uri) {
  const segs = uri.split('/');

  if (segs.length !== 3) {
    throw new Error(`Invalid repository uri ${uri}`);
  }

  return segs[2];
}
/*
 * From RepositoryUri to repository name with organization prefix.
 * e.g. github.com/elastic/elasticsearch -> elastic/elasticsearch
 */


function toRepoNameWithOrg(uri) {
  const segs = uri.split('/');

  if (segs.length !== 3) {
    throw new Error(`Invalid repository uri ${uri}`);
  }

  return `${segs[1]}/${segs[2]}`;
}

const compiled = _pathToRegexp.default.compile(_routes.MAIN);

function toCanonicalUrl(lspUrl) {
  const [resource, org, repo] = lspUrl.repoUri.split('/');

  if (!lspUrl.pathType) {
    lspUrl.pathType = 'blob';
  }

  let goto;

  if (lspUrl.position) {
    goto = `!L${lspUrl.position.line + 1}:${lspUrl.position.character}`;
  }

  const data = {
    resource,
    org,
    repo,
    path: lspUrl.file,
    goto,
    ...lspUrl
  };
  const uri = decodeURIComponent(compiled(data));
  return lspUrl.schema ? `${lspUrl.schema}/${uri}` : uri;
}

const decodeRevisionString = revision => {
  return revision.replace(/:/g, '/');
};

exports.decodeRevisionString = decodeRevisionString;

const encodeRevisionString = revision => {
  return revision.replace(/\//g, ':');
};

exports.encodeRevisionString = encodeRevisionString;