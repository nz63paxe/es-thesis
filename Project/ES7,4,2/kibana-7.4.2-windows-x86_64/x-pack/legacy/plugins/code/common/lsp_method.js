"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspMethod = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class LspMethod {
  constructor(method, client) {
    _defineProperty(this, "client", void 0);

    _defineProperty(this, "method", void 0);

    this.client = client;
    this.method = method;
  }

  asyncTask(input) {
    const abortController = new AbortController();

    const promise = () => {
      return this.client.sendRequest(this.method, input, abortController.signal).then(result => result.result);
    };

    return {
      cancel() {
        abortController.abort();
      },

      promise
    };
  }

  async send(input) {
    return await this.client.sendRequest(this.method, input).then(result => result.result);
  }

}

exports.LspMethod = LspMethod;