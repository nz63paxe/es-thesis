"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TEXT_FILE_LIMIT = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const TEXT_FILE_LIMIT = 1024 * 1024; // 1mb

exports.TEXT_FILE_LIMIT = TEXT_FILE_LIMIT;