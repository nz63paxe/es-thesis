"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.APP_USAGE_TYPE = exports.APP_TITLE = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const APP_TITLE = 'Code (Beta)';
exports.APP_TITLE = APP_TITLE;
const APP_USAGE_TYPE = 'code';
exports.APP_USAGE_TYPE = APP_USAGE_TYPE;