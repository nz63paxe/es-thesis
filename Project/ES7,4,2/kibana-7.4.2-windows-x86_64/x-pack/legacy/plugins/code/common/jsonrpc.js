"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResponseError = void 0;

var _lsp_error_codes = require("./lsp_error_codes");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Since vscode-jsonrpc can't be used under IE, we copy some type definitions from
// https://github.com/Microsoft/vscode-languageserver-node/blob/8801c20b/jsonrpc/src/messages.ts

/**
 * An error object return in a response in case a request
 * has failed.
 */
class ResponseError extends Error {
  constructor(code, message, data) {
    super(message);

    _defineProperty(this, "code", void 0);

    _defineProperty(this, "data", void 0);

    this.code = Number.isInteger(code) ? code : _lsp_error_codes.UnknownErrorCode;
    this.data = data;
    Object.setPrototypeOf(this, ResponseError.prototype);
  }

  toJson() {
    return {
      code: this.code,
      message: this.message,
      data: this.data
    };
  }

}

exports.ResponseError = ResponseError;