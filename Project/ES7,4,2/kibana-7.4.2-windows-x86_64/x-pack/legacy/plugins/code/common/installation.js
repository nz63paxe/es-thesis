"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InstallationType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let InstallationType;
exports.InstallationType = InstallationType;

(function (InstallationType) {
  InstallationType[InstallationType["Embed"] = 0] = "Embed";
  InstallationType[InstallationType["Plugin"] = 1] = "Plugin";
})(InstallationType || (exports.InstallationType = InstallationType = {}));