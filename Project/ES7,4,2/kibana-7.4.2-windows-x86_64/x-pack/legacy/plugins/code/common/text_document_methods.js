"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TextDocumentMethods = void 0;

var _lsp_method = require("./lsp_method");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class TextDocumentMethods {
  constructor(client) {
    _defineProperty(this, "documentSymbol", void 0);

    _defineProperty(this, "hover", void 0);

    _defineProperty(this, "definition", void 0);

    _defineProperty(this, "edefinition", void 0);

    _defineProperty(this, "references", void 0);

    _defineProperty(this, "client", void 0);

    this.client = client;
    this.documentSymbol = new _lsp_method.LspMethod('textDocument/documentSymbol', this.client);
    this.hover = new _lsp_method.LspMethod('textDocument/hover', this.client);
    this.definition = new _lsp_method.LspMethod('textDocument/definition', this.client);
    this.edefinition = new _lsp_method.LspMethod('textDocument/edefinition', this.client);
    this.references = new _lsp_method.LspMethod('textDocument/references', this.client);
  }

}

exports.TextDocumentMethods = TextDocumentMethods;