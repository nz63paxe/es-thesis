"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.code = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _path = require("path");

var _constants = require("./common/constants");

var _server = require("./server");

var _disk_watermark = require("./server/disk_watermark");

var _language_servers = require("./server/lsp/language_servers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const code = kibana => new kibana.Plugin({
  require: ['kibana', 'elasticsearch', 'xpack_main'],
  id: 'code',
  configPrefix: 'xpack.code',
  publicDir: (0, _path.resolve)(__dirname, 'public'),
  uiExports: {
    app: {
      title: _constants.APP_TITLE,
      main: 'plugins/code/index',
      euiIconType: 'codeApp'
    },
    styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),

    injectDefaultVars(server) {
      const config = server.config();
      return {
        codeUiEnabled: config.get('xpack.code.ui.enabled')
      };
    },

    hacks: ['plugins/code/hacks/toggle_app_link_in_nav']
  },

  config(Joi) {
    const langSwitches = {};

    _language_servers.LanguageServers.forEach(lang => {
      langSwitches[lang.name] = Joi.object({
        enabled: Joi.boolean().default(true)
      });
    });

    _language_servers.LanguageServersDeveloping.forEach(lang => {
      langSwitches[lang.name] = Joi.object({
        enabled: Joi.boolean().default(false)
      });
    });

    return Joi.object({
      ui: Joi.object({
        enabled: Joi.boolean().default(false)
      }).default(),
      enabled: Joi.boolean().default(true),
      queueIndex: Joi.string().default('.code_internal-worker-queue'),
      // 1 hour by default.
      queueTimeoutMs: Joi.number().default(_moment.default.duration(1, 'hour').asMilliseconds()),
      // The frequency which update scheduler executes. 1 minute by default.
      updateFrequencyMs: Joi.number().default(_moment.default.duration(1, 'minute').asMilliseconds()),
      // The frequency which index scheduler executes. 1 day by default.
      indexFrequencyMs: Joi.number().default(_moment.default.duration(1, 'day').asMilliseconds()),
      // The frequency which each repo tries to update. 5 minutes by default.
      updateRepoFrequencyMs: Joi.number().default(_moment.default.duration(5, 'minute').asMilliseconds()),
      // The frequency which each repo tries to index. 1 day by default.
      indexRepoFrequencyMs: Joi.number().default(_moment.default.duration(1, 'day').asMilliseconds()),
      // whether we want to show more logs
      verbose: Joi.boolean().default(false),
      lsp: Joi.object({ ...langSwitches,
        // timeout of a request
        requestTimeoutMs: Joi.number().default(_moment.default.duration(10, 'second').asMilliseconds()),
        // if we want the language server run in seperately
        detach: Joi.boolean().default(false),
        // enable oom_score_adj on linux
        oomScoreAdj: Joi.boolean().default(true)
      }).default(),
      repos: Joi.array().default([]),
      security: Joi.object({
        enableMavenImport: Joi.boolean().default(true),
        enableGradleImport: Joi.boolean().default(false),
        installGoDependency: Joi.boolean().default(false),
        installNodeDependency: Joi.boolean().default(true),
        gitHostWhitelist: Joi.array().items(Joi.string()).default(['github.com', 'gitlab.com', 'bitbucket.org', 'gitbox.apache.org', 'eclipse.org']),
        gitProtocolWhitelist: Joi.array().items(Joi.string()).default(['https', 'git', 'ssh']),
        enableGitCertCheck: Joi.boolean().default(true)
      }).default(),
      disk: Joi.object({
        thresholdEnabled: Joi.bool().default(true),
        watermarkLow: Joi.string().default(`${_disk_watermark.DEFAULT_WATERMARK_LOW_PERCENTAGE}%`)
      }).default(),
      maxWorkspace: Joi.number().default(5),
      // max workspace folder for each language server
      enableGlobalReference: Joi.boolean().default(false),
      // Global reference as optional feature for now
      enableCommitIndexing: Joi.boolean().default(false),
      codeNodeUrl: Joi.string(),
      clustering: Joi.object({
        enabled: Joi.bool().default(false),
        codeNodes: Joi.array().items(Joi.object({
          id: Joi.string(),
          address: Joi.string()
        })).default([])
      }).default()
    }).default();
  },

  init(server, options) {
    if (!options.ui.enabled) {
      return;
    }

    const initializerContext = {};
    const coreSetup = {
      http: {
        server
      }
    }; // Set up with the new platform plugin lifecycle API.

    const plugin = (0, _server.codePlugin)(initializerContext);
    plugin.setup(coreSetup, options); // @ts-ignore

    const kbnServer = this.kbnServer;
    kbnServer.ready().then(async () => {
      await plugin.start(coreSetup);
    });
    server.events.on('stop', async () => {
      await plugin.stop();
    });
  }

});

exports.code = code;