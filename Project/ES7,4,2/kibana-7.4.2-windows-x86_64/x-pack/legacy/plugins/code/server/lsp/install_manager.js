"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InstallManager = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _installation = require("../../common/installation");

var _language_server = require("../../common/language_server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class InstallManager {
  constructor(server, serverOptions) {
    this.server = server;
    this.serverOptions = serverOptions;
  }

  status(def) {
    if (def.installationType === _installation.InstallationType.Embed) {
      return _language_server.LanguageServerStatus.READY;
    } // @ts-ignore


    const plugin = this.server.plugins[def.installationPluginName];

    if (plugin) {
      const pluginPath = plugin.install.path;

      if (_fs.default.existsSync(pluginPath)) {
        return _language_server.LanguageServerStatus.READY;
      }
    }

    return _language_server.LanguageServerStatus.NOT_INSTALLED;
  }

  installationPath(def) {
    if (def.installationType === _installation.InstallationType.Embed) {
      return def.embedPath;
    } // @ts-ignore


    const plugin = this.server.plugins[def.installationPluginName];

    if (plugin) {
      return plugin.install.path;
    }

    return undefined;
  }

}

exports.InstallManager = InstallManager;