"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DocumentSearchClient = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _line_mapper = require("../../common/line_mapper");

var _schema = require("../indexer/schema");

var _composite_source_merger = require("../utils/composite_source_merger");

var _abstract_search_client = require("./abstract_search_client");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const HIT_MERGE_LINE_INTERVAL = 2; // Inclusive

const MAX_HIT_NUMBER = 5;

class DocumentSearchClient extends _abstract_search_client.AbstractSearchClient {
  constructor(client, log) {
    super(client, log);
    this.client = client;
    this.log = log;

    _defineProperty(this, "HIGHLIGHT_PRE_TAG", '_@-');

    _defineProperty(this, "HIGHLIGHT_POST_TAG", '-@_');

    _defineProperty(this, "LINE_SEPARATOR", '\n');
  }

  async search(req) {
    const resultsPerPage = this.getResultsPerPage(req);
    const from = (req.page - 1) * resultsPerPage;
    const size = resultsPerPage; // The query to search qname field.

    const qnameQuery = {
      constant_score: {
        filter: {
          match: {
            qnames: {
              query: req.query,
              operator: 'OR',
              prefix_length: 0,
              max_expansions: 50,
              fuzzy_transpositions: true,
              lenient: false,
              zero_terms_query: 'NONE',
              boost: 1.0
            }
          }
        },
        boost: 1.0
      }
    }; // The queries to search content and path filter.

    const contentQuery = {
      match: {
        content: req.query
      }
    };
    const pathQuery = {
      match: {
        path: req.query
      }
    }; // Post filters for repository

    let repositoryPostFilters = [];

    if (req.repoFilters) {
      repositoryPostFilters = req.repoFilters.map(repoUri => {
        return {
          term: {
            repoUri
          }
        };
      });
    } // Post filters for language


    let languagePostFilters = [];

    if (req.langFilters) {
      languagePostFilters = req.langFilters.map(lang => {
        return {
          term: {
            language: lang
          }
        };
      });
    }

    const index = req.repoScope ? (0, _schema.DocumentSearchIndexWithScope)(req.repoScope) : `${_schema.DocumentIndexNamePrefix}*`;
    const rawRes = await this.client.search({
      index,
      body: {
        from,
        size,
        query: {
          bool: {
            should: [qnameQuery, contentQuery, pathQuery],
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        },
        post_filter: {
          bool: {
            must: [{
              bool: {
                should: repositoryPostFilters,
                disable_coord: false,
                adjust_pure_negative: true,
                boost: 1.0
              }
            }, {
              bool: {
                should: languagePostFilters,
                disable_coord: false,
                adjust_pure_negative: true,
                boost: 1.0
              }
            }],
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        },
        aggregations: {
          repoUri: {
            terms: {
              field: 'repoUri',
              size: 10,
              min_doc_count: 1,
              shard_min_doc_count: 0,
              show_term_doc_count_error: false,
              order: [{
                _count: 'desc'
              }, {
                _key: 'asc'
              }]
            }
          },
          language: {
            terms: {
              field: 'language',
              size: 10,
              min_doc_count: 1,
              shard_min_doc_count: 0,
              show_term_doc_count_error: false,
              order: [{
                _count: 'desc'
              }, {
                _key: 'asc'
              }]
            }
          }
        },
        highlight: {
          // TODO: we might need to improve the highlighting separator.
          pre_tags: [this.HIGHLIGHT_PRE_TAG],
          post_tags: [this.HIGHLIGHT_POST_TAG],
          fields: {
            content: {},
            path: {}
          }
        }
      }
    });
    const hits = rawRes.hits.hits;
    const aggregations = rawRes.aggregations;
    const results = hits.map(hit => {
      const doc = hit._source;
      const {
        repoUri,
        path,
        language
      } = doc;
      let termContent = []; // Similar to https://github.com/lambdalab/lambdalab/blob/master/services/liaceservice/src/main/scala/com/lambdalab/liaceservice/LiaceServiceImpl.scala#L147
      // Might need refactoring.

      if (hit.highlight) {
        const highlightContent = hit.highlight.content;

        if (highlightContent) {
          highlightContent.forEach(c => {
            termContent = termContent.concat(this.extractKeywords(c));
          });
        }
      }

      const hitsContent = this.termsToHits(doc.content, termContent);
      const sourceContent = this.getSourceContent(hitsContent, doc);
      const item = {
        uri: repoUri,
        filePath: path,
        language: language,
        hits: hitsContent.length,
        compositeContent: sourceContent
      };
      return item;
    });
    const total = rawRes.hits.total.value;
    return {
      query: req.query,
      from,
      page: req.page,
      totalPage: Math.ceil(total / resultsPerPage),
      results,
      repoAggregations: aggregations.repoUri.buckets,
      langAggregations: aggregations.language.buckets,
      took: rawRes.took,
      total
    };
  }

  async suggest(req) {
    const resultsPerPage = this.getResultsPerPage(req);
    const from = (req.page - 1) * resultsPerPage;
    const size = resultsPerPage;
    const index = req.repoScope ? (0, _schema.DocumentSearchIndexWithScope)(req.repoScope) : `${_schema.DocumentIndexNamePrefix}*`;
    const queryStr = req.query.toLowerCase();
    const rawRes = await this.client.search({
      index,
      body: {
        from,
        size,
        query: {
          bool: {
            should: [{
              prefix: {
                'path.hierarchy': {
                  value: queryStr,
                  boost: 1.0
                }
              }
            }, {
              term: {
                'path.hierarchy': {
                  value: queryStr,
                  boost: 10.0
                }
              }
            }],
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        }
      }
    });
    const hits = rawRes.hits.hits;
    const results = hits.map(hit => {
      const doc = hit._source;
      const {
        repoUri,
        path,
        language
      } = doc;
      const item = {
        uri: repoUri,
        filePath: path,
        language: language,
        hits: 0,
        compositeContent: {
          content: '',
          lineMapping: [],
          ranges: []
        }
      };
      return item;
    });
    const total = rawRes.hits.total.value;
    return {
      query: req.query,
      from,
      page: req.page,
      totalPage: Math.ceil(total / resultsPerPage),
      results,
      took: rawRes.took,
      total
    };
  }

  getSourceContent(hitsContent, doc) {
    const docInLines = doc.content.split(this.LINE_SEPARATOR);
    let slicedRanges = [];

    if (hitsContent.length === 0) {
      // Always add a placeholder range of the first line so that for filepath
      // matching search result, we will render some file content.
      slicedRanges = [{
        startLine: 0,
        endLine: 0
      }];
    } else {
      const slicedHighlights = hitsContent.slice(0, MAX_HIT_NUMBER);
      slicedRanges = slicedHighlights.map(hit => ({
        startLine: hit.range.startLoc.line,
        endLine: hit.range.endLoc.line
      }));
    }

    const expandedRanges = (0, _composite_source_merger.expandRanges)(slicedRanges, HIT_MERGE_LINE_INTERVAL);
    const mergedRanges = (0, _composite_source_merger.mergeRanges)(expandedRanges);
    const lineMapping = new _composite_source_merger.LineMapping();
    const result = (0, _composite_source_merger.extractSourceContent)(mergedRanges, docInLines, lineMapping);
    const ranges = hitsContent.filter(hit => lineMapping.hasLine(hit.range.startLoc.line)).map(hit => ({
      startColumn: hit.range.startLoc.column + 1,
      startLineNumber: lineMapping.lineNumber(hit.range.startLoc.line),
      endColumn: hit.range.endLoc.column + 1,
      endLineNumber: lineMapping.lineNumber(hit.range.endLoc.line)
    }));
    return {
      content: result.join(this.LINE_SEPARATOR),
      lineMapping: lineMapping.toStringArray(),
      ranges
    };
  }

  termsToHits(source, terms) {
    // Dedup search terms by using Set.
    const filteredTerms = new Set(terms.filter(t => t.trim().length > 0).map(t => _lodash.default.escapeRegExp(t)));

    if (filteredTerms.size === 0) {
      return [];
    }

    const lineMapper = new _line_mapper.LineMapper(source);
    const regex = new RegExp(`(${Array.from(filteredTerms.values()).join('|')})`, 'g');
    let match;
    const hits = [];

    do {
      match = regex.exec(source);

      if (match) {
        const begin = match.index;
        const end = regex.lastIndex;
        const startLoc = lineMapper.getLocation(begin);
        const endLoc = lineMapper.getLocation(end);
        const range = {
          startLoc,
          endLoc
        };
        const hit = {
          range,
          score: 0.0,
          term: match[1]
        };
        hits.push(hit);
      }
    } while (match);

    return hits;
  }

  extractKeywords(text) {
    if (!text) {
      return [];
    } else {
      // console.log(text);
      const keywordRegex = new RegExp(`${this.HIGHLIGHT_PRE_TAG}(\\w*)${this.HIGHLIGHT_POST_TAG}`, 'g');
      const keywords = text.match(keywordRegex);

      if (keywords) {
        return keywords.map(k => {
          return k.replace(new RegExp(this.HIGHLIGHT_PRE_TAG, 'g'), '').replace(new RegExp(this.HIGHLIGHT_POST_TAG, 'g'), '');
        });
      } else {
        return [];
      }
    }
  }

}

exports.DocumentSearchClient = DocumentSearchClient;