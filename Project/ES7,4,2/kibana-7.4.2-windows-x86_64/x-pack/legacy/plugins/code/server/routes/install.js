"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.installRoute = installRoute;

var Boom = _interopRequireWildcard(require("boom"));

var _language_servers = require("../lsp/language_servers");

var _apis = require("../distributed/apis");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function installRoute(router, codeServices) {
  const lspService = codeServices.serviceFor(_apis.LspServiceDefinition);
  const kibanaVersion = router.server.config().get('pkg.version');

  const status = async (endpoint, def) => ({
    name: def.name,
    status: await lspService.languageServerStatus(endpoint, {
      langName: def.name
    }),
    version: def.version,
    build: def.build,
    languages: def.languages,
    installationType: def.installationType,
    downloadUrl: typeof def.downloadUrl === 'function' ? def.downloadUrl(kibanaVersion) : def.downloadUrl,
    pluginName: def.installationPluginName
  });

  router.route({
    path: '/api/code/install',

    async handler(req) {
      const endpoint = await codeServices.locate(req, '');
      return await Promise.all((0, _language_servers.enabledLanguageServers)(router.server).map(def => status(endpoint, def)));
    },

    method: 'GET'
  });
  router.route({
    path: '/api/code/install/{name}',

    async handler(req) {
      const name = req.params.name;
      const def = (0, _language_servers.enabledLanguageServers)(router.server).find(d => d.name === name);
      const endpoint = await codeServices.locate(req, '');

      if (def) {
        return await status(endpoint, def);
      } else {
        return Boom.notFound(`language server ${name} not found.`);
      }
    },

    method: 'GET'
  });
}