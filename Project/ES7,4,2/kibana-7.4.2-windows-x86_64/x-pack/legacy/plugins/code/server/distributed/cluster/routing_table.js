"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RoutingTable = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * A snapshot of the routing table, which defines on which node each repository resides.
 *
 * Currently, the relationship between nodes and repositories is one to many.
 */
class RoutingTable {
  constructor(assignments = []) {
    this.assignments = assignments;

    _defineProperty(this, "node2Repos", void 0);

    _defineProperty(this, "repo2Node", void 0);

    this.repo2Node = new Map(assignments.map(t => [t.resource, t.nodeId]));
    this.node2Repos = assignments.reduce(function (map, assignment) {
      let arr = map.get(assignment.nodeId);

      if (!arr) {
        arr = [];
        map.set(assignment.nodeId, arr);
      }

      arr.push(assignment.resource);
      return map;
    }, new Map());
  }

  assign(assignments) {
    return new RoutingTable(this.assignments.concat(assignments));
  }
  /**
   * return a new routing table without given repositories
   */


  withoutRepositories(repoUris) {
    return new RoutingTable(this.assignments.filter(assignment => !repoUris.has(assignment.resource)));
  }

  nodeIds() {
    return Array.from(this.node2Repos.keys());
  }

  repositoryURIs() {
    return Array.from(this.repo2Node.keys());
  }

  getNodeIdByRepositoryURI(repoURI) {
    return this.repo2Node.get(repoURI);
  }

  getRepositoryURIsByNodeId(nodeId) {
    return this.node2Repos.get(nodeId) || [];
  }

  toString() {
    let str = '[';
    let first = true;
    this.repo2Node.forEach((v, k, m) => {
      if (first) {
        first = false;
      } else {
        str += ', ';
      }

      str += `${k}: ${v}`;
    });
    return str + ']';
  }

}

exports.RoutingTable = RoutingTable;