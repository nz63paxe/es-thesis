"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeNodeEndpoint = void 0;

var _local_endpoint = require("../local_endpoint");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CodeNodeEndpoint extends _local_endpoint.LocalEndpoint {
  constructor(httpRequest, resource, codeNodeUrl) {
    super(httpRequest, resource);
    this.httpRequest = httpRequest;
    this.resource = resource;
    this.codeNodeUrl = codeNodeUrl;
  }

}

exports.CodeNodeEndpoint = CodeNodeEndpoint;