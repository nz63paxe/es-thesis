"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryServiceFactory = void 0;

var _repository_service = require("./repository_service");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class RepositoryServiceFactory {
  newInstance(repoPath, credsPath, log, enableGitCertCheck) {
    return new _repository_service.RepositoryService(repoPath, credsPath, log, enableGitCertCheck);
  }

}

exports.RepositoryServiceFactory = RepositoryServiceFactory;