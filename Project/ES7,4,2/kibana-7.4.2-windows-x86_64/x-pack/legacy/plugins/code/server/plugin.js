"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodePlugin = void 0;

var _i18n = require("@kbn/i18n");

var _crypto = _interopRequireDefault(require("crypto"));

var _ = _interopRequireWildcard(require("lodash"));

var _indexer = require("./indexer");

var _log = require("./log");

var _language_servers = require("./lsp/language_servers");

var _security = require("./security");

var _server_options = require("./server_options");

var _routes = require("./routes");

var _code_services = require("./distributed/code_services");

var _code_node_adapter = require("./distributed/multinode/code_node_adapter");

var _local_handler_adapter = require("./distributed/local_handler_adapter");

var _non_code_node_adapter = require("./distributed/multinode/non_code_node_adapter");

var _apis = require("./distributed/apis");

var _init_es = require("./init_es");

var _init_local = require("./init_local");

var _init_queue = require("./init_queue");

var _init_workers = require("./init_workers");

var _cluster_node_adapter = require("./distributed/cluster/cluster_node_adapter");

var _usage_collector = require("./usage_collector");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class CodePlugin {
  constructor(initializerContext) {
    _defineProperty(this, "isCodeNode", false);

    _defineProperty(this, "gitOps", null);

    _defineProperty(this, "queue", null);

    _defineProperty(this, "log", void 0);

    _defineProperty(this, "serverOptions", void 0);

    _defineProperty(this, "indexScheduler", null);

    _defineProperty(this, "updateScheduler", null);

    _defineProperty(this, "lspService", null);

    _defineProperty(this, "codeServices", null);

    this.log = {};
    this.serverOptions = {};
  } // TODO: options is not a valid param for the setup() api
  // of the new platform. Will need to pass through the configs
  // correctly in the new platform.


  setup(core, options) {
    const {
      server
    } = core.http;
    this.log = new _log.Logger(server);
    this.serverOptions = new _server_options.ServerOptions(options, server.config());
    const xpackMainPlugin = server.plugins.xpack_main;
    xpackMainPlugin.registerFeature({
      id: 'code',
      name: _i18n.i18n.translate('xpack.code.featureRegistry.codeFeatureName', {
        defaultMessage: 'Code'
      }),
      icon: 'codeApp',
      navLinkId: 'code',
      app: ['code', 'kibana'],
      catalogue: [],
      // TODO add catalogue here
      privileges: {
        all: {
          excludeFromBasePrivileges: true,
          api: ['code_user', 'code_admin'],
          savedObject: {
            all: [],
            read: ['config']
          },
          ui: ['show', 'user', 'admin']
        },
        read: {
          api: ['code_user'],
          savedObject: {
            all: [],
            read: ['config']
          },
          ui: ['show', 'user']
        }
      }
    });
  } // TODO: CodeStart will not have the register route api.
  // Let's make it CoreSetup as the param for now.


  async start(core) {
    // called after all plugins are set up
    const {
      server
    } = core.http;
    const codeServerRouter = new _security.CodeServerRouter(server);
    const codeNodeUrl = this.serverOptions.codeNodeUrl;

    const rndString = _crypto.default.randomBytes(20).toString('hex');

    (0, _routes.checkRoute)(server, rndString);

    if (this.serverOptions.clusterEnabled) {
      this.initDevMode(server);
      this.codeServices = await this.initClusterNode(server, codeServerRouter);
    } else if (codeNodeUrl) {
      const checkResult = await this.retryUntilAvailable(async () => await (0, _routes.checkCodeNode)(codeNodeUrl, this.log, rndString), 5000);

      if (checkResult.me) {
        const codeServices = new _code_services.CodeServices(new _code_node_adapter.CodeNodeAdapter(codeServerRouter, this.log));
        this.log.info('Initializing Code plugin as code-node.');
        this.codeServices = await this.initCodeNode(server, codeServices);
      } else {
        this.codeServices = await this.initNonCodeNode(codeNodeUrl, core);
      }
    } else {
      const codeServices = new _code_services.CodeServices(new _local_handler_adapter.LocalHandlerAdapter()); // codeNodeUrl not set, single node mode

      this.log.info('Initializing Code plugin as single-node.');
      this.initDevMode(server);
      this.codeServices = await this.initCodeNode(server, codeServices);
    }

    await this.codeServices.start();
  }

  async initClusterNode(server, codeServerRouter) {
    this.log.info('Initializing Code plugin as cluster-node');
    const {
      esClient,
      repoConfigController,
      repoIndexInitializerFactory
    } = await (0, _init_es.initEs)(server, this.log);
    const codeServices = new _code_services.CodeServices(new _cluster_node_adapter.ClusterNodeAdapter(codeServerRouter, this.log, this.serverOptions, esClient));
    this.queue = (0, _init_queue.initQueue)(server, this.log, esClient);
    const {
      gitOps,
      lspService
    } = (0, _init_local.initLocalService)(server, this.log, this.serverOptions, codeServices, esClient, repoConfigController);
    this.lspService = lspService;
    this.gitOps = gitOps;
    const {
      indexScheduler,
      updateScheduler
    } = (0, _init_workers.initWorkers)(server, this.log, esClient, this.queue, lspService, gitOps, this.serverOptions, codeServices);
    this.indexScheduler = indexScheduler;
    this.updateScheduler = updateScheduler; // Execute index version checking and try to migrate index data if necessary.

    await (0, _indexer.tryMigrateIndices)(esClient, this.log);
    this.initRoutes(server, codeServices, repoIndexInitializerFactory, repoConfigController);
    return codeServices;
  }

  async initCodeNode(server, codeServices) {
    this.isCodeNode = true;
    const {
      esClient,
      repoConfigController,
      repoIndexInitializerFactory
    } = await (0, _init_es.initEs)(server, this.log);
    this.queue = (0, _init_queue.initQueue)(server, this.log, esClient);
    const {
      gitOps,
      lspService
    } = (0, _init_local.initLocalService)(server, this.log, this.serverOptions, codeServices, esClient, repoConfigController);
    this.lspService = lspService;
    this.gitOps = gitOps;
    const {
      indexScheduler,
      updateScheduler
    } = (0, _init_workers.initWorkers)(server, this.log, esClient, this.queue, lspService, gitOps, this.serverOptions, codeServices);
    this.indexScheduler = indexScheduler;
    this.updateScheduler = updateScheduler; // Execute index version checking and try to migrate index data if necessary.

    await (0, _indexer.tryMigrateIndices)(esClient, this.log);
    this.initRoutes(server, codeServices, repoIndexInitializerFactory, repoConfigController); // TODO: extend the usage collection to cluster mode.

    (0, _usage_collector.initCodeUsageCollector)(server, esClient, lspService);
    return codeServices;
  }

  async stop() {
    if (this.isCodeNode) {
      if (this.gitOps) await this.gitOps.cleanAllRepo();
      if (this.indexScheduler) this.indexScheduler.stop();
      if (this.updateScheduler) this.updateScheduler.stop();
      if (this.queue) this.queue.destroy();
      if (this.lspService) await this.lspService.shutdown();
    }

    if (this.codeServices) {
      await this.codeServices.stop();
    }
  }

  async initNonCodeNode(url, core) {
    const {
      server
    } = core.http;
    this.log.info(`Initializing Code plugin as non-code node, redirecting all code requests to ${url}`);
    const codeServices = new _code_services.CodeServices(new _non_code_node_adapter.NonCodeNodeAdapter(url, this.log));
    codeServices.registerHandler(_apis.GitServiceDefinition, null, _apis.GitServiceDefinitionOption);
    codeServices.registerHandler(_apis.RepositoryServiceDefinition, null);
    codeServices.registerHandler(_apis.LspServiceDefinition, null, _apis.LspServiceDefinitionOption);
    codeServices.registerHandler(_apis.WorkspaceDefinition, null);
    codeServices.registerHandler(_apis.SetupDefinition, null);
    const {
      repoConfigController,
      repoIndexInitializerFactory
    } = await (0, _init_es.initEs)(server, this.log);
    this.initRoutes(server, codeServices, repoIndexInitializerFactory, repoConfigController);
    return codeServices;
  }

  async initRoutes(server, codeServices, repoIndexInitializerFactory, repoConfigController) {
    const codeServerRouter = new _security.CodeServerRouter(server);
    (0, _routes.repositoryRoute)(codeServerRouter, codeServices, repoIndexInitializerFactory, repoConfigController, this.serverOptions);
    (0, _routes.repositorySearchRoute)(codeServerRouter, this.log);

    if (this.serverOptions.enableCommitIndexing) {
      (0, _routes.commitSearchRoute)(codeServerRouter, this.log);
    }

    (0, _routes.documentSearchRoute)(codeServerRouter, this.log);
    (0, _routes.symbolSearchRoute)(codeServerRouter, this.log);
    (0, _routes.fileRoute)(codeServerRouter, codeServices);
    (0, _routes.workspaceRoute)(codeServerRouter, this.serverOptions, codeServices);
    (0, _routes.symbolByQnameRoute)(codeServerRouter, this.log);
    (0, _routes.installRoute)(codeServerRouter, codeServices);
    (0, _routes.lspRoute)(codeServerRouter, codeServices, this.serverOptions);
    (0, _routes.setupRoute)(codeServerRouter, codeServices);
    (0, _routes.statusRoute)(codeServerRouter, codeServices);
  }

  async retryUntilAvailable(func, intervalMs, retries = Number.MAX_VALUE) {
    const value = await func();

    if (value) {
      return value;
    } else {
      const promise = new Promise(resolve => {
        const retry = () => {
          func().then(v => {
            if (v) {
              resolve(v);
            } else {
              retries--;

              if (retries > 0) {
                setTimeout(retry, intervalMs);
              } else {
                resolve(v);
              }
            }
          });
        };

        setTimeout(retry, intervalMs);
      });
      return await promise;
    }
  }

  initDevMode(server) {
    // @ts-ignore
    const devMode = server.config().get('env.dev');
    server.injectUiAppVars('code', () => ({
      enableLangserversDeveloping: devMode
    })); // Enable the developing language servers in development mode.

    if (devMode) {
      _language_servers.JAVA.downloadUrl = _.partialRight(_language_servers.JAVA.downloadUrl, devMode);
    }
  }

}

exports.CodePlugin = CodePlugin;