"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConfigClusterMembershipService = void 0;

var _util = _interopRequireDefault(require("util"));

var _code_nodes = require("./code_nodes");

var _cluster_state = require("./cluster_state");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ConfigClusterMembershipService {
  constructor(options, clusterService, log) {
    this.options = options;
    this.clusterService = clusterService;
    this.log = log;

    _defineProperty(this, "nodes", void 0);

    _defineProperty(this, "localNode", void 0);

    this.localNode = {
      id: options.serverUUID,
      address: options.localAddress
    }; // if no nodes are configured, add the local node, to be compatible with the single node mode

    if (this.options.codeNodes.length > 0) {
      this.nodes = new _code_nodes.CodeNodes(this.options.codeNodes);
    } else {
      this.nodes = new _code_nodes.CodeNodes([this.localNode]);
    }

    this.log.info(`Joined node [${_util.default.inspect(this.localNode)}] to cluster ${_util.default.inspect(this.nodes.nodes)}`);
  }

  async start() {
    const state = this.clusterService.state();
    this.clusterService.setClusterState(new _cluster_state.ClusterState(state.clusterMeta, state.routingTable, this.nodes));
  }

  async stop() {}

}

exports.ConfigClusterMembershipService = ConfigClusterMembershipService;