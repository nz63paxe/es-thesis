"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WorkspaceCommand = void 0;

var _child_process = require("child_process");

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _properLockfile = _interopRequireDefault(require("proper-lockfile"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class WorkspaceCommand {
  constructor(repoConfig, workspaceDir, revision, log) {
    this.repoConfig = repoConfig;
    this.workspaceDir = workspaceDir;
    this.revision = revision;
    this.log = log;
  }

  async runInit(force) {
    if (this.repoConfig.init) {
      const versionFile = _path.default.join(this.workspaceDir, 'init.version');

      if (this.checkRevision(versionFile) && !force) {
        this.log.info('a same revision exists, init cmd skipped.');
        return;
      }

      const lockFile = this.workspaceDir; // path.join(this.workspaceDir, 'init.lock');

      const isLocked = await _properLockfile.default.check(lockFile);

      if (isLocked) {
        this.log.info('another process is running, please try again later');
        return;
      }

      const release = await _properLockfile.default.lock(lockFile);

      try {
        if (!this.repoConfig.hasOwnProperty('init')) {
          throw new Error(`RepoConfig's init comes from a prototype, this is unexpected and unsupported`);
        }

        const process = this.spawnProcess(this.repoConfig.init);

        const logFile = _path.default.join(this.workspaceDir, 'init.log');

        const logFileStream = _fs.default.createWriteStream(logFile, {
          encoding: 'utf-8',
          flags: 'a+'
        });

        this.redirectOutput(process.stdout, logFileStream);
        this.redirectOutput(process.stderr, logFileStream, true);
        process.on('close', async (code, signal) => {
          logFileStream.close();
          await this.writeRevisionFile(versionFile);
          this.log.info(`init process finished with code: ${code} signal: ${signal}`);
          await release();
        });
      } catch (e) {
        this.log.error(e);
        release();
      }
    }
  }

  spawnProcess(repoCmd) {
    let cmd;
    let args;

    if (typeof repoCmd === 'string') {
      cmd = repoCmd;
      args = [];
    } else {
      [cmd, ...args] = repoCmd;
    }

    return (0, _child_process.spawn)(cmd, args, {
      detached: false,
      cwd: this.workspaceDir
    });
  }

  redirectOutput(from, to, isError = false) {
    from.on('data', data => {
      if (isError) {
        this.log.error(data.toString('utf-8'));
      } else {
        this.log.info(data.toString('utf-8'));
      }

      to.write(data);
    });
  }
  /**
   * check the revision file in workspace, return true if it exists and its content equals current revision.
   * @param versionFile
   */


  checkRevision(versionFile) {
    if (_fs.default.existsSync(versionFile)) {
      const revision = _fs.default.readFileSync(versionFile, 'utf8').trim();

      return revision === this.revision;
    }

    return false;
  }

  writeRevisionFile(versionFile) {
    return new Promise((resolve, reject) => {
      _fs.default.writeFile(versionFile, this.revision, err => {
        if (err) {
          reject(err);
        }

        resolve();
      });
    });
  }

}

exports.WorkspaceCommand = WorkspaceCommand;