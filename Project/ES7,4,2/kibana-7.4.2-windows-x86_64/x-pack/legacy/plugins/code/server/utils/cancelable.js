"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Cancelable = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class Cancelable {
  constructor(fn) {
    this.fn = fn;

    _defineProperty(this, "promise", void 0);

    _defineProperty(this, "resolve", undefined);

    _defineProperty(this, "reject", undefined);

    _defineProperty(this, "_cancel", undefined);

    _defineProperty(this, "resolved", false);

    this.promise = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    }).then(t => {
      this.resolved = true;
      return t;
    });
    fn(this.resolve, this.reject, cancel => {
      this._cancel = cancel;
    });
  }

  cancel(error = 'canceled') {
    if (!this.resolved) {
      if (this._cancel) {
        this._cancel(error);
      } else if (this.reject) {
        this.reject(error);
      }
    }
  }

  error(error) {
    if (this.reject) {
      this.reject(error);
    }
  }

  static fromPromise(promise) {
    return new Cancelable((resolve, reject, c) => {
      promise.then(resolve, reject);
    });
  }

}

exports.Cancelable = Cancelable;