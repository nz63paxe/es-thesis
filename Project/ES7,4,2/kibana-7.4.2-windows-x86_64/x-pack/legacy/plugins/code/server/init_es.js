"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initEs = initEs;

var _indexer = require("./indexer");

var _repository_config_controller = require("./repository_config_controller");

var _esclient_with_internal_request = require("./utils/esclient_with_internal_request");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function initEs(server, log) {
  // wait until elasticsearch is ready
  await server.plugins.elasticsearch.waitUntilReady();
  const esClient = new _esclient_with_internal_request.EsClientWithInternalRequest(server);
  const repoConfigController = new _repository_config_controller.RepositoryConfigController(esClient);
  const repoIndexInitializerFactory = new _indexer.RepositoryIndexInitializerFactory(esClient, log);
  return {
    esClient,
    repoConfigController,
    repoIndexInitializerFactory
  };
}