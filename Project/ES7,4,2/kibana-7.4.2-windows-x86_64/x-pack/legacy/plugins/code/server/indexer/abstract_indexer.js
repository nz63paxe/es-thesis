"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractIndexer = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _model = require("../../model");

var _index_stats_aggregator = require("../utils/index_stats_aggregator");

var _index_creator = require("./index_creator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AbstractIndexer {
  constructor(repoUri, revision, client, log) {
    this.repoUri = repoUri;
    this.revision = revision;
    this.client = client;
    this.log = log;

    _defineProperty(this, "type", _model.IndexerType.UNKNOWN);

    _defineProperty(this, "cancelled", false);

    _defineProperty(this, "indexCreator", void 0);

    _defineProperty(this, "INDEXER_PROGRESS_UPDATE_INTERVAL_MS", 1000);

    this.log = log.addTags(['indexer', this.type]);
    this.indexCreator = new _index_creator.IndexCreator(client);
  }

  async start(progressReporter, checkpointReq) {
    this.log.info(`Indexer ${this.type} started for repo ${this.repoUri} with revision ${this.revision}`);
    const isCheckpointValid = this.validateCheckpoint(checkpointReq);

    if (this.needRefreshIndices(checkpointReq)) {
      // Prepare the ES index
      const res = await this.prepareIndex();

      if (!res) {
        this.log.error(`Prepare index for ${this.repoUri} error. Skip indexing.`);
        return new Map();
      } // Clean up the index if necessary


      await this.cleanIndex();
    } // Prepare all the index requests


    let totalCount = 0;
    let prevTimestamp = (0, _moment.default)(0);
    let successCount = 0;
    let failCount = 0;
    const statsBuffer = [];

    try {
      totalCount = await this.getIndexRequestCount();
    } catch (error) {
      this.log.error(`Get index request count for ${this.repoUri} error.`);
      this.log.error(error);
      throw error;
    }

    let meetCheckpoint = false;
    const reqsIterator = await this.getIndexRequestIterator();

    for await (const req of reqsIterator) {
      if (this.isCancelled()) {
        this.log.info(`Indexer cancelled. Stop right now.`);
        break;
      } // If checkpoint is valid and has not been met


      if (isCheckpointValid && !meetCheckpoint) {
        meetCheckpoint = meetCheckpoint || this.ifCheckpointMet(req, checkpointReq);

        if (!meetCheckpoint) {
          // If the checkpoint has not been met yet, skip current request.
          continue;
        } else {
          this.log.info(`Checkpoint met. Continue with indexing.`);
        }
      }

      try {
        const stats = await this.processRequest(req);
        statsBuffer.push(stats);
        successCount += 1;
      } catch (error) {
        this.log.error(`Process index request error. ${error}`);
        failCount += 1;
      } // Double check if the the indexer is cancelled or not, because the
      // processRequest process could take fairly long and during this time
      // the index job might have been cancelled already. In this case,
      // we shall not update the progress.


      if (!this.isCancelled() && progressReporter) {
        this.log.debug(`Update progress for ${this.type} indexer.`); // Update progress if progress reporter has been provided.

        const progress = {
          type: this.type,
          total: totalCount,
          success: successCount,
          fail: failCount,
          percentage: Math.floor(100 * (successCount + failCount) / totalCount),
          checkpoint: req
        };

        if ((0, _moment.default)().diff(prevTimestamp) > this.INDEXER_PROGRESS_UPDATE_INTERVAL_MS || // Ensure that the progress reporter always executed at the end of the job.
        successCount + failCount === totalCount) {
          progressReporter(progress);
          prevTimestamp = (0, _moment.default)();
        }
      }
    }

    return (0, _index_stats_aggregator.aggregateIndexStats)(statsBuffer);
  }

  cancel() {
    this.cancelled = true;
  }

  isCancelled() {
    return this.cancelled;
  } // If the current checkpoint is valid


  validateCheckpoint(checkpointReq) {
    return checkpointReq !== undefined;
  } // If it's necessary to refresh (create and reset) all the related indices


  needRefreshIndices(checkpointReq) {
    return false;
  }

  ifCheckpointMet(req, checkpointReq) {
    // Please override this function
    return false;
  }

  async cleanIndex() {
    // This is the abstract implementation. You should override this.
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  async *getIndexRequestIterator() {// This is the abstract implementation. You should override this.
  }

  async getIndexRequestCount() {
    // This is the abstract implementation. You should override this.
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  async processRequest(request) {
    // This is the abstract implementation. You should override this.
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  async prepareIndexCreationRequests() {
    // This is the abstract implementation. You should override this.
    return new Promise((resolve, reject) => {
      resolve();
    });
  }

  async prepareIndex() {
    const creationReqs = await this.prepareIndexCreationRequests();

    for (const req of creationReqs) {
      try {
        const res = await this.indexCreator.createIndex(req);

        if (!res) {
          this.log.info(`Index creation failed for ${req.index}.`);
          return false;
        }
      } catch (error) {
        this.log.error(`Index creation error.`);
        this.log.error(error);
        return false;
      }
    }

    return true;
  }

}

exports.AbstractIndexer = AbstractIndexer;