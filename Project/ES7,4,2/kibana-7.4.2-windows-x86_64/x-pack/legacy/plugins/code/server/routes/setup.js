"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupRoute = setupRoute;

var _apis = require("../distributed/apis");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function setupRoute(router, codeServices) {
  const setupService = codeServices.serviceFor(_apis.SetupDefinition);
  router.route({
    method: 'get',
    path: '/api/code/setup',

    async handler(req) {
      const endpoint = await codeServices.locate(req, '');
      return await setupService.setup(endpoint, {});
    }

  });
}