"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReferenceIndexName = exports.ReferenceIndexNamePrefix = exports.ReferenceSchema = void 0;

var _repository_utils = require("../../../common/repository_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ReferenceSchema = {
  category: {
    type: 'keyword'
  },
  location: {
    properties: {
      uri: {
        type: 'text'
      }
    }
  },
  symbol: {
    properties: {
      name: {
        type: 'text'
      },
      kind: {
        type: 'keyword'
      },
      location: {
        properties: {
          uri: {
            type: 'text'
          }
        }
      }
    }
  }
};
exports.ReferenceSchema = ReferenceSchema;
const ReferenceIndexNamePrefix = `.code-reference`;
exports.ReferenceIndexNamePrefix = ReferenceIndexNamePrefix;

const ReferenceIndexName = repoUri => {
  return `${ReferenceIndexNamePrefix}-${_repository_utils.RepositoryUtils.normalizeRepoUriToIndexName(repoUri)}`;
};

exports.ReferenceIndexName = ReferenceIndexName;