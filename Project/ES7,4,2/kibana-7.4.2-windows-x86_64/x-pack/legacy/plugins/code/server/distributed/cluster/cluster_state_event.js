"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterStateEvent = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * An object indicates cluster state changes.
 */
class ClusterStateEvent {
  constructor(current, prev) {
    this.current = current;
    this.prev = prev;
  }

}

exports.ClusterStateEvent = ClusterStateEvent;