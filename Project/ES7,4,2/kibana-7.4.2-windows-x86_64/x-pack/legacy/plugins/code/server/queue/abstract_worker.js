"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractWorker = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _model = require("../../model");

var _esqueue = require("../lib/esqueue");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AbstractWorker {
  // The id of the worker. Also serves as the id of the job this worker consumes.
  constructor(queue, log) {
    this.queue = queue;
    this.log = log;

    _defineProperty(this, "id", '');
  } // Assemble jobs, for now most of the job object construction should be the same.


  async createJob(payload, options) {
    const timestamp = (0, _moment.default)().valueOf();

    if (options.timeout !== undefined && options.timeout !== null) {
      // If the job explicitly specify the timeout, then honor it.
      return {
        payload,
        options,
        timestamp
      };
    } else {
      // Otherwise, use a default timeout.
      const timeout = await this.getTimeoutMs(payload);
      return {
        payload,
        options: { ...options,
          timeout
        },
        timestamp
      };
    }
  }

  async executeJob(job) {
    // This is an abstract class. Do nothing here. You should override this.
    return new Promise((resolve, _) => {
      resolve();
    });
  } // Enqueue the job.


  async enqueueJob(payload, options) {
    const job = await this.createJob(payload, options);
    return new Promise((resolve, reject) => {
      const jobInternal = this.queue.addJob(this.id, job, job.options);
      jobInternal.on(_esqueue.events.EVENT_JOB_CREATED, async createdJob => {
        if (createdJob.id === jobInternal.id) {
          await this.onJobEnqueued(job);
          resolve(jobInternal);
        }
      });
      jobInternal.on(_esqueue.events.EVENT_JOB_CREATE_ERROR, reject);
    });
  }

  bind(codeServices) {
    const workerFn = (payload, cancellationToken) => {
      const job = { ...payload,
        cancellationToken
      };
      return this.executeJob(job);
    };

    const workerOptions = {
      interval: 5000,
      capacity: 5,
      intervalErrorMultiplier: 1,
      codeServices
    };
    const queueWorker = this.queue.registerWorker(this.id, workerFn, workerOptions);
    queueWorker.on(_esqueue.events.EVENT_WORKER_COMPLETE, async res => {
      const result = res.output.content;
      const job = res.job;
      await this.onJobCompleted(job, result);
    });
    queueWorker.on(_esqueue.events.EVENT_WORKER_JOB_EXECUTION_ERROR, async res => {
      await this.onJobExecutionError(res);
    });
    queueWorker.on(_esqueue.events.EVENT_WORKER_JOB_TIMEOUT, async res => {
      await this.onJobTimeOut(res);
    });
    return this;
  }

  async onJobEnqueued(job) {
    this.log.info(`${this.id} job enqueued with details ${JSON.stringify(job)}`);
    return await this.updateProgress(job, _model.WorkerReservedProgress.INIT);
  }

  async onJobCompleted(job, res) {
    this.log.info(`${this.id} job completed with result ${JSON.stringify(res)} in ${this.workerTaskDurationSeconds(job)} seconds.`);

    if (res.cancelled) {
      // Skip updating job progress if the job is done because of cancellation.
      return;
    }

    return await this.updateProgress(job, _model.WorkerReservedProgress.COMPLETED);
  }

  async onJobExecutionError(res) {
    this.log.error(`${this.id} job execution error ${JSON.stringify(res)} in ${this.workerTaskDurationSeconds(res.job)} seconds.`);
    return await this.updateProgress(res.job, _model.WorkerReservedProgress.ERROR, res.error);
  }

  async onJobTimeOut(res) {
    this.log.error(`${this.id} job timed out ${JSON.stringify(res)} in ${this.workerTaskDurationSeconds(res.job)} seconds.`);
    return await this.updateProgress(res.job, _model.WorkerReservedProgress.TIMEOUT, res.error);
  }

  async updateProgress(job, progress, error) {
    // This is an abstract class. Do nothing here. You should override this.
    return new Promise((resolve, _) => {
      resolve();
    });
  }

  async getTimeoutMs(payload) {
    // Set to 1 hour by default. Override this function for sub classes if necessary.
    return _moment.default.duration(1, 'hour').asMilliseconds();
  }

  workerTaskDurationSeconds(job) {
    const diff = (0, _moment.default)().diff((0, _moment.default)(job.timestamp));
    return _moment.default.duration(diff).asSeconds();
  }

}

exports.AbstractWorker = AbstractWorker;