"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspTestRunner = void 0;

var _fs = _interopRequireDefault(require("fs"));

var sl = _interopRequireWildcard(require("stats-lite"));

var _lodash = _interopRequireDefault(require("lodash"));

var _papaparse = _interopRequireDefault(require("papaparse"));

var _install_manager = require("./install_manager");

var _java_launcher = require("./java_launcher");

var _language_servers = require("./language_servers");

var _ts_launcher = require("./ts_launcher");

var _git_operations = require("../git_operations");

var _test_utils = require("../test_utils");

var _console_logger_factory = require("../utils/console_logger_factory");

var _test_config = require("../../model/test_config");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const requestTypeMapping = new Map([[_test_config.RequestType.FULL, 'full'], [_test_config.RequestType.HOVER, 'hover'], [_test_config.RequestType.INITIALIZE, 'initialize']]);
const serverOptions = (0, _test_utils.createTestServerOption)();

class LspTestRunner {
  constructor(repo, requestType, times) {
    _defineProperty(this, "repo", void 0);

    _defineProperty(this, "result", void 0);

    _defineProperty(this, "proxy", void 0);

    _defineProperty(this, "requestType", void 0);

    _defineProperty(this, "times", void 0);

    this.repo = repo;
    this.requestType = requestType;
    this.times = times;
    this.proxy = null;
    this.result = {
      repoName: `${repo.url}`,
      startTime: 0,
      numberOfRequests: times,
      rps: 0,
      OK: 0,
      KO: 0,
      KORate: '',
      latency_max: 0,
      latency_min: 0,
      latency_medium: 0,
      latency_95: 0,
      latency_99: 0,
      latency_avg: 0,
      latency_std_dev: 0
    };

    if (!_fs.default.existsSync(serverOptions.workspacePath)) {
      _fs.default.mkdirSync(serverOptions.workspacePath);
    }
  }

  async sendRandomRequest() {
    const repoPath = this.repo.path;
    const files = await this.getAllFile();
    const randomFile = files[Math.floor(Math.random() * files.length)];
    await this.proxy.initialize(repoPath);

    switch (this.requestType) {
      case _test_config.RequestType.HOVER:
        {
          const req = {
            method: 'textDocument/hover',
            params: {
              textDocument: {
                uri: `file://${this.repo.path}/${randomFile}`
              },
              position: this.randomizePosition()
            }
          };
          await this.launchRequest(req);
          break;
        }

      case _test_config.RequestType.INITIALIZE:
        {
          this.proxy.initialize(repoPath);
          break;
        }

      case _test_config.RequestType.FULL:
        {
          const req = {
            method: 'textDocument/full',
            params: {
              textDocument: {
                uri: `file://${this.repo.path}/${randomFile}`
              },
              reference: false
            }
          };
          await this.launchRequest(req);
          break;
        }

      default:
        {
          console.error('Unknown request type!');
          break;
        }
    }
  }

  async launchRequest(req) {
    this.result.startTime = Date.now();
    let OK = 0;
    let KO = 0;
    const responseTimes = [];

    for (let i = 0; i < this.times; i++) {
      try {
        const start = Date.now();
        await this.proxy.handleRequest(req);
        responseTimes.push(Date.now() - start);
        OK += 1;
      } catch (e) {
        KO += 1;
      }
    }

    this.result.KO = KO;
    this.result.OK = OK;
    this.result.KORate = (KO / this.times * 100).toFixed(2) + '%';
    this.result.rps = this.times / (Date.now() - this.result.startTime);
    this.collectMetrics(responseTimes);
  }

  collectMetrics(responseTimes) {
    this.result.latency_max = Math.max.apply(null, responseTimes);
    this.result.latency_min = Math.min.apply(null, responseTimes);
    this.result.latency_avg = sl.mean(responseTimes);
    this.result.latency_medium = sl.median(responseTimes);
    this.result.latency_95 = sl.percentile(responseTimes, 0.95);
    this.result.latency_99 = sl.percentile(responseTimes, 0.99);
    this.result.latency_std_dev = sl.stdev(responseTimes).toFixed(2);
  }

  dumpToCSV(resultFile) {
    const newResult = _lodash.default.mapKeys(this.result, (v, k) => {
      if (k !== 'repoName') {
        return `${requestTypeMapping.get(this.requestType)}_${k}`;
      } else {
        return 'repoName';
      }
    });

    if (!_fs.default.existsSync(resultFile)) {
      console.log(_papaparse.default.unparse([newResult]));

      _fs.default.writeFileSync(resultFile, _papaparse.default.unparse([newResult]));
    } else {
      const file = _fs.default.createReadStream(resultFile);

      _papaparse.default.parse(file, {
        header: true,
        complete: parsedResult => {
          const originResults = parsedResult.data;
          const index = originResults.findIndex(originResult => {
            return originResult.repoName === newResult.repoName;
          });

          if (index === -1) {
            originResults.push(newResult);
          } else {
            originResults[index] = { ...originResults[index],
              ...newResult
            };
          }

          _fs.default.writeFileSync(resultFile, _papaparse.default.unparse(originResults));
        }
      });
    }
  }

  async getAllFile() {
    const gitOperator = new _git_operations.GitOperations(this.repo.path);

    try {
      const result = [];
      const fileIterator = await gitOperator.iterateRepo('', 'HEAD');

      for await (const file of fileIterator) {
        const filePath = file.path;

        if (filePath.endsWith(this.repo.language)) {
          result.push(filePath);
        }
      }

      return result;
    } catch (e) {
      console.error(`get files error: ${e}`);
      throw e;
    }
  }

  randomizePosition() {
    // TODO:pcxu randomize position according to source file
    return {
      line: 19,
      character: 2
    };
  }

  async launchLspByLanguage() {
    switch (this.repo.language) {
      case 'java':
        {
          this.proxy = await this.launchJavaLanguageServer();
          break;
        }

      case 'ts':
        {
          this.proxy = await this.launchTypescriptLanguageServer();
          break;
        }

      default:
        {
          console.error('unknown language type');
          break;
        }
    }
  }

  async launchTypescriptLanguageServer() {
    const launcher = new _ts_launcher.TypescriptServerLauncher('127.0.0.1', serverOptions, new _console_logger_factory.ConsoleLoggerFactory());
    return await launcher.launch(false, 1, _language_servers.TYPESCRIPT.embedPath);
  }

  async launchJavaLanguageServer() {
    const launcher = new _java_launcher.JavaLauncher('127.0.0.1', serverOptions, new _console_logger_factory.ConsoleLoggerFactory()); // @ts-ignore

    const installManager = new _install_manager.InstallManager(null, serverOptions);
    return await launcher.launch(false, 1, installManager.installationPath(_language_servers.JAVA));
  }

}

exports.LspTestRunner = LspTestRunner;