"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspService = void 0;

var _language_server = require("../../common/language_server");

var _controller = require("./controller");

var _workspace_handler = require("./workspace_handler");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LspService {
  constructor(targetHost, serverOptions, gitOps, client, installManager, loggerFactory, repoConfigController) {
    _defineProperty(this, "controller", void 0);

    _defineProperty(this, "workspaceHandler", void 0);

    this.workspaceHandler = new _workspace_handler.WorkspaceHandler(gitOps, serverOptions.workspacePath, client, loggerFactory);
    this.controller = new _controller.LanguageServerController(serverOptions, targetHost, installManager, loggerFactory, repoConfigController);
  }
  /**
   * send a lsp request to language server, will initiate the language server if needed
   * @param method the method name
   * @param params the request params
   */


  async sendRequest(method, params) {
    const request = {
      method,
      params
    };
    await this.workspaceHandler.handleRequest(request);
    const response = await this.controller.handleRequest(request);
    return this.workspaceHandler.handleResponse(request, response);
  }

  async launchServers() {
    await this.controller.launchServers();
  }

  async deleteWorkspace(repoUri) {
    for (const path of await this.workspaceHandler.listWorkspaceFolders(repoUri)) {
      await this.controller.unloadWorkspace(path);
    }

    await this.workspaceHandler.clearWorkspace(repoUri);
  }
  /**
   * shutdown all launched language servers
   */


  async shutdown() {
    await this.controller.exit();
  }

  supportLanguage(lang) {
    return this.controller.getLanguageServerDef(lang).length > 0;
  }

  getLanguageSeverDef(lang) {
    return this.controller.getLanguageServerDef(lang);
  }

  languageServerStatus(name) {
    const defs = this.controller.getLanguageServerDef(name);

    if (defs.length > 0) {
      const def = defs[0];
      return this.controller.status(def);
    } else {
      return _language_server.LanguageServerStatus.NOT_INSTALLED;
    }
  }

  async initializeState(repoUri, revision) {
    const workspacePath = await this.workspaceHandler.revisionDir(repoUri, revision);
    return await this.controller.initializeState(workspacePath);
  }

}

exports.LspService = LspService;