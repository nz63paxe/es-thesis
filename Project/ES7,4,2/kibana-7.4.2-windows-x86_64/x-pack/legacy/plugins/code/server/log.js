"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Logger = void 0;

var _util = require("util");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Logger {
  constructor(server, baseTags = ['code']) {
    this.server = server;
    this.baseTags = baseTags;

    _defineProperty(this, "verbose", false);

    if (server) {
      this.verbose = this.server.config().get('xpack.code.verbose');
    }
  } // Return a new logger with new tags


  addTags(tags) {
    return new Logger(this.server, this.baseTags.concat(tags));
  }

  info(msg) {
    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    this.server.log([...this.baseTags, 'info'], msg);
  }

  error(msg) {
    if (msg instanceof Error) {
      msg = msg.stack;
    }

    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    this.server.log([...this.baseTags, 'error'], msg);
  }

  log(message) {
    this.info(message);
  }

  debug(msg) {
    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    if (this.verbose) {
      this.server.log([...this.baseTags, 'info'], msg);
    } else {
      this.server.log([...this.baseTags, 'debug'], msg);
    }
  }

  warn(msg) {
    if (msg instanceof Error) {
      msg = msg.stack;
    }

    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    this.server.log([...this.baseTags, 'warning'], msg);
  } // Log subprocess stdout


  stdout(msg) {
    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    if (this.verbose) {
      this.server.log([...this.baseTags, 'info', 'stdout'], msg);
    } else {
      this.server.log([...this.baseTags, 'debug', 'stdout'], msg);
    }
  } // Log subprocess stderr


  stderr(msg) {
    if (typeof msg !== 'string') {
      msg = (0, _util.inspect)(msg, {
        colors: process.stdout.isTTY
      });
    }

    if (this.verbose) {
      this.server.log([...this.baseTags, 'error', 'stderr'], msg);
    } else {
      this.server.log([...this.baseTags, 'debug', 'stderr'], msg);
    }
  }

}

exports.Logger = Logger;