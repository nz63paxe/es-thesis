"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HashSchedulePolicy = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * A policy that assigns resources to nodes based on hash of the resource id. It is easy and stable for a cluster with
 * a static node table.
 */
class HashSchedulePolicy {
  canAllocate(resource, routingTable, nodes) {
    return !_lodash.default.isEmpty(nodes.nodes);
  }

  canAllocateOnNode(resource, node, routingTable, nodes) {
    if (_lodash.default.isEmpty(nodes.nodes)) {
      return false;
    }

    const sortedNodeIds = nodes.nodes.map(n => n.id).sort();
    const hashCode = this.hash(resource);
    const targetNodeId = sortedNodeIds[hashCode % sortedNodeIds.length];
    return targetNodeId === node.id;
  }
  /**
   * To calculate the hash code of the string as s[0]*31^(n-1) + s[1]*31^(n-2) + ... + s[n-1],
   * see code snippets from https://gist.github.com/hyamamoto/fd435505d29ebfa3d9716fd2be8d42f0.
   */


  hash(s) {
    let hashCode = 0;

    for (let i = 0; i < s.length; i++) {
      // eslint-disable-next-line no-bitwise
      hashCode = Math.imul(31, hashCode) + s.charCodeAt(i) | 0;
    }

    return Math.abs(hashCode);
  }

  score(resource, node, routingTable, nodes) {
    return 1;
  }

}

exports.HashSchedulePolicy = HashSchedulePolicy;