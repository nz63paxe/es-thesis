"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fileRoute = fileRoute;

var _boom = _interopRequireDefault(require("boom"));

var _git_operations = require("../git_operations");

var _search = require("../search");

var _esclient_with_request = require("../utils/esclient_with_request");

var _uri_util = require("../../common/uri_util");

var _apis = require("../distributed/apis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function fileRoute(router, codeServices) {
  const gitService = codeServices.serviceFor(_apis.GitServiceDefinition);

  async function getRepoUriFromMeta(req, repoUri) {
    const repoObjectClient = new _search.RepositoryObjectClient(new _esclient_with_request.EsClientWithRequest(req));

    try {
      const repo = await repoObjectClient.getRepository(repoUri);
      return repo.uri;
    } catch (e) {
      return undefined;
    }
  }

  router.route({
    path: '/api/code/repo/{uri*3}/tree/{ref}/{path*}',
    method: 'GET',

    async handler(req) {
      const {
        uri,
        path,
        ref
      } = req.params;
      const revision = (0, _uri_util.decodeRevisionString)(ref);
      const queries = req.query;
      const limit = queries.limit ? parseInt(queries.limit, 10) : _git_operations.DEFAULT_TREE_CHILDREN_LIMIT;
      const skip = queries.skip ? parseInt(queries.skip, 10) : 0;
      const withParents = 'parents' in queries;
      const flatten = 'flatten' in queries;
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        return await gitService.fileTree(endpoint, {
          uri: repoUri,
          path,
          revision,
          skip,
          limit,
          withParents,
          flatten
        });
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
  router.route({
    path: '/api/code/repo/{uri*3}/blob/{ref}/{path*}',
    method: 'GET',

    async handler(req, h) {
      const {
        uri,
        path,
        ref
      } = req.params;
      const revision = (0, _uri_util.decodeRevisionString)(ref);
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        const blob = await gitService.blob(endpoint, {
          uri,
          path,
          line: req.query.line,
          revision: decodeURIComponent(revision)
        });

        if (blob.imageType) {
          const response = h.response(blob.content);
          response.type(blob.imageType);
          return response;
        } else if (blob.isBinary) {
          return h.response('').type('application/octet-stream').code(204);
        } else {
          if (blob.content) {
            return h.response(blob.content).type('text/plain').header('lang', blob.lang);
          } else {
            return h.response('').type(`text/big`);
          }
        }
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
  router.route({
    path: '/app/code/repo/{uri*3}/raw/{ref}/{path*}',
    method: 'GET',

    async handler(req, h) {
      const {
        uri,
        path,
        ref
      } = req.params;
      const revision = (0, _uri_util.decodeRevisionString)(ref);
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        const blob = await gitService.raw(endpoint, {
          uri: repoUri,
          path,
          revision
        });

        if (blob.isBinary) {
          return h.response(blob.content).encoding('binary');
        } else {
          return h.response(blob.content).type('text/plain');
        }
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
  router.route({
    path: '/api/code/repo/{uri*3}/history/{ref}',
    method: 'GET',
    handler: historyHandler
  });
  router.route({
    path: '/api/code/repo/{uri*3}/history/{ref}/{path*}',
    method: 'GET',
    handler: historyHandler
  });

  async function historyHandler(req) {
    const {
      uri,
      ref,
      path
    } = req.params;
    const revision = (0, _uri_util.decodeRevisionString)(ref);
    const queries = req.query;
    const count = queries.count ? parseInt(queries.count, 10) : 10;
    const after = queries.after !== undefined;

    try {
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);
      return await gitService.history(endpoint, {
        uri: repoUri,
        path,
        revision,
        count,
        after
      });
    } catch (e) {
      if (e.isBoom) {
        return e;
      } else {
        return _boom.default.internal(e.message || e.name);
      }
    }
  }

  router.route({
    path: '/api/code/repo/{uri*3}/references',
    method: 'GET',

    async handler(req) {
      const uri = req.params.uri;
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        return await gitService.branchesAndTags(endpoint, {
          uri: repoUri
        });
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
  router.route({
    path: '/api/code/repo/{uri*3}/diff/{revision}',
    method: 'GET',

    async handler(req) {
      const {
        uri,
        revision
      } = req.params;
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        return await gitService.commitDiff(endpoint, {
          uri: repoUri,
          revision: (0, _uri_util.decodeRevisionString)(revision)
        });
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
  router.route({
    path: '/api/code/repo/{uri*3}/blame/{revision}/{path*}',
    method: 'GET',

    async handler(req) {
      const {
        uri,
        path,
        revision
      } = req.params;
      const repoUri = await getRepoUriFromMeta(req, uri);

      if (!repoUri) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      const endpoint = await codeServices.locate(req, uri);

      try {
        return await gitService.blame(endpoint, {
          uri: repoUri,
          revision: (0, _uri_util.decodeRevisionString)(decodeURIComponent(revision)),
          path
        });
      } catch (e) {
        if (e.isBoom) {
          return e;
        } else {
          return _boom.default.internal(e.message || e.name);
        }
      }
    }

  });
}