"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterService = void 0;

var _cluster_meta = require("./cluster_meta");

var _search = require("../../search");

var _poller = require("../../poller");

var _cluster_state = require("./cluster_state");

var _cluster_state_event = require("./cluster_state_event");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * ClusterService synchronizes the core cluster states with the remote, and provides methods to read the current state,
 * and to register listeners to watch the state change.
 */
class ClusterService {
  constructor(esClient, logger, repositoryObjectClient = new _search.RepositoryObjectClient(esClient)) {
    this.esClient = esClient;
    this.logger = logger;
    this.repositoryObjectClient = repositoryObjectClient;

    _defineProperty(this, "clusterStateListeners", []);

    _defineProperty(this, "clusterStatePoller", new _poller.Poller({
      functionToPoll: () => {
        return this.pollClusterState();
      },
      pollFrequencyInMillis: 5000,
      trailing: true,
      continuePollingOnError: true
    }));

    _defineProperty(this, "currentState", _cluster_state.ClusterState.empty());
  }

  async start() {
    this.clusterStatePoller.start();
  }

  async stop() {
    this.clusterStatePoller.stop();
  }
  /**
   * Sync the cluster meta-data with the remote storage.
   */


  async pollClusterState() {
    const repos = await this.repositoryObjectClient.getAllRepositories();
    const repoUris = new Set(repos.map(repo => repo.uri));
    const currentRepoUris = new Set(this.currentState.clusterMeta.repositories.map(repo => repo.uri));
    const added = new Set(Array.from(repoUris).filter(uri => !currentRepoUris.has(uri)));
    const removed = new Set(Array.from(currentRepoUris).filter(uri => !repoUris.has(uri)));

    if (added.size === 0 && removed.size === 0) {
      // the cluster state and the routing table is only needed to be updated when repository objects have changed.
      return;
    }

    this.setClusterState(new _cluster_state.ClusterState(new _cluster_meta.ClusterMetadata(repos), this.currentState.routingTable.withoutRepositories(removed), this.currentState.nodes));
  }

  state() {
    return this.currentState;
  }

  addClusterStateListener(applier) {
    this.clusterStateListeners.push(applier);
  }

  async callClusterStateListeners(event) {
    for (const applier of this.clusterStateListeners) {
      await applier.onClusterStateChanged(event);
    }
  }
  /**
   * Set the local in memory cluster state, and call cluster state listeners if the state has been changed.
   *
   * @param newState is the new cluster state to set.
   */


  setClusterState(newState) {
    if (newState === this.currentState) {
      return undefined;
    }

    const event = new _cluster_state_event.ClusterStateEvent(newState, this.currentState);
    this.currentState = newState;
    setTimeout(async () => {
      this.callClusterStateListeners(event);
    });
    return event;
  }
  /**
   * Invoke the updater with the current cluster state as the parameter, and write the result of the updater to remote
   * as the new cluster state, if the current local cluster state matches the current remote cluster state, otherwise
   * the updater should be executed again, with the most recent remote cluster state as the input.
   * It means the method works as a series of CAS operations, until the first success operation.
   * It also means the updater should be side-effect free.
   */


  async updateClusterState(updater) {
    const newState = updater(this.currentState);
    return this.setClusterState(newState);
  }

}
/**
 * A cluster applier is a listener of cluster state event.
 */


exports.ClusterService = ClusterService;