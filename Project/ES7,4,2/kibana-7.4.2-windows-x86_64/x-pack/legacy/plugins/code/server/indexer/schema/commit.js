"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitSearchIndexWithScope = exports.CommitIndexName = exports.CommitIndexNamePrefix = exports.CommitSchema = void 0;

var _repository_utils = require("../../../common/repository_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const CommitSchema = {
  repoUri: {
    type: 'keyword'
  },
  id: {
    type: 'keyword'
  },
  message: {
    type: 'text'
  },
  body: {
    type: 'text'
  },
  date: {
    type: 'date'
  },
  parents: {
    type: 'keyword'
  },
  author: {
    properties: {
      name: {
        type: 'text'
      },
      email: {
        type: 'text'
      }
    }
  },
  committer: {
    properties: {
      name: {
        type: 'text'
      },
      email: {
        type: 'text'
      }
    }
  }
};
exports.CommitSchema = CommitSchema;
const CommitIndexNamePrefix = `.code-commit`;
exports.CommitIndexNamePrefix = CommitIndexNamePrefix;

const CommitIndexName = repoUri => {
  return `${CommitIndexNamePrefix}-${_repository_utils.RepositoryUtils.normalizeRepoUriToIndexName(repoUri)}`;
};

exports.CommitIndexName = CommitIndexName;

const CommitSearchIndexWithScope = repoScope => {
  return repoScope.map(repoUri => `${CommitIndexName(repoUri)}*`).join(',');
};

exports.CommitSearchIndexWithScope = CommitSearchIndexWithScope;