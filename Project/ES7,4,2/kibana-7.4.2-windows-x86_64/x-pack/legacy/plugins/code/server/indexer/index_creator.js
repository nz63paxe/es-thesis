"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IndexCreator = void 0;

var _version = _interopRequireDefault(require("./schema/version.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * This IndexCreator deals with anything with elasticsearch index creation.
 */
class IndexCreator {
  constructor(client) {
    this.client = client;

    _defineProperty(this, "version", void 0);

    this.version = Number(_version.default.codeIndexVersion);
  }

  async createIndex(request) {
    const body = {
      settings: request.settings,
      mappings: {
        // Apply the index version in the reserved _meta field of the index.
        _meta: {
          version: this.version
        },
        dynamic_templates: [{
          fieldDefaultNotAnalyzed: {
            match: '*',
            mapping: {
              index: false,
              norms: false
            }
          }
        }],
        properties: request.schema
      }
    };
    const exists = await this.client.indices.existsAlias({
      name: request.index
    });

    if (!exists) {
      // Create the actual index first with the version as the index suffix number.
      await this.client.indices.create({
        index: `${request.index}-${this.version}`,
        body
      }); // Create the alias to point the index just created.

      await this.client.indices.putAlias({
        index: `${request.index}-${this.version}`,
        name: request.index
      });
      return true;
    }

    return exists;
  }

}

exports.IndexCreator = IndexCreator;