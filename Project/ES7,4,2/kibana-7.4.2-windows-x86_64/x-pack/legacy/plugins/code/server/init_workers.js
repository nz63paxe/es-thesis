"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initWorkers = initWorkers;

var _checkDiskSpace = _interopRequireDefault(require("check-disk-space"));

var _model = require("../model");

var _disk_watermark = require("./disk_watermark");

var _indexer = require("./indexer");

var _queue = require("./queue");

var _repository_service_factory = require("./repository_service_factory");

var _apis = require("./distributed/apis");

var _scheduler = require("./scheduler");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initWorkers(server, log, esClient, queue, lspService, gitOps, serverOptions, codeServices) {
  // Initialize indexing factories.
  const lspIndexerFactory = new _indexer.LspIndexerFactory(lspService, serverOptions, gitOps, esClient, log);
  const indexerFactoryMap = new Map();
  indexerFactoryMap.set(_model.IndexerType.LSP, lspIndexerFactory);

  if (serverOptions.enableCommitIndexing) {
    const commitIndexerFactory = new _indexer.CommitIndexerFactory(gitOps, esClient, log);
    indexerFactoryMap.set(_model.IndexerType.COMMIT, commitIndexerFactory);
  } // Initialize queue worker cancellation service.


  const cancellationService = new _queue.CancellationSerivce();
  const indexWorker = new _queue.IndexWorker(queue, log, esClient, indexerFactoryMap, gitOps, cancellationService).bind(codeServices);
  const repoServiceFactory = new _repository_service_factory.RepositoryServiceFactory();
  const watermarkService = new _disk_watermark.DiskWatermarkService(_checkDiskSpace.default, serverOptions, log);
  const cloneWorker = new _queue.CloneWorker(queue, log, esClient, serverOptions, gitOps, indexWorker, repoServiceFactory, cancellationService, watermarkService).bind(codeServices);
  const deleteWorker = new _queue.DeleteWorker(queue, log, esClient, serverOptions, gitOps, cancellationService, lspService, repoServiceFactory).bind(codeServices);
  const updateWorker = new _queue.UpdateWorker(queue, log, esClient, serverOptions, gitOps, repoServiceFactory, cancellationService, watermarkService).bind(codeServices);
  codeServices.registerHandler(_apis.RepositoryServiceDefinition, (0, _apis.getRepositoryHandler)(cloneWorker, deleteWorker, indexWorker)); // Initialize schedulers.

  const cloneScheduler = new _scheduler.CloneScheduler(cloneWorker, serverOptions, esClient, log);
  const updateScheduler = new _scheduler.UpdateScheduler(updateWorker, serverOptions, esClient, log);
  const indexScheduler = new _scheduler.IndexScheduler(indexWorker, serverOptions, esClient, log);
  updateScheduler.start();
  indexScheduler.start(); // Check if the repository is local on the file system.
  // This should be executed once at the startup time of Kibana.

  cloneScheduler.schedule();
  return {
    indexScheduler,
    updateScheduler
  };
}