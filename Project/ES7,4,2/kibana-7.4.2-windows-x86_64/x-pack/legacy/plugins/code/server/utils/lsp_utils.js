"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.groupFiles = groupFiles;
exports.findTitleFromHover = findTitleFromHover;

var _lodash = require("lodash");

var _language_servers = require("../lsp/language_servers");

var _composite_source_merger = require("./composite_source_merger");

var _detect_language = require("./detect_language");

var _uri_util = require("../../common/uri_util");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function groupFiles(list, sourceLoader) {
  const files = [];
  const groupedLocations = (0, _lodash.groupBy)(list, 'uri');

  for (const url of Object.keys(groupedLocations)) {
    const {
      repoUri,
      revision,
      file
    } = (0, _uri_util.parseLspUrl)(url);
    const locations = groupedLocations[url];
    const lines = locations.map(l => ({
      startLine: l.range.start.line,
      endLine: l.range.end.line
    }));
    const ranges = (0, _composite_source_merger.expandRanges)(lines, 1);
    const mergedRanges = (0, _composite_source_merger.mergeRanges)(ranges);

    try {
      const blob = await sourceLoader({
        uri: repoUri,
        path: file,
        revision
      });

      if (blob.content) {
        const source = blob.content.split('\n');
        const language = blob.lang;
        const lineMappings = new _composite_source_merger.LineMapping();
        const code = (0, _composite_source_merger.extractSourceContent)(mergedRanges, source, lineMappings).join('\n');
        const lineNumbers = lineMappings.toStringArray();
        const highlights = locations.map(l => {
          const {
            start,
            end
          } = l.range;
          const startLineNumber = lineMappings.lineNumber(start.line);
          const endLineNumber = lineMappings.lineNumber(end.line);
          return {
            startLineNumber,
            startColumn: start.character + 1,
            endLineNumber,
            endColumn: end.character + 1
          };
        });
        files.push({
          repo: repoUri,
          file,
          language,
          uri: url,
          revision,
          code,
          lineNumbers,
          highlights
        });
      }
    } catch (e) {// can't load this file, ignore this result
    }
  }

  return (0, _lodash.groupBy)(files, 'repo');
}

async function findTitleFromHover(hover, uri, position) {
  let title;

  if (hover.result && hover.result.contents) {
    if (Array.isArray(hover.result.contents)) {
      const content = hover.result.contents[0];
      title = hover.result.contents[0].value;
      const lang = await (0, _detect_language.detectLanguage)(uri.replace('file://', '')); // TODO(henrywong) Find a gernal approach to construct the reference title.

      if (content.kind) {
        // The format of the hover result is 'MarkupContent', extract appropriate pieces as the references title.
        if (_language_servers.GO.languages.includes(lang)) {
          title = title.substring(title.indexOf('```go\n') + 5, title.lastIndexOf('\n```'));

          if (title.includes('{\n')) {
            title = title.substring(0, title.indexOf('{\n'));
          }
        }
      } else if (_language_servers.CTAGS.languages.includes(lang)) {
        // There are language servers may provide hover results with markdown syntax, like ctags-langserver,
        // extract the plain text.
        if (title.substring(0, 2) === '**' && title.includes('**\n')) {
          title = title.substring(title.indexOf('**\n') + 3);
        }
      }
    } else {
      title = hover.result.contents;
    }
  } else {
    title = (0, _lodash.last)(uri.split('/')) + `(${position.line}, ${position.character})`;
  }

  return title;
}