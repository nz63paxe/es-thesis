"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initQueue = initQueue;

var _esqueue = require("./lib/esqueue");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initQueue(server, log, esClient) {
  const queueIndex = server.config().get('xpack.code.queueIndex');
  const queueTimeoutMs = server.config().get('xpack.code.queueTimeoutMs');
  const queue = new _esqueue.Esqueue(queueIndex, {
    client: esClient,
    timeout: queueTimeoutMs
  });
  return queue;
}