"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.aggregateIndexStats = aggregateIndexStats;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function aggregateIndexStats(stats) {
  const res = new Map();
  stats.forEach(s => {
    s.forEach((value, key) => {
      if (!res.has(key)) {
        res.set(key, 0);
      }

      res.set(key, res.get(key) + value);
    });
  });
  return res;
}