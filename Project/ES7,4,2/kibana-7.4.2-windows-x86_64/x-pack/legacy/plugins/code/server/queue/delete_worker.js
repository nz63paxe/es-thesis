"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DeleteWorker = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _model = require("../../model");

var _schema = require("../indexer/schema");

var _search = require("../search");

var _abstract_worker = require("./abstract_worker");

var _cancellation_service = require("./cancellation_service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class DeleteWorker extends _abstract_worker.AbstractWorker {
  constructor(queue, log, client, serverOptions, gitOps, cancellationService, lspService, repoServiceFactory) {
    super(queue, log);
    this.queue = queue;
    this.log = log;
    this.client = client;
    this.serverOptions = serverOptions;
    this.gitOps = gitOps;
    this.cancellationService = cancellationService;
    this.lspService = lspService;
    this.repoServiceFactory = repoServiceFactory;

    _defineProperty(this, "id", 'delete');

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  async executeJob(job) {
    const {
      uri
    } = job.payload;

    try {
      // 1. Check if the uri exsits
      await this.objectClient.getRepository(uri); // 2. Double check if the uri contains ".." which could result in
      // deleting incorrect files in the file systems.

      if (uri.split('/').includes('..')) {
        throw new Error('Repository URI should not contain "..".');
      }
    } catch (error) {
      this.log.error(`Invalid repository uri ${uri}. Skip delete job.`);
      this.log.error(error);
      return {
        uri,
        // Because the repository does not exist, we can regard the delete job to be successful.
        res: true
      };
    }

    try {
      // 1. Cancel running clone and update workers
      await this.cancellationService.cancelCloneJob(uri, _cancellation_service.CancellationReason.REPOSITORY_DELETE);
      await this.cancellationService.cancelUpdateJob(uri, _cancellation_service.CancellationReason.REPOSITORY_DELETE); // 2. Delete workspace and index workers. Since the indexing could be
      // hanging in the initialization stage, we should delete the workspace
      // to cancel it in the meantime.

      const deleteWorkspacePromise = this.deletePromiseWrapper(this.lspService.deleteWorkspace(uri), 'workspace', uri);
      const indexJobCancelPromise = this.cancellationService.cancelIndexJob(uri, _cancellation_service.CancellationReason.REPOSITORY_DELETE);
      await Promise.all([deleteWorkspacePromise, indexJobCancelPromise]); // 3. Delete ES indices and aliases

      const deleteSymbolESIndexPromise = this.deletePromiseWrapper(this.client.indices.delete({
        index: `${(0, _schema.SymbolIndexName)(uri)}*`
      }), 'symbol ES index', uri);
      const deleteReferenceESIndexPromise = this.deletePromiseWrapper(this.client.indices.delete({
        index: `${(0, _schema.ReferenceIndexName)(uri)}*`
      }), 'reference ES index', uri);
      await Promise.all([deleteSymbolESIndexPromise, deleteReferenceESIndexPromise]);
      const repoService = this.repoServiceFactory.newInstance(this.serverOptions.repoPath, this.serverOptions.credsPath, this.log, this.serverOptions.security.enableGitCertCheck);
      this.gitOps.cleanRepo(uri);
      await this.deletePromiseWrapper(repoService.remove(uri), 'git data', uri); // 4. Delete the document index and alias where the repository document and all status reside,
      // so that you won't be able to import the same repositories until they are
      // fully removed.

      await this.deletePromiseWrapper(this.client.indices.delete({
        index: `${(0, _schema.DocumentIndexName)(uri)}*`
      }), 'document ES index', uri);
      return {
        uri,
        res: true
      };
    } catch (error) {
      this.log.error(`Delete repository ${uri} error.`);
      this.log.error(error);
      return {
        uri,
        res: false
      };
    }
  }

  async onJobEnqueued(job) {
    const repoUri = job.payload.uri;
    const progress = {
      uri: repoUri,
      progress: _model.WorkerReservedProgress.INIT,
      timestamp: new Date()
    };
    return await this.objectClient.setRepositoryDeleteStatus(repoUri, progress);
  }

  async updateProgress(job, progress) {
    const {
      uri
    } = job.payload;
    const p = {
      uri,
      progress,
      timestamp: new Date()
    };

    if (progress !== _model.WorkerReservedProgress.COMPLETED) {
      return await this.objectClient.updateRepositoryDeleteStatus(uri, p);
    }
  }

  async getTimeoutMs(_) {
    return _moment.default.duration(1, 'hour').asMilliseconds() + _moment.default.duration(10, 'minutes').asMilliseconds();
  }

  deletePromiseWrapper(promise, type, repoUri) {
    return promise.then(() => {
      this.log.info(`Delete ${type} of repository ${repoUri} done.`);
    }).catch(error => {
      this.log.error(`Delete ${type} of repository ${repoUri} error.`);
      this.log.error(error);
    });
  }

}

exports.DeleteWorker = DeleteWorker;