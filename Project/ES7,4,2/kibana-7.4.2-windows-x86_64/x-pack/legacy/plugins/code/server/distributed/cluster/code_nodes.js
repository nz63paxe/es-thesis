"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeNodes = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CodeNodes {
  constructor(nodes) {
    this.nodes = nodes;

    _defineProperty(this, "idToNodes", void 0);

    this.idToNodes = new Map(nodes.map(node => {
      return [node.id, node];
    }));
  }

  getNodeById(id) {
    return this.idToNodes.get(id);
  }

}

exports.CodeNodes = CodeNodes;