"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterResourceLocator = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _local_endpoint = require("../local_endpoint");

var _repository_utils = require("../../../common/repository_utils");

var _cluster_node_endpoint = require("./cluster_node_endpoint");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class ClusterResourceLocator {
  constructor(clusterService, clusterMembershipService, schedulerService) {
    this.clusterService = clusterService;
    this.clusterMembershipService = clusterMembershipService;
    this.schedulerService = schedulerService;
  }

  repositoryUri(url) {
    return _repository_utils.RepositoryUtils.buildRepository(url).uri;
  }

  async locate(req, resource) {
    // to be compatible with
    if (resource.trim() === '') {
      return new _local_endpoint.LocalEndpoint(req, resource);
    }

    const state = this.clusterService.state();
    const nodeId = state.routingTable.getNodeIdByRepositoryURI(this.repositoryUri(resource));

    if (!nodeId) {
      throw _boom.default.notFound(`resource [${resource}] not exists`);
    }

    if (this.clusterMembershipService.localNode.id === nodeId) {
      return new _local_endpoint.LocalEndpoint(req, resource);
    } else {
      const node = state.nodes.getNodeById(nodeId);

      if (!node) {
        throw _boom.default.notFound(`Node [${nodeId}] not found`);
      }

      return new _cluster_node_endpoint.ClusterNodeEndpoint(req, resource, node);
    }
  }

  async isResourceLocal(resource) {
    const state = this.clusterService.state();
    return this.clusterMembershipService.localNode.id === state.routingTable.getNodeIdByRepositoryURI(this.repositoryUri(resource));
  }

  async allocate(req, resource) {
    // make the cluster service synchronize the meta data and allocate new resources to nodes
    await this.clusterService.pollClusterState(); // allocate the repository to nodes

    await this.schedulerService.allocateUnassigned(); // the resource should be assigned to a node for now, if possible

    return this.locate(req, resource);
  }

}

exports.ClusterResourceLocator = ClusterResourceLocator;