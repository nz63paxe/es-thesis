"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NonCodeNodeAdapter = void 0;

var _wreck = _interopRequireDefault(require("@hapi/wreck"));

var _util = _interopRequireDefault(require("util"));

var _boom = _interopRequireDefault(require("boom"));

var _service_handler_adapter = require("../service_handler_adapter");

var _code_node_resource_locator = require("./code_node_resource_locator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const pickHeaders = ['authorization'];

function filterHeaders(originRequest) {
  const result = {};

  for (const header of pickHeaders) {
    if (originRequest.headers[header]) {
      result[header] = originRequest.headers[header];
    }
  }

  return result;
}

class NonCodeNodeAdapter {
  constructor(codeNodeUrl, log) {
    this.codeNodeUrl = codeNodeUrl;
    this.log = log;

    _defineProperty(this, "handlers", new Map());

    _defineProperty(this, "locator", new _code_node_resource_locator.CodeNodeResourceLocator(this.codeNodeUrl));
  }

  async start() {}

  async stop() {}

  getService(serviceDefinition) {
    const serviceHandler = this.handlers.get(serviceDefinition);

    if (!serviceHandler) {
      // we don't need implement code for service, so we can register here.
      this.registerHandler(serviceDefinition, null);
    }

    return serviceHandler;
  }

  registerHandler(serviceDefinition, // serviceHandler will always be null here since it will be overridden by the request forwarding.
  serviceHandler, options = _service_handler_adapter.DEFAULT_SERVICE_OPTION) {
    const dispatchedHandler = {}; // eslint-disable-next-line guard-for-in

    for (const method in serviceDefinition) {
      const d = serviceDefinition[method];
      const path = `${options.routePrefix}/${d.routePath || method}`;

      dispatchedHandler[method] = async (endpoint, params) => {
        const payload = {
          context: endpoint.toContext(),
          params
        };
        const {
          data
        } = await this.requestFn(endpoint.codeNodeUrl, path, payload, endpoint.httpRequest);
        return data;
      };
    }

    this.handlers.set(serviceDefinition, dispatchedHandler);
    return dispatchedHandler;
  }

  async requestFn(baseUrl, path, payload, originRequest) {
    const opt = {
      baseUrl,
      payload: JSON.stringify(payload),
      // redirect all headers to CodeNode
      headers: { ...filterHeaders(originRequest),
        'kbn-xsrf': 'kibana'
      }
    };

    const promise = _wreck.default.request('POST', path, opt);

    const res = await promise;
    this.log.debug(`sending RPC call to ${baseUrl}${path} ${res.statusCode}`);

    if (res.statusCode && res.statusCode >= 200 && res.statusCode < 300) {
      const buffer = await _wreck.default.read(res, {});

      try {
        return JSON.parse(buffer.toString(), (key, value) => {
          return value && value.type === 'Buffer' ? Buffer.from(value.data) : value;
        });
      } catch (e) {
        this.log.error('parse json failed: ' + buffer.toString());
        throw _boom.default.boomify(e, {
          statusCode: 500
        });
      }
    } else {
      this.log.error(`received ${res.statusCode} from ${baseUrl}/${path}, params was ${_util.default.inspect(payload.params)}`);
      const body = await _wreck.default.read(res, {
        json: true
      });
      throw new _boom.default(body.message, {
        statusCode: res.statusCode || 500,
        data: body.error
      });
    }
  }

}

exports.NonCodeNodeAdapter = NonCodeNodeAdapter;