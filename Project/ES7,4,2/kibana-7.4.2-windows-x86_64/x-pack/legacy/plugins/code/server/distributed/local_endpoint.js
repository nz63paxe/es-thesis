"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LocalEndpoint = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class LocalEndpoint {
  constructor(httpRequest, resource) {
    this.httpRequest = httpRequest;
    this.resource = resource;
  }

  toContext() {
    return {
      resource: this.resource,
      path: this.httpRequest.path
    };
  }

}

exports.LocalEndpoint = LocalEndpoint;