"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryIndexInitializerFactory = void 0;

var _ = require(".");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class RepositoryIndexInitializerFactory {
  constructor(client, log) {
    this.client = client;
    this.log = log;
  }

  async create(repoUri, revision) {
    return new _.RepositoryIndexInitializer(repoUri, revision, this.client, this.log);
  }

}

exports.RepositoryIndexInitializerFactory = RepositoryIndexInitializerFactory;