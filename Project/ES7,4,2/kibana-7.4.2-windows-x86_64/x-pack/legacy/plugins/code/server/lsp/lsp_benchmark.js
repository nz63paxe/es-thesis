"use strict";

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _jsYaml = _interopRequireDefault(require("js-yaml"));

var _test_config = require("../../model/test_config");

var _test_repo_manager = require("./test_repo_manager");

var _lsp_test_runner = require("./lsp_test_runner");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
jest.setTimeout(300000);
let repoManger;
const resultFile = `benchmark_result_${Date.now()}.csv`;

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

beforeAll(async () => {
  const config = _jsYaml.default.safeLoad(_fs.default.readFileSync('test_config.yml', 'utf8'));

  repoManger = new _test_repo_manager.TestRepoManager(config);
  await repoManger.importAllRepos();
});
it('test Java lsp full', async () => {
  const repo = repoManger.getRepo('java');
  const runner = new _lsp_test_runner.LspTestRunner(repo, _test_config.RequestType.FULL, 10);
  await runner.launchLspByLanguage(); // sleep until jdt connection established

  await sleep(3000);
  await runner.sendRandomRequest();
  await runner.proxy.exit();
  runner.dumpToCSV(resultFile);
  expect(true);
});
it('test Java lsp hover', async () => {
  const repo = repoManger.getRepo('java');
  const runner = new _lsp_test_runner.LspTestRunner(repo, _test_config.RequestType.HOVER, 10);
  await runner.launchLspByLanguage(); // sleep until jdt connection established

  await sleep(3000);
  await runner.sendRandomRequest();
  await runner.proxy.exit();
  runner.dumpToCSV(resultFile);
  expect(true);
});
it('test ts lsp full', async () => {
  const repo = repoManger.getRepo('ts');
  const runner = new _lsp_test_runner.LspTestRunner(repo, _test_config.RequestType.FULL, 10);
  await runner.launchLspByLanguage();
  await sleep(2000);
  await runner.sendRandomRequest();
  await runner.proxy.exit();
  runner.dumpToCSV(resultFile);
  await sleep(2000);
  expect(true);
});
it('test ts lsp hover', async () => {
  const repo = repoManger.getRepo('ts');
  const runner = new _lsp_test_runner.LspTestRunner(repo, _test_config.RequestType.HOVER, 10);
  await runner.launchLspByLanguage();
  await sleep(3000);
  await runner.sendRandomRequest();
  await runner.proxy.exit();
  runner.dumpToCSV(resultFile);
  await sleep(2000);
  expect(true);
});
afterAll(async () => {
  // eslint-disable-next-line no-console
  console.log(`result file ${_path.default.resolve(__dirname)}/${resultFile} was saved!`);
  await repoManger.cleanAllRepos();
});