"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getGitServiceHandler = exports.GitServiceDefinition = exports.GitServiceDefinitionOption = void 0;

var _fileType = _interopRequireDefault(require("file-type"));

var _boom = _interopRequireDefault(require("boom"));

var _nodegit = require("@elastic/nodegit");

var _git_operations = require("../../git_operations");

var _buffer = require("../../utils/buffer");

var _detect_language = require("../../utils/detect_language");

var _file = require("../../../common/file");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const GitServiceDefinitionOption = {
  routePrefix: '/api/code/internal/git'
};
exports.GitServiceDefinitionOption = GitServiceDefinitionOption;
const GitServiceDefinition = {
  fileTree: {
    request: {},
    response: {}
  },
  blob: {
    request: {},
    response: {}
  },
  raw: {
    request: {},
    response: {}
  },
  history: {
    request: {},
    response: {}
  },
  branchesAndTags: {
    request: {},
    response: {}
  },
  commitDiff: {
    request: {},
    response: {}
  },
  blame: {
    request: {},
    response: {}
  },
  commit: {
    request: {},
    response: {}
  },
  headRevision: {
    request: {},
    response: {}
  }
};
exports.GitServiceDefinition = GitServiceDefinition;

const getGitServiceHandler = gitOps => ({
  async fileTree({
    uri,
    path,
    revision,
    skip,
    limit,
    withParents,
    flatten
  }, context) {
    return await gitOps.fileTree(uri, path, revision, skip, limit, withParents, flatten);
  },

  async blob({
    uri,
    path,
    revision,
    line
  }) {
    const blob = await gitOps.fileContent(uri, path, revision);
    const isBinary = blob.isBinary();

    if (isBinary) {
      const type = (0, _fileType.default)(blob.content());

      if (type && type.mime && type.mime.startsWith('image/')) {
        return {
          isBinary,
          imageType: type.mime,
          content: blob.content().toString()
        };
      } else {
        return {
          isBinary
        };
      }
    } else {
      if (line) {
        const [from, to] = line.split(',');
        let fromLine = parseInt(from, 10);
        let toLine = to === undefined ? fromLine + 1 : parseInt(to, 10);

        if (fromLine > toLine) {
          [fromLine, toLine] = [toLine, fromLine];
        }

        const lines = (0, _buffer.extractLines)(blob.content(), fromLine, toLine);
        const lang = await (0, _detect_language.detectLanguage)(path, lines);
        return {
          isBinary,
          lang,
          content: lines
        };
      } else if (blob.rawsize() <= _file.TEXT_FILE_LIMIT) {
        const lang = await (0, _detect_language.detectLanguage)(path, blob.content());
        return {
          isBinary,
          lang,
          content: blob.content().toString()
        };
      } else {
        return {
          isBinary
        };
      }
    }
  },

  async raw({
    uri,
    path,
    revision
  }) {
    const blob = await gitOps.fileContent(uri, path, revision);
    const isBinary = blob.isBinary();
    return {
      isBinary,
      content: blob.content()
    };
  },

  async history({
    uri,
    path,
    revision,
    count,
    after
  }) {
    const repository = await gitOps.openRepo(uri);
    const commit = await gitOps.getCommitInfo(uri, revision);

    if (commit === null) {
      throw _boom.default.notFound(`commit ${revision} not found in repo ${uri}`);
    }

    const walk = repository.createRevWalk();
    walk.sorting(_nodegit.Revwalk.SORT.TIME);

    const commitId = _nodegit.Oid.fromString(commit.id);

    walk.push(commitId);
    let commits;

    if (path) {
      // magic number 10000: how many commits at the most to iterate in order to find the commits contains the path
      const results = await walk.fileHistoryWalk(path, count, 10000);
      commits = results.map(result => result.commit);
    } else {
      commits = await walk.getCommits(count);
    }

    if (after && commits.length > 0) {
      if (commits[0].id().equal(commitId)) {
        commits = commits.slice(1);
      }
    }

    return commits.map(_git_operations.commitInfo);
  },

  async branchesAndTags({
    uri
  }) {
    return await gitOps.getBranchAndTags(uri);
  },

  async commitDiff({
    uri,
    revision
  }) {
    return await gitOps.getCommitDiff(uri, revision);
  },

  async blame({
    uri,
    path,
    revision
  }) {
    return await gitOps.blame(uri, revision, path);
  },

  async commit({
    uri,
    revision
  }) {
    return await gitOps.getCommitOr404(uri, revision);
  },

  async headRevision({
    uri
  }) {
    return await gitOps.getHeadRevision(uri);
  }

});

exports.getGitServiceHandler = getGitServiceHandler;