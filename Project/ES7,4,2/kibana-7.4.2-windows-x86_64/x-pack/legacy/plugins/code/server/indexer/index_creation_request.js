"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCommitIndexCreationRequest = exports.getReferenceIndexCreationRequest = exports.getSymbolIndexCreationRequest = exports.getDocumentIndexCreationRequest = void 0;

var _schema = require("./schema");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getDocumentIndexCreationRequest = repoUri => {
  return {
    index: (0, _schema.DocumentIndexName)(repoUri),
    settings: { ..._schema.DocumentAnalysisSettings,
      number_of_shards: 1,
      auto_expand_replicas: '0-1'
    },
    schema: _schema.DocumentSchema
  };
};

exports.getDocumentIndexCreationRequest = getDocumentIndexCreationRequest;

const getSymbolIndexCreationRequest = repoUri => {
  return {
    index: (0, _schema.SymbolIndexName)(repoUri),
    settings: { ..._schema.SymbolAnalysisSettings,
      number_of_shards: 1,
      auto_expand_replicas: '0-1'
    },
    schema: _schema.SymbolSchema
  };
};

exports.getSymbolIndexCreationRequest = getSymbolIndexCreationRequest;

const getReferenceIndexCreationRequest = repoUri => {
  return {
    index: (0, _schema.ReferenceIndexName)(repoUri),
    settings: {
      number_of_shards: 1,
      auto_expand_replicas: '0-1'
    },
    schema: _schema.ReferenceSchema
  };
};

exports.getReferenceIndexCreationRequest = getReferenceIndexCreationRequest;

const getCommitIndexCreationRequest = repoUri => {
  return {
    index: (0, _schema.CommitIndexName)(repoUri),
    settings: {
      number_of_shards: 1,
      auto_expand_replicas: '0-1'
    },
    schema: _schema.CommitSchema
  };
};

exports.getCommitIndexCreationRequest = getCommitIndexCreationRequest;