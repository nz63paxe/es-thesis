"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeNodeAdapter = void 0;

var _util = _interopRequireDefault(require("util"));

var _boom = _interopRequireDefault(require("boom"));

var _service_handler_adapter = require("../service_handler_adapter");

var _local_handler_adapter = require("../local_handler_adapter");

var _local_endpoint = require("../local_endpoint");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class CodeNodeAdapter {
  constructor(server, log) {
    this.server = server;
    this.log = log;

    _defineProperty(this, "localAdapter", new _local_handler_adapter.LocalHandlerAdapter());

    _defineProperty(this, "locator", {
      async locate(httpRequest, resource) {
        return Promise.resolve(new _local_endpoint.LocalEndpoint(httpRequest, resource));
      },

      isResourceLocal(resource) {
        return Promise.resolve(false);
      },

      async allocate(httpRequest, resource) {
        return Promise.resolve(new _local_endpoint.LocalEndpoint(httpRequest, resource));
      }

    });
  }

  async start() {}

  async stop() {}

  getService(serviceDefinition) {
    // services on code node dispatch to local directly
    return this.localAdapter.getService(serviceDefinition);
  }

  registerHandler(serviceDefinition, serviceHandler, options = _service_handler_adapter.DEFAULT_SERVICE_OPTION) {
    if (!serviceHandler) {
      throw new Error("Code node service handler can't be null!");
    }

    const serviceMethodMap = this.localAdapter.registerHandler(serviceDefinition, serviceHandler); // eslint-disable-next-line guard-for-in

    for (const method in serviceDefinition) {
      const d = serviceDefinition[method];
      const path = `${options.routePrefix}/${d.routePath || method}`; // register routes, receive requests from non-code node.

      this.server.route({
        method: 'post',
        path,
        handler: async req => {
          const {
            context,
            params
          } = req.payload;
          this.log.debug(`Receiving RPC call ${req.url.path} ${_util.default.inspect(params)}`);
          const endpoint = {
            toContext() {
              return context;
            }

          };

          try {
            const data = await serviceMethodMap[method](endpoint, params);
            return {
              data
            };
          } catch (e) {
            if (!_boom.default.isBoom(e)) {
              throw _boom.default.boomify(e, {
                statusCode: 500
              });
            } else {
              throw e;
            }
          }
        }
      });
    }

    return serviceMethodMap;
  }

}

exports.CodeNodeAdapter = CodeNodeAdapter;