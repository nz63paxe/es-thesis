"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterState = void 0;

var _routing_table = require("./routing_table");

var _cluster_meta = require("./cluster_meta");

var _code_nodes = require("./code_nodes");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * A snapshot of the meta data and the routing table of the cluster.
 */
class ClusterState {
  constructor(clusterMeta, routingTable, nodes) {
    this.clusterMeta = clusterMeta;
    this.routingTable = routingTable;
    this.nodes = nodes;
  }

  static empty() {
    return new ClusterState(new _cluster_meta.ClusterMetadata([]), new _routing_table.RoutingTable([]), new _code_nodes.CodeNodes([]));
  }
  /**
   * Get all repository objects belongs to the given node id according to the routing table.
   * @param nodeId the id of the node.
   */


  getNodeRepositories(nodeId) {
    return this.routingTable.getRepositoryURIsByNodeId(nodeId).map(uri => {
      return this.clusterMeta.getRepository(uri);
    }).filter(repo => {
      // the repository uri exists in the routing table, but not exists as a meta object
      // it means the routing table is stale
      return repo !== undefined;
    });
  }
  /**
   * find repositories not exists in the routing table, or the node it assigned to doesn't exists in the cluster
   */


  getUnassignedRepositories() {
    return this.clusterMeta.repositories.filter(repo => {
      const nodeId = this.routingTable.getNodeIdByRepositoryURI(repo.uri);

      if (!nodeId) {
        return true;
      }

      return this.nodes.getNodeById(nodeId) === undefined;
    });
  }

  toString() {
    return `{routingTable: ${this.routingTable}}`;
  }

}

exports.ClusterState = ClusterState;