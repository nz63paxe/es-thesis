"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractLauncher = void 0;

var _vscodeJsonrpc = require("vscode-jsonrpc");

var _lsp_error_codes = require("../../common/lsp_error_codes");

var _proxy = require("./proxy");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

let seqNo = 1;

class AbstractLauncher {
  constructor(name, targetHost, options, loggerFactory) {
    this.name = name;
    this.targetHost = targetHost;
    this.options = options;
    this.loggerFactory = loggerFactory;

    _defineProperty(this, "running", false);

    _defineProperty(this, "currentPid", -1);

    _defineProperty(this, "child", null);

    _defineProperty(this, "startTime", -1);

    _defineProperty(this, "proxyConnected", false);

    _defineProperty(this, "log", void 0);

    _defineProperty(this, "spawnTimes", 0);

    _defineProperty(this, "launchReject", void 0);

    _defineProperty(this, "startupTimeout", 10000);

    _defineProperty(this, "maxRespawn", 3);

    this.log = this.loggerFactory.getLogger([`${seqNo++}`, `${this.name}`, 'code']);
  }

  async launch(builtinWorkspace, maxWorkspace, installationPath) {
    const port = await this.getPort();
    const log = this.log;
    const proxy = new _proxy.LanguageServerProxy(port, this.targetHost, log);

    if (this.options.lsp.detach) {
      log.debug('Detach mode, expected language server launch externally');
      proxy.onConnected(() => {
        this.running = true;
      });
      proxy.onDisconnected(() => {
        this.running = false;

        if (!proxy.isClosed) {
          log.debug(`${this.name} language server disconnected, reconnecting`);
          setTimeout(() => this.reconnect(proxy, installationPath), 1000);
        }
      });
    } else {
      const child = await this.doSpawnProcess(installationPath, port, log);
      this.onProcessExit(child, () => {
        if (!proxy.isClosed) this.reconnect(proxy, installationPath);
      });
      proxy.onDisconnected(async () => {
        this.proxyConnected = false;

        if (!proxy.isClosed) {
          log.debug('proxy disconnected, reconnecting');
          setTimeout(async () => {
            await this.reconnect(proxy, installationPath, child);
          }, 1000);
        } else if (this.child) {
          log.info('proxy closed, kill process');
          await this.killProcess(child);
        }
      });
      this.child = child;
    }

    proxy.onExit(() => {
      log.debug('proxy exited, is the program running? ' + this.running);

      if (this.child && this.running) {
        const p = this.child;
        this.killProcess(p);
      }
    });
    this.startConnect(proxy);
    await new Promise((resolve, reject) => {
      proxy.onConnected(() => {
        this.proxyConnected = true;
        resolve();
      });

      this.launchReject = err => {
        log.debug('launch error ' + err);
        proxy.exit().catch(this.log.debug);
        reject(err);
      };
    });
    return this.createExpander(proxy, builtinWorkspace, maxWorkspace);
  }

  onProcessExit(child, reconnectFn) {
    const pid = child.pid;
    child.onExit(() => {
      if (this.currentPid === pid) {
        this.running = false; // if the process exited before proxy connected, then we reconnect

        if (!this.proxyConnected) {
          reconnectFn();
        }
      }
    });
  }
  /**
   * proxy should be connected within this timeout, otherwise we reconnect.
   */


  /**
   * try reconnect the proxy when disconnected
   */
  async reconnect(proxy, installationPath, child) {
    this.log.debug('reconnecting');

    if (this.options.lsp.detach) {
      this.startConnect(proxy);
    } else {
      const processExpired = () => Date.now() - this.startTime > this.startupTimeout;

      if (child && !child.killed() && !processExpired()) {
        this.startConnect(proxy);
      } else {
        if (this.spawnTimes < this.maxRespawn) {
          if (child && this.running) {
            this.log.debug('killing the old program.');
            await this.killProcess(child);
          }

          const port = await this.getPort();
          proxy.changePort(port);
          this.child = await this.doSpawnProcess(installationPath, port, this.log);
          this.onProcessExit(this.child, () => this.reconnect(proxy, installationPath, child));
          this.startConnect(proxy);
        } else {
          const ServerStartFailed = new _vscodeJsonrpc.ResponseError(_lsp_error_codes.LanguageServerStartFailed, 'Launch language server failed.');
          this.launchReject(ServerStartFailed);
          proxy.setError(ServerStartFailed);
          this.log.warn(`spawned program ${this.spawnTimes} times, mark this proxy unusable.`);
        }
      }
    }
  }

  async doSpawnProcess(installationPath, port, log) {
    this.log.debug('start program');
    const child = await this.spawnProcess(installationPath, port, log);
    this.currentPid = child.pid;
    this.spawnTimes += 1;
    this.startTime = Date.now();
    this.running = true;
    return child;
  }

  startConnect(proxy) {
    proxy.connect();
  }
  /**
   * await for proxy connected, create a request expander
   * @param proxy
   * @param builtinWorkspace
   * @param maxWorkspace
   */


  killProcess(child) {
    if (!child.killed()) {
      return new Promise((resolve, reject) => {
        // if not killed within 1s
        const t = setTimeout(reject, 1000);
        child.onExit(() => {
          clearTimeout(t);
          resolve(true);
        });
        child.kill(false);
      }).catch(() => {
        // force kill
        child.kill(true);
        return child.killed();
      }).finally(() => {
        if (this.currentPid === child.pid) this.running = false;
      });
    }
  }

}

exports.AbstractLauncher = AbstractLauncher;