"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LanguageServerProxy = void 0;

var _events = _interopRequireDefault(require("events"));

var net = _interopRequireWildcard(require("net"));

var _url = require("url");

var _vscodeJsonrpc = require("vscode-jsonrpc");

var _messages = require("vscode-jsonrpc/lib/messages");

var _main = require("vscode-languageserver-protocol/lib/main");

var _lsp_error_codes = require("../../common/lsp_error_codes");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LanguageServerProxy {
  get isClosed() {
    return this.closed;
  }

  constructor(targetPort, targetHost, logger) {
    _defineProperty(this, "error", null);

    _defineProperty(this, "initialized", false);

    _defineProperty(this, "socket", void 0);

    _defineProperty(this, "clientConnection", null);

    _defineProperty(this, "closed", false);

    _defineProperty(this, "targetHost", void 0);

    _defineProperty(this, "targetPort", void 0);

    _defineProperty(this, "logger", void 0);

    _defineProperty(this, "eventEmitter", new _events.default());

    _defineProperty(this, "currentServer", null);

    _defineProperty(this, "listeningPort", null);

    this.targetHost = targetHost;
    this.targetPort = targetPort;
    this.logger = logger;
  }

  async handleRequest(request) {
    const response = {
      jsonrpc: '',
      id: null
    };
    const conn = await this.connected();
    const params = Array.isArray(request.params) ? request.params : [request.params];

    if (!request.isNotification) {
      const file = request.documentUri || request.workspacePath;
      this.logger.debug(`sending request ${request.method} for ${file}`);
      response.result = await conn.sendRequest(request.method, ...params);
      this.logger.debug(`request ${request.method} for ${file} done.`);
    } else {
      conn.sendNotification(request.method, ...params);
    }

    return response;
  }

  connected() {
    if (this.closed) {
      return Promise.reject(new _messages.ResponseError(_lsp_error_codes.RequestCancelled, 'Server closed'));
    } else if (this.error) {
      return Promise.reject(new _messages.ResponseError(_lsp_error_codes.InternalError, 'Server error', this.error));
    } else if (this.clientConnection) {
      return Promise.resolve(this.clientConnection);
    } else {
      return new Promise((resolve, reject) => {
        this.eventEmitter.on('err', error => reject(new _messages.ResponseError(_lsp_error_codes.InternalError, 'Server error', error)));
        this.eventEmitter.on('exit', () => reject(new _messages.ResponseError(_lsp_error_codes.RequestCancelled, 'Server closed')));
        this.eventEmitter.on('connect', () => resolve(this.clientConnection));
      });
    }
  }

  async initialize(clientCapabilities, workspaceFolders, initOptions) {
    if (this.error) {
      throw this.error;
    }

    const clientConn = await this.connected();
    const rootUri = workspaceFolders[0].uri;

    if (initOptions && initOptions.clientCapabilities && Object.keys(clientCapabilities).length === 0) {
      clientCapabilities = initOptions.clientCapabilities;
    }

    const params = {
      processId: null,
      workspaceFolders,
      rootUri,
      capabilities: clientCapabilities,
      rootPath: (0, _url.fileURLToPath)(rootUri)
    };
    this.logger.debug(`sending initialize for ${params.rootPath}`);
    return await clientConn.sendRequest('initialize', initOptions && initOptions.initialOptions ? { ...params,
      initializationOptions: initOptions.initialOptions
    } : params).then(r => {
      this.logger.info(`initialized at ${rootUri}`); // @ts-ignore
      // TODO fix this

      clientConn.sendNotification(_main.InitializedNotification.type, {});
      this.initialized = true;
      return r;
    });
  }

  async shutdown() {
    const clientConn = await this.connected();
    this.logger.info(`sending shutdown request`);
    return await clientConn.sendRequest('shutdown');
  }
  /**
   * send a exit request to Language Server
   * https://microsoft.github.io/language-server-protocol/specification#exit
   */


  async exit() {
    this.closed = true; // stop the socket reconnect

    if (this.clientConnection) {
      this.logger.info('sending `shutdown` request to language server.');
      const clientConn = this.clientConnection;
      clientConn.sendRequest('shutdown');
      this.logger.info('sending `exit` notification to language server.'); // @ts-ignore

      clientConn.sendNotification(_main.ExitNotification.type);
    }

    this.eventEmitter.emit('exit');
  }

  startServerConnection() {
    // prevent calling this method multiple times which may cause 'port already in use' error
    if (this.currentServer) {
      if (this.listeningPort === this.targetPort) {
        return;
      } else {
        this.currentServer.close();
      }
    }

    const server = net.createServer(socket => {
      this.initialized = false;
      server.close();
      this.currentServer = null;
      socket.on('close', () => this.onSocketClosed());
      this.eventEmitter.off('changePort', server.close);
      this.logger.info('langserver connection established on port ' + this.targetPort);
      const reader = new _vscodeJsonrpc.SocketMessageReader(socket);
      const writer = new _vscodeJsonrpc.SocketMessageWriter(socket);
      this.clientConnection = (0, _vscodeJsonrpc.createMessageConnection)(reader, writer, this.logger);
      this.registerOnNotificationHandler(this.clientConnection);
      this.clientConnection.listen();
      this.eventEmitter.emit('connect');
    });
    server.on('error', this.setError);
    const port = this.targetPort;
    server.listen(port, () => {
      this.listeningPort = port;
      this.currentServer = server;
      server.removeListener('error', this.setError);
      this.logger.info('Wait langserver connection on port ' + this.targetPort);
    });
  }
  /**
   * get notification when proxy's socket disconnect
   * @param listener
   */


  onDisconnected(listener) {
    this.eventEmitter.on('close', listener);
  }

  onExit(listener) {
    this.eventEmitter.on('exit', listener);
  }
  /**
   * get notification when proxy's socket connect
   * @param listener
   */


  onConnected(listener) {
    this.eventEmitter.on('connect', listener);
  }

  connect() {
    this.logger.debug('connecting');
    this.socket = new net.Socket();
    this.socket.on('connect', () => {
      const reader = new _vscodeJsonrpc.SocketMessageReader(this.socket);
      const writer = new _vscodeJsonrpc.SocketMessageWriter(this.socket);
      this.clientConnection = (0, _vscodeJsonrpc.createMessageConnection)(reader, writer, this.logger);
      this.registerOnNotificationHandler(this.clientConnection);
      this.clientConnection.listen();
      this.eventEmitter.emit('connect');
    });
    this.socket.on('close', () => this.onSocketClosed());
    this.socket.on('error', () => void 0);
    this.socket.on('timeout', () => void 0);
    this.socket.on('drain', () => void 0);
    this.socket.connect(this.targetPort, this.targetHost);
  }

  unloadWorkspace(workspaceDir) {
    return Promise.reject('should not hit here');
  }

  onSocketClosed() {
    this.clientConnection = null;
    this.eventEmitter.emit('close');
  }

  registerOnNotificationHandler(clientConnection) {
    // @ts-ignore
    clientConnection.onNotification(_main.LogMessageNotification.type, notification => {
      switch (notification.type) {
        case _main.MessageType.Log:
          this.logger.debug(notification.message);
          break;

        case _main.MessageType.Info:
          this.logger.debug(notification.message);
          break;

        case _main.MessageType.Warning:
          this.logger.warn(notification.message);
          break;

        case _main.MessageType.Error:
          this.logger.error(notification.message);
          break;
      }
    });
  }

  changePort(port) {
    if (port !== this.targetPort) {
      this.targetPort = port;
    }
  }

  setError(error) {
    this.error = error;
    this.eventEmitter.emit('err', error);
  }

}

exports.LanguageServerProxy = LanguageServerProxy;