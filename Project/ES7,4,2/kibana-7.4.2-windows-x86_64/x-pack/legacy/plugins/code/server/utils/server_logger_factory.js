"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ServerLoggerFactory = void 0;

var _log = require("../log");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class ServerLoggerFactory {
  constructor(server) {
    this.server = server;
  }

  getLogger(tags) {
    return new _log.Logger(this.server, tags);
  }

}

exports.ServerLoggerFactory = ServerLoggerFactory;