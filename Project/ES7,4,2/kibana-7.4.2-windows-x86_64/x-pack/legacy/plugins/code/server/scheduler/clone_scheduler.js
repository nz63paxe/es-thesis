"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CloneScheduler = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _repository_utils = require("../../common/repository_utils");

var _search = require("../search");

var _abstract_scheduler = require("./abstract_scheduler");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Currently, this clone schedule is only used for reclone git repository
// in case the local repository is gone for any reasons. This will only
// be scheduled once at the startup time of Kibana.
class CloneScheduler extends _abstract_scheduler.AbstractScheduler {
  constructor(cloneWorker, serverOptions, client, log, onScheduleFinished) {
    super(client, Number.MAX_SAFE_INTEGER, onScheduleFinished);
    this.cloneWorker = cloneWorker;
    this.serverOptions = serverOptions;
    this.client = client;
    this.log = log;
    this.onScheduleFinished = onScheduleFinished;

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  getRepoSchedulingFrequencyMs() {
    // We don't need this scheduler to be executed repeatedly for now.
    return Number.MAX_SAFE_INTEGER;
  }

  async executeSchedulingJob(repo) {
    const {
      uri,
      url
    } = repo;
    this.log.debug(`Try to schedule clone repo request for ${uri}`);

    try {
      // 1. Check if the repository is in deletion
      let inDelete = false;

      try {
        await this.objectClient.getRepositoryDeleteStatus(repo.uri);
        inDelete = true;
      } catch (error) {
        inDelete = false;
      } // 2. Check if the folder exsits


      const path = _repository_utils.RepositoryUtils.repositoryLocalPath(this.serverOptions.repoPath, uri);

      const repoExist = _fs.default.existsSync(path);

      if (!inDelete && !repoExist) {
        this.log.info(`Repository does not exist on local disk. Start to schedule clone repo request for ${uri}`);
        const payload = {
          url
        };
        await this.cloneWorker.enqueueJob(payload, {});
      } else {
        this.log.debug(`Repository ${uri} has not been fully cloned yet or in update/delete. Skip clone.`);
      }
    } catch (error) {
      this.log.error(`Schedule clone for ${uri} error.`);
      this.log.error(error);
    }
  }

}

exports.CloneScheduler = CloneScheduler;