"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkCodeNode = checkCodeNode;
exports.checkRoute = checkRoute;

var _nodeFetch = _interopRequireDefault(require("node-fetch"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function checkCodeNode(url, log, rndStr) {
  try {
    const res = await (0, _nodeFetch.default)(`${url}/api/code/codeNode?rndStr=${rndStr}`, {});

    if (res.ok) {
      return await res.json();
    }
  } catch (e) {
    // request failed
    log.error(e);
  }

  log.info(`Access code node ${url} failed, try again later.`);
  return null;
}

function checkRoute(server, rndStr) {
  server.route({
    method: 'GET',
    path: '/api/code/codeNode',
    options: {
      auth: false
    },

    handler(req) {
      return {
        me: req.query.rndStr === rndStr
      };
    }

  });
}