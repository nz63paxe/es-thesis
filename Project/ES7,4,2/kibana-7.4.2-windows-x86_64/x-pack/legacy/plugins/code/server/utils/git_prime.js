"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CommitDescription", {
  enumerable: true,
  get: function () {
    return isogit.CommitDescription;
  }
});
Object.defineProperty(exports, "TagDescription", {
  enumerable: true,
  get: function () {
    return isogit.TagDescription;
  }
});
Object.defineProperty(exports, "TreeDescription", {
  enumerable: true,
  get: function () {
    return isogit.TreeDescription;
  }
});
Object.defineProperty(exports, "TreeEntry", {
  enumerable: true,
  get: function () {
    return isogit.TreeEntry;
  }
});
exports.GitPrime = void 0;

var _nodegit = require("@elastic/nodegit");

var isogit = _interopRequireWildcard(require("isomorphic-git"));

var fs = _interopRequireWildcard(require("fs"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

isogit.plugins.set('fs', fs);

class GitPrime {
  static async readObject({
    format = 'parsed',
    gitdir,
    oid,
    filepath
  }) {
    const repo = await _nodegit.Repository.openBare(gitdir);
    const odb = await repo.odb();
    const o = await odb.read(_nodegit.Oid.fromString(oid));
    const obj = new WrappedObj(o, repo);

    if (obj.type === 'commit' && filepath) {
      const commit = await repo.getCommit(o.id());
      const entry = await commit.getEntry(filepath);
      return GitPrime.readObject({
        oid: entry.oid(),
        gitdir,
        format
      });
    }

    if (format === 'parsed' || format === 'content') {
      await obj.parse();
    }

    return obj;
  }

  static async resolveRef({
    gitdir,
    ref,
    depth
  }) {
    return await isogit.resolveRef({
      gitdir,
      ref,
      depth
    });
  }

  static async expandOid({
    gitdir,
    oid
  }) {
    const repo = await _nodegit.Repository.openBare(gitdir);
    const o = await _nodegit.Object.lookupPrefix(repo, _nodegit.Oid.fromString(oid), oid.length, _nodegit.Object.TYPE.COMMIT);
    return o.id().tostrS();
  }

  static async listBranches(param) {
    return await isogit.listBranches(param);
  }

  static async listTags(param) {
    return await isogit.listTags(param);
  }

  static async isTextFile({
    gitdir,
    oid
  }) {
    const repo = await _nodegit.Repository.openBare(gitdir);
    const blob = await repo.getBlob(oid);
    return blob.isBinary() === 0;
  }

}

exports.GitPrime = GitPrime;

class WrappedObj {
  constructor(o, repo) {
    this.o = o;
    this.repo = repo;

    _defineProperty(this, "_format", 'content');

    _defineProperty(this, "_object", void 0);

    this._object = null;
  }

  get object() {
    return this._object;
  }

  get format() {
    return this._format;
  }

  get oid() {
    return this.o.id().tostrS();
  }

  get type() {
    return type2str(this.o.type());
  }

  async parse() {
    function fromSignature(sign) {
      return {
        name: sign.name(),
        email: sign.email(),
        timestamp: sign.when().time,
        timezoneOffset: sign.when().offset
      };
    }

    switch (this.o.type()) {
      case 1:
        const commit = await this.repo.getCommit(this.o.id());
        this._object = {
          tree: commit.treeId().tostrS(),
          author: fromSignature(commit.author()),
          message: commit.message(),
          committer: fromSignature(commit.committer()),
          parent: commit.parents().map(o => o.tostrS())
        };
        break;

      case 2:
        const tree = await this.repo.getTree(this.o.id());
        const entries = tree.entries().map(convertEntry);
        this._object = {
          entries
        };
        break;

      case 3:
        const blob = await this.repo.getBlob(this.o.id());
        this._object = {
          content() {
            return blob.content();
          },

          isBinary() {
            return blob.isBinary() === 1;
          },

          rawsize() {
            return blob.rawsize();
          }

        };
        break;

      case 4:
        const tag = await this.repo.getTag(this.o.id());
        this._object = {
          message: tag.message(),
          object: tag.targetId().tostrS(),
          tagger: fromSignature(tag.tagger()),
          tag: tag.name(),
          type: type2str(tag.targetType())
        };
        break;

      default:
        throw new Error('invalid object type ' + this.o.type());
    }

    this._format = 'parsed';
  }

}

function convertEntry(t) {
  let mode;

  switch (t.filemode()) {
    case _nodegit.TreeEntry.FILEMODE.EXECUTABLE:
      mode = '100755';
      break;

    case _nodegit.TreeEntry.FILEMODE.BLOB:
      mode = '100644';
      break;

    case _nodegit.TreeEntry.FILEMODE.COMMIT:
      mode = '160000';
      break;

    case _nodegit.TreeEntry.FILEMODE.TREE:
      mode = '040000';
      break;

    case _nodegit.TreeEntry.FILEMODE.LINK:
      mode = '120000';
      break;

    default:
      throw new Error('invalid file mode.');
  }

  return {
    mode,
    path: t.path(),
    oid: t.sha(),
    type: type2str(t.type())
  };
}

function type2str(t) {
  switch (t) {
    case 1:
      return 'commit';

    case 2:
      return 'tree';

    case 3:
      return 'blob';

    case 4:
      return 'tag';

    default:
      throw new Error('invalid object type ' + t);
  }
}