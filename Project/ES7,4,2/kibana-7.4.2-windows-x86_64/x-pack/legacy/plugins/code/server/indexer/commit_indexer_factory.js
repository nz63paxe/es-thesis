"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitIndexerFactory = void 0;

var _ = require(".");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CommitIndexerFactory {
  constructor(gitOps, client, log) {
    this.gitOps = gitOps;
    this.client = client;
    this.log = log;
  }

  async create(repoUri, revision, // Apply this param when the incremental indexer has been built.
  enforcedReindex = false) {
    this.log.info(`Create indexer to index ${repoUri} commits`); // Create the indexer to index the entire repository.

    return new _.CommitIndexer(repoUri, revision, this.gitOps, this.client, this.log);
  }

}

exports.CommitIndexerFactory = CommitIndexerFactory;