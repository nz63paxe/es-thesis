"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.repositorySearchRoute = repositorySearchRoute;
exports.documentSearchRoute = documentSearchRoute;
exports.symbolSearchRoute = symbolSearchRoute;
exports.commitSearchRoute = commitSearchRoute;

var _boom = _interopRequireDefault(require("boom"));

var _search = require("../search");

var _esclient_with_request = require("../utils/esclient_with_request");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function repositorySearchRoute(router, log) {
  router.route({
    path: '/api/code/search/repo',
    method: 'GET',

    async handler(req) {
      let page = 1;
      const {
        p,
        q,
        repoScope
      } = req.query;

      if (p) {
        page = parseInt(p, 10);
      }

      let scope = [];

      if (typeof repoScope === 'string') {
        scope = repoScope.split(',');
      }

      const searchReq = {
        query: q,
        page,
        repoScope: scope
      };

      try {
        const repoSearchClient = new _search.RepositorySearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        const res = await repoSearchClient.search(searchReq);
        return res;
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
  router.route({
    path: '/api/code/suggestions/repo',
    method: 'GET',

    async handler(req) {
      let page = 1;
      const {
        p,
        q,
        repoScope
      } = req.query;

      if (p) {
        page = parseInt(p, 10);
      }

      let scope = [];

      if (typeof repoScope === 'string') {
        scope = repoScope.split(',');
      }

      const searchReq = {
        query: q,
        page,
        repoScope: scope
      };

      try {
        const repoSearchClient = new _search.RepositorySearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        const res = await repoSearchClient.suggest(searchReq);
        return res;
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
}

function documentSearchRoute(router, log) {
  router.route({
    path: '/api/code/search/doc',
    method: 'GET',

    async handler(req) {
      let page = 1;
      const {
        p,
        q,
        langs,
        repos,
        repoScope
      } = req.query;

      if (p) {
        page = parseInt(p, 10);
      }

      let scope = [];

      if (typeof repoScope === 'string') {
        scope = [repoScope];
      } else if (Array.isArray(repoScope)) {
        scope = repoScope;
      }

      const searchReq = {
        query: q,
        page,
        langFilters: langs ? langs.split(',') : [],
        repoFilters: repos ? decodeURIComponent(repos).split(',') : [],
        repoScope: scope
      };

      try {
        const docSearchClient = new _search.DocumentSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        const res = await docSearchClient.search(searchReq);
        return res;
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
  router.route({
    path: '/api/code/suggestions/doc',
    method: 'GET',

    async handler(req) {
      let page = 1;
      const {
        p,
        q,
        repoScope
      } = req.query;

      if (p) {
        page = parseInt(p, 10);
      }

      let scope = [];

      if (typeof repoScope === 'string') {
        scope = repoScope.split(',');
      }

      const searchReq = {
        query: q,
        page,
        repoScope: scope
      };

      try {
        const docSearchClient = new _search.DocumentSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        const res = await docSearchClient.suggest(searchReq);
        return res;
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
}

function symbolSearchRoute(router, log) {
  const symbolSearchHandler = async req => {
    let page = 1;
    const {
      p,
      q,
      repoScope
    } = req.query;

    if (p) {
      page = parseInt(p, 10);
    }

    let scope = [];

    if (typeof repoScope === 'string') {
      scope = repoScope.split(',');
    }

    const searchReq = {
      query: q,
      page,
      repoScope: scope
    };

    try {
      const symbolSearchClient = new _search.SymbolSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
      const res = await symbolSearchClient.suggest(searchReq);
      return res;
    } catch (error) {
      return _boom.default.internal(`Search Exception`);
    }
  }; // Currently these 2 are the same.


  router.route({
    path: '/api/code/suggestions/symbol',
    method: 'GET',
    handler: symbolSearchHandler
  });
  router.route({
    path: '/api/code/search/symbol',
    method: 'GET',
    handler: symbolSearchHandler
  });
}

function commitSearchRoute(router, log) {
  router.route({
    path: '/api/code/search/commit',
    method: 'GET',

    async handler(req) {
      let page = 1;
      const {
        p,
        q,
        repos,
        repoScope
      } = req.query;

      if (p) {
        page = parseInt(p, 10);
      }

      let scope = [];

      if (typeof repoScope === 'string') {
        scope = [repoScope];
      } else if (Array.isArray(repoScope)) {
        scope = repoScope;
      }

      const searchReq = {
        query: q,
        page,
        repoFilters: repos ? decodeURIComponent(repos).split(',') : [],
        repoScope: scope
      };

      try {
        const commitSearchClient = new _search.CommitSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        const res = await commitSearchClient.search(searchReq);
        return res;
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
}