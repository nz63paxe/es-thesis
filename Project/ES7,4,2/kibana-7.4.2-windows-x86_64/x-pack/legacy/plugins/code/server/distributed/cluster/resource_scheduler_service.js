"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResourceSchedulerService = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _cluster_state = require("./cluster_state");

var _resource_scheduler = require("./resource_scheduler");

var _hash_schedule_policy = require("./hash_schedule_policy");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * This service watches the change of the cluster state and allocates resources to nodes if needed:
 * - when a new resource is added to the cluster
 * - a resource is unassigned, e.g., due to disk watermark constraints
 * - when a node is removed from the cluster (HA)
 */
class ResourceSchedulerService {
  constructor(clusterService, log) {
    this.clusterService = clusterService;
    this.log = log;

    _defineProperty(this, "scheduler", new _resource_scheduler.ResourceScheduler([new _hash_schedule_policy.HashSchedulePolicy()], this.log));

    this.clusterService.addClusterStateListener(this);
  }

  async start() {}

  async stop() {}

  async allocateUnassigned(state) {
    if (!state) {
      state = this.clusterService.state();
    }

    const unassignedRepos = state.getUnassignedRepositories();

    if (_lodash.default.isEmpty(unassignedRepos)) {
      return;
    }

    await this.clusterService.updateClusterState(current => {
      // sort repositories by names to make the allocation result stable
      const unassigned = current.getUnassignedRepositories().map(repo => repo.uri).sort();
      const assignments = this.scheduler.allocate(unassigned, current);

      if (_lodash.default.isEmpty(assignments)) {
        return current;
      }

      return new _cluster_state.ClusterState(current.clusterMeta, current.routingTable.assign(assignments), current.nodes);
    });
  }

  async onClusterStateChanged(event) {
    await this.allocateUnassigned(event.current);
  }

}

exports.ResourceSchedulerService = ResourceSchedulerService;