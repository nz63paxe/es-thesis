"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractScheduler = void 0;

var _poller = require("../poller");

var _search = require("../search");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AbstractScheduler {
  constructor(client, pollFrequencyMs, onScheduleFinished) {
    this.client = client;
    this.onScheduleFinished = onScheduleFinished;

    _defineProperty(this, "poller", void 0);

    this.poller = new _poller.Poller({
      functionToPoll: () => {
        return this.schedule();
      },
      pollFrequencyInMillis: pollFrequencyMs,
      trailing: true,
      continuePollingOnError: true,
      pollFrequencyErrorMultiplier: 2
    });
  }

  start() {
    this.poller.start();
  }

  stop() {
    this.poller.stop();
  }

  async schedule() {
    const repoObjectClient = new _search.RepositoryObjectClient(this.client);
    const repos = await repoObjectClient.getAllRepositories();
    const schedulingPromises = repos.map(r => {
      return this.executeSchedulingJob(r);
    });
    await Promise.all(schedulingPromises); // Execute the callback after each schedule is done.

    if (this.onScheduleFinished) {
      this.onScheduleFinished();
    }
  }

  repoNextSchedulingTime() {
    const duration = this.getRepoSchedulingFrequencyMs() / 2 + Math.random() * Number.MAX_SAFE_INTEGER % this.getRepoSchedulingFrequencyMs();
    const now = new Date().getTime();
    return new Date(now + duration);
  }

  getRepoSchedulingFrequencyMs() {
    // This is an abstract class. Do nothing here. You should override this.
    return -1;
  }

  async executeSchedulingJob(repo) {
    // This is an abstract class. Do nothing here. You should override this.
    return new Promise((resolve, _) => {
      resolve();
    });
  }

}

exports.AbstractScheduler = AbstractScheduler;