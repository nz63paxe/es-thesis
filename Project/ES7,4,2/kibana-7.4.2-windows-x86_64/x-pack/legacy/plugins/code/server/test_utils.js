"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createTestServerOption = createTestServerOption;
exports.createTestHapiServer = createTestHapiServer;
exports.emptyAsyncFunc = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _hapi = require("hapi");

var os = _interopRequireWildcard(require("os"));

var _path = _interopRequireDefault(require("path"));

var _server_options = require("./server_options");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// TODO migrate other duplicate classes, functions
const emptyAsyncFunc = async _ => {
  Promise.resolve({});
};

exports.emptyAsyncFunc = emptyAsyncFunc;
const TEST_OPTIONS = {
  enabled: true,
  queueIndex: '.code_internal-worker-queue',
  queueTimeoutMs: 60 * 60 * 1000,
  // 1 hour by default
  updateFreqencyMs: 5 * 60 * 1000,
  // 5 minutes by default
  indexFrequencyMs: 24 * 60 * 60 * 1000,
  // 1 day by default
  lsp: {
    requestTimeoutMs: 5 * 60,
    // timeout a request over 30s
    detach: false,
    verbose: false
  },
  security: {
    enableMavenImport: true,
    enableGradleImport: true,
    installNodeDependency: true,
    enableGitCertCheck: true,
    gitProtocolWhitelist: ['ssh', 'https', 'git']
  },
  disk: {
    thresholdEnabled: true,
    watermarkLow: '80%'
  },
  clustering: {
    enabled: false,
    codeNodes: []
  },
  repos: [],
  maxWorkspace: 5 // max workspace folder for each language server

};

function createTestServerOption() {
  const tmpDataPath = _fs.default.mkdtempSync(_path.default.join(os.tmpdir(), 'code_test'));

  const config = {
    get(key) {
      if (key === 'path.data') {
        return tmpDataPath;
      }
    }

  };
  return new _server_options.ServerOptions(TEST_OPTIONS, config);
}

function createTestHapiServer() {
  const server = new _hapi.Server(); // @ts-ignore

  server.config = () => {
    return {
      get(key) {
        if (key === 'env.dev') return false;else return true;
      }

    };
  };

  return server;
}