"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IndexWorker = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _model = require("../../model");

var _git_operations = require("../git_operations");

var _search = require("../search");

var _index_stats_aggregator = require("../utils/index_stats_aggregator");

var _abstract_worker = require("./abstract_worker");

var _cancellation_service = require("./cancellation_service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexWorker extends _abstract_worker.AbstractWorker {
  constructor(queue, log, client, indexerFactories, gitOps, cancellationService) {
    super(queue, log);
    this.queue = queue;
    this.log = log;
    this.client = client;
    this.indexerFactories = indexerFactories;
    this.gitOps = gitOps;
    this.cancellationService = cancellationService;

    _defineProperty(this, "id", 'index');

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  async executeJob(job) {
    const {
      payload,
      cancellationToken
    } = job;
    const {
      uri,
      revision,
      enforceReindex
    } = payload;
    const indexerNumber = this.indexerFactories.size;
    const {
      resume,
      checkpointReqs
    } = await this.shouldJobResume(payload);

    if (!resume) {
      this.log.info(`Index job skipped for ${uri} at revision ${revision}`);
      return {
        uri,
        revision
      };
    } // Binding the index cancellation logic


    let cancelled = false;
    await this.cancellationService.cancelIndexJob(uri, _cancellation_service.CancellationReason.NEW_JOB_OVERRIDEN);
    const indexPromises = Array.from(this.indexerFactories.values()).map(async factory => {
      const indexer = await factory.create(uri, revision, enforceReindex);

      if (!indexer) {
        this.log.info(`Failed to create indexer for ${uri}`);
        return new Map(); // return an empty map as stats.
      }

      if (cancellationToken) {
        cancellationToken.on(() => {
          indexer.cancel();
          cancelled = true;
        });
      }

      const progressReporter = this.getProgressReporter(uri, revision, indexer.type, indexerNumber);
      return indexer.start(progressReporter, checkpointReqs.get(indexer.type));
    });
    const allPromise = Promise.all(indexPromises);

    if (cancellationToken) {
      await this.cancellationService.registerCancelableIndexJob(uri, cancellationToken, allPromise);
    }

    const stats = await allPromise;
    const res = {
      uri,
      revision,
      stats: (0, _index_stats_aggregator.aggregateIndexStats)(stats),
      cancelled
    };
    return res;
  }

  async onJobEnqueued(job) {
    const {
      uri,
      revision
    } = job.payload;
    const progress = {
      uri,
      progress: _model.WorkerReservedProgress.INIT,
      timestamp: new Date(),
      revision
    };
    return await this.objectClient.setRepositoryIndexStatus(uri, progress);
  }

  async onJobCompleted(job, res) {
    if (res.cancelled) {
      // Skip updating job progress if the job is done because of cancellation.
      return;
    }

    this.log.info(`Index worker finished with stats: ${JSON.stringify([...res.stats])}`);
    await super.onJobCompleted(job, res);
    const {
      uri,
      revision
    } = job.payload;

    try {
      // Double check if the current revision is different from the origin reivsion.
      // If so, kick off another index job to catch up the data descrepency.
      const gitStatus = await this.objectClient.getRepositoryGitStatus(uri);

      if (gitStatus.revision !== revision) {
        const payload = {
          uri,
          revision: gitStatus.revision
        };
        await this.enqueueJob(payload, {});
      }

      return await this.objectClient.updateRepository(uri, {
        indexedRevision: revision
      });
    } catch (error) {
      this.log.error(`Update indexed revision in repository object error.`);
      this.log.error(error);
    }
  }

  async updateProgress(job, progress) {
    const {
      uri
    } = job.payload;
    let p = {
      uri,
      progress,
      timestamp: new Date()
    };

    if (progress === _model.WorkerReservedProgress.COMPLETED || progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT) {
      // Reset the checkpoints if necessary.
      p = { ...p,
        indexProgress: {
          checkpoint: null
        },
        commitIndexProgress: {
          checkpoint: null
        }
      };
    }

    try {
      return await this.objectClient.updateRepositoryIndexStatus(uri, p);
    } catch (error) {
      this.log.error(`Update index progress error.`);
      this.log.error(error);
    }
  }

  async getTimeoutMs(payload) {
    try {
      const totalCount = await this.gitOps.countRepoFiles(payload.uri, _git_operations.HEAD);

      let timeout = _moment.default.duration(1, 'hour').asMilliseconds();

      if (totalCount > 0) {
        // timeout = ln(file_count) in hour
        // e.g. 10 files -> 2.3 hours, 100 files -> 4.6 hours, 1000 -> 6.9 hours, 10000 -> 9.2 hours
        timeout = _moment.default.duration(Math.log(totalCount), 'hour').asMilliseconds();
      }

      this.log.info(`Set index job timeout to be ${timeout} ms.`);
      return timeout;
    } catch (error) {
      this.log.error(`Get repo file total count error.`);
      this.log.error(error);
      throw error;
    }
  }

  getProgressReporter(repoUri, revision, type, total) // total number of indexers
  {
    return async progress => {
      const indexStatus = await this.objectClient.getRepositoryIndexStatus(repoUri);
      const p = {
        uri: repoUri,
        progress: indexStatus.progress,
        timestamp: new Date(),
        revision
      };

      switch (type) {
        case _model.IndexerType.COMMIT:
          p.commitIndexProgress = progress;
          p.progress = progress.percentage + (indexStatus.indexProgress ? indexStatus.indexProgress.percentage : 0);
          break;

        case _model.IndexerType.LSP:
          p.indexProgress = progress;
          p.progress = progress.percentage + (indexStatus.commitIndexProgress ? indexStatus.commitIndexProgress.percentage : 0);
          break;

        default:
          this.log.warn(`Unknown indexer type ${type} for indexing progress report.`);
          break;
      }

      return await this.objectClient.updateRepositoryIndexStatus(repoUri, p);
    };
  } // 1. Try to load checkpointed requests for all indexers to the `checkpointReqs` field
  // of the return value.
  // 2. Make a decision on if the index job should proceed indicate by the boolean `resume`
  // field of the return value.


  async shouldJobResume(payload) {
    const {
      uri,
      revision
    } = payload;
    const workerProgress = await this.objectClient.getRepositoryIndexStatus(uri);
    let lspCheckpointReq;
    let commitCheckpointReq;
    const checkpointReqs = new Map();

    if (workerProgress) {
      // There exist an ongoing index process
      const {
        uri: currentUri,
        revision: currentRevision,
        indexProgress: currentLspIndexProgress,
        commitIndexProgress: currentCommitIndexProgress,
        progress
      } = workerProgress;
      lspCheckpointReq = currentLspIndexProgress && currentLspIndexProgress.checkpoint;
      commitCheckpointReq = currentCommitIndexProgress && currentCommitIndexProgress.checkpoint;

      if (!lspCheckpointReq && !commitCheckpointReq && progress > _model.WorkerReservedProgress.INIT && progress < _model.WorkerReservedProgress.COMPLETED && currentUri === uri && currentRevision === revision) {
        // Dedup this index job request if:
        // 1. no checkpoint exist (undefined or empty string) for either LSP or commit indexer
        // 2. index progress is ongoing
        // 3. the uri and revision match the current job
        return {
          resume: false,
          checkpointReqs
        };
      }
    } // Add the checkpoints into the map as a result to return. They could be undefined.


    checkpointReqs.set(_model.IndexerType.LSP, lspCheckpointReq);
    checkpointReqs.set(_model.IndexerType.LSP_INC, lspCheckpointReq);
    checkpointReqs.set(_model.IndexerType.COMMIT, commitCheckpointReq);
    checkpointReqs.set(_model.IndexerType.COMMIT_INC, commitCheckpointReq);
    return {
      resume: true,
      checkpointReqs
    };
  }

}

exports.IndexWorker = IndexWorker;