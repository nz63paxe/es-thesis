"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspIndexerFactory = void 0;

var _ = require(".");

var _search = require("../search");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LspIndexerFactory {
  constructor(lspService, options, gitOps, client, log) {
    this.lspService = lspService;
    this.options = options;
    this.gitOps = gitOps;
    this.client = client;
    this.log = log;

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  async create(repoUri, revision, enforcedReindex = false) {
    try {
      const repo = await this.objectClient.getRepository(repoUri);
      const indexedRevision = repo.indexedRevision; // Skip incremental indexer if enforced reindex.

      if (!enforcedReindex && indexedRevision) {
        this.log.info(`Create indexer to index ${repoUri} from ${indexedRevision} to ${revision}`); // Create the indexer to index only the diff between these 2 revisions.

        return new _.LspIncrementalIndexer(repo.uri, revision, indexedRevision, this.lspService, this.options, this.gitOps, this.client, this.log);
      } else {
        this.log.info(`Create indexer to index ${repoUri} at ${revision}`); // Create the indexer to index the entire repository.

        return new _.LspIndexer(repo.uri, revision, this.lspService, this.options, this.gitOps, this.client, this.log);
      }
    } catch (error) {
      this.log.error(`Create indexer error for ${repoUri}.`);
      this.log.error(error);
      return undefined;
    }
  }

}

exports.LspIndexerFactory = LspIndexerFactory;