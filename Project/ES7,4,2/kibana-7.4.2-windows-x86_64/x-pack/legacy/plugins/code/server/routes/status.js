"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.statusRoute = statusRoute;

var _boom = _interopRequireDefault(require("boom"));

var _repo_file_status = require("../../common/repo_file_status");

var _language_servers = require("../lsp/language_servers");

var _language_server = require("../../common/language_server");

var _request_expander = require("../lsp/request_expander");

var _search = require("../search");

var _esclient_with_request = require("../utils/esclient_with_request");

var _apis = require("../distributed/apis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function statusRoute(router, codeServices) {
  const gitService = codeServices.serviceFor(_apis.GitServiceDefinition);
  const lspService = codeServices.serviceFor(_apis.LspServiceDefinition);

  async function handleRepoStatus(endpoint, report, repoUri, revision, repoObjectClient) {
    const commit = await gitService.commit(endpoint, {
      uri: repoUri,
      revision: decodeURIComponent(revision)
    });
    const head = await gitService.headRevision(endpoint, {
      uri: repoUri
    });

    if (head === commit.id) {
      try {
        const indexStatus = await repoObjectClient.getRepositoryIndexStatus(repoUri);

        if (indexStatus.progress < 100) {
          report.repoStatus = _repo_file_status.RepoFileStatus.INDEXING;
        }
      } catch (e) {
        // index may not stated yet
        report.repoStatus = _repo_file_status.RepoFileStatus.INDEXING;
      }
    } else {
      report.repoStatus = _repo_file_status.RepoFileStatus.REVISION_NOT_INDEXED;
    }
  }

  async function handleFileStatus(endpoint, report, blob) {
    if (blob.content) {
      const lang = blob.lang;
      const defs = await lspService.languageSeverDef(endpoint, {
        lang
      });

      if (defs.length === 0) {
        report.fileStatus = _repo_file_status.RepoFileStatus.FILE_NOT_SUPPORTED;
      } else {
        return defs;
      }
    } else {
      report.fileStatus = _repo_file_status.RepoFileStatus.FILE_IS_TOO_BIG;
    }

    return [];
  }

  async function handleLspStatus(endpoint, report, defs, repoUri, revision) {
    const dedicated = defs.find(d => d !== _language_servers.CTAGS);
    const generic = defs.find(d => d === _language_servers.CTAGS);
    report.langServerType = dedicated ? _repo_file_status.LangServerType.DEDICATED : _repo_file_status.LangServerType.GENERIC;

    if (dedicated && (await lspService.languageServerStatus(endpoint, {
      langName: dedicated.name
    })) === _language_server.LanguageServerStatus.NOT_INSTALLED) {
      report.langServerStatus = _repo_file_status.RepoFileStatus.LANG_SERVER_NOT_INSTALLED;

      if (generic) {
        // dedicated lang server not installed, fallback to generic
        report.langServerType = _repo_file_status.LangServerType.GENERIC;
      }
    } else {
      const def = dedicated || generic;
      const state = await lspService.initializeState(endpoint, {
        repoUri,
        revision
      });
      const initState = state[def.name];
      report.langServerStatus = initState === _request_expander.WorkspaceStatus.Initialized ? _repo_file_status.RepoFileStatus.LANG_SERVER_INITIALIZED : _repo_file_status.RepoFileStatus.LANG_SERVER_IS_INITIALIZING;
    }
  }

  router.route({
    path: '/api/code/repo/{uri*3}/status/{ref}/{path*}',
    method: 'GET',

    async handler(req) {
      const {
        uri,
        path,
        ref
      } = req.params;
      const report = {};
      const repoObjectClient = new _search.RepositoryObjectClient(new _esclient_with_request.EsClientWithRequest(req));
      const endpoint = await codeServices.locate(req, uri);

      try {
        // Check if the repository already exists
        await repoObjectClient.getRepository(uri);
      } catch (e) {
        return _boom.default.notFound(`repo ${uri} not found`);
      }

      await handleRepoStatus(endpoint, report, uri, ref, repoObjectClient);

      if (path) {
        try {
          try {
            const blob = await gitService.blob(endpoint, {
              uri,
              path,
              revision: decodeURIComponent(ref)
            }); // text file

            if (!blob.isBinary) {
              const defs = await handleFileStatus(endpoint, report, blob);

              if (defs.length > 0) {
                await handleLspStatus(endpoint, report, defs, uri, ref);
              }
            }
          } catch (e) {// not a file? The path may be a dir.
          }
        } catch (e) {
          return _boom.default.internal(e.message || e.name);
        }
      }

      return report;
    }

  });
}