"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiskWatermarkService = exports.DEFAULT_WATERMARK_LOW_PERCENTAGE = void 0;

var _i18n = require("@kbn/i18n");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const DEFAULT_WATERMARK_LOW_PERCENTAGE = 90;
exports.DEFAULT_WATERMARK_LOW_PERCENTAGE = DEFAULT_WATERMARK_LOW_PERCENTAGE;

class DiskWatermarkService {
  // True for percentage mode (e.g. 90%), false for absolute mode (e.g. 500mb)
  constructor(diskSpaceChecker, serverOptions, logger) {
    this.diskSpaceChecker = diskSpaceChecker;
    this.serverOptions = serverOptions;
    this.logger = logger;

    _defineProperty(this, "percentageMode", true);

    _defineProperty(this, "watermark", DEFAULT_WATERMARK_LOW_PERCENTAGE);

    _defineProperty(this, "enabled", false);

    this.enabled = this.serverOptions.disk.thresholdEnabled;

    if (this.enabled) {
      this.parseWatermarkConfigString(this.serverOptions.disk.watermarkLow);
    }
  }

  async isLowWatermark() {
    if (!this.enabled) {
      return false;
    }

    try {
      const res = await this.diskSpaceChecker(this.serverOptions.repoPath);
      const {
        free,
        size
      } = res;

      if (this.percentageMode) {
        const percentage = (size - free) * 100 / size;
        return percentage > this.watermark;
      } else {
        return free <= this.watermark;
      }
    } catch (err) {
      return true;
    }
  }

  diskWatermarkViolationMessage() {
    if (this.percentageMode) {
      return _i18n.i18n.translate('xpack.code.git.diskWatermarkLowPercentageMessage', {
        defaultMessage: `Disk usage watermark level higher than {watermark}`,
        values: {
          watermark: this.serverOptions.disk.watermarkLow
        }
      });
    } else {
      return _i18n.i18n.translate('xpack.code.git.diskWatermarkLowMessage', {
        defaultMessage: `Available disk space lower than {watermark}`,
        values: {
          watermark: this.serverOptions.disk.watermarkLow
        }
      });
    }
  }

  parseWatermarkConfigString(diskWatermarkLow) {
    // Including undefined, null and empty string.
    if (!diskWatermarkLow) {
      this.logger.error(`Empty disk watermark config for Code. Fallback with default value (${DEFAULT_WATERMARK_LOW_PERCENTAGE}%)`);
      return;
    }

    try {
      const str = diskWatermarkLow.trim().toLowerCase();

      if (str.endsWith('%')) {
        this.percentageMode = true;
        this.watermark = parseInt(str.substr(0, str.length - 1), 10);
      } else if (str.endsWith('kb')) {
        this.percentageMode = false;
        this.watermark = parseInt(str.substr(0, str.length - 2), 10) * Math.pow(1024, 1);
      } else if (str.endsWith('mb')) {
        this.percentageMode = false;
        this.watermark = parseInt(str.substr(0, str.length - 2), 10) * Math.pow(1024, 2);
      } else if (str.endsWith('gb')) {
        this.percentageMode = false;
        this.watermark = parseInt(str.substr(0, str.length - 2), 10) * Math.pow(1024, 3);
      } else if (str.endsWith('tb')) {
        this.percentageMode = false;
        this.watermark = parseInt(str.substr(0, str.length - 2), 10) * Math.pow(1024, 4);
      } else {
        throw new Error('Unrecognized unit for disk size config.');
      }
    } catch (error) {
      this.logger.error(`Invalid disk watermark config for Code. Fallback with default value (${DEFAULT_WATERMARK_LOW_PERCENTAGE}%)`);
    }
  }

}

exports.DiskWatermarkService = DiskWatermarkService;