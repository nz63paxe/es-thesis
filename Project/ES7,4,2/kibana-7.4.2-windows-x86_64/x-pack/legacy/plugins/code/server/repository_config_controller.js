"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryConfigController = void 0;

var _uri_util = require("../common/uri_util");

var _search = require("./search");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class RepositoryConfigController {
  constructor(esClient) {
    this.esClient = esClient;

    _defineProperty(this, "repositoryConfigCache", {});

    _defineProperty(this, "repoObjectClient", void 0);

    this.repoObjectClient = new _search.RepositoryObjectClient(esClient);
  }

  async isLanguageDisabled(uri, lang) {
    const {
      repoUri
    } = (0, _uri_util.parseLspUrl)(uri);
    let repoConfig = this.repositoryConfigCache[repoUri];

    if (!repoConfig) {
      try {
        repoConfig = await this.repoObjectClient.getRepositoryConfig(repoUri);
      } catch (err) {
        return false;
      }
    }

    if (lang === 'go' && repoConfig.disableGo === true) {
      return true;
    }

    if (lang === 'java' && repoConfig.disableJava === true) {
      return true;
    }

    if (lang === 'typescript' && repoConfig.disableTypescript === true) {
      return true;
    }

    return false;
  }

  async resetConfigCache(repoUri) {
    delete this.repositoryConfigCache[repoUri];
  }

}

exports.RepositoryConfigController = RepositoryConfigController;