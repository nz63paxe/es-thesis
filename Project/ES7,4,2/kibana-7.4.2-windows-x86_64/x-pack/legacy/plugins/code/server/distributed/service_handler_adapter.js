"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_SERVICE_OPTION = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DEFAULT_SERVICE_OPTION = {
  routePrefix: '/api/code/internal'
};
exports.DEFAULT_SERVICE_OPTION = DEFAULT_SERVICE_OPTION;