"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspIndexer = void 0;

var _vscodeJsonrpc = require("vscode-jsonrpc");

var _file = require("../../common/file");

var _lsp_error_codes = require("../../common/lsp_error_codes");

var _uri_util = require("../../common/uri_util");

var _model = require("../../model");

var _git_operations = require("../git_operations");

var _detect_language = require("../utils/detect_language");

var _abstract_indexer = require("./abstract_indexer");

var _batch_index_helper = require("./batch_index_helper");

var _index_creation_request = require("./index_creation_request");

var _schema = require("./schema");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LspIndexer extends _abstract_indexer.AbstractIndexer {
  // Batch index helper for symbols/references
  // Batch index helper for documents
  constructor(repoUri, revision, lspService, options, gitOps, client, log) {
    super(repoUri, revision, client, log);
    this.repoUri = repoUri;
    this.revision = revision;
    this.lspService = lspService;
    this.options = options;
    this.gitOps = gitOps;
    this.client = client;
    this.log = log;

    _defineProperty(this, "type", _model.IndexerType.LSP);

    _defineProperty(this, "lspBatchIndexHelper", void 0);

    _defineProperty(this, "docBatchIndexHelper", void 0);

    _defineProperty(this, "LSP_BATCH_INDEX_SIZE", 1000);

    _defineProperty(this, "DOC_BATCH_INDEX_SIZE", 50);

    _defineProperty(this, "FILE_OVERSIZE_ERROR_MSG", 'File size exceeds limit. Skip index.');

    this.lspBatchIndexHelper = new _batch_index_helper.BatchIndexHelper(client, log, this.LSP_BATCH_INDEX_SIZE);
    this.docBatchIndexHelper = new _batch_index_helper.BatchIndexHelper(client, log, this.DOC_BATCH_INDEX_SIZE);
  }

  async start(progressReporter, checkpointReq) {
    try {
      return await super.start(progressReporter, checkpointReq);
    } finally {
      if (!this.isCancelled()) {
        // Flush all the index request still in the cache for bulk index.
        this.lspBatchIndexHelper.flush();
        this.docBatchIndexHelper.flush();
      }
    }
  }

  cancel() {
    this.lspBatchIndexHelper.cancel();
    this.docBatchIndexHelper.cancel();
    super.cancel();
  } // If the current checkpoint is valid


  validateCheckpoint(checkpointReq) {
    return checkpointReq !== undefined && checkpointReq.revision === this.revision;
  } // If it's necessary to refresh (create and reset) all the related indices


  needRefreshIndices(checkpointReq) {
    // If it's not resumed from a checkpoint, then try to refresh all the indices.
    return !this.validateCheckpoint(checkpointReq);
  }

  ifCheckpointMet(req, checkpointReq) {
    // Assume for the same revision, the order of the files we iterate the repository is definite
    // everytime.
    return req.filePath === checkpointReq.filePath && req.revision === checkpointReq.revision;
  }

  async prepareIndexCreationRequests() {
    return [(0, _index_creation_request.getDocumentIndexCreationRequest)(this.repoUri), (0, _index_creation_request.getReferenceIndexCreationRequest)(this.repoUri), (0, _index_creation_request.getSymbolIndexCreationRequest)(this.repoUri)];
  }

  async *getIndexRequestIterator() {
    try {
      const fileIterator = await this.gitOps.iterateRepo(this.repoUri, _git_operations.HEAD);

      for await (const file of fileIterator) {
        const filePath = file.path;
        const req = {
          repoUri: this.repoUri,
          filePath,
          // Always use HEAD for now until we have multi revision.
          // Also, since the workspace might get updated during the index, we always
          // want the revision to keep updated so that lsp proxy could pass the revision
          // check per discussion here: https://github.com/elastic/code/issues/1317#issuecomment-504615833
          revision: _git_operations.HEAD
        };
        yield req;
      }
    } catch (error) {
      this.log.error(`Prepare ${this.type} indexing requests error.`);
      this.log.error(error);
      throw error;
    }
  }

  async getIndexRequestCount() {
    try {
      return await this.gitOps.countRepoFiles(this.repoUri, _git_operations.HEAD);
    } catch (error) {
      if (this.isCancelled()) {
        this.log.debug(`Indexer ${this.type} got cancelled. Skip get index count error.`);
        return 1;
      } else {
        this.log.error(`Get ${this.type} index requests count error.`);
        this.log.error(error);
        throw error;
      }
    }
  }

  async cleanIndex() {
    // Clean up all the symbol documents in the symbol index
    try {
      await this.client.deleteByQuery({
        index: (0, _schema.SymbolIndexName)(this.repoUri),
        body: {
          query: {
            match_all: {}
          }
        }
      });
      this.log.info(`Clean up symbols for ${this.repoUri} done.`);
    } catch (error) {
      this.log.error(`Clean up symbols for ${this.repoUri} error.`);
      this.log.error(error);
    } // Clean up all the reference documents in the reference index


    try {
      await this.client.deleteByQuery({
        index: (0, _schema.ReferenceIndexName)(this.repoUri),
        body: {
          query: {
            match_all: {}
          }
        }
      });
      this.log.info(`Clean up references for ${this.repoUri} done.`);
    } catch (error) {
      this.log.error(`Clean up references for ${this.repoUri} error.`);
      this.log.error(error);
    } // Clean up all the document documents in the document index but keep the repository document.


    try {
      await this.client.deleteByQuery({
        index: (0, _schema.DocumentIndexName)(this.repoUri),
        body: {
          query: {
            bool: {
              must_not: _schema.ALL_RESERVED.map(field => ({
                exists: {
                  field
                }
              }))
            }
          }
        }
      });
      this.log.info(`Clean up documents for ${this.repoUri} done.`);
    } catch (error) {
      this.log.error(`Clean up documents for ${this.repoUri} error.`);
      this.log.error(error);
    }
  }

  async getFileSource(request) {
    const {
      revision,
      filePath
    } = request; // Always read file content from the original bare repo

    const blob = await this.gitOps.fileContent(this.repoUri, filePath, revision);

    if (blob.rawsize() > _file.TEXT_FILE_LIMIT) {
      throw new Error(this.FILE_OVERSIZE_ERROR_MSG);
    }

    return blob.content().toString();
  }

  async execLspIndexing(request) {
    const {
      repoUri,
      revision,
      filePath
    } = request;
    const lspDocUri = (0, _uri_util.toCanonicalUrl)({
      repoUri,
      revision,
      file: filePath,
      schema: 'git:'
    });
    const symbolNames = new Set();
    let symbolsLength = 0;
    let referencesLength = 0;

    try {
      const lang = (0, _detect_language.detectLanguageByFilename)(filePath); // filter file by language

      if (lang && this.lspService.supportLanguage(lang)) {
        const response = await this.lspService.sendRequest('textDocument/full', {
          textDocument: {
            uri: lspDocUri
          },
          reference: this.options.enableGlobalReference
        });

        if (response && response.result && response.result.length > 0 && response.result[0]) {
          const {
            symbols,
            references
          } = response.result[0];

          for (const symbol of symbols) {
            await this.lspBatchIndexHelper.index((0, _schema.SymbolIndexName)(repoUri), symbol);
            symbolNames.add(symbol.symbolInformation.name);
          }

          symbolsLength = symbols.length;

          for (const ref of references) {
            await this.lspBatchIndexHelper.index((0, _schema.ReferenceIndexName)(repoUri), ref);
          }

          referencesLength = references.length;
        } else {
          this.log.debug(`Empty response from lsp server. Skip symbols and references indexing.`);
        }
      } else {
        this.log.debug(`Unsupported language. Skip symbols and references indexing.`);
      }
    } catch (error) {
      if (error instanceof _vscodeJsonrpc.ResponseError && error.code === _lsp_error_codes.LanguageServerNotInstalled) {
        // TODO maybe need to report errors to the index task and warn user later
        this.log.debug(`Index symbols or references error due to language server not installed`);
      } else if (error instanceof _vscodeJsonrpc.ResponseError && error.code === _lsp_error_codes.LanguageServerStartFailed) {
        this.log.debug(`Index symbols or references error due to language server can't be started.`);
      } else {
        this.log.warn(`Index symbols or references error.`);
        this.log.warn(error);
      }
    }

    return {
      symbolNames,
      symbolsLength,
      referencesLength
    };
  }

  async processRequest(request) {
    const stats = new Map().set(_model.IndexStatsKey.Symbol, 0).set(_model.IndexStatsKey.Reference, 0).set(_model.IndexStatsKey.File, 0);
    const {
      repoUri,
      revision,
      filePath
    } = request;
    this.log.debug(`Indexing ${filePath} at revision ${revision} for ${repoUri}`);
    let content = '';

    try {
      content = await this.getFileSource(request);
    } catch (error) {
      if (error.message === this.FILE_OVERSIZE_ERROR_MSG) {
        // Skip this index request if the file is oversized
        this.log.debug(this.FILE_OVERSIZE_ERROR_MSG);
        return stats;
      } else {
        // Rethrow the issue if for other reasons
        throw error;
      }
    }

    const {
      symbolNames,
      symbolsLength,
      referencesLength
    } = await this.execLspIndexing(request);
    stats.set(_model.IndexStatsKey.Symbol, symbolsLength);
    stats.set(_model.IndexStatsKey.Reference, referencesLength);
    const language = await (0, _detect_language.detectLanguage)(filePath, Buffer.from(content));
    const body = {
      repoUri,
      path: filePath,
      content,
      language,
      qnames: Array.from(symbolNames)
    };
    await this.docBatchIndexHelper.index((0, _schema.DocumentIndexName)(repoUri), body);
    stats.set(_model.IndexStatsKey.File, 1);
    return stats;
  }

}

exports.LspIndexer = LspIndexer;