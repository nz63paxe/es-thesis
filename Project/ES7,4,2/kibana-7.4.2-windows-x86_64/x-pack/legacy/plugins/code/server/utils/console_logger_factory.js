"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ConsoleLoggerFactory = void 0;

var _console_logger = require("./console_logger");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class ConsoleLoggerFactory {
  getLogger(tags) {
    return new _console_logger.ConsoleLogger();
  }

}

exports.ConsoleLoggerFactory = ConsoleLoggerFactory;