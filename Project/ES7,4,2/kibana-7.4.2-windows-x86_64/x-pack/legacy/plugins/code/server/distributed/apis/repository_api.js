"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRepositoryHandler = exports.RepositoryServiceDefinition = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const RepositoryServiceDefinition = {
  clone: {
    request: {},
    response: {}
  },
  delete: {
    request: {},
    response: {}
  },
  index: {
    request: {},
    response: {}
  }
};
exports.RepositoryServiceDefinition = RepositoryServiceDefinition;

const getRepositoryHandler = (cloneWorker, deleteWorker, indexWorker) => ({
  async clone(payload) {
    await cloneWorker.enqueueJob(payload, {});
    return {};
  },

  async delete(payload) {
    await deleteWorker.enqueueJob(payload, {});
    return {};
  },

  async index(payload) {
    await indexWorker.enqueueJob(payload, {});
    return {};
  }

});

exports.getRepositoryHandler = getRepositoryHandler;