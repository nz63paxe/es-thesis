"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ServerOptions = void 0;

var _path = require("path");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ServerOptions {
  constructor(options, config) {
    this.options = options;
    this.config = config;

    _defineProperty(this, "workspacePath", (0, _path.resolve)(this.config.get('path.data'), 'code/workspace'));

    _defineProperty(this, "repoPath", (0, _path.resolve)(this.config.get('path.data'), 'code/repos'));

    _defineProperty(this, "credsPath", (0, _path.resolve)(this.config.get('path.data'), 'code/credentials'));

    _defineProperty(this, "jdtWorkspacePath", (0, _path.resolve)(this.config.get('path.data'), 'code/jdt_ws'));

    _defineProperty(this, "jdtConfigPath", (0, _path.resolve)(this.config.get('path.data'), 'code/jdt_config'));

    _defineProperty(this, "goPath", (0, _path.resolve)(this.config.get('path.data'), 'code/gopath'));

    _defineProperty(this, "updateFrequencyMs", this.options.updateFrequencyMs);

    _defineProperty(this, "indexFrequencyMs", this.options.indexFrequencyMs);

    _defineProperty(this, "updateRepoFrequencyMs", this.options.updateRepoFrequencyMs);

    _defineProperty(this, "indexRepoFrequencyMs", this.options.indexRepoFrequencyMs);

    _defineProperty(this, "maxWorkspace", this.options.maxWorkspace);

    _defineProperty(this, "enableGlobalReference", this.options.enableGlobalReference);

    _defineProperty(this, "enableCommitIndexing", this.options.enableCommitIndexing);

    _defineProperty(this, "lsp", this.options.lsp);

    _defineProperty(this, "security", this.options.security);

    _defineProperty(this, "disk", this.options.disk);

    _defineProperty(this, "repoConfigs", this.options.repos.reduce((previous, current) => {
      previous[current.repo] = current;
      return previous;
    }, {}));

    _defineProperty(this, "enabled", this.options.enabled);

    _defineProperty(this, "codeNodeUrl", this.options.codeNodeUrl);

    _defineProperty(this, "clusterEnabled", this.options.clustering.enabled);

    _defineProperty(this, "codeNodes", this.options.clustering.codeNodes);
  }
  /**
   * TODO 'server.uuid' is not guaranteed to be loaded when the object is constructed.
   *
   * See [[manageUuid()]], as it was called asynchronously without actions on the completion.
   */


  get serverUUID() {
    return this.config.get('server.uuid');
  }

  get localAddress() {
    const serverCfg = this.config.get('server');
    return 'http://' + serverCfg.host + ':' + serverCfg.port + serverCfg.basePath;
  }

}

exports.ServerOptions = ServerOptions;