"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LocalHandlerAdapter = void 0;

var _local_endpoint = require("./local_endpoint");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LocalHandlerAdapter {
  constructor() {
    _defineProperty(this, "handlers", new Map());

    _defineProperty(this, "locator", {
      async locate(httpRequest, resource) {
        return Promise.resolve(new _local_endpoint.LocalEndpoint(httpRequest, resource));
      },

      isResourceLocal(resource) {
        return Promise.resolve(true);
      },

      async allocate(httpRequest, resource) {
        return Promise.resolve(new _local_endpoint.LocalEndpoint(httpRequest, resource));
      }

    });
  }

  async start() {}

  async stop() {}

  registerHandler(serviceDefinition, serviceHandler) {
    if (!serviceHandler) {
      throw new Error("Local service handler can't be null!");
    }

    const dispatchedHandler = {}; // eslint-disable-next-line guard-for-in

    for (const method in serviceDefinition) {
      dispatchedHandler[method] = function (endpoint, params) {
        return serviceHandler[method](params, endpoint.toContext());
      };
    }

    this.handlers.set(serviceDefinition, dispatchedHandler);
    return dispatchedHandler;
  }

  getService(serviceDefinition) {
    const serviceHandler = this.handlers.get(serviceDefinition);

    if (serviceHandler) {
      return serviceHandler;
    } else {
      throw new Error(`handler for ${serviceDefinition} not found`);
    }
  }

}

exports.LocalHandlerAdapter = LocalHandlerAdapter;