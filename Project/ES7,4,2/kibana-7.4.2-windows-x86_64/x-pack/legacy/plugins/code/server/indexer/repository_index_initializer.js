"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryIndexInitializer = void 0;

var _model = require("../../model");

var _abstract_indexer = require("./abstract_indexer");

var _schema = require("./schema");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Inherit AbstractIndexer's index creation logics. This is not an actual indexer.
class RepositoryIndexInitializer extends _abstract_indexer.AbstractIndexer {
  constructor(repoUri, revision, client, log) {
    super(repoUri, revision, client, log);
    this.repoUri = repoUri;
    this.revision = revision;
    this.client = client;
    this.log = log;

    _defineProperty(this, "type", _model.IndexerType.REPOSITORY);
  }

  async prepareIndexCreationRequests() {
    const creationReq = {
      index: (0, _schema.RepositoryIndexName)(this.repoUri),
      settings: { ..._schema.RepositoryAnalysisSettings,
        number_of_shards: 1,
        auto_expand_replicas: '0-1'
      },
      schema: _schema.RepositorySchema
    };
    return [creationReq];
  }

  async init() {
    const res = await this.prepareIndex();

    if (!res) {
      this.log.error(`Initialize repository index failed.`);
    }

    return;
  }

}

exports.RepositoryIndexInitializer = RepositoryIndexInitializer;