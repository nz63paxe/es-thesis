"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CtagsLauncher = void 0;

var _getPort = _interopRequireDefault(require("get-port"));

var _request_expander = require("./request_expander");

var _abstract_launcher = require("./abstract_launcher");

var _embed_ctag_server = require("./process/embed_ctag_server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const CTAGS_LANG_DETACH_PORT = 2092;

class CtagsLauncher extends _abstract_launcher.AbstractLauncher {
  constructor(targetHost, options, loggerFactory) {
    super('ctags', targetHost, options, loggerFactory);
    this.targetHost = targetHost;
    this.options = options;
    this.loggerFactory = loggerFactory;

    _defineProperty(this, "isRunning", false);

    _defineProperty(this, "embed", null);
  }

  get running() {
    return this.isRunning;
  }

  createExpander(proxy, builtinWorkspace, maxWorkspace) {
    return new _request_expander.RequestExpander(proxy, builtinWorkspace, maxWorkspace, this.options, {}, this.log);
  }

  startConnect(proxy) {
    proxy.startServerConnection();

    if (this.embed) {
      this.embed.start().catch(err => this.log.error(err));
    }
  }

  async getPort() {
    if (!this.options.lsp.detach) {
      return await (0, _getPort.default)();
    }

    return CTAGS_LANG_DETACH_PORT;
  }

  async spawnProcess(installationPath, port, log) {
    if (!this.embed) {
      this.embed = new _embed_ctag_server.EmbedCtagServer(port, log);
    }

    return this.embed;
  }

}

exports.CtagsLauncher = CtagsLauncher;