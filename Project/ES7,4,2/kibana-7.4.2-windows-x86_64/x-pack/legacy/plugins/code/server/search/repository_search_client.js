"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositorySearchClient = void 0;

var _schema = require("../indexer/schema");

var _abstract_search_client = require("./abstract_search_client");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class RepositorySearchClient extends _abstract_search_client.AbstractSearchClient {
  constructor(client, log) {
    super(client, log);
    this.client = client;
    this.log = log;
  }

  async search(req) {
    const resultsPerPage = this.getResultsPerPage(req);
    const from = (req.page - 1) * resultsPerPage;
    const size = resultsPerPage;
    const index = req.repoScope ? (0, _schema.RepositorySearchIndexWithScope)(req.repoScope) : `${_schema.RepositoryIndexNamePrefix}*`;
    const queryStr = req.query.toLowerCase();
    const rawRes = await this.client.search({
      index,
      body: {
        from,
        size,
        query: {
          bool: {
            should: [{
              simple_query_string: {
                query: queryStr,
                fields: [`${_schema.RepositoryReservedField}.name^1.0`, `${_schema.RepositoryReservedField}.org^1.0`],
                default_operator: 'or',
                lenient: false,
                analyze_wildcard: false,
                boost: 1.0
              }
            }, // This prefix query is mostly for typeahead search.
            {
              prefix: {
                [`${_schema.RepositoryReservedField}.name`]: {
                  value: queryStr,
                  boost: 100.0
                }
              }
            }],
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        }
      }
    });
    const hits = rawRes.hits.hits;
    const repos = hits.filter(hit => hit._source[_schema.RepositoryReservedField]).map(hit => {
      const repo = hit._source[_schema.RepositoryReservedField];
      return repo;
    });
    const total = rawRes.hits.total.value;
    return {
      repositories: repos,
      took: rawRes.took,
      total,
      from,
      page: req.page,
      totalPage: Math.ceil(total / resultsPerPage)
    };
  }

}

exports.RepositorySearchClient = RepositorySearchClient;