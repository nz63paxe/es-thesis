"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeServices = void 0;

var _service_handler_adapter = require("./service_handler_adapter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CodeServices {
  constructor(adapter) {
    this.adapter = adapter;
  }

  registerHandler(serviceDefinition, serviceHandler, options = _service_handler_adapter.DEFAULT_SERVICE_OPTION) {
    this.adapter.registerHandler(serviceDefinition, serviceHandler, options);
  }

  async start() {
    await this.adapter.start();
  }

  async stop() {
    await this.adapter.stop();
  }

  allocate(req, resource) {
    return this.adapter.locator.allocate(req, resource);
  }

  locate(req, resource) {
    return this.adapter.locator.locate(req, resource);
  }

  isResourceLocal(resource) {
    return this.adapter.locator.isResourceLocal(resource);
  }

  serviceFor(serviceDefinition) {
    return this.adapter.getService(serviceDefinition);
  }

}

exports.CodeServices = CodeServices;