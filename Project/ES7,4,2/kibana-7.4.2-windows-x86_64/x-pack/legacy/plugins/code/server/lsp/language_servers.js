"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enabledLanguageServers = enabledLanguageServers;
exports.LanguageServersDeveloping = exports.LanguageServers = exports.CTAGS = exports.GO = exports.JAVA = exports.TYPESCRIPT = void 0;

var _installation = require("../../common/installation");

var _language_server = require("../../common/language_server");

var _ctags_launcher = require("./ctags_launcher");

var _go_launcher = require("./go_launcher");

var _java_launcher = require("./java_launcher");

var _ts_launcher = require("./ts_launcher");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const TYPESCRIPT = {
  name: 'TypeScript',
  builtinWorkspaceFolders: false,
  languages: ['typescript', 'javascript'],
  launcher: _ts_launcher.TypescriptServerLauncher,
  installationType: _installation.InstallationType.Embed,
  embedPath: require.resolve('@elastic/javascript-typescript-langserver/lib/language-server.js'),
  priority: 2
};
exports.TYPESCRIPT = TYPESCRIPT;
const JAVA = {
  name: 'Java',
  builtinWorkspaceFolders: true,
  languages: ['java'],
  launcher: _java_launcher.JavaLauncher,
  installationType: _installation.InstallationType.Plugin,
  installationPluginName: 'java-langserver',
  installationFolderName: 'jdt',
  priority: 2,
  downloadUrl: (version, devMode) => devMode ? `https://snapshots.elastic.co/downloads/kibana-plugins/java-langserver/java-langserver-${version}-SNAPSHOT-$OS.zip` : `https://artifacts.elastic.co/downloads/kibana-plugins/java-langserver/java-langserver-${version}-$OS.zip`
};
exports.JAVA = JAVA;
const GO = {
  name: 'Go',
  builtinWorkspaceFolders: true,
  languages: ['go'],
  launcher: _go_launcher.GoServerLauncher,
  installationType: _installation.InstallationType.Plugin,
  installationPluginName: 'go-langserver',
  priority: 2,
  installationFolderName: 'golsp',
  downloadUrl: (version, devMode) => devMode ? `https://snapshots.elastic.co/downloads/kibana-plugins/go-langserver/go-langserver-${version}-SNAPSHOT-$OS.zip` : `https://artifacts.elastic.co/downloads/kibana-plugins/go-langserver/go-langserver-${version}-$OS.zip`
};
exports.GO = GO;
const CTAGS = {
  name: 'Ctags',
  builtinWorkspaceFolders: true,
  languages: _language_server.CTAGS_SUPPORT_LANGS,
  launcher: _ctags_launcher.CtagsLauncher,
  installationType: _installation.InstallationType.Embed,
  embedPath: require.resolve('@elastic/ctags-langserver/lib/cli.js'),
  priority: 1
};
exports.CTAGS = CTAGS;
const LanguageServers = [TYPESCRIPT, JAVA, GO];
exports.LanguageServers = LanguageServers;
const LanguageServersDeveloping = [CTAGS];
exports.LanguageServersDeveloping = LanguageServersDeveloping;

function enabledLanguageServers(server) {
  const devMode = server.config().get('env.dev');

  function isEnabled(lang, defaultEnabled) {
    const name = lang.name;
    const enabled = server.config().get(`xpack.code.lsp.${name}.enabled`);
    return enabled === undefined ? defaultEnabled : enabled;
  }

  const results = LanguageServers.filter(lang => isEnabled(lang, true));

  if (devMode) {
    return results.concat(LanguageServersDeveloping.filter(lang => isEnabled(lang, devMode)));
  }

  return results;
}