"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchCodeUsageMetrics = fetchCodeUsageMetrics;
exports.initCodeUsageCollector = initCodeUsageCollector;

var _constants = require("../common/constants");

var _language_server = require("../common/language_server");

var _usage_telemetry_metrics = require("../model/usage_telemetry_metrics");

var _search = require("./search");

var _language_servers = require("./lsp/language_servers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function fetchCodeUsageMetrics(client, lspService) {
  const repositoryObjectClient = new _search.RepositoryObjectClient(client);
  const allRepos = await repositoryObjectClient.getAllRepositories();

  const langServerEnabled = async name => {
    const status = await lspService.languageServerStatus(name);
    return status !== _language_server.LanguageServerStatus.NOT_INSTALLED ? 1 : 0;
  };

  const langServersEnabled = await Promise.all(_language_servers.LanguageServers.map(async langServer => {
    return {
      key: langServer.name,
      enabled: await langServerEnabled(langServer.name)
    };
  }));
  return {
    [_usage_telemetry_metrics.CodeUsageMetrics.ENABLED]: 1,
    [_usage_telemetry_metrics.CodeUsageMetrics.REPOSITORIES]: allRepos.length,
    [_usage_telemetry_metrics.CodeUsageMetrics.LANGUAGE_SERVERS]: langServersEnabled
  };
}

function initCodeUsageCollector(server, client, lspService) {
  const codeUsageCollector = server.usage.collectorSet.makeUsageCollector({
    type: _constants.APP_USAGE_TYPE,
    isReady: () => true,
    fetch: async () => fetchCodeUsageMetrics(client, lspService)
  });
  server.usage.collectorSet.register(codeUsageCollector);
}