"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SymbolSearchIndexWithScope = exports.SymbolIndexName = exports.SymbolIndexNamePrefix = exports.SymbolAnalysisSettings = exports.SymbolSchema = void 0;

var _repository_utils = require("../../../common/repository_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const SymbolSchema = {
  qname: {
    type: 'text',
    analyzer: 'qname_path_hierarchy_case_sensitive_analyzer',
    fields: {
      // Create a 'lowercased' field to match query in lowercased mode.
      lowercased: {
        type: 'text',
        analyzer: 'qname_path_hierarchy_case_insensitive_analyzer'
      }
    }
  },
  symbolInformation: {
    properties: {
      name: {
        type: 'text',
        analyzer: 'qname_path_hierarchy_case_sensitive_analyzer',
        fields: {
          // Create a 'lowercased' field to match query in lowercased mode.
          lowercased: {
            type: 'text',
            analyzer: 'qname_path_hierarchy_case_insensitive_analyzer'
          }
        }
      },
      kind: {
        type: 'integer',
        index: false
      },
      location: {
        properties: {
          uri: {
            // Indexed now for symbols batch deleting in incremental indexing
            type: 'keyword'
          }
        }
      }
    }
  }
};
exports.SymbolSchema = SymbolSchema;
const SymbolAnalysisSettings = {
  analysis: {
    analyzer: {
      qname_path_hierarchy_case_sensitive_analyzer: {
        type: 'custom',
        tokenizer: 'qname_path_hierarchy_tokenizer'
      },
      qname_path_hierarchy_case_insensitive_analyzer: {
        type: 'custom',
        tokenizer: 'qname_path_hierarchy_tokenizer',
        filter: ['lowercase']
      }
    },
    tokenizer: {
      qname_path_hierarchy_tokenizer: {
        type: 'path_hierarchy',
        delimiter: '.',
        reverse: 'true'
      }
    }
  }
};
exports.SymbolAnalysisSettings = SymbolAnalysisSettings;
const SymbolIndexNamePrefix = `.code-symbol`;
exports.SymbolIndexNamePrefix = SymbolIndexNamePrefix;

const SymbolIndexName = repoUri => {
  return `${SymbolIndexNamePrefix}-${_repository_utils.RepositoryUtils.normalizeRepoUriToIndexName(repoUri)}`;
};

exports.SymbolIndexName = SymbolIndexName;

const SymbolSearchIndexWithScope = repoScope => {
  return repoScope.map(repoUri => `${SymbolIndexName(repoUri)}*`).join(',');
};

exports.SymbolSearchIndexWithScope = SymbolSearchIndexWithScope;