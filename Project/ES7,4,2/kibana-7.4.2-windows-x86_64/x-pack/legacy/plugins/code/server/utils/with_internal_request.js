"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithInternalRequest = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class WithInternalRequest {
  constructor(server) {
    _defineProperty(this, "callCluster", void 0);

    const cluster = server.plugins.elasticsearch.getCluster('admin');
    this.callCluster = cluster.callWithInternalUser;
  }

}

exports.WithInternalRequest = WithInternalRequest;