"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tryMigrateIndices = exports.IndexMigrator = void 0;

var _ = require(".");

var _search = require("../search");

var _es_exception = require("./es_exception");

var _version = _interopRequireDefault(require("./schema/version.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexMigrator {
  constructor(client, log) {
    this.client = client;
    this.log = log;

    _defineProperty(this, "version", void 0);

    this.version = Number(_version.default.codeIndexVersion);
  }

  async migrateIndex(oldIndexName, request) {
    const body = {
      settings: request.settings,
      mappings: {
        // Apply the index version in the reserved _meta field of the index.
        _meta: {
          version: this.version
        },
        dynamic_templates: [{
          fieldDefaultNotAnalyzed: {
            match: '*',
            mapping: {
              index: false,
              norms: false
            }
          }
        }],
        properties: request.schema
      }
    };
    const newIndexName = `${request.index}-${this.version}`;

    try {
      try {
        // Create the new index first with the version as the index suffix number.
        await this.client.indices.create({
          index: newIndexName,
          body
        });
      } catch (error) {
        if (error.body && error.body.error && error.body.error.type === _es_exception.EsException.RESOURCE_ALREADY_EXISTS_EXCEPTION) {
          this.log.debug(`The target verion index already exists. Skip index migration`);
          return;
        } else {
          this.log.error(`Create new index ${newIndexName} for index migration error.`);
          this.log.error(error);
          throw error;
        }
      }

      try {
        // Issue the reindex request for import the data from the old index.
        await this.client.reindex({
          body: {
            source: {
              index: oldIndexName
            },
            dest: {
              index: newIndexName
            }
          }
        });
      } catch (error) {
        this.log.error(`Migrate data from ${oldIndexName} to ${newIndexName} for index migration error.`);
        this.log.error(error);
        throw error;
      }

      try {
        // Update the alias
        await this.client.indices.updateAliases({
          body: {
            actions: [{
              remove: {
                index: oldIndexName,
                alias: request.index
              }
            }, {
              add: {
                index: newIndexName,
                alias: request.index
              }
            }]
          }
        });
      } catch (error) {
        this.log.error(`Update the index alias for ${newIndexName} error.`);
        this.log.error(error);
        throw error;
      }

      try {
        // Delete the old index
        await this.client.indices.delete({
          index: oldIndexName
        });
      } catch (error) {
        this.log.error(`Clean up the old index ${oldIndexName} error.`);
        this.log.error(error); // This won't affect serving, so do not throw the error anymore.
      }
    } catch (error) {
      this.log.error(`Index upgrade/migration to version ${this.version} failed.`);
      this.log.error(error);
    }
  }

}

exports.IndexMigrator = IndexMigrator;

const tryMigrateIndices = async (client, log) => {
  log.info('Check the versions of Code indices...');
  const repoObjectClient = new _search.RepositoryObjectClient(client);
  const repos = await repoObjectClient.getAllRepositories();
  const migrationPromises = [];

  for (const repo of repos) {
    const docIndexVersionController = new _.IndexVersionController(client, log);
    const docCreationReq = (0, _.getDocumentIndexCreationRequest)(repo.uri);
    migrationPromises.push(docIndexVersionController.tryUpgrade(docCreationReq));
    const symbolIndexVersionController = new _.IndexVersionController(client, log);
    const symbolCreationReq = (0, _.getSymbolIndexCreationRequest)(repo.uri);
    migrationPromises.push(symbolIndexVersionController.tryUpgrade(symbolCreationReq));
    const refIndexVersionController = new _.IndexVersionController(client, log);
    const refCreationReq = (0, _.getReferenceIndexCreationRequest)(repo.uri);
    migrationPromises.push(refIndexVersionController.tryUpgrade(refCreationReq));
  }

  return Promise.all(migrationPromises);
};

exports.tryMigrateIndices = tryMigrateIndices;