"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CancellationSerivce = exports.CancellationReason = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let CancellationReason;
exports.CancellationReason = CancellationReason;

(function (CancellationReason) {
  CancellationReason["REPOSITORY_DELETE"] = "Cancel job because of deleting the entire repository";
  CancellationReason["LOW_DISK_SPACE"] = "Cancel job because of low available disk space";
  CancellationReason["NEW_JOB_OVERRIDEN"] = "Cancel job because of a new job of the same type has been registered";
})(CancellationReason || (exports.CancellationReason = CancellationReason = {}));

class CancellationSerivce {
  constructor() {
    _defineProperty(this, "cloneCancellationMap", void 0);

    _defineProperty(this, "updateCancellationMap", void 0);

    _defineProperty(this, "indexCancellationMap", void 0);

    this.cloneCancellationMap = new Map();
    this.updateCancellationMap = new Map();
    this.indexCancellationMap = new Map();
  }

  async cancelCloneJob(repoUri, reason) {
    await this.cancelJob(this.cloneCancellationMap, repoUri, reason);
  }

  async cancelUpdateJob(repoUri, reason) {
    await this.cancelJob(this.updateCancellationMap, repoUri, reason);
  }

  async cancelIndexJob(repoUri, reason) {
    await this.cancelJob(this.indexCancellationMap, repoUri, reason);
  }

  async registerCancelableCloneJob(repoUri, token, jobPromise) {
    await this.registerCancelableJob(this.cloneCancellationMap, repoUri, token, jobPromise);
  }

  async registerCancelableUpdateJob(repoUri, token, jobPromise) {
    await this.registerCancelableJob(this.updateCancellationMap, repoUri, token, jobPromise);
  }

  async registerCancelableIndexJob(repoUri, token, jobPromise) {
    await this.registerCancelableJob(this.indexCancellationMap, repoUri, token, jobPromise);
  }

  async registerCancelableJob(jobMap, repoUri, token, jobPromise) {
    // Try to cancel the job first.
    await this.cancelJob(jobMap, repoUri, CancellationReason.NEW_JOB_OVERRIDEN);
    jobMap.set(repoUri, {
      token,
      jobPromise
    }); // remove the record from the cancellation service when the promise is fulfilled or rejected.

    jobPromise.finally(() => {
      jobMap.delete(repoUri);
    });
  }

  async cancelJob(jobMap, repoUri, reason) {
    const payload = jobMap.get(repoUri);

    if (payload) {
      const {
        token,
        jobPromise
      } = payload; // 1. Use the cancellation token to pass cancel message to job

      token.cancel(reason); // 2. waiting on the actual job promise to be resolved

      try {
        await jobPromise;
      } catch (e) {// the exception from the job also indicates the job is finished, and it should be the duty of the worker for
        // the job to handle it, so it's safe to just ignore the exception here
      }
    }
  }

}

exports.CancellationSerivce = CancellationSerivce;