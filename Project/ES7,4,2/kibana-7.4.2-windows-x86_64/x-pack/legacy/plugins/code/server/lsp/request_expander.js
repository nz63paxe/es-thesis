"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RequestExpander = exports.WorkspaceUnloadedError = exports.WorkspaceStatus = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _url = require("url");

var _messages = require("vscode-jsonrpc/lib/messages");

var _lsp_error_codes = require("../../common/lsp_error_codes");

var _cancelable = require("../utils/cancelable");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

let WorkspaceStatus;
exports.WorkspaceStatus = WorkspaceStatus;

(function (WorkspaceStatus) {
  WorkspaceStatus["Uninitialized"] = "Uninitialized";
  WorkspaceStatus["Initializing"] = "Initializing";
  WorkspaceStatus["Initialized"] = "Initialized";
})(WorkspaceStatus || (exports.WorkspaceStatus = WorkspaceStatus = {}));

const WorkspaceUnloadedError = new _messages.ResponseError(_lsp_error_codes.RequestCancelled, 'Workspace unloaded');
exports.WorkspaceUnloadedError = WorkspaceUnloadedError;

class RequestExpander {
  // a map for workspacePath -> Workspace
  constructor(proxy, builtinWorkspace, maxWorkspace, serverOptions, initialOptions, log) {
    this.builtinWorkspace = builtinWorkspace;
    this.maxWorkspace = maxWorkspace;
    this.serverOptions = serverOptions;
    this.initialOptions = initialOptions;
    this.log = log;

    _defineProperty(this, "lastAccess", 0);

    _defineProperty(this, "proxy", void 0);

    _defineProperty(this, "jobQueue", []);

    _defineProperty(this, "workspaces", new Map());

    _defineProperty(this, "workspaceRoot", void 0);

    _defineProperty(this, "running", false);

    _defineProperty(this, "exited", false);

    this.proxy = proxy;
    this.handle = this.handle.bind(this);
    proxy.onDisconnected(() => {
      this.workspaces.clear();
    });
    this.workspaceRoot = _fs.default.realpathSync(this.serverOptions.workspacePath);
  }

  handleRequest(request) {
    this.lastAccess = Date.now();
    return new Promise((resolve, reject) => {
      if (this.exited) {
        reject(new Error('proxy is exited.'));
        return;
      }

      this.jobQueue.push({
        request,
        resolve,
        reject,
        startTime: Date.now()
      });
      this.log.debug(`queued  a ${request.method} job for workspace ${request.workspacePath}`);

      if (!this.running) {
        this.running = true;
        this.handleNext();
      }
    });
  }

  async exit() {
    this.exited = true;
    this.running = false;
    this.log.debug(`exiting proxy`);
    return this.proxy.exit();
  }

  async unloadWorkspace(workspacePath) {
    this.log.debug('unload workspace ' + workspacePath);

    if (this.hasWorkspacePath(workspacePath)) {
      const ws = this.getWorkspace(workspacePath);

      if (ws.initPromise) {
        ws.initPromise.cancel(WorkspaceUnloadedError);
      }

      if (this.builtinWorkspace) {
        const params = {
          event: {
            removed: [{
              name: workspacePath,
              uri: (0, _url.pathToFileURL)(workspacePath).href
            }],
            added: []
          }
        };
        await this.proxy.handleRequest({
          method: 'workspace/didChangeWorkspaceFolders',
          params,
          isNotification: true
        });
      } else {
        await this.exit();
      }
    }

    this.removeWorkspace(workspacePath);
    const newJobQueue = [];
    this.jobQueue.forEach(job => {
      if (job.request.workspacePath === workspacePath) {
        job.reject(WorkspaceUnloadedError);
        this.log.debug(`canceled a ${job.request.method} job because of unload workspace`);
      } else {
        newJobQueue.push(job);
      }
    });
    this.jobQueue = newJobQueue;
  }

  async initialize(workspacePath) {
    this.updateWorkspace(workspacePath);
    const ws = this.getWorkspace(workspacePath);
    ws.status = WorkspaceStatus.Initializing;

    try {
      if (this.builtinWorkspace) {
        if (this.proxy.initialized) {
          await this.changeWorkspaceFolders(workspacePath, this.maxWorkspace);
        } else {
          // this is the first workspace, init the lsp server first
          await this.sendInitRequest(workspacePath);
        }

        ws.status = WorkspaceStatus.Initialized;
        delete ws.initPromise;
      } else {
        for (const w of this.workspaces.values()) {
          if (w.status === WorkspaceStatus.Initialized) {
            await this.proxy.shutdown();
            this.workspaces.clear();
            break;
          }
        }

        const response = await this.sendInitRequest(workspacePath);
        ws.status = WorkspaceStatus.Initialized;
        return response;
      }
    } catch (e) {
      ws.status = WorkspaceStatus.Uninitialized;
      throw e;
    }
  }

  async sendInitRequest(workspacePath) {
    return await this.proxy.initialize({}, [{
      name: workspacePath,
      uri: (0, _url.pathToFileURL)(workspacePath).href
    }], this.initialOptions);
  }

  handle() {
    const job = this.jobQueue.shift();

    if (job && !this.exited) {
      this.log.debug('dequeue a job');
      const {
        request,
        resolve,
        reject
      } = job;
      this.expand(request, job.startTime).then(value => {
        try {
          resolve(value);
        } finally {
          this.handleNext();
        }
      }, err => {
        try {
          this.log.error(err);
          reject(err);
        } finally {
          this.handleNext();
        }
      });
    } else {
      this.running = false;
    }
  }

  handleNext() {
    setTimeout(this.handle, 0);
  }

  async expand(request, startTime) {
    if (request.workspacePath) {
      const ws = this.getWorkspace(request.workspacePath);

      if (ws.status === WorkspaceStatus.Uninitialized) {
        ws.initPromise = _cancelable.Cancelable.fromPromise(this.initialize(request.workspacePath));
      } // Uninitialized or initializing


      if (ws.status === WorkspaceStatus.Initializing) {
        await ws.initPromise.promise;
      }
    }

    return await this.proxy.handleRequest(request);
  }
  /**
   * use DidChangeWorkspaceFolders notification add a new workspace folder
   * replace the oldest one if count > maxWorkspace
   * builtinWorkspace = false is equal to maxWorkspace =1
   * @param workspacePath
   * @param maxWorkspace
   */


  async changeWorkspaceFolders(workspacePath, maxWorkspace) {
    const params = {
      event: {
        added: [{
          name: workspacePath,
          uri: (0, _url.pathToFileURL)(workspacePath).href
        }],
        removed: []
      }
    };
    this.updateWorkspace(workspacePath);

    if (this.workspaces.size > this.maxWorkspace) {
      let oldestWorkspace;
      let oldestAccess = Number.MAX_VALUE;

      for (const [workspace, ws] of this.workspaces) {
        if (ws.lastAccess < oldestAccess) {
          oldestAccess = ws.lastAccess;
          oldestWorkspace = _path.default.join(this.serverOptions.workspacePath, workspace);
        }
      }

      if (oldestWorkspace) {
        params.event.removed.push({
          name: oldestWorkspace,
          uri: (0, _url.pathToFileURL)(oldestWorkspace).href
        });
        this.removeWorkspace(oldestWorkspace);
      }
    } // adding a workspace folder may also need initialize


    await this.proxy.handleRequest({
      method: 'workspace/didChangeWorkspaceFolders',
      params,
      isNotification: true
    });
  }

  removeWorkspace(workspacePath) {
    this.workspaces.delete(this.relativePath(workspacePath));
  }

  updateWorkspace(workspacePath) {
    this.getWorkspace(workspacePath).lastAccess = Date.now();
  }

  hasWorkspacePath(workspacePath) {
    return this.workspaces.has(this.relativePath(workspacePath));
  }
  /**
   * use a relative path to prevent bugs due to symbolic path
   * @param workspacePath
   */


  relativePath(workspacePath) {
    const realPath = _fs.default.realpathSync(workspacePath);

    return _path.default.relative(this.workspaceRoot, realPath);
  }

  getWorkspace(workspacePath) {
    const p = this.relativePath(workspacePath);
    let ws = this.workspaces.get(p);

    if (!ws) {
      ws = {
        status: WorkspaceStatus.Uninitialized,
        lastAccess: Date.now()
      };
      this.workspaces.set(p, ws);
    }

    return ws;
  }

  initializeState(workspaceDir) {
    if (this.hasWorkspacePath(workspaceDir)) {
      return this.getWorkspace(workspaceDir).status;
    }

    return WorkspaceStatus.Uninitialized;
  }

}

exports.RequestExpander = RequestExpander;