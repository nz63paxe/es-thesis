"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmbedCtagServer = void 0;

var lsp = _interopRequireWildcard(require("vscode-languageserver"));

var _lspConnection = require("@elastic/ctags-langserver/lib/lsp-connection");

var _embed_program = require("./embed_program");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class EmbedCtagServer extends _embed_program.EmbedProgram {
  constructor(port, log) {
    super(log);
    this.port = port;

    _defineProperty(this, "connection", null);
  }

  async stop() {
    if (this.connection) {
      this.connection.dispose();
    }
  }

  async start() {
    this.connection = (0, _lspConnection.createLspConnection)({
      ctagsPath: '',
      showMessageLevel: lsp.MessageType.Warning,
      socketPort: this.port
    });
    this.connection.listen();
  }

}

exports.EmbedCtagServer = EmbedCtagServer;