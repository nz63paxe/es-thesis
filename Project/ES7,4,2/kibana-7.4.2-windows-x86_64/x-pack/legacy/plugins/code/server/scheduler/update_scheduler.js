"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UpdateScheduler = void 0;

var _model = require("../../model");

var _search = require("../search");

var _abstract_scheduler = require("./abstract_scheduler");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class UpdateScheduler extends _abstract_scheduler.AbstractScheduler {
  constructor(updateWorker, serverOptions, client, log, onScheduleFinished) {
    super(client, serverOptions.updateFrequencyMs, onScheduleFinished);
    this.updateWorker = updateWorker;
    this.serverOptions = serverOptions;
    this.client = client;
    this.log = log;
    this.onScheduleFinished = onScheduleFinished;

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  getRepoSchedulingFrequencyMs() {
    return this.serverOptions.updateRepoFrequencyMs;
  } // TODO: Currently the schduling algorithm the most naive one, which go through
  // all repositories and execute update. Later we can repeat the one we used
  // before for task throttling.


  async executeSchedulingJob(repo) {
    this.log.debug(`Try to schedule update repo request for ${repo.uri}`);

    try {
      // This repository is too soon to execute the next update job.
      if (repo.nextUpdateTimestamp && new Date() < new Date(repo.nextUpdateTimestamp)) {
        this.log.debug(`Repo ${repo.uri} is too soon to execute the next update job.`);
        return;
      }

      this.log.debug(`Start to schedule update repo request for ${repo.uri}`);
      let inDelete = false;

      try {
        await this.objectClient.getRepositoryDeleteStatus(repo.uri);
        inDelete = true;
      } catch (error) {
        inDelete = false;
      }

      const cloneStatus = await this.objectClient.getRepositoryGitStatus(repo.uri); // Schedule update job only when the repo has been fully cloned already and not in delete

      if (!inDelete && cloneStatus.cloneProgress && cloneStatus.cloneProgress.isCloned && cloneStatus.progress === _model.WorkerReservedProgress.COMPLETED) {
        const payload = repo; // Update the next repo update timestamp.

        const nextRepoUpdateTimestamp = this.repoNextSchedulingTime();
        await this.objectClient.updateRepository(repo.uri, {
          nextUpdateTimestamp: nextRepoUpdateTimestamp
        });
        await this.updateWorker.enqueueJob(payload, {});
      } else {
        this.log.debug(`Repo ${repo.uri} has not been fully cloned yet or in update/delete. Skip update.`);
      }
    } catch (error) {
      this.log.error(`Schedule update for ${repo.uri} error.`);
      this.log.error(error);
    }
  }

}

exports.UpdateScheduler = UpdateScheduler;