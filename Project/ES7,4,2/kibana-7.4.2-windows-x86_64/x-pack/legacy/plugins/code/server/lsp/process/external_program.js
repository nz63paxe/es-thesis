"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExternalProgram = void 0;

var _fs = _interopRequireDefault(require("fs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const OOM_SCORE_ADJ = 667;
const OOM_ADJ = 10;

class ExternalProgram {
  constructor(child, options, log) {
    this.child = child;
    this.options = options;
    this.log = log;

    _defineProperty(this, "pid", void 0);

    this.pid = child.pid;

    if (this.options.lsp.oomScoreAdj && process.platform === 'linux') {
      this.adjustOom(this.pid);
    }

    this.log.debug('spawned a child process ' + this.pid);
  }

  kill(force = false) {
    if (force) {
      this.child.kill('SIGKILL');
      this.log.info('force killed process ' + this.pid);
    } else {
      this.child.kill();
      this.log.info('killed process ' + this.pid);
    }
  }

  killed() {
    return this.child.killed;
  }

  onExit(callback) {
    this.child.on('exit', callback);
  }

  adjustOom(pid) {
    try {
      _fs.default.writeFileSync(`/proc/${pid}/oom_score_adj`, `${OOM_SCORE_ADJ}\n`);

      this.log.debug(`wrote oom_score_adj of process ${pid} to ${OOM_SCORE_ADJ}`);
    } catch (e) {
      this.log.warn(e);

      try {
        _fs.default.writeFileSync(`/proc/${pid}/oom_adj`, `${OOM_ADJ}\n`);

        this.log.debug(`wrote oom_adj of process ${pid} to ${OOM_ADJ}`);
      } catch (err) {
        this.log.warn('write oom_score_adj and oom_adj file both failed, reduce priority not working');
        this.log.warn(err);
      }
    }
  }

}

exports.ExternalProgram = ExternalProgram;