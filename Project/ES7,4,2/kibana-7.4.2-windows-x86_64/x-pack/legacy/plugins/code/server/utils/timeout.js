"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.promiseTimeout = promiseTimeout;

var _boom = _interopRequireDefault(require("boom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function promiseTimeout(ms, promise) {
  const boom = _boom.default.gatewayTimeout('Timed out in ' + ms + 'ms.'); // @ts-ignore


  boom.isTimeout = true;

  if (ms > 0) {
    // Create a promise that rejects in <ms> milliseconds
    const timeout = new Promise((resolve, reject) => {
      const id = setTimeout(() => {
        clearTimeout(id);
        reject(boom);
      }, ms);
    }); // Returns a race between our timeout and the passed in promise

    return Promise.race([promise, timeout]);
  } else {
    return Promise.reject(boom);
  }
}