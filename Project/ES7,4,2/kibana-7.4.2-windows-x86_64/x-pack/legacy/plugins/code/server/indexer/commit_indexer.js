"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitIndexer = void 0;

var _model = require("../../model");

var _git_operations = require("../git_operations");

var _abstract_indexer = require("./abstract_indexer");

var _batch_index_helper = require("./batch_index_helper");

var _index_creation_request = require("./index_creation_request");

var _schema = require("./schema");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// TODO: implement an incremental commit indexer.
class CommitIndexer extends _abstract_indexer.AbstractIndexer {
  // Batch index helper for commits
  constructor(repoUri, revision, gitOps, client, log) {
    super(repoUri, revision, client, log);
    this.repoUri = repoUri;
    this.revision = revision;
    this.gitOps = gitOps;
    this.client = client;
    this.log = log;

    _defineProperty(this, "type", _model.IndexerType.COMMIT);

    _defineProperty(this, "commitBatchIndexHelper", void 0);

    _defineProperty(this, "COMMIT_BATCH_INDEX_SIZE", 1000);

    _defineProperty(this, "commits", null);

    this.commitBatchIndexHelper = new _batch_index_helper.BatchIndexHelper(client, log, this.COMMIT_BATCH_INDEX_SIZE);
  }

  async start(progressReporter, checkpointReq) {
    try {
      return await super.start(progressReporter, checkpointReq);
    } finally {
      if (!this.isCancelled()) {
        // Flush all the index request still in the cache for bulk index.
        this.commitBatchIndexHelper.flush();
      }
    }
  }

  cancel() {
    this.commitBatchIndexHelper.cancel();
    super.cancel();
  } // If the current checkpoint is valid


  validateCheckpoint(checkpointReq) {
    // Up to change when integrate with the actual git api.
    return checkpointReq !== undefined && checkpointReq.revision === this.revision;
  } // If it's necessary to refresh (create and reset) all the related indices


  needRefreshIndices(checkpointReq) {
    // If it's not resumed from a checkpoint, then try to refresh all the indices.
    return !this.validateCheckpoint(checkpointReq);
  }

  ifCheckpointMet(req, checkpointReq) {
    // Assume for the same revision, the order of the files we iterate the repository is definite
    // everytime. This is up to change when integrate with the actual git api.
    return req.commit.id === checkpointReq.commit.id;
  }

  async prepareIndexCreationRequests() {
    return [(0, _index_creation_request.getCommitIndexCreationRequest)(this.repoUri)];
  }

  async *getIndexRequestIterator() {
    if (!this.commits) {
      return;
    }

    try {
      for await (const commit of this.commits) {
        const req = {
          repoUri: this.repoUri,
          revision: this.revision,
          commit
        };
        yield req;
      }
    } catch (error) {
      this.log.error(`Prepare commit indexing requests error.`);
      this.log.error(error);
      throw error;
    }
  }

  async getIndexRequestCount() {
    try {
      this.commits = await this.gitOps.iterateCommits(this.repoUri, _git_operations.HEAD);
      return this.commits.length;
    } catch (error) {
      if (this.isCancelled()) {
        this.log.debug(`Indexer ${this.type} got cancelled. Skip get index count error.`);
        return 1;
      } else {
        this.log.error(`Get lsp index requests count error.`);
        this.log.error(error);
        throw error;
      }
    }
  }

  async cleanIndex() {
    // Clean up all the commits in the commit index
    try {
      await this.client.deleteByQuery({
        index: (0, _schema.CommitIndexName)(this.repoUri),
        body: {
          query: {
            match_all: {}
          }
        }
      });
      this.log.info(`Clean up commits for ${this.repoUri} done.`);
    } catch (error) {
      this.log.error(`Clean up commits for ${this.repoUri} error.`);
      this.log.error(error);
    }
  }

  async processRequest(request) {
    const stats = new Map().set(_model.IndexStatsKey.Commit, 0);
    const {
      repoUri,
      commit
    } = request;
    await this.commitBatchIndexHelper.index((0, _schema.CommitIndexName)(repoUri), commit);
    stats.set(_model.IndexStatsKey.Commit, 1);
    return stats;
  }

}

exports.CommitIndexer = CommitIndexer;