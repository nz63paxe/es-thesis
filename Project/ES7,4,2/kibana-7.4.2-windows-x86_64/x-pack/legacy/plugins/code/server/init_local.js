"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initLocalService = initLocalService;

var _git_operations = require("./git_operations");

var _apis = require("./distributed/apis");

var _install_manager = require("./lsp/install_manager");

var _lsp_service = require("./lsp/lsp_service");

var _server_logger_factory = require("./utils/server_logger_factory");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initLocalService(server, log, serverOptions, codeServices, esClient, repoConfigController) {
  // Initialize git operations
  const gitOps = new _git_operations.GitOperations(serverOptions.repoPath);
  codeServices.registerHandler(_apis.GitServiceDefinition, (0, _apis.getGitServiceHandler)(gitOps), _apis.GitServiceDefinitionOption);
  const installManager = new _install_manager.InstallManager(server, serverOptions);
  const lspService = new _lsp_service.LspService('127.0.0.1', serverOptions, gitOps, esClient, installManager, new _server_logger_factory.ServerLoggerFactory(server), repoConfigController);
  server.events.on('stop', async () => {
    log.debug('shutdown lsp process');
    await lspService.shutdown();
    await gitOps.cleanAllRepo();
  });
  codeServices.registerHandler(_apis.LspServiceDefinition, (0, _apis.getLspServiceHandler)(lspService), _apis.LspServiceDefinitionOption);
  codeServices.registerHandler(_apis.WorkspaceDefinition, (0, _apis.getWorkspaceHandler)(server, lspService.workspaceHandler));
  codeServices.registerHandler(_apis.SetupDefinition, _apis.setupServiceHandler);
  return {
    gitOps,
    lspService,
    installManager
  };
}