"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LanguageServerController = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _vscodeJsonrpc = require("vscode-jsonrpc");

var _language_server = require("../../common/language_server");

var _lsp_error_codes = require("../../common/lsp_error_codes");

var _detect_language = require("../utils/detect_language");

var _language_servers = require("./language_servers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Manage different LSP servers and forward request to different LSP using LanguageServerProxy, currently
 * we just use forward request to all the LSP servers we are running.
 */
class LanguageServerController {
  // a list of support language servers
  // a { lang -> server } map from above list
  constructor(options, targetHost, installManager, loggerFactory, repoConfigController) {
    this.options = options;
    this.targetHost = targetHost;
    this.installManager = installManager;
    this.loggerFactory = loggerFactory;
    this.repoConfigController = repoConfigController;

    _defineProperty(this, "languageServers", void 0);

    _defineProperty(this, "languageServerMap", void 0);

    _defineProperty(this, "log", void 0);

    this.log = loggerFactory.getLogger([]);
    this.languageServers = (0, _language_servers.enabledLanguageServers)(installManager.server).map(def => ({
      definition: def,
      builtinWorkspaceFolders: def.builtinWorkspaceFolders,
      languages: def.languages,
      maxWorkspace: options.maxWorkspace,
      launcher: new def.launcher(this.targetHost, options, loggerFactory)
    }));

    const add2map = (map, lang, ls) => {
      const arr = map[lang] || [];
      arr.push(ls);
      map[lang] = arr.sort((a, b) => b.definition.priority - a.definition.priority);
    };

    this.languageServerMap = this.languageServers.reduce((map, ls) => {
      ls.languages.forEach(lang => add2map(map, lang, ls));
      map[ls.definition.name] = [ls];
      return map;
    }, {});
  }

  async handleRequest(request) {
    const file = request.resolvedFilePath;

    if (file) {
      // #todo add test for this
      const lang = await (0, _detect_language.detectLanguage)(file.replace('file://', ''));

      if (await this.repoConfigController.isLanguageDisabled(request.documentUri, lang)) {
        throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.LanguageDisabled, `language disabled for the file`);
      }

      return await this.dispatchRequest(lang, request);
    } else {
      throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.UnknownErrorCode, `can't detect language without a file`);
    }
  }

  async dispatchRequest(lang, request) {
    if (lang) {
      const ls = this.findLanguageServer(lang);

      if (ls.builtinWorkspaceFolders) {
        if (!ls.languageServerHandlers && !ls.launcher.running) {
          ls.languageServerHandlers = ls.launcher.launch(ls.builtinWorkspaceFolders, ls.maxWorkspace, this.installManager.installationPath(ls.definition));
        }

        const handler = await ls.languageServerHandlers;
        return await handler.handleRequest(request);
      } else {
        const handler = await this.findOrCreateHandler(ls, request);
        handler.lastAccess = Date.now();
        return await handler.handleRequest(request);
      }
    } else {
      throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.UnknownFileLanguage, `can't detect language from file:${request.resolvedFilePath}`);
    }
  }
  /**
   * shutdown all language servers
   */


  async exit() {
    for (const ls of this.languageServers) {
      if (ls.languageServerHandlers) {
        if (ls.builtinWorkspaceFolders) {
          if (ls.languageServerHandlers) {
            try {
              const h = await ls.languageServerHandlers;
              await h.exit();
            } catch (e) {// expected error because of handler launch failed
            }
          }
        } else {
          const handlers = ls.languageServerHandlers;

          for (const handlerPromise of Object.values(handlers)) {
            const handler = await handlerPromise;
            await handler.exit();
          }
        }
      }
    }
  }

  async launchServers() {
    for (const ls of this.languageServers) {
      const installed = this.installManager.status(ls.definition) === _language_server.LanguageServerStatus.READY; // for those language server has builtin workspace support, we can launch them during kibana startup


      if (installed && ls.builtinWorkspaceFolders) {
        try {
          ls.languageServerHandlers = ls.launcher.launch(true, ls.maxWorkspace, this.installManager.installationPath(ls.definition));
        } catch (e) {
          this.log.error(e);
        }
      }
    }
  }

  async unloadWorkspace(workspaceDir) {
    for (const languageServer of this.languageServers) {
      if (languageServer.languageServerHandlers) {
        if (languageServer.builtinWorkspaceFolders) {
          try {
            const handler = await languageServer.languageServerHandlers;
            await handler.unloadWorkspace(workspaceDir);
          } catch (err) {// expected error because of handler launch failed
          }
        } else {
          const handlers = languageServer.languageServerHandlers;

          const realPath = _fs.default.realpathSync(workspaceDir);

          const handler = handlers[realPath];

          if (handler) {
            await (await handler).unloadWorkspace(realPath);
            delete handlers[realPath];
          }
        }
      }
    }
  }

  status(def) {
    const status = this.installManager.status(def); // installed, but is it running?

    if (status === _language_server.LanguageServerStatus.READY) {
      const ls = this.languageServers.find(d => d.definition === def);

      if (ls && ls.launcher.running) {
        return _language_server.LanguageServerStatus.RUNNING;
      }
    }

    return status;
  }

  getLanguageServerDef(lang) {
    const data = this.languageServerMap[lang];

    if (data) {
      return data.map(d => d.definition);
    }

    return [];
  }

  async findOrCreateHandler(languageServer, request) {
    let handlers;

    if (languageServer.languageServerHandlers) {
      handlers = languageServer.languageServerHandlers;
    } else {
      handlers = languageServer.languageServerHandlers = {};
    }

    if (!request.workspacePath) {
      throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.UnknownErrorCode, `no workspace in request?`);
    }

    const realPath = _fs.default.realpathSync(request.workspacePath);

    let handler = handlers[realPath];

    if (handler) {
      return handler;
    } else {
      const maxWorkspace = languageServer.maxWorkspace;
      const handlerArray = Object.entries(handlers);

      if (handlerArray.length < maxWorkspace) {
        handler = languageServer.launcher.launch(languageServer.builtinWorkspaceFolders, maxWorkspace, this.installManager.installationPath(languageServer.definition));
        handlers[realPath] = handler;
        return handler;
      } else {
        let [oldestWorkspace, oldestHandler] = handlerArray[0];

        for (const e of handlerArray) {
          const [ws, handlePromise] = e;
          const h = await handlePromise;
          const oldestAccess = (await oldestHandler).lastAccess;

          if (h.lastAccess < oldestAccess) {
            oldestWorkspace = ws;
            oldestHandler = handlePromise;
          }
        }

        delete handlers[oldestWorkspace];
        handlers[request.workspacePath] = oldestHandler;
        return oldestHandler;
      }
    }
  }

  findLanguageServer(lang) {
    const lsArr = this.languageServerMap[lang];

    if (lsArr) {
      const ls = lsArr.find(d => this.installManager.status(d.definition) !== _language_server.LanguageServerStatus.NOT_INSTALLED);

      if (!this.options.lsp.detach && ls === undefined) {
        throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.LanguageServerNotInstalled, `language server ${lang} not installed`);
      } else {
        return ls;
      }
    } else {
      throw new _vscodeJsonrpc.ResponseError(_lsp_error_codes.UnknownFileLanguage, `unsupported language ${lang}`);
    }
  }

  async initializeState(workspacePath) {
    const result = {};

    for (const languageServer of this.languageServers) {
      if (languageServer.languageServerHandlers) {
        if (languageServer.builtinWorkspaceFolders) {
          const handler = await languageServer.languageServerHandlers;
          result[languageServer.definition.name] = await handler.initializeState(workspacePath);
        } else {
          const handlers = languageServer.languageServerHandlers;

          const realPath = _fs.default.realpathSync(workspacePath);

          const handler = handlers[realPath];

          if (handler) {
            result[languageServer.definition.name] = (await handler).initializeState(workspacePath);
          }
        }
      }
    }

    return result;
  }

}

exports.LanguageServerController = LanguageServerController;