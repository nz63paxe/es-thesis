"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmbedProgram = void 0;

var _events = require("events");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

let globalPid = 0;

class EmbedProgram {
  constructor(log) {
    this.log = log;

    _defineProperty(this, "eventEmitter", new _events.EventEmitter());

    _defineProperty(this, "_killed", false);

    _defineProperty(this, "pid", void 0);

    this.pid = globalPid++;
  }

  kill(force) {
    this.stop().then(() => {
      this.log.debug('embed program stopped');
      this._killed = true;
      this.eventEmitter.emit('exit');
    });
  }

  killed() {
    return this._killed;
  }

  onExit(callback) {
    this.eventEmitter.on('exit', callback);
  }

}

exports.EmbedProgram = EmbedProgram;