"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositoryObjectClient = void 0;

var _schema = require("../indexer/schema");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * This RepositoryObjectClient is dedicated to manipulate resository related objects
 * stored in ES.
 */
class RepositoryObjectClient {
  constructor(esClient) {
    this.esClient = esClient;
  }

  async getRepositoryGitStatus(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryGitStatusReservedField);
  }

  async getRepositoryIndexStatus(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryIndexStatusReservedField);
  }

  async getRepositoryDeleteStatus(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryDeleteStatusReservedField);
  }

  async getRepositoryConfig(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryConfigReservedField);
  }

  async getRepository(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryReservedField);
  }

  async getRepositoryRandomStr(repoUri) {
    return await this.getRepositoryObject(repoUri, _schema.RepositoryRandomPathReservedField);
  }

  async getAllRepositories() {
    const res = await this.esClient.search({
      index: `${_schema.RepositoryIndexNamePrefix}*`,
      body: {
        query: {
          exists: {
            field: _schema.RepositoryReservedField
          }
        }
      },
      from: 0,
      size: 10000
    });
    const hits = res.hits.hits;
    const repos = hits.map(hit => {
      const repo = hit._source[_schema.RepositoryReservedField];
      return repo;
    });
    return repos;
  }

  async setRepositoryGitStatus(repoUri, gitStatus) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryGitStatusReservedField, gitStatus);
  }

  async setRepositoryIndexStatus(repoUri, indexStatus) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryIndexStatusReservedField, indexStatus);
  }

  async setRepositoryDeleteStatus(repoUri, deleteStatus) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryDeleteStatusReservedField, deleteStatus);
  }

  async setRepositoryConfig(repoUri, config) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryConfigReservedField, config);
  }

  async setRepositoryRandomStr(repoUri, randomStr) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryRandomPathReservedField, randomStr);
  }

  async setRepository(repoUri, repo) {
    return await this.setRepositoryObject(repoUri, _schema.RepositoryReservedField, repo);
  }

  async updateRepositoryGitStatus(repoUri, obj) {
    return await this.updateRepositoryObject(repoUri, _schema.RepositoryGitStatusReservedField, obj);
  }

  async updateRepositoryIndexStatus(repoUri, obj) {
    return await this.updateRepositoryObject(repoUri, _schema.RepositoryIndexStatusReservedField, obj);
  }

  async updateRepositoryDeleteStatus(repoUri, obj) {
    return await this.updateRepositoryObject(repoUri, _schema.RepositoryDeleteStatusReservedField, obj);
  }

  async updateRepository(repoUri, obj) {
    return await this.updateRepositoryObject(repoUri, _schema.RepositoryReservedField, obj);
  }

  async getRepositoryObject(repoUri, reservedFieldName) {
    const res = await this.esClient.get({
      index: (0, _schema.RepositoryIndexName)(repoUri),
      id: this.getRepositoryObjectId(reservedFieldName)
    });
    return res._source[reservedFieldName];
  }

  async setRepositoryObject(repoUri, reservedFieldName, obj) {
    return await this.esClient.index({
      index: (0, _schema.RepositoryIndexName)(repoUri),
      id: this.getRepositoryObjectId(reservedFieldName),
      refresh: 'true',
      body: JSON.stringify({
        [reservedFieldName]: obj
      })
    });
  }

  async updateRepositoryObject(repoUri, reservedFieldName, obj) {
    return await this.esClient.update({
      index: (0, _schema.RepositoryIndexName)(repoUri),
      id: this.getRepositoryObjectId(reservedFieldName),
      refresh: 'true',
      body: JSON.stringify({
        doc: {
          [reservedFieldName]: obj
        }
      })
    });
  }

  getRepositoryObjectId(reservedFieldName) {
    return reservedFieldName;
  }

}

exports.RepositoryObjectClient = RepositoryObjectClient;