"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLspServiceHandler = exports.LspServiceDefinition = exports.LspServiceDefinitionOption = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const LspServiceDefinitionOption = {
  routePrefix: '/api/code/internal/lsp'
};
exports.LspServiceDefinitionOption = LspServiceDefinitionOption;
const LspServiceDefinition = {
  sendRequest: {
    request: {},
    response: {}
  },
  languageSeverDef: {
    request: {},
    response: {}
  },
  languageServerStatus: {
    request: {},
    response: {}
  },
  initializeState: {
    request: {},
    response: {}
  }
};
exports.LspServiceDefinition = LspServiceDefinition;

const getLspServiceHandler = lspService => ({
  async sendRequest({
    method,
    params
  }) {
    return await lspService.sendRequest(method, params);
  },

  async languageSeverDef({
    lang
  }) {
    return lspService.getLanguageSeverDef(lang);
  },

  async languageServerStatus({
    langName
  }) {
    return lspService.languageServerStatus(langName);
  },

  async initializeState({
    repoUri,
    revision
  }) {
    return await lspService.initializeState(repoUri, revision);
  }

});

exports.getLspServiceHandler = getLspServiceHandler;