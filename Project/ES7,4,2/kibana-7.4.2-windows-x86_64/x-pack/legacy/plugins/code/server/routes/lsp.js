"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lspRoute = lspRoute;
exports.symbolByQnameRoute = symbolByQnameRoute;

var _boom = _interopRequireDefault(require("boom"));

var _vscodeJsonrpc = require("vscode-jsonrpc");

var _lsp_error_codes = require("../../common/lsp_error_codes");

var _uri_util = require("../../common/uri_util");

var _log = require("../log");

var _search = require("../search");

var _esclient_with_request = require("../utils/esclient_with_request");

var _timeout = require("../utils/timeout");

var _apis = require("../distributed/apis");

var _lsp_utils = require("../utils/lsp_utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const LANG_SERVER_ERROR = 'language server error';

function lspRoute(server, codeServices, serverOptions) {
  const log = new _log.Logger(server.server);
  const lspService = codeServices.serviceFor(_apis.LspServiceDefinition);
  const gitService = codeServices.serviceFor(_apis.GitServiceDefinition);
  server.route({
    path: '/api/code/lsp/textDocument/{method}',

    async handler(req, h) {
      if (typeof req.payload === 'object' && req.payload != null) {
        const method = req.params.method;

        if (method) {
          try {
            const params = req.payload;
            const uri = params.textDocument.uri;
            const {
              repoUri
            } = (0, _uri_util.parseLspUrl)(uri);
            const endpoint = await codeServices.locate(req, repoUri);
            const requestPromise = lspService.sendRequest(endpoint, {
              method: `textDocument/${method}`,
              params: req.payload
            });
            return await (0, _timeout.promiseTimeout)(serverOptions.lsp.requestTimeoutMs, requestPromise);
          } catch (error) {
            if (error instanceof _vscodeJsonrpc.ResponseError) {
              // hide some errors;
              if (error.code === _lsp_error_codes.UnknownFileLanguage || error.code === _lsp_error_codes.ServerNotInitialized || error.code === _lsp_error_codes.LanguageServerStartFailed) {
                log.debug(error);
              }

              return h.response({
                error: {
                  code: error.code,
                  msg: LANG_SERVER_ERROR
                }
              }).type('json').code(500); // different code for LS errors and other internal errors.
            } else if (error.isBoom) {
              return error;
            } else {
              log.error(error);
              return h.response({
                error: {
                  code: error.code || 500,
                  msg: LANG_SERVER_ERROR
                }
              }).type('json').code(500);
            }
          }
        } else {
          return h.response('missing `method` in request').code(400);
        }
      } else {
        return h.response('json body required').code(400); // bad request
      }
    },

    method: 'POST'
  });
  server.route({
    path: '/api/code/lsp/findDefinitions',
    method: 'POST',

    async handler(req, h) {
      // @ts-ignore
      const {
        textDocument,
        position
      } = req.payload;
      const {
        uri
      } = textDocument;
      const endpoint = await codeServices.locate(req, (0, _uri_util.parseLspUrl)(uri).repoUri);
      const response = await (0, _timeout.promiseTimeout)(serverOptions.lsp.requestTimeoutMs, lspService.sendRequest(endpoint, {
        method: `textDocument/edefinition`,
        params: {
          textDocument: {
            uri
          },
          position
        }
      }));
      const hover = await lspService.sendRequest(endpoint, {
        method: 'textDocument/hover',
        params: {
          textDocument: {
            uri
          },
          position
        }
      });
      const title = await (0, _lsp_utils.findTitleFromHover)(hover, uri, position);
      const symbolSearchClient = new _search.SymbolSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
      const locators = response.result;
      const locations = [];

      for (const locator of locators) {
        if (locator.location) {
          locations.push(locator.location);
        } else if (locator.qname) {
          const searchResults = await symbolSearchClient.findByQname(req.params.qname);

          for (const symbol of searchResults.symbols) {
            locations.push(symbol.symbolInformation.location);
          }
        }
      }

      const files = await (0, _lsp_utils.groupFiles)(locations, async loc => {
        const ep = await codeServices.locate(req, loc.uri);
        return await gitService.blob(ep, loc);
      });
      return {
        title,
        files,
        uri,
        position
      };
    }

  });
  server.route({
    path: '/api/code/lsp/findReferences',
    method: 'POST',

    async handler(req, h) {
      try {
        // @ts-ignore
        const {
          textDocument,
          position
        } = req.payload;
        const {
          uri
        } = textDocument;
        const endpoint = await codeServices.locate(req, (0, _uri_util.parseLspUrl)(uri).repoUri);
        const response = await (0, _timeout.promiseTimeout)(serverOptions.lsp.requestTimeoutMs, lspService.sendRequest(endpoint, {
          method: `textDocument/references`,
          params: {
            textDocument: {
              uri
            },
            position
          }
        }));
        const hover = await lspService.sendRequest(endpoint, {
          method: 'textDocument/hover',
          params: {
            textDocument: {
              uri
            },
            position
          }
        });
        const title = await (0, _lsp_utils.findTitleFromHover)(hover, uri, position);
        const files = await (0, _lsp_utils.groupFiles)(response.result, async loc => {
          const ep = await codeServices.locate(req, loc.uri);
          return await gitService.blob(ep, loc);
        });
        return {
          title,
          files,
          uri,
          position
        };
      } catch (error) {
        log.error(error);

        if (error instanceof _vscodeJsonrpc.ResponseError) {
          return h.response({
            error: {
              code: error.code,
              msg: LANG_SERVER_ERROR
            }
          }).type('json').code(500); // different code for LS errors and other internal errors.
        } else if (error.isBoom) {
          return error;
        } else {
          return h.response({
            error: {
              code: 500,
              msg: LANG_SERVER_ERROR
            }
          }).type('json').code(500);
        }
      }
    }

  });
}

function symbolByQnameRoute(router, log) {
  router.route({
    path: '/api/code/lsp/symbol/{qname}',
    method: 'GET',

    async handler(req) {
      try {
        const symbolSearchClient = new _search.SymbolSearchClient(new _esclient_with_request.EsClientWithRequest(req), log);
        return await symbolSearchClient.findByQname(req.params.qname);
      } catch (error) {
        return _boom.default.internal(`Search Exception`);
      }
    }

  });
}