"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GoServerLauncher = void 0;

var _child_process = require("child_process");

var _fs = _interopRequireDefault(require("fs"));

var _getPort = _interopRequireDefault(require("get-port"));

var glob = _interopRequireWildcard(require("glob"));

var _path = _interopRequireDefault(require("path"));

var _vscodeLanguageserverProtocol = require("vscode-languageserver-protocol");

var _abstract_launcher = require("./abstract_launcher");

var _external_program = require("./process/external_program");

var _request_expander = require("./request_expander");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const GO_LANG_DETACH_PORT = 2091;

class GoServerLauncher extends _abstract_launcher.AbstractLauncher {
  constructor(targetHost, options, loggerFactory) {
    super('go', targetHost, options, loggerFactory);
    this.targetHost = targetHost;
    this.options = options;
    this.loggerFactory = loggerFactory;
  }

  createExpander(proxy, builtinWorkspace, maxWorkspace) {
    return new _request_expander.RequestExpander(proxy, builtinWorkspace, maxWorkspace, this.options, {
      initialOptions: {
        installGoDependency: this.options.security.installGoDependency
      },
      clientCapabilities: {
        textDocument: {
          hover: {
            contentFormat: [_vscodeLanguageserverProtocol.MarkupKind.Markdown, _vscodeLanguageserverProtocol.MarkupKind.PlainText]
          }
        }
      }
    }, this.log);
  }

  async startConnect(proxy) {
    await proxy.connect();
  }

  async getPort() {
    if (!this.options.lsp.detach) {
      return await (0, _getPort.default)();
    }

    return GO_LANG_DETACH_PORT;
  }

  async getBundledGoToolchain(installationPath, log) {
    const GoToolchain = glob.sync('**/go/**', {
      cwd: installationPath
    });

    if (!GoToolchain.length) {
      return undefined;
    }

    return _path.default.resolve(installationPath, GoToolchain[0]);
  }

  async spawnProcess(installationPath, port, log) {
    const launchersFound = glob.sync(process.platform === 'win32' ? 'go-langserver.exe' : 'go-langserver', {
      cwd: installationPath
    });

    if (!launchersFound.length) {
      throw new Error('Cannot find executable go language server');
    }

    const goToolchain = await this.getBundledGoToolchain(installationPath, log);

    if (!goToolchain) {
      throw new Error('Cannot find go toolchain in bundle installation');
    } // Construct $GOROOT from the bundled go toolchain.


    const goRoot = goToolchain;

    const goHome = _path.default.resolve(goToolchain, 'bin'); // Construct $GOPATH under 'kibana/data/code'.


    const goPath = this.options.goPath;

    if (!_fs.default.existsSync(goPath)) {
      _fs.default.mkdirSync(goPath);
    }

    const goCache = _path.default.resolve(goPath, '.cache');

    const params = ['-port=' + port.toString()];

    const golsp = _path.default.resolve(installationPath, launchersFound[0]);

    const env = Object.create(process.env);
    env.PATH = process.platform === 'win32' ? goHome + ';' + env.PATH : goHome + ':' + env.PATH;
    const p = (0, _child_process.spawn)(golsp, params, {
      detached: false,
      stdio: 'pipe',
      env: { ...env,
        CLIENT_HOST: '127.0.0.1',
        CLIENT_PORT: port.toString(),
        GOROOT: goRoot,
        GOPATH: goPath,
        GOCACHE: goCache,
        CGO_ENABLED: '0'
      }
    });
    p.stdout.on('data', data => {
      log.stdout(data.toString());
    });
    p.stderr.on('data', data => {
      log.stderr(data.toString());
    });
    log.info(`Launch Go Language Server at port ${port.toString()}, pid:${p.pid}, GOROOT:${goRoot}`);
    return new _external_program.ExternalProgram(p, this.options, log);
  }

}

exports.GoServerLauncher = GoServerLauncher;