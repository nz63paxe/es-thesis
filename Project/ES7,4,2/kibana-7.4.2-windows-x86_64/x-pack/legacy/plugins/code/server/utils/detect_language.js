"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.detectLanguageByFilename = detectLanguageByFilename;
exports.detectLanguage = detectLanguage;

var _path = _interopRequireDefault(require("path"));

var _fs = _interopRequireDefault(require("fs"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function matchAll(s, re) {
  let m;
  const result = [];

  while ((m = re.exec(s)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === re.lastIndex) {
      re.lastIndex++;
    }

    result.push(m);
  }

  return result;
}

const entryFilePath = require.resolve('monaco-editor/esm/vs/basic-languages/monaco.contribution');

const languageMap = {};

if (entryFilePath) {
  const entryFileContent = _fs.default.readFileSync(entryFilePath, 'utf8');

  const importRegex = /import '(.*\.contribution\.js)'/gm;

  const dir = _path.default.dirname(entryFilePath);

  const regex = /id:\s*'(.*)',\s*extensions:\s*\[(.+)\]/gm;

  for (const m of matchAll(entryFileContent, importRegex)) {
    const contributionFile = _path.default.join(dir, m[1]);

    if (_fs.default.existsSync(contributionFile)) {
      const contributionContent = _fs.default.readFileSync(contributionFile, 'utf8');

      for (const mm of matchAll(contributionContent, regex)) {
        const langId = mm[1];
        const extensions = mm[2];

        for (let ext of extensions.split(',')) {
          ext = ext.trim().slice(1, -1);
          languageMap[ext] = langId;
        }
      }
    }
  }
}

function detectByFilename(file) {
  const ext = _path.default.extname(file);

  if (ext) {
    return languageMap[ext];
  }

  return 'other'; // TODO: if how should we deal with other types?
}

function detectLanguageByFilename(filename) {
  const lang = detectByFilename(filename);
  return lang && lang.toLowerCase();
}

async function detectLanguage(file, fileContent) {
  const lang = detectByFilename(file);
  return await Promise.resolve(lang ? lang.toLowerCase() : null);
}