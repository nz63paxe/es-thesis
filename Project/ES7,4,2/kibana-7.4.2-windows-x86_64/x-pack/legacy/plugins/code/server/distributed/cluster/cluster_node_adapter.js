"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterNodeAdapter = void 0;

var _util = _interopRequireDefault(require("util"));

var _boom = _interopRequireDefault(require("boom"));

var _config_cluster_membership_service = require("./config_cluster_membership_service");

var _cluster_service = require("./cluster_service");

var _cluster_resource_locator = require("./cluster_resource_locator");

var _non_code_node_adapter = require("../multinode/non_code_node_adapter");

var _resource_scheduler_service = require("./resource_scheduler_service");

var _cluster_node_endpoint = require("./cluster_node_endpoint");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Serve requests based on the routing table.
 *
 * For local request:
 * - serve request locally or reroute to the remote node, based on the routing table
 *
 * For remote request:
 * - serve request locally if the requested resource is on the local node, otherwise reject it
 */
class ClusterNodeAdapter {
  // used to forward requests
  constructor(server, log, serverOptions, esClient) {
    this.server = server;
    this.log = log;

    _defineProperty(this, "clusterService", void 0);

    _defineProperty(this, "clusterMembershipService", void 0);

    _defineProperty(this, "schedulerService", void 0);

    _defineProperty(this, "handlers", new Map());

    _defineProperty(this, "nonCodeAdapter", new _non_code_node_adapter.NonCodeNodeAdapter('', this.log));

    _defineProperty(this, "locator", void 0);

    this.clusterService = new _cluster_service.ClusterService(esClient, log);
    this.clusterMembershipService = new _config_cluster_membership_service.ConfigClusterMembershipService(serverOptions, this.clusterService, log);
    this.schedulerService = new _resource_scheduler_service.ResourceSchedulerService(this.clusterService, log);
    this.locator = new _cluster_resource_locator.ClusterResourceLocator(this.clusterService, this.clusterMembershipService, this.schedulerService);
  }

  async start() {
    await this.clusterService.start();
    await this.clusterMembershipService.start();
    await this.schedulerService.start();
  }

  async stop() {
    await this.schedulerService.stop();
    await this.clusterMembershipService.stop();
    await this.clusterService.stop();
  }

  getService(serviceDefinition) {
    const handler = this.handlers.get(serviceDefinition);

    if (!handler) {
      throw new Error(`Handler not exists for ${serviceDefinition}`);
    }

    return handler;
  }

  registerHandler(serviceDefinition, serviceHandler, options) {
    if (!serviceHandler) {
      throw new Error('Service handler cannot be null');
    }

    const routableServiceHandler = {}; // register a local handler that is able to route the request

    for (const method in serviceDefinition) {
      if (serviceDefinition.hasOwnProperty(method)) {
        const localHandler = serviceHandler[method];
        const wrappedHandler = this.wrapRoutableHandler(method, serviceDefinition[method], localHandler, options);
        routableServiceHandler[method] = wrappedHandler;
        const d = serviceDefinition[method];
        const path = `${options.routePrefix}/${d.routePath || method}`;
        this.server.route({
          method: 'post',
          path,
          handler: async req => {
            const {
              context,
              params
            } = req.payload;
            this.log.debug(`Receiving RPC call ${req.url.path} ${_util.default.inspect(params)}`);

            try {
              const data = await localHandler(params, context);
              return {
                data
              };
            } catch (e) {
              throw _boom.default.boomify(e);
            }
          }
        });
        this.log.info(`Registered handler for ${path}`);
      }
    }

    this.handlers.set(serviceDefinition, routableServiceHandler);
    return routableServiceHandler;
  }

  wrapRoutableHandler(method, d, handler, options) {
    return async (endpoint, params) => {
      const requestContext = endpoint.toContext();

      if (endpoint instanceof _cluster_node_endpoint.ClusterNodeEndpoint) {
        let path = `${options.routePrefix}/${d.routePath || method}`; // if we want to join base url http://localhost:5601/api with path /abc/def to get
        // http://localhost:5601/api/abc/def, the base url must end with '/', and the path cannot start with '/'
        // see https://github.com/hapijs/wreck/commit/6fc514c58c5c181327fa3e84d2a95d7d8b93f079

        if (path.startsWith('/')) {
          path = path.substr(1);
        }

        const payload = {
          context: requestContext,
          params
        };
        this.log.debug(`Request [${path}] at [${endpoint.codeNode.address}] with [${_util.default.inspect(payload)}]`);
        const {
          data
        } = await this.nonCodeAdapter.requestFn(endpoint.codeNode.address, path, payload, endpoint.httpRequest);
        return data;
      } else {
        return handler(params, requestContext);
      }
    };
  }

}

exports.ClusterNodeAdapter = ClusterNodeAdapter;