"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LspIncrementalIndexer = void 0;

var _git_diff = require("../../common/git_diff");

var _uri_util = require("../../common/uri_util");

var _model = require("../../model");

var _git_operations = require("../git_operations");

var _detect_language = require("../utils/detect_language");

var _lsp_indexer = require("./lsp_indexer");

var _schema = require("./schema");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LspIncrementalIndexer extends _lsp_indexer.LspIndexer {
  constructor(repoUri, revision, originRevision, lspService, options, gitOps, client, log) {
    super(repoUri, revision, lspService, options, gitOps, client, log);
    this.repoUri = repoUri;
    this.revision = revision;
    this.originRevision = originRevision;
    this.lspService = lspService;
    this.options = options;
    this.gitOps = gitOps;
    this.client = client;
    this.log = log;

    _defineProperty(this, "type", _model.IndexerType.LSP_INC);

    _defineProperty(this, "diff", undefined);
  }

  async start(progressReporter, checkpointReq) {
    return await super.start(progressReporter, checkpointReq);
  } // If the current checkpoint is valid. Otherwise, ignore the checkpoint


  validateCheckpoint(checkpointReq) {
    return checkpointReq !== undefined && checkpointReq.revision === this.revision && checkpointReq.originRevision === this.originRevision;
  } // If it's necessary to refresh (create and reset) all the related indices


  needRefreshIndices(_) {
    return false;
  }

  ifCheckpointMet(req, checkpointReq) {
    // Assume for the same revision pair, the order of the files we iterate the diff is definite
    // everytime.
    return req.filePath === checkpointReq.filePath && req.revision === checkpointReq.revision && req.originRevision === checkpointReq.originRevision && req.kind === checkpointReq.kind;
  }

  async prepareIndexCreationRequests() {
    // We don't need to create new indices for incremental indexing.
    return [];
  }

  async processRequest(request) {
    const stats = new Map().set(_model.IndexStatsKey.Symbol, 0).set(_model.IndexStatsKey.Reference, 0).set(_model.IndexStatsKey.File, 0).set(_model.IndexStatsKey.SymbolDeleted, 0).set(_model.IndexStatsKey.ReferenceDeleted, 0).set(_model.IndexStatsKey.FileDeleted, 0);

    if (this.isCancelled()) {
      this.log.debug(`Incremental indexer is cancelled. Skip.`);
      return stats;
    }

    const {
      kind
    } = request;
    this.log.debug(`Index ${kind} request ${JSON.stringify(request, null, 2)}`);

    switch (kind) {
      case _git_diff.DiffKind.ADDED:
        {
          await this.handleAddedRequest(request, stats);
          break;
        }

      case _git_diff.DiffKind.DELETED:
        {
          await this.handleDeletedRequest(request, stats);
          break;
        }

      case _git_diff.DiffKind.MODIFIED:
        {
          await this.handleModifiedRequest(request, stats);
          break;
        }

      case _git_diff.DiffKind.RENAMED:
        {
          await this.handleRenamedRequest(request, stats);
          break;
        }

      default:
        {
          this.log.debug(`Unsupported diff kind ${kind} for incremental indexing. Skip this request.`);
        }
    }

    return stats;
  }

  async *getIndexRequestIterator() {
    try {
      if (this.diff) {
        for (const f of this.diff.files) {
          yield {
            repoUri: this.repoUri,
            filePath: f.path,
            originPath: f.originPath,
            // Always use HEAD for now until we have multi revision.
            // Also, since the workspace might get updated during the index, we always
            // want the revision to keep updated so that lsp proxy could pass the revision
            // check per discussion here: https://github.com/elastic/code/issues/1317#issuecomment-504615833
            revision: _git_operations.HEAD,
            kind: f.kind,
            originRevision: this.originRevision
          };
        }
      }
    } catch (error) {
      this.log.error(`Get lsp incremental index requests count error.`);
      this.log.error(error);
      throw error;
    }
  }

  async getIndexRequestCount() {
    try {
      // cache here to avoid pulling the diff twice.
      this.diff = await this.gitOps.getDiff(this.repoUri, this.originRevision, this.revision);
      return this.diff.files.length;
    } catch (error) {
      if (this.isCancelled()) {
        this.log.debug(`Incremental indexer got cancelled. Skip get index count error.`);
        return 1;
      } else {
        this.log.error(`Get lsp incremental index requests count error.`);
        this.log.error(error);
        throw error;
      }
    }
  }

  async cleanIndex() {
    this.log.info('Do not need to clean index for incremental indexing.');
  }

  async handleAddedRequest(request, stats) {
    const {
      repoUri,
      filePath
    } = request;
    let content = '';

    try {
      content = await this.getFileSource(request);
    } catch (error) {
      if (error.message === this.FILE_OVERSIZE_ERROR_MSG) {
        // Skip this index request if the file is oversized
        this.log.debug(this.FILE_OVERSIZE_ERROR_MSG);
        return stats;
      } else {
        // Rethrow the issue if for other reasons
        throw error;
      }
    }

    const {
      symbolNames,
      symbolsLength,
      referencesLength
    } = await this.execLspIndexing(request);
    stats.set(_model.IndexStatsKey.Symbol, symbolsLength);
    stats.set(_model.IndexStatsKey.Reference, referencesLength);
    const language = await (0, _detect_language.detectLanguage)(filePath, Buffer.from(content));
    const body = {
      repoUri,
      path: filePath,
      content,
      language,
      qnames: Array.from(symbolNames)
    };
    await this.docBatchIndexHelper.index((0, _schema.DocumentIndexName)(repoUri), body);
    stats.set(_model.IndexStatsKey.File, 1);
  }

  async handleDeletedRequest(request, stats) {
    const {
      revision,
      filePath,
      repoUri
    } = request; // Delete the document with the exact file path. TODO: add stats

    const docRes = await this.client.deleteByQuery({
      index: (0, _schema.DocumentIndexName)(repoUri),
      body: {
        query: {
          term: {
            'path.hierarchy': filePath
          }
        }
      }
    });

    if (docRes) {
      stats.set(_model.IndexStatsKey.FileDeleted, docRes.deleted);
    }

    const lspDocUri = (0, _uri_util.toCanonicalUrl)({
      repoUri,
      revision,
      file: filePath,
      schema: 'git:'
    }); // Delete all symbols within this file

    const symbolRes = await this.client.deleteByQuery({
      index: (0, _schema.SymbolIndexName)(repoUri),
      body: {
        query: {
          term: {
            'symbolInformation.location.uri': lspDocUri
          }
        }
      }
    });

    if (symbolRes) {
      stats.set(_model.IndexStatsKey.SymbolDeleted, symbolRes.deleted);
    } // TODO: When references is enabled. Clean up the references as well.

  }

  async handleModifiedRequest(request, stats) {
    const {
      originRevision,
      originPath
    } = request; // 1. first delete all related indexed data

    await this.handleDeletedRequest({ ...request,
      revision: originRevision,
      filePath: originPath ? originPath : ''
    }, stats); // 2. index data with modified version

    await this.handleAddedRequest(request, stats);
  }

  async handleRenamedRequest(request, stats) {
    // Do the same as modified file
    await this.handleModifiedRequest(request, stats);
  }

}

exports.LspIncrementalIndexer = LspIncrementalIndexer;