"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getWorkspaceHandler = exports.WorkspaceDefinition = void 0;

var _workspace_command = require("../../lsp/workspace_command");

var _log = require("../../log");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const WorkspaceDefinition = {
  initCmd: {
    request: {},
    response: {}
  }
};
exports.WorkspaceDefinition = WorkspaceDefinition;

const getWorkspaceHandler = (server, workspaceHandler) => ({
  async initCmd({
    repoUri,
    revision,
    repoConfig,
    force
  }) {
    try {
      const {
        workspaceDir,
        workspaceRevision
      } = await workspaceHandler.openWorkspace(repoUri, revision);
      const log = new _log.Logger(server, ['workspace', repoUri]);
      const workspaceCmd = new _workspace_command.WorkspaceCommand(repoConfig, workspaceDir, workspaceRevision, log);
      await workspaceCmd.runInit(force);
      return {};
    } catch (e) {
      if (e.isBoom) {
        return e;
      }
    }
  }

});

exports.getWorkspaceHandler = getWorkspaceHandler;