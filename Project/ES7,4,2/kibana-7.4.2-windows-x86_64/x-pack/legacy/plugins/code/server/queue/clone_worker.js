"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CloneWorker = void 0;

var _lodash = require("lodash");

var _git_url_utils = require("../../common/git_url_utils");

var _repository_utils = require("../../common/repository_utils");

var _model = require("../../model");

var _abstract_git_worker = require("./abstract_git_worker");

var _cancellation_service = require("./cancellation_service");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class CloneWorker extends _abstract_git_worker.AbstractGitWorker {
  constructor(queue, log, client, serverOptions, gitOps, indexWorker, repoServiceFactory, cancellationService, watermarkService) {
    super(queue, log, client, serverOptions, gitOps, watermarkService);
    this.queue = queue;
    this.log = log;
    this.client = client;
    this.serverOptions = serverOptions;
    this.gitOps = gitOps;
    this.indexWorker = indexWorker;
    this.repoServiceFactory = repoServiceFactory;
    this.cancellationService = cancellationService;
    this.watermarkService = watermarkService;

    _defineProperty(this, "id", 'clone');
  }

  async executeJob(job) {
    const superRes = await super.executeJob(job);

    if (superRes.cancelled) {
      return superRes;
    }

    const {
      payload,
      cancellationToken
    } = job;
    const {
      url
    } = payload;

    try {
      (0, _git_url_utils.validateGitUrl)(url, this.serverOptions.security.gitHostWhitelist, this.serverOptions.security.gitProtocolWhitelist);
    } catch (error) {
      this.log.error(`Validate git url ${url} error.`);
      this.log.error(error);
      return {
        uri: url
      };
    }

    this.log.info(`Execute clone job for ${url}`);
    const repoService = this.repoServiceFactory.newInstance(this.serverOptions.repoPath, this.serverOptions.credsPath, this.log, this.serverOptions.security.enableGitCertCheck);

    const repo = _repository_utils.RepositoryUtils.buildRepository(url); // Try to cancel any existing clone job for this repository.


    await this.cancellationService.cancelCloneJob(repo.uri, _cancellation_service.CancellationReason.NEW_JOB_OVERRIDEN);
    let cancelled = false;
    let cancelledReason;

    if (cancellationToken) {
      cancellationToken.on(reason => {
        cancelled = true;
        cancelledReason = reason;
      });
    }

    const cloneJobPromise = repoService.clone(repo, async (progress, cloneProgress) => {
      if (cancelled) {
        // return false to stop the clone progress
        return false;
      } // Keep an eye on the disk usage during clone in case it goes above the
      // disk watermark config.


      if (await this.watermarkService.isLowWatermark()) {
        // Cancel this clone job
        if (cancellationToken) {
          cancellationToken.cancel(_cancellation_service.CancellationReason.LOW_DISK_SPACE);
        } // return false to stop the clone progress


        return false;
      } // For clone job payload, it only has the url. Populate back the
      // repository uri before update progress.


      job.payload.uri = repo.uri;
      this.updateProgress(job, progress, undefined, cloneProgress);
      return true;
    });

    if (cancellationToken) {
      await this.cancellationService.registerCancelableCloneJob(repo.uri, cancellationToken, cloneJobPromise);
    }

    const res = await cloneJobPromise;
    return { ...res,
      cancelled,
      cancelledReason
    };
  }

  async onJobCompleted(job, res) {
    if (res.cancelled) {
      await this.onJobCancelled(job, res.cancelledReason); // Skip updating job progress if the job is done because of cancellation.

      return;
    }

    const {
      uri,
      revision
    } = res.repo;
    this.log.info(`Clone job done for ${uri}`); // For clone job payload, it only has the url. Populate back the
    // repository uri.

    job.payload.uri = uri;
    await super.onJobCompleted(job, res); // Throw out a repository index request after 1 second.

    return (0, _lodash.delay)(async () => {
      const payload = {
        uri,
        revision
      };
      await this.indexWorker.enqueueJob(payload, {});
    }, 1000);
  }

  async onJobEnqueued(job) {
    const {
      url
    } = job.payload;

    const repo = _repository_utils.RepositoryUtils.buildRepository(url);

    const progress = {
      uri: repo.uri,
      progress: _model.WorkerReservedProgress.INIT,
      timestamp: new Date()
    };
    return await this.objectClient.setRepositoryGitStatus(repo.uri, progress);
  }

  async onJobExecutionError(res) {
    // The payload of clone job won't have the `uri`, but only with `url`.
    const url = res.job.payload.url;

    const repo = _repository_utils.RepositoryUtils.buildRepository(url);

    res.job.payload.uri = repo.uri;
    return await super.onJobExecutionError(res);
  }

  async onJobTimeOut(res) {
    // The payload of clone job won't have the `uri`, but only with `url`.
    const url = res.job.payload.url;

    const repo = _repository_utils.RepositoryUtils.buildRepository(url);

    res.job.payload.uri = repo.uri;
    return await super.onJobTimeOut(res);
  }

}

exports.CloneWorker = CloneWorker;