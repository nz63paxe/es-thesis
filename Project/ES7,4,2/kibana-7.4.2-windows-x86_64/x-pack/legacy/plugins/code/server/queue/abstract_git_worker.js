"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractGitWorker = void 0;

var _model = require("../../model");

var _search = require("../search");

var _abstract_worker = require("./abstract_worker");

var _cancellation_service = require("./cancellation_service");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AbstractGitWorker extends _abstract_worker.AbstractWorker {
  constructor(queue, log, client, serverOptions, gitOps, watermarkService) {
    super(queue, log);
    this.queue = queue;
    this.log = log;
    this.client = client;
    this.serverOptions = serverOptions;
    this.gitOps = gitOps;
    this.watermarkService = watermarkService;

    _defineProperty(this, "id", 'abstract-git');

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(client);
  }

  async executeJob(job) {
    const uri = job.payload.uri;

    if (await this.watermarkService.isLowWatermark()) {
      // Return job result as cancelled.
      return {
        uri,
        cancelled: true,
        cancelledReason: _cancellation_service.CancellationReason.LOW_DISK_SPACE
      };
    }

    return {
      uri
    };
  }

  async onJobCompleted(job, res) {
    await super.onJobCompleted(job, res); // Update the default branch.

    const repoUri = res.uri;
    const revision = await this.gitOps.getHeadRevision(repoUri);
    const defaultBranch = await this.gitOps.getDefaultBranch(repoUri); // Update the repository data.

    try {
      await this.objectClient.updateRepository(repoUri, {
        defaultBranch,
        revision
      });
    } catch (error) {
      this.log.error(`Update repository default branch and revision error.`);
      this.log.error(error);
    } // Update the git operation status.


    try {
      return await this.objectClient.updateRepositoryGitStatus(repoUri, {
        revision,
        progress: _model.WorkerReservedProgress.COMPLETED,
        cloneProgress: {
          isCloned: true
        }
      });
    } catch (error) {
      this.log.error(`Update revision of repo clone done error.`);
      this.log.error(error);
    }
  }

  async updateProgress(job, progress, error, cloneProgress) {
    const {
      uri
    } = job.payload;
    const p = {
      uri,
      progress,
      timestamp: new Date(),
      cloneProgress,
      errorMessage: error ? error.message : undefined
    };

    try {
      return await this.objectClient.updateRepositoryGitStatus(uri, p);
    } catch (err) {// Do nothing here since it's not blocking anything.
      // this.log.warn(`Update git clone progress error.`);
      // this.log.warn(err);
    }
  }

  async onJobCancelled(job, reason) {
    if (reason && reason === _cancellation_service.CancellationReason.LOW_DISK_SPACE) {
      // If the clone/update job is cancelled because of the disk watermark, manually
      // trigger onJobExecutionError.
      const msg = this.watermarkService.diskWatermarkViolationMessage();
      this.log.error('Git clone/update job completed because of low disk space. Move forward as error.');
      const error = new Error(msg);
      await this.onJobExecutionError({
        job,
        error
      });
    }
  }

}

exports.AbstractGitWorker = AbstractGitWorker;