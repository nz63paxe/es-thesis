"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitSearchClient = void 0;

var _schema = require("../indexer/schema");

var _abstract_search_client = require("./abstract_search_client");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CommitSearchClient extends _abstract_search_client.AbstractSearchClient {
  constructor(client, log) {
    super(client, log);
    this.client = client;
    this.log = log;
  }

  async search(req) {
    const resultsPerPage = this.getResultsPerPage(req);
    const from = (req.page - 1) * resultsPerPage;
    const size = resultsPerPage;
    const index = req.repoScope ? (0, _schema.CommitSearchIndexWithScope)(req.repoScope) : `${_schema.CommitIndexNamePrefix}*`; // Post filters for repository

    let repositoryPostFilters = [];

    if (req.repoFilters) {
      repositoryPostFilters = req.repoFilters.map(repoUri => {
        return {
          term: {
            repoUri
          }
        };
      });
    }

    const rawRes = await this.client.search({
      index,
      body: {
        from,
        size,
        query: {
          bool: {
            should: [{
              match: {
                message: req.query
              }
            }, {
              match: {
                body: req.query
              }
            }],
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        },
        post_filter: {
          bool: {
            should: repositoryPostFilters,
            disable_coord: false,
            adjust_pure_negative: true,
            boost: 1.0
          }
        },
        aggregations: {
          repoUri: {
            terms: {
              field: 'repoUri',
              size: 10,
              min_doc_count: 1,
              shard_min_doc_count: 0,
              show_term_doc_count_error: false,
              order: [{
                _count: 'desc'
              }, {
                _key: 'asc'
              }]
            }
          }
        }
      }
    });
    const hits = rawRes.hits.hits;
    const aggregations = rawRes.aggregations;
    const results = hits.map(hit => {
      const commit = hit._source;
      return commit;
    });
    const total = rawRes.hits.total.value;
    return {
      query: req.query,
      from,
      page: req.page,
      totalPage: Math.ceil(total / resultsPerPage),
      commits: results,
      repoAggregations: aggregations.repoUri.buckets,
      took: rawRes.took,
      total
    };
  }

}

exports.CommitSearchClient = CommitSearchClient;