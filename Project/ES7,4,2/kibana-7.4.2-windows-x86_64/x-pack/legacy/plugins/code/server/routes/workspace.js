"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.workspaceRoute = workspaceRoute;

var _boom = _interopRequireDefault(require("boom"));

var _apis = require("../distributed/apis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function workspaceRoute(router, serverOptions, codeServices) {
  const workspaceService = codeServices.serviceFor(_apis.WorkspaceDefinition);
  router.route({
    path: '/api/code/workspace',
    method: 'GET',

    async handler() {
      return serverOptions.repoConfigs;
    }

  });
  router.route({
    path: '/api/code/workspace/{uri*3}/{revision}',
    requireAdmin: true,
    method: 'POST',

    async handler(req) {
      const repoUri = req.params.uri;
      const revision = req.params.revision;
      const repoConfig = serverOptions.repoConfigs[repoUri];
      const force = !!req.query.force;

      if (repoConfig) {
        const endpoint = await codeServices.locate(req, repoUri);

        try {
          await workspaceService.initCmd(endpoint, {
            repoUri,
            revision,
            force,
            repoConfig
          });
        } catch (e) {
          if (e.isBoom) {
            return e;
          }
        }
      } else {
        return _boom.default.notFound(`repo config for ${repoUri} not found.`);
      }
    }

  });
}