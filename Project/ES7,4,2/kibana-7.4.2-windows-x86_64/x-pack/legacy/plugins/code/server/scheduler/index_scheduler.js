"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IndexScheduler = void 0;

var _repository_utils = require("../../common/repository_utils");

var _model = require("../../model");

var _search = require("../search");

var _abstract_scheduler = require("./abstract_scheduler");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexScheduler extends _abstract_scheduler.AbstractScheduler {
  constructor(indexWorker, serverOptions, client, log, onScheduleFinished) {
    super(client, serverOptions.indexFrequencyMs, onScheduleFinished);
    this.indexWorker = indexWorker;
    this.serverOptions = serverOptions;
    this.client = client;
    this.log = log;
    this.onScheduleFinished = onScheduleFinished;

    _defineProperty(this, "objectClient", void 0);

    this.objectClient = new _search.RepositoryObjectClient(this.client);
  }

  getRepoSchedulingFrequencyMs() {
    return this.serverOptions.indexRepoFrequencyMs;
  }

  async executeSchedulingJob(repo) {
    this.log.debug(`Schedule index repo request for ${repo.uri}`);

    try {
      // This repository is too soon to execute the next index job.
      if (repo.nextIndexTimestamp && new Date() < new Date(repo.nextIndexTimestamp)) {
        this.log.debug(`Repo ${repo.uri} is too soon to execute the next index job.`);
        return;
      }

      const cloneStatus = await this.objectClient.getRepositoryGitStatus(repo.uri);

      if (!_repository_utils.RepositoryUtils.hasFullyCloned(cloneStatus.cloneProgress) || cloneStatus.progress !== _model.WorkerReservedProgress.COMPLETED) {
        this.log.debug(`Repo ${repo.uri} has not been fully cloned yet or in update. Skip index.`);
        return;
      }

      const repoIndexStatus = await this.objectClient.getRepositoryIndexStatus(repo.uri); // Schedule index job only when the indexed revision is different from the current repository
      // revision.

      this.log.debug(`Current repo revision: ${repo.revision}, indexed revision ${repoIndexStatus.revision}.`);

      if (repoIndexStatus.progress >= 0 && repoIndexStatus.progress < _model.WorkerReservedProgress.COMPLETED) {
        this.log.debug(`Repo is still in indexing. Skip index for ${repo.uri}`);
      } else if (repoIndexStatus.progress === _model.WorkerReservedProgress.COMPLETED && repoIndexStatus.revision === repo.revision) {
        this.log.debug(`Repo does not change since last index. Skip index for ${repo.uri}.`);
      } else {
        const payload = {
          uri: repo.uri,
          revision: repo.revision
        }; // Update the next repo index timestamp.

        const nextRepoIndexTimestamp = this.repoNextSchedulingTime();
        await this.objectClient.updateRepository(repo.uri, {
          nextIndexTimestamp: nextRepoIndexTimestamp
        });
        await this.indexWorker.enqueueJob(payload, {});
      }
    } catch (error) {
      this.log.error(`Schedule index job for ${repo.uri} error.`);
      this.log.error(error);
    }
  }

}

exports.IndexScheduler = IndexScheduler;