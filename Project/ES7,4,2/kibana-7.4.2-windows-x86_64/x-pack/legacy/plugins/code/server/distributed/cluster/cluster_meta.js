"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClusterMetadata = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * A snapshot of the cluster meta data, which includes:
 * - repository objects
 */
class ClusterMetadata {
  constructor(repositories = []) {
    this.repositories = repositories;

    _defineProperty(this, "uri2Repo", void 0);

    this.uri2Repo = repositories.reduce((prev, cur) => {
      prev.set(cur.uri, cur);
      return prev;
    }, new Map());
  }

  getRepository(uri) {
    return this.uri2Repo.get(uri);
  }

}

exports.ClusterMetadata = ClusterMetadata;