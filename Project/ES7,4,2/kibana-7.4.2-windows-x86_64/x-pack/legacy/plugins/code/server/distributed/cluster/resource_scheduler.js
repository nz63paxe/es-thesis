"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResourceScheduler = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * The scheduler decides how to allocate resources to nodes. Allocating resources to nodes basically follows 2 steps:
 * - Filter: find nodes that is applicable for the resource, based on attributes of nodes and restrictions of policies.
 * - Score: score each applicable node for the resource, and return a list of applicable nodes sorted by the score in the
 *   descending order.
 * - Decide: pick the best fit node from the list as the candidate.
 */
class ResourceScheduler {
  constructor(policies, log) {
    this.policies = policies;
    this.log = log;
  }

  allocate(resources, state) {
    if (_lodash.default.isEmpty(resources)) {
      return [];
    }

    if (_lodash.default.isEmpty(state.nodes.nodes)) {
      return [];
    } // remove repos cannot be allocated


    const allocatableRepos = resources.filter(repo => {
      return _lodash.default.every(this.policies, policy => policy.canAllocate(repo, state.routingTable, state.nodes));
    });

    if (_lodash.default.isEmpty(allocatableRepos)) {
      return [];
    }

    const assignments = [];
    let routingTable = state.routingTable;

    for (const resource of resources) {
      const assignment = this.allocateResource(resource, state.nodes, routingTable);

      if (!assignment) {
        continue;
      }

      assignments.push(assignment);
      routingTable = routingTable.assign([assignment]);
      this.log.info(`Assigned resource [${resource}] to node [${assignment.nodeId}]`);
    }

    return assignments;
  }

  allocateResource(resource, nodes, routingTable) {
    const scoredNodes = nodes.nodes.filter(node => {
      // remove nodes that the
      return _lodash.default.every(this.policies, policy => policy.canAllocateOnNode(resource, node, routingTable, nodes));
    }).map(node => {
      const score = this.policies.reduce((prev, policy) => prev * policy.score(resource, node, routingTable, nodes), 1);
      return {
        node,
        score
      };
    });

    if (_lodash.default.isEmpty(scoredNodes)) {
      return undefined;
    }

    let bestFit = scoredNodes[0];

    for (const node of scoredNodes) {
      if (node.score >= bestFit.score) {
        // use the node id as the tie-breaker
        if (node.node.id > bestFit.node.id) {
          bestFit = node;
        }
      }
    }

    return {
      nodeId: bestFit.node.id,
      resource
    };
  }

}

exports.ResourceScheduler = ResourceScheduler;