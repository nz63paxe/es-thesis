"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeNodeResourceLocator = void 0;

var _code_node_endpoint = require("./code_node_endpoint");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class CodeNodeResourceLocator {
  constructor(codeNodeUrl) {
    this.codeNodeUrl = codeNodeUrl;
  }

  async locate(httpRequest, resource) {
    return Promise.resolve(new _code_node_endpoint.CodeNodeEndpoint(httpRequest, resource, this.codeNodeUrl));
  }

  isResourceLocal(resource) {
    return Promise.resolve(false);
  }

  allocate(req, resource) {
    return this.locate(req, resource);
  }

}

exports.CodeNodeResourceLocator = CodeNodeResourceLocator;