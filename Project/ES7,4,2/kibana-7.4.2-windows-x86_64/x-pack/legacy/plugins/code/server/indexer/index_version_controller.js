"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IndexVersionController = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _2 = require(".");

var _es_exception = require("./es_exception");

var _version = _interopRequireDefault(require("./schema/version.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexVersionController {
  constructor(client, log) {
    this.client = client;
    this.log = log;

    _defineProperty(this, "version", void 0);

    _defineProperty(this, "DEFAULT_VERSION", 0);

    this.version = Number(_version.default.codeIndexVersion);
  }

  async tryUpgrade(request) {
    this.log.debug(`Try upgrade index mapping/settings for index ${request.index}.`);

    try {
      const esIndexVersion = await this.getIndexVersionFromES(request.index);
      const needUpgrade = this.needUpgrade(esIndexVersion);

      if (needUpgrade) {
        const migrator = new _2.IndexMigrator(this.client, this.log);
        const oldIndexName = `${request.index}-${esIndexVersion}`;
        this.log.info(`Migrate index mapping/settings from version ${esIndexVersion} for ${request.index}`);
        return migrator.migrateIndex(oldIndexName, request);
      } else {
        this.log.debug(`Index version is update-to-date for ${request.index}`);
      }
    } catch (error) {
      if (error.body && error.body.error && error.body.error.type === _es_exception.EsException.INDEX_NOT_FOUND_EXCEPTION) {
        this.log.info(`Skip upgrade index ${request.index} because original index does not exist.`);
      } else {
        this.log.error(`Try upgrade index error for ${request.index}.`);
        this.log.error(error);
      }
    }
  }
  /*
   * Currently there is a simple rule to decide if we need upgrade the index or not: if the index
   * version is smaller than current version specified in the package.json file under `codeIndexVersion`.
   */


  needUpgrade(oldIndexVersion) {
    return oldIndexVersion < this.version;
  }

  async getIndexVersionFromES(indexName) {
    const res = await this.client.indices.getMapping({
      index: indexName
    });
    const esIndexName = Object.keys(res)[0];

    const version = _lodash.default.get(res, [esIndexName, 'mappings', '_meta', 'version'], this.DEFAULT_VERSION);

    if (version === this.DEFAULT_VERSION) {
      this.log.warn(`Can't find index version field in _meta for ${indexName}.`);
    }

    return version;
  }

}

exports.IndexVersionController = IndexVersionController;