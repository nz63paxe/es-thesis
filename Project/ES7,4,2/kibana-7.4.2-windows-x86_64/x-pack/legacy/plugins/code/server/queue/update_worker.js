"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UpdateWorker = void 0;

var _model = require("../../model");

var _abstract_git_worker = require("./abstract_git_worker");

var _cancellation_service = require("./cancellation_service");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class UpdateWorker extends _abstract_git_worker.AbstractGitWorker {
  constructor(queue, log, client, serverOptions, gitOps, repoServiceFactory, cancellationService, watermarkService) {
    super(queue, log, client, serverOptions, gitOps, watermarkService);
    this.queue = queue;
    this.log = log;
    this.client = client;
    this.serverOptions = serverOptions;
    this.gitOps = gitOps;
    this.repoServiceFactory = repoServiceFactory;
    this.cancellationService = cancellationService;
    this.watermarkService = watermarkService;

    _defineProperty(this, "id", 'update');
  }

  async executeJob(job) {
    const superRes = await super.executeJob(job);

    if (superRes.cancelled) {
      return superRes;
    }

    const {
      payload,
      cancellationToken
    } = job;
    const repo = payload;
    this.log.info(`Execute update job for ${repo.uri}`);
    const repoService = this.repoServiceFactory.newInstance(this.serverOptions.repoPath, this.serverOptions.credsPath, this.log, this.serverOptions.security.enableGitCertCheck); // Try to cancel any existing update job for this repository.

    await this.cancellationService.cancelUpdateJob(repo.uri, _cancellation_service.CancellationReason.NEW_JOB_OVERRIDEN);
    let cancelled = false;
    let cancelledReason;

    if (cancellationToken) {
      cancellationToken.on(reason => {
        cancelled = true;
        cancelledReason = reason;
      });
    }

    const updateJobPromise = repoService.update(repo, async () => {
      if (cancelled) {
        // return false to stop the update progress
        return false;
      } // Keep an eye on the disk usage during update in case it goes above the
      // disk watermark config.


      if (await this.watermarkService.isLowWatermark()) {
        // Cancel this update job
        if (cancellationToken) {
          cancellationToken.cancel(_cancellation_service.CancellationReason.LOW_DISK_SPACE);
        } // return false to stop the update progress


        return false;
      }

      return true;
    });

    if (cancellationToken) {
      await this.cancellationService.registerCancelableUpdateJob(repo.uri, cancellationToken, updateJobPromise);
    }

    const res = await updateJobPromise;
    return { ...res,
      cancelled,
      cancelledReason
    };
  }

  async onJobCompleted(job, res) {
    if (res.cancelled) {
      await this.onJobCancelled(job, res.cancelledReason); // Skip updating job progress if the job is done because of cancellation.

      return;
    }

    this.log.info(`Update job done for ${job.payload.uri}`);
    return await super.onJobCompleted(job, res);
  }

  async onJobExecutionError(res) {
    return await this.overrideUpdateErrorProgress(res);
  }

  async onJobTimeOut(res) {
    return await this.overrideUpdateErrorProgress(res);
  }

  async overrideUpdateErrorProgress(res) {
    this.log.warn(`Update job error`);
    this.log.warn(res.error); // Do not persist update errors assuming the next update trial is scheduling soon.

    return await this.updateProgress(res.job, _model.WorkerReservedProgress.COMPLETED);
  }

}

exports.UpdateWorker = UpdateWorker;