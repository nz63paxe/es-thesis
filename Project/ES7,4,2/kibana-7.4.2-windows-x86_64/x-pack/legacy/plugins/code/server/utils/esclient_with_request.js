"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EsClientWithRequest = void 0;

var _es_index_client = require("./es_index_client");

var _with_request = require("./with_request");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class EsClientWithRequest extends _with_request.WithRequest {
  constructor(req) {
    super(req);
    this.req = req;

    _defineProperty(this, "indices", new _es_index_client.EsIndexClient(this));
  }

  bulk(params) {
    return this.callCluster('bulk', params);
  }

  delete(params) {
    return this.callCluster('delete', params);
  }

  deleteByQuery(params) {
    return this.callCluster('deleteByQuery', params);
  }

  get(params) {
    return this.callCluster('get', params);
  }

  index(params) {
    return this.callCluster('index', params);
  }

  ping() {
    return this.callCluster('ping');
  }

  reindex(params) {
    return this.callCluster('reindex', params);
  }

  search(params) {
    return this.callCluster('search', params);
  }

  update(params) {
    return this.callCluster('update', params);
  }

  updateByQuery(params) {
    return this.callCluster('updateByQuery', params);
  }

}

exports.EsClientWithRequest = EsClientWithRequest;