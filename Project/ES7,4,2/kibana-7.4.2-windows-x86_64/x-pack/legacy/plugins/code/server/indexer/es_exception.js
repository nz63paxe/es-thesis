"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EsException = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let EsException;
exports.EsException = EsException;

(function (EsException) {
  EsException["INDEX_NOT_FOUND_EXCEPTION"] = "index_not_found_exception";
  EsException["RESOURCE_ALREADY_EXISTS_EXCEPTION"] = "resource_already_exists_exception";
})(EsException || (exports.EsException = EsException = {}));