"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.commitInfo = commitInfo;
exports.GitOperations = exports.DEFAULT_TREE_CHILDREN_LIMIT = exports.HEAD = void 0;

var _nodegit = require("@elastic/nodegit");

var _boom = _interopRequireDefault(require("boom"));

var _lruCache = _interopRequireDefault(require("lru-cache"));

var Path = _interopRequireWildcard(require("path"));

var fs = _interopRequireWildcard(require("fs"));

var _git_diff = require("../common/git_diff");

var _model = require("../model");

var _commit = require("../model/commit");

var _detect_language = require("./utils/detect_language");

var _git_prime = require("./utils/git_prime");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const HEAD = 'HEAD';
exports.HEAD = HEAD;
const REFS_HEADS = 'refs/heads/';
const DEFAULT_TREE_CHILDREN_LIMIT = 50;
/**
 * do a nodegit operation and check the results. If it throws a not found error or returns null,
 * rethrow a Boom.notFound error.
 * @param func the nodegit operation
 * @param message the message pass to Boom.notFound error
 */

exports.DEFAULT_TREE_CHILDREN_LIMIT = DEFAULT_TREE_CHILDREN_LIMIT;

async function checkExists(func, message) {
  let result;

  try {
    result = await func();
  } catch (e) {
    if (e.errno === _nodegit.Error.CODE.ENOTFOUND) {
      throw _boom.default.notFound(message);
    } else {
      throw e;
    }
  }

  if (result == null) {
    throw _boom.default.notFound(message);
  }

  return result;
}

function entry2Tree(entry, prefixPath = '') {
  const type = GitOperations.mode2type(entry.mode);
  const {
    path,
    oid
  } = entry;
  return {
    name: path,
    path: prefixPath ? prefixPath + '/' + path : path,
    sha1: oid,
    type
  };
}

class GitOperations {
  // 1 hour;
  constructor(repoRoot) {
    _defineProperty(this, "REPO_LRU_CACHE_SIZE", 16);

    _defineProperty(this, "REPO_MAX_AGE_MS", 60 * 60 * 1000);

    _defineProperty(this, "repoRoot", void 0);

    _defineProperty(this, "repoCache", void 0);

    this.repoRoot = repoRoot;
    const options = {
      max: this.REPO_LRU_CACHE_SIZE,
      maxAge: this.REPO_MAX_AGE_MS,
      dispose: (repoUri, repo) => {
        // Clean up the repository before disposing this repo out of the cache.
        repo.cleanup();
      }
    };
    this.repoCache = new _lruCache.default(options);
  }

  cleanRepo(uri) {
    if (this.repoCache.has(uri)) {
      this.repoCache.del(uri);
    }
  }

  async cleanAllRepo() {
    this.repoCache.reset();
  }

  async fileContent(uri, path, revision = 'master') {
    const gitdir = this.repoDir(uri);
    const commit = await this.getCommitOr404(uri, revision);
    const file = await _git_prime.GitPrime.readObject({
      gitdir,
      oid: commit.id,
      filepath: path,
      format: 'content'
    });

    if (file && file.type === 'blob') {
      return file.object;
    }

    throw _boom.default.unsupportedMediaType(`${uri}/${path} is not a file.`);
  }

  async getCommit(uri, revision) {
    const info = await this.getCommitOr404(uri, revision);
    const repo = await this.openRepo(uri);
    return await checkExists(() => this.findCommit(repo, info.id), `revision or branch ${revision} not found in ${uri}`);
  }

  async getDefaultBranch(uri) {
    const gitdir = this.repoDir(uri);
    const ref = await _git_prime.GitPrime.resolveRef({
      gitdir,
      ref: HEAD,
      depth: 2
    });

    if (ref.startsWith(REFS_HEADS)) {
      return ref.substr(REFS_HEADS.length);
    }

    return ref;
  }

  async getHeadRevision(uri) {
    const gitdir = this.repoDir(uri);
    return await _git_prime.GitPrime.resolveRef({
      gitdir,
      ref: HEAD,
      depth: 10
    });
  }

  async blame(uri, revision, path) {
    const repo = await this.openRepo(uri);
    const newestCommit = (await this.getCommit(uri, revision)).id();
    const blame = await _nodegit.Blame.file(repo, path, {
      newestCommit
    });
    const results = [];

    for (let i = 0; i < blame.getHunkCount(); i++) {
      const hunk = blame.getHunkByIndex(i); // @ts-ignore wrong definition in nodegit

      const commit = await repo.getCommit(hunk.finalCommitId());
      results.push({
        committer: {
          // @ts-ignore wrong definition in nodegit
          name: hunk.finalSignature().name(),
          // @ts-ignore wrong definition in nodegit
          email: hunk.finalSignature().email()
        },
        // @ts-ignore wrong definition in nodegit
        startLine: hunk.finalStartLineNumber(),
        // @ts-ignore wrong definition in nodegit
        lines: hunk.linesInHunk(),
        commit: {
          id: commit.sha(),
          message: commit.message(),
          date: commit.date().toISOString()
        }
      });
    }

    return results;
  }

  async openRepo(uri) {
    if (this.repoCache.has(uri)) {
      const repo = this.repoCache.get(uri);
      return Promise.resolve(repo);
    }

    const repoDir = this.repoDir(uri);
    const repo = await checkExists(() => _nodegit.Repository.open(repoDir), `repo ${uri} not found`);
    this.repoCache.set(uri, repo);
    return Promise.resolve(repo);
  }

  repoDir(uri) {
    const repoDir = Path.join(this.repoRoot, uri);
    this.checkPath(repoDir);
    return repoDir;
  }

  checkPath(path) {
    if (!fs.realpathSync(path).startsWith(fs.realpathSync(this.repoRoot))) {
      throw new Error('invalid path');
    }
  }

  static async isTextFile(gitdir, entry) {
    if (entry.type === 'blob') {
      const type = GitOperations.mode2type(entry.mode);

      if (type === _model.FileTreeItemType.File) {
        return await _git_prime.GitPrime.isTextFile({
          gitdir,
          oid: entry.oid
        });
      }
    }

    return false;
  }

  async countRepoFiles(uri, revision) {
    let count = 0;
    const commit = await this.getCommitOr404(uri, revision);
    const gitdir = this.repoDir(uri);
    const commitObject = await _git_prime.GitPrime.readObject({
      gitdir,
      oid: commit.id
    });
    const treeId = commitObject.object.tree;

    async function walk(oid) {
      const tree = await GitOperations.readTree(gitdir, oid);

      for (const entry of tree.entries) {
        if (entry.type === 'tree') {
          await walk(entry.oid);
        } else if (await GitOperations.isTextFile(gitdir, entry)) {
          count++;
        }
      }
    }

    await walk(treeId);
    return count;
  }

  async iterateRepo(uri, revision) {
    const commit = await this.getCommitOr404(uri, revision);
    const gitdir = this.repoDir(uri);
    const commitObject = await _git_prime.GitPrime.readObject({
      gitdir,
      oid: commit.id
    });
    const treeId = commitObject.object.tree;

    async function* walk(oid, prefix = '') {
      const tree = await GitOperations.readTree(gitdir, oid);

      for (const entry of tree.entries) {
        const path = prefix ? `${prefix}/${entry.path}` : entry.path;

        if (entry.type === 'tree') {
          yield* walk(entry.oid, path);
        } else if (await GitOperations.isTextFile(gitdir, entry)) {
          const type = GitOperations.mode2type(entry.mode);
          yield {
            name: entry.path,
            type,
            path,
            repoUri: uri,
            sha1: entry.oid
          };
        }
      }
    }

    return walk(treeId);
  }

  static async readTree(gitdir, oid) {
    const {
      type,
      object
    } = await _git_prime.GitPrime.readObject({
      gitdir,
      oid
    });

    if (type === 'commit') {
      return await this.readTree(gitdir, object.tree);
    } else if (type === 'tree') {
      const tree = object;
      return {
        entries: tree.entries,
        gitdir,
        oid
      };
    } else {
      throw new Error(`${oid} is not a tree`);
    }
  }

  static mode2type(mode) {
    switch (mode) {
      case '100755':
      case '100644':
        return _model.FileTreeItemType.File;

      case '120000':
        return _model.FileTreeItemType.Link;

      case '40000':
      case '040000':
        return _model.FileTreeItemType.Directory;

      case '160000':
        return _model.FileTreeItemType.Submodule;

      default:
        throw new Error('unknown mode: ' + mode);
    }
  }

  async iterateCommits(uri, startRevision, untilRevision) {
    const repository = await this.openRepo(uri);
    const commit = await this.getCommit(uri, startRevision);
    const revWalk = repository.createRevWalk();
    revWalk.sorting(_nodegit.Revwalk.SORT.TOPOLOGICAL);
    revWalk.push(commit.id());
    const commits = await revWalk.getCommitsUntil(c => {
      // Iterate commits all the way to the oldest one.
      return true;
    });
    const res = commits.map(c => {
      return {
        repoUri: uri,
        id: c.sha(),
        message: c.message(),
        body: c.body(),
        date: c.date(),
        parents: c.parents().map(i => i.tostrS()),
        author: {
          name: c.author().name(),
          email: c.author().email()
        },
        committer: {
          name: c.committer().name(),
          email: c.committer().email()
        }
      };
    });
    return res;
  }
  /**
   * Return a fileTree structure by walking the repo file tree.
   * @param uri the repo uri
   * @param path the start path
   * @param revision the revision
   * @param skip pagination parameter, skip how many nodes in each children.
   * @param limit pagination parameter, limit the number of node's children.
   * @param resolveParents whether the return value should always start from root
   * @param flatten
   */


  async fileTree(uri, path, revision = HEAD, skip = 0, limit = DEFAULT_TREE_CHILDREN_LIMIT, resolveParents = false, flatten = false) {
    const commit = await this.getCommitOr404(uri, revision);
    const gitdir = this.repoDir(uri);

    if (path.startsWith('/')) {
      path = path.slice(1);
    }

    if (path.endsWith('/')) {
      path = path.slice(0, -1);
    }

    function type2item(type) {
      switch (type) {
        case 'blob':
          return _model.FileTreeItemType.File;

        case 'tree':
          return _model.FileTreeItemType.Directory;

        case 'commit':
          return _model.FileTreeItemType.Submodule;

        default:
          throw new Error(`unsupported file tree item type ${type}`);
      }
    }

    if (resolveParents) {
      const root = {
        name: '',
        path: '',
        type: _model.FileTreeItemType.Directory
      };
      const tree = await GitOperations.readTree(gitdir, commit.treeId);
      await this.fillChildren(root, tree, {
        skip,
        limit,
        flatten
      });

      if (path) {
        await this.resolvePath(root, tree, path.split('/'), {
          skip,
          limit,
          flatten
        });
      }

      return root;
    } else {
      const obj = await _git_prime.GitPrime.readObject({
        gitdir,
        oid: commit.id,
        filepath: path
      });
      const result = {
        name: path.split('/').pop() || '',
        path,
        type: type2item(obj.type),
        sha1: obj.oid
      };

      if (result.type === _model.FileTreeItemType.Directory) {
        await this.fillChildren(result, {
          gitdir,
          entries: obj.object.entries,
          oid: obj.oid
        }, {
          skip,
          limit,
          flatten
        });
      }

      return result;
    }
  }

  async fillChildren(result, {
    entries,
    gitdir
  }, {
    skip,
    limit,
    flatten
  }) {
    result.childrenCount = entries.length;
    result.children = [];

    for (const e of entries.slice(skip, Math.min(entries.length, skip + limit))) {
      const child = entry2Tree(e, result.path);
      result.children.push(child);

      if (flatten && child.type === _model.FileTreeItemType.Directory) {
        const tree = await GitOperations.readTree(gitdir, e.oid);

        if (tree.entries.length === 1) {
          await this.fillChildren(child, tree, {
            skip,
            limit,
            flatten
          });
        }
      }
    }
  }

  async resolvePath(result, tree, paths, opt) {
    const [path, ...rest] = paths;

    for (const entry of tree.entries) {
      if (entry.path === path) {
        if (!result.children) {
          result.children = [];
        }

        const child = entry2Tree(entry, result.path);
        const idx = result.children.findIndex(i => i.sha1 === entry.oid);

        if (idx < 0) {
          result.children.push(child);
        } else {
          result.children[idx] = child;
        }

        if (entry.type === 'tree') {
          const subTree = await GitOperations.readTree(tree.gitdir, entry.oid);
          await this.fillChildren(child, subTree, opt);

          if (rest.length > 0) {
            await this.resolvePath(child, subTree, rest, opt);
          }
        }
      }
    }
  }

  async getCommitDiff(uri, revision) {
    const repo = await this.openRepo(uri);
    const commit = await this.getCommit(uri, revision);
    const diffs = await commit.getDiff();
    const commitDiff = {
      commit: commitInfo(commit),
      additions: 0,
      deletions: 0,
      files: []
    };

    for (const diff of diffs) {
      const patches = await diff.patches();

      for (const patch of patches) {
        const {
          total_deletions,
          total_additions
        } = patch.lineStats();
        commitDiff.additions += total_additions;
        commitDiff.deletions += total_deletions;

        if (patch.isAdded()) {
          const path = patch.newFile().path();
          const modifiedCode = await this.getModifiedCode(commit, path);
          const language = await (0, _detect_language.detectLanguage)(path, modifiedCode);
          commitDiff.files.push({
            language,
            path,
            modifiedCode,
            additions: total_additions,
            deletions: total_deletions,
            kind: _git_diff.DiffKind.ADDED
          });
        } else if (patch.isDeleted()) {
          const path = patch.oldFile().path();
          const originCode = await this.getOriginCode(commit, repo, path);
          const language = await (0, _detect_language.detectLanguage)(path, originCode);
          commitDiff.files.push({
            language,
            path,
            originCode,
            kind: _git_diff.DiffKind.DELETED,
            additions: total_additions,
            deletions: total_deletions
          });
        } else if (patch.isModified()) {
          const path = patch.newFile().path();
          const modifiedCode = await this.getModifiedCode(commit, path);
          const originPath = patch.oldFile().path();
          const originCode = await this.getOriginCode(commit, repo, originPath);
          const language = await (0, _detect_language.detectLanguage)(patch.newFile().path(), modifiedCode);
          commitDiff.files.push({
            language,
            path,
            originPath,
            originCode,
            modifiedCode,
            kind: _git_diff.DiffKind.MODIFIED,
            additions: total_additions,
            deletions: total_deletions
          });
        } else if (patch.isRenamed()) {
          const path = patch.newFile().path();
          commitDiff.files.push({
            path,
            originPath: patch.oldFile().path(),
            kind: _git_diff.DiffKind.RENAMED,
            additions: total_additions,
            deletions: total_deletions
          });
        }
      }
    }

    return commitDiff;
  }

  async getDiff(uri, oldRevision, newRevision) {
    const repo = await this.openRepo(uri);
    const oldCommit = await this.getCommit(uri, oldRevision);
    const newCommit = await this.getCommit(uri, newRevision);
    const oldTree = await oldCommit.getTree();
    const newTree = await newCommit.getTree();
    const diff = await _nodegit.Diff.treeToTree(repo, oldTree, newTree);
    const res = {
      additions: 0,
      deletions: 0,
      files: []
    };
    const patches = await diff.patches();

    for (const patch of patches) {
      const {
        total_deletions,
        total_additions
      } = patch.lineStats();
      res.additions += total_additions;
      res.deletions += total_deletions;

      if (patch.isAdded()) {
        const path = patch.newFile().path();
        res.files.push({
          path,
          additions: total_additions,
          deletions: total_deletions,
          kind: _git_diff.DiffKind.ADDED
        });
      } else if (patch.isDeleted()) {
        const path = patch.oldFile().path();
        res.files.push({
          path,
          kind: _git_diff.DiffKind.DELETED,
          additions: total_additions,
          deletions: total_deletions
        });
      } else if (patch.isModified()) {
        const path = patch.newFile().path();
        const originPath = patch.oldFile().path();
        res.files.push({
          path,
          originPath,
          kind: _git_diff.DiffKind.MODIFIED,
          additions: total_additions,
          deletions: total_deletions
        });
      } else if (patch.isRenamed()) {
        const path = patch.newFile().path();
        res.files.push({
          path,
          originPath: patch.oldFile().path(),
          kind: _git_diff.DiffKind.RENAMED,
          additions: total_additions,
          deletions: total_deletions
        });
      }
    }

    return res;
  }

  async getOriginCode(commit, repo, path) {
    for (const oid of commit.parents()) {
      const parentCommit = await repo.getCommit(oid);

      if (parentCommit) {
        const entry = await parentCommit.getEntry(path);

        if (entry) {
          return (await entry.getBlob()).content().toString('utf8');
        }
      }
    }

    return '';
  }

  async getModifiedCode(commit, path) {
    const entry = await commit.getEntry(path);
    return (await entry.getBlob()).content().toString('utf8');
  }

  async findCommit(repo, oid) {
    try {
      return repo.getCommit(_nodegit.Oid.fromString(oid));
    } catch (e) {
      return null;
    }
  }

  async getBranchAndTags(repoUri) {
    const gitdir = this.repoDir(repoUri);
    const remoteBranches = await _git_prime.GitPrime.listBranches({
      gitdir,
      remote: 'origin'
    });
    const results = [];

    for (const name of remoteBranches) {
      const reference = `refs/remotes/origin/${name}`;
      const commit = await this.getCommitInfo(repoUri, reference);

      if (commit) {
        results.push({
          name,
          reference,
          type: _commit.ReferenceType.REMOTE_BRANCH,
          commit
        });
      }
    }

    const tags = await _git_prime.GitPrime.listTags({
      gitdir
    });

    for (const name of tags) {
      const reference = `refs/tags/${name}`;
      const commit = await this.getCommitInfo(repoUri, reference);

      if (commit) {
        results.push({
          name,
          reference,
          type: _commit.ReferenceType.TAG,
          commit
        });
      }
    }

    return results;
  }

  async getCommitOr404(repoUri, ref) {
    const commit = await this.getCommitInfo(repoUri, ref);

    if (!commit) {
      throw _boom.default.notFound(`repo ${repoUri} or ${ref} not found`);
    }

    return commit;
  }

  async getCommitInfo(repoUri, ref) {
    const gitdir = this.repoDir(repoUri); // depth: avoid infinite loop

    let obj = null;
    let oid = '';

    if (/^[0-9a-f]{5,40}$/.test(ref)) {
      // it's possible ref is sha-1 object id
      try {
        oid = ref;

        if (oid.length < 40) {
          oid = await _git_prime.GitPrime.expandOid({
            gitdir,
            oid
          });
        }

        obj = await _git_prime.GitPrime.readObject({
          gitdir,
          oid,
          format: 'parsed'
        });
      } catch (e) {// expandOid or readObject failed
      }
    } // test if it is a reference


    if (!obj) {
      try {
        // try local branches or tags
        oid = await _git_prime.GitPrime.resolveRef({
          gitdir,
          ref,
          depth: 10
        });
      } catch (e) {
        // try remote branches
        try {
          oid = await _git_prime.GitPrime.resolveRef({
            gitdir,
            ref: `origin/${ref}`,
            depth: 10
          });
        } catch (e1) {// no match
        }
      }

      if (oid) {
        obj = await _git_prime.GitPrime.readObject({
          gitdir,
          oid,
          format: 'parsed'
        });
      }
    }

    if (obj) {
      if (obj.type === 'commit') {
        const commit = obj.object;
        return {
          id: obj.oid,
          author: commit.author.name,
          committer: commit.committer.name,
          message: commit.message,
          updated: new Date(commit.committer.timestamp * 1000),
          parents: commit.parent,
          treeId: commit.tree
        };
      } else if (obj.type === 'tag') {
        const tag = obj.object;

        if (tag.type === 'commit') {
          return await this.getCommitInfo(repoUri, tag.object);
        }
      }
    }

    return null;
  }

}

exports.GitOperations = GitOperations;

function commitInfo(commit) {
  return {
    updated: commit.date(),
    message: commit.message(),
    author: commit.author().name(),
    committer: commit.committer().name(),
    id: commit.sha().substr(0, 7),
    parents: commit.parents().map(oid => oid.toString().substring(0, 7)),
    treeId: commit.treeId().tostrS()
  };
}