"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JavaLauncher = void 0;

var _child_process = require("child_process");

var _fs = require("fs");

var _getPort = _interopRequireDefault(require("get-port"));

var glob = _interopRequireWildcard(require("glob"));

var _os = require("os");

var _path = _interopRequireDefault(require("path"));

var _abstract_launcher = require("./abstract_launcher");

var _request_expander = require("./request_expander");

var _external_program = require("./process/external_program");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const JAVA_LANG_DETACH_PORT = 2090;

class JavaLauncher extends _abstract_launcher.AbstractLauncher {
  constructor(targetHost, options, loggerFactory) {
    super('java', targetHost, options, loggerFactory);
    this.targetHost = targetHost;
    this.options = options;
    this.loggerFactory = loggerFactory;

    _defineProperty(this, "needModuleArguments", true);
  }

  createExpander(proxy, builtinWorkspace, maxWorkspace) {
    return new _request_expander.RequestExpander(proxy, builtinWorkspace, maxWorkspace, this.options, {
      initialOptions: {
        settings: {
          'java.import.gradle.enabled': this.options.security.enableGradleImport,
          'java.import.maven.enabled': this.options.security.enableMavenImport,
          'java.autobuild.enabled': false
        }
      },
      clientCapabilities: {
        textDocument: {
          documentSymbol: {
            hierarchicalDocumentSymbolSupport: true
          }
        }
      }
    }, this.log);
  }

  startConnect(proxy) {
    proxy.startServerConnection();
  }

  async getPort() {
    if (!this.options.lsp.detach) {
      return await (0, _getPort.default)();
    }

    return JAVA_LANG_DETACH_PORT;
  }

  async getJavaHome(installationPath, log) {
    function findJDK(platform) {
      const JDKFound = glob.sync(`**/jdks/*${platform}/jdk-*`, {
        cwd: installationPath
      });

      if (!JDKFound.length) {
        log.error('Cannot find Java Home in Bundle installation for ' + platform);
        return undefined;
      }

      return _path.default.resolve(installationPath, JDKFound[0]);
    }

    let bundledJavaHome; // detect platform

    const osPlatform = (0, _os.platform)();

    switch (osPlatform) {
      case 'darwin':
        bundledJavaHome = `${findJDK('osx')}/Contents/Home`;
        break;

      case 'win32':
        bundledJavaHome = `${findJDK('windows')}`;
        break;

      case 'freebsd':
      case 'linux':
        bundledJavaHome = `${findJDK('linux')}`;
        break;

      default:
        log.error('No Bundle JDK defined ' + osPlatform);
    }

    if (this.getSystemJavaHome()) {
      const javaHomePath = this.getSystemJavaHome();
      const javaVersion = await this.getJavaVersion(javaHomePath);

      if (javaVersion > 8) {
        // for JDK's versiob > 8, we need extra arguments as default
        return javaHomePath;
      } else if (javaVersion === 8) {
        // JDK's version = 8, needn't extra arguments
        this.needModuleArguments = false;
        return javaHomePath;
      } else {// JDK's version < 8, use bundled JDK instead, whose version > 8, so need extra arguments as default
      }
    }

    return bundledJavaHome;
  }

  async spawnProcess(installationPath, port, log) {
    const launchersFound = glob.sync('**/plugins/org.eclipse.equinox.launcher_*.jar', {
      cwd: installationPath
    });

    if (!launchersFound.length) {
      throw new Error('Cannot find language server jar file');
    }

    const javaHomePath = await this.getJavaHome(installationPath, log);

    if (!javaHomePath) {
      throw new Error('Cannot find Java Home');
    }

    const javaPath = _path.default.resolve(javaHomePath, 'bin', process.platform === 'win32' ? 'java.exe' : 'java');

    const configPath = process.platform === 'win32' ? _path.default.resolve(installationPath, 'repository/config_win') : this.options.jdtConfigPath;
    const params = ['-Declipse.application=org.elastic.jdt.ls.core.id1', '-Dosgi.bundles.defaultStartLevel=4', '-Declipse.product=org.elastic.jdt.ls.core.product', '-Dlog.level=ALL', '-Dfile.encoding=utf8', '-noverify', '-Xmx4G', '-jar', _path.default.resolve(installationPath, launchersFound[0]), '-configuration', configPath, '-data', this.options.jdtWorkspacePath];

    if (this.needModuleArguments) {
      params.push('--add-modules=ALL-SYSTEM', '--add-opens', 'java.base/java.util=ALL-UNNAMED', '--add-opens', 'java.base/java.lang=ALL-UNNAMED');
    }

    const p = (0, _child_process.spawn)(javaPath, params, {
      detached: false,
      stdio: 'pipe',
      env: { ...process.env,
        CLIENT_HOST: '127.0.0.1',
        CLIENT_PORT: port.toString(),
        JAVA_HOME: javaHomePath
      }
    });
    p.stdout.on('data', data => {
      log.stdout(data.toString());
    });
    p.stderr.on('data', data => {
      log.stderr(data.toString());
    });
    log.info(`Launch Java Language Server at port ${port.toString()}, pid:${p.pid}, JAVA_HOME:${javaHomePath}`);
    return new _external_program.ExternalProgram(p, this.options, log);
  } // TODO(pcxu): run /usr/libexec/java_home to get all java homes for macOS


  getSystemJavaHome() {
    let javaHome = process.env.JDK_HOME;

    if (!javaHome) {
      javaHome = process.env.JAVA_HOME;
    }

    if (javaHome) {
      javaHome = this.expandHomeDir(javaHome);
      const JAVAC_FILENAME = 'javac' + (process.platform === 'win32' ? '.exe' : '');

      if ((0, _fs.existsSync)(javaHome) && (0, _fs.existsSync)(_path.default.resolve(javaHome, 'bin', JAVAC_FILENAME))) {
        return javaHome;
      }
    }

    return '';
  }

  getJavaVersion(javaHome) {
    return new Promise((resolve, reject) => {
      (0, _child_process.execFile)(_path.default.resolve(javaHome, 'bin', process.platform === 'win32' ? 'java.exe' : 'java'), ['-version'], {}, (error, stdout, stderr) => {
        const javaVersion = this.parseMajorVersion(stderr);
        resolve(javaVersion);
      });
    });
  }

  parseMajorVersion(content) {
    let regexp = /version "(.*)"/g;
    let match = regexp.exec(content);

    if (!match) {
      return 0;
    }

    let version = match[1];

    if (version.startsWith('1.')) {
      version = version.substring(2);
    }

    regexp = /\d+/g;
    match = regexp.exec(version);
    let javaVersion = 0;

    if (match) {
      javaVersion = parseInt(match[0], 10);
    }

    return javaVersion;
  }

  expandHomeDir(javaHome) {
    const homeDir = process.env[process.platform === 'win32' ? 'USERPROFILE' : 'HOME'];

    if (!javaHome) {
      return javaHome;
    }

    if (javaHome === '~') {
      return homeDir;
    }

    if (javaHome.slice(0, 2) !== '~/') {
      return javaHome;
    }

    return _path.default.join(homeDir, javaHome.slice(2));
  }

}

exports.JavaLauncher = JavaLauncher;