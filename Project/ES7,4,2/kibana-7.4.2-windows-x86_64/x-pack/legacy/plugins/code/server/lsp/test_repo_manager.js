"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TestRepoManager = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _nodegit = _interopRequireDefault(require("@elastic/nodegit"));

var _rimraf = _interopRequireDefault(require("rimraf"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class TestRepoManager {
  constructor(testConfig) {
    _defineProperty(this, "repos", void 0);

    this.repos = testConfig.repos;
  }

  async importAllRepos() {
    for (const repo of this.repos) {
      await this.importRepo(repo.url, repo.path);
    }
  }

  importRepo(url, path) {
    return new Promise(resolve => {
      if (!_fs.default.existsSync(path)) {
        (0, _rimraf.default)(path, error => {
          console.log(`begin to import ${url} to ${path}`);

          _nodegit.default.Clone.clone(url, path, {
            fetchOpts: {
              callbacks: {
                certificateCheck: () => 0
              }
            }
          }).then(repo => {
            console.log(`import ${url} done`);
            resolve(repo);
          });
        });
      } else {
        resolve();
      }
    });
  }

  async cleanAllRepos() {
    this.repos.forEach(repo => {
      this.cleanRepo(repo.path);
    });
  }

  async cleanRepo(path) {
    return new Promise(resolve => {
      if (_fs.default.existsSync(path)) {
        (0, _rimraf.default)(path, resolve);
      } else {
        resolve(true);
      }
    });
  }

  getRepo(language) {
    return this.repos.filter(repo => {
      return repo.language === language;
    })[0];
  }

}

exports.TestRepoManager = TestRepoManager;