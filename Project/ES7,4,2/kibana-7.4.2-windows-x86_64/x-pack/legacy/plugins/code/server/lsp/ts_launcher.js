"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TypescriptServerLauncher = void 0;

var _child_process = require("child_process");

var _getPort = _interopRequireDefault(require("get-port"));

var _path = require("path");

var _abstract_launcher = require("./abstract_launcher");

var _request_expander = require("./request_expander");

var _external_program = require("./process/external_program");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const TS_LANG_DETACH_PORT = 2089;

class TypescriptServerLauncher extends _abstract_launcher.AbstractLauncher {
  constructor(targetHost, options, loggerFactory) {
    super('typescript', targetHost, options, loggerFactory);
    this.targetHost = targetHost;
    this.options = options;
    this.loggerFactory = loggerFactory;

    _defineProperty(this, "startupTimeout", 5000);
  }

  async getPort() {
    if (!this.options.lsp.detach) {
      return await (0, _getPort.default)();
    }

    return TS_LANG_DETACH_PORT;
  }

  createExpander(proxy, builtinWorkspace, maxWorkspace) {
    return new _request_expander.RequestExpander(proxy, builtinWorkspace, maxWorkspace, this.options, {
      initialOptions: {
        installNodeDependency: this.options.security.installNodeDependency,
        gitHostWhitelist: this.options.security.gitHostWhitelist
      }
    }, this.log);
  }

  async spawnProcess(installationPath, port, log) {
    const p = (0, _child_process.spawn)(process.execPath, [installationPath, '-p', port.toString(), '-c', '1'], {
      detached: false,
      stdio: 'pipe',
      cwd: (0, _path.resolve)(installationPath, '../..')
    });
    p.stdout.on('data', data => {
      log.stdout(data.toString());
    });
    p.stderr.on('data', data => {
      log.stderr(data.toString());
    });
    return new _external_program.ExternalProgram(p, this.options, log);
  }

}

exports.TypescriptServerLauncher = TypescriptServerLauncher;