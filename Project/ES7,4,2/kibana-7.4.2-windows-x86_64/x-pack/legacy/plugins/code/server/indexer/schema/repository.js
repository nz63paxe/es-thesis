"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositorySearchIndexWithScope = exports.RepositoryIndexName = exports.RepositoryIndexNamePrefix = exports.RepositoryAnalysisSettings = exports.RepositorySchema = void 0;

var _document = require("./document");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const RepositorySchema = _document.DocumentSchema;
exports.RepositorySchema = RepositorySchema;
const RepositoryAnalysisSettings = _document.DocumentAnalysisSettings;
exports.RepositoryAnalysisSettings = RepositoryAnalysisSettings;
const RepositoryIndexNamePrefix = _document.DocumentIndexNamePrefix;
exports.RepositoryIndexNamePrefix = RepositoryIndexNamePrefix;
const RepositoryIndexName = _document.DocumentIndexName;
exports.RepositoryIndexName = RepositoryIndexName;
const RepositorySearchIndexWithScope = _document.DocumentSearchIndexWithScope;
exports.RepositorySearchIndexWithScope = RepositorySearchIndexWithScope;