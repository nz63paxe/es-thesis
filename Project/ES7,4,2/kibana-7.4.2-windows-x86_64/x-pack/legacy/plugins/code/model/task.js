"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TaskType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Time consuming task that should be queued and executed seperately */
let TaskType;
exports.TaskType = TaskType;

(function (TaskType) {
  TaskType[TaskType["Import"] = 0] = "Import";
  TaskType[TaskType["Update"] = 1] = "Update";
  TaskType[TaskType["Delete"] = 2] = "Delete";
  TaskType[TaskType["Index"] = 3] = "Index";
})(TaskType || (exports.TaskType = TaskType = {}));