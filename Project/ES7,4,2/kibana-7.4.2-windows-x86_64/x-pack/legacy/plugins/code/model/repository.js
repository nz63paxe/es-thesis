"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortFileTree = sortFileTree;
exports.WorkerReservedProgress = exports.IndexStatsKey = exports.FileTreeItemType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function sortFileTree(a, b) {
  // consider Link and File are the same type, Submodule and Directory are the same type when sorting.
  // Submodule and Directory are before Link and File
  const types1 = [FileTreeItemType.File, FileTreeItemType.Link];
  const types2 = [FileTreeItemType.Directory, FileTreeItemType.Submodule];

  if (types1.includes(a.type)) {
    if (types1.includes(b.type)) {
      return a.name.localeCompare(b.name);
    } else {
      return 1;
    }
  } else if (types2.includes(a.type)) {
    if (types2.includes(b.type)) {
      return a.name.localeCompare(b.name);
    } else {
      return -1;
    }
  }

  return a.name.localeCompare(b.name);
}

let FileTreeItemType;
exports.FileTreeItemType = FileTreeItemType;

(function (FileTreeItemType) {
  FileTreeItemType[FileTreeItemType["File"] = 0] = "File";
  FileTreeItemType[FileTreeItemType["Directory"] = 1] = "Directory";
  FileTreeItemType[FileTreeItemType["Submodule"] = 2] = "Submodule";
  FileTreeItemType[FileTreeItemType["Link"] = 3] = "Link";
})(FileTreeItemType || (exports.FileTreeItemType = FileTreeItemType = {}));

let IndexStatsKey;
exports.IndexStatsKey = IndexStatsKey;

(function (IndexStatsKey) {
  IndexStatsKey["Commit"] = "commit-added-count";
  IndexStatsKey["CommitDeleted"] = "commit-deleted-count";
  IndexStatsKey["File"] = "file-added-count";
  IndexStatsKey["FileDeleted"] = "file-deleted-count";
  IndexStatsKey["Symbol"] = "symbol-added-count";
  IndexStatsKey["SymbolDeleted"] = "symbol-deleted-count";
  IndexStatsKey["Reference"] = "reference-added-count";
  IndexStatsKey["ReferenceDeleted"] = "reference-deleted-count";
})(IndexStatsKey || (exports.IndexStatsKey = IndexStatsKey = {}));

let WorkerReservedProgress;
exports.WorkerReservedProgress = WorkerReservedProgress;

(function (WorkerReservedProgress) {
  WorkerReservedProgress[WorkerReservedProgress["INIT"] = 0] = "INIT";
  WorkerReservedProgress[WorkerReservedProgress["COMPLETED"] = 100] = "COMPLETED";
  WorkerReservedProgress[WorkerReservedProgress["ERROR"] = -100] = "ERROR";
  WorkerReservedProgress[WorkerReservedProgress["TIMEOUT"] = -200] = "TIMEOUT";
})(WorkerReservedProgress || (exports.WorkerReservedProgress = WorkerReservedProgress = {}));