"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReferenceType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let ReferenceType;
exports.ReferenceType = ReferenceType;

(function (ReferenceType) {
  ReferenceType["BRANCH"] = "BRANCH";
  ReferenceType["TAG"] = "TAG";
  ReferenceType["REMOTE_BRANCH"] = "REMOTE_BRANCH";
  ReferenceType["OTHER"] = "OTHER";
})(ReferenceType || (exports.ReferenceType = ReferenceType = {}));