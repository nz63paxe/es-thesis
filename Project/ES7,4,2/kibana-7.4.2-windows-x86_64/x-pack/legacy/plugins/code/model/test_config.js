"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RequestType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let RequestType;
exports.RequestType = RequestType;

(function (RequestType) {
  RequestType[RequestType["INITIALIZE"] = 0] = "INITIALIZE";
  RequestType[RequestType["HOVER"] = 1] = "HOVER";
  RequestType[RequestType["FULL"] = 2] = "FULL";
})(RequestType || (exports.RequestType = RequestType = {}));