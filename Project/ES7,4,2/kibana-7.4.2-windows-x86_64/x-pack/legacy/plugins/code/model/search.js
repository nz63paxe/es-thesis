"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchScope = exports.IndexerType = void 0;

var _model = require("../model");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let IndexerType; // The request for LspIndexer

exports.IndexerType = IndexerType;

(function (IndexerType) {
  IndexerType["LSP"] = "LSP";
  IndexerType["LSP_INC"] = "LSP_INCREMENTAL";
  IndexerType["COMMIT"] = "COMMIT";
  IndexerType["COMMIT_INC"] = "COMMIT_INCREMENTAL";
  IndexerType["REPOSITORY"] = "REPOSITORY";
  IndexerType["UNKNOWN"] = "UNKNOWN";
})(IndexerType || (exports.IndexerType = IndexerType = {}));

let SearchScope;
exports.SearchScope = SearchScope;

(function (SearchScope) {
  SearchScope["DEFAULT"] = "default";
  SearchScope["SYMBOL"] = "symbol";
  SearchScope["REPOSITORY"] = "repository";
  SearchScope["FILE"] = "file";
})(SearchScope || (exports.SearchScope = SearchScope = {}));