"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeUIUsageMetrics = exports.CodeUsageMetrics = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let CodeUsageMetrics;
exports.CodeUsageMetrics = CodeUsageMetrics;

(function (CodeUsageMetrics) {
  CodeUsageMetrics["ENABLED"] = "enabled";
  CodeUsageMetrics["REPOSITORIES"] = "repositories";
  CodeUsageMetrics["LANGUAGE_SERVERS"] = "langserver";
})(CodeUsageMetrics || (exports.CodeUsageMetrics = CodeUsageMetrics = {}));

let CodeUIUsageMetrics;
exports.CodeUIUsageMetrics = CodeUIUsageMetrics;

(function (CodeUIUsageMetrics) {
  CodeUIUsageMetrics["ADMIN_PAGE_LOAD_COUNT"] = "adminPageLoadCount";
  CodeUIUsageMetrics["SOURCE_VIEW_PAGE_LOAD_COUNT"] = "sourceViewPageLoadCount";
  CodeUIUsageMetrics["SEARCH_PAGE_LOAD_COUNT"] = "searchPageLoadCount";
  CodeUIUsageMetrics["FILE_TREE_CLICK_COUNT"] = "fileTreeClickCount";
  CodeUIUsageMetrics["BREADCRUMB_CLICK_COUNT"] = "breadcrumbClickCount";
  CodeUIUsageMetrics["LINE_NUMBER_CLICK_COUNT"] = "lineNumberClickCount";
  CodeUIUsageMetrics["STRUCTURE_TREE_CLICK_COUNT"] = "structureTreeClickCount";
  CodeUIUsageMetrics["LSP_DATA_AVAILABLE_PAGE_VIEW_COUNT"] = "lspDataAvailablePageViewCount";
})(CodeUIUsageMetrics || (exports.CodeUIUsageMetrics = CodeUIUsageMetrics = {}));