"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.binaryInfo = binaryInfo;

var _binary_info = _interopRequireDefault(require("@elastic/nodegit/dist/utils/binary_info"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/* eslint-disable @typescript-eslint/camelcase */
// @ts-ignore
function binaryInfo(platform, arch) {
  const info = (0, _binary_info.default)(platform, arch);
  const downloadUrl = info.hosted_tarball;
  const packageName = info.package_name;
  return {
    downloadUrl,
    packageName
  };
}