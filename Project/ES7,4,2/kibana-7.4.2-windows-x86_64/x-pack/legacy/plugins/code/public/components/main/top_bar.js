"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TopBar = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _uri_util = require("../../../common/uri_util");

var _breadcrumb = require("./breadcrumb");

var _search_bar = require("../search_bar");

var _status_indicator = require("../status_indicator/status_indicator");

var _branch_selector = require("./branch_selector");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TopBar =
/*#__PURE__*/
function (_React$Component) {
  _inherits(TopBar, _React$Component);

  function TopBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, TopBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(TopBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      value: (0, _uri_util.decodeRevisionString)(_this.props.routeParams.revision)
    });

    _defineProperty(_assertThisInitialized(_this), "getBranch", function (revision) {
      var r = (0, _uri_util.decodeRevisionString)(revision);

      if (r.toUpperCase() === 'HEAD' && _this.props.currentRepository) {
        return _this.props.currentRepository.defaultBranch;
      }

      if (_this.props.branches.length === 0 && _this.props.tags.length === 0) {
        return r;
      }

      var branch = _this.props.branches.find(function (b) {
        return b.name === r;
      });

      var tag = _this.props.tags.find(function (b) {
        return b.name === r;
      });

      if (branch) {
        return branch.name;
      } else if (tag) {
        return tag.name;
      } else {
        return "tree: ".concat(r);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "getHrefFromRevision", function (r) {
      var _this$props$routePara = _this.props.routeParams,
          resource = _this$props$routePara.resource,
          org = _this$props$routePara.org,
          repo = _this$props$routePara.repo,
          _this$props$routePara2 = _this$props$routePara.path,
          path = _this$props$routePara2 === void 0 ? '' : _this$props$routePara2,
          pathType = _this$props$routePara.pathType;
      return "#/".concat(resource, "/").concat(org, "/").concat(repo, "/").concat(pathType, "/").concat(r, "/").concat(path);
    });

    return _this;
  }

  _createClass(TopBar, [{
    key: "render",
    value: function render() {
      var branch = this.branch;
      return _react.default.createElement("div", {
        className: "code-top-bar__container"
      }, _react.default.createElement(_search_bar.SearchBar, {
        query: this.props.query,
        onSearchScopeChanged: this.props.onSearchScopeChanged,
        enableSubmitWhenOptionsChanged: false,
        searchOptions: this.props.searchOptions
      }), _react.default.createElement(_eui.EuiFlexGroup, {
        gutterSize: "none",
        justifyContent: "spaceBetween",
        className: "codeTopBar__toolbar"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        className: "codeTopBar__left"
      }, _react.default.createElement(_eui.EuiFlexGroup, {
        gutterSize: "l",
        alignItems: "center"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        className: "codeContainer__select",
        grow: false
      }, _react.default.createElement(_branch_selector.BranchSelector, {
        getHrefFromRevision: this.getHrefFromRevision,
        branches: this.props.branches,
        tags: this.props.tags,
        revision: branch
      })), _react.default.createElement(_breadcrumb.Breadcrumb, {
        routeParams: this.props.routeParams
      }), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_status_indicator.StatusIndicator, null)))), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, this.props.buttons)));
    }
  }, {
    key: "branch",
    get: function get() {
      return this.getBranch(this.state.value);
    }
  }, {
    key: "branchOptions",
    get: function get() {
      var _this2 = this;

      return this.props.branches.map(function (b) {
        return _defineProperty({
          value: b.name,
          text: b.name
        }, 'data-test-subj', "codeBranchSelectOption-".concat(b.name).concat(_this2.branch === b.name ? 'Active' : ''));
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props) {
      return {
        value: (0, _uri_util.decodeRevisionString)(props.routeParams.revision)
      };
    }
  }]);

  return TopBar;
}(_react.default.Component);

exports.TopBar = TopBar;