"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _app = require("./app");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Plugin =
/*#__PURE__*/
function () {
  function Plugin(initializerContext) {
    _classCallCheck(this, Plugin);
  }

  _createClass(Plugin, [{
    key: "setup",
    value: function setup(core) {// called when plugin is setting up
    }
  }, {
    key: "start",
    value: function start(core) {
      // called after all plugins are set up
      (0, _app.startApp)(core);
    }
  }, {
    key: "stop",
    value: function stop() {// called when plugin is torn down, aka window.onbeforeunload
    }
  }]);

  return Plugin;
}();

exports.Plugin = Plugin;