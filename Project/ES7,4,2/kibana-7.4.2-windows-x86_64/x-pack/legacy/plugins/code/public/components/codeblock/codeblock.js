"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeBlock = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _resize_checker = require("ui/resize_checker");

var _monaco = require("../../monaco/monaco");

var _single_selection_helper = require("../../monaco/single_selection_helper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CodeBlock =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeBlock, _React$PureComponent);

  function CodeBlock() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeBlock);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeBlock)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "el", null);

    _defineProperty(_assertThisInitialized(_this), "ed", void 0);

    _defineProperty(_assertThisInitialized(_this), "resizeChecker", void 0);

    _defineProperty(_assertThisInitialized(_this), "currentHighlightDecorations", []);

    _defineProperty(_assertThisInitialized(_this), "lineNumbersFunc", function (line) {
      if (_this.props.lineNumbersFunc) {
        return _this.props.lineNumbersFunc(line);
      }

      return "".concat((_this.props.startLine || 0) + line);
    });

    return _this;
  }

  _createClass(CodeBlock, [{
    key: "componentDidMount",
    value: function () {
      var _componentDidMount = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var _this2 = this;

        var decorations;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!this.el) {
                  _context.next = 8;
                  break;
                }

                _context.next = 3;
                return this.tryLoadFile(this.props.code, this.props.language || 'text');

              case 3:
                this.ed.onMouseDown(function (e) {
                  if (_this2.props.onClick && (e.target.type === _monaco.monaco.editor.MouseTargetType.GUTTER_LINE_NUMBERS || e.target.type === _monaco.monaco.editor.MouseTargetType.CONTENT_TEXT)) {
                    var position = e.target.position || {
                      lineNumber: 0,
                      column: 0
                    };
                    var lineNumber = (_this2.props.startLine || 0) + position.lineNumber;

                    _this2.props.onClick({
                      lineNumber: lineNumber,
                      column: position.column
                    });
                  }
                });
                (0, _single_selection_helper.registerEditor)(this.ed);

                if (this.props.highlightRanges) {
                  decorations = this.props.highlightRanges.map(function (range) {
                    return {
                      range: range,
                      options: {
                        inlineClassName: 'codeSearch__highlight'
                      }
                    };
                  });
                  this.currentHighlightDecorations = this.ed.deltaDecorations([], decorations);
                }

                this.resizeChecker = new _resize_checker.ResizeChecker(this.el);
                this.resizeChecker.on('resize', function () {
                  setTimeout(function () {
                    _this2.ed.layout();
                  });
                });

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function componentDidMount() {
        return _componentDidMount.apply(this, arguments);
      }

      return componentDidMount;
    }()
  }, {
    key: "tryLoadFile",
    value: function () {
      var _tryLoadFile = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(code, language) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _monaco.monaco.editor.colorize(code, language, {});

              case 3:
                this.loadFile(code, language);
                _context2.next = 9;
                break;

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                this.loadFile(code);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 6]]);
      }));

      function tryLoadFile(_x, _x2) {
        return _tryLoadFile.apply(this, arguments);
      }

      return tryLoadFile;
    }()
  }, {
    key: "loadFile",
    value: function loadFile(code) {
      var language = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'text';
      this.ed = _monaco.monaco.editor.create(this.el, {
        value: code,
        language: language,
        lineNumbers: this.lineNumbersFunc.bind(this),
        readOnly: true,
        folding: this.props.folding,
        minimap: {
          enabled: false
        },
        scrollbar: {
          vertical: 'hidden',
          handleMouseWheel: false,
          verticalScrollbarSize: 0
        },
        hover: {
          enabled: false // disable default hover;

        },
        contextmenu: false,
        selectOnLineNumbers: false,
        selectionHighlight: false,
        renderLineHighlight: 'none',
        renderIndentGuides: false,
        automaticLayout: false
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps.code !== this.props.code || prevProps.highlightRanges !== this.props.highlightRanges) {
        if (this.ed) {
          var model = this.ed.getModel();

          if (model) {
            model.setValue(this.props.code);

            if (this.props.highlightRanges) {
              var decorations = this.props.highlightRanges.map(function (range) {
                return {
                  range: range,
                  options: {
                    inlineClassName: 'codeSearch__highlight'
                  }
                };
              });
              this.currentHighlightDecorations = this.ed.deltaDecorations(this.currentHighlightDecorations, decorations);
            }
          }
        }
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.ed) {
        this.ed.dispose();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var linesCount = this.props.code.split('\n').length;
      return _react.default.createElement(_eui.EuiPanel, {
        style: {
          marginBottom: '2rem'
        },
        paddingSize: "s"
      }, this.props.fileComponent, _react.default.createElement("div", {
        ref: function ref(r) {
          return _this3.el = r;
        },
        style: {
          height: linesCount * 18
        }
      }));
    }
  }]);

  return CodeBlock;
}(_react.default.PureComponent);

exports.CodeBlock = CodeBlock;