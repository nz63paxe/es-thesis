"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLspMethods = watchLspMethods;
exports.watchCloseReference = watchCloseReference;
exports.watchLoadRepo = watchLoadRepo;
exports.watchMainRouteChange = watchMainRouteChange;

var _querystring = _interopRequireDefault(require("querystring"));

var _new_platform = require("ui/new_platform");

var _url = _interopRequireDefault(require("url"));

var _effects = require("redux-saga/effects");

var _uri_util = require("../../common/uri_util");

var _actions = require("../actions");

var _status = require("../actions/status");

var _types = require("../common/types");

var _file_tree = require("../reducers/file_tree");

var _selectors = require("../selectors");

var _url2 = require("../utils/url");

var _patterns = require("./patterns");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleReferences),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(handleDefinitions),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLspMethods),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(handleClosePanel),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(watchCloseReference),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(handleReference),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(openDefinitions),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFile),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(loadRepoSaga),
    _marked10 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadRepo),
    _marked11 =
/*#__PURE__*/
regeneratorRuntime.mark(handleMainRouteChange),
    _marked12 =
/*#__PURE__*/
regeneratorRuntime.mark(watchMainRouteChange);

function handleReferences(action) {
  var params, _ref, title, files, repos;

  return regeneratorRuntime.wrap(function handleReferences$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          params = action.payload;
          _context.next = 4;
          return (0, _effects.call)(requestFindReferences, params);

        case 4:
          _ref = _context.sent;
          title = _ref.title;
          files = _ref.files;
          repos = Object.keys(files).map(function (repo) {
            return {
              repo: repo,
              files: files[repo]
            };
          });
          _context.next = 10;
          return (0, _effects.put)((0, _actions.findReferencesSuccess)({
            title: title,
            repos: repos
          }));

        case 10:
          _context.next = 16;
          break;

        case 12:
          _context.prev = 12;
          _context.t0 = _context["catch"](0);
          _context.next = 16;
          return (0, _effects.put)((0, _actions.findReferencesFailed)(_context.t0));

        case 16:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 12]]);
}

function handleDefinitions(action) {
  var _params, _ref2, _title, _files, _repos;

  return regeneratorRuntime.wrap(function handleDefinitions$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _params = action.payload;
          _context2.next = 4;
          return (0, _effects.call)(requestFindDefinitions, _params);

        case 4:
          _ref2 = _context2.sent;
          _title = _ref2.title;
          _files = _ref2.files;
          _repos = Object.keys(_files).map(function (repo) {
            return {
              repo: repo,
              files: _files[repo]
            };
          });
          _context2.next = 10;
          return (0, _effects.put)((0, _actions.findDefinitionsSuccess)({
            title: _title,
            repos: _repos
          }));

        case 10:
          _context2.next = 16;
          break;

        case 12:
          _context2.prev = 12;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 16;
          return (0, _effects.put)((0, _actions.findDefinitionsFailed)(_context2.t0));

        case 16:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 12]]);
}

function requestFindReferences(params) {
  return _new_platform.npStart.core.http.post("/api/code/lsp/findReferences", {
    body: JSON.stringify(params)
  });
}

function requestFindDefinitions(params) {
  return _new_platform.npStart.core.http.post("/api/code/lsp/findDefinitions", {
    body: JSON.stringify(params)
  });
}

function watchLspMethods() {
  return regeneratorRuntime.wrap(function watchLspMethods$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.takeLatest)(String(_actions.findReferences), handleReferences);

        case 2:
          _context3.next = 4;
          return (0, _effects.takeLatest)(String(_actions.findDefinitions), handleDefinitions);

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function handleClosePanel(action) {
  var search, pathname, queryParams, query;
  return regeneratorRuntime.wrap(function handleClosePanel$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          if (!action.payload) {
            _context4.next = 10;
            break;
          }

          _context4.next = 3;
          return (0, _effects.select)(_selectors.urlQueryStringSelector);

        case 3:
          search = _context4.sent;
          pathname = _url2.history.location.pathname;
          queryParams = _url.default.parse(search, true).query;

          if (queryParams.tab) {
            delete queryParams.tab;
          }

          if (queryParams.refUrl) {
            delete queryParams.refUrl;
          }

          query = _querystring.default.stringify(queryParams);

          if (query) {
            _url2.history.push("".concat(pathname, "?").concat(query));
          } else {
            _url2.history.push(pathname);
          }

        case 10:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function watchCloseReference() {
  return regeneratorRuntime.wrap(function watchCloseReference$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _effects.takeLatest)(String(_actions.closePanel), handleClosePanel);

        case 2:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

function handleReference(url) {
  var refUrl, _parseLspUrl, uri, position, schema, repoUri, file, revision;

  return regeneratorRuntime.wrap(function handleReference$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.select)(_selectors.refUrlSelector);

        case 2:
          refUrl = _context6.sent;

          if (!(refUrl === url)) {
            _context6.next = 5;
            break;
          }

          return _context6.abrupt("return");

        case 5:
          _parseLspUrl = (0, _uri_util.parseLspUrl)(url), uri = _parseLspUrl.uri, position = _parseLspUrl.position, schema = _parseLspUrl.schema, repoUri = _parseLspUrl.repoUri, file = _parseLspUrl.file, revision = _parseLspUrl.revision;

          if (!(uri && position)) {
            _context6.next = 9;
            break;
          }

          _context6.next = 9;
          return (0, _effects.put)((0, _actions.findReferences)({
            textDocument: {
              uri: (0, _uri_util.toCanonicalUrl)({
                revision: revision,
                schema: schema,
                repoUri: repoUri,
                file: file
              })
            },
            position: position
          }));

        case 9:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function openDefinitions(url) {
  var refUrl, _parseLspUrl2, uri, position, schema, repoUri, file, revision;

  return regeneratorRuntime.wrap(function openDefinitions$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _effects.select)(_selectors.refUrlSelector);

        case 2:
          refUrl = _context7.sent;

          if (!(refUrl === url)) {
            _context7.next = 5;
            break;
          }

          return _context7.abrupt("return");

        case 5:
          _parseLspUrl2 = (0, _uri_util.parseLspUrl)(url), uri = _parseLspUrl2.uri, position = _parseLspUrl2.position, schema = _parseLspUrl2.schema, repoUri = _parseLspUrl2.repoUri, file = _parseLspUrl2.file, revision = _parseLspUrl2.revision;

          if (!(uri && position)) {
            _context7.next = 9;
            break;
          }

          _context7.next = 9;
          return (0, _effects.put)((0, _actions.findDefinitions)({
            textDocument: {
              uri: (0, _uri_util.toCanonicalUrl)({
                revision: revision,
                schema: schema,
                repoUri: repoUri,
                file: file
              })
            },
            position: position
          }));

        case 9:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

function handleFile(repoUri, file, revision) {
  var response, payload;
  return regeneratorRuntime.wrap(function handleFile$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.select)(_selectors.fileSelector);

        case 2:
          response = _context8.sent;
          payload = response && response.payload;

          if (!(payload && payload.path === file && payload.revision === revision && payload.uri === repoUri)) {
            _context8.next = 6;
            break;
          }

          return _context8.abrupt("return");

        case 6:
          _context8.next = 8;
          return (0, _effects.put)((0, _actions.fetchFile)({
            uri: repoUri,
            path: file,
            revision: revision
          }));

        case 8:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8);
}

function fetchRepo(repoUri) {
  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(repoUri));
}

function loadRepoSaga(action) {
  var repo, repoScope;
  return regeneratorRuntime.wrap(function loadRepoSaga$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.call)(fetchRepo, action.payload);

        case 3:
          repo = _context9.sent;
          _context9.next = 6;
          return (0, _effects.put)((0, _status.loadRepoSuccess)(repo));

        case 6:
          _context9.next = 8;
          return (0, _effects.select)(_selectors.repoScopeSelector);

        case 8:
          repoScope = _context9.sent;

          if (!(repoScope.length === 0)) {
            _context9.next = 12;
            break;
          }

          _context9.next = 12;
          return (0, _effects.put)((0, _actions.turnOnDefaultRepoScope)(repo));

        case 12:
          _context9.next = 18;
          break;

        case 14:
          _context9.prev = 14;
          _context9.t0 = _context9["catch"](0);
          _context9.next = 18;
          return (0, _effects.put)((0, _status.loadRepoFailed)(_context9.t0));

        case 18:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, null, [[0, 14]]);
}

function watchLoadRepo() {
  return regeneratorRuntime.wrap(function watchLoadRepo$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.takeEvery)(String(_status.loadRepo), loadRepoSaga);

        case 2:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10);
}

function handleMainRouteChange(action) {
  var repos, _ref3, location, search, queryParams, _params2, resource, org, repo, file, pathType, revision, goto, repoUri, position, tab, _refUrl, commits, lastRequestPath, tree, isDir, isTreeLoaded, targetTree, uri;

  return regeneratorRuntime.wrap(function handleMainRouteChange$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          isTreeLoaded = function _ref4(isDirectory, targetTree) {
            if (!isDirectory) {
              return !!targetTree;
            } else if (!targetTree) {
              return false;
            } else {
              return targetTree.children && targetTree.children.length > 0;
            }
          };

          _context11.next = 3;
          return (0, _effects.select)(_selectors.reposSelector);

        case 3:
          repos = _context11.sent;

          if (!(repos.length === 0)) {
            _context11.next = 7;
            break;
          }

          _context11.next = 7;
          return (0, _effects.put)((0, _actions.fetchRepos)());

        case 7:
          _ref3 = action.payload, location = _ref3.location;
          search = location.search.startsWith('?') ? location.search.substring(1) : location.search;
          queryParams = _querystring.default.parse(search);
          _params2 = action.payload.params, resource = _params2.resource, org = _params2.org, repo = _params2.repo, file = _params2.path, pathType = _params2.pathType, revision = _params2.revision, goto = _params2.goto;
          repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);

          if (goto) {
            position = (0, _uri_util.parseGoto)(goto);
          }

          if (!file) {
            _context11.next = 40;
            break;
          }

          if (![_types.PathTypes.blob, _types.PathTypes.blame].includes(pathType)) {
            _context11.next = 34;
            break;
          }

          _context11.next = 17;
          return (0, _effects.put)((0, _actions.revealPosition)(position));

        case 17:
          tab = queryParams.tab, _refUrl = queryParams.refUrl;

          if (!(tab === 'references' && _refUrl)) {
            _context11.next = 23;
            break;
          }

          _context11.next = 21;
          return (0, _effects.call)(handleReference, decodeURIComponent(_refUrl));

        case 21:
          _context11.next = 30;
          break;

        case 23:
          if (!(tab === 'definitions' && _refUrl)) {
            _context11.next = 28;
            break;
          }

          _context11.next = 26;
          return (0, _effects.call)(openDefinitions, decodeURIComponent(_refUrl));

        case 26:
          _context11.next = 30;
          break;

        case 28:
          _context11.next = 30;
          return (0, _effects.put)((0, _actions.closePanel)(false));

        case 30:
          _context11.next = 32;
          return (0, _effects.call)(handleFile, repoUri, file, revision);

        case 32:
          _context11.next = 40;
          break;

        case 34:
          _context11.next = 36;
          return (0, _effects.select)(function (state) {
            return state.revision.treeCommits[file];
          });

        case 36:
          commits = _context11.sent;

          if (!(commits === undefined)) {
            _context11.next = 40;
            break;
          }

          _context11.next = 40;
          return (0, _effects.put)((0, _actions.fetchTreeCommits)({
            revision: revision,
            uri: repoUri,
            path: file
          }));

        case 40:
          _context11.next = 42;
          return (0, _effects.select)(_selectors.lastRequestPathSelector);

        case 42:
          lastRequestPath = _context11.sent;
          _context11.next = 45;
          return (0, _effects.select)(_selectors.getTree);

        case 45:
          tree = _context11.sent;
          isDir = pathType === _types.PathTypes.tree;
          _context11.next = 49;
          return (0, _effects.select)((0, _selectors.createTreeSelector)(file || ''));

        case 49:
          targetTree = _context11.sent;

          if (isTreeLoaded(isDir, targetTree)) {
            _context11.next = 53;
            break;
          }

          _context11.next = 53;
          return (0, _effects.put)((0, _actions.fetchRepoTree)({
            uri: repoUri,
            revision: revision,
            path: file || '',
            parents: (0, _file_tree.getPathOfTree)(tree, (file || '').split('/')) === null,
            isDir: isDir
          }));

        case 53:
          uri = (0, _uri_util.toCanonicalUrl)({
            repoUri: repoUri,
            file: file,
            revision: revision
          });

          if (!(file && pathType === _types.PathTypes.blob)) {
            _context11.next = 58;
            break;
          }

          if (!(lastRequestPath !== uri)) {
            _context11.next = 58;
            break;
          }

          _context11.next = 58;
          return (0, _effects.put)((0, _actions.loadStructure)(uri));

        case 58:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}

function watchMainRouteChange() {
  return regeneratorRuntime.wrap(function watchMainRouteChange$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.next = 2;
          return (0, _effects.takeLatest)(_patterns.mainRoutePattern, handleMainRouteChange);

        case 2:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12);
}