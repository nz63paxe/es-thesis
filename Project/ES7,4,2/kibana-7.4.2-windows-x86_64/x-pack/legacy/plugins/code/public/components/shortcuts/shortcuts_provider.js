"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ShortcutsProvider = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../actions");

var _shortcut = require("./shortcut");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ShortcutsComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ShortcutsComponent, _React$Component);

  function ShortcutsComponent(props) {
    var _this;

    _classCallCheck(this, ShortcutsComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ShortcutsComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "os", void 0);

    _defineProperty(_assertThisInitialized(_this), "handleKeydown", function (event) {
      var target = event.target;
      var key = event.key; // @ts-ignore

      if (target && target.tagName === 'INPUT') {
        if (key === 'Escape') {
          // @ts-ignore
          target.blur();
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleKeyPress", function (event) {
      var target = event.target;
      var key = event.key; // @ts-ignore

      if (target && target.tagName === 'INPUT') {
        return;
      }

      var isPressed = function isPressed(s) {
        if (s.modifier) {
          var mods = s.modifier.get(_this.os) || [];
          var _iteratorNormalCompletion = true;
          var _didIteratorError = false;
          var _iteratorError = undefined;

          try {
            for (var _iterator = mods[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
              var mod = _step.value;

              switch (mod) {
                case _shortcut.Modifier.alt:
                  if (!event.altKey) {
                    return false;
                  }

                  break;

                case _shortcut.Modifier.ctrl:
                  if (!event.ctrlKey) {
                    return false;
                  }

                  break;

                case _shortcut.Modifier.meta:
                  if (!event.metaKey) {
                    return false;
                  }

                  break;

                case _shortcut.Modifier.shift:
                  if (!event.shiftKey) {
                    return false;
                  }

                  break;
              }
            }
          } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion && _iterator.return != null) {
                _iterator.return();
              }
            } finally {
              if (_didIteratorError) {
                throw _iteratorError;
              }
            }
          }
        }

        return key === s.key;
      };

      var isTriggered = false;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = _this.props.shortcuts[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var shortcut = _step2.value;

          if (isPressed(shortcut) && shortcut.onPress) {
            shortcut.onPress(_this.props.dispatch);
            isTriggered = true;
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      if (isTriggered) {
        // Discard this input since it's been triggered already.
        event.preventDefault();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "closeModal", function () {
      _this.props.dispatch((0, _actions.toggleHelp)(false));
    });

    if (navigator.appVersion.indexOf('Win') !== -1) {
      _this.os = _shortcut.OS.win;
    } else if (navigator.appVersion.indexOf('Mac') !== -1) {
      _this.os = _shortcut.OS.mac;
    } else {
      _this.os = _shortcut.OS.linux;
    }

    return _this;
  }

  _createClass(ShortcutsComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      document.addEventListener('keydown', this.handleKeydown);
      document.addEventListener('keypress', this.handleKeyPress);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('keydown', this.handleKeydown);
      document.removeEventListener('keypress', this.handleKeyPress);
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement(_react.default.Fragment, null, this.props.showHelp && _react.default.createElement(_eui.EuiOverlayMask, null, _react.default.createElement(_eui.EuiModal, {
        onClose: this.closeModal
      }, _react.default.createElement(_eui.EuiModalHeader, null, _react.default.createElement(_eui.EuiModalHeaderTitle, null, "Keyboard Shortcuts")), _react.default.createElement(_eui.EuiModalBody, null, this.renderShortcuts()), _react.default.createElement(_eui.EuiModalFooter, null, _react.default.createElement(_eui.EuiButton, {
        onClick: this.closeModal,
        fill: true
      }, "Close")))));
    }
  }, {
    key: "showModifier",
    value: function showModifier(mod) {
      switch (mod) {
        case _shortcut.Modifier.meta:
          if (this.os === _shortcut.OS.mac) {
            return '⌘';
          } else if (this.os === _shortcut.OS.win) {
            return '⊞ Win';
          } else {
            return 'meta';
          }

        case _shortcut.Modifier.shift:
          if (this.os === _shortcut.OS.mac) {
            return '⇧';
          } else {
            return 'shift';
          }

        case _shortcut.Modifier.ctrl:
          if (this.os === _shortcut.OS.mac) {
            return '⌃';
          } else {
            return 'ctrl';
          }

        case _shortcut.Modifier.alt:
          if (this.os === _shortcut.OS.mac) {
            return '⌥';
          } else {
            return 'alt';
          }

      }
    }
  }, {
    key: "renderShortcuts",
    value: function renderShortcuts() {
      var _this2 = this;

      return this.props.shortcuts.map(function (s, idx) {
        return _react.default.createElement("div", {
          key: 'shortcuts_' + idx
        }, _this2.renderModifier(s), _react.default.createElement("span", {
          className: "codeShortcuts__key"
        }, s.key), _react.default.createElement("span", {
          className: "codeShortcuts__helpText"
        }, s.help));
      });
    }
  }, {
    key: "renderModifier",
    value: function renderModifier(hotKey) {
      var _this3 = this;

      if (hotKey.modifier) {
        var modifiers = hotKey.modifier.get(this.os) || [];
        return modifiers.map(function (m) {
          return _react.default.createElement("div", {
            className: "codeShortcuts__key"
          }, _this3.showModifier(m));
        });
      } else {
        return null;
      }
    }
  }]);

  return ShortcutsComponent;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    shortcuts: state.shortcuts.shortcuts,
    showHelp: state.shortcuts.showHelp
  };
};

var ShortcutsProvider = (0, _reactRedux.connect)(mapStateToProps)(ShortcutsComponent);
exports.ShortcutsProvider = ShortcutsProvider;