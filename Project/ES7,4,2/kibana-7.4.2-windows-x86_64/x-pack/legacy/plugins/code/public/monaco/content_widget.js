"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toggleClass = toggleClass;
exports.ContentWidget = void 0;

var _scrollableElement = require("monaco-editor/esm/vs/base/browser/ui/scrollbar/scrollableElement");

var _disposable = require("./disposable");

var _monaco = require("./monaco");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function toggleClass(node, clazzName, toggle) {
  node.classList.toggle(clazzName, toggle);
}

var ContentWidget =
/*#__PURE__*/
function (_Disposable) {
  _inherits(ContentWidget, _Disposable);

  _createClass(ContentWidget, [{
    key: "isVisible",
    get: function get() {
      return this.visible;
    },
    set: function set(value) {
      this.visible = value;
      toggleClass(this.containerDomNode, 'hidden', !this.visible);
    }
  }]);

  function ContentWidget(id, editor) {
    var _this;

    _classCallCheck(this, ContentWidget);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ContentWidget).call(this));
    _this.id = id;
    _this.editor = editor;

    _defineProperty(_assertThisInitialized(_this), "containerDomNode", void 0);

    _defineProperty(_assertThisInitialized(_this), "domNode", void 0);

    _defineProperty(_assertThisInitialized(_this), "extraNode", void 0);

    _defineProperty(_assertThisInitialized(_this), "scrollbar", void 0);

    _defineProperty(_assertThisInitialized(_this), "showAtPosition", void 0);

    _defineProperty(_assertThisInitialized(_this), "stoleFocus", false);

    _defineProperty(_assertThisInitialized(_this), "visible", void 0);

    _this.containerDomNode = document.createElement('div');
    _this.domNode = document.createElement('div');
    _this.extraNode = document.createElement('div');
    _this.scrollbar = new _scrollableElement.DomScrollableElement(_this.domNode, {});

    _this.disposables.push(_this.scrollbar);

    _this.containerDomNode.appendChild(_this.scrollbar.getDomNode());

    _this.containerDomNode.appendChild(_this.extraNode);

    _this.visible = false;

    _this.editor.onDidLayoutChange(function (e) {
      return _this.updateMaxHeight();
    });

    _this.editor.onDidChangeModel(function () {
      return _this.hide();
    });

    _this.updateMaxHeight();

    _this.showAtPosition = null; // @ts-ignore

    _this.editor.addContentWidget(_assertThisInitialized(_this));

    return _this;
  }

  _createClass(ContentWidget, [{
    key: "getId",
    value: function getId() {
      return this.id;
    }
  }, {
    key: "getDomNode",
    value: function getDomNode() {
      return this.containerDomNode;
    }
  }, {
    key: "showAt",
    value: function showAt(position, focus) {
      this.showAtPosition = position; // @ts-ignore

      this.editor.layoutContentWidget(this);
      this.isVisible = true;
      this.editor.render();
      this.stoleFocus = focus;

      if (focus) {
        this.containerDomNode.focus();
      }
    }
  }, {
    key: "hide",
    value: function hide() {
      if (!this.isVisible) {
        return;
      }

      this.isVisible = false; // @ts-ignore

      this.editor.layoutContentWidget(this);

      if (this.stoleFocus) {
        this.editor.focus();
      }
    } // @ts-ignore

  }, {
    key: "getPosition",
    value: function getPosition() {
      var ContentWidgetPositionPreference = _monaco.monaco.editor.ContentWidgetPositionPreference;

      if (this.isVisible) {
        return {
          position: this.showAtPosition,
          preference: [ContentWidgetPositionPreference.ABOVE, ContentWidgetPositionPreference.BELOW]
        };
      }

      return null;
    }
  }, {
    key: "dispose",
    value: function dispose() {
      // @ts-ignore
      this.editor.removeContentWidget(this);
      this.disposables.forEach(function (d) {
        return d.dispose();
      });
    }
  }, {
    key: "updateContents",
    value: function updateContents(node, extra) {
      this.domNode.textContent = '';
      this.domNode.appendChild(node);
      this.extraNode.innerHTML = '';

      if (extra) {
        this.extraNode.appendChild(extra);
      }

      this.updateFont(); // @ts-ignore

      this.editor.layoutContentWidget(this);
      this.onContentsChange();
    }
  }, {
    key: "onContentsChange",
    value: function onContentsChange() {
      this.scrollbar.scanDomNode();
    }
  }, {
    key: "updateMaxHeight",
    value: function updateMaxHeight() {
      var height = Math.max(this.editor.getLayoutInfo().height / 4, 250);
      var _this$editor$getConfi = this.editor.getConfiguration().fontInfo,
          fontSize = _this$editor$getConfi.fontSize,
          lineHeight = _this$editor$getConfi.lineHeight;
      this.domNode.style.fontSize = "".concat(fontSize, "px");
      this.domNode.style.lineHeight = "".concat(lineHeight, "px");
      this.domNode.style.maxHeight = "".concat(height, "px");
    }
  }, {
    key: "updateFont",
    value: function updateFont() {
      var _this2 = this;

      var codeTags = Array.prototype.slice.call(this.domNode.getElementsByTagName('code'));
      var codeClasses = Array.prototype.slice.call(this.domNode.getElementsByClassName('code'));
      [].concat(_toConsumableArray(codeTags), _toConsumableArray(codeClasses)).forEach(function (node) {
        return _this2.editor.applyFontInfo(node);
      });
    }
  }]);

  return ContentWidget;
}(_disposable.Disposable);

exports.ContentWidget = ContentWidget;