"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitLink = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CommitLink = function CommitLink(_ref) {
  var repoUri = _ref.repoUri,
      commit = _ref.commit,
      children = _ref.children;
  // const href = DIFF.replace(':resource/:org/:repo', repoUri).replace(':commitId', commit);
  return (// <EuiLink href={`#${href}`}>
    _react.default.createElement(_eui.EuiBadge, {
      color: "hollow"
    }, children || commit) // </EuiLink>

  );
};

exports.CommitLink = CommitLink;