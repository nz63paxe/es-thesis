"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatusChanged = exports.FetchRepoFileStatusFailed = exports.FetchRepoFileStatusSuccess = exports.FetchRepoFileStatus = exports.pollRepoDeleteStatusStop = exports.pollRepoIndexStatusStop = exports.pollRepoCloneStatusStop = exports.pollRepoDeleteStatusStart = exports.pollRepoIndexStatusStart = exports.pollRepoCloneStatusStart = exports.updateDeleteProgress = exports.updateIndexProgress = exports.updateCloneProgress = exports.loadRepoFailed = exports.loadRepoSuccess = exports.loadRepo = exports.loadStatusFailed = exports.loadStatusSuccess = exports.loadStatus = exports.RepoState = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RepoState;
exports.RepoState = RepoState;

(function (RepoState) {
  RepoState[RepoState["CLONING"] = 0] = "CLONING";
  RepoState[RepoState["DELETING"] = 1] = "DELETING";
  RepoState[RepoState["INDEXING"] = 2] = "INDEXING";
  RepoState[RepoState["READY"] = 3] = "READY";
  RepoState[RepoState["CLONE_ERROR"] = 4] = "CLONE_ERROR";
  RepoState[RepoState["DELETE_ERROR"] = 5] = "DELETE_ERROR";
  RepoState[RepoState["INDEX_ERROR"] = 6] = "INDEX_ERROR";
  RepoState[RepoState["UNKNOWN"] = 7] = "UNKNOWN";
})(RepoState || (exports.RepoState = RepoState = {}));

var loadStatus = (0, _reduxActions.createAction)('LOAD STATUS');
exports.loadStatus = loadStatus;
var loadStatusSuccess = (0, _reduxActions.createAction)('LOAD STATUS SUCCESS');
exports.loadStatusSuccess = loadStatusSuccess;
var loadStatusFailed = (0, _reduxActions.createAction)('LOAD STATUS FAILED');
exports.loadStatusFailed = loadStatusFailed;
var loadRepo = (0, _reduxActions.createAction)('LOAD REPO');
exports.loadRepo = loadRepo;
var loadRepoSuccess = (0, _reduxActions.createAction)('LOAD REPO SUCCESS');
exports.loadRepoSuccess = loadRepoSuccess;
var loadRepoFailed = (0, _reduxActions.createAction)('LOAD REPO FAILED');
exports.loadRepoFailed = loadRepoFailed;
var updateCloneProgress = (0, _reduxActions.createAction)('UPDATE CLONE PROGRESS');
exports.updateCloneProgress = updateCloneProgress;
var updateIndexProgress = (0, _reduxActions.createAction)('UPDATE INDEX PROGRESS');
exports.updateIndexProgress = updateIndexProgress;
var updateDeleteProgress = (0, _reduxActions.createAction)('UPDATE DELETE PROGRESS');
exports.updateDeleteProgress = updateDeleteProgress;
var pollRepoCloneStatusStart = (0, _reduxActions.createAction)('POLL CLONE STATUS START');
exports.pollRepoCloneStatusStart = pollRepoCloneStatusStart;
var pollRepoIndexStatusStart = (0, _reduxActions.createAction)('POLL INDEX STATUS START');
exports.pollRepoIndexStatusStart = pollRepoIndexStatusStart;
var pollRepoDeleteStatusStart = (0, _reduxActions.createAction)('POLL DELETE STATUS START');
exports.pollRepoDeleteStatusStart = pollRepoDeleteStatusStart;
var pollRepoCloneStatusStop = (0, _reduxActions.createAction)('POLL CLONE STATUS STOP');
exports.pollRepoCloneStatusStop = pollRepoCloneStatusStop;
var pollRepoIndexStatusStop = (0, _reduxActions.createAction)('POLL INDEX STATUS STOP');
exports.pollRepoIndexStatusStop = pollRepoIndexStatusStop;
var pollRepoDeleteStatusStop = (0, _reduxActions.createAction)('POLL DELETE STATUS STOP');
exports.pollRepoDeleteStatusStop = pollRepoDeleteStatusStop;
var FetchRepoFileStatus = (0, _reduxActions.createAction)('FETCH REPO FILE STATUS');
exports.FetchRepoFileStatus = FetchRepoFileStatus;
var FetchRepoFileStatusSuccess = (0, _reduxActions.createAction)('FETCH REPO FILE STATUS SUCCESS');
exports.FetchRepoFileStatusSuccess = FetchRepoFileStatusSuccess;
var FetchRepoFileStatusFailed = (0, _reduxActions.createAction)('FETCH REPO FILE STATUS FAILED');
exports.FetchRepoFileStatusFailed = FetchRepoFileStatusFailed;
var StatusChanged = (0, _reduxActions.createAction)('STATUS CHANGED');
exports.StatusChanged = StatusChanged;