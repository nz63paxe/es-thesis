"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuggestionComponent = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SuggestionComponent = function SuggestionComponent(props) {
  var click = function click() {
    return props.onClick(props.suggestion);
  }; // An util function to help highlight the substring which matches the query.


  var renderMatchingText = function renderMatchingText(text) {
    if (!text) {
      return text;
    } // Match the text with query in case sensitive mode first.


    var index = text.indexOf(props.query);

    if (index < 0) {
      // Fall back with case insensitive mode first.
      index = text.toLowerCase().indexOf(props.query.toLowerCase());
    }

    if (index >= 0) {
      var prefix = text.substring(0, index);
      var highlight = text.substring(index, index + props.query.length);
      var surfix = text.substring(index + props.query.length);
      return _react.default.createElement("span", null, prefix, _react.default.createElement("strong", null, highlight), surfix);
    } else {
      return text;
    }
  };

  var icon = props.suggestion.tokenType ? _react.default.createElement("div", {
    className: "codeSearch-suggestion__token"
  }, _react.default.createElement(_eui.EuiToken, {
    iconType: props.suggestion.tokenType
  })) : null;
  return (// eslint-disable-next-line jsx-a11y/click-events-have-key-events, jsx-a11y/interactive-supports-focus
    _react.default.createElement("div", {
      className: 'codeSearch__suggestion-item ' + (props.selected ? 'codeSearch__suggestion-item--active' : ''),
      role: "option",
      onClick: click // active={props.selected}
      ,
      onMouseEnter: props.onMouseEnter,
      ref: props.innerRef,
      id: props.ariaId,
      "aria-selected": props.selected
    }, _react.default.createElement("div", {
      className: "codeSearch-suggestion--inner"
    }, icon, _react.default.createElement("div", {
      className: "codeSearch__suggestionTextContainer"
    }, _react.default.createElement("div", {
      className: "codeSearch__suggestion-text",
      "data-test-subj": "codeTypeaheadItem"
    }, renderMatchingText(props.suggestion.text)), _react.default.createElement("div", {
      className: "codeSearch-suggestion__description"
    }, props.suggestion.description))))
  );
};

exports.SuggestionComponent = SuggestionComponent;