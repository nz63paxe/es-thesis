"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.closeSymbolPath = exports.openSymbolPath = exports.loadStructureFailed = exports.loadStructureSuccess = exports.loadStructure = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadStructure = (0, _reduxActions.createAction)('LOAD STRUCTURE');
exports.loadStructure = loadStructure;
var loadStructureSuccess = (0, _reduxActions.createAction)('LOAD STRUCTURE SUCCESS');
exports.loadStructureSuccess = loadStructureSuccess;
var loadStructureFailed = (0, _reduxActions.createAction)('LOAD STRUCTURE FAILED');
exports.loadStructureFailed = loadStructureFailed;
var openSymbolPath = (0, _reduxActions.createAction)('OPEN SYMBOL PATH');
exports.openSymbolPath = openSymbolPath;
var closeSymbolPath = (0, _reduxActions.createAction)('CLOSE SYMBOL PATH');
exports.closeSymbolPath = closeSymbolPath;