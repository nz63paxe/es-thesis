"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SetupGuide = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _documentation_links = require("../../lib/documentation_links");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var steps = [{
  title: _i18n.i18n.translate('xpack.code.adminPage.setupGuide.checkMultiInstanceTitle', {
    defaultMessage: 'Check if multiple Kibana instances are used as a clusterURL'
  }),
  children: _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.checkMultiInstanceDescription1",
    defaultMessage: "If you are using single Kibana instance, you can skip this step."
  })), _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.checkMultiInstanceDescription2",
    defaultMessage: "If you are using multiple Kibana instances, you need to assign one Kibana instance as `Code node`. To do this, add the following line of code into your kibana.yml file of every Kibana instance and restart the instances:"
  })), _react2.default.createElement("pre", null, _react2.default.createElement("code", null, "xpack.code.codeNodeUrl: 'http://$YourCodeNodeAddress'")), _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.checkMultiInstanceDescription3",
    defaultMessage: "Where `$YourCodeNoteAddress` is the URL of your assigned Code node accessible by other Kibana instances."
  })))
}, {
  title: _i18n.i18n.translate('xpack.code.adminPage.setupGuide.installExtraLangSupportTitle', {
    defaultMessage: 'Install extra language support optionally'
  }),
  children: _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.installExtraLangSupportDescription1",
    defaultMessage: "Look {link} to learn more about supported languages and language server installation.",
    values: {
      link: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.codeInstallLangServer,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.installExtraLangSupportHereLinkText",
        defaultMessage: "here"
      }))
    }
  })), _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.installExtraLangSupportDescription2",
    defaultMessage: "If you need Java language support, you can manage language server installation {link}.",
    values: {
      link: _react2.default.createElement(_reactRouterDom.Link, {
        to: "/admin?tab=LanguageServers"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.installExtraLangSupportHereLinkText",
        defaultMessage: "here"
      }))
    }
  })))
}, {
  title: _i18n.i18n.translate('xpack.code.adminPage.setupGuide.addRepositoryTitle', {
    defaultMessage: 'Add a repository to Code'
  }),
  children: _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.addRepositoryDescription",
    defaultMessage: "Import {sampleRepoLink} or {ownRepoLink}. It is as easy as copy and paste git clone URLs to Code.",
    values: {
      sampleRepoLink: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.codeGettingStarted,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.addRepositorySampleRepoLinkText",
        defaultMessage: "a sample repo"
      })),
      ownRepoLink: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.codeRepoManagement,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.addRepositoryOwnRepoLinkText",
        defaultMessage: "your own repo"
      }))
    }
  })))
}, {
  title: _i18n.i18n.translate('xpack.code.adminPage.setupGuide.verifyImportTitle', {
    defaultMessage: 'Verify the repo is successfully imported'
  }),
  children: _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.setupGuide.verifyImportDescription",
    defaultMessage: "You can verify your repo is successfully imported by {searchingLink} and {navigatingLink} the repo. If language support is available to the repo, make sure {semanticNavigationLink} is available as well.",
    values: {
      searchingLink: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.codeSearch,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.verifyImportSearchingLinkText",
        defaultMessage: "searching"
      })),
      navigatingLink: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.codeOtherFeatures,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.verifyImportNavigatingLinkText",
        defaultMessage: "navigating"
      })),
      semanticNavigationLink: _react2.default.createElement(_eui.EuiLink, {
        href: _documentation_links.documentationLinks.semanticNavigation,
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.setupGuide.verifyImportSemanticNavigatingLinkText",
        defaultMessage: "semantic navigation"
      }))
    }
  })))
}];

var toastMessage = _react2.default.createElement("div", null, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
  id: "xpack.code.adminPage.setupGuide.permissionChangesDescription",
  defaultMessage: "We\u2019ve made some changes to roles and permissions in Kibana. Read more about how these changes affect your Code implementation below."
})), _react2.default.createElement(_eui.EuiButton, {
  size: "s",
  href: _documentation_links.documentationLinks.kibanaRoleManagement
}, _react2.default.createElement(_react.FormattedMessage, {
  id: "xpack.code.adminPage.setupGuide.learnMoreButtonLabel",
  defaultMessage: "Learn more"
})));

var SetupGuidePage =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(SetupGuidePage, _React$PureComponent);

  function SetupGuidePage(props) {
    var _this;

    _classCallCheck(this, SetupGuidePage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SetupGuidePage).call(this, props));
    _this.state = {
      hideToast: false
    };
    return _this;
  }

  _createClass(SetupGuidePage, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var setup = null;

      if (this.props.setupOk !== undefined) {
        setup = _react2.default.createElement("div", null, !this.state.hideToast && _react2.default.createElement(_eui.EuiGlobalToastList, {
          toasts: [{
            title: _i18n.i18n.translate('xpack.code.adminPage.setupGuide.permissionChangesTitle', {
              defaultMessage: 'Permission Changes'
            }),
            color: 'primary',
            iconType: 'iInCircle',
            text: toastMessage,
            id: ''
          }],
          dismissToast: function dismissToast() {
            _this2.setState({
              hideToast: true
            });
          },
          toastLifeTimeMs: 10000
        }), _react2.default.createElement(_react2.default.Fragment, null, this.props.setupOk === false && _react2.default.createElement(_eui.EuiCallOut, {
          title: "Code instance not found.",
          color: "danger",
          iconType: "cross"
        }, _react2.default.createElement("p", null, "Please follow the guide below to configure your Kibana instance. Once configured, refresh this page.")), this.props.setupOk === true && _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_eui.EuiSpacer, {
          size: "s"
        }), _react2.default.createElement(_reactRouterDom.Link, {
          to: "/admin"
        }, _react2.default.createElement(_eui.EuiButton, {
          iconType: "sortLeft"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.adminPage.setupGuide.backToDashboardButtonLabel",
          defaultMessage: "Back To repository dashboard"
        }))), _react2.default.createElement(_eui.EuiSpacer, {
          size: "s"
        })), _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiTitle, null, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.adminPage.setupGuide.getStartedTitle",
          defaultMessage: "Getting started in Elastic Code"
        }))), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiSteps, {
          steps: steps
        }))));
      }

      return _react2.default.createElement("div", {
        className: "codeContainer__setup"
      }, setup);
    }
  }]);

  return SetupGuidePage;
}(_react2.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return {
    setupOk: state.setup.ok
  };
};

var SetupGuide = (0, _reactRedux.connect)(mapStateToProps)(SetupGuidePage);
exports.SetupGuide = SetupGuide;