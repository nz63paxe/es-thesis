"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchRepoOrRevisionChange = watchRepoOrRevisionChange;
exports.watchRoute = watchRoute;
exports.handleRepoChange = handleRepoChange;
exports.watchRepoChange = watchRepoChange;

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _selectors = require("../selectors");

var _route = require("../actions/route");

var ROUTES = _interopRequireWildcard(require("../components/routes"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoOrRevisionChange),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoOrRevisionChange),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRoute),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRoute),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoChange),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoChange);

var MAIN_ROUTES = [ROUTES.MAIN, ROUTES.MAIN_ROOT];

function handleRepoOrRevisionChange() {
  var repoUri, revision;
  return regeneratorRuntime.wrap(function handleRepoOrRevisionChange$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(_selectors.repoUriSelector);

        case 2:
          repoUri = _context.sent;
          _context.next = 5;
          return (0, _effects.select)(_selectors.revisionSelector);

        case 5:
          revision = _context.sent;
          _context.next = 8;
          return (0, _effects.put)((0, _actions.fetchRepoCommits)({
            uri: repoUri,
            revision: revision
          }));

        case 8:
          _context.next = 10;
          return (0, _effects.put)((0, _actions.fetchRootRepoTree)({
            uri: repoUri,
            revision: revision
          }));

        case 10:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

function watchRepoOrRevisionChange() {
  return regeneratorRuntime.wrap(function watchRepoOrRevisionChange$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeLatest)([String(_route.repoChange), String(_route.revisionChange)], handleRepoOrRevisionChange);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

var getRepoFromMatch = function getRepoFromMatch(match) {
  return "".concat(match.params.resource, "/").concat(match.params.org, "/").concat(match.params.repo);
};

function handleRoute(action) {
  var currentMatch, previousMatch, currentRepo, previousRepo, currentRevision, previousRevision, currentFilePath, previousFilePath, _currentRepo;

  return regeneratorRuntime.wrap(function handleRoute$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          currentMatch = action.payload;
          _context3.next = 3;
          return (0, _effects.select)(_selectors.previousMatchSelector);

        case 3:
          previousMatch = _context3.sent;

          if (!MAIN_ROUTES.includes(currentMatch.path)) {
            _context3.next = 34;
            break;
          }

          if (!MAIN_ROUTES.includes(previousMatch.path)) {
            _context3.next = 23;
            break;
          }

          currentRepo = getRepoFromMatch(currentMatch);
          previousRepo = getRepoFromMatch(previousMatch);
          currentRevision = currentMatch.params.revision;
          previousRevision = previousMatch.params.revision;
          currentFilePath = currentMatch.params.path;
          previousFilePath = previousMatch.params.path;

          if (!(currentRepo !== previousRepo)) {
            _context3.next = 15;
            break;
          }

          _context3.next = 15;
          return (0, _effects.put)((0, _route.repoChange)(currentRepo));

        case 15:
          if (!(currentRevision !== previousRevision)) {
            _context3.next = 18;
            break;
          }

          _context3.next = 18;
          return (0, _effects.put)((0, _route.revisionChange)());

        case 18:
          if (!(currentFilePath !== previousFilePath)) {
            _context3.next = 21;
            break;
          }

          _context3.next = 21;
          return (0, _effects.put)((0, _route.filePathChange)());

        case 21:
          _context3.next = 32;
          break;

        case 23:
          _context3.next = 25;
          return (0, _effects.put)((0, _route.routePathChange)());

        case 25:
          _currentRepo = getRepoFromMatch(currentMatch);
          _context3.next = 28;
          return (0, _effects.put)((0, _route.repoChange)(_currentRepo));

        case 28:
          _context3.next = 30;
          return (0, _effects.put)((0, _route.revisionChange)());

        case 30:
          _context3.next = 32;
          return (0, _effects.put)((0, _route.filePathChange)());

        case 32:
          _context3.next = 37;
          break;

        case 34:
          if (!(currentMatch.path !== previousMatch.path)) {
            _context3.next = 37;
            break;
          }

          _context3.next = 37;
          return (0, _effects.put)((0, _route.routePathChange)());

        case 37:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function watchRoute() {
  return regeneratorRuntime.wrap(function watchRoute$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(String(_actions.routeChange), handleRoute);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function handleRepoChange(action) {
  return regeneratorRuntime.wrap(function handleRepoChange$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return (0, _effects.put)((0, _actions.loadRepo)(action.payload));

        case 2:
          _context5.next = 4;
          return (0, _effects.put)((0, _actions.fetchRepoBranches)({
            uri: action.payload
          }));

        case 4:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

function watchRepoChange() {
  return regeneratorRuntime.wrap(function watchRepoChange$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.takeEvery)(String(_route.repoChange), handleRepoChange);

        case 2:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}