"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Content = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

require("github-markdown-css/github-markdown.css");

var _react2 = _interopRequireDefault(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _new_platform = require("ui/new_platform");

var _repository_utils = require("../../../common/repository_utils");

var _model = require("../../../model");

var _actions = require("../../actions");

var _types = require("../../common/types");

var _selectors = require("../../selectors");

var _uri_util = require("../../../common/uri_util");

var _url = require("../../utils/url");

var _editor = require("../editor/editor");

var _clone_status = require("./clone_status");

var _commit_history = require("./commit_history");

var _directory = require("./directory");

var _error_panel = require("./error_panel");

var _not_found = require("./not_found");

var _top_bar = require("./top_bar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LANG_MD = 'markdown';
var ButtonOption;

(function (ButtonOption) {
  ButtonOption["Code"] = "Code";
  ButtonOption["Blame"] = "Blame";
  ButtonOption["History"] = "History";
  ButtonOption["Folder"] = "Directory";
})(ButtonOption || (ButtonOption = {}));

var CodeContent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeContent, _React$PureComponent);

  function CodeContent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeContent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeContent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      fileLoading: _this.props.fileLoading
    });

    _defineProperty(_assertThisInitialized(_this), "findNode", function (pathSegments, node) {
      if (!node) {
        return undefined;
      } else if (pathSegments.length === 0) {
        return node;
      } else if (pathSegments.length === 1) {
        return (node.children || []).find(function (n) {
          return n.name === pathSegments[0];
        });
      } else {
        var currentFolder = pathSegments.shift();
        var nextNode = (node.children || []).find(function (n) {
          return n.name === currentFolder;
        });

        if (nextNode) {
          return _this.findNode(pathSegments, nextNode);
        } else {
          return undefined;
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "switchButton", function (id) {
      var _this$props$match$par = _this.props.match.params,
          path = _this$props$match$par.path,
          resource = _this$props$match$par.resource,
          org = _this$props$match$par.org,
          repo = _this$props$match$par.repo,
          revision = _this$props$match$par.revision;
      var queryString = _this.props.location.search;
      var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);

      switch (id) {
        case ButtonOption.Code:
          _url.history.push("/".concat(repoUri, "/").concat(_types.PathTypes.blob, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path || '').concat(queryString));

          break;

        case ButtonOption.Folder:
          _url.history.push("/".concat(repoUri, "/").concat(_types.PathTypes.tree, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path || '').concat(queryString));

          break;

        case ButtonOption.Blame:
          _url.history.push("/".concat(repoUri, "/").concat(_types.PathTypes.blame, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path || '').concat(queryString));

          break;

        case ButtonOption.History:
          _url.history.push("/".concat(repoUri, "/").concat(_types.PathTypes.commits, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path || '').concat(queryString));

          break;
      }
    });

    _defineProperty(_assertThisInitialized(_this), "openRawFile", function () {
      var _this$props$match$par2 = _this.props.match.params,
          path = _this$props$match$par2.path,
          resource = _this$props$match$par2.resource,
          org = _this$props$match$par2.org,
          repo = _this$props$match$par2.repo,
          revision = _this$props$match$par2.revision;
      var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);
      window.open(_new_platform.npStart.core.http.basePath.prepend("/app/code/repo/".concat(repoUri, "/raw/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path)));
    });

    _defineProperty(_assertThisInitialized(_this), "renderFileLoadingIndicator", function () {
      var fileName = (_this.props.match.params.path || '').split('/').slice(-1)[0];
      return _react2.default.createElement(_eui.EuiPage, {
        restrictWidth: true
      }, _react2.default.createElement(_eui.EuiPageBody, null, _react2.default.createElement(_eui.EuiPageContent, {
        verticalPosition: "center",
        horizontalPosition: "center"
      }, _react2.default.createElement(_eui.EuiPageContentHeader, null, _react2.default.createElement(_eui.EuiPageContentHeaderSection, null, _react2.default.createElement("h2", null, fileName, " is loading..."))), _react2.default.createElement(_eui.EuiPageContentBody, null, _react2.default.createElement(_eui.EuiProgress, {
        size: "s",
        color: "primary"
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "renderButtons", function () {
      var buttonId;

      switch (_this.props.match.params.pathType) {
        case _types.PathTypes.blame:
          buttonId = ButtonOption.Blame;
          break;

        case _types.PathTypes.blob:
          buttonId = ButtonOption.Code;
          break;

        case _types.PathTypes.tree:
          buttonId = ButtonOption.Folder;
          break;

        case _types.PathTypes.commits:
          buttonId = ButtonOption.History;
          break;

        default:
          break;
      }

      var currentTree = _this.props.currentTree;

      if (currentTree && (currentTree.type === _model.FileTreeItemType.File || currentTree.type === _model.FileTreeItemType.Link)) {
        var isText = true;
        var isMarkdown = false;
        var blameDisabled = false;

        if (_this.props.file) {
          var _this$props$file = _this.props.file,
              isUnsupported = _this$props$file.isUnsupported,
              isOversize = _this$props$file.isOversize,
              isImage = _this$props$file.isImage,
              lang = _this$props$file.lang;
          isMarkdown = lang === LANG_MD;
          isText = !isUnsupported && !isOversize && !isImage;
          blameDisabled = !!(isUnsupported || isOversize || isImage);
        }

        var rawButtonOptions = [{
          id: 'Raw',
          label: isText ? _i18n.i18n.translate('xpack.code.mainPage.content.buttons.rawButtonLabel', {
            defaultMessage: 'Raw'
          }) : _i18n.i18n.translate('xpack.code.mainPage.content.buttons.downloadButtonLabel', {
            defaultMessage: 'Download'
          })
        }];
        var buttonOptions = [{
          id: ButtonOption.Code,
          label: isText && !isMarkdown ? _i18n.i18n.translate('xpack.code.mainPage.content.buttons.codeButtonLabel', {
            defaultMessage: 'Code'
          }) : _i18n.i18n.translate('xpack.code.mainPage.content.buttons.contentButtonLabel', {
            defaultMessage: 'content'
          })
        }, {
          id: ButtonOption.Blame,
          label: _i18n.i18n.translate('xpack.code.mainPage.content.buttons.blameButtonLabel', {
            defaultMessage: 'Blame'
          }),
          isDisabled: blameDisabled
        }, {
          id: ButtonOption.History,
          label: _i18n.i18n.translate('xpack.code.mainPage.content.buttons.historyButtonLabel', {
            defaultMessage: 'History'
          })
        }];
        return _react2.default.createElement(_eui.EuiFlexGroup, {
          direction: "row",
          alignItems: "center",
          gutterSize: "none"
        }, _react2.default.createElement(_eui.EuiButtonGroup, {
          buttonSize: "s",
          color: "primary",
          options: buttonOptions,
          type: "single",
          idSelected: buttonId,
          onChange: _this.switchButton,
          className: "codeButtonGroup"
        }), _react2.default.createElement(_eui.EuiButtonGroup, {
          buttonSize: "s",
          color: "primary",
          options: rawButtonOptions,
          type: "single",
          idSelected: '',
          onChange: _this.openRawFile,
          className: "codeButtonGroup"
        }));
      } else if (_this.shouldRenderCloneProgress()) {
        return null;
      } else {
        return _react2.default.createElement(_eui.EuiFlexGroup, {
          direction: "row",
          alignItems: "center",
          gutterSize: "none"
        }, _react2.default.createElement(_eui.EuiButtonGroup, {
          className: "codeButtonGroup",
          buttonSize: "s",
          color: "primary",
          options: [{
            id: ButtonOption.Folder,
            label: _i18n.i18n.translate('xpack.code.mainPage.content.buttons.folderButtonLabel', {
              defaultMessage: 'Directory'
            })
          }, {
            id: ButtonOption.History,
            label: _i18n.i18n.translate('xpack.code.mainPage.content.buttons.historyButtonLabel', {
              defaultMessage: 'History'
            })
          }],
          type: "single",
          idSelected: buttonId,
          onChange: _this.switchButton
        }));
      }
    });

    return _this;
  }

  _createClass(CodeContent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this2 = this;

      // after this amount of time, show file loading indicator if necessary
      var TIME_DELAY_MS = 500;

      if (!prevProps.fileLoading && this.props.fileLoading && !this.state.fileLoading) {
        setTimeout(function () {
          return _this2.setState({
            fileLoading: _this2.props.fileLoading
          });
        }, TIME_DELAY_MS);
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react2.default.createElement("div", {
        className: "codeContainer__main"
      }, _react2.default.createElement(_top_bar.TopBar, {
        routeParams: this.props.match.params,
        onSearchScopeChanged: this.props.onSearchScopeChanged,
        buttons: this.renderButtons(),
        searchOptions: this.props.searchOptions,
        branches: this.props.branches,
        tags: this.props.tags,
        query: this.props.query,
        currentRepository: this.props.currentRepository
      }), this.renderContent());
    }
  }, {
    key: "shouldRenderCloneProgress",
    value: function shouldRenderCloneProgress() {
      if (!this.props.repoStatus) {
        return false;
      }

      var _this$props$repoStatu = this.props.repoStatus,
          progress = _this$props$repoStatu.progress,
          cloneProgress = _this$props$repoStatu.cloneProgress,
          state = _this$props$repoStatu.state;
      return !!progress && state === _actions.RepoState.CLONING && progress < _model.WorkerReservedProgress.COMPLETED && !_repository_utils.RepositoryUtils.hasFullyCloned(cloneProgress);
    }
  }, {
    key: "renderCloneProgress",
    value: function renderCloneProgress() {
      if (!this.props.repoStatus) {
        return null;
      }

      var _this$props$repoStatu2 = this.props.repoStatus,
          progress = _this$props$repoStatu2.progress,
          cloneProgress = _this$props$repoStatu2.cloneProgress;
      var _this$props$match$par3 = this.props.match.params,
          org = _this$props$match$par3.org,
          repo = _this$props$match$par3.repo;
      return _react2.default.createElement(_clone_status.CloneStatus, {
        repoName: "".concat(org, "/").concat(repo),
        progress: progress ? progress : 0,
        cloneProgress: cloneProgress
      });
    }
  }, {
    key: "renderContent",
    value: function renderContent() {
      var _this$props = this.props,
          file = _this$props.file,
          match = _this$props.match,
          tree = _this$props.tree,
          fileTreeLoadingPaths = _this$props.fileTreeLoadingPaths,
          isNotFound = _this$props.isNotFound,
          notFoundDirs = _this$props.notFoundDirs;
      var _match$params = match.params,
          path = _match$params.path,
          pathType = _match$params.pathType,
          resource = _match$params.resource,
          org = _match$params.org,
          repo = _match$params.repo,
          revision = _match$params.revision; // The clone progress rendering should come before the NotFound rendering.

      if (this.shouldRenderCloneProgress()) {
        return this.renderCloneProgress();
      }

      if (isNotFound || notFoundDirs.includes(path || '')) {
        return _react2.default.createElement(_not_found.NotFound, null);
      }

      var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);

      switch (pathType) {
        case _types.PathTypes.tree:
          var node = this.findNode(path ? path.split('/') : [], tree);
          return _react2.default.createElement("div", {
            className: "codeContainer__directoryView"
          }, _react2.default.createElement(_directory.Directory, {
            node: node,
            loading: fileTreeLoadingPaths.includes(path)
          }), _react2.default.createElement(_commit_history.CommitHistory, {
            repoUri: repoUri,
            header: _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_eui.EuiTitle, {
              size: "s",
              className: "codeMargin__title"
            }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.code.mainPage.directory.recentCommitsTitle",
              defaultMessage: "Recent Commits"
            }))), _react2.default.createElement(_eui.EuiButton, {
              size: "s",
              href: "#/".concat(resource, "/").concat(org, "/").concat(repo, "/").concat(_types.PathTypes.commits, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path || '')
            }, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.code.mainPage.directory.viewAllCommitsButtonLabel",
              defaultMessage: "View All"
            })))
          }));

        case _types.PathTypes.blob:
          {
            var _fileLoading = this.state.fileLoading;

            if (!file) {
              return _fileLoading && _react2.default.createElement(_eui.EuiFlexGroup, {
                direction: "row",
                className: "codeContainer__blame",
                gutterSize: "none"
              }, _fileLoading && this.renderFileLoadingIndicator(), _react2.default.createElement(_editor.Editor, {
                showBlame: false,
                hidden: _fileLoading
              }));
            } else {
              var fileLanguage = file.lang,
                  fileContent = file.content,
                  isUnsupported = file.isUnsupported,
                  isOversize = file.isOversize,
                  isImage = file.isImage;

              if (isUnsupported) {
                return _react2.default.createElement(_error_panel.ErrorPanel, {
                  title: _react2.default.createElement("h2", null, "Unsupported File"),
                  content: "Unfortunately that\u2019s an unsupported file type and we\u2019re unable to render it here."
                });
              }

              if (isOversize) {
                return _react2.default.createElement(_error_panel.ErrorPanel, {
                  title: _react2.default.createElement("h2", null, "File is too big"),
                  content: "Sorry about that, but we can\u2019t show files that are this big right now."
                });
              }

              if (fileLanguage === LANG_MD) {
                var markdownRenderers = {
                  link: function link(_ref) {
                    var children = _ref.children,
                        href = _ref.href;
                    return _react2.default.createElement(_eui.EuiLink, {
                      href: href,
                      target: "_blank"
                    }, children);
                  }
                };
                return _react2.default.createElement("div", {
                  className: "markdown-body code-markdown-container kbnMarkdown__body"
                }, _react2.default.createElement(_reactMarkdown.default, {
                  source: fileContent,
                  escapeHtml: true,
                  skipHtml: true,
                  renderers: markdownRenderers
                }));
              } else if (isImage) {
                var rawUrl = _new_platform.npStart.core.http.basePath.prepend("/app/code/repo/".concat(repoUri, "/raw/").concat(revision, "/").concat(path));

                return _react2.default.createElement("div", {
                  className: "code-auto-margin"
                }, _react2.default.createElement("img", {
                  src: rawUrl,
                  alt: rawUrl
                }));
              } else {
                return _react2.default.createElement(_eui.EuiFlexGroup, {
                  direction: "row",
                  className: "codeContainer__blame",
                  gutterSize: "none"
                }, _fileLoading && this.renderFileLoadingIndicator(), _react2.default.createElement(_editor.Editor, {
                  showBlame: false,
                  hidden: _fileLoading
                }));
              }
            }
          }

        case _types.PathTypes.blame:
          var fileLoading = this.state.fileLoading;
          return _react2.default.createElement(_eui.EuiFlexGroup, {
            direction: "row",
            className: "codeContainer__blame",
            gutterSize: "none"
          }, fileLoading && this.renderFileLoadingIndicator(), _react2.default.createElement(_editor.Editor, {
            showBlame: true,
            hidden: fileLoading
          }));

        case _types.PathTypes.commits:
          return _react2.default.createElement("div", {
            className: "codeContainer__history"
          }, _react2.default.createElement(_commit_history.CommitHistory, {
            repoUri: repoUri,
            header: _react2.default.createElement(_eui.EuiTitle, {
              className: "codeMargin__title"
            }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.code.mainPage.history.commitHistoryTitle",
              defaultMessage: "Commit History"
            }))),
            showPagination: true
          }));
      }
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (!props.fileLoading) {
        return {
          fileLoading: props.fileLoading
        };
      }

      return null;
    }
  }]);

  return CodeContent;
}(_react2.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return {
    isNotFound: state.file.isNotFound,
    notFoundDirs: state.fileTree.notFoundDirs,
    file: state.file.file,
    tree: state.fileTree.tree,
    fileTreeLoadingPaths: state.fileTree.fileTreeLoadingPaths,
    currentTree: (0, _selectors.currentTreeSelector)(state),
    branches: state.revision.branches,
    tags: state.revision.tags,
    hasMoreCommits: (0, _selectors.hasMoreCommitsSelector)(state),
    loadingCommits: state.revision.loadingCommits,
    repoStatus: (0, _selectors.repoStatusSelector)(state, (0, _selectors.repoUriSelector)(state)),
    searchOptions: state.search.searchOptions,
    query: state.search.query,
    currentRepository: state.repository.repository,
    fileLoading: state.file.loading
  };
};

var mapDispatchToProps = {
  onSearchScopeChanged: _actions.changeSearchScope
};
var Content = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps // @ts-ignore
)(CodeContent));
exports.Content = Content;