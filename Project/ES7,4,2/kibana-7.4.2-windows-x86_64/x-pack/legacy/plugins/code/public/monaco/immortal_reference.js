"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ImmortalReference = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ImmortalReference =
/*#__PURE__*/
function () {
  function ImmortalReference(object) {
    _classCallCheck(this, ImmortalReference);

    this.object = object;
  }

  _createClass(ImmortalReference, [{
    key: "dispose",
    value: function dispose() {
      /* noop */
    }
  }]);

  return ImmortalReference;
}();

exports.ImmortalReference = ImmortalReference;