"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.switchLanguageServerFailed = exports.switchLanguageServerSuccess = exports.switchLanguageServer = exports.gotoRepoFailed = exports.gotoRepo = exports.initRepoCommand = exports.fetchRepoConfigFailed = exports.fetchRepoConfigSuccess = exports.fetchRepoConfigs = exports.closeToast = exports.importRepoFailed = exports.importRepoSuccess = exports.importRepo = exports.indexRepoFailed = exports.indexRepoSuccess = exports.indexRepo = exports.deleteRepoFailed = exports.deleteRepoFinished = exports.deleteRepoSuccess = exports.deleteRepo = exports.fetchReposFailed = exports.fetchReposSuccess = exports.fetchRepos = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var fetchRepos = (0, _reduxActions.createAction)('FETCH REPOS');
exports.fetchRepos = fetchRepos;
var fetchReposSuccess = (0, _reduxActions.createAction)('FETCH REPOS SUCCESS');
exports.fetchReposSuccess = fetchReposSuccess;
var fetchReposFailed = (0, _reduxActions.createAction)('FETCH REPOS FAILED');
exports.fetchReposFailed = fetchReposFailed;
var deleteRepo = (0, _reduxActions.createAction)('DELETE REPOS');
exports.deleteRepo = deleteRepo;
var deleteRepoSuccess = (0, _reduxActions.createAction)('DELETE REPOS SUCCESS');
exports.deleteRepoSuccess = deleteRepoSuccess;
var deleteRepoFinished = (0, _reduxActions.createAction)('DELETE REPOS FINISHED');
exports.deleteRepoFinished = deleteRepoFinished;
var deleteRepoFailed = (0, _reduxActions.createAction)('DELETE REPOS FAILED');
exports.deleteRepoFailed = deleteRepoFailed;
var indexRepo = (0, _reduxActions.createAction)('INDEX REPOS');
exports.indexRepo = indexRepo;
var indexRepoSuccess = (0, _reduxActions.createAction)('INDEX REPOS SUCCESS');
exports.indexRepoSuccess = indexRepoSuccess;
var indexRepoFailed = (0, _reduxActions.createAction)('INDEX REPOS FAILED');
exports.indexRepoFailed = indexRepoFailed;
var importRepo = (0, _reduxActions.createAction)('IMPORT REPO');
exports.importRepo = importRepo;
var importRepoSuccess = (0, _reduxActions.createAction)('IMPORT REPO SUCCESS');
exports.importRepoSuccess = importRepoSuccess;
var importRepoFailed = (0, _reduxActions.createAction)('IMPORT REPO FAILED');
exports.importRepoFailed = importRepoFailed;
var closeToast = (0, _reduxActions.createAction)('CLOSE TOAST');
exports.closeToast = closeToast;
var fetchRepoConfigs = (0, _reduxActions.createAction)('FETCH REPO CONFIGS');
exports.fetchRepoConfigs = fetchRepoConfigs;
var fetchRepoConfigSuccess = (0, _reduxActions.createAction)('FETCH REPO CONFIGS SUCCESS');
exports.fetchRepoConfigSuccess = fetchRepoConfigSuccess;
var fetchRepoConfigFailed = (0, _reduxActions.createAction)('FETCH REPO CONFIGS FAILED');
exports.fetchRepoConfigFailed = fetchRepoConfigFailed;
var initRepoCommand = (0, _reduxActions.createAction)('INIT REPO CMD');
exports.initRepoCommand = initRepoCommand;
var gotoRepo = (0, _reduxActions.createAction)('GOTO REPO');
exports.gotoRepo = gotoRepo;
var gotoRepoFailed = (0, _reduxActions.createAction)('GOTO REPO FAILED');
exports.gotoRepoFailed = gotoRepoFailed;
var switchLanguageServer = (0, _reduxActions.createAction)('SWITCH LANGUAGE SERVER');
exports.switchLanguageServer = switchLanguageServer;
var switchLanguageServerSuccess = (0, _reduxActions.createAction)('SWITCH LANGUAGE SERVER SUCCESS');
exports.switchLanguageServerSuccess = switchLanguageServerSuccess;
var switchLanguageServerFailed = (0, _reduxActions.createAction)('SWITCH LANGUAGE SERVER FAILED');
exports.switchLanguageServerFailed = switchLanguageServerFailed;