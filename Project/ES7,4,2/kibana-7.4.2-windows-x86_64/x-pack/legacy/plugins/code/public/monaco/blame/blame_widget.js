"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BlameWidget = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _blame = require("../../components/main/blame");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BlameWidget =
/*#__PURE__*/
function () {
  function BlameWidget(blame, isFirstLine, editor) {
    var _this = this;

    _classCallCheck(this, BlameWidget);

    this.blame = blame;
    this.isFirstLine = isFirstLine;
    this.editor = editor;

    _defineProperty(this, "allowEditorOverflow", true);

    _defineProperty(this, "suppressMouseDown", false);

    _defineProperty(this, "domNode", void 0);

    _defineProperty(this, "containerNode", void 0);

    this.containerNode = document.createElement('div');
    this.domNode = document.createElement('div');
    this.containerNode.appendChild(this.domNode);
    this.editor.onDidLayoutChange(function () {
      return _this.update();
    }); // this.editor.onDidScrollChange(e => this.update());

    this.update(); // @ts-ignore

    this.editor.addContentWidget(this);
    this.editor.layoutContentWidget(this);
  }

  _createClass(BlameWidget, [{
    key: "destroy",
    value: function destroy() {
      this.editor.removeContentWidget(this);
    }
  }, {
    key: "getDomNode",
    value: function getDomNode() {
      return this.containerNode;
    }
  }, {
    key: "getId",
    value: function getId() {
      return 'blame_' + this.blame.startLine;
    }
  }, {
    key: "getPosition",
    value: function getPosition() {
      return {
        position: {
          column: 0,
          lineNumber: this.blame.startLine
        },
        preference: [0]
      };
    }
  }, {
    key: "update",
    value: function update() {
      this.containerNode.style.width = '0px';
      var _this$editor$getConfi = this.editor.getConfiguration().fontInfo,
          fontSize = _this$editor$getConfi.fontSize,
          lineHeight = _this$editor$getConfi.lineHeight;
      this.domNode.style.position = 'relative';
      this.domNode.style.left = '-332px';
      this.domNode.style.marginLeft = '16px';
      this.domNode.style.width = '300px';
      this.domNode.style.fontSize = "".concat(fontSize, "px");
      this.domNode.style.lineHeight = "".concat(lineHeight, "px");

      var element = _react.default.createElement(_blame.Blame, {
        blame: this.blame,
        isFirstLine: this.isFirstLine
      }, null); // @ts-ignore


      _reactDom.default.render(element, this.domNode);
    }
  }]);

  return BlameWidget;
}();

exports.BlameWidget = BlameWidget;