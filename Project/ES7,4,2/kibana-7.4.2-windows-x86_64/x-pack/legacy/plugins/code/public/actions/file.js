"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fetchMoreCommits = exports.fetchTreeCommitsFailed = exports.fetchTreeCommitsSuccess = exports.fetchTreeCommits = exports.dirNotFound = exports.setNotFound = exports.fetchDirectoryFailed = exports.fetchDirectorySuccess = exports.fetchDirectory = exports.fetchFileFailed = exports.fetchFileSuccess = exports.fetchFile = exports.fetchRepoCommitsFailed = exports.fetchRepoCommitsSuccess = exports.fetchRepoCommits = exports.fetchRepoBranchesFailed = exports.fetchRepoBranchesSuccess = exports.fetchRepoBranches = exports.fetchRepoTreeFailed = exports.fetchRepoTreeSuccess = exports.fetchRepoTree = exports.fetchRootRepoTreeFailed = exports.fetchRootRepoTreeSuccess = exports.fetchRootRepoTree = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var fetchRootRepoTree = (0, _reduxActions.createAction)('FETCH ROOT REPO TREE');
exports.fetchRootRepoTree = fetchRootRepoTree;
var fetchRootRepoTreeSuccess = (0, _reduxActions.createAction)('FETCH ROOT REPO TREE SUCCESS');
exports.fetchRootRepoTreeSuccess = fetchRootRepoTreeSuccess;
var fetchRootRepoTreeFailed = (0, _reduxActions.createAction)('FETCH ROOT REPO TREE FAILED');
exports.fetchRootRepoTreeFailed = fetchRootRepoTreeFailed;
var fetchRepoTree = (0, _reduxActions.createAction)('FETCH REPO TREE');
exports.fetchRepoTree = fetchRepoTree;
var fetchRepoTreeSuccess = (0, _reduxActions.createAction)('FETCH REPO TREE SUCCESS');
exports.fetchRepoTreeSuccess = fetchRepoTreeSuccess;
var fetchRepoTreeFailed = (0, _reduxActions.createAction)('FETCH REPO TREE FAILED');
exports.fetchRepoTreeFailed = fetchRepoTreeFailed;
var fetchRepoBranches = (0, _reduxActions.createAction)('FETCH REPO BRANCHES');
exports.fetchRepoBranches = fetchRepoBranches;
var fetchRepoBranchesSuccess = (0, _reduxActions.createAction)('FETCH REPO BRANCHES SUCCESS');
exports.fetchRepoBranchesSuccess = fetchRepoBranchesSuccess;
var fetchRepoBranchesFailed = (0, _reduxActions.createAction)('FETCH REPO BRANCHES FAILED');
exports.fetchRepoBranchesFailed = fetchRepoBranchesFailed;
var fetchRepoCommits = (0, _reduxActions.createAction)('FETCH REPO COMMITS');
exports.fetchRepoCommits = fetchRepoCommits;
var fetchRepoCommitsSuccess = (0, _reduxActions.createAction)('FETCH REPO COMMITS SUCCESS');
exports.fetchRepoCommitsSuccess = fetchRepoCommitsSuccess;
var fetchRepoCommitsFailed = (0, _reduxActions.createAction)('FETCH REPO COMMITS FAILED');
exports.fetchRepoCommitsFailed = fetchRepoCommitsFailed;
var fetchFile = (0, _reduxActions.createAction)('FETCH FILE');
exports.fetchFile = fetchFile;
var fetchFileSuccess = (0, _reduxActions.createAction)('FETCH FILE SUCCESS');
exports.fetchFileSuccess = fetchFileSuccess;
var fetchFileFailed = (0, _reduxActions.createAction)('FETCH FILE ERROR');
exports.fetchFileFailed = fetchFileFailed;
var fetchDirectory = (0, _reduxActions.createAction)('FETCH REPO DIR');
exports.fetchDirectory = fetchDirectory;
var fetchDirectorySuccess = (0, _reduxActions.createAction)('FETCH REPO DIR SUCCESS');
exports.fetchDirectorySuccess = fetchDirectorySuccess;
var fetchDirectoryFailed = (0, _reduxActions.createAction)('FETCH REPO DIR FAILED');
exports.fetchDirectoryFailed = fetchDirectoryFailed;
var setNotFound = (0, _reduxActions.createAction)('SET FILE NOT FOUND');
exports.setNotFound = setNotFound;
var dirNotFound = (0, _reduxActions.createAction)('DIR NOT FOUND');
exports.dirNotFound = dirNotFound;
var fetchTreeCommits = (0, _reduxActions.createAction)('FETCH TREE COMMITS');
exports.fetchTreeCommits = fetchTreeCommits;
var fetchTreeCommitsSuccess = (0, _reduxActions.createAction)('FETCH TREE COMMITS SUCCESS');
exports.fetchTreeCommitsSuccess = fetchTreeCommitsSuccess;
var fetchTreeCommitsFailed = (0, _reduxActions.createAction)('FETCH TREE COMMITS FAILED');
exports.fetchTreeCommitsFailed = fetchTreeCommitsFailed;
var fetchMoreCommits = (0, _reduxActions.createAction)('FETCH MORE COMMITS');
exports.fetchMoreCommits = fetchMoreCommits;