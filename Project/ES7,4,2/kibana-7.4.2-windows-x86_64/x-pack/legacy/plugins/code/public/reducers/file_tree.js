"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPathOfTree = getPathOfTree;
exports.fileTree = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _model = require("../../model");

var _actions = require("../actions");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function mergeNode(a, b) {
  var childrenMap = {};

  if (a.children) {
    a.children.forEach(function (child) {
      childrenMap[child.name] = child;
    });
  }

  if (b.children) {
    b.children.forEach(function (childB) {
      var childA = childrenMap[childB.name];

      if (childA) {
        childrenMap[childB.name] = mergeNode(childA, childB);
      } else {
        childrenMap[childB.name] = childB;
      }
    });
  }

  return _objectSpread({}, a, {}, b, {
    children: Object.values(childrenMap).sort(_model.sortFileTree)
  });
}

function getPathOfTree(tree, paths) {
  var child = tree;
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    var _loop = function _loop() {
      var p = _step.value;

      if (child && child.children) {
        child = child.children.find(function (c) {
          return c.name === p;
        });
      } else {
        return {
          v: null
        };
      }
    };

    for (var _iterator = paths[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var _ret = _loop();

      if (_typeof(_ret) === "object") return _ret.v;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator.return != null) {
        _iterator.return();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  return child;
}

var initialState = {
  tree: {
    name: '',
    path: '',
    type: _model.FileTreeItemType.Directory
  },
  fileTreeLoadingPaths: [''],
  notFoundDirs: [],
  revision: ''
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.tree = initialState.tree;
    draft.fileTreeLoadingPaths = initialState.fileTreeLoadingPaths;
    draft.notFoundDirs = initialState.notFoundDirs;
    draft.revision = initialState.revision;
  });
};

var fileTree = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.fetchRepoTree), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.fileTreeLoadingPaths.push(action.payload.path);
  });
}), _defineProperty(_handleActions, String(_actions.fetchRepoTreeSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.revision = action.payload.revision;
    draft.notFoundDirs = draft.notFoundDirs.filter(function (dir) {
      return dir !== action.payload.path;
    });
    draft.fileTreeLoadingPaths = draft.fileTreeLoadingPaths.filter(function (p) {
      return p !== action.payload.path && p !== '';
    });
    var _ref = action.payload,
        tree = _ref.tree,
        path = _ref.path,
        withParents = _ref.withParents;
    var sortedTree = tree;

    while (sortedTree && sortedTree.children && sortedTree.children.length > 0) {
      sortedTree.children = sortedTree.children.sort(_model.sortFileTree); // each time the tree should only have one child that has children

      sortedTree = sortedTree.children.find(function (child) {
        return child.children && child.children.length > 0;
      });
    }

    if (withParents || path === '/' || path === '') {
      draft.tree = mergeNode(draft.tree, tree);
    } else {
      var parentsPath = path.split('/');
      var lastPath = parentsPath.pop();
      var parent = getPathOfTree(draft.tree, parentsPath);

      if (parent) {
        parent.children = parent.children || [];
        var idx = parent.children.findIndex(function (c) {
          return c.name === lastPath;
        });

        if (idx >= 0) {
          parent.children[idx] = tree;
        } else {
          parent.children.push(tree);
        }
      }
    }
  });
}), _defineProperty(_handleActions, String(_actions.fetchRootRepoTreeSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.fileTreeLoadingPaths = draft.fileTreeLoadingPaths.filter(function (p) {
      return p !== '/' && p !== '';
    });
    draft.tree = mergeNode(draft.tree, action.payload.tree);
    draft.revision = action.payload.revision;
  });
}), _defineProperty(_handleActions, String(_actions.fetchRootRepoTreeFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.fileTreeLoadingPaths = draft.fileTreeLoadingPaths.filter(function (p) {
      return p !== '/' && p !== '';
    });
  });
}), _defineProperty(_handleActions, String(_actions.dirNotFound), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.notFoundDirs.push(action.payload);
  });
}), _defineProperty(_handleActions, String(_actions.fetchRepoTreeFailed), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.fileTreeLoadingPaths = draft.fileTreeLoadingPaths.filter(function (p) {
      return p !== action.payload.path && p !== '';
    });
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _defineProperty(_handleActions, String(_route.revisionChange), clearState), _handleActions), initialState);
exports.fileTree = fileTree;