"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findDefinitionsFailed = exports.findDefinitionsSuccess = exports.findDefinitions = exports.revealPosition = exports.hoverResult = exports.closePanel = exports.findReferencesFailed = exports.findReferencesSuccess = exports.findReferences = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var findReferences = (0, _reduxActions.createAction)('FIND REFERENCES');
exports.findReferences = findReferences;
var findReferencesSuccess = (0, _reduxActions.createAction)('FIND REFERENCES SUCCESS');
exports.findReferencesSuccess = findReferencesSuccess;
var findReferencesFailed = (0, _reduxActions.createAction)('FIND REFERENCES ERROR');
exports.findReferencesFailed = findReferencesFailed;
var closePanel = (0, _reduxActions.createAction)('CLOSE PANEL');
exports.closePanel = closePanel;
var hoverResult = (0, _reduxActions.createAction)('HOVER RESULT');
exports.hoverResult = hoverResult;
var revealPosition = (0, _reduxActions.createAction)('REVEAL POSITION');
exports.revealPosition = revealPosition;
var findDefinitions = (0, _reduxActions.createAction)('FIND DEFINITIONS');
exports.findDefinitions = findDefinitions;
var findDefinitionsSuccess = (0, _reduxActions.createAction)('FIND DEFINITIONS SUCCESS');
exports.findDefinitionsSuccess = findDefinitionsSuccess;
var findDefinitionsFailed = (0, _reduxActions.createAction)('FIND DEFINITIONS ERROR');
exports.findDefinitionsFailed = findDefinitionsFailed;