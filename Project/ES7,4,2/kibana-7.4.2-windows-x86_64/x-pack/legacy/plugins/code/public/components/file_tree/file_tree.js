"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FileTree = exports.CodeFileTree = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _classnames = _interopRequireDefault(require("classnames"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _model = require("../../../model");

var _types = require("../../common/types");

var _uri_util = require("../../../common/uri_util");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CodeFileTree =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CodeFileTree, _React$Component);

  _createClass(CodeFileTree, null, [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      return {
        openPaths: CodeFileTree.getOpenPaths(props.match.params.path || '', state.openPaths)
      };
    }
  }]);

  function CodeFileTree(props) {
    var _this;

    _classCallCheck(this, CodeFileTree);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CodeFileTree).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "openTreePath", function (path) {
      var newClosedPaths = _this.state.closedPaths.filter(function (p) {
        return !(p === path);
      });

      _this.setState({
        openPaths: CodeFileTree.getOpenPaths(path, _this.state.openPaths),
        closedPaths: newClosedPaths
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeTreePath", function (path) {
      var isSubFolder = function isSubFolder(p) {
        return p.startsWith(path + '/');
      };

      var newOpenPaths = _this.state.openPaths.filter(function (p) {
        return !(p === path || isSubFolder(p));
      });

      var newClosedPaths = [].concat(_toConsumableArray(_this.state.closedPaths), [path]);

      _this.setState({
        openPaths: newOpenPaths,
        closedPaths: newClosedPaths
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onClick", function (node) {
      var _this$props$match$par = _this.props.match.params,
          resource = _this$props$match$par.resource,
          org = _this$props$match$par.org,
          repo = _this$props$match$par.repo,
          revision = _this$props$match$par.revision,
          path = _this$props$match$par.path;

      if (!(path === node.path)) {
        var pathType;

        if (node.type === _model.FileTreeItemType.Link || node.type === _model.FileTreeItemType.File) {
          pathType = _types.PathTypes.blob;
        } else {
          pathType = _types.PathTypes.tree;
        }

        _this.props.history.push("/".concat(resource, "/").concat(org, "/").concat(repo, "/").concat(pathType, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(node.path));
      }
    });

    _defineProperty(_assertThisInitialized(_this), "toggleTree", function (path) {
      if (_this.isPathOpen(path)) {
        _this.closeTreePath(path);
      } else {
        _this.openTreePath(path);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "flattenDirectory", function (node) {
      if (node.childrenCount === 1 && node.children[0].type === _model.FileTreeItemType.Directory) {
        if (node.children[0].path === _this.props.match.params.path) {
          return [node, node.children[0]];
        } else {
          return [node].concat(_toConsumableArray(_this.flattenDirectory(node.children[0])));
        }
      } else {
        return [node];
      }
    });

    _defineProperty(_assertThisInitialized(_this), "getItemRenderer", function (node, forceOpen, flattenFrom) {
      return function () {
        var _nodeTypeMap;

        var className = 'codeFileTree__item kbn-resetFocusState';

        var onClick = function onClick() {
          var path = flattenFrom ? flattenFrom.path : node.path;

          _this.toggleTree(path);

          _this.onClick(node); // track file tree click count


          (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.COUNT, _usage_telemetry_metrics.CodeUIUsageMetrics.FILE_TREE_CLICK_COUNT);
        };

        var nodeTypeMap = (_nodeTypeMap = {}, _defineProperty(_nodeTypeMap, _model.FileTreeItemType.Directory, 'Directory'), _defineProperty(_nodeTypeMap, _model.FileTreeItemType.File, 'File'), _defineProperty(_nodeTypeMap, _model.FileTreeItemType.Link, 'Link'), _defineProperty(_nodeTypeMap, _model.FileTreeItemType.Submodule, 'Submodule'), _nodeTypeMap);

        var bg = _react.default.createElement("div", {
          tabIndex: 0,
          className: "codeFileTree__node--link",
          "data-test-subj": "codeFileTreeNode-".concat(nodeTypeMap[node.type], "-").concat(node.path),
          onClick: onClick,
          onKeyDown: onClick,
          role: "button"
        });

        if (_this.props.match.params.path === node.path) {
          bg = _react.default.createElement("div", {
            ref: function ref(el) {
              return _this.scrollIntoView(el);
            },
            className: "codeFileTree__node--fullWidth",
            tabIndex: 0,
            "data-test-subj": "codeFileTreeNode-".concat(nodeTypeMap[node.type], "-").concat(node.path),
            onClick: onClick,
            onKeyDown: onClick,
            role: "button"
          });
        }

        switch (node.type) {
          case _model.FileTreeItemType.Directory:
            {
              return _react.default.createElement("div", {
                className: "codeFileTree__node"
              }, _react.default.createElement("div", {
                className: className,
                role: "button",
                tabIndex: 0,
                onKeyDown: onClick,
                onClick: onClick
              }, forceOpen ? _react.default.createElement(_eui.EuiIcon, {
                type: "arrowDown",
                size: "s",
                className: "codeFileTree__icon"
              }) : _react.default.createElement(_eui.EuiIcon, {
                type: "arrowRight",
                size: "s",
                className: "codeFileTree__icon"
              }), _react.default.createElement(_eui.EuiIcon, {
                type: forceOpen ? 'folderOpen' : 'folderClosed',
                "data-test-subj": "codeFileTreeNode-Directory-Icon-".concat(node.path, "-").concat(forceOpen ? 'open' : 'closed')
              }), _react.default.createElement("span", {
                className: "codeFileTree__directory"
              }, _react.default.createElement(_eui.EuiText, {
                size: "xs",
                grow: false,
                className: "eui-displayInlineBlock"
              }, node.name))), bg);
            }

          case _model.FileTreeItemType.Submodule:
            {
              return _react.default.createElement("div", {
                className: "codeFileTree__node"
              }, _react.default.createElement("div", {
                tabIndex: 0,
                onKeyDown: onClick,
                onClick: onClick,
                className: (0, _classnames.default)(className, 'codeFileTree__file'),
                role: "button"
              }, _react.default.createElement(_eui.EuiIcon, {
                type: "submodule"
              }), _react.default.createElement("span", {
                className: "codeFileTree__directory"
              }, _react.default.createElement(_eui.EuiText, {
                size: "xs",
                grow: false,
                color: "default",
                className: "eui-displayInlineBlock"
              }, node.name))), bg);
            }

          case _model.FileTreeItemType.Link:
            {
              return _react.default.createElement("div", {
                className: "codeFileTree__node"
              }, _react.default.createElement("div", {
                tabIndex: 0,
                onKeyDown: onClick,
                onClick: onClick,
                className: (0, _classnames.default)(className, 'codeFileTree__file'),
                role: "button"
              }, _react.default.createElement(_eui.EuiIcon, {
                type: "symlink"
              }), _react.default.createElement("span", {
                className: "codeFileTree__directory"
              }, _react.default.createElement(_eui.EuiText, {
                size: "xs",
                grow: false,
                color: "default",
                className: "eui-displayInlineBlock"
              }, node.name))), bg);
            }

          case _model.FileTreeItemType.File:
            {
              return _react.default.createElement("div", {
                className: "codeFileTree__node"
              }, _react.default.createElement("div", {
                tabIndex: 0,
                onKeyDown: onClick,
                onClick: onClick,
                className: (0, _classnames.default)(className, 'codeFileTree__file'),
                role: "button"
              }, _react.default.createElement(_eui.EuiIcon, {
                type: "document"
              }), _react.default.createElement("span", {
                className: "codeFileTree__directory"
              }, _react.default.createElement(_eui.EuiText, {
                size: "xs",
                grow: false,
                color: "default",
                className: "eui-displayInlineBlock"
              }, node.name))), bg);
            }
        }
      };
    });

    _defineProperty(_assertThisInitialized(_this), "treeToItems", function (node) {
      var forceOpen = node.type === _model.FileTreeItemType.Directory ? _this.isPathOpen(node.path) : false;
      var data = {
        id: node.name,
        name: node.name,
        isSelected: false,
        renderItem: _this.getItemRenderer(node, forceOpen),
        forceOpen: forceOpen,
        onClick: function onClick() {
          return void 0;
        }
      };

      if (node.type === _model.FileTreeItemType.Directory && Number(node.childrenCount) > 0) {
        var nodes = _this.flattenDirectory(node);

        var length = nodes.length;

        if (length > 1 && !(_this.props.match.params.path === node.path)) {
          data.name = nodes.map(function (n) {
            return n.name;
          }).join('/');
          data.id = data.name;
          var lastNode = nodes[length - 1];

          var flattenNode = _objectSpread({}, lastNode, {
            name: data.name,
            id: data.id
          });

          data.forceOpen = _this.isPathOpen(node.path);
          data.renderItem = _this.getItemRenderer(flattenNode, data.forceOpen, node);

          if (data.forceOpen && Number(flattenNode.childrenCount) > 0) {
            data.items = flattenNode.children.map(_this.treeToItems);
          }
        } else if (forceOpen && node.children) {
          data.items = node.children.map(_this.treeToItems);
        }
      }

      return data;
    });

    var _path = props.match.params.path;

    if (_path) {
      _this.state = {
        openPaths: CodeFileTree.getOpenPaths(_path, []),
        closedPaths: []
      };
    } else {
      _this.state = {
        openPaths: [],
        closedPaths: []
      };
    }

    return _this;
  }

  _createClass(CodeFileTree, [{
    key: "scrollIntoView",
    value: function scrollIntoView(el) {
      if (el) {
        var rect = el.getBoundingClientRect();
        var elemTop = rect.top;
        var elemBottom = rect.bottom;
        var isVisible = elemTop >= 0 && elemBottom <= window.innerHeight;

        if (!isVisible) {
          el.scrollIntoView({
            behavior: 'smooth',
            block: 'center',
            inline: 'center'
          });
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var items = [{
        name: '',
        id: '',
        items: (this.props.node.children || []).map(this.treeToItems)
      }];
      return this.props.node && _react.default.createElement(_eui.EuiSideNav, {
        items: items,
        isOpenOnMobile: true,
        className: "codeContainer__sideTabTree"
      });
    }
  }, {
    key: "isPathOpen",
    value: function isPathOpen(path) {
      if (this.props.isNotFound) return false;
      return this.state.openPaths.includes(path) && !this.state.closedPaths.includes(path);
    }
  }]);

  return CodeFileTree;
}(_react.default.Component);

exports.CodeFileTree = CodeFileTree;

_defineProperty(CodeFileTree, "getOpenPaths", function (path, openPaths) {
  var p = path;

  var newOpenPaths = _toConsumableArray(openPaths);

  var pathSegs = p.split('/');

  while (!openPaths.includes(p)) {
    newOpenPaths.push(p);
    pathSegs.pop();

    if (pathSegs.length <= 0) {
      break;
    }

    p = pathSegs.join('/');
  }

  return newOpenPaths;
});

var mapStateToProps = function mapStateToProps(state) {
  return {
    node: state.fileTree.tree,
    isNotFound: state.file.isNotFound
  };
};

var FileTree = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps)(CodeFileTree));
exports.FileTree = FileTree;