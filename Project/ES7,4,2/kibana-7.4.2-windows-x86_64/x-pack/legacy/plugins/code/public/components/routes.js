"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SEARCH = exports.ADMIN = exports.MAIN_ROOT = exports.REPO = exports.DIFF = exports.MAIN = exports.SETUP = exports.ROOT = void 0;

var _types = require("../common/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ROOT = '/';
exports.ROOT = ROOT;
var SETUP = '/setup-guide';
exports.SETUP = SETUP;
var pathTypes = ":pathType(".concat(_types.PathTypes.blob, "|").concat(_types.PathTypes.tree, "|").concat(_types.PathTypes.blame, "|").concat(_types.PathTypes.commits, ")");
var MAIN = "/:resource/:org/:repo/".concat(pathTypes, "/:revision/:path*:goto(!.*)?");
exports.MAIN = MAIN;
var DIFF = '/:resource/:org/:repo/commit/:commitId';
exports.DIFF = DIFF;
var REPO = "/:resource/:org/:repo";
exports.REPO = REPO;
var MAIN_ROOT = "/:resource/:org/:repo/".concat(pathTypes, "/:revision");
exports.MAIN_ROOT = MAIN_ROOT;
var ADMIN = '/admin';
exports.ADMIN = ADMIN;
var SEARCH = '/search';
exports.SEARCH = SEARCH;