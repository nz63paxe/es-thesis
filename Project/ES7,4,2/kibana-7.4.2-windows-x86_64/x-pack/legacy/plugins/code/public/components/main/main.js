"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Main = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _new_platform = require("ui/new_platform");

var _constants = require("../../../common/constants");

var _shortcuts = require("../shortcuts");

var _content = require("./content");

var _side_tabs = require("./side_tabs");

var _selectors = require("../../selectors");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CodeMain =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CodeMain, _React$Component);

  function CodeMain() {
    _classCallCheck(this, CodeMain);

    return _possibleConstructorReturn(this, _getPrototypeOf(CodeMain).apply(this, arguments));
  }

  _createClass(CodeMain, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.setBreadcrumbs(); // track source page load count

      (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.LOADED, _usage_telemetry_metrics.CodeUIUsageMetrics.SOURCE_VIEW_PAGE_LOAD_COUNT);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.setBreadcrumbs();
    }
  }, {
    key: "setBreadcrumbs",
    value: function setBreadcrumbs() {
      var _this$props$match$par = this.props.match.params,
          resource = _this$props$match$par.resource,
          org = _this$props$match$par.org,
          repo = _this$props$match$par.repo;

      _new_platform.npStart.core.chrome.setBreadcrumbs([{
        text: _constants.APP_TITLE,
        href: '#/'
      }, {
        text: "".concat(org, " \u2192 ").concat(repo),
        href: "#/".concat(resource, "/").concat(org, "/").concat(repo)
      }]);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _new_platform.npStart.core.chrome.setBreadcrumbs([{
        text: _constants.APP_TITLE,
        href: '#/'
      }]);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          loadingFileTree = _this$props.loadingFileTree,
          loadingStructureTree = _this$props.loadingStructureTree,
          hasStructure = _this$props.hasStructure,
          languageServerInitializing = _this$props.languageServerInitializing;
      return _react.default.createElement("div", {
        className: "codeContainer__root"
      }, _react.default.createElement("div", {
        className: "codeContainer__rootInner"
      }, _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_side_tabs.SideTabs, {
        currentTree: this.props.currentTree,
        loadingFileTree: loadingFileTree,
        loadingStructureTree: loadingStructureTree,
        hasStructure: hasStructure,
        languageServerInitializing: languageServerInitializing
      }), _react.default.createElement(_content.Content, null))), _react.default.createElement(_shortcuts.ShortcutsProvider, null));
    }
  }]);

  return CodeMain;
}(_react.default.Component);

var mapStateToProps = function mapStateToProps(state) {
  return {
    loadingFileTree: state.fileTree.fileTreeLoadingPaths.includes(''),
    loadingStructureTree: state.symbol.loading,
    hasStructure: (0, _selectors.structureSelector)(state).length > 0 && !state.symbol.error,
    languageServerInitializing: state.symbol.languageServerInitializing,
    currentTree: (0, _selectors.currentTreeSelector)(state)
  };
};

var Main = (0, _reactRedux.connect)(mapStateToProps)(CodeMain);
exports.Main = Main;