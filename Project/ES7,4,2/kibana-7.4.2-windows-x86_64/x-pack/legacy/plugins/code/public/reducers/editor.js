"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.editor = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  loading: false,
  panelShowing: undefined,
  panelContents: [],
  panelTitle: ''
};

function panelInit(draft, action) {
  draft.refPayload = action.payload;
  draft.loading = true;
  draft.panelContents = initialState.panelContents;
  draft.panelTitle = initialState.panelTitle;
}

function panelSuccess(draft, action) {
  var _ref = action.payload,
      title = _ref.title,
      repos = _ref.repos;
  draft.panelContents = repos;
  draft.panelTitle = title;
  draft.loading = false;
}

function panelFailed(draft) {
  draft.panelContents = [];
  draft.loading = false;
  delete draft.refPayload;
}

var editor = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.findReferences), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    panelInit(draft, action);
    draft.panelShowing = 'references';
  });
}), _defineProperty(_handleActions, String(_actions.findReferencesSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    panelSuccess(draft, action);
  });
}), _defineProperty(_handleActions, String(_actions.findReferencesFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    panelFailed(draft);
  });
}), _defineProperty(_handleActions, String(_actions.findDefinitions), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    panelInit(draft, action);
    draft.panelShowing = 'definitions';
  });
}), _defineProperty(_handleActions, String(_actions.findDefinitionsSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    panelSuccess(draft, action);
  });
}), _defineProperty(_handleActions, String(_actions.findDefinitionsFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    panelFailed(draft);
  });
}), _defineProperty(_handleActions, String(_actions.closePanel), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.panelShowing = undefined;
    draft.loading = false;
    delete draft.refPayload;
    draft.panelContents = [];
    draft.panelTitle = '';
  });
}), _defineProperty(_handleActions, String(_actions.hoverResult), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.currentHover = action.payload;
  });
}), _defineProperty(_handleActions, String(_actions.revealPosition), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.revealPosition = action.payload;
  });
}), _handleActions), initialState);
exports.editor = editor;