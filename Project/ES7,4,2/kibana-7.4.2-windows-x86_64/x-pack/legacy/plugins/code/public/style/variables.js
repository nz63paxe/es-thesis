"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.px = px;
exports.percent = percent;
exports.pxToRem = pxToRem;
exports.fontFamily = exports.fontSizes = exports.colors = exports.rem = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var rem = 14;
exports.rem = rem;

function px(value) {
  return "".concat(value, "px");
}

function percent(value) {
  return "".concat(value, "%");
}

function pxToRem(value) {
  return "".concat(value / rem, "rem");
}

var colors = {
  textBlue: '#0079A5',
  borderGrey: '#D9D9D9',
  white: '#fff',
  textGrey: '#3F3F3F'
};
exports.colors = colors;
var fontSizes = {
  small: '10px',
  normal: '1rem',
  large: '18px',
  xlarge: '2rem'
};
exports.fontSizes = fontSizes;
var fontFamily = 'SFProText-Regular';
exports.fontFamily = fontFamily;