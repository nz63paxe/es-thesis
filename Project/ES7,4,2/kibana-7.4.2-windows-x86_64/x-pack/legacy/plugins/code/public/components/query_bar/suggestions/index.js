"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  AutocompleteSuggestionType: true
};
exports.AutocompleteSuggestionType = void 0;

var _suggestions_provider = require("./suggestions_provider");

Object.keys(_suggestions_provider).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _suggestions_provider[key];
    }
  });
});

var _symbol_suggestions_provider = require("./symbol_suggestions_provider");

Object.keys(_symbol_suggestions_provider).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _symbol_suggestions_provider[key];
    }
  });
});

var _file_suggestions_provider = require("./file_suggestions_provider");

Object.keys(_file_suggestions_provider).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _file_suggestions_provider[key];
    }
  });
});

var _repository_suggestions_provider = require("./repository_suggestions_provider");

Object.keys(_repository_suggestions_provider).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _repository_suggestions_provider[key];
    }
  });
});

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AutocompleteSuggestionType;
exports.AutocompleteSuggestionType = AutocompleteSuggestionType;

(function (AutocompleteSuggestionType) {
  AutocompleteSuggestionType["SYMBOL"] = "symbol";
  AutocompleteSuggestionType["FILE"] = "file";
  AutocompleteSuggestionType["REPOSITORY"] = "repository";
})(AutocompleteSuggestionType || (exports.AutocompleteSuggestionType = AutocompleteSuggestionType = {}));