"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotFound = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _error_panel = require("./error_panel");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var NotFound = function NotFound() {
  return _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "flexStart"
  }, _react.default.createElement(_error_panel.ErrorPanel, {
    title: _react.default.createElement("h2", null, "404"),
    content: "Unfortunately that page doesn\u2019t exist. You can try searching to find what you\u2019re looking for."
  }));
};

exports.NotFound = NotFound;