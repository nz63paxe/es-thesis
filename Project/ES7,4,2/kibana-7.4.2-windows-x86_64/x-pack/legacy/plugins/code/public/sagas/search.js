"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchDocumentSearch = watchDocumentSearch;
exports.watchRepositorySearch = watchRepositorySearch;
exports.watchSearchRouteChange = watchSearchRouteChange;
exports.watchRepoScopeSearch = watchRepoScopeSearch;

var _querystring = _interopRequireDefault(require("querystring"));

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _model = require("../../model");

var _actions = require("../actions");

var _patterns = require("./patterns");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleDocumentSearch),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchDocumentSearch),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepositorySearch),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepositorySearch),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(handleSearchRouteChange),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(resetDefaultRepoScope),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(watchSearchRouteChange),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(handleReposSearchForScope),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoScopeSearch);

function requestDocumentSearch(payload) {
  var query = payload.query,
      page = payload.page,
      languages = payload.languages,
      repositories = payload.repositories,
      repoScope = payload.repoScope;
  var queryParams = {
    q: query
  };

  if (page) {
    queryParams.p = page;
  }

  if (languages) {
    queryParams.langs = languages;
  }

  if (repositories) {
    queryParams.repos = repositories;
  }

  if (repoScope) {
    queryParams.repoScope = repoScope;
  }

  if (query && query.length > 0) {
    return _new_platform.npStart.core.http.get("/api/code/search/doc", {
      query: queryParams
    });
  } else {
    return {
      documents: [],
      took: 0,
      total: 0
    };
  }
}

function handleDocumentSearch(action) {
  var data;
  return regeneratorRuntime.wrap(function handleDocumentSearch$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.call)(requestDocumentSearch, action.payload);

        case 3:
          data = _context.sent;
          _context.next = 6;
          return (0, _effects.put)((0, _actions.documentSearchSuccess)(data));

        case 6:
          _context.next = 12;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          _context.next = 12;
          return (0, _effects.put)((0, _actions.documentSearchFailed)(_context.t0));

        case 12:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 8]]);
}

function requestRepositorySearch(q) {
  return _new_platform.npStart.core.http.get("/api/code/search/repo", {
    query: {
      q: q
    }
  });
}

function watchDocumentSearch() {
  return regeneratorRuntime.wrap(function watchDocumentSearch$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeLatest)(String(_actions.documentSearch), handleDocumentSearch);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function handleRepositorySearch(action) {
  var _data;

  return regeneratorRuntime.wrap(function handleRepositorySearch$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.call)(requestRepositorySearch, action.payload.query);

        case 3:
          _data = _context3.sent;
          _context3.next = 6;
          return (0, _effects.put)((0, _actions.repositorySearchSuccess)(_data));

        case 6:
          _context3.next = 12;
          break;

        case 8:
          _context3.prev = 8;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 12;
          return (0, _effects.put)((0, _actions.repositorySearchFailed)(_context3.t0));

        case 12:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 8]]);
}

function watchRepositorySearch() {
  return regeneratorRuntime.wrap(function watchRepositorySearch$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeLatest)([String(_actions.repositorySearch), String(_actions.repositorySearchQueryChanged)], handleRepositorySearch);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function handleSearchRouteChange(action) {
  var _ref, location, rawSearchStr, queryParams, q, p, langs, repos, scope, repoScope;

  return regeneratorRuntime.wrap(function handleSearchRouteChange$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _ref = action.payload, location = _ref.location;
          rawSearchStr = location.search.length > 0 ? location.search.substring(1) : '';
          queryParams = _querystring.default.parse(rawSearchStr);
          q = queryParams.q, p = queryParams.p, langs = queryParams.langs, repos = queryParams.repos, scope = queryParams.scope, repoScope = queryParams.repoScope;
          _context5.next = 6;
          return (0, _effects.put)((0, _actions.changeSearchScope)(scope));

        case 6:
          if (!(scope === _model.SearchScope.REPOSITORY)) {
            _context5.next = 11;
            break;
          }

          _context5.next = 9;
          return (0, _effects.put)((0, _actions.repositorySearch)({
            query: q
          }));

        case 9:
          _context5.next = 13;
          break;

        case 11:
          _context5.next = 13;
          return (0, _effects.put)((0, _actions.documentSearch)({
            query: q,
            page: p,
            languages: langs,
            repositories: repos,
            repoScope: repoScope
          }));

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

function resetDefaultRepoScope() {
  return regeneratorRuntime.wrap(function resetDefaultRepoScope$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.put)((0, _actions.turnOffDefaultRepoScope)());

        case 2:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function watchSearchRouteChange() {
  return regeneratorRuntime.wrap(function watchSearchRouteChange$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _effects.takeLatest)(_patterns.searchRoutePattern, handleSearchRouteChange);

        case 2:
          _context7.next = 4;
          return (0, _effects.takeLatest)(_patterns.adminRoutePattern, resetDefaultRepoScope);

        case 4:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

function handleReposSearchForScope(action) {
  var _data2;

  return regeneratorRuntime.wrap(function handleReposSearchForScope$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _context8.next = 3;
          return (0, _effects.call)(requestRepositorySearch, action.payload.query);

        case 3:
          _data2 = _context8.sent;
          _context8.next = 6;
          return (0, _effects.put)((0, _actions.searchReposForScopeSuccess)(_data2));

        case 6:
          _context8.next = 12;
          break;

        case 8:
          _context8.prev = 8;
          _context8.t0 = _context8["catch"](0);
          _context8.next = 12;
          return (0, _effects.put)((0, _actions.searchReposForScopeFailed)(_context8.t0));

        case 12:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, null, [[0, 8]]);
}

function watchRepoScopeSearch() {
  return regeneratorRuntime.wrap(function watchRepoScopeSearch$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _effects.takeEvery)(_actions.searchReposForScope, handleReposSearchForScope);

        case 2:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}