"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.App = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _reactRedux = require("react-redux");

var _admin = require("./admin_page/admin");

var _setup_guide = require("./admin_page/setup_guide");

var _diff = require("./diff_page/diff");

var _main = require("./main/main");

var _not_found = require("./main/not_found");

var _route = require("./route");

var ROUTES = _interopRequireWildcard(require("./routes"));

var _search = require("./search_page/search");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RooComponent = function RooComponent(props) {
  if (props.setupOk) {
    return _react.default.createElement(_reactRouterDom.Redirect, {
      to: '/admin'
    });
  }

  return _react.default.createElement(_setup_guide.SetupGuide, null);
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    setupOk: state.setup.ok
  };
};

var Root = (0, _reactRedux.connect)(mapStateToProps)(RooComponent);

var Empty = function Empty() {
  return null;
};

var App = function App() {
  return _react.default.createElement(_reactRouterDom.HashRouter, null, _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_route.Route, {
    path: ROUTES.DIFF,
    exact: true,
    component: _diff.Diff
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.ROOT,
    exact: true,
    component: Root
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.MAIN,
    component: _main.Main,
    exact: true
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.MAIN_ROOT,
    component: _main.Main
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.ADMIN,
    component: _admin.Admin
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.SEARCH,
    component: _search.Search
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.REPO,
    render: Empty,
    exact: true
  }), _react.default.createElement(_route.Route, {
    path: ROUTES.SETUP,
    component: _setup_guide.SetupGuide,
    exact: true
  }), _react.default.createElement(_route.Route, {
    path: "*",
    component: _not_found.NotFound
  })));
};

exports.App = App;