"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Blame = void 0;

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Blame =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Blame, _React$PureComponent);

  function Blame() {
    _classCallCheck(this, Blame);

    return _possibleConstructorReturn(this, _getPrototypeOf(Blame).apply(this, arguments));
  }

  _createClass(Blame, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          blame = _this$props.blame,
          isFirstLine = _this$props.isFirstLine;
      return _react.default.createElement(_eui.EuiFlexGroup, {
        className: isFirstLine ? 'codeBlame__item codeBlame__item--first ' : 'codeBlame__item',
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_eui.EuiText, {
        size: "xs",
        className: "codeText__blameMessage eui-textTruncate"
      }, blame.commit.message)), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false,
        className: "eui-textTruncate"
      }, _react.default.createElement(_eui.EuiText, {
        size: "xs",
        className: "eui-textTruncate code-auto-margin"
      }, _react.default.createElement(_eui.EuiTextColor, {
        color: "subdued"
      }, (0, _moment.default)(blame.commit.date).fromNow()))));
    }
  }]);

  return Blame;
}(_react.default.PureComponent);

exports.Blame = Blame;