"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadConfigsFailed = exports.loadConfigsSuccess = exports.loadConfigs = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadConfigs = (0, _reduxActions.createAction)('LOAD CONFIGS');
exports.loadConfigs = loadConfigs;
var loadConfigsSuccess = (0, _reduxActions.createAction)('LOAD CONFIGS SUCCESS');
exports.loadConfigsSuccess = loadConfigsSuccess;
var loadConfigsFailed = (0, _reduxActions.createAction)('LOAD CONFIGS FAILED');
exports.loadConfigsFailed = loadConfigsFailed;