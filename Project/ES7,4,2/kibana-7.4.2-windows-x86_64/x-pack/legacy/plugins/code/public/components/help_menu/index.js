"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "HelpMenu", {
  enumerable: true,
  get: function get() {
    return _help_menu.HelpMenu;
  }
});

var _help_menu = require("./help_menu");