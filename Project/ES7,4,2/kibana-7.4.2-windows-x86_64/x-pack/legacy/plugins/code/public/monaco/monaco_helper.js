"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonacoHelper = void 0;

var _resize_checker = require("ui/resize_checker");

var _uri_util = require("../../common/uri_util");

var _editor_service = require("./editor_service");

var _hover_controller = require("./hover/hover_controller");

var _monaco = require("./monaco");

var _references_action = require("./references/references_action");

var _single_selection_helper = require("./single_selection_helper");

var _textmodel_resolver = require("./textmodel_resolver");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MonacoHelper =
/*#__PURE__*/
function () {
  _createClass(MonacoHelper, [{
    key: "initialized",
    get: function get() {
      return this.monaco !== null;
    }
  }]);

  function MonacoHelper(container, editorActions, urlQuery) {
    var _this = this;

    _classCallCheck(this, MonacoHelper);

    this.container = container;
    this.editorActions = editorActions;
    this.urlQuery = urlQuery;

    _defineProperty(this, "decorations", []);

    _defineProperty(this, "editor", null);

    _defineProperty(this, "monaco", null);

    _defineProperty(this, "resizeChecker", null);

    _defineProperty(this, "updateUrlQuery", function (q) {
      _this.urlQuery = q;
    });

    _defineProperty(this, "getUrlQuery", function () {
      return _this.urlQuery;
    });

    _defineProperty(this, "destroy", function () {
      _this.monaco = null;
      document.removeEventListener('copy', _this.handleCopy);
      document.removeEventListener('cut', _this.handleCopy);

      if (_this.resizeChecker) {
        _this.resizeChecker.destroy();
      }
    });

    this.handleCopy = this.handleCopy.bind(this);
  }

  _createClass(MonacoHelper, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      return new Promise(function (resolve) {
        _this2.monaco = _monaco.monaco;
        var codeEditorService = new _editor_service.EditorService(_this2.getUrlQuery);
        codeEditorService.setMonacoHelper(_this2);
        _this2.editor = _monaco.monaco.editor.create(_this2.container, {
          readOnly: true,
          minimap: {
            enabled: false
          },
          hover: {
            enabled: false // disable default hover;

          },
          occurrencesHighlight: false,
          selectionHighlight: false,
          renderLineHighlight: 'none',
          contextmenu: false,
          folding: true,
          scrollBeyondLastLine: false,
          renderIndentGuides: false,
          automaticLayout: false,
          lineDecorationsWidth: 16
        }, {
          textModelService: new _textmodel_resolver.TextModelResolverService(_monaco.monaco),
          codeEditorService: codeEditorService
        });
        (0, _single_selection_helper.registerEditor)(_this2.editor);
        _this2.resizeChecker = new _resize_checker.ResizeChecker(_this2.container);

        _this2.resizeChecker.on('resize', function () {
          setTimeout(function () {
            _this2.editor.layout();
          });
        });

        (0, _references_action.registerReferencesAction)(_this2.editor, _this2.getUrlQuery);
        var hoverController = new _hover_controller.HoverController(_this2.editor);
        hoverController.setReduxActions(_this2.editorActions);
        document.addEventListener('copy', _this2.handleCopy);
        document.addEventListener('cut', _this2.handleCopy);
        resolve(_this2.editor);
      });
    }
  }, {
    key: "loadFile",
    value: function () {
      var _loadFile = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(repoUri, file, text, lang) {
        var revision,
            ed,
            oldModel,
            uri,
            newModel,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                revision = _args.length > 4 && _args[4] !== undefined ? _args[4] : 'master';

                if (this.initialized) {
                  _context.next = 4;
                  break;
                }

                _context.next = 4;
                return this.init();

              case 4:
                ed = this.editor;
                oldModel = ed.getModel();

                if (oldModel) {
                  oldModel.dispose();
                }

                ed.setModel(null);
                uri = this.monaco.Uri.parse((0, _uri_util.toCanonicalUrl)({
                  schema: 'git:',
                  repoUri: repoUri,
                  file: file,
                  revision: revision,
                  pathType: 'blob'
                }));
                newModel = this.monaco.editor.getModel(uri);

                if (!newModel) {
                  newModel = this.monaco.editor.createModel(text, lang, uri);
                } else {
                  newModel.setValue(text);
                }

                ed.setModel(newModel);
                return _context.abrupt("return", ed);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function loadFile(_x, _x2, _x3, _x4) {
        return _loadFile.apply(this, arguments);
      }

      return loadFile;
    }()
  }, {
    key: "revealPosition",
    value: function revealPosition(line, pos) {
      var position = {
        lineNumber: line,
        column: pos
      };
      this.decorations = this.editor.deltaDecorations(this.decorations, [{
        range: new this.monaco.Range(line, 0, line, 0),
        options: {
          isWholeLine: true,
          className: "code-monaco-highlight-line code-line-number-".concat(line),
          linesDecorationsClassName: 'code-mark-line-number'
        }
      }]);
      this.editor.setPosition(position);
      this.editor.revealLineInCenterIfOutsideViewport(line);
    }
  }, {
    key: "clearLineSelection",
    value: function clearLineSelection() {
      if (this.editor) {
        this.decorations = this.editor.deltaDecorations(this.decorations, []);
      }
    }
  }, {
    key: "handleCopy",
    value: function handleCopy(e) {
      if (this.editor && this.editor.hasTextFocus() && this.editor.hasWidgetFocus()) {
        var selection = this.editor.getSelection();

        if (selection && !selection.isEmpty()) {
          var text = this.editor.getModel().getValueInRange(selection);
          e.clipboardData.setData('text/plain', text);
          e.preventDefault();
        }
      }
    }
  }]);

  return MonacoHelper;
}();

exports.MonacoHelper = MonacoHelper;