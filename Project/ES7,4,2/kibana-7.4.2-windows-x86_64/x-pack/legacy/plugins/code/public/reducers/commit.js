"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.commit = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _commit = require("../actions/commit");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  commit: null,
  loading: false
};
var commit = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_commit.loadCommit), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_commit.loadCommitSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.commit = action.payload;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_commit.loadCommitFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.commit = null;
    draft.loading = false;
  });
}), _handleActions), initialState);
exports.commit = commit;