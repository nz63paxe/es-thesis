"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AbstractSuggestionsProvider = void 0;

var _ = require(".");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AbstractSuggestionsProvider =
/*#__PURE__*/
function () {
  function AbstractSuggestionsProvider() {
    _classCallCheck(this, AbstractSuggestionsProvider);

    _defineProperty(this, "MAX_SUGGESTIONS_PER_GROUP", 5);
  }

  _createClass(AbstractSuggestionsProvider, [{
    key: "getSuggestions",
    value: function () {
      var _getSuggestions = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(query, scope, repoScope) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!this.matchSearchScope(scope)) {
                  _context.next = 6;
                  break;
                }

                _context.next = 3;
                return this.fetchSuggestions(query, repoScope);

              case 3:
                return _context.abrupt("return", _context.sent);

              case 6:
                return _context.abrupt("return", new Promise(function (resolve, reject) {
                  resolve({
                    type: _.AutocompleteSuggestionType.SYMBOL,
                    total: 0,
                    hasMore: false,
                    suggestions: []
                  });
                }));

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getSuggestions(_x, _x2, _x3) {
        return _getSuggestions.apply(this, arguments);
      }

      return getSuggestions;
    }()
  }, {
    key: "fetchSuggestions",
    value: function () {
      var _fetchSuggestions = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(query, repoScope) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", new Promise(function (resolve, reject) {
                  resolve({
                    type: _.AutocompleteSuggestionType.SYMBOL,
                    total: 0,
                    hasMore: false,
                    suggestions: []
                  });
                }));

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function fetchSuggestions(_x4, _x5) {
        return _fetchSuggestions.apply(this, arguments);
      }

      return fetchSuggestions;
    }()
  }, {
    key: "matchSearchScope",
    value: function matchSearchScope(scope) {
      return true;
    }
  }]);

  return AbstractSuggestionsProvider;
}();

exports.AbstractSuggestionsProvider = AbstractSuggestionsProvider;