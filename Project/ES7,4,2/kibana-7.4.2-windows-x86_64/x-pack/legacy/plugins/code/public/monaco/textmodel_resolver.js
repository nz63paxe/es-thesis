"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TextModelResolverService = void 0;

var _new_platform = require("ui/new_platform");

var _immortal_reference = require("./immortal_reference");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var TextModelResolverService =
/*#__PURE__*/
function () {
  function TextModelResolverService(monaco) {
    _classCallCheck(this, TextModelResolverService);

    this.monaco = monaco;
  }

  _createClass(TextModelResolverService, [{
    key: "createModelReference",
    value: function () {
      var _createModelReference = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(resource) {
        var model, result;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                model = this.monaco.editor.getModel(resource);

                if (model) {
                  _context.next = 10;
                  break;
                }

                _context.next = 4;
                return this.fetchText(resource);

              case 4:
                result = _context.sent;

                if (result) {
                  _context.next = 9;
                  break;
                }

                return _context.abrupt("return", new _immortal_reference.ImmortalReference(null));

              case 9:
                model = this.monaco.editor.createModel(result.text, result.lang, resource);

              case 10:
                return _context.abrupt("return", new _immortal_reference.ImmortalReference({
                  textEditorModel: model
                }));

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createModelReference(_x) {
        return _createModelReference.apply(this, arguments);
      }

      return createModelReference;
    }()
  }, {
    key: "registerTextModelContentProvider",
    value: function registerTextModelContentProvider(scheme, provider) {
      return {
        dispose: function dispose() {
          /* no op */
        }
      };
    }
  }, {
    key: "fetchText",
    value: function () {
      var _fetchText = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(resource) {
        var repo, revision, file, response, contentType, lang, text;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                repo = "".concat(resource.authority).concat(resource.path);
                revision = resource.query;
                file = resource.fragment;
                _context2.next = 5;
                return fetch(_new_platform.npStart.core.http.basePath.prepend("/api/code/repo/".concat(repo, "/blob/").concat(revision, "/").concat(file)));

              case 5:
                response = _context2.sent;

                if (!(response.status === 200)) {
                  _context2.next = 16;
                  break;
                }

                contentType = response.headers.get('Content-Type');

                if (!(contentType && contentType.startsWith('text/'))) {
                  _context2.next = 14;
                  break;
                }

                lang = response.headers.get('lang');
                _context2.next = 12;
                return response.text();

              case 12:
                text = _context2.sent;
                return _context2.abrupt("return", {
                  text: text,
                  lang: lang
                });

              case 14:
                _context2.next = 17;
                break;

              case 16:
                return _context2.abrupt("return", null);

              case 17:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function fetchText(_x2) {
        return _fetchText.apply(this, arguments);
      }

      return fetchText;
    }()
  }]);

  return TextModelResolverService;
}();

exports.TextModelResolverService = TextModelResolverService;