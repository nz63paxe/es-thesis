"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeSymbolTree = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _url = _interopRequireDefault(require("url"));

var _vscodeLanguageserverTypes = require("vscode-languageserver-types");

var _lodash = require("lodash");

var _repository_utils = require("../../../common/repository_utils");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CodeSymbolTree =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeSymbolTree, _React$PureComponent);

  function CodeSymbolTree() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeSymbolTree);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeSymbolTree)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {});

    _defineProperty(_assertThisInitialized(_this), "getClickHandler", function (symbol) {
      return function () {
        _this.setState({
          activeSymbol: symbol
        }); // track structure tree click count


        (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.COUNT, _usage_telemetry_metrics.CodeUIUsageMetrics.STRUCTURE_TREE_CLICK_COUNT);
      };
    });

    _defineProperty(_assertThisInitialized(_this), "getStructureTreeItemRenderer", function (range, name, kind) {
      var isContainer = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;
      var forceOpen = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;
      var path = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : '';
      return function () {
        var tokenType = 'tokenFile'; // @ts-ignore

        tokenType = "token".concat(Object.keys(_vscodeLanguageserverTypes.SymbolKind).find(function (k) {
          return _vscodeLanguageserverTypes.SymbolKind[k] === kind;
        }));
        var bg = null;

        if (_this.state.activeSymbol && _this.state.activeSymbol.name === name && (0, _lodash.isEqual)(_this.state.activeSymbol.range, range)) {
          bg = _react.default.createElement("div", {
            className: "code-full-width-node"
          });
        }

        var queries = _url.default.parse(_this.props.location.search, true).query;

        return _react.default.createElement("div", {
          className: "code-symbol-container"
        }, bg, _react.default.createElement(_reactRouterDom.Link, {
          "data-test-subj": "codeStructureTreeNode-".concat(name),
          to: _url.default.format({
            pathname: _repository_utils.RepositoryUtils.locationToUrl({
              uri: _this.props.uri,
              range: range
            }),
            query: _objectSpread({
              sideTab: 'structure'
            }, queries)
          }),
          className: "code-symbol-link codeFileTree__node--link",
          onClick: _this.getClickHandler({
            name: name,
            range: range
          })
        }), _react.default.createElement("div", {
          className: isContainer ? 'codeSymbol' : 'codeSymbol codeSymbol--nested'
        }, isContainer && (forceOpen ? _react.default.createElement(_eui.EuiIcon, {
          type: "arrowDown",
          size: "s",
          color: "subdued",
          className: "codeStructureTree--icon",
          onClick: function onClick() {
            return _this.props.closeSymbolPath(path);
          }
        }) : _react.default.createElement(_eui.EuiIcon, {
          type: "arrowRight",
          size: "s",
          color: "subdued",
          className: "codeStructureTree--icon",
          onClick: function onClick() {
            return _this.props.openSymbolPath(path);
          }
        })), _react.default.createElement(_eui.EuiFlexGroup, {
          gutterSize: "none",
          alignItems: "center",
          className: "code-structure-node"
        }, _react.default.createElement(_eui.EuiToken, {
          iconType: tokenType
        }), _react.default.createElement(_eui.EuiText, {
          size: "s"
        }, name))));
      };
    });

    _defineProperty(_assertThisInitialized(_this), "symbolsToSideNavItems", function (symbolsWithMembers) {
      return symbolsWithMembers.map(function (s, index) {
        var item = {
          name: s.name,
          id: "".concat(s.name, "_").concat(index),
          onClick: function onClick() {
            return void 0;
          }
        };

        if (s.members) {
          item.forceOpen = !_this.props.closedPaths.includes(s.path);

          if (item.forceOpen) {
            item.items = _this.symbolsToSideNavItems(s.members);
          }

          item.renderItem = _this.getStructureTreeItemRenderer(s.selectionRange, s.name, s.kind, s.members.length > 0, item.forceOpen, s.path);
        } else {
          item.renderItem = _this.getStructureTreeItemRenderer(s.selectionRange, s.name, s.kind, false, false, s.path);
        }

        return item;
      });
    });

    return _this;
  }

  _createClass(CodeSymbolTree, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.uri && prevProps.uri !== this.props.uri && this.props.structureTree.length > 0) {
        // track lsp data available  page view count
        (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.COUNT, _usage_telemetry_metrics.CodeUIUsageMetrics.LSP_DATA_AVAILABLE_PAGE_VIEW_COUNT);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var items = [{
        name: '',
        id: '',
        items: this.symbolsToSideNavItems(this.props.structureTree)
      }];
      return _react.default.createElement("div", {
        className: "codeContainer__sideTabTree"
      }, _react.default.createElement(_eui.EuiSideNav, {
        items: items
      }));
    }
  }]);

  return CodeSymbolTree;
}(_react.default.PureComponent);

exports.CodeSymbolTree = CodeSymbolTree;