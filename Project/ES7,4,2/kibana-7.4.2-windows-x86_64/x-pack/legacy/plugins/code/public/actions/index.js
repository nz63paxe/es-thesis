"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  routeChange: true,
  checkSetupSuccess: true,
  checkSetupFailed: true
};
exports.checkSetupFailed = exports.checkSetupSuccess = exports.routeChange = void 0;

var _reduxActions = require("redux-actions");

var _repository = require("./repository");

Object.keys(_repository).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _repository[key];
    }
  });
});

var _search = require("./search");

Object.keys(_search).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _search[key];
    }
  });
});

var _file = require("./file");

Object.keys(_file).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _file[key];
    }
  });
});

var _structure = require("./structure");

Object.keys(_structure).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _structure[key];
    }
  });
});

var _editor = require("./editor");

Object.keys(_editor).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _editor[key];
    }
  });
});

var _commit = require("./commit");

Object.keys(_commit).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _commit[key];
    }
  });
});

var _status = require("./status");

Object.keys(_status).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _status[key];
    }
  });
});

var _project_config = require("./project_config");

Object.keys(_project_config).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _project_config[key];
    }
  });
});

var _shortcuts = require("./shortcuts");

Object.keys(_shortcuts).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _shortcuts[key];
    }
  });
});

var _route = require("./route");

Object.keys(_route).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _route[key];
    }
  });
});

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var routeChange = (0, _reduxActions.createAction)('CODE SEARCH ROUTE CHANGE');
exports.routeChange = routeChange;
var checkSetupSuccess = (0, _reduxActions.createAction)('SETUP CHECK SUCCESS');
exports.checkSetupSuccess = checkSetupSuccess;
var checkSetupFailed = (0, _reduxActions.createAction)('SETUP CHECK FAILED');
exports.checkSetupFailed = checkSetupFailed;