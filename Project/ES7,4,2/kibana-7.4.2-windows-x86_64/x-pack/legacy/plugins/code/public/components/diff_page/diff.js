"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Diff = exports.DiffPage = exports.DiffLayout = void 0;

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _actions = require("../../actions");

var _diff_editor = require("./diff_editor");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject11() {
  var data = _taggedTemplateLiteral(["\n  cursor: default;\n"]);

  _templateObject11 = function _templateObject11() {
    return data;
  };

  return data;
}

function _templateObject10() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n  line-height: calc(48rem / 14);\n"]);

  _templateObject10 = function _templateObject10() {
    return data;
  };

  return data;
}

function _templateObject9() {
  var data = _taggedTemplateLiteral(["\n  border-left: ", ";\n  height: calc(32rem / 14);\n  line-height: calc(32rem / 14);\n  padding-left: ", ";\n  margin: ", " 0;\n"]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n  margin-right: ", ";\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  height: calc(48rem / 14);\n  border-bottom: ", ";\n  padding: 0 ", ";\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  padding: ", " ", ";\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  padding: ", " ", ";\n  border-radius: ", ";\n  color: white;\n  margin-right: ", ";\n  background-color: ", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  display: inline-block;\n  padding: 0 ", ";\n  border: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var COMMIT_ID_LENGTH = 16;

var B = _styledComponents.default.b(_templateObject());

var PrimaryB = (0, _styledComponents.default)(B)(_templateObject2(), _eui_theme_light.default.euiColorPrimary);

var CommitId = _styledComponents.default.span(_templateObject3(), _eui_theme_light.default.paddingSizes.xs, _eui_theme_light.default.euiBorderThin);

var Addition = _styledComponents.default.div(_templateObject4(), _eui_theme_light.default.paddingSizes.xs, _eui_theme_light.default.paddingSizes.s, _eui_theme_light.default.euiSizeXS, _eui_theme_light.default.euiSizeS, _eui_theme_light.default.euiColorDanger);

var Deletion = (0, _styledComponents.default)(Addition)(_templateObject5(), _eui_theme_light.default.euiColorVis0);

var Container = _styledComponents.default.div(_templateObject6(), _eui_theme_light.default.paddingSizes.xs, _eui_theme_light.default.paddingSizes.m);

var TopBarContainer = _styledComponents.default.div(_templateObject7(), _eui_theme_light.default.euiBorderThin, _eui_theme_light.default.paddingSizes.m); // @types/styled-components@3.0.1 does not yet support `defaultProps`, which EuiAccordion uses
// Ref: https://github.com/DefinitelyTyped/DefinitelyTyped/pull/31903
// const Accordion = styled(EuiAccordion)`
//   border: ${theme.euiBorderThick};
//   border-radius: ${theme.euiSizeS};
//   margin-bottom: ${theme.euiSize};
// `;


var accordionStyles = {
  border: _eui_theme_light.default.euiBorderThick,
  borderRadius: _eui_theme_light.default.euiSizeS,
  marginBottom: _eui_theme_light.default.euiSize
};
var Icon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject8(), _eui_theme_light.default.euiSizeS);

var Parents = _styledComponents.default.div(_templateObject9(), _eui_theme_light.default.euiBorderThin, _eui_theme_light.default.paddingSizes.s, _eui_theme_light.default.euiSizeS);

var H4 = _styledComponents.default.h4(_templateObject10());

var ButtonContainer = _styledComponents.default.div(_templateObject11());

var DiffLayout;
exports.DiffLayout = DiffLayout;

(function (DiffLayout) {
  DiffLayout[DiffLayout["Unified"] = 0] = "Unified";
  DiffLayout[DiffLayout["Split"] = 1] = "Split";
})(DiffLayout || (exports.DiffLayout = DiffLayout = {}));

var onClick = function onClick(e) {
  e.preventDefault();
  e.stopPropagation();
};

var Difference = function Difference(props) {
  return _react.default.createElement(_eui.EuiAccordion, {
    style: accordionStyles,
    initialIsOpen: true,
    id: props.fileDiff.path,
    buttonContent: _react.default.createElement(ButtonContainer, {
      role: "button",
      onClick: onClick
    }, _react.default.createElement(_eui.EuiFlexGroup, {
      justifyContent: "spaceBetween",
      gutterSize: "none",
      alignItems: "center"
    }, _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_eui.EuiFlexGroup, {
      gutterSize: "none"
    }, _react.default.createElement(Addition, null, props.fileDiff.additions), _react.default.createElement(Deletion, null, props.fileDiff.deletions))), _react.default.createElement(_eui.EuiFlexItem, null, props.fileDiff.path), _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement("div", {
      className: "euiButton euiButton--primary euiButton--small",
      role: "button"
    }, _react.default.createElement("span", {
      className: "euiButton__content"
    }, _react.default.createElement(_reactRouterDom.Link, {
      to: "/".concat(props.repoUri, "/blob/").concat(props.revision, "/").concat(props.fileDiff.path)
    }, "View File"))))))
  }, _react.default.createElement(_diff_editor.DiffEditor, {
    originCode: props.fileDiff.originCode,
    modifiedCode: props.fileDiff.modifiedCode,
    language: props.fileDiff.language,
    renderSideBySide: true
  }));
};

var DiffPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DiffPage, _React$Component);

  function DiffPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DiffPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DiffPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      diffLayout: DiffLayout.Split
    });

    _defineProperty(_assertThisInitialized(_this), "setLayoutUnified", function () {
      _this.setState({
        diffLayout: DiffLayout.Unified
      });
    });

    _defineProperty(_assertThisInitialized(_this), "setLayoutSplit", function () {
      _this.setState({
        diffLayout: DiffLayout.Split
      });
    });

    return _this;
  }

  _createClass(DiffPage, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          commit = _this$props.commit,
          match = _this$props.match;
      var _match$params = match.params,
          repo = _match$params.repo,
          org = _match$params.org,
          resource = _match$params.resource;
      var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);

      if (!commit) {
        return null;
      }

      var additions = commit.additions,
          deletions = commit.deletions,
          files = commit.files;
      var parents = commit.commit.parents;
      var title = commit.commit.message.split('\n')[0];
      var parentsLinks = null;

      if (parents.length > 1) {
        var _parents = _slicedToArray(parents, 2),
            p1 = _parents[0],
            p2 = _parents[1];

        parentsLinks = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_reactRouterDom.Link, {
          to: "/".concat(repoUri, "/commit/").concat(p1)
        }, p1), "+", _react.default.createElement(_reactRouterDom.Link, {
          to: "/".concat(repoUri, "/commit/").concat(p2)
        }, p2));
      } else if (parents.length === 1) {
        parentsLinks = _react.default.createElement(_reactRouterDom.Link, {
          to: "/".concat(repoUri, "/commit/").concat(parents[0])
        }, parents[0]);
      }

      var topBar = _react.default.createElement(TopBarContainer, null, _react.default.createElement("div", null, _react.default.createElement(_eui.EuiTitle, {
        size: "xs"
      }, _react.default.createElement(H4, null, title))), _react.default.createElement("div", null, _react.default.createElement(Parents, null, "Parents: ", parentsLinks)));

      var fileCount = files.length;
      var diffs = commit.files.map(function (file) {
        return _react.default.createElement(Difference, {
          repoUri: repoUri,
          revision: commit.commit.id,
          fileDiff: file,
          key: file.path
        });
      });
      return _react.default.createElement("div", {
        className: "diff"
      }, topBar, _react.default.createElement(Container, null, _react.default.createElement(_eui.EuiText, null, commit.commit.message)), _react.default.createElement(Container, null, _react.default.createElement(_eui.EuiFlexGroup, {
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_eui.EuiText, null, _react.default.createElement(Icon, {
        type: "dataVisualizer"
      }), "Showing", _react.default.createElement(PrimaryB, null, " ", fileCount, " Changed files "), "with", _react.default.createElement(B, null, " ", additions, " additions"), " and ", _react.default.createElement(B, null, deletions, " deletions "))), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_eui.EuiText, null, "Committed by", _react.default.createElement(PrimaryB, null, " ", commit.commit.committer, " "), _react.default.createElement(CommitId, null, commit.commit.id.substr(0, COMMIT_ID_LENGTH)))))), _react.default.createElement(Container, null, diffs));
    }
  }]);

  return DiffPage;
}(_react.default.Component);

exports.DiffPage = DiffPage;

var mapStateToProps = function mapStateToProps(state) {
  return {
    commit: state.commit.commit,
    query: state.search.query,
    repoScope: state.search.searchOptions.repoScope.map(function (r) {
      return r.uri;
    })
  };
};

var mapDispatchToProps = {
  onSearchScopeChanged: _actions.changeSearchScope
};
var Diff = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(DiffPage));
exports.Diff = Diff;