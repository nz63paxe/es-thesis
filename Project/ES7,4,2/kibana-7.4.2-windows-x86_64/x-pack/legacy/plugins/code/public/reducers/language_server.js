"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.languageServer = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _language_server = require("../../common/language_server");

var _language_server2 = require("../actions/language_server");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  languageServers: [],
  loading: false,
  installServerLoading: {}
};
var languageServer = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_language_server2.loadLanguageServers), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_language_server2.loadLanguageServersSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.languageServers = action.payload;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_language_server2.loadLanguageServersFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.languageServers = [];
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_language_server2.requestInstallLanguageServer), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.installServerLoading[action.payload] = true;
  });
}), _defineProperty(_handleActions, String(_language_server2.requestInstallLanguageServerSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.installServerLoading[action.payload] = false;
    draft.languageServers.find(function (ls) {
      return ls.name === action.payload;
    }).status = _language_server.LanguageServerStatus.READY;
  });
}), _handleActions), initialState);
exports.languageServer = languageServer;