"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SymbolSuggestionsProvider = void 0;

var _new_platform = require("ui/new_platform");

var _ = require(".");

var _repository_utils = require("../../../../common/repository_utils");

var _uri_util = require("../../../../common/uri_util");

var _model = require("../../../../model");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var SymbolSuggestionsProvider =
/*#__PURE__*/
function (_AbstractSuggestionsP) {
  _inherits(SymbolSuggestionsProvider, _AbstractSuggestionsP);

  function SymbolSuggestionsProvider() {
    _classCallCheck(this, SymbolSuggestionsProvider);

    return _possibleConstructorReturn(this, _getPrototypeOf(SymbolSuggestionsProvider).apply(this, arguments));
  }

  _createClass(SymbolSuggestionsProvider, [{
    key: "matchSearchScope",
    value: function matchSearchScope(scope) {
      return scope === _model.SearchScope.DEFAULT || scope === _model.SearchScope.SYMBOL;
    }
  }, {
    key: "fetchSuggestions",
    value: function () {
      var _fetchSuggestions = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(query, repoScope) {
        var _this = this;

        var queryParams, res, suggestions;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                queryParams = {
                  q: query
                };

                if (repoScope && repoScope.length > 0) {
                  queryParams.repoScope = repoScope.join(',');
                }

                _context.next = 5;
                return _new_platform.npStart.core.http.get("/api/code/suggestions/symbol", {
                  query: queryParams
                });

              case 5:
                res = _context.sent;
                suggestions = Array.from(res.symbols).slice(0, this.MAX_SUGGESTIONS_PER_GROUP).map(function (symbol) {
                  return {
                    description: _this.getSymbolDescription(symbol.symbolInformation.location),
                    end: 10,
                    start: 1,
                    text: symbol.qname ? symbol.qname : symbol.symbolInformation.name,
                    tokenType: _this.symbolKindToTokenClass(symbol.symbolInformation.kind),
                    selectUrl: _this.getSymbolLinkUrl(symbol.symbolInformation.location)
                  };
                });
                return _context.abrupt("return", {
                  type: _.AutocompleteSuggestionType.SYMBOL,
                  total: res.total,
                  hasMore: res.total > this.MAX_SUGGESTIONS_PER_GROUP,
                  suggestions: suggestions
                });

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                return _context.abrupt("return", {
                  type: _.AutocompleteSuggestionType.SYMBOL,
                  total: 0,
                  hasMore: false,
                  suggestions: []
                });

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 10]]);
      }));

      function fetchSuggestions(_x, _x2) {
        return _fetchSuggestions.apply(this, arguments);
      }

      return fetchSuggestions;
    }()
  }, {
    key: "getSymbolDescription",
    value: function getSymbolDescription(location) {
      try {
        var _parseLspUrl = (0, _uri_util.parseLspUrl)(location.uri),
            repoUri = _parseLspUrl.repoUri,
            file = _parseLspUrl.file;

        var repoName = (0, _uri_util.toRepoNameWithOrg)(repoUri);
        return "".concat(repoName, " > ").concat(file);
      } catch (error) {
        return '';
      }
    }
  }, {
    key: "getSymbolLinkUrl",
    value: function getSymbolLinkUrl(location) {
      try {
        return _repository_utils.RepositoryUtils.locationToUrl(location);
      } catch (error) {
        return '';
      }
    }
  }, {
    key: "symbolKindToTokenClass",
    value: function symbolKindToTokenClass(kind) {
      switch (kind) {
        case 1:
          // File
          return 'tokenFile';

        case 2:
          // Module
          return 'tokenModule';

        case 3:
          // Namespace
          return 'tokenNamespace';

        case 4:
          // Package
          return 'tokenPackage';

        case 5:
          // Class
          return 'tokenClass';

        case 6:
          // Method
          return 'tokenMethod';

        case 7:
          // Property
          return 'tokenProperty';

        case 8:
          // Field
          return 'tokenField';

        case 9:
          // Constructor
          return 'tokenConstant';

        case 10:
          // Enum
          return 'tokenEnum';

        case 11:
          // Interface
          return 'tokenInterface';

        case 12:
          // Function
          return 'tokenFunction';

        case 13:
          // Variable
          return 'tokenVariable';

        case 14:
          // Constant
          return 'tokenConstant';

        case 15:
          // String
          return 'tokenString';

        case 16:
          // Number
          return 'tokenNumber';

        case 17:
          // Bollean
          return 'tokenBoolean';

        case 18:
          // Array
          return 'tokenArray';

        case 19:
          // Object
          return 'tokenObject';

        case 20:
          // Key
          return 'tokenKey';

        case 21:
          // Null
          return 'tokenNull';

        case 22:
          // EnumMember
          return 'tokenEnumMember';

        case 23:
          // Struct
          return 'tokenStruct';

        case 24:
          // Event
          return 'tokenEvent';

        case 25:
          // Operator
          return 'tokenOperator';

        case 26:
          // TypeParameter
          return 'tokenParameter';

        default:
          return 'tokenElement';
      }
    }
  }]);

  return SymbolSuggestionsProvider;
}(_.AbstractSuggestionsProvider);

exports.SymbolSuggestionsProvider = SymbolSuggestionsProvider;