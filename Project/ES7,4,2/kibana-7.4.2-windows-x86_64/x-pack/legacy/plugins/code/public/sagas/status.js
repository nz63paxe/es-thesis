"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchRepoCloneSuccess = watchRepoCloneSuccess;
exports.watchRepoDeleteFinished = watchRepoDeleteFinished;
exports.watchStatusChange = watchStatusChange;
exports.deleteRepoStatusPollingStopPattern = exports.indexRepoStatusPollingStopPattern = exports.cloneRepoStatusPollingStopPattern = exports.cloneCompletedPattern = void 0;

var _effects = require("redux-saga/effects");

var _new_platform = require("ui/new_platform");

var _lodash = require("lodash");

var _reduxSaga = require("redux-saga");

var _model = require("../../model");

var _actions = require("../actions");

var ROUTES = _interopRequireWildcard(require("../components/routes"));

var _patterns = require("./patterns");

var _repo_file_status = require("../../common/repo_file_status");

var _selectors = require("../selectors");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoCloneSuccess),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoCloneSuccess),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoDeleteFinished),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoDeleteFinished),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(handleMainRouteChange),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(startPollingStatus),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(compareStatus),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(fetchStatus),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(watchStatusChange);

var matchSelector = function matchSelector(state) {
  return state.route.match;
};

var cloneCompletedPattern = function cloneCompletedPattern(action) {
  return action.type === String(_actions.updateCloneProgress) && action.payload.progress === _model.WorkerReservedProgress.COMPLETED;
};

exports.cloneCompletedPattern = cloneCompletedPattern;

var deleteCompletedPattern = function deleteCompletedPattern(action) {
  return action.type === String(_actions.updateDeleteProgress) && action.payload.progress === _model.WorkerReservedProgress.COMPLETED;
};

var cloneRepoStatusPollingStopPattern = function cloneRepoStatusPollingStopPattern(repoUri) {
  return function (action) {
    return action.type === String(_actions.pollRepoCloneStatusStop) && action.payload === repoUri;
  };
};

exports.cloneRepoStatusPollingStopPattern = cloneRepoStatusPollingStopPattern;

var indexRepoStatusPollingStopPattern = function indexRepoStatusPollingStopPattern(repoUri) {
  return function (action) {
    return action.type === String(_actions.pollRepoIndexStatusStop) && action.payload === repoUri;
  };
};

exports.indexRepoStatusPollingStopPattern = indexRepoStatusPollingStopPattern;

var deleteRepoStatusPollingStopPattern = function deleteRepoStatusPollingStopPattern(repoUri) {
  return function (action) {
    return action.type === String(_actions.pollRepoDeleteStatusStop) && action.payload === repoUri;
  };
};

exports.deleteRepoStatusPollingStopPattern = deleteRepoStatusPollingStopPattern;

function handleRepoCloneSuccess() {
  var match;
  return regeneratorRuntime.wrap(function handleRepoCloneSuccess$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.select)(matchSelector);

        case 2:
          match = _context.sent;

          if (!(match.path === ROUTES.MAIN || match.path === ROUTES.MAIN_ROOT)) {
            _context.next = 6;
            break;
          }

          _context.next = 6;
          return (0, _effects.put)((0, _actions.routeChange)(match));

        case 6:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

function watchRepoCloneSuccess() {
  return regeneratorRuntime.wrap(function watchRepoCloneSuccess$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(cloneCompletedPattern, handleRepoCloneSuccess);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function handleRepoDeleteFinished(action) {
  return regeneratorRuntime.wrap(function handleRepoDeleteFinished$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.put)((0, _actions.deleteRepoFinished)(action.payload.uri));

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function watchRepoDeleteFinished() {
  return regeneratorRuntime.wrap(function watchRepoDeleteFinished$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(deleteCompletedPattern, handleRepoDeleteFinished);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function handleMainRouteChange(action) {
  var _params, resource, org, repo, path, revision, uri, newStatusPath, currentStatusPath;

  return regeneratorRuntime.wrap(function handleMainRouteChange$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          // in source view page, we need repos as default repo scope options when no query input
          _params = action.payload.params, resource = _params.resource, org = _params.org, repo = _params.repo, path = _params.path, revision = _params.revision;
          uri = "".concat(resource, "/").concat(org, "/").concat(repo);
          newStatusPath = {
            uri: uri,
            revision: revision,
            path: path
          };
          _context5.next = 5;
          return (0, _effects.select)(function (state) {
            return state.status.currentStatusPath;
          });

        case 5:
          currentStatusPath = _context5.sent;

          if ((0, _lodash.isEqual)(newStatusPath, currentStatusPath)) {
            _context5.next = 9;
            break;
          }

          _context5.next = 9;
          return (0, _effects.call)(startPollingStatus, newStatusPath);

        case 9:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

var STATUS_POLLING_FREQ_HIGH_MS = 3000;
var STATUS_POLLING_FREQ_LOW_MS = 10000;

function startPollingStatus(location) {
  var currentStatusPath, previousStatus, newStatus, delayMs;
  return regeneratorRuntime.wrap(function startPollingStatus$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.put)((0, _actions.FetchRepoFileStatus)(location));

        case 2:
          _context6.next = 4;
          return (0, _effects.select)(function (state) {
            return state.status.currentStatusPath;
          });

        case 4:
          currentStatusPath = _context6.sent;

        case 5:
          if (!(0, _lodash.isEqual)(location, currentStatusPath)) {
            _context6.next = 24;
            break;
          }

          _context6.next = 8;
          return (0, _effects.select)(_selectors.statusSelector);

        case 8:
          previousStatus = _context6.sent;
          _context6.next = 11;
          return (0, _effects.call)(fetchStatus, location);

        case 11:
          newStatus = _context6.sent;
          delayMs = STATUS_POLLING_FREQ_LOW_MS;

          if (!newStatus) {
            _context6.next = 20;
            break;
          }

          _context6.next = 16;
          return (0, _effects.call)(compareStatus, previousStatus, newStatus);

        case 16:
          if (newStatus.langServerStatus === _repo_file_status.RepoFileStatus.LANG_SERVER_IS_INITIALIZING) {
            delayMs = STATUS_POLLING_FREQ_HIGH_MS;
          }

          _context6.next = 19;
          return (0, _effects.select)(function (state) {
            return state.status.currentStatusPath;
          });

        case 19:
          currentStatusPath = _context6.sent;

        case 20:
          _context6.next = 22;
          return (0, _effects.call)(_reduxSaga.delay, delayMs);

        case 22:
          _context6.next = 5;
          break;

        case 24:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function compareStatus(prevStatus, currentStatus) {
  return regeneratorRuntime.wrap(function compareStatus$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          if ((0, _lodash.isEqual)(prevStatus, currentStatus)) {
            _context7.next = 3;
            break;
          }

          _context7.next = 3;
          return (0, _effects.put)((0, _actions.StatusChanged)({
            prevStatus: prevStatus,
            currentStatus: currentStatus
          }));

        case 3:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

function fetchStatus(location) {
  var _newStatus;

  return regeneratorRuntime.wrap(function fetchStatus$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.put)((0, _actions.FetchRepoFileStatus)(location));

        case 2:
          _context8.prev = 2;
          _context8.next = 5;
          return (0, _effects.call)(requestStatus, location);

        case 5:
          _newStatus = _context8.sent;
          _context8.next = 8;
          return (0, _effects.put)((0, _actions.FetchRepoFileStatusSuccess)({
            statusReport: _newStatus,
            path: location
          }));

        case 8:
          return _context8.abrupt("return", _newStatus);

        case 11:
          _context8.prev = 11;
          _context8.t0 = _context8["catch"](2);
          _context8.next = 15;
          return (0, _effects.put)((0, _actions.FetchRepoFileStatusFailed)(_context8.t0));

        case 15:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, null, [[2, 11]]);
}

function requestStatus(location) {
  var uri = location.uri,
      revision = location.revision,
      path = location.path;
  var pathname = path ? "/api/code/repo/".concat(uri, "/status/").concat(revision, "/").concat(path) : "/api/code/repo/".concat(uri, "/status/").concat(revision);
  return _new_platform.npStart.core.http.get(pathname);
}

function watchStatusChange() {
  return regeneratorRuntime.wrap(function watchStatusChange$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _effects.takeLatest)(_patterns.mainRoutePattern, handleMainRouteChange);

        case 2:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}