"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuggestionsComponent = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _react2 = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _url = _interopRequireDefault(require("url"));

var _ = require("../..");

var _suggestion_component = require("./suggestion_component");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SuggestionsComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(SuggestionsComponent, _Component);

  function SuggestionsComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SuggestionsComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SuggestionsComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "childNodes", []);

    _defineProperty(_assertThisInitialized(_this), "parentNode", null);

    _defineProperty(_assertThisInitialized(_this), "scrollIntoView", function () {
      if (_this.props.groupIndex === null || _this.props.itemIndex === null) {
        return;
      }

      var parent = _this.parentNode;
      var child = _this.childNodes[_this.props.itemIndex];

      if (_this.props.groupIndex == null || _this.props.itemIndex === null || !parent || !child) {
        return;
      }

      var scrollTop = Math.max(Math.min(parent.scrollTop, child.offsetTop), child.offsetTop + child.offsetHeight - parent.offsetHeight);
      parent.scrollTop = scrollTop;
    });

    _defineProperty(_assertThisInitialized(_this), "handleScroll", function () {
      if (!_this.props.loadMore || !_this.parentNode) {
        return;
      }

      var position = _this.parentNode.scrollTop + _this.parentNode.offsetHeight;
      var height = _this.parentNode.scrollHeight;
      var remaining = height - position;
      var margin = 50;

      if (!height || !position) {
        return;
      }

      if (remaining <= margin) {
        _this.props.loadMore();
      }
    });

    return _this;
  }

  _createClass(SuggestionsComponent, [{
    key: "viewMoreUrl",
    value: function viewMoreUrl() {
      return _url.default.format({
        pathname: '/search',
        query: {
          q: this.props.query
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      if (!this.props.show || (0, _lodash.isEmpty)(this.props.suggestionGroups)) {
        return null;
      }

      return _react2.default.createElement("div", {
        className: "reactSuggestionTypeahead"
      }, _react2.default.createElement("div", {
        className: "kbnTypeahead"
      }, _react2.default.createElement("div", {
        className: "kbnTypeahead__popover"
      }, this.renderSuggestionGroups(), _react2.default.createElement(_reactRouterDom.Link, {
        to: this.viewMoreUrl()
      }, _react2.default.createElement("div", {
        className: "codeSearch__full-text-button"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchBar.viewFullSearchLinkText",
        defaultMessage: "Press \u2B90 Return for Full Text Search"
      }))))));
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps.groupIndex !== this.props.groupIndex || prevProps.itemIndex !== this.props.itemIndex) {
        this.scrollIntoView();
      }
    }
  }, {
    key: "renderSuggestionGroups",
    value: function renderSuggestionGroups() {
      var _this2 = this;

      return this.props.suggestionGroups.filter(function (group) {
        return group.suggestions.length > 0;
      }).map(function (group, groupIndex) {
        var suggestions = group.suggestions,
            total = group.total,
            type = group.type,
            hasMore = group.hasMore;
        var suggestionComps = suggestions.map(function (suggestion, itemIndex) {
          var innerRef = function innerRef(node) {
            return _this2.childNodes[itemIndex] = node;
          };

          var mouseEnter = function mouseEnter() {
            return _this2.props.onMouseEnter(groupIndex, itemIndex);
          };

          var isSelected = groupIndex === _this2.props.groupIndex && itemIndex === _this2.props.itemIndex;
          return _react2.default.createElement(_suggestion_component.SuggestionComponent, {
            query: _this2.props.query,
            innerRef: innerRef,
            selected: isSelected,
            suggestion: suggestion,
            onClick: _this2.props.onClick,
            onMouseEnter: mouseEnter,
            ariaId: "suggestion-".concat(groupIndex, "-").concat(itemIndex),
            key: "".concat(suggestion.tokenType, " - ").concat(groupIndex, "-").concat(itemIndex, " - ").concat(suggestion.text)
          });
        });

        var groupHeader = _react2.default.createElement(_eui.EuiFlexGroup, {
          justifyContent: "spaceBetween",
          className: "codeSearch-suggestion__group-header"
        }, _react2.default.createElement(_eui.EuiFlexGroup, {
          direction: "row",
          gutterSize: "none",
          alignItems: "center"
        }, _react2.default.createElement(_eui.EuiToken, {
          iconType: _this2.getGroupTokenType(group.type)
        }), _react2.default.createElement(_eui.EuiText, {
          className: "codeSearch-suggestion__group-title"
        }, _this2.getGroupTitle(group.type))), _react2.default.createElement("div", {
          className: "codeSearch-suggestion__group-result"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.resultCountText",
          defaultMessage: "{total} {total, plural, one {Result} other {Results}}",
          values: {
            total: total
          }
        })));

        var viewMore = _react2.default.createElement("div", {
          className: "codeSearch-suggestion__link"
        }, _react2.default.createElement(_reactRouterDom.Link, {
          to: _this2.viewMoreUrl()
        }, ' ', _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.viewMoreLinkText",
          defaultMessage: "View More"
        })));

        return _react2.default.createElement("div", {
          id: "kbnTypeahead__items",
          className: "kbnTypeahead__items codeSearch-suggestion__group",
          role: "listbox",
          "data-test-subj": "codeTypeaheadList-".concat(type),
          ref: function ref(node) {
            return _this2.parentNode = node;
          },
          onScroll: _this2.handleScroll,
          key: "".concat(type, "-suggestions")
        }, groupHeader, suggestionComps, hasMore ? viewMore : null);
      });
    }
  }, {
    key: "getGroupTokenType",
    value: function getGroupTokenType(type) {
      switch (type) {
        case _.AutocompleteSuggestionType.FILE:
          return 'tokenFile';

        case _.AutocompleteSuggestionType.REPOSITORY:
          return 'tokenRepo';

        case _.AutocompleteSuggestionType.SYMBOL:
          return 'tokenSymbol';
      }
    }
  }, {
    key: "getGroupTitle",
    value: function getGroupTitle(type) {
      switch (type) {
        case _.AutocompleteSuggestionType.FILE:
          return _i18n.i18n.translate('xpack.code.searchBar.fileGroupTitle', {
            defaultMessage: 'Files'
          });

        case _.AutocompleteSuggestionType.REPOSITORY:
          return _i18n.i18n.translate('xpack.code.searchBar.repositorylGroupTitle', {
            defaultMessage: 'Repos'
          });

        case _.AutocompleteSuggestionType.SYMBOL:
          return _i18n.i18n.translate('xpack.code.searchBar.symbolGroupTitle', {
            defaultMessage: 'Symbols'
          });
      }
    }
  }]);

  return SuggestionsComponent;
}(_react2.Component);

exports.SuggestionsComponent = SuggestionsComponent;