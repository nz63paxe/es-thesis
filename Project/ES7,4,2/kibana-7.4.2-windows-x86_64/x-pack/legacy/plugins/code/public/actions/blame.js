"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadBlameFailed = exports.loadBlameSuccess = exports.loadBlame = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadBlame = (0, _reduxActions.createAction)('LOAD BLAME');
exports.loadBlame = loadBlame;
var loadBlameSuccess = (0, _reduxActions.createAction)('LOAD BLAME SUCCESS');
exports.loadBlameSuccess = loadBlameSuccess;
var loadBlameFailed = (0, _reduxActions.createAction)('LOAD BLAME FAILED');
exports.loadBlameFailed = loadBlameFailed;