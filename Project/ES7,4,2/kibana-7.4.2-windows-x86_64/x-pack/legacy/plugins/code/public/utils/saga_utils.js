"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.singletonRequestSaga = void 0;

var _effects = require("redux-saga/effects");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(cancelRequest);

function cancelRequest(action, abortController) {
  return regeneratorRuntime.wrap(function cancelRequest$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.take)(action.type);

        case 2:
          abortController.abort();

        case 3:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

var singletonRequestSaga = function singletonRequestSaga(saga) {
  return (
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(action) {
      var abortController, task;
      return regeneratorRuntime.wrap(function _callee$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              abortController = new AbortController();
              _context2.next = 3;
              return (0, _effects.spawn)(cancelRequest, action, abortController);

            case 3:
              task = _context2.sent;
              _context2.next = 6;
              return (0, _effects.spawn)(saga, action, abortController.signal, task);

            case 6:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee);
    })
  );
};

exports.singletonRequestSaga = singletonRequestSaga;