"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BranchSelector = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var RevisionSelectorTabs;

(function (RevisionSelectorTabs) {
  RevisionSelectorTabs["Branches"] = "Branches";
  RevisionSelectorTabs["Tags"] = "Tags";
})(RevisionSelectorTabs || (RevisionSelectorTabs = {}));

var BranchSelector = function BranchSelector(props) {
  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isPopoverOpen = _useState2[0],
      togglePopoverOpen = _useState2[1];

  var _useState3 = (0, _react.useState)(''),
      _useState4 = _slicedToArray(_useState3, 2),
      query = _useState4[0],
      setQuery = _useState4[1];

  var filterRevision = function filterRevision(r) {
    return r.name.toLowerCase().includes(query.toLowerCase());
  };

  var filteredBranches = props.branches.filter(filterRevision);
  var filteredTags = props.tags.filter(filterRevision);

  var getListItem = function getListItem(b) {
    return b === props.revision ? _react.default.createElement(_eui.EuiListGroupItem, {
      size: "s",
      className: "codeBranchSelectorRevisionItem",
      label: b,
      isActive: true,
      iconType: "check",
      key: b,
      href: props.getHrefFromRevision(b),
      "data-test-subj": "codeBranchSelectOption-".concat(b, "Active")
    }) : _react.default.createElement(_eui.EuiListGroupItem, {
      size: "s",
      className: "codeBranchSelectorRevisionItem",
      label: b,
      key: b,
      href: props.getHrefFromRevision(b),
      iconType: "empty",
      "data-test-subj": "codeBranchSelectOption-".concat(b)
    });
  };

  var tabs = [{
    id: RevisionSelectorTabs.Branches,
    name: RevisionSelectorTabs.Branches,
    content: _react.default.createElement("div", {
      className: "codeBranchSelectorTabContentContainer"
    }, _react.default.createElement(_eui.EuiFieldSearch, {
      value: query,
      incremental: true,
      onChange: function onChange(e) {
        return setQuery(e.target.value);
      },
      placeholder: "Search branches"
    }), _react.default.createElement(_eui.EuiListGroup, {
      className: "codeBranchSelectorTabs"
    }, filteredBranches.map(function (b) {
      return getListItem(b.name);
    })))
  }, {
    id: RevisionSelectorTabs.Tags,
    name: RevisionSelectorTabs.Tags,
    content: _react.default.createElement("div", {
      className: "codeBranchSelectorTabContentContainer"
    }, _react.default.createElement(_eui.EuiFieldSearch, {
      value: query,
      incremental: true,
      onChange: function onChange(e) {
        return setQuery(e.target.value);
      },
      placeholder: "Search tags"
    }), _react.default.createElement(_eui.EuiListGroup, {
      className: "codeBranchSelectorTabs"
    }, filteredTags.map(function (b) {
      return getListItem(b.name);
    })))
  }];
  return (// @ts-ignore
    _react.default.createElement(_eui.EuiPopover, {
      anchorPosition: "downLeft",
      panelClassName: "codeBranchSelectorPopover",
      display: "block",
      isOpen: isPopoverOpen,
      hasArrow: false,
      button: _react.default.createElement(_eui.EuiSuperSelect, {
        onClick: function onClick() {
          return togglePopoverOpen(true);
        },
        "data-test-subj": "codeBranchSelector",
        valueOfSelected: props.revision,
        options: [{
          value: props.revision,
          inputDisplay: props.revision
        }]
      }),
      closePopover: function closePopover() {
        return togglePopoverOpen(false);
      }
    }, _react.default.createElement(_eui.EuiTabbedContent, {
      tabs: tabs,
      initialSelectedTab: tabs[0],
      size: 's'
    }))
  );
};

exports.BranchSelector = BranchSelector;