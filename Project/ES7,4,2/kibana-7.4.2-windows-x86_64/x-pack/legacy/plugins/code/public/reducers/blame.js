"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blame = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _blame = require("../actions/blame");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  blames: [],
  loading: false
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.blames = initialState.blames;
    draft.loading = initialState.loading;
    draft.error = undefined;
  });
};

var blame = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_blame.loadBlame), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_blame.loadBlameSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.blames = action.payload;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_blame.loadBlameFailed), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = false;
    draft.error = action.payload;
    draft.blames = [];
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _defineProperty(_handleActions, String(_route.revisionChange), clearState), _defineProperty(_handleActions, String(_route.filePathChange), clearState), _handleActions), initialState);
exports.blame = blame;