"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchBar = void 0;

var _querystring = _interopRequireDefault(require("querystring"));

var _react = _interopRequireDefault(require("react"));

var _url = _interopRequireDefault(require("url"));

var _model = require("../../../model");

var _types = require("../../common/types");

var _url2 = require("../../utils/url");

var _shortcuts = require("../shortcuts");

var _query_bar = require("../query_bar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SearchBar =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(SearchBar, _React$PureComponent);

  function SearchBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SearchBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SearchBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "queryBar", null);

    _defineProperty(_assertThisInitialized(_this), "onSearchChanged", function (query) {
      // Merge the default repository scope if necessary.
      var repoScopes = _this.props.searchOptions.repoScope.map(function (repo) {
        return repo.uri;
      });

      if (_this.props.searchOptions.defaultRepoScopeOn && _this.props.searchOptions.defaultRepoScope) {
        repoScopes.push(_this.props.searchOptions.defaultRepoScope.uri);
      } // Update the url and push to history as well.


      var previousQueries = _querystring.default.parse(_url2.history.location.search.replace('?', ''));

      var queries = _objectSpread({}, previousQueries, {
        repoScope: repoScopes,
        q: query
      });

      if (repoScopes.length === 0) {
        delete queries.repoScope;
      }

      _url2.history.push(_url.default.format({
        pathname: '/search',
        query: queries
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "onSubmit", function (q) {
      // ignore empty query
      if (q.trim().length > 0) {
        _this.onSearchChanged(q);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onSelect", function (item) {
      _url2.history.push(item.selectUrl);
    });

    _defineProperty(_assertThisInitialized(_this), "suggestionProviders", [new _query_bar.SymbolSuggestionsProvider(), new _query_bar.FileSuggestionsProvider(), new _query_bar.RepositorySuggestionsProvider()]);

    return _this;
  }

  _createClass(SearchBar, [{
    key: "toggleOptionsFlyout",
    value: function toggleOptionsFlyout() {
      if (this.queryBar) {
        this.queryBar.toggleOptionsFlyout();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react.default.createElement("div", {
        className: "codeSearchbar__container"
      }, _react.default.createElement(_shortcuts.ShortcutsProvider, null), _react.default.createElement(_shortcuts.Shortcut, {
        keyCode: "p",
        help: _types.SearchScopeText[_model.SearchScope.REPOSITORY],
        onPress: function onPress() {
          _this2.props.onSearchScopeChanged(_model.SearchScope.REPOSITORY);

          if (_this2.queryBar) {
            _this2.queryBar.focusInput();
          }
        }
      }), _react.default.createElement(_shortcuts.Shortcut, {
        keyCode: "y",
        help: _types.SearchScopeText[_model.SearchScope.SYMBOL],
        onPress: function onPress() {
          _this2.props.onSearchScopeChanged(_model.SearchScope.SYMBOL);

          if (_this2.queryBar) {
            _this2.queryBar.focusInput();
          }
        }
      }), _react.default.createElement(_shortcuts.Shortcut, {
        keyCode: "s",
        help: _types.SearchScopeText[_model.SearchScope.DEFAULT],
        onPress: function onPress() {
          _this2.props.onSearchScopeChanged(_model.SearchScope.DEFAULT);

          if (_this2.queryBar) {
            _this2.queryBar.focusInput();
          }
        }
      }), _react.default.createElement(_query_bar.QueryBar, {
        query: this.props.query,
        onSubmit: this.onSubmit,
        onSelect: this.onSelect,
        appName: "code",
        suggestionProviders: this.suggestionProviders,
        onSearchScopeChanged: this.props.onSearchScopeChanged,
        enableSubmitWhenOptionsChanged: this.props.enableSubmitWhenOptionsChanged,
        ref: function ref(instance) {
          if (instance) {
            // @ts-ignore
            _this2.queryBar = instance.getWrappedInstance();
          }
        }
      }));
    }
  }]);

  return SearchBar;
}(_react.default.PureComponent);

exports.SearchBar = SearchBar;