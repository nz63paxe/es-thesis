"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.search = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _querystring = _interopRequireDefault(require("querystring"));

var _reduxActions = require("redux-actions");

var _url = require("../utils/url");

var _model = require("../../model");

var _actions = require("../actions");

var _repository_utils = require("../../common/repository_utils");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var repositories = [];

var getRepoScopeFromUrl = function getRepoScopeFromUrl() {
  var _querystring$parse = _querystring.default.parse(_url.history.location.search.replace('?', '')),
      repoScope = _querystring$parse.repoScope;

  if (repoScope) {
    return String(repoScope).split(',').map(function (r) {
      return {
        uri: r,
        org: _repository_utils.RepositoryUtils.orgNameFromUri(r),
        name: _repository_utils.RepositoryUtils.repoNameFromUri(r)
      };
    });
  } else {
    return [];
  }
};

var initialState = {
  query: '',
  isLoading: false,
  isScopeSearchLoading: false,
  scope: _model.SearchScope.DEFAULT,
  searchOptions: {
    repoScope: getRepoScopeFromUrl(),
    defaultRepoScopeOn: false
  },
  scopeSearchResults: {
    repositories: repositories,
    total: 0,
    took: 0
  }
};
var search = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.changeSearchScope), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (Object.values(_model.SearchScope).includes(action.payload)) {
      draft.scope = action.payload;
    } else {
      draft.scope = _model.SearchScope.DEFAULT;
    }

    draft.isLoading = false;
  });
}), _defineProperty(_handleActions, String(_actions.documentSearch), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload) {
      draft.query = action.payload.query;
      draft.page = parseInt(action.payload.page, 10);

      if (action.payload.languages) {
        draft.languages = new Set(decodeURIComponent(action.payload.languages).split(','));
      } else {
        draft.languages = new Set();
      }

      if (action.payload.repositories) {
        draft.repositories = new Set(decodeURIComponent(action.payload.repositories).split(','));
      } else {
        draft.repositories = new Set();
      }

      draft.isLoading = true;
      delete draft.error;
    }
  });
}), _defineProperty(_handleActions, String(_actions.documentSearchSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var _ref = action.payload,
        from = _ref.from,
        page = _ref.page,
        totalPage = _ref.totalPage,
        results = _ref.results,
        total = _ref.total,
        repoAggregations = _ref.repoAggregations,
        langAggregations = _ref.langAggregations,
        took = _ref.took;
    draft.isLoading = false;
    var repoStats = repoAggregations.map(function (agg) {
      return {
        name: agg.key,
        value: agg.doc_count
      };
    });
    var languageStats = langAggregations.map(function (agg) {
      return {
        name: agg.key,
        value: agg.doc_count
      };
    });
    draft.documentSearchResults = _objectSpread({}, draft.documentSearchResults, {
      query: state.query,
      total: total,
      took: took,
      stats: {
        total: total,
        from: from,
        to: from + results.length,
        page: page,
        totalPage: totalPage,
        repoStats: repoStats,
        languageStats: languageStats
      },
      results: results
    });
  });
}), _defineProperty(_handleActions, String(_actions.documentSearchFailed), function (state, action) {
  if (action.payload) {
    return (0, _immer.default)(state, function (draft) {
      draft.isLoading = false;
      draft.error = action.payload;
    });
  } else {
    return state;
  }
}), _defineProperty(_handleActions, String(_actions.suggestionSearch), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload) {
      draft.query = action.payload;
    }
  });
}), _defineProperty(_handleActions, String(_actions.repositorySearch), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload) {
      draft.query = action.payload.query;
      draft.isLoading = true;
      delete draft.error;
      delete draft.repositorySearchResults;
    }
  });
}), _defineProperty(_handleActions, String(_actions.repositorySearchSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.repositorySearchResults = action.payload;
    draft.isLoading = false;
  });
}), _defineProperty(_handleActions, String(_actions.repositorySearchFailed), function (state, action) {
  if (action.payload) {
    return (0, _immer.default)(state, function (draft) {
      draft.isLoading = false;
      draft.error = action.payload;
    });
  } else {
    return state;
  }
}), _defineProperty(_handleActions, String(_actions.saveSearchOptions), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.searchOptions = action.payload;
  });
}), _defineProperty(_handleActions, String(_actions.searchReposForScope), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.isScopeSearchLoading = true;
  });
}), _defineProperty(_handleActions, String(_actions.searchReposForScopeSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.scopeSearchResults = action.payload;
    draft.isScopeSearchLoading = false;
  });
}), _defineProperty(_handleActions, String(_actions.turnOnDefaultRepoScope), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.searchOptions.defaultRepoScope = action.payload;
    draft.searchOptions.defaultRepoScopeOn = true;
  });
}), _defineProperty(_handleActions, String(_actions.turnOffDefaultRepoScope), function (state) {
  return (0, _immer.default)(state, function (draft) {
    delete draft.searchOptions.defaultRepoScope;
    draft.searchOptions.defaultRepoScopeOn = false;
  });
}), _handleActions), initialState);
exports.search = search;