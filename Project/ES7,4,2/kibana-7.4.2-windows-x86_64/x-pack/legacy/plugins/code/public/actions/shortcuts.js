"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toggleHelp = exports.unregisterShortcut = exports.registerShortcut = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var registerShortcut = (0, _reduxActions.createAction)('REGISTER SHORTCUT');
exports.registerShortcut = registerShortcut;
var unregisterShortcut = (0, _reduxActions.createAction)('UNREGISTER SHORTCUT');
exports.unregisterShortcut = unregisterShortcut;
var toggleHelp = (0, _reduxActions.createAction)('TOGGLE SHORTCUTS HELP');
exports.toggleHelp = toggleHelp;