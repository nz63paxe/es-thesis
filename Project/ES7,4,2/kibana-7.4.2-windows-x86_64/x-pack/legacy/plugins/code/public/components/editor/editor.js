"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Editor = exports.EditorComponent = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _actions = require("../../actions");

var _blame_widget = require("../../monaco/blame/blame_widget");

var _monaco = require("../../monaco/monaco");

var _monaco_helper = require("../../monaco/monaco_helper");

var _selectors = require("../../selectors");

var _url = require("../../utils/url");

var _shortcuts = require("../shortcuts");

var _references_panel = require("./references_panel");

var _uri_util = require("../../../common/uri_util");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var EditorComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(EditorComponent, _React$Component);

  function EditorComponent(props, context) {
    var _this;

    _classCallCheck(this, EditorComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(EditorComponent).call(this, props, context));

    _defineProperty(_assertThisInitialized(_this), "blameWidgets", void 0);

    _defineProperty(_assertThisInitialized(_this), "container", void 0);

    _defineProperty(_assertThisInitialized(_this), "monaco", void 0);

    _defineProperty(_assertThisInitialized(_this), "editor", void 0);

    _defineProperty(_assertThisInitialized(_this), "lineDecorations", null);

    _defineProperty(_assertThisInitialized(_this), "gutterClickHandler", void 0);

    _defineProperty(_assertThisInitialized(_this), "registerGutterClickHandler", function () {
      if (!_this.gutterClickHandler) {
        _this.gutterClickHandler = _this.editor.onMouseDown(function (e) {
          // track line number click count
          (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.COUNT, _usage_telemetry_metrics.CodeUIUsageMetrics.LINE_NUMBER_CLICK_COUNT);
          var _this$props$match$par = _this.props.match.params,
              resource = _this$props$match$par.resource,
              org = _this$props$match$par.org,
              repo = _this$props$match$par.repo,
              revision = _this$props$match$par.revision,
              path = _this$props$match$par.path,
              pathType = _this$props$match$par.pathType;
          var queryString = _this.props.location.search;
          var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);

          if (e.target.type === _monaco.monaco.editor.MouseTargetType.GUTTER_LINE_NUMBERS) {
            var url = "".concat(repoUri, "/").concat(pathType, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path);
            var position = e.target.position || {
              lineNumber: 0,
              column: 0
            };

            _url.history.push("/".concat(url, "!L").concat(position.lineNumber, ":0").concat(queryString));
          }

          _this.monaco.container.focus();
        });
      }
    });

    return _this;
  }

  _createClass(EditorComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.container = document.getElementById('mainEditor');
      this.monaco = new _monaco_helper.MonacoHelper(this.container, this.props, this.props.location.search);

      if (!this.props.revealPosition) {
        this.monaco.clearLineSelection();
      }

      var file = this.props.file;

      if (file && file.content) {
        var _file$payload = file.payload,
            uri = _file$payload.uri,
            path = _file$payload.path,
            revision = _file$payload.revision;
        this.loadText(file.content, uri, path, file.lang, revision).then(function () {
          if (_this2.props.revealPosition) {
            _this2.revealPosition(_this2.props.revealPosition);
          }

          if (_this2.props.showBlame) {
            _this2.loadBlame(_this2.props.blames);
          }
        });
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this3 = this;

      var file = this.props.file;

      if (!file) {
        return;
      }

      var _file$payload2 = file.payload,
          uri = _file$payload2.uri,
          path = _file$payload2.path,
          revision = _file$payload2.revision;
      var _this$props$match$par2 = this.props.match.params,
          resource = _this$props$match$par2.resource,
          org = _this$props$match$par2.org,
          repo = _this$props$match$par2.repo,
          routeRevision = _this$props$match$par2.revision,
          routePath = _this$props$match$par2.path;
      var prevContent = prevProps.file && prevProps.file.content;
      var qs = this.props.location.search;

      if (!this.props.revealPosition && this.monaco) {
        this.monaco.clearLineSelection();
      }

      if (prevContent !== file.content) {
        this.loadText(file.content, uri, path, file.lang, revision).then(function () {
          if (_this3.props.revealPosition) {
            _this3.revealPosition(_this3.props.revealPosition);
          }
        });
      } else if (file.payload.uri === "".concat(resource, "/").concat(org, "/").concat(repo) && file.payload.revision === routeRevision && file.payload.path === routePath && prevProps.revealPosition !== this.props.revealPosition) {
        this.revealPosition(this.props.revealPosition);
      }

      if (this.monaco && qs !== prevProps.location.search) {
        this.monaco.updateUrlQuery(qs);
      }

      if (this.editor) {
        if (prevProps.showBlame !== this.props.showBlame && this.props.showBlame) {
          this.editor.updateOptions({
            lineDecorationsWidth: 316
          });
          this.loadBlame(this.props.blames);
        } else if (!this.props.showBlame) {
          this.destroyBlameWidgets();
          this.editor.updateOptions({
            lineDecorationsWidth: 16
          });
        }

        if (prevProps.blames !== this.props.blames && this.props.showBlame) {
          this.editor.updateOptions({
            lineDecorationsWidth: 316
          });
          this.loadBlame(this.props.blames);
        }
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.gutterClickHandler) {
        this.gutterClickHandler.dispose();
      }

      this.monaco.destroy();
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement(_eui.EuiFlexItem, {
        "data-test-subj": "codeSourceViewer",
        className: "codeOverflowHidden",
        grow: this.props.hidden ? false : 1,
        hidden: this.props.hidden
      }, _react.default.createElement(_shortcuts.Shortcut, {
        keyCode: "f",
        help: "With editor \u2018active\u2019 Find in file",
        linuxModifier: [_shortcuts.Modifier.ctrl],
        macModifier: [_shortcuts.Modifier.meta],
        winModifier: [_shortcuts.Modifier.ctrl]
      }), _react.default.createElement("div", {
        tabIndex: 0,
        className: "codeContainer__editor",
        id: "mainEditor",
        hidden: this.props.hidden
      }), this.renderReferences());
    }
  }, {
    key: "loadBlame",
    value: function loadBlame(blames) {
      var _this4 = this;

      if (this.blameWidgets) {
        this.destroyBlameWidgets();
      }

      if (!this.lineDecorations) {
        this.lineDecorations = this.monaco.editor.deltaDecorations([], [{
          range: new _monaco.monaco.Range(1, 1, Infinity, 1),
          options: {
            isWholeLine: true,
            linesDecorationsClassName: 'code-line-decoration'
          }
        }]);
      }

      this.blameWidgets = blames.map(function (b, index) {
        return new _blame_widget.BlameWidget(b, index === 0, _this4.monaco.editor);
      });
    }
  }, {
    key: "destroyBlameWidgets",
    value: function destroyBlameWidgets() {
      if (this.blameWidgets) {
        this.blameWidgets.forEach(function (bw) {
          return bw.destroy();
        });
      }

      if (this.lineDecorations) {
        this.monaco.editor.deltaDecorations(this.lineDecorations, []);
        this.lineDecorations = null;
      }

      this.blameWidgets = null;
    }
  }, {
    key: "loadText",
    value: function () {
      var _loadText = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(text, repo, file, lang, revision) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!this.monaco) {
                  _context.next = 14;
                  break;
                }

                _context.prev = 1;
                _context.next = 4;
                return _monaco.monaco.editor.colorize(text, lang, {});

              case 4:
                _context.next = 9;
                break;

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](1);
                // workaround a upstream issue: https://github.com/microsoft/monaco-editor/issues/134
                lang = 'text';

              case 9:
                _context.next = 11;
                return this.monaco.loadFile(repo, file, text, lang, revision);

              case 11:
                this.editor = _context.sent;

                if (this.props.showBlame) {
                  this.editor.updateOptions({
                    lineDecorationsWidth: 316
                  });
                  this.loadBlame(this.props.blames);
                }

                this.registerGutterClickHandler();

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[1, 6]]);
      }));

      function loadText(_x, _x2, _x3, _x4, _x5) {
        return _loadText.apply(this, arguments);
      }

      return loadText;
    }()
  }, {
    key: "revealPosition",
    value: function revealPosition(pos) {
      if (this.monaco) {
        if (pos) {
          this.monaco.revealPosition(pos.line, pos.character);
        } else {
          this.monaco.clearLineSelection();
        }
      }
    }
  }, {
    key: "renderReferences",
    value: function renderReferences() {
      var _this5 = this;

      return this.props.panelShowing && _react.default.createElement(_references_panel.ReferencesPanel, {
        onClose: function onClose() {
          return _this5.props.closePanel(true);
        },
        references: this.props.panelContents,
        isLoading: this.props.isPanelLoading,
        title: this.props.panelTitle,
        refUrl: this.props.refUrl
      });
    }
  }]);

  return EditorComponent;
}(_react.default.Component);

exports.EditorComponent = EditorComponent;

_defineProperty(EditorComponent, "defaultProps", {
  hidden: false
});

var mapStateToProps = function mapStateToProps(state) {
  return {
    file: state.file.file,
    panelShowing: state.editor.panelShowing,
    isPanelLoading: state.editor.loading,
    panelContents: state.editor.panelContents,
    panelTitle: state.editor.panelTitle,
    hover: state.editor.hover,
    refUrl: (0, _selectors.refUrlSelector)(state),
    revealPosition: state.editor.revealPosition,
    blames: state.blame.blames
  };
};

var mapDispatchToProps = {
  closePanel: _actions.closePanel,
  hoverResult: _actions.hoverResult
};
var Editor = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(EditorComponent));
exports.Editor = Editor;