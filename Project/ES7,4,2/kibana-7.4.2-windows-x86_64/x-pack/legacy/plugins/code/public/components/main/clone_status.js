"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CloneStatus = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react2 = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CloneStatus = function CloneStatus(props) {
  var progressRate = props.progress,
      cloneProgress = props.cloneProgress,
      repoName = props.repoName;

  var progress = _i18n.i18n.translate('xpack.code.mainPage.content.cloneStatus.progress.receivingRateOnlyText', {
    defaultMessage: 'Receiving objects: {progressRate}%',
    values: {
      progressRate: progressRate.toFixed(2)
    }
  });

  if (progressRate < 0) {
    progress = _i18n.i18n.translate('xpack.code.mainPage.content.cloneStatus.progress.cloneFailedText', {
      defaultMessage: 'Clone Failed'
    });
  } else if (cloneProgress) {
    var receivedObjects = cloneProgress.receivedObjects,
        totalObjects = cloneProgress.totalObjects,
        indexedObjects = cloneProgress.indexedObjects;

    if (receivedObjects === totalObjects) {
      progress = _i18n.i18n.translate('xpack.code.mainPage.content.cloneStatus.progress.indexingText', {
        defaultMessage: 'Indexing objects: {progressRate}% {indexedObjects}/{totalObjects}',
        values: {
          progressRate: (indexedObjects * 100 / totalObjects).toFixed(2),
          indexedObjects: indexedObjects,
          totalObjects: totalObjects
        }
      });
    } else {
      progress = _i18n.i18n.translate('xpack.code.mainPage.content.cloneStatus.progress.receivingText', {
        defaultMessage: 'Receiving objects: {progressRate}% {receivedObjects}/{totalObjects}',
        values: {
          progressRate: (receivedObjects * 100 / totalObjects).toFixed(2),
          receivedObjects: receivedObjects,
          totalObjects: totalObjects
        }
      });
    }
  }

  return _react2.default.createElement(_eui.EuiFlexGroup, {
    direction: "column",
    alignItems: "center"
  }, _react2.default.createElement(_eui.EuiSpacer, {
    size: "xxl"
  }), _react2.default.createElement(_eui.EuiSpacer, {
    size: "xxl"
  }), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiText, {
    style: {
      fontSize: _eui_theme_light.default.euiSizeXXL,
      color: '#1A1A1A'
    }
  }, repoName, ' ', _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.mainPage.content.cloneStatus.isCloningText",
    defaultMessage: "is cloning"
  }))), _react2.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiText, {
    style: {
      fontSize: _eui_theme_light.default.euiSizeM,
      color: '#69707D'
    }
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.mainPage.content.cloneStatus.yourProjectWillBeAvailableText",
    defaultMessage: "Your project will be available when this process is complete"
  }))), _react2.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiText, {
    size: "m",
    color: "subdued"
  }, progress))), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      minWidth: 640
    }
  }, _react2.default.createElement(_eui.EuiProgress, {
    color: "primary",
    size: "s",
    max: 100,
    value: progressRate
  })));
};

exports.CloneStatus = CloneStatus;