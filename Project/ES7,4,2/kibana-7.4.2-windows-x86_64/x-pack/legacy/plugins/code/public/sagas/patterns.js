"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.blamePattern = exports.sourceFilePattern = exports.commitRoutePattern = exports.searchRoutePattern = exports.mainRoutePattern = exports.repoRoutePattern = exports.adminRoutePattern = exports.setupRoutePattern = exports.rootRoutePattern = exports.generatePattern = void 0;

var _actions = require("../actions");

var _types = require("../common/types");

var ROUTES = _interopRequireWildcard(require("../components/routes"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var generatePattern = function generatePattern(path) {
  return function (action) {
    return action.type === String(_actions.routeChange) && action.payload.path === path;
  };
};

exports.generatePattern = generatePattern;
var rootRoutePattern = generatePattern(ROUTES.ROOT);
exports.rootRoutePattern = rootRoutePattern;
var setupRoutePattern = generatePattern(ROUTES.SETUP);
exports.setupRoutePattern = setupRoutePattern;
var adminRoutePattern = generatePattern(ROUTES.ADMIN);
exports.adminRoutePattern = adminRoutePattern;
var repoRoutePattern = generatePattern(ROUTES.REPO);
exports.repoRoutePattern = repoRoutePattern;

var mainRoutePattern = function mainRoutePattern(action) {
  return action.type === String(_actions.routeChange) && (ROUTES.MAIN === action.payload.path || ROUTES.MAIN_ROOT === action.payload.path);
};

exports.mainRoutePattern = mainRoutePattern;
var searchRoutePattern = generatePattern(ROUTES.SEARCH);
exports.searchRoutePattern = searchRoutePattern;
var commitRoutePattern = generatePattern(ROUTES.DIFF);
exports.commitRoutePattern = commitRoutePattern;

var sourceFilePattern = function sourceFilePattern(action) {
  return mainRoutePattern(action) && action.payload.params.pathType === _types.PathTypes.blob;
};

exports.sourceFilePattern = sourceFilePattern;

var blamePattern = function blamePattern(action) {
  return mainRoutePattern(action) && action.payload.params.pathType === _types.PathTypes.blame;
};

exports.blamePattern = blamePattern;