"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CodeResult = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _repository_utils = require("../../../common/repository_utils");

var _url = require("../../utils/url");

var _codeblock = require("../codeblock/codeblock");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CodeResult =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeResult, _React$PureComponent);

  function CodeResult() {
    _classCallCheck(this, CodeResult);

    return _possibleConstructorReturn(this, _getPrototypeOf(CodeResult).apply(this, arguments));
  }

  _createClass(CodeResult, [{
    key: "render",
    value: function render() {
      var _this = this;

      var _this$props = this.props,
          results = _this$props.results,
          query = _this$props.query;
      return results.map(function (item) {
        var uri = item.uri,
            filePath = item.filePath,
            hits = item.hits,
            compositeContent = item.compositeContent;
        var content = compositeContent.content,
            lineMapping = compositeContent.lineMapping,
            ranges = compositeContent.ranges;
        var repoLinkUrl = "/".concat(uri, "/tree/HEAD/");
        var fileLinkUrl = "/".concat(uri, "/blob/HEAD/").concat(filePath);
        var key = "".concat(uri, "-").concat(filePath, "-").concat(query);

        var lineMappingFunc = function lineMappingFunc(l) {
          return lineMapping[l - 1];
        };

        return _react2.default.createElement("div", {
          key: "resultitem".concat(key),
          "data-test-subj": "codeSearchResultList"
        }, _react2.default.createElement("div", {
          style: {
            marginBottom: '.5rem'
          }
        }, _react2.default.createElement(_reactRouterDom.Link, {
          to: repoLinkUrl
        }, _react2.default.createElement(_eui.EuiFlexGroup, {
          direction: "row",
          alignItems: "center",
          justifyContent: "flexStart",
          gutterSize: "none"
        }, _react2.default.createElement(_eui.EuiFlexItem, {
          grow: false
        }, _react2.default.createElement(_eui.EuiText, {
          size: "s",
          color: "subdued"
        }, _repository_utils.RepositoryUtils.orgNameFromUri(uri), "/")), _react2.default.createElement(_eui.EuiFlexItem, {
          grow: false
        }, _react2.default.createElement(_eui.EuiText, {
          size: "s",
          color: "default"
        }, _react2.default.createElement("strong", null, _repository_utils.RepositoryUtils.repoNameFromUri(uri))))))), _react2.default.createElement(_eui.EuiFlexGroup, {
          alignItems: "center",
          justifyContent: "flexStart",
          gutterSize: "xs",
          style: {
            marginBottom: '1rem'
          }
        }, _react2.default.createElement(_eui.EuiFlexItem, {
          grow: false
        }, _react2.default.createElement(_eui.EuiBadge, {
          color: "default"
        }, hits)), _react2.default.createElement(_eui.EuiText, {
          size: "s"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchPage.hitsCountText",
          defaultMessage: " hits from "
        }), _react2.default.createElement(_reactRouterDom.Link, {
          to: fileLinkUrl,
          "data-test-subj": "codeSearchResultFileItem"
        }, filePath))), _react2.default.createElement(_codeblock.CodeBlock, {
          key: "code".concat(key),
          language: item.language,
          startLine: 0,
          code: content,
          highlightRanges: ranges,
          folding: false,
          lineNumbersFunc: lineMappingFunc,
          onClick: _this.onCodeClick.bind(_this, lineMapping, fileLinkUrl)
        }));
      });
    }
  }, {
    key: "onCodeClick",
    value: function onCodeClick(lineNumbers, fileUrl, pos) {
      var line = parseInt(lineNumbers[pos.lineNumber - 1], 10);

      if (!isNaN(line)) {
        _url.history.push("".concat(fileUrl, "!L").concat(line, ":0"));
      }
    }
  }]);

  return CodeResult;
}(_react2.default.PureComponent);

exports.CodeResult = CodeResult;