"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filePathChange = exports.revisionChange = exports.repoChange = exports.routePathChange = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var routePathChange = (0, _reduxActions.createAction)('ROUTE PATH CHANGE');
exports.routePathChange = routePathChange;
var repoChange = (0, _reduxActions.createAction)('REPOSITORY CHANGE');
exports.repoChange = repoChange;
var revisionChange = (0, _reduxActions.createAction)('REVISION CHANGE');
exports.revisionChange = revisionChange;
var filePathChange = (0, _reduxActions.createAction)('FILE PATH CHANGE');
exports.filePathChange = filePathChange;