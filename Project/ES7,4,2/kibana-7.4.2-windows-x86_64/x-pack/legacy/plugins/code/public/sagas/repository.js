"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchImportRepo = watchImportRepo;
exports.watchDeleteRepo = watchDeleteRepo;
exports.watchIndexRepo = watchIndexRepo;
exports.watchFetchRepos = watchFetchRepos;
exports.watchFetchRepoConfigs = watchFetchRepoConfigs;
exports.watchInitRepoCmd = watchInitRepoCmd;
exports.watchGotoRepo = watchGotoRepo;
exports.watchAdminRouteChange = watchAdminRouteChange;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _language_server = require("../actions/language_server");

var _url = require("../utils/url");

var _patterns = require("./patterns");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchRepos),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(handleDeleteRepo),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleIndexRepo),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(handleImportRepo),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchRepoConfigs),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(handleInitCmd),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(handleGotoRepo),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(watchImportRepo),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(watchDeleteRepo),
    _marked10 =
/*#__PURE__*/
regeneratorRuntime.mark(watchIndexRepo),
    _marked11 =
/*#__PURE__*/
regeneratorRuntime.mark(watchFetchRepos),
    _marked12 =
/*#__PURE__*/
regeneratorRuntime.mark(watchFetchRepoConfigs),
    _marked13 =
/*#__PURE__*/
regeneratorRuntime.mark(watchInitRepoCmd),
    _marked14 =
/*#__PURE__*/
regeneratorRuntime.mark(watchGotoRepo),
    _marked15 =
/*#__PURE__*/
regeneratorRuntime.mark(handleAdminRouteChange),
    _marked16 =
/*#__PURE__*/
regeneratorRuntime.mark(watchAdminRouteChange);

function requestRepos() {
  return _new_platform.npStart.core.http.get('/api/code/repos');
}

function handleFetchRepos() {
  var repos;
  return regeneratorRuntime.wrap(function handleFetchRepos$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.call)(requestRepos);

        case 3:
          repos = _context.sent;
          _context.next = 6;
          return (0, _effects.put)((0, _actions.fetchReposSuccess)(repos));

        case 6:
          _context.next = 12;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          _context.next = 12;
          return (0, _effects.put)((0, _actions.fetchReposFailed)(_context.t0));

        case 12:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 8]]);
}

function requestDeleteRepo(uri) {
  return _new_platform.npStart.core.http.delete("/api/code/repo/".concat(uri));
}

function requestIndexRepo(uri) {
  return _new_platform.npStart.core.http.post("/api/code/repo/index/".concat(uri), {
    body: JSON.stringify({
      reindex: true
    })
  });
}

function handleDeleteRepo(action) {
  return regeneratorRuntime.wrap(function handleDeleteRepo$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(requestDeleteRepo, action.payload || '');

        case 3:
          _context2.next = 5;
          return (0, _effects.put)((0, _actions.deleteRepoSuccess)(action.payload || ''));

        case 5:
          _context2.next = 7;
          return (0, _effects.put)((0, _actions.updateDeleteProgress)({
            uri: action.payload,
            progress: 0
          }));

        case 7:
          _context2.next = 13;
          break;

        case 9:
          _context2.prev = 9;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 13;
          return (0, _effects.put)((0, _actions.deleteRepoFailed)(_context2.t0));

        case 13:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 9]]);
}

function handleIndexRepo(action) {
  return regeneratorRuntime.wrap(function handleIndexRepo$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.call)(requestIndexRepo, action.payload || '');

        case 3:
          _context3.next = 5;
          return (0, _effects.put)((0, _actions.indexRepoSuccess)(action.payload || ''));

        case 5:
          _context3.next = 7;
          return (0, _effects.put)((0, _actions.updateIndexProgress)({
            uri: action.payload,
            progress: 0
          }));

        case 7:
          _context3.next = 13;
          break;

        case 9:
          _context3.prev = 9;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 13;
          return (0, _effects.put)((0, _actions.indexRepoFailed)(_context3.t0));

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 9]]);
}

function requestImportRepo(uri) {
  return _new_platform.npStart.core.http.post('/api/code/repo', {
    body: JSON.stringify({
      url: uri
    })
  });
}

function handleImportRepo(action) {
  var data;
  return regeneratorRuntime.wrap(function handleImportRepo$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.prev = 0;
          _context4.next = 3;
          return (0, _effects.call)(requestImportRepo, action.payload || '');

        case 3:
          data = _context4.sent;
          _context4.next = 6;
          return (0, _effects.put)((0, _actions.importRepoSuccess)(data));

        case 6:
          _context4.next = 12;
          break;

        case 8:
          _context4.prev = 8;
          _context4.t0 = _context4["catch"](0);
          _context4.next = 12;
          return (0, _effects.put)((0, _actions.importRepoFailed)(_context4.t0));

        case 12:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4, null, [[0, 8]]);
}

function handleFetchRepoConfigs() {
  var configs;
  return regeneratorRuntime.wrap(function handleFetchRepoConfigs$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.call)(requestRepoConfigs);

        case 3:
          configs = _context5.sent;
          _context5.next = 6;
          return (0, _effects.put)((0, _actions.fetchRepoConfigSuccess)(configs));

        case 6:
          _context5.next = 12;
          break;

        case 8:
          _context5.prev = 8;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 12;
          return (0, _effects.put)((0, _actions.fetchRepoConfigFailed)(_context5.t0));

        case 12:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 8]]);
}

function requestRepoConfigs() {
  return _new_platform.npStart.core.http.get('/api/code/workspace');
}

function handleInitCmd(action) {
  var repoUri;
  return regeneratorRuntime.wrap(function handleInitCmd$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          repoUri = action.payload;
          _context6.next = 3;
          return (0, _effects.call)(requestRepoInitCmd, repoUri);

        case 3:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function requestRepoInitCmd(repoUri) {
  return _new_platform.npStart.core.http.post("/api/code/workspace/".concat(repoUri, "/master"), {
    query: {
      force: true
    }
  });
}

function handleGotoRepo(action) {
  var _repoUri, loadRepoDoneAction;

  return regeneratorRuntime.wrap(function handleGotoRepo$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _repoUri = action.payload;
          _context7.next = 4;
          return (0, _effects.put)((0, _actions.loadRepo)(_repoUri));

        case 4:
          _context7.next = 6;
          return (0, _effects.take)([String(_actions.loadRepoSuccess), String(_actions.loadRepoFailed)]);

        case 6:
          loadRepoDoneAction = _context7.sent;

          if (loadRepoDoneAction.type === String(_actions.loadRepoSuccess)) {
            _url.history.replace("/".concat(_repoUri, "/tree/").concat(loadRepoDoneAction.payload.defaultBranch || 'master'));
          } else {
            // redirect to root project path if repo not found to show 404 page
            _url.history.replace("/".concat(action.payload, "/tree/master"));
          }

          _context7.next = 15;
          break;

        case 10:
          _context7.prev = 10;
          _context7.t0 = _context7["catch"](0);

          _url.history.replace("/".concat(action.payload, "/tree/master"));

          _context7.next = 15;
          return (0, _effects.put)((0, _actions.gotoRepoFailed)(_context7.t0));

        case 15:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, null, [[0, 10]]);
}

function watchImportRepo() {
  return regeneratorRuntime.wrap(function watchImportRepo$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.takeEvery)(String(_actions.importRepo), handleImportRepo);

        case 2:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8);
}

function watchDeleteRepo() {
  return regeneratorRuntime.wrap(function watchDeleteRepo$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _effects.takeEvery)(String(_actions.deleteRepo), handleDeleteRepo);

        case 2:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}

function watchIndexRepo() {
  return regeneratorRuntime.wrap(function watchIndexRepo$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.takeEvery)(String(_actions.indexRepo), handleIndexRepo);

        case 2:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10);
}

function watchFetchRepos() {
  return regeneratorRuntime.wrap(function watchFetchRepos$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchRepos), handleFetchRepos);

        case 2:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}

function watchFetchRepoConfigs() {
  return regeneratorRuntime.wrap(function watchFetchRepoConfigs$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _context12.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchRepoConfigs), handleFetchRepoConfigs);

        case 2:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12);
}

function watchInitRepoCmd() {
  return regeneratorRuntime.wrap(function watchInitRepoCmd$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.next = 2;
          return (0, _effects.takeEvery)(String(_actions.initRepoCommand), handleInitCmd);

        case 2:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13);
}

function watchGotoRepo() {
  return regeneratorRuntime.wrap(function watchGotoRepo$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          _context14.next = 2;
          return (0, _effects.takeLatest)(String(_actions.gotoRepo), handleGotoRepo);

        case 2:
        case "end":
          return _context14.stop();
      }
    }
  }, _marked14);
}

function handleAdminRouteChange() {
  return regeneratorRuntime.wrap(function handleAdminRouteChange$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.next = 2;
          return (0, _effects.put)((0, _actions.fetchRepos)());

        case 2:
          _context15.next = 4;
          return (0, _effects.put)((0, _actions.fetchRepoConfigs)());

        case 4:
          _context15.next = 6;
          return (0, _effects.put)((0, _language_server.loadLanguageServers)());

        case 6:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked15);
}

function watchAdminRouteChange() {
  return regeneratorRuntime.wrap(function watchAdminRouteChange$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          _context16.next = 2;
          return (0, _effects.takeLatest)(_patterns.adminRoutePattern, handleAdminRouteChange);

        case 2:
        case "end":
          return _context16.stop();
      }
    }
  }, _marked16);
}