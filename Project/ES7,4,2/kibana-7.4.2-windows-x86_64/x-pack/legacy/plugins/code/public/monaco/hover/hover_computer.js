"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HoverComputer = exports.LOADING = void 0;

var _lsp_client = require("../../../common/lsp_client");

var _index = require("../../stores/index");

var _selectors = require("../../selectors");

var _repo_file_status = require("../../../common/repo_file_status");

var _lsp_error_codes = require("../../../common/lsp_error_codes");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LOADING = 'loading';
exports.LOADING = LOADING;

var HoverComputer =
/*#__PURE__*/
function () {
  function HoverComputer() {
    _classCallCheck(this, HoverComputer);

    _defineProperty(this, "lspMethods", void 0);

    _defineProperty(this, "range", null);

    _defineProperty(this, "uri", null);

    var lspClient = new _lsp_client.LspRestClient('/api/code/lsp');
    this.lspMethods = new _lsp_client.TextDocumentMethods(lspClient);
  }

  _createClass(HoverComputer, [{
    key: "setParams",
    value: function setParams(uri, range) {
      this.range = range;
      this.uri = uri;
    }
  }, {
    key: "compute",
    value: function compute() {
      var status = (0, _selectors.statusSelector)(_index.store.getState());

      if (status && status.langServerType !== _repo_file_status.LangServerType.NONE && status.fileStatus !== _repo_file_status.RepoFileStatus.FILE_NOT_SUPPORTED && status.fileStatus !== _repo_file_status.RepoFileStatus.FILE_IS_TOO_BIG) {
        if (status.langServerStatus === _repo_file_status.RepoFileStatus.LANG_SERVER_IS_INITIALIZING) {
          return this.initializing();
        }

        return this.lspMethods.hover.asyncTask({
          position: {
            line: this.range.startLineNumber - 1,
            character: this.range.startColumn - 1
          },
          textDocument: {
            uri: this.uri
          }
        });
      }

      return this.empty();
    }
  }, {
    key: "empty",
    value: function empty() {
      var empty = {
        range: this.lspRange(),
        contents: []
      };
      return {
        cancel: function cancel() {},
        promise: function promise() {
          return Promise.resolve(empty);
        }
      };
    }
  }, {
    key: "initializing",
    value: function initializing() {
      return {
        cancel: function cancel() {},
        promise: function promise() {
          return Promise.reject({
            code: _lsp_error_codes.ServerNotInitialized
          });
        }
      };
    }
  }, {
    key: "lspRange",
    value: function lspRange() {
      return {
        start: {
          line: this.range.startLineNumber - 1,
          character: this.range.startColumn - 1
        },
        end: {
          line: this.range.endLineNumber - 1,
          character: this.range.endColumn - 1
        }
      };
    }
  }, {
    key: "loadingMessage",
    value: function loadingMessage() {
      return {
        range: this.lspRange(),
        contents: LOADING
      };
    }
  }]);

  return HoverComputer;
}();

exports.HoverComputer = HoverComputer;