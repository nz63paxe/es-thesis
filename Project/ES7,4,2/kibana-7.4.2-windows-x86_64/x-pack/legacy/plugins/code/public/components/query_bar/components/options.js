"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchOptions = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _react2 = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SearchOptions =
/*#__PURE__*/
function (_Component) {
  _inherits(SearchOptions, _Component);

  function SearchOptions() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SearchOptions);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SearchOptions)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      query: '',
      isFlyoutOpen: false,
      repoScope: _this.props.searchOptions.repoScope,
      defaultRepoScopeOn: _this.props.searchOptions.defaultRepoScopeOn
    });

    _defineProperty(_assertThisInitialized(_this), "applyAndClose", function () {
      if (_this.state.defaultRepoScopeOn && _this.props.defaultSearchScope) {
        _this.props.saveSearchOptions({
          repoScope: (0, _lodash.unique)([].concat(_toConsumableArray(_this.state.repoScope), [_this.props.defaultSearchScope]), function (r) {
            return r.uri;
          }),
          defaultRepoScopeOn: _this.state.defaultRepoScopeOn
        });
      } else {
        _this.props.saveSearchOptions({
          repoScope: _this.state.repoScope,
          defaultRepoScopeOn: _this.state.defaultRepoScopeOn
        });
      }

      _this.setState({
        isFlyoutOpen: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "removeRepoScope", function (r) {
      return function () {
        _this.setState(function (prevState) {
          var nextState = {
            repoScope: prevState.repoScope.filter(function (rs) {
              return rs.uri !== r;
            })
          };

          if (_this.props.defaultSearchScope && r === _this.props.defaultSearchScope.uri) {
            nextState.defaultRepoScopeOn = false;
          }

          return nextState;
        });
      };
    });

    _defineProperty(_assertThisInitialized(_this), "onRepoSearchChange", function (searchValue) {
      _this.setState({
        query: searchValue
      });

      if (searchValue) {
        _this.props.repositorySearch({
          query: searchValue
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onRepoChange", function (repos) {
      _this.setState(function (prevState) {
        return {
          repoScope: (0, _lodash.unique)([].concat(_toConsumableArray(prevState.repoScope), _toConsumableArray(repos.map(function (r) {
            return [].concat(_toConsumableArray(_this.props.repoSearchResults), _toConsumableArray(_this.props.defaultRepoOptions)).find(function (rs) {
              return rs.name === r.label;
            });
          }))), function (r) {
            return r.uri;
          })
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggleOptionsFlyout", function () {
      _this.setState({
        isFlyoutOpen: !_this.state.isFlyoutOpen
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeOptionsFlyout", function () {
      _this.setState({
        isFlyoutOpen: false,
        repoScope: _this.props.searchOptions.repoScope,
        defaultRepoScopeOn: _this.props.searchOptions.defaultRepoScopeOn
      });
    });

    return _this;
  }

  _createClass(SearchOptions, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (this.props.searchOptions.defaultRepoScopeOn !== prevProps.searchOptions.defaultRepoScopeOn) {
        this.setState({
          defaultRepoScopeOn: this.props.searchOptions.defaultRepoScopeOn
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var optionsFlyout;
      var repoScope = this.state.defaultRepoScopeOn && this.props.defaultSearchScope ? (0, _lodash.unique)([].concat(_toConsumableArray(this.state.repoScope), [this.props.defaultSearchScope]), function (r) {
        return r.uri;
      }) : this.state.repoScope;

      if (this.state.isFlyoutOpen) {
        var selectedRepos = repoScope.map(function (r) {
          return _react2.default.createElement("div", {
            key: r.uri
          }, _react2.default.createElement(_eui.EuiPanel, {
            paddingSize: "s"
          }, _react2.default.createElement(_eui.EuiFlexGroup, {
            gutterSize: "none",
            justifyContent: "spaceBetween",
            alignItems: "center"
          }, _react2.default.createElement("div", {
            className: "codeQueryBar"
          }, _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement(_eui.EuiTextColor, {
            color: "subdued"
          }, r.org, "/"), _react2.default.createElement("b", null, r.name))), _react2.default.createElement(_eui.EuiIcon, {
            className: "codeUtility__cursor--pointer",
            type: "cross",
            onClick: _this2.removeRepoScope(r.uri)
          }))), _react2.default.createElement(_eui.EuiSpacer, {
            size: "s"
          }));
        });
        var repoCandidates = this.state.query ? this.props.repoSearchResults.map(function (repo) {
          return {
            label: repo.name
          };
        }) : this.props.defaultRepoOptions.map(function (repo) {
          return {
            label: repo.name
          };
        });
        optionsFlyout = _react2.default.createElement(_eui.EuiFlyout, {
          onClose: this.closeOptionsFlyout,
          size: "s",
          "aria-labelledby": "flyoutSmallTitle",
          className: "codeSearchSettings__flyout"
        }, _react2.default.createElement(_eui.EuiFlyoutHeader, null, _react2.default.createElement(_eui.EuiTitle, {
          size: "s"
        }, _react2.default.createElement("h2", {
          id: "flyoutSmallTitle",
          className: ""
        }, _react2.default.createElement(_eui.EuiNotificationBadge, {
          size: "m",
          className: "code-notification-badge"
        }, repoScope.length), _react2.default.createElement(_eui.EuiTextColor, {
          color: "secondary",
          className: "code-flyout-title"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.searchFilterTitle",
          defaultMessage: " {filterCount, plural, one {Search Filter} other {Search Filters}} ",
          values: {
            filterCount: repoScope.length
          }
        }))))), _react2.default.createElement(_eui.EuiFlyoutBody, null, _react2.default.createElement(_eui.EuiTitle, {
          size: "xs"
        }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.searchScopeTitle",
          defaultMessage: "Search Scope"
        }))), _react2.default.createElement(_eui.EuiText, {
          size: "xs"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.addToScopeDescription",
          defaultMessage: "Add indexed repos to your search scope"
        })), _react2.default.createElement(_eui.EuiSpacer, {
          size: "m"
        }), _react2.default.createElement(_eui.EuiComboBox, {
          noSuggestions: repoCandidates.length === 0,
          placeholder: _i18n.i18n.translate('xpack.code.searchBar.addRepoPlaceholder', {
            defaultMessage: 'Search to add repos'
          }),
          async: true,
          options: repoCandidates,
          selectedOptions: [],
          isLoading: this.props.searchLoading,
          onChange: this.onRepoChange,
          onSearchChange: this.onRepoSearchChange,
          isClearable: true
        }), _react2.default.createElement(_eui.EuiSpacer, {
          size: "m"
        }), selectedRepos, _react2.default.createElement(_eui.EuiSpacer, {
          size: "s"
        }), _react2.default.createElement(_eui.EuiFlexGroup, {
          justifyContent: "flexEnd",
          gutterSize: "none"
        }, _react2.default.createElement(_eui.EuiButton, {
          onClick: this.applyAndClose,
          fill: true,
          iconSide: "right"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchBar.applyAndCloseButtonLabel",
          defaultMessage: "Apply and Close"
        })))));
      }

      return _react2.default.createElement("div", null, _react2.default.createElement("div", {
        className: "kuiLocalSearchAssistedInput__assistance"
      }, _react2.default.createElement(_eui.EuiButtonEmpty, {
        size: "xs",
        onClick: this.toggleOptionsFlyout
      }, _react2.default.createElement(_eui.EuiNotificationBadge, {
        size: "m",
        className: "code-notification-badge"
      }, repoScope.length), _react2.default.createElement(_eui.EuiTextColor, {
        color: "secondary"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchBar.searchFilterTitle",
        defaultMessage: " {filterCount, plural, one {Search Filter} other {Search Filters}} ",
        values: {
          filterCount: repoScope.length
        }
      })))), optionsFlyout);
    }
  }]);

  return SearchOptions;
}(_react2.Component);

exports.SearchOptions = SearchOptions;