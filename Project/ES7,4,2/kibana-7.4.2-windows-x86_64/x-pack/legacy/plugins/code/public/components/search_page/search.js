"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Search = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _querystring = _interopRequireDefault(require("querystring"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _url = _interopRequireDefault(require("url"));

var _model = require("../../../model");

var _actions = require("../../actions");

var _url2 = require("../../utils/url");

var _project_item = require("../admin_page/project_item");

var _search_bar = require("../search_bar");

var _code_result = require("./code_result");

var _empty_placeholder = require("./empty_placeholder");

var _pagination = require("./pagination");

var _side_bar = require("./side_bar");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SearchPage =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(SearchPage, _React$PureComponent);

  function SearchPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SearchPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SearchPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      uri: ''
    });

    _defineProperty(_assertThisInitialized(_this), "searchBar", null);

    _defineProperty(_assertThisInitialized(_this), "onLanguageFilterToggled", function (lang) {
      var _this$props = _this.props,
          languages = _this$props.languages,
          repositories = _this$props.repositories,
          query = _this$props.query,
          page = _this$props.page;
      var tempLangs = new Set();

      if (languages && languages.has(lang)) {
        // Remove this language filter
        tempLangs = new Set(languages);
        tempLangs.delete(lang);
      } else {
        // Add this language filter
        tempLangs = languages ? new Set(languages) : new Set();
        tempLangs.add(lang);
      }

      var queries = _querystring.default.parse(_url2.history.location.search.replace('?', ''));

      return function () {
        _url2.history.push(_url.default.format({
          pathname: '/search',
          query: _objectSpread({}, queries, {
            q: query,
            p: page,
            langs: Array.from(tempLangs).join(','),
            repos: repositories ? Array.from(repositories).join(',') : undefined
          })
        }));
      };
    });

    _defineProperty(_assertThisInitialized(_this), "onRepositoryFilterToggled", function (repo) {
      var _this$props2 = _this.props,
          languages = _this$props2.languages,
          repositories = _this$props2.repositories,
          query = _this$props2.query;
      var tempRepos = new Set();

      if (repositories && repositories.has(repo)) {
        // Remove this repository filter
        tempRepos = new Set(repositories);
        tempRepos.delete(repo);
      } else {
        // Add this language filter
        tempRepos = repositories ? new Set(repositories) : new Set();
        tempRepos.add(repo);
      }

      var queries = _querystring.default.parse(_url2.history.location.search.replace('?', ''));

      return function () {
        _url2.history.push(_url.default.format({
          pathname: '/search',
          query: _objectSpread({}, queries, {
            q: query,
            p: 1,
            langs: languages ? Array.from(languages).join(',') : undefined,
            repos: Array.from(tempRepos).join(',')
          })
        }));
      };
    });

    return _this;
  }

  _createClass(SearchPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // track search page load count
      (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.LOADED, _usage_telemetry_metrics.CodeUIUsageMetrics.SEARCH_PAGE_LOAD_COUNT);

      _chrome.default.breadcrumbs.push({
        text: "Search"
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _chrome.default.breadcrumbs.pop();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          query = _this$props3.query,
          scope = _this$props3.scope,
          documentSearchResults = _this$props3.documentSearchResults,
          languages = _this$props3.languages,
          isLoading = _this$props3.isLoading,
          repositories = _this$props3.repositories,
          repositorySearchResults = _this$props3.repositorySearchResults;
      var mainComp = isLoading ? _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, {
        size: "xl"
      }), _react2.default.createElement(_eui.EuiSpacer, {
        size: "xl"
      }), _react2.default.createElement(_eui.EuiText, {
        textAlign: "center"
      }, "Loading..."), _react2.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }), _react2.default.createElement(_eui.EuiText, {
        textAlign: "center"
      }, _react2.default.createElement(_eui.EuiLoadingSpinner, {
        size: "xl"
      }))) : _react2.default.createElement(_empty_placeholder.EmptyPlaceholder, {
        query: query,
        toggleOptionsFlyout: function toggleOptionsFlyout() {
          _this2.searchBar.toggleOptionsFlyout();
        }
      });
      var repoStats = [];
      var languageStats = [];

      if (scope === _model.SearchScope.REPOSITORY && repositorySearchResults && repositorySearchResults.total > 0) {
        var repos = repositorySearchResults.repositories,
            from = repositorySearchResults.from,
            total = repositorySearchResults.total;
        var resultComps = repos && repos.map(function (repo) {
          return _react2.default.createElement(_eui.EuiFlexItem, {
            key: repo.uri
          }, _react2.default.createElement(_project_item.ProjectItem, {
            key: repo.uri,
            project: repo,
            showStatus: false,
            enableManagement: false
          }));
        });
        var to = from + repos.length;

        var statsComp = _react2.default.createElement(_eui.EuiTitle, {
          size: "m"
        }, _react2.default.createElement("h1", null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.searchPage.showingResultsTitle",
          defaultMessage: "Showing {from} - {to} of {total} results.",
          values: {
            from: from,
            to: to,
            total: total
          }
        })));

        mainComp = _react2.default.createElement("div", {
          className: "codeContainer__search--inner"
        }, statsComp, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement("div", {
          className: "codeContainer__search--results"
        }, resultComps));
      } else if (scope === _model.SearchScope.DEFAULT && documentSearchResults) {
        var _ref = documentSearchResults,
            stats = _ref.stats,
            results = _ref.results;
        var _ref2 = stats,
            _total = _ref2.total,
            _from = _ref2.from,
            _to = _ref2.to,
            page = _ref2.page,
            totalPage = _ref2.totalPage;
        languageStats = stats.languageStats;
        repoStats = stats.repoStats;

        if (documentSearchResults.total > 0) {
          var _statsComp = _react2.default.createElement(_eui.EuiTitle, {
            size: "m"
          }, _react2.default.createElement("h1", null, _react2.default.createElement(_react.FormattedMessage, {
            id: "xpack.code.searchPage.showingResultsTitle",
            defaultMessage: "Showing {from} - {to} of {total} results.",
            values: {
              from: _from,
              to: _to,
              total: _total
            }
          })));

          mainComp = _react2.default.createElement("div", {
            className: "codeContainer__search--inner"
          }, _statsComp, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement("div", {
            className: "codeContainer__search--results"
          }, _react2.default.createElement(_code_result.CodeResult, {
            results: results,
            query: this.props.query
          })), _react2.default.createElement(_pagination.Pagination, {
            query: this.props.query,
            totalPage: totalPage,
            currentPage: page - 1
          }));
        }
      }

      return _react2.default.createElement("div", {
        className: "codeContainer__root"
      }, _react2.default.createElement("div", {
        className: "codeContainer__rootInner"
      }, _react2.default.createElement(_side_bar.SideBar, {
        query: this.props.query,
        scope: scope,
        repositories: repositories,
        languages: languages,
        repoFacets: repoStats,
        langFacets: languageStats,
        onLanguageFilterToggled: this.onLanguageFilterToggled,
        onRepositoryFilterToggled: this.onRepositoryFilterToggled
      }), _react2.default.createElement("div", {
        className: "codeContainer__search--main"
      }, _react2.default.createElement(_search_bar.SearchBar, {
        searchOptions: this.props.searchOptions,
        query: this.props.query,
        onSearchScopeChanged: this.props.onSearchScopeChanged,
        enableSubmitWhenOptionsChanged: true,
        ref: function ref(element) {
          return _this2.searchBar = element;
        }
      }), mainComp)));
    }
  }]);

  return SearchPage;
}(_react2.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return _objectSpread({}, state.search);
};

var mapDispatchToProps = {
  onSearchScopeChanged: _actions.changeSearchScope
};
var Search = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(SearchPage);
exports.Search = Search;