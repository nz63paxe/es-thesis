"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.QueryBar = exports.CodeQueryBar = void 0;

var _lodash = require("lodash");

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _reactRedux = require("react-redux");

var _actions = require("../../../actions");

var _match_pairs = require("../lib/match_pairs");

var _suggestions_component = require("./typeahead/suggestions_component");

var _types = require("../../../common/types");

var _options = require("./options");

var _scope_selector = require("./scope_selector");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var KEY_CODES = {
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  ENTER: 13,
  ESC: 27,
  TAB: 9,
  HOME: 36,
  END: 35
};

var CodeQueryBar =
/*#__PURE__*/
function (_Component) {
  _inherits(CodeQueryBar, _Component);

  function CodeQueryBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeQueryBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeQueryBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      query: _this.props.query,
      inputIsPristine: true,
      isSuggestionsVisible: false,
      groupIndex: null,
      itemIndex: null,
      suggestionGroups: [],
      showOptions: false
    });

    _defineProperty(_assertThisInitialized(_this), "updateSuggestions", (0, _lodash.debounce)(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var suggestionGroups;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _this.getSuggestions();

            case 2:
              _context.t0 = _context.sent;

              if (_context.t0) {
                _context.next = 5;
                break;
              }

              _context.t0 = [];

            case 5:
              suggestionGroups = _context.t0;

              if (!_this.componentIsUnmounting) {
                _this.setState({
                  suggestionGroups: suggestionGroups
                });
              }

            case 7:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })), 100));

    _defineProperty(_assertThisInitialized(_this), "inputRef", null);

    _defineProperty(_assertThisInitialized(_this), "optionFlyout", null);

    _defineProperty(_assertThisInitialized(_this), "componentIsUnmounting", false);

    _defineProperty(_assertThisInitialized(_this), "isDirty", function () {
      return _this.state.query !== _this.props.query;
    });

    _defineProperty(_assertThisInitialized(_this), "loadMore", function () {// TODO(mengwei): Add action for load more.
    });

    _defineProperty(_assertThisInitialized(_this), "incrementIndex", function (currGroupIndex, currItemIndex) {
      var nextItemIndex = currItemIndex + 1;

      if (currGroupIndex === null) {
        currGroupIndex = 0;
      }

      var nextGroupIndex = currGroupIndex;
      var group = _this.state.suggestionGroups[currGroupIndex];

      if (currItemIndex === null || nextItemIndex >= group.suggestions.length) {
        nextItemIndex = 0;
        nextGroupIndex = currGroupIndex + 1;

        if (nextGroupIndex >= _this.state.suggestionGroups.length) {
          nextGroupIndex = 0;
        }
      }

      _this.setState({
        groupIndex: nextGroupIndex,
        itemIndex: nextItemIndex
      });
    });

    _defineProperty(_assertThisInitialized(_this), "decrementIndex", function (currGroupIndex, currItemIndex) {
      var prevItemIndex = currItemIndex - 1;

      if (currGroupIndex === null) {
        currGroupIndex = _this.state.suggestionGroups.length - 1;
      }

      var prevGroupIndex = currGroupIndex;

      if (currItemIndex === null || prevItemIndex < 0) {
        prevGroupIndex = currGroupIndex - 1;

        if (prevGroupIndex < 0) {
          prevGroupIndex = _this.state.suggestionGroups.length - 1;
        }

        var group = _this.state.suggestionGroups[prevGroupIndex];
        prevItemIndex = group.suggestions.length - 1;
      }

      _this.setState({
        groupIndex: prevGroupIndex,
        itemIndex: prevItemIndex
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getSuggestions",
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var query, _this$inputRef, selectionStart, selectionEnd, res;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (_this.inputRef) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return");

            case 2:
              query = _this.state.query;

              if (!(query.length === 0)) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", []);

            case 5:
              if (!(!_this.props.suggestionProviders || _this.props.suggestionProviders.length === 0)) {
                _context2.next = 7;
                break;
              }

              return _context2.abrupt("return", []);

            case 7:
              _this$inputRef = _this.inputRef, selectionStart = _this$inputRef.selectionStart, selectionEnd = _this$inputRef.selectionEnd;

              if (!(selectionStart === null || selectionEnd === null)) {
                _context2.next = 10;
                break;
              }

              return _context2.abrupt("return");

            case 10:
              if (_this.props.onSuggestionQuerySubmitted) {
                _this.props.onSuggestionQuerySubmitted(query);
              }

              _context2.next = 13;
              return Promise.all(_this.props.suggestionProviders.map(function (provider) {
                // Merge the default repository scope if necessary.
                var repoScopes = _this.props.searchOptions.repoScope.map(function (repo) {
                  return repo.uri;
                });

                if (_this.props.searchOptions.defaultRepoScopeOn && _this.props.searchOptions.defaultRepoScope) {
                  repoScopes.push(_this.props.searchOptions.defaultRepoScope.uri);
                }

                return provider.getSuggestions(query, _this.props.searchScope, repoScopes);
              }));

            case 13:
              res = _context2.sent;
              return _context2.abrupt("return", res.filter(function (group) {
                return group.suggestions.length > 0;
              }));

            case 15:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    })));

    _defineProperty(_assertThisInitialized(_this), "selectSuggestion", function (item) {
      if (!_this.inputRef) {
        return;
      }

      var _this$inputRef2 = _this.inputRef,
          selectionStart = _this$inputRef2.selectionStart,
          selectionEnd = _this$inputRef2.selectionEnd;

      if (selectionStart === null || selectionEnd === null) {
        return;
      }

      _this.setState({
        query: _this.state.query,
        groupIndex: null,
        itemIndex: null,
        isSuggestionsVisible: false
      }, function () {
        if (item) {
          _this.props.onSelect(item, _this.state.query);
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onOutsideClick", function () {
      _this.setState({
        isSuggestionsVisible: false,
        groupIndex: null,
        itemIndex: null
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onClickInput", function (event) {
      if (event.target instanceof HTMLInputElement) {
        _this.onInputChange(event.target.value);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onClickSubmitButton", function (event) {
      _this.onSubmit(function () {
        return event.preventDefault();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onClickSuggestion", function (suggestion) {
      if (!_this.inputRef) {
        return;
      }

      _this.selectSuggestion(suggestion);

      _this.inputRef.focus();
    });

    _defineProperty(_assertThisInitialized(_this), "onMouseEnterSuggestion", function (groupIndex, itemIndex) {
      _this.setState({
        groupIndex: groupIndex,
        itemIndex: itemIndex
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onInputChange", function (value) {
      var hasValue = Boolean(value.trim());

      _this.setState({
        query: value,
        inputIsPristine: false,
        isSuggestionsVisible: hasValue,
        groupIndex: null,
        itemIndex: null
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (event) {
      _this.updateSuggestions();

      _this.onInputChange(event.target.value);
    });

    _defineProperty(_assertThisInitialized(_this), "onScopeChange", function (scope) {
      var onSearchScopeChanged = _this.props.onSearchScopeChanged;

      if (onSearchScopeChanged) {
        onSearchScopeChanged(scope);
      }

      _this.updateSuggestions();
    });

    _defineProperty(_assertThisInitialized(_this), "onKeyUp", function (event) {
      if ([KEY_CODES.LEFT, KEY_CODES.RIGHT, KEY_CODES.HOME, KEY_CODES.END].includes(event.keyCode)) {
        _this.setState({
          isSuggestionsVisible: true
        });

        if (event.target instanceof HTMLInputElement) {
          _this.onInputChange(event.target.value);
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onKeyDown", function (event) {
      if (event.target instanceof HTMLInputElement) {
        var _this$state = _this.state,
            isSuggestionsVisible = _this$state.isSuggestionsVisible,
            groupIndex = _this$state.groupIndex,
            itemIndex = _this$state.itemIndex;
        var preventDefault = event.preventDefault.bind(event);
        var target = event.target,
            key = event.key,
            metaKey = event.metaKey;
        var value = target.value,
            selectionStart = target.selectionStart,
            selectionEnd = target.selectionEnd;

        var updateQuery = function updateQuery(query, newSelectionStart, newSelectionEnd) {
          _this.setState({
            query: query
          }, function () {
            target.setSelectionRange(newSelectionStart, newSelectionEnd);
          });
        };

        switch (event.keyCode) {
          case KEY_CODES.DOWN:
            event.preventDefault();

            if (isSuggestionsVisible && groupIndex !== null && itemIndex !== null) {
              _this.incrementIndex(groupIndex, itemIndex);
            } else {
              _this.setState({
                isSuggestionsVisible: true,
                groupIndex: 0,
                itemIndex: 0
              });
            }

            break;

          case KEY_CODES.UP:
            event.preventDefault();

            if (isSuggestionsVisible && groupIndex !== null && itemIndex !== null) {
              _this.decrementIndex(groupIndex, itemIndex);
            } else {
              var lastGroupIndex = _this.state.suggestionGroups.length - 1;
              var group = _this.state.suggestionGroups[lastGroupIndex];

              if (group !== null) {
                var lastItemIndex = group.suggestions.length - 1;

                _this.setState({
                  isSuggestionsVisible: true,
                  groupIndex: lastGroupIndex,
                  itemIndex: lastItemIndex
                });
              }
            }

            break;

          case KEY_CODES.ENTER:
            event.preventDefault();

            if (isSuggestionsVisible && groupIndex !== null && itemIndex !== null && _this.state.suggestionGroups[groupIndex]) {
              var _group = _this.state.suggestionGroups[groupIndex];

              _this.selectSuggestion(_group.suggestions[itemIndex]);
            } else {
              _this.onSubmit(function () {
                return event.preventDefault();
              });
            }

            break;

          case KEY_CODES.ESC:
            event.preventDefault();

            _this.setState({
              isSuggestionsVisible: false,
              groupIndex: null,
              itemIndex: null
            });

            break;

          case KEY_CODES.TAB:
            _this.setState({
              isSuggestionsVisible: false,
              groupIndex: null,
              itemIndex: null
            });

            break;

          default:
            if (selectionStart !== null && selectionEnd !== null) {
              (0, _match_pairs.matchPairs)({
                value: value,
                selectionStart: selectionStart,
                selectionEnd: selectionEnd,
                key: key,
                metaKey: metaKey,
                updateQuery: updateQuery,
                preventDefault: preventDefault
              });
            }

            break;
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onSubmit", function (preventDefault) {
      if (preventDefault) {
        preventDefault();
      }

      _this.props.onSubmit(_this.state.query);

      _this.setState({
        isSuggestionsVisible: false
      });
    });

    return _this;
  }

  _createClass(CodeQueryBar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.updateSuggestions();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps.query !== this.props.query) {
        this.updateSuggestions();
      } // When search options (e.g. repository scopes) change,
      // submit the search query again to refresh the search result.


      if (this.props.enableSubmitWhenOptionsChanged && !_.isEqual(prevProps.searchOptions, this.props.searchOptions)) {
        this.onSubmit();
      }
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.updateSuggestions.cancel();
      this.componentIsUnmounting = true;
    }
  }, {
    key: "focusInput",
    value: function focusInput() {
      if (this.inputRef) {
        this.inputRef.focus();
      }
    }
  }, {
    key: "toggleOptionsFlyout",
    value: function toggleOptionsFlyout() {
      if (this.optionFlyout) {
        this.optionFlyout.toggleOptionsFlyout();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var inputRef = function inputRef(node) {
        if (node) {
          _this2.inputRef = node;
        }
      };

      var activeDescendant = this.state.isSuggestionsVisible ? "suggestion-".concat(this.state.groupIndex, "-").concat(this.state.itemIndex) : '';
      return _react.default.createElement(_eui.EuiFlexGroup, {
        responsive: false,
        gutterSize: "none"
      }, _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_scope_selector.ScopeSelector, {
        scope: this.props.searchScope,
        onScopeChanged: this.onScopeChange
      })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiOutsideClickDetector, {
        onOutsideClick: this.onOutsideClick
      }, _react.default.createElement("div", {
        style: {
          position: 'relative'
        },
        role: "combobox",
        "aria-haspopup": "true",
        "aria-expanded": this.state.isSuggestionsVisible,
        "aria-owns": "typeahead-items",
        "aria-controls": "typeahead-items"
      }, _react.default.createElement("form", {
        name: "queryBarForm"
      }, _react.default.createElement("div", {
        className: "kuiLocalSearch",
        role: "search"
      }, _react.default.createElement("div", {
        className: "kuiLocalSearchAssistedInput"
      }, _react.default.createElement(_eui.EuiFieldText, {
        className: "kuiLocalSearchAssistedInput__input codeSearchBar__input",
        placeholder: _types.SearchScopePlaceholderText[this.props.searchScope],
        value: this.state.query,
        onKeyDown: this.onKeyDown,
        onKeyUp: this.onKeyUp,
        onChange: this.onChange,
        onClick: this.onClickInput,
        fullWidth: true,
        autoFocus: !this.props.disableAutoFocus,
        inputRef: inputRef,
        autoComplete: "off",
        spellCheck: false,
        "aria-label": "Search input",
        type: "text",
        "data-test-subj": "queryInput",
        "aria-autocomplete": "list",
        "aria-controls": "typeahead-items",
        "aria-activedescendant": activeDescendant,
        role: "textbox"
      }), _react.default.createElement(_options.SearchOptions, {
        defaultRepoOptions: this.props.defaultRepoOptions,
        defaultSearchScope: this.props.currentRepository || this.props.searchOptions.defaultRepoScope,
        repositorySearch: this.props.repositorySearch,
        saveSearchOptions: this.props.saveSearchOptions,
        repoSearchResults: this.props.repoSearchResults,
        searchLoading: this.props.searchLoading,
        searchOptions: this.props.searchOptions,
        ref: function ref(element) {
          return _this2.optionFlyout = element;
        }
      })))), _react.default.createElement(_suggestions_component.SuggestionsComponent, {
        query: this.state.query,
        show: this.state.isSuggestionsVisible,
        suggestionGroups: this.state.suggestionGroups,
        groupIndex: this.state.groupIndex,
        itemIndex: this.state.itemIndex,
        onClick: this.onClickSuggestion,
        onMouseEnter: this.onMouseEnterSuggestion,
        loadMore: this.loadMore
      })))));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      if ((0, _lodash.isEqual)(prevState.currentProps, nextProps)) {
        return null;
      }

      var nextState = {
        currentProps: nextProps
      };

      if (nextProps.query !== prevState.query) {
        nextState.query = nextProps.query;
      }

      return nextState;
    }
    /*
     Keep the "draft" value in local state until the user actually submits the query. There are a couple advantages:
       1. Each app doesn't have to maintain its own "draft" value if it wants to put off updating the query in app state
      until the user manually submits their changes. Most apps have watches on the query value in app state so we don't
      want to trigger those on every keypress. Also, some apps (e.g. dashboard) already juggle multiple query values,
      each with slightly different semantics and I'd rather not add yet another variable to the mix.
       2. Changes to the local component state won't trigger an Angular digest cycle. Triggering digest cycles on every
      keypress has been a major source of performance issues for us in previous implementations of the query bar.
      See https://github.com/elastic/kibana/issues/14086
    */

  }]);

  return CodeQueryBar;
}(_react.Component);

exports.CodeQueryBar = CodeQueryBar;

var mapStateToProps = function mapStateToProps(state) {
  return {
    repoSearchResults: state.search.scopeSearchResults.repositories,
    searchLoading: state.search.isScopeSearchLoading,
    searchScope: state.search.scope,
    searchOptions: state.search.searchOptions,
    defaultRepoOptions: state.repositoryManagement.repositories.slice(0, 5),
    currentRepository: state.repository.repository
  };
};

var mapDispatchToProps = {
  repositorySearch: _actions.searchReposForScope,
  saveSearchOptions: _actions.saveSearchOptions,
  onSuggestionQuerySubmitted: _actions.suggestionSearch
};
var QueryBar = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps, null, {
  withRef: true
})(CodeQueryBar);
exports.QueryBar = QueryBar;