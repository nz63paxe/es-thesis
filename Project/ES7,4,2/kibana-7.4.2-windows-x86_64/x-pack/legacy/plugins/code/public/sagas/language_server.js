"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLoadLanguageServers = watchLoadLanguageServers;
exports.watchInstallLanguageServer = watchInstallLanguageServer;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _language_server = require("../actions/language_server");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleInstallLanguageServer),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(handleLoadLanguageServer),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadLanguageServers),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchInstallLanguageServer);

function fetchLangServers() {
  return _new_platform.npStart.core.http.get('/api/code/install');
}

function installLanguageServer(languageServer) {
  return _new_platform.npStart.core.http.post("/api/code/install/".concat(languageServer));
}

function handleInstallLanguageServer(action) {
  return regeneratorRuntime.wrap(function handleInstallLanguageServer$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.call)(installLanguageServer, action.payload);

        case 3:
          _context.next = 5;
          return (0, _effects.put)((0, _language_server.requestInstallLanguageServerSuccess)(action.payload));

        case 5:
          _context.next = 11;
          break;

        case 7:
          _context.prev = 7;
          _context.t0 = _context["catch"](0);
          _context.next = 11;
          return (0, _effects.put)((0, _language_server.requestInstallLanguageServerFailed)(_context.t0));

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 7]]);
}

function handleLoadLanguageServer() {
  var langServers;
  return regeneratorRuntime.wrap(function handleLoadLanguageServer$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(fetchLangServers);

        case 3:
          langServers = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)((0, _language_server.loadLanguageServersSuccess)(langServers));

        case 6:
          _context2.next = 12;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 12;
          return (0, _effects.put)((0, _language_server.loadLanguageServersFailed)(_context2.t0));

        case 12:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 8]]);
}

function watchLoadLanguageServers() {
  return regeneratorRuntime.wrap(function watchLoadLanguageServers$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return (0, _effects.takeEvery)(String(_language_server.loadLanguageServers), handleLoadLanguageServer);

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function watchInstallLanguageServer() {
  return regeneratorRuntime.wrap(function watchInstallLanguageServer$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(String(_language_server.requestInstallLanguageServer), handleInstallLanguageServer);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}