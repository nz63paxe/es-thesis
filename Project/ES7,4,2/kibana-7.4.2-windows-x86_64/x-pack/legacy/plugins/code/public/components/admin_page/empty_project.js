"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmptyProject = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _capabilities = require("ui/capabilities");

var _import_project = require("./import_project");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var EmptyProject = function EmptyProject() {
  var isAdmin = _capabilities.capabilities.get().code.admin;

  return _react.default.createElement("div", {
    className: "codeTab__projects"
  }, _react.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react.default.createElement("div", {
    className: "codeTab__projects--emptyHeader"
  }, _react.default.createElement(_eui.EuiText, null, _react.default.createElement("h1", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.code.adminPage.repoTab.emptyRepo.noRepositoryText",
    defaultMessage: "You don't have any repos yet"
  }))), _react.default.createElement(_eui.EuiText, {
    color: "subdued"
  }, isAdmin && _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.code.adminPage.repoTab.emptyRepo.importFirstRepositoryText",
    defaultMessage: "Let's import your first one"
  })))), isAdmin && _react.default.createElement(_import_project.ImportProject, null), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "center"
  }, _react.default.createElement(_reactRouterDom.Link, {
    to: "/setup-guide"
  }, _react.default.createElement(_eui.EuiButton, null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.code.adminPage.repoTab.emptyRepo.viewSetupGuideButtonLabel",
    defaultMessage: "View the Setup Guide"
  })))));
};

exports.EmptyProject = EmptyProject;