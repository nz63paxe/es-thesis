"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.definitionProvider = void 0;

var _url = _interopRequireDefault(require("url"));

var _querystring = _interopRequireDefault(require("querystring"));

var _new_platform = require("ui/new_platform");

var _monaco = require("../monaco");

var _lsp_client = require("../../../common/lsp_client");

var _uri_util = require("../../../common/uri_util");

var _url2 = require("../../utils/url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var definitionProvider = {
  provideDefinition: function () {
    var _provideDefinition = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(model, position, token) {
      var lspClient, lspMethods, handleLocation, handleQname, _handleQname, openDefinitionsPanel, result, l, location;

      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              openDefinitionsPanel = function _ref4() {
                if (model && position) {
                  var _parseSchema = (0, _uri_util.parseSchema)(model.uri.toString()),
                      uri = _parseSchema.uri;

                  var refUrl = "git:/".concat(uri, "!L").concat(position.lineNumber - 1, ":").concat(position.column - 1);

                  var queries = _url.default.parse(_url2.history.location.search, true).query;

                  var query = _querystring.default.stringify(_objectSpread({}, queries, {
                    tab: 'definitions',
                    refUrl: refUrl
                  }));

                  _url2.history.push("".concat(uri, "?").concat(query));
                }
              };

              _handleQname = function _ref3() {
                _handleQname = _asyncToGenerator(
                /*#__PURE__*/
                regeneratorRuntime.mark(function _callee(qname) {
                  var res;
                  return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                      switch (_context.prev = _context.next) {
                        case 0:
                          _context.next = 2;
                          return _new_platform.npStart.core.http.get("/api/code/lsp/symbol/".concat(qname));

                        case 2:
                          res = _context.sent;

                          if (!res.symbols) {
                            _context.next = 5;
                            break;
                          }

                          return _context.abrupt("return", res.symbols.map(function (s) {
                            return handleLocation(s.symbolInformation.location);
                          }));

                        case 5:
                          return _context.abrupt("return", []);

                        case 6:
                        case "end":
                          return _context.stop();
                      }
                    }
                  }, _callee);
                }));
                return _handleQname.apply(this, arguments);
              };

              handleQname = function _ref2(_x4) {
                return _handleQname.apply(this, arguments);
              };

              handleLocation = function _ref(location) {
                return {
                  uri: _monaco.monaco.Uri.parse(location.uri),
                  range: {
                    startLineNumber: location.range.start.line + 1,
                    startColumn: location.range.start.character + 1,
                    endLineNumber: location.range.end.line + 1,
                    endColumn: location.range.end.character + 1
                  }
                };
              };

              lspClient = new _lsp_client.LspRestClient('/api/code/lsp');
              lspMethods = new _lsp_client.TextDocumentMethods(lspClient);
              _context2.next = 8;
              return lspMethods.edefinition.send({
                position: {
                  line: position.lineNumber - 1,
                  character: position.column - 1
                },
                textDocument: {
                  uri: model.uri.toString()
                }
              });

            case 8:
              result = _context2.sent;

              if (!result) {
                _context2.next = 25;
                break;
              }

              if (!(result.length > 1)) {
                _context2.next = 15;
                break;
              }

              openDefinitionsPanel();
              return _context2.abrupt("return", result.filter(function (l) {
                return l.location !== undefined;
              }).map(function (l) {
                return handleLocation(l.location);
              }));

            case 15:
              l = result[0];
              location = l.location;

              if (!location) {
                _context2.next = 21;
                break;
              }

              return _context2.abrupt("return", [handleLocation(location)]);

            case 21:
              if (!l.qname) {
                _context2.next = 25;
                break;
              }

              _context2.next = 24;
              return handleQname(l.qname);

            case 24:
              return _context2.abrupt("return", _context2.sent);

            case 25:
              return _context2.abrupt("return", []);

            case 26:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    function provideDefinition(_x, _x2, _x3) {
      return _provideDefinition.apply(this, arguments);
    }

    return provideDefinition;
  }()
};
exports.definitionProvider = definitionProvider;