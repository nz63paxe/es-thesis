"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadCommitFailed = exports.loadCommitSuccess = exports.loadCommit = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadCommit = (0, _reduxActions.createAction)('LOAD COMMIT');
exports.loadCommit = loadCommit;
var loadCommitSuccess = (0, _reduxActions.createAction)('LOAD COMMIT SUCCESS');
exports.loadCommitSuccess = loadCommitSuccess;
var loadCommitFailed = (0, _reduxActions.createAction)('LOAD COMMIT FAILED');
exports.loadCommitFailed = loadCommitFailed;