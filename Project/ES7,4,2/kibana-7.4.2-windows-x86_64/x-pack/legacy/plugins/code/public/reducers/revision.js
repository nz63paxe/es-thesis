"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.revision = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _commit = require("../../model/commit");

var _actions = require("../actions");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  branches: [],
  tags: [],
  treeCommits: {},
  loadingCommits: false,
  commitsFullyLoaded: {}
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.branches = initialState.branches;
    draft.tags = initialState.tags;
    draft.treeCommits = initialState.treeCommits;
    draft.loadingCommits = initialState.loadingCommits;
    draft.commitsFullyLoaded = initialState.commitsFullyLoaded;
  });
};

var revision = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.fetchRepoCommitsSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.treeCommits[''] = action.payload;
    draft.loadingCommits = false;
  });
}), _defineProperty(_handleActions, String(_actions.fetchMoreCommits), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loadingCommits = true;
  });
}), _defineProperty(_handleActions, String(_actions.fetchRepoBranchesSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var references = action.payload;
    draft.tags = references.filter(function (r) {
      return r.type === _commit.ReferenceType.TAG;
    });
    draft.branches = references.filter(function (r) {
      return r.type !== _commit.ReferenceType.TAG;
    });
  });
}), _defineProperty(_handleActions, String(_actions.fetchTreeCommits), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loadingCommits = true;
  });
}), _defineProperty(_handleActions, String(_actions.fetchTreeCommitsFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loadingCommits = false;
  });
}), _defineProperty(_handleActions, String(_actions.fetchTreeCommitsSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var _ref = action.payload,
        path = _ref.path,
        commits = _ref.commits,
        append = _ref.append;

    if (commits.length === 0) {
      draft.commitsFullyLoaded[path] = true;
    } else if (append) {
      draft.treeCommits[path] = draft.treeCommits[path].concat(commits);
    } else {
      draft.treeCommits[path] = commits;
    }

    draft.loadingCommits = false;
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _handleActions), initialState);
exports.revision = revision;