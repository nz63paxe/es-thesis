"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SideTabs = void 0;

var _eui = require("@elastic/eui");

var _querystring = require("querystring");

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _query_string = require("ui/utils/query_string");

var _i18n = require("@kbn/i18n");

var _file_tree = require("../file_tree/file_tree");

var _shortcuts = require("../shortcuts");

var _symbol_tree = require("../symbol_tree/symbol_tree");

var _model = require("../../../model");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Tabs;

(function (Tabs) {
  Tabs["file"] = "file";
  Tabs["structure"] = "structure";
})(Tabs || (Tabs = {}));

var CodeSideTabs =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeSideTabs, _React$PureComponent);

  function CodeSideTabs() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeSideTabs);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeSideTabs)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "switchTab", function (tab) {
      var history = _this.props.history;
      var _history$location = history.location,
          pathname = _history$location.pathname,
          search = _history$location.search; // @ts-ignore

      history.push(_query_string.QueryString.replaceParamInUrl("".concat(pathname).concat(search), 'sideTab', tab));
    });

    _defineProperty(_assertThisInitialized(_this), "toggleTab", function () {
      var currentTab = _this.sideTab;

      if (currentTab === Tabs.file) {
        _this.switchTab(Tabs.structure);
      } else {
        _this.switchTab(Tabs.file);
      }
    });

    return _this;
  }

  _createClass(CodeSideTabs, [{
    key: "renderLoadingSpinner",
    value: function renderLoadingSpinner(text) {
      return _react.default.createElement("div", null, _react.default.createElement(_eui.EuiSpacer, {
        size: "xl"
      }), _react.default.createElement(_eui.EuiSpacer, {
        size: "xl"
      }), _react.default.createElement(_eui.EuiText, {
        textAlign: "center"
      }, text), _react.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }), _react.default.createElement(_eui.EuiText, {
        textAlign: "center"
      }, _react.default.createElement(_eui.EuiLoadingSpinner, {
        size: "xl"
      })));
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var tabs = this.tabs;
      var selectedTab = tabs.find(function (t) {
        return t.id === _this2.sideTab;
      });
      return _react.default.createElement("div", null, _react.default.createElement(_eui.EuiTabbedContent, {
        className: "code-navigation__sidebar",
        tabs: tabs,
        onTabClick: function onTabClick(tab) {
          return _this2.switchTab(tab.id);
        },
        expand: true,
        selectedTab: selectedTab
      }), _react.default.createElement(_shortcuts.Shortcut, {
        keyCode: "t",
        help: "Toggle tree and symbol view in sidebar",
        onPress: this.toggleTab
      }));
    }
  }, {
    key: "sideTab",
    get: function get() {
      var search = this.props.location.search;
      var qs = search;

      if (search.charAt(0) === '?') {
        qs = search.substr(1);
      }

      var tab = (0, _querystring.parse)(qs).sideTab;
      return tab === Tabs.structure ? Tabs.structure : Tabs.file;
    }
  }, {
    key: "tabs",
    get: function get() {
      var _this$props = this.props,
          languageServerInitializing = _this$props.languageServerInitializing,
          loadingFileTree = _this$props.loadingFileTree,
          loadingStructureTree = _this$props.loadingStructureTree;
      var fileTabContent = loadingFileTree ? this.renderLoadingSpinner(_i18n.i18n.translate('xpack.code.mainPage.sideTab.loadingFileTreeText', {
        defaultMessage: 'Loading file tree'
      })) : _react.default.createElement("div", {
        className: "codeFileTree__container"
      }, _react.default.createElement(_file_tree.FileTree, null));
      var structureTabContent;

      if (languageServerInitializing) {
        structureTabContent = this.renderLoadingSpinner(_i18n.i18n.translate('xpack.code.mainPage.sideTab.languageServerInitializingText', {
          defaultMessage: 'Language server is initializing'
        }));
      } else if (loadingStructureTree) {
        structureTabContent = this.renderLoadingSpinner(_i18n.i18n.translate('xpack.code.mainPage.sideTab.loadingStructureTreeText', {
          defaultMessage: 'Loading structure tree'
        }));
      } else {
        var _this$props$match$par = this.props.match.params,
            resource = _this$props$match$par.resource,
            org = _this$props$match$par.org,
            repo = _this$props$match$par.repo,
            revision = _this$props$match$par.revision,
            path = _this$props$match$par.path;
        var uri = "git://".concat(resource, "/").concat(org, "/").concat(repo, "/blob/").concat(revision, "/").concat(path);
        structureTabContent = _react.default.createElement(_symbol_tree.SymbolTree, {
          uri: uri
        });
      }

      return [{
        id: Tabs.file,
        name: _i18n.i18n.translate('xpack.code.mainPage.sideTab.fileTabLabel', {
          defaultMessage: 'Files'
        }),
        content: fileTabContent,
        'data-test-subj': "codeFileTreeTab".concat(this.sideTab === Tabs.file ? 'Active' : '')
      }, {
        id: Tabs.structure,
        name: _i18n.i18n.translate('xpack.code.mainPage.sideTab.structureTabLabel', {
          defaultMessage: 'Structure'
        }),
        content: structureTabContent,
        disabled: !(this.props.currentTree && this.props.currentTree.type === _model.FileTreeItemType.File) || !this.props.hasStructure,
        'data-test-subj': 'codeStructureTreeTab'
      }];
    }
  }]);

  return CodeSideTabs;
}(_react.default.PureComponent);

var SideTabs = (0, _reactRouterDom.withRouter)(CodeSideTabs);
exports.SideTabs = SideTabs;