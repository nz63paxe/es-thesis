"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Directory = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _model = require("../../../model");

var _types = require("../../common/types");

var _uri_util = require("../../../common/uri_util");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DirectoryNodes = function DirectoryNodes(props) {
  var _typeIconMap;

  var typeIconMap = (_typeIconMap = {}, _defineProperty(_typeIconMap, _model.FileTreeItemType.File, 'document'), _defineProperty(_typeIconMap, _model.FileTreeItemType.Directory, 'folderClosed'), _defineProperty(_typeIconMap, _model.FileTreeItemType.Link, 'symlink'), _defineProperty(_typeIconMap, _model.FileTreeItemType.Submodule, 'submodule'), _typeIconMap);
  var nodes = props.nodes.map(function (n) {
    return _react.default.createElement(_eui.EuiFlexItem, {
      key: n.path,
      grow: false
    }, _react.default.createElement(_reactRouterDom.Link, {
      className: "code-link",
      to: props.getUrl(n.path),
      "data-test-subj": "codeFileExplorerNode-".concat(n.name)
    }, _react.default.createElement("div", {
      className: "code-directory__node"
    }, _react.default.createElement(_eui.EuiIcon, {
      type: typeIconMap[n.type],
      color: "subdued"
    }), _react.default.createElement(_eui.EuiText, {
      size: "xs",
      className: "code-fileNodeName eui-textTruncate"
    }, n.name))));
  });
  return _react.default.createElement("div", {
    className: "codeContainer__directoryList"
  }, _react.default.createElement("div", null, _react.default.createElement("div", null, _react.default.createElement(_eui.EuiTitle, {
    size: "s"
  }, _react.default.createElement("h3", null, props.title))), _react.default.createElement(_eui.EuiFlexGroup, {
    wrap: true,
    direction: "row",
    gutterSize: "none",
    justifyContent: "flexStart"
  }, nodes)));
};

var Directory = (0, _reactRouterDom.withRouter)(function (props) {
  var files = [];
  var folders = [];

  if (props.node && props.node.children) {
    files = props.node.children.filter(function (n) {
      return n.type === _model.FileTreeItemType.File || n.type === _model.FileTreeItemType.Link;
    });
    folders = props.node.children.filter(function (n) {
      return n.type === _model.FileTreeItemType.Directory || n.type === _model.FileTreeItemType.Submodule;
    });
  }

  var _props$match$params = props.match.params,
      resource = _props$match$params.resource,
      org = _props$match$params.org,
      repo = _props$match$params.repo,
      revision = _props$match$params.revision;

  var getUrl = function getUrl(pathType) {
    return function (path) {
      return "/".concat(resource, "/").concat(org, "/").concat(repo, "/").concat(pathType, "/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(path);
    };
  };

  var fileList = _react.default.createElement(DirectoryNodes, {
    nodes: files,
    title: _i18n.i18n.translate('xpack.code.mainPage.content.directory.filesTitle', {
      defaultMessage: 'Files'
    }),
    getUrl: getUrl(_types.PathTypes.blob)
  });

  var folderList = _react.default.createElement(DirectoryNodes, {
    nodes: folders,
    title: _i18n.i18n.translate('xpack.code.mainPage.content.directory.directoriesTitle', {
      defaultMessage: 'Directories'
    }),
    getUrl: getUrl(_types.PathTypes.tree)
  });

  var children = props.loading ? _react.default.createElement("div", null, _react.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, "Loading..."), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: "xl"
  }))) : _react.default.createElement(_react.default.Fragment, null, files.length > 0 && fileList, folders.length > 0 && folderList);
  return _react.default.createElement("div", null, children);
});
exports.Directory = Directory;