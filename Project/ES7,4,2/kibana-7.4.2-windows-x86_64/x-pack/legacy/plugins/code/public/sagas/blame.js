"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLoadBlame = watchLoadBlame;
exports.watchBlame = watchBlame;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _blame = require("../actions/blame");

var _patterns = require("./patterns");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchBlame),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadBlame),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleBlame),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchBlame);

function requestBlame(repoUri, revision, path) {
  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(repoUri, "/blame/").concat(encodeURIComponent(revision), "/").concat(path));
}

function handleFetchBlame(action) {
  var _ref, repoUri, revision, path, blame;

  return regeneratorRuntime.wrap(function handleFetchBlame$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _ref = action.payload, repoUri = _ref.repoUri, revision = _ref.revision, path = _ref.path;
          _context.next = 4;
          return (0, _effects.call)(requestBlame, repoUri, revision, path);

        case 4:
          blame = _context.sent;
          _context.next = 7;
          return (0, _effects.put)((0, _blame.loadBlameSuccess)(blame));

        case 7:
          _context.next = 13;
          break;

        case 9:
          _context.prev = 9;
          _context.t0 = _context["catch"](0);
          _context.next = 13;
          return (0, _effects.put)((0, _blame.loadBlameFailed)(_context.t0));

        case 13:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 9]]);
}

function watchLoadBlame() {
  return regeneratorRuntime.wrap(function watchLoadBlame$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(String(_blame.loadBlame), handleFetchBlame);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function handleBlame(action) {
  var _params, resource, org, repo, revision, path, repoUri;

  return regeneratorRuntime.wrap(function handleBlame$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _params = action.payload.params, resource = _params.resource, org = _params.org, repo = _params.repo, revision = _params.revision, path = _params.path;
          repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);
          _context3.next = 4;
          return (0, _effects.put)((0, _blame.loadBlame)({
            repoUri: repoUri,
            revision: revision,
            path: path
          }));

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function watchBlame() {
  return regeneratorRuntime.wrap(function watchBlame$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(_patterns.blamePattern, handleBlame);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}