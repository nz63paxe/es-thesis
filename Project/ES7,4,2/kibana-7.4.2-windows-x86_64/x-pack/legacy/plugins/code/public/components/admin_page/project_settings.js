"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProjectSettings = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _repository_utils = require("../../../common/repository_utils");

var _actions = require("../../actions");

var _icons = require("../shared/icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var defaultConfig = {
  disableGo: true,
  disableJava: true,
  disableTypescript: true
};

var ProjectSettingsModal =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(ProjectSettingsModal, _React$PureComponent);

  function ProjectSettingsModal() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ProjectSettingsModal);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ProjectSettingsModal)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      config: _this.props.config
    });

    _defineProperty(_assertThisInitialized(_this), "onSwitchChange", function (ls) {
      return function (e) {
        var checked = e.target.checked;

        _this.setState(function (prevState) {
          return {
            config: _objectSpread({}, prevState.config, _defineProperty({}, "disable".concat(ls), !checked))
          };
        });
      };
    });

    _defineProperty(_assertThisInitialized(_this), "saveChanges", function () {
      _this.props.switchLanguageServer({
        repoUri: _this.props.repoUri,
        config: _this.state.config
      });
    });

    return _this;
  }

  _createClass(ProjectSettingsModal, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          repoUri = _this$props.repoUri,
          languageServers = _this$props.languageServers,
          onClose = _this$props.onClose;
      var _this$state$config = this.state.config,
          disableJava = _this$state$config.disableJava,
          disableTypescript = _this$state$config.disableTypescript;

      var org = _repository_utils.RepositoryUtils.orgNameFromUri(repoUri);

      var repoName = _repository_utils.RepositoryUtils.repoNameFromUri(repoUri);

      var languageServerSwitches = languageServers.map(function (ls) {
        var checked = ls.name === 'Java' ? !disableJava : !disableTypescript;
        return _react.default.createElement("div", {
          key: ls.name
        }, _react.default.createElement(_eui.EuiSwitch, {
          name: ls.name,
          label: _react.default.createElement("span", null, ls.name === 'Java' ? _react.default.createElement("div", {
            className: "codeSettingsPanel__icon"
          }, _react.default.createElement(_icons.JavaIcon, null)) : _react.default.createElement("div", {
            className: "codeSettingsPanel__icon"
          }, _react.default.createElement(_icons.TypeScriptIcon, null)), ls.name),
          checked: checked,
          onChange: _this2.onSwitchChange(ls.name)
        }));
      });
      return _react.default.createElement(_eui.EuiOverlayMask, null, _react.default.createElement(_eui.EuiModal, {
        onClose: onClose
      }, _react.default.createElement(_eui.EuiModalHeader, null, _react.default.createElement(_eui.EuiModalHeaderTitle, null, _react.default.createElement("h3", null, "Project Settings"), _react.default.createElement(_eui.EuiText, null, org, "/", repoName))), _react.default.createElement(_eui.EuiModalBody, null, _react.default.createElement(_eui.EuiTitle, {
        size: "xxs"
      }, _react.default.createElement("h5", null, "Language Servers")), languageServerSwitches), _react.default.createElement(_eui.EuiModalFooter, null, _react.default.createElement(_eui.EuiButtonEmpty, null, _react.default.createElement(_reactRouterDom.Link, {
        to: "/admin?tab=LanguageServers"
      }, "Manage Language Servers")), _react.default.createElement(_eui.EuiButton, {
        onClick: this.saveChanges
      }, "Save Changes"))));
    }
  }]);

  return ProjectSettingsModal;
}(_react.default.PureComponent);

var mapStateToProps = function mapStateToProps(state, ownProps) {
  return {
    languageServers: state.languageServer.languageServers,
    config: state.repositoryManagement.repoLangseverConfigs[ownProps.repoUri] || defaultConfig
  };
};

var mapDispatchToProps = {
  switchLanguageServer: _actions.switchLanguageServer
};
var ProjectSettings = (0, _reactRedux.connect)( // @ts-ignore
mapStateToProps, mapDispatchToProps)(ProjectSettingsModal);
exports.ProjectSettings = ProjectSettings;