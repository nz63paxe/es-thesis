"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reposSelector = exports.statusSelector = exports.previousMatchSelector = exports.urlQueryStringSelector = exports.repoScopeSelector = exports.currentRepoSelector = exports.createTreeSelector = exports.currentTreeSelector = exports.hasMoreCommitsSelector = exports.treeCommitsSelector = exports.currentPathSelector = exports.allStatusSelector = exports.repoStatusSelector = exports.routeSelector = exports.revisionSelector = exports.repoUriSelector = exports.searchScopeSelector = exports.fileSelector = exports.refUrlSelector = exports.structureSelector = exports.lastRequestPathSelector = exports.getTreeRevision = exports.getTree = void 0;

function _toArray(arr) { return _arrayWithHoles(arr) || _iterableToArray(arr) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getTree = function getTree(state) {
  return state.fileTree.tree;
};

exports.getTree = getTree;

var getTreeRevision = function getTreeRevision(state) {
  return state.fileTree.revision;
};

exports.getTreeRevision = getTreeRevision;

var lastRequestPathSelector = function lastRequestPathSelector(state) {
  return state.symbol.lastRequestPath || '';
};

exports.lastRequestPathSelector = lastRequestPathSelector;

var structureSelector = function structureSelector(state) {
  var pathname = lastRequestPathSelector(state);
  var symbols = state.symbol.structureTree[pathname];
  return symbols || [];
};

exports.structureSelector = structureSelector;

var refUrlSelector = function refUrlSelector(state) {
  var payload = state.editor.refPayload;

  if (payload) {
    var _payload$position = payload.position,
        line = _payload$position.line,
        character = _payload$position.character;
    return "".concat(payload.textDocument.uri, "!L").concat(line, ":").concat(character);
  }

  return undefined;
};

exports.refUrlSelector = refUrlSelector;

var fileSelector = function fileSelector(state) {
  return state.file.file;
};

exports.fileSelector = fileSelector;

var searchScopeSelector = function searchScopeSelector(state) {
  return state.search.scope;
};

exports.searchScopeSelector = searchScopeSelector;

var repoUriSelector = function repoUriSelector(state) {
  var _state$route$match$pa = state.route.match.params,
      resource = _state$route$match$pa.resource,
      org = _state$route$match$pa.org,
      repo = _state$route$match$pa.repo;
  return "".concat(resource, "/").concat(org, "/").concat(repo);
};

exports.repoUriSelector = repoUriSelector;

var revisionSelector = function revisionSelector(state) {
  return state.route.match.params.revision;
};

exports.revisionSelector = revisionSelector;

var routeSelector = function routeSelector(state) {
  return state.route.match;
};

exports.routeSelector = routeSelector;

var repoStatusSelector = function repoStatusSelector(state, repoUri) {
  return state.status.status[repoUri];
};

exports.repoStatusSelector = repoStatusSelector;

var allStatusSelector = function allStatusSelector(state) {
  return state.status.status;
};

exports.allStatusSelector = allStatusSelector;

var currentPathSelector = function currentPathSelector(state) {
  return state.route.match.params.path || '';
};

exports.currentPathSelector = currentPathSelector;

var treeCommitsSelector = function treeCommitsSelector(state) {
  var path = currentPathSelector(state);
  return state.revision.treeCommits[path] || [];
};

exports.treeCommitsSelector = treeCommitsSelector;

var hasMoreCommitsSelector = function hasMoreCommitsSelector(state) {
  var path = currentPathSelector(state);
  var isLoading = state.revision.loadingCommits;

  if (isLoading) {
    return false;
  }

  if (state.revision.commitsFullyLoaded[path]) {
    return false;
  }

  var commits = state.revision.treeCommits[path];

  if (!commits) {
    // To avoid infinite loops in component `InfiniteScroll`,
    // here we set hasMore to false before we receive the first batch.
    return false;
  }

  return true;
};

exports.hasMoreCommitsSelector = hasMoreCommitsSelector;

function find(tree, paths) {
  if (paths.length === 0) {
    return tree;
  }

  var _paths = _toArray(paths),
      p = _paths[0],
      rest = _paths.slice(1);

  if (tree.children) {
    var child = tree.children.find(function (c) {
      return c.name === p;
    });

    if (child) {
      return find(child, rest);
    }
  }

  return null;
}

var currentTreeSelector = function currentTreeSelector(state) {
  var tree = getTree(state);
  var path = currentPathSelector(state) || '';
  return find(tree, path.split('/'));
};

exports.currentTreeSelector = currentTreeSelector;

var createTreeSelector = function createTreeSelector(path) {
  return function (state) {
    var tree = getTree(state);
    return find(tree, path.split('/'));
  };
};

exports.createTreeSelector = createTreeSelector;

var currentRepoSelector = function currentRepoSelector(state) {
  return state.repository.repository;
};

exports.currentRepoSelector = currentRepoSelector;

var repoScopeSelector = function repoScopeSelector(state) {
  return state.search.searchOptions.repoScope;
};

exports.repoScopeSelector = repoScopeSelector;

var urlQueryStringSelector = function urlQueryStringSelector(state) {
  return state.route.match.location.search;
};

exports.urlQueryStringSelector = urlQueryStringSelector;

var previousMatchSelector = function previousMatchSelector(state) {
  return state.route.previousMatch;
};

exports.previousMatchSelector = previousMatchSelector;

var statusSelector = function statusSelector(state) {
  return state.status.repoFileStatus;
};

exports.statusSelector = statusSelector;

var reposSelector = function reposSelector(state) {
  return state.repositoryManagement.repositories;
};

exports.reposSelector = reposSelector;