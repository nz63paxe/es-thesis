"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.status = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _model = require("../../model");

var _actions = require("../actions");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  status: {},
  loading: false
};

var getIndexState = function getIndexState(indexStatus) {
  var progress = indexStatus.progress;

  if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT || progress < _model.WorkerReservedProgress.INIT) {
    return _actions.RepoState.INDEX_ERROR;
  } else if (progress < _model.WorkerReservedProgress.COMPLETED) {
    return _actions.RepoState.INDEXING;
  } else if (progress === _model.WorkerReservedProgress.COMPLETED) {
    return _actions.RepoState.READY;
  }

  return _actions.RepoState.UNKNOWN;
};

var getGitState = function getGitState(gitStatus) {
  var progress = gitStatus.progress;

  if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT || progress < _model.WorkerReservedProgress.INIT) {
    return _actions.RepoState.CLONE_ERROR;
  } else if (progress < _model.WorkerReservedProgress.COMPLETED) {
    return _actions.RepoState.CLONING;
  } else if (progress === _model.WorkerReservedProgress.COMPLETED) {
    return _actions.RepoState.READY;
  }

  return _actions.RepoState.UNKNOWN;
};

var getDeleteState = function getDeleteState(deleteStatus) {
  var progress = deleteStatus.progress;

  if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT || progress < _model.WorkerReservedProgress.INIT) {
    return _actions.RepoState.DELETE_ERROR;
  } else if (progress < _model.WorkerReservedProgress.COMPLETED) {
    return _actions.RepoState.DELETING;
  } else {
    return _actions.RepoState.UNKNOWN;
  }
};

var status = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.loadStatus), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_actions.loadStatusSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = false;
    Object.keys(action.payload).forEach(function (repoUri) {
      var statuses = action.payload[repoUri];

      if (statuses.deleteStatus) {
        // 1. Look into delete status first
        var deleteState = getDeleteState(statuses.deleteStatus);
        draft.status[repoUri] = _objectSpread({}, statuses.deleteStatus, {
          state: deleteState
        });
        return;
      } else {
        // 2. Then take the index state and git clone state into
        // account in the meantime.
        var indexState = _actions.RepoState.UNKNOWN;

        if (statuses.indexStatus) {
          indexState = getIndexState(statuses.indexStatus);
        }

        var gitState = _actions.RepoState.UNKNOWN;

        if (statuses.gitStatus) {
          gitState = getGitState(statuses.gitStatus);
        }

        if (statuses.gitStatus) {
          // Git state has higher priority over index state
          if (gitState === _actions.RepoState.CLONING || gitState === _actions.RepoState.CLONE_ERROR) {
            draft.status[repoUri] = _objectSpread({}, statuses.gitStatus, {
              state: gitState
            });
            return;
          } else if (gitState === _actions.RepoState.READY && !statuses.indexStatus) {
            // If git state is ready and index status is not there, then return
            // the git status
            draft.status[repoUri] = _objectSpread({}, statuses.gitStatus, {
              state: gitState
            });
            return;
          }
        }

        if (statuses.indexStatus) {
          draft.status[repoUri] = _objectSpread({}, statuses.indexStatus, {
            state: indexState
          });
          return;
        } // If non of delete/git/index status exists, then do nothing here.

      }
    });
  });
}), _defineProperty(_handleActions, String(_actions.loadStatusFailed), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = false;
    draft.error = action.payload;
  });
}), _defineProperty(_handleActions, String(_actions.updateCloneProgress), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var progress = action.payload.progress;
    var s = _actions.RepoState.CLONING;

    if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT) {
      s = _actions.RepoState.CLONE_ERROR;
    } else if (progress === _model.WorkerReservedProgress.COMPLETED) {
      s = _actions.RepoState.READY;
    }

    draft.status[action.payload.uri] = _objectSpread({}, action.payload, {
      state: s
    });
  });
}), _defineProperty(_handleActions, String(_actions.updateIndexProgress), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var progress = action.payload.progress;
    var s = _actions.RepoState.INDEXING;

    if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT) {
      s = _actions.RepoState.INDEX_ERROR;
    } else if (progress === _model.WorkerReservedProgress.COMPLETED) {
      s = _actions.RepoState.READY;
    }

    draft.status[action.payload.uri] = _objectSpread({}, action.payload, {
      state: s
    });
  });
}), _defineProperty(_handleActions, String(_actions.updateDeleteProgress), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var progress = action.payload.progress;

    if (progress === _model.WorkerReservedProgress.COMPLETED) {
      delete draft.status[action.payload.uri];
    } else {
      var s = _actions.RepoState.DELETING;

      if (progress === _model.WorkerReservedProgress.ERROR || progress === _model.WorkerReservedProgress.TIMEOUT) {
        s = _actions.RepoState.DELETE_ERROR;
      }

      draft.status[action.payload.uri] = _objectSpread({}, action.payload, {
        state: s
      });
    }
  });
}), _defineProperty(_handleActions, String(_actions.deleteRepoFinished), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    delete draft.status[action.payload];
  });
}), _defineProperty(_handleActions, String(_actions.FetchRepoFileStatusSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var _action$payload = action.payload,
        path = _action$payload.path,
        statusReport = _action$payload.statusReport;
    draft.repoFileStatus = statusReport;
    draft.currentStatusPath = path;
  });
}), _defineProperty(_handleActions, String(_actions.FetchRepoFileStatus), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.currentStatusPath = action.payload;
  });
}), _handleActions), initialState);
exports.status = status;