"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.repository = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  repoNotFound: false
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.repository = undefined;
    draft.repoNotFound = initialState.repoNotFound;
  });
};

var repository = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.loadRepo), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.repository = undefined;
    draft.repoNotFound = false;
  });
}), _defineProperty(_handleActions, String(_actions.loadRepoSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.repository = action.payload;
    draft.repoNotFound = false;
  });
}), _defineProperty(_handleActions, String(_actions.loadRepoFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.repository = undefined;
    draft.repoNotFound = true;
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _handleActions), initialState);
exports.repository = repository;