"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MonacoDiffEditor = void 0;

var _resize_checker = require("ui/resize_checker");

var _monaco = require("./monaco");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MonacoDiffEditor =
/*#__PURE__*/
function () {
  function MonacoDiffEditor(container, originCode, modifiedCode, language, renderSideBySide) {
    _classCallCheck(this, MonacoDiffEditor);

    this.container = container;
    this.originCode = originCode;
    this.modifiedCode = modifiedCode;
    this.language = language;
    this.renderSideBySide = renderSideBySide;

    _defineProperty(this, "diffEditor", null);

    _defineProperty(this, "resizeChecker", null);
  }

  _createClass(MonacoDiffEditor, [{
    key: "init",
    value: function init() {
      var _this = this;

      return new Promise(function (resolve) {
        var originalModel = _monaco.monaco.editor.createModel(_this.originCode, _this.language);

        var modifiedModel = _monaco.monaco.editor.createModel(_this.modifiedCode, _this.language);

        var diffEditor = _monaco.monaco.editor.createDiffEditor(_this.container, {
          enableSplitViewResizing: false,
          renderSideBySide: _this.renderSideBySide,
          scrollBeyondLastLine: false
        });

        _this.resizeChecker = new _resize_checker.ResizeChecker(_this.container);

        _this.resizeChecker.on('resize', function () {
          setTimeout(function () {
            _this.diffEditor.layout();
          });
        });

        diffEditor.setModel({
          original: originalModel,
          modified: modifiedModel
        });
        _this.diffEditor = diffEditor;
      });
    }
  }]);

  return MonacoDiffEditor;
}();

exports.MonacoDiffEditor = MonacoDiffEditor;