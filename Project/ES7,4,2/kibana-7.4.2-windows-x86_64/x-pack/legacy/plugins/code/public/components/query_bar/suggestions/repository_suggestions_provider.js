"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RepositorySuggestionsProvider = void 0;

var _new_platform = require("ui/new_platform");

var _ = require(".");

var _uri_util = require("../../../../common/uri_util");

var _model = require("../../../../model");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var RepositorySuggestionsProvider =
/*#__PURE__*/
function (_AbstractSuggestionsP) {
  _inherits(RepositorySuggestionsProvider, _AbstractSuggestionsP);

  function RepositorySuggestionsProvider() {
    _classCallCheck(this, RepositorySuggestionsProvider);

    return _possibleConstructorReturn(this, _getPrototypeOf(RepositorySuggestionsProvider).apply(this, arguments));
  }

  _createClass(RepositorySuggestionsProvider, [{
    key: "matchSearchScope",
    value: function matchSearchScope(scope) {
      return scope === _model.SearchScope.DEFAULT || scope === _model.SearchScope.REPOSITORY;
    }
  }, {
    key: "fetchSuggestions",
    value: function () {
      var _fetchSuggestions = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(query, repoScope) {
        var queryParams, res, suggestions;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                queryParams = {
                  q: query
                };

                if (repoScope && repoScope.length > 0) {
                  queryParams.repoScope = repoScope.join(',');
                }

                _context.next = 5;
                return _new_platform.npStart.core.http.get("/api/code/suggestions/repo", {
                  query: queryParams
                });

              case 5:
                res = _context.sent;
                suggestions = Array.from(res.repositories).slice(0, this.MAX_SUGGESTIONS_PER_GROUP).map(function (repo) {
                  return {
                    description: repo.url,
                    end: 10,
                    start: 1,
                    text: (0, _uri_util.toRepoNameWithOrg)(repo.uri),
                    tokenType: '',
                    selectUrl: "/".concat(repo.uri)
                  };
                });
                return _context.abrupt("return", {
                  type: _.AutocompleteSuggestionType.REPOSITORY,
                  total: res.total,
                  hasMore: res.total > this.MAX_SUGGESTIONS_PER_GROUP,
                  suggestions: suggestions
                });

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                return _context.abrupt("return", {
                  type: _.AutocompleteSuggestionType.REPOSITORY,
                  total: 0,
                  hasMore: false,
                  suggestions: []
                });

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 10]]);
      }));

      function fetchSuggestions(_x, _x2) {
        return _fetchSuggestions.apply(this, arguments);
      }

      return fetchSuggestions;
    }()
  }]);

  return RepositorySuggestionsProvider;
}(_.AbstractSuggestionsProvider);

exports.RepositorySuggestionsProvider = RepositorySuggestionsProvider;