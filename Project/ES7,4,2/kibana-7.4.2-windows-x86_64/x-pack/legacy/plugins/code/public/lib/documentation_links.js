"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.documentationLinks = void 0;

var _new_platform = require("ui/new_platform");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// TODO make sure document links are right
var documentationLinks = {
  code: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code.html"),
  codeIntelligence: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code.html"),
  gitFormat: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code.html"),
  codeInstallLangServer: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-install-lang-server.html"),
  codeGettingStarted: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-import-first-repo.html"),
  codeRepoManagement: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-repo-management.html"),
  codeSearch: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-search.html"),
  codeOtherFeatures: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-basic-nav.html"),
  semanticNavigation: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/code-semantic-nav.html"),
  kibanaRoleManagement: "".concat(_new_platform.npStart.core.docLinks.ELASTIC_WEBSITE_URL, "guide/en/kibana/").concat(_new_platform.npStart.core.docLinks.DOC_LINK_VERSION, "/kibana-role-management.html")
};
exports.documentationLinks = documentationLinks;