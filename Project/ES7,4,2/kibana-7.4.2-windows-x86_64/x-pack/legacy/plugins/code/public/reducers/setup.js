"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setup = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {};
var setup = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.checkSetupFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.ok = false;
  });
}), _defineProperty(_handleActions, String(_actions.checkSetupSuccess), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.ok = true;
  });
}), _handleActions), initialState);
exports.setup = setup;