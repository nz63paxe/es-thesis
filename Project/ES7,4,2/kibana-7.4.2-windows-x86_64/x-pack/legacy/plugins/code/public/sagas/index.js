"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rootSaga = rootSaga;

var _effects = require("redux-saga/effects");

var _blame = require("./blame");

var _commit = require("./commit");

var _editor = require("./editor");

var _file = require("./file");

var _language_server = require("./language_server");

var _project_status = require("./project_status");

var _repository = require("./repository");

var _search = require("./search");

var _setup = require("./setup");

var _status = require("./status");

var _structure = require("./structure");

var _route = require("./route");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(rootSaga);

function rootSaga() {
  return regeneratorRuntime.wrap(function rootSaga$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return (0, _effects.fork)(_route.watchRepoChange);

        case 2:
          _context.next = 4;
          return (0, _effects.fork)(_route.watchRepoOrRevisionChange);

        case 4:
          _context.next = 6;
          return (0, _effects.fork)(_route.watchRoute);

        case 6:
          _context.next = 8;
          return (0, _effects.fork)(_setup.watchRootRoute);

        case 8:
          _context.next = 10;
          return (0, _effects.fork)(_commit.watchLoadCommit);

        case 10:
          _context.next = 12;
          return (0, _effects.fork)(_repository.watchFetchRepos);

        case 12:
          _context.next = 14;
          return (0, _effects.fork)(_repository.watchDeleteRepo);

        case 14:
          _context.next = 16;
          return (0, _effects.fork)(_repository.watchIndexRepo);

        case 16:
          _context.next = 18;
          return (0, _effects.fork)(_repository.watchImportRepo);

        case 18:
          _context.next = 20;
          return (0, _effects.fork)(_file.watchFetchRepoTree);

        case 20:
          _context.next = 22;
          return (0, _effects.fork)(_file.watchFetchRootRepoTree);

        case 22:
          _context.next = 24;
          return (0, _effects.fork)(_file.watchFetchBranchesAndCommits);

        case 24:
          _context.next = 26;
          return (0, _effects.fork)(_search.watchDocumentSearch);

        case 26:
          _context.next = 28;
          return (0, _effects.fork)(_search.watchRepositorySearch);

        case 28:
          _context.next = 30;
          return (0, _effects.fork)(_structure.watchLoadStructure);

        case 30:
          _context.next = 32;
          return (0, _effects.fork)(_editor.watchLspMethods);

        case 32:
          _context.next = 34;
          return (0, _effects.fork)(_editor.watchCloseReference);

        case 34:
          _context.next = 36;
          return (0, _effects.fork)(_repository.watchFetchRepoConfigs);

        case 36:
          _context.next = 38;
          return (0, _effects.fork)(_repository.watchInitRepoCmd);

        case 38:
          _context.next = 40;
          return (0, _effects.fork)(_repository.watchGotoRepo);

        case 40:
          _context.next = 42;
          return (0, _effects.fork)(_editor.watchLoadRepo);

        case 42:
          _context.next = 44;
          return (0, _effects.fork)(_search.watchSearchRouteChange);

        case 44:
          _context.next = 46;
          return (0, _effects.fork)(_repository.watchAdminRouteChange);

        case 46:
          _context.next = 48;
          return (0, _effects.fork)(_editor.watchMainRouteChange);

        case 48:
          _context.next = 50;
          return (0, _effects.fork)(_editor.watchLoadRepo);

        case 50:
          _context.next = 52;
          return (0, _effects.fork)(_file.watchRepoRouteChange);

        case 52:
          _context.next = 54;
          return (0, _effects.fork)(_blame.watchLoadBlame);

        case 54:
          _context.next = 56;
          return (0, _effects.fork)(_blame.watchBlame);

        case 56:
          _context.next = 58;
          return (0, _effects.fork)(_status.watchRepoCloneSuccess);

        case 58:
          _context.next = 60;
          return (0, _effects.fork)(_status.watchRepoDeleteFinished);

        case 60:
          _context.next = 62;
          return (0, _effects.fork)(_language_server.watchLoadLanguageServers);

        case 62:
          _context.next = 64;
          return (0, _effects.fork)(_language_server.watchInstallLanguageServer);

        case 64:
          _context.next = 66;
          return (0, _effects.fork)(_project_status.watchLoadRepoListStatus);

        case 66:
          _context.next = 68;
          return (0, _effects.fork)(_project_status.watchLoadRepoStatus);

        case 68:
          _context.next = 70;
          return (0, _effects.fork)(_status.watchStatusChange);

        case 70:
          _context.next = 72;
          return (0, _effects.fork)(_project_status.watchPollingRepoStatus);

        case 72:
          _context.next = 74;
          return (0, _effects.fork)(_project_status.watchResetPollingStatus);

        case 74:
          _context.next = 76;
          return (0, _effects.fork)(_project_status.watchRepoDeleteStatusPolling);

        case 76:
          _context.next = 78;
          return (0, _effects.fork)(_project_status.watchRepoIndexStatusPolling);

        case 78:
          _context.next = 80;
          return (0, _effects.fork)(_project_status.watchRepoCloneStatusPolling);

        case 80:
          _context.next = 82;
          return (0, _effects.fork)(_search.watchRepoScopeSearch);

        case 82:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}