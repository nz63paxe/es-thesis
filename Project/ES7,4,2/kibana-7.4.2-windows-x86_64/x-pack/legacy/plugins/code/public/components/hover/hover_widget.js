"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HoverWidget = exports.HoverState = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _textToHtmlTokenizer = require("monaco-editor/esm/vs/editor/common/modes/textToHtmlTokenizer");

var _modes = require("monaco-editor/esm/vs/editor/common/modes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var HoverState;
exports.HoverState = HoverState;

(function (HoverState) {
  HoverState[HoverState["LOADING"] = 0] = "LOADING";
  HoverState[HoverState["INITIALIZING"] = 1] = "INITIALIZING";
  HoverState[HoverState["READY"] = 2] = "READY";
})(HoverState || (exports.HoverState = HoverState = {}));

var HoverWidget =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(HoverWidget, _React$PureComponent);

  function HoverWidget() {
    _classCallCheck(this, HoverWidget);

    return _possibleConstructorReturn(this, _getPrototypeOf(HoverWidget).apply(this, arguments));
  }

  _createClass(HoverWidget, [{
    key: "render",
    value: function render() {
      var contents;

      switch (this.props.state) {
        case HoverState.READY:
          contents = this.renderContents();
          break;

        case HoverState.INITIALIZING:
          contents = this.renderInitialting();
          break;

        case HoverState.LOADING:
        default:
          contents = this.renderLoading();
      }

      return _react2.default.createElement(_react2.default.Fragment, null, contents);
    }
  }, {
    key: "renderLoading",
    value: function renderLoading() {
      return _react2.default.createElement("div", {
        className: "hover-row"
      }, _react2.default.createElement("div", {
        className: "text-placeholder gradient"
      }), _react2.default.createElement("div", {
        className: "text-placeholder gradient",
        style: {
          width: '90%'
        }
      }), _react2.default.createElement("div", {
        className: "text-placeholder gradient",
        style: {
          width: '75%'
        }
      }));
    }
  }, {
    key: "renderContents",
    value: function renderContents() {
      var markdownRenderers = {
        link: function link(_ref) {
          var children = _ref.children,
              href = _ref.href;
          return _react2.default.createElement(_eui.EuiLink, {
            href: href,
            target: "_blank"
          }, children);
        },
        code: function code(_ref2) {
          var value = _ref2.value,
              language = _ref2.language;

          var support = _modes.TokenizationRegistry.get(language);

          var code = (0, _textToHtmlTokenizer.tokenizeToString)(value || '', support);
          return _react2.default.createElement("div", {
            className: "code",
            dangerouslySetInnerHTML: {
              __html: code
            }
          });
        }
      };
      return this.props.contents.filter(function (content) {
        return !!content;
      }).map(function (markedString, idx) {
        var markdown;

        if (typeof markedString === 'string') {
          markdown = markedString;
        } else if (markedString.language) {
          markdown = '```' + markedString.language + '\n' + markedString.value + '\n```';
        } else {
          markdown = markedString.value;
        }

        return _react2.default.createElement("div", {
          className: "hover-row",
          key: "hover_".concat(idx)
        }, _react2.default.createElement(_reactMarkdown.default, {
          source: markdown,
          escapeHtml: true,
          skipHtml: true,
          renderers: markdownRenderers
        }));
      });
    }
  }, {
    key: "renderInitialting",
    value: function renderInitialting() {
      return _react2.default.createElement("div", {
        className: "hover-row"
      }, _react2.default.createElement(_eui.EuiText, {
        textAlign: "center"
      }, _react2.default.createElement("h4", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.monaco.hover.languageServerInitializingTitle",
        defaultMessage: "Language Server is initializing\u2026"
      })), _react2.default.createElement(_eui.EuiText, {
        size: "xs",
        color: "subdued"
      }, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.monaco.hover.languageServerInitializingDescription",
        defaultMessage: "Depending on the size of your repo, this could take a few minutes."
      })))));
    }
  }]);

  return HoverWidget;
}(_react2.default.PureComponent);

exports.HoverWidget = HoverWidget;