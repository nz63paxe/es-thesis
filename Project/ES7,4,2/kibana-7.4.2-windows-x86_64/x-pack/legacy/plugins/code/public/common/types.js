"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchScopePlaceholderText = exports.SearchScopeText = exports.PathTypes = void 0;

var _i18n = require("@kbn/i18n");

var _model = require("../../model");

var _SearchScopeText, _SearchScopePlacehold;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var PathTypes;
exports.PathTypes = PathTypes;

(function (PathTypes) {
  PathTypes["blob"] = "blob";
  PathTypes["tree"] = "tree";
  PathTypes["blame"] = "blame";
  PathTypes["commits"] = "commits";
})(PathTypes || (exports.PathTypes = PathTypes = {}));

var SearchScopeText = (_SearchScopeText = {}, _defineProperty(_SearchScopeText, _model.SearchScope.DEFAULT, _i18n.i18n.translate('xpack.code.searchScope.defaultDropDownOptionLabel', {
  defaultMessage: 'Search Everything'
})), _defineProperty(_SearchScopeText, _model.SearchScope.REPOSITORY, _i18n.i18n.translate('xpack.code.searchScope.repositoryDropDownOptionLabel', {
  defaultMessage: 'Search Repositories'
})), _defineProperty(_SearchScopeText, _model.SearchScope.SYMBOL, _i18n.i18n.translate('xpack.code.searchScope.symbolDropDownOptionLabel', {
  defaultMessage: 'Search Symbols'
})), _defineProperty(_SearchScopeText, _model.SearchScope.FILE, _i18n.i18n.translate('xpack.code.searchScope.fileDropDownOptionLabel', {
  defaultMessage: 'Search Files'
})), _SearchScopeText);
exports.SearchScopeText = SearchScopeText;
var SearchScopePlaceholderText = (_SearchScopePlacehold = {}, _defineProperty(_SearchScopePlacehold, _model.SearchScope.DEFAULT, _i18n.i18n.translate('xpack.code.searchScope.defaultPlaceholder', {
  defaultMessage: 'Type to find anything'
})), _defineProperty(_SearchScopePlacehold, _model.SearchScope.REPOSITORY, _i18n.i18n.translate('xpack.code.searchScope.repositoryPlaceholder', {
  defaultMessage: 'Type to find repositories'
})), _defineProperty(_SearchScopePlacehold, _model.SearchScope.SYMBOL, _i18n.i18n.translate('xpack.code.searchScope.symbolPlaceholder', {
  defaultMessage: 'Type to find symbols'
})), _defineProperty(_SearchScopePlacehold, _model.SearchScope.FILE, _i18n.i18n.translate('xpack.code.searchScope.filePlaceholder', {
  defaultMessage: 'Type to find files'
})), _SearchScopePlacehold);
exports.SearchScopePlaceholderText = SearchScopePlaceholderText;