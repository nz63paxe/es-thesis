"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReferencesPanel = void 0;

var _eui = require("@elastic/eui");

var _classnames = _interopRequireDefault(require("classnames"));

var _querystring = _interopRequireDefault(require("querystring"));

var _react = _interopRequireDefault(require("react"));

var _uri_util = require("../../../common/uri_util");

var _url = require("../../utils/url");

var _codeblock = require("../codeblock/codeblock");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ReferencesPanel =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ReferencesPanel, _React$Component);

  function ReferencesPanel(props) {
    var _this;

    _classCallCheck(this, ReferencesPanel);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ReferencesPanel).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "close", function () {
      _this.props.onClose();
    });

    _defineProperty(_assertThisInitialized(_this), "toggleExpand", function () {
      _this.setState({
        expanded: !_this.state.expanded
      });
    });

    _this.state = {
      expanded: false
    };
    return _this;
  }

  _createClass(ReferencesPanel, [{
    key: "render",
    value: function render() {
      var body = this.props.isLoading ? _react.default.createElement(_eui.EuiLoadingKibana, {
        size: "xl"
      }) : this.renderGroupByRepo();
      var styles = {};
      var expanded = this.state.expanded;
      return _react.default.createElement(_eui.EuiPanel, {
        grow: false,
        className: (0, _classnames.default)(['code-editor-references-panel', expanded ? 'expanded' : '']),
        style: styles
      }, _react.default.createElement(_eui.EuiButtonIcon, {
        size: "s",
        onClick: this.toggleExpand,
        iconType: expanded ? 'arrowDown' : 'arrowUp',
        "aria-label": "Next",
        className: "expandButton"
      }), !expanded && _react.default.createElement(_eui.EuiButtonIcon, {
        className: "euiFlyout__closeButton",
        size: "s",
        onClick: this.close,
        iconType: "cross",
        "aria-label": "Next"
      }), _react.default.createElement(_eui.EuiTitle, {
        size: "s"
      }, _react.default.createElement("h3", null, this.props.title)), _react.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }), _react.default.createElement("div", {
        className: "code-auto-overflow-y"
      }, body));
    }
  }, {
    key: "renderGroupByRepo",
    value: function renderGroupByRepo() {
      var _this2 = this;

      return this.props.references.map(function (ref) {
        return _this2.renderReferenceRepo(ref);
      });
    }
  }, {
    key: "renderReferenceRepo",
    value: function renderReferenceRepo(_ref) {
      var _this3 = this;

      var repo = _ref.repo,
          files = _ref.files;

      var _repo$split$slice = repo.split('/').slice(1),
          _repo$split$slice2 = _slicedToArray(_repo$split$slice, 2),
          org = _repo$split$slice2[0],
          name = _repo$split$slice2[1];

      var buttonContent = _react.default.createElement("span", null, _react.default.createElement("span", null, org), "/", _react.default.createElement("b", null, name));

      return _react.default.createElement(_eui.EuiAccordion, {
        id: repo,
        key: repo,
        buttonContentClassName: "code-editor-reference-accordion-button",
        buttonContent: buttonContent,
        paddingSize: "s",
        initialIsOpen: true
      }, files.map(function (file) {
        return _this3.renderReference(file);
      }));
    }
  }, {
    key: "renderReference",
    value: function renderReference(file) {
      var key = "".concat(file.uri);

      var lineNumberFn = function lineNumberFn(l) {
        return file.lineNumbers[l - 1];
      };

      var fileComponent = _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiText, null, _react.default.createElement("a", {
        href: "#".concat(this.computeUrl(file.uri))
      }, file.file)), _react.default.createElement(_eui.EuiSpacer, {
        size: "s"
      }));

      return _react.default.createElement(_codeblock.CodeBlock, {
        key: key,
        language: file.language,
        startLine: 0,
        code: file.code,
        folding: false,
        lineNumbersFunc: lineNumberFn,
        highlightRanges: file.highlights,
        fileComponent: fileComponent,
        onClick: this.onCodeClick.bind(this, file.lineNumbers, file.uri)
      });
    }
  }, {
    key: "onCodeClick",
    value: function onCodeClick(lineNumbers, url, pos) {
      var line = parseInt(lineNumbers[pos.lineNumber - 1], 10);

      _url.history.push(this.computeUrl(url, line));
    }
  }, {
    key: "computeUrl",
    value: function computeUrl(url, line) {
      var _ref2 = (0, _uri_util.parseSchema)(url),
          uri = _ref2.uri;

      var search = _url.history.location.search;

      if (search.startsWith('?')) {
        search = search.substring(1);
      }

      var queries = _querystring.default.parse(search);

      var query = _querystring.default.stringify(_objectSpread({}, queries, {
        tab: 'references',
        refUrl: this.props.refUrl
      }));

      return line !== undefined ? "".concat(uri, "!L").concat(line, ":0?").concat(query) : "".concat(uri, "?").concat(query);
    }
  }]);

  return ReferencesPanel;
}(_react.default.Component);

exports.ReferencesPanel = ReferencesPanel;