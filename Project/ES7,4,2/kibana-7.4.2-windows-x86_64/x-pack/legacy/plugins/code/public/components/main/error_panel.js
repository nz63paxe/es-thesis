"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorPanel = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _url = require("../../utils/url");

var _icons = require("../shared/icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ErrorPanel = function ErrorPanel(props) {
  return _react.default.createElement("div", {
    className: "codePanel__error",
    "data-test-subj": "codeNotFoundErrorPage"
  }, _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, _react.default.createElement(_icons.ErrorIcon, null)), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, props.title), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, _react.default.createElement(_eui.EuiTextColor, null, props.content)), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    onClick: _url.history.goBack
  }, "Go Back")), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiSpacer, null)));
};

exports.ErrorPanel = ErrorPanel;