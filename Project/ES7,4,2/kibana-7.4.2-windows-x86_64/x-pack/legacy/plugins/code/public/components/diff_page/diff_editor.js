"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiffEditor = void 0;

var _react = _interopRequireDefault(require("react"));

var _monaco_diff_editor = require("../../monaco/monaco_diff_editor");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DiffEditor =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DiffEditor, _React$Component);

  function DiffEditor() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DiffEditor);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DiffEditor)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "diffEditor", null);

    _defineProperty(_assertThisInitialized(_this), "mountDiffEditor", function (container) {
      _this.diffEditor = new _monaco_diff_editor.MonacoDiffEditor(container, _this.props.originCode, _this.props.modifiedCode, _this.props.language, _this.props.renderSideBySide);

      _this.diffEditor.init();
    });

    return _this;
  }

  _createClass(DiffEditor, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if (prevProps.renderSideBySide !== this.props.renderSideBySide) {
        this.updateLayout(this.props.renderSideBySide);
      }
    }
  }, {
    key: "updateLayout",
    value: function updateLayout(renderSideBySide) {
      this.diffEditor.diffEditor.updateOptions({
        renderSideBySide: renderSideBySide
      });
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement("div", {
        id: "diffEditor",
        ref: this.mountDiffEditor,
        style: {
          height: 1000
        }
      });
    }
  }]);

  return DiffEditor;
}(_react.default.Component);

exports.DiffEditor = DiffEditor;