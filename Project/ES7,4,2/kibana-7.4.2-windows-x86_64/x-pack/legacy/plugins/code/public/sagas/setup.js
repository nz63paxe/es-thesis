"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchRootRoute = watchRootRoute;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _patterns = require("./patterns");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleRootRoute),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRootRoute);

function handleRootRoute() {
  return regeneratorRuntime.wrap(function handleRootRoute$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.call)(requestSetup);

        case 3:
          _context.next = 5;
          return (0, _effects.put)((0, _actions.checkSetupSuccess)());

        case 5:
          _context.next = 11;
          break;

        case 7:
          _context.prev = 7;
          _context.t0 = _context["catch"](0);
          _context.next = 11;
          return (0, _effects.put)((0, _actions.checkSetupFailed)());

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 7]]);
}

function requestSetup() {
  return _new_platform.npStart.core.http.head("/api/code/setup");
}

function watchRootRoute() {
  return regeneratorRuntime.wrap(function watchRootRoute$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(_patterns.rootRoutePattern, handleRootRoute);

        case 2:
          _context2.next = 4;
          return (0, _effects.takeEvery)(_patterns.setupRoutePattern, handleRootRoute);

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}