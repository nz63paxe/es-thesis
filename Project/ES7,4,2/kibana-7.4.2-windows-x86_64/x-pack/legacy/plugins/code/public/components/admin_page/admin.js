"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Admin = void 0;

var _i18n = require("@kbn/i18n");

var _querystring = require("querystring");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _url = _interopRequireDefault(require("url"));

var _eui = require("@elastic/eui");

var _actions = require("../../actions");

var _search_bar = require("../search_bar");

var _empty_project = require("./empty_project");

var _language_server_tab = require("./language_server_tab");

var _project_tab = require("./project_tab");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AdminTabs;

(function (AdminTabs) {
  AdminTabs["projects"] = "0";
  AdminTabs["languageServers"] = "1";
})(AdminTabs || (AdminTabs = {}));

var AdminPage =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(AdminPage, _React$PureComponent);

  _createClass(AdminPage, null, [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props) {
      var getTab = function getTab() {
        var search = props.location.search;
        var qs = search;

        if (search.charAt(0) === '?') {
          qs = search.substr(1);
        }

        return (0, _querystring.parse)(qs).tab || AdminTabs.projects;
      };

      return {
        tab: getTab()
      };
    }
  }]);

  function AdminPage(props) {
    var _this;

    _classCallCheck(this, AdminPage);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AdminPage).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "searchBar", null);

    _defineProperty(_assertThisInitialized(_this), "tabs", [{
      id: AdminTabs.projects,
      name: _i18n.i18n.translate('xpack.code.adminPage.repoTabLabel', {
        defaultMessage: 'Repositories'
      }),
      disabled: false
    }, {
      id: AdminTabs.languageServers,
      name: _i18n.i18n.translate('xpack.code.adminPage.langserverTabLabel', {
        defaultMessage: 'Language servers'
      }),
      disabled: false
    }]);

    _defineProperty(_assertThisInitialized(_this), "getAdminTabClickHandler", function (tab) {
      return function () {
        _this.setState({
          tab: tab
        });

        _this.props.history.push(_url.default.format({
          pathname: '/admin',
          query: {
            tab: tab
          }
        }));
      };
    });

    _defineProperty(_assertThisInitialized(_this), "filterRepos", function () {
      return _this.props.repositories;
    });

    _defineProperty(_assertThisInitialized(_this), "renderTabContent", function () {
      switch (_this.state.tab) {
        case AdminTabs.languageServers:
          {
            return _react.default.createElement(_language_server_tab.LanguageSeverTab, null);
          }

        case AdminTabs.projects:
        default:
          {
            var repositoriesCount = _this.props.repositories.length;
            var showEmpty = repositoriesCount === 0 && !_this.props.repositoryLoading;

            if (showEmpty) {
              return _react.default.createElement(_empty_project.EmptyProject, null);
            }

            return _react.default.createElement(_project_tab.ProjectTab, null);
          }
      }
    });

    var getTab = function getTab() {
      var search = props.location.search;
      var qs = search;

      if (search.charAt(0) === '?') {
        qs = search.substr(1);
      }

      return (0, _querystring.parse)(qs).tab || AdminTabs.projects;
    };

    _this.state = {
      tab: getTab()
    };
    return _this;
  }

  _createClass(AdminPage, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      // track admin page load count
      (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.LOADED, _usage_telemetry_metrics.CodeUIUsageMetrics.ADMIN_PAGE_LOAD_COUNT);
    }
  }, {
    key: "renderTabs",
    value: function renderTabs() {
      var _this2 = this;

      var tabs = this.tabs.map(function (tab) {
        return _react.default.createElement(_eui.EuiTab, {
          onClick: _this2.getAdminTabClickHandler(tab.id),
          isSelected: tab.id === _this2.state.tab,
          disabled: tab.disabled,
          key: tab.id
        }, tab.name);
      });
      return _react.default.createElement(_eui.EuiTabs, null, tabs);
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return _react.default.createElement("div", {
        className: "codeContainer__root"
      }, _react.default.createElement("div", {
        className: "codeContainer__rootInner"
      }, _react.default.createElement("div", {
        className: "codeContainer__adminWrapper"
      }, _react.default.createElement(_search_bar.SearchBar, {
        searchOptions: this.props.searchOptions,
        query: this.props.query,
        onSearchScopeChanged: this.props.onSearchScopeChanged,
        enableSubmitWhenOptionsChanged: false,
        ref: function ref(element) {
          return _this3.searchBar = element;
        }
      }), _react.default.createElement("div", {
        className: "codeContainer__adminMain"
      }, this.renderTabs(), this.renderTabContent()))));
    }
  }]);

  return AdminPage;
}(_react.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return _objectSpread({}, state.search, {
    repositories: state.repositoryManagement.repositories,
    repositoryLoading: state.repositoryManagement.loading
  });
};

var mapDispatchToProps = {
  onSearchScopeChanged: _actions.changeSearchScope
};
var Admin = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(AdminPage));
exports.Admin = Admin;