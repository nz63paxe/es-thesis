"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Shortcut", {
  enumerable: true,
  get: function get() {
    return _shortcut.Shortcut;
  }
});
Object.defineProperty(exports, "OS", {
  enumerable: true,
  get: function get() {
    return _shortcut.OS;
  }
});
Object.defineProperty(exports, "HotKey", {
  enumerable: true,
  get: function get() {
    return _shortcut.HotKey;
  }
});
Object.defineProperty(exports, "Modifier", {
  enumerable: true,
  get: function get() {
    return _shortcut.Modifier;
  }
});
Object.defineProperty(exports, "ShortcutsProvider", {
  enumerable: true,
  get: function get() {
    return _shortcuts_provider.ShortcutsProvider;
  }
});

var _shortcut = require("./shortcut");

var _shortcuts_provider = require("./shortcuts_provider");