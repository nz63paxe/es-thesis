"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProjectItem = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _i18n = require("@kbn/i18n");

var _model = require("../../../model");

var _actions = require("../../actions");

var _status = require("../../actions/status");

var _stateColor;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var stateColor = (_stateColor = {}, _defineProperty(_stateColor, _status.RepoState.CLONING, 'secondary'), _defineProperty(_stateColor, _status.RepoState.DELETING, 'accent'), _defineProperty(_stateColor, _status.RepoState.INDEXING, 'primary'), _stateColor);

var CodeProjectItem =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeProjectItem, _React$PureComponent);

  function CodeProjectItem() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeProjectItem);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeProjectItem)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      showDeleteConfirmModal: false,
      showReindexConfirmModal: false
    });

    _defineProperty(_assertThisInitialized(_this), "openReindexModal", function () {
      _this.setState({
        showReindexConfirmModal: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeReindexModal", function () {
      _this.setState({
        showReindexConfirmModal: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "openDeleteModal", function () {
      _this.setState({
        showDeleteConfirmModal: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeDeleteModal", function () {
      _this.setState({
        showDeleteConfirmModal: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "confirmDelete", function () {
      if (_this.props.deleteRepo) {
        _this.props.deleteRepo(_this.props.project.uri);

        _this.closeDeleteModal();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "confirmReindex", function () {
      if (_this.props.indexRepo) {
        _this.props.indexRepo(_this.props.project.uri);

        _this.closeReindexModal();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "renderReindexConfirmModal", function () {
      return _react2.default.createElement(_eui.EuiOverlayMask, null, _react2.default.createElement(_eui.EuiConfirmModal, {
        title: _i18n.i18n.translate('xpack.code.repoItem.reindexConfirmTitle', {
          defaultMessage: 'Reindex this repository?'
        }),
        onCancel: _this.closeReindexModal,
        onConfirm: _this.confirmReindex,
        cancelButtonText: _i18n.i18n.translate('xpack.code.repoItem.cancelButtonText', {
          defaultMessage: "No, don't do it"
        }),
        confirmButtonText: _i18n.i18n.translate('xpack.code.repoItem.confirmButtonText', {
          defaultMessage: 'Yes, do it'
        }),
        defaultFocusedButton: _eui.EUI_MODAL_CONFIRM_BUTTON
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "renderDeleteConfirmModal", function () {
      return _react2.default.createElement(_eui.EuiOverlayMask, null, _react2.default.createElement(_eui.EuiConfirmModal, {
        title: _i18n.i18n.translate('xpack.code.repoItem.deleteConfirmTitle', {
          defaultMessage: 'Delete this repository?'
        }),
        onCancel: _this.closeDeleteModal,
        onConfirm: _this.confirmDelete,
        cancelButtonText: _i18n.i18n.translate('xpack.code.repoItem.cancelButtonText', {
          defaultMessage: "No, don't do it"
        }),
        confirmButtonText: _i18n.i18n.translate('xpack.code.repoItem.confirmButtonText', {
          defaultMessage: 'Yes, do it'
        }),
        buttonColor: "danger",
        defaultFocusedButton: _eui.EUI_MODAL_CONFIRM_BUTTON
      }));
    });

    return _this;
  }

  _createClass(CodeProjectItem, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          project = _this$props.project,
          showStatus = _this$props.showStatus,
          status = _this$props.status,
          enableManagement = _this$props.enableManagement;
      var name = project.name,
          org = project.org,
          uri = project.uri,
          url = project.url;

      var onClickSettings = function onClickSettings() {
        return _this2.props.openSettings && _this2.props.openSettings(uri, url);
      };

      var footer = null;
      var disableRepoLink = false;
      var hasError = false;

      if (!status) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.initText",
          defaultMessage: "INIT..."
        }));
      } else if (status.state === _status.RepoState.READY) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter",
          "data-test-subj": "repositoryIndexDone"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.lastUpdatedText",
          defaultMessage: "LAST UPDATED"
        }), ":", ' ', (0, _moment.default)(status.timestamp).locale(_i18n.i18n.getLocale()).fromNow());
      } else if (status.state === _status.RepoState.DELETING) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.deletingText",
          defaultMessage: "DELETING..."
        }));
      } else if (status.state === _status.RepoState.INDEXING) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter",
          "data-test-subj": "repositoryIndexOngoing"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.indexingText",
          defaultMessage: "INDEXING..."
        }));
      } else if (status.state === _status.RepoState.CLONING) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.cloningText",
          defaultMessage: "CLONING..."
        }));
      } else if (status.state === _status.RepoState.DELETE_ERROR) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter codeFooter--error"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.deleteErrorText",
          defaultMessage: "ERROR DELETE REPO"
        }));
        hasError = true;
      } else if (status.state === _status.RepoState.INDEX_ERROR) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter codeFooter--error"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.indexErrorText",
          defaultMessage: "ERROR INDEX REPO"
        }));
        hasError = true;
      } else if (status.state === _status.RepoState.CLONE_ERROR) {
        footer = _react2.default.createElement("div", {
          className: "codeFooter codeFooter--error"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.code.repoItem.cloneErrorText",
          defaultMessage: "ERROR CLONING REPO"
        }), "\xA0", _react2.default.createElement(_eui.EuiToolTip, {
          position: "top",
          content: status.errorMessage
        }, _react2.default.createElement(_eui.EuiIcon, {
          type: "iInCircle"
        }))); // Disable repo link is clone failed.

        disableRepoLink = true;
        hasError = true;
      }

      var repoTitle = _react2.default.createElement(_eui.EuiText, {
        "data-test-subj": "codeRepositoryItem"
      }, _react2.default.createElement(_eui.EuiTextColor, {
        color: "subdued"
      }, org), "/", _react2.default.createElement("strong", null, name));

      var settingsShow = status && status.state !== _status.RepoState.CLONING && status.state !== _status.RepoState.DELETING;
      var settingsVisibility = settingsShow ? 'visible' : 'hidden';
      var indexShow = status && status.state !== _status.RepoState.CLONING && status.state !== _status.RepoState.DELETING && status.state !== _status.RepoState.INDEXING && status.state !== _status.RepoState.CLONE_ERROR;
      var indexVisibility = indexShow ? 'visible' : 'hidden';
      var deleteShow = status && status.state !== _status.RepoState.DELETING;
      var deleteVisibility = deleteShow ? 'visible' : 'hidden';

      var projectManagement = _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement(_eui.EuiFlexGroup, {
        gutterSize: "none"
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false,
        style: {
          display: 'none'
        }
      }, _react2.default.createElement("div", {
        className: "codeButton__project",
        "data-test-subj": "settingsRepositoryButton",
        tabIndex: 0,
        onKeyPress: onClickSettings,
        onClick: onClickSettings,
        role: "button",
        style: {
          visibility: settingsVisibility
        }
      }, _react2.default.createElement(_eui.EuiIcon, {
        type: "gear"
      }), _react2.default.createElement(_eui.EuiText, {
        size: "xs",
        color: "subdued"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.repoItem.settingsButtonLabel",
        defaultMessage: "Settings"
      })))), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement("div", {
        className: "codeButton__project",
        "data-test-subj": "indexRepositoryButton",
        tabIndex: 0,
        onKeyPress: this.openReindexModal,
        onClick: this.openReindexModal,
        role: "button",
        style: {
          visibility: indexVisibility
        }
      }, _react2.default.createElement(_eui.EuiIcon, {
        type: "indexSettings"
      }), _react2.default.createElement(_eui.EuiText, {
        size: "xs",
        color: "subdued"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.repoItem.reindexButtonLabel",
        defaultMessage: "Reindex"
      })))), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement("div", {
        className: "codeButton__project",
        "data-test-subj": "deleteRepositoryButton",
        tabIndex: 0,
        onKeyPress: this.openDeleteModal,
        onClick: this.openDeleteModal,
        role: "button",
        style: {
          visibility: deleteVisibility
        }
      }, _react2.default.createElement(_eui.EuiIcon, {
        type: "trash",
        color: "danger"
      }), _react2.default.createElement(_eui.EuiText, {
        size: "xs",
        color: "subdued"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.repoItem.deleteButtonLabel",
        defaultMessage: "Delete"
      }))))));

      var repoStatus = _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("h6", null, _react2.default.createElement(_eui.EuiTextColor, {
        color: "subdued"
      }, footer)));

      return _react2.default.createElement(_eui.EuiPanel, {
        className: hasError ? 'codePanel__project codePanel__project--error' : 'codePanel__project'
      }, this.renderProgress(), _react2.default.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        justifyContent: "flexStart"
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: 3
      }, disableRepoLink ? repoTitle : _react2.default.createElement(_reactRouterDom.Link, {
        to: "/".concat(uri),
        "data-test-subj": "adminLinkTo".concat(name)
      }, repoTitle), showStatus ? repoStatus : null), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: 3
      }, _react2.default.createElement(_eui.EuiText, {
        color: "subdued",
        size: "s"
      }, _react2.default.createElement(_eui.EuiLink, {
        href: 'https://' + uri,
        target: "_blank"
      }, uri))), enableManagement && projectManagement), this.state.showDeleteConfirmModal && this.renderDeleteConfirmModal(), this.state.showReindexConfirmModal && this.renderReindexConfirmModal());
    }
  }, {
    key: "renderProgress",
    value: function renderProgress() {
      var status = this.props.status;

      if (status && (status.state === _status.RepoState.CLONING || status.state === _status.RepoState.DELETING || status.state === _status.RepoState.INDEXING)) {
        var color = stateColor[status.state];

        if (status.progress === _model.WorkerReservedProgress.COMPLETED) {
          return null;
        } else if (status.progress > _model.WorkerReservedProgress.INIT) {
          return _react2.default.createElement(_eui.EuiProgress, {
            max: 100,
            value: status.progress,
            size: "s",
            color: color,
            position: "absolute"
          });
        } else {
          return _react2.default.createElement(_eui.EuiProgress, {
            size: "s",
            color: color,
            position: "absolute"
          });
        }
      }
    }
  }]);

  return CodeProjectItem;
}(_react2.default.PureComponent);

var mapDispatchToProps = {
  deleteRepo: _actions.deleteRepo,
  indexRepo: _actions.indexRepo,
  initRepoCommand: _actions.initRepoCommand
};
var ProjectItem = (0, _reactRedux.connect)(null, mapDispatchToProps)(CodeProjectItem);
exports.ProjectItem = ProjectItem;