"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shortcuts = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var helpShortcut = {
  key: '?',
  help: 'Display this page',
  modifier: new Map(),
  onPress: function onPress(dispatch) {
    dispatch((0, _actions.toggleHelp)(null));
  }
};
var initialState = {
  showHelp: false,
  shortcuts: [helpShortcut]
};
var shortcuts = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.toggleHelp), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload === null) {
      draft.showHelp = !state.showHelp;
    } else {
      draft.showHelp = action.payload;
    }
  });
}), _defineProperty(_handleActions, String(_actions.registerShortcut), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var hotKey = action.payload;
    draft.shortcuts.push(hotKey);
  });
}), _defineProperty(_handleActions, String(_actions.unregisterShortcut), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var hotKey = action.payload;
    var idx = state.shortcuts.indexOf(hotKey);

    if (idx >= 0) {
      draft.shortcuts.splice(idx, 1);
    }
  });
}), _handleActions), initialState);
exports.shortcuts = shortcuts;