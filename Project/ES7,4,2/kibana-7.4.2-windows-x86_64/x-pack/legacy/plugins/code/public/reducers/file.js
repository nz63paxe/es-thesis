"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.file = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  isNotFound: false,
  loading: false
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.file = undefined;
    draft.isNotFound = initialState.isNotFound;
    draft.loading = initialState.loading;
  });
};

var file = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.fetchFile), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_actions.fetchFileSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.file = action.payload;
    draft.isNotFound = false;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_actions.fetchFileFailed), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.file = undefined;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_actions.setNotFound), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.isNotFound = action.payload;
    draft.loading = false;
  });
}), _defineProperty(_handleActions, String(_actions.routeChange), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.isNotFound = false;
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _defineProperty(_handleActions, String(_route.revisionChange), clearState), _defineProperty(_handleActions, String(_route.filePathChange), clearState), _handleActions), initialState);
exports.file = file;