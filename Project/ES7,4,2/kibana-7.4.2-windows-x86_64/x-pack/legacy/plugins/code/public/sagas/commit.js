"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLoadCommit = watchLoadCommit;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _patterns = require("./patterns");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleLoadCommit),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadCommit);

function requestCommit(repo, commitId) {
  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(repo, "/diff/").concat(commitId));
}

function handleLoadCommit(action) {
  var _params, commitId, resource, org, repo, repoUri, commit;

  return regeneratorRuntime.wrap(function handleLoadCommit$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _params = action.payload.params, commitId = _params.commitId, resource = _params.resource, org = _params.org, repo = _params.repo;
          _context.next = 4;
          return (0, _effects.put)((0, _actions.loadCommit)(commitId));

        case 4:
          repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);
          _context.next = 7;
          return (0, _effects.call)(requestCommit, repoUri, commitId);

        case 7:
          commit = _context.sent;
          _context.next = 10;
          return (0, _effects.put)((0, _actions.loadCommitSuccess)(commit));

        case 10:
          _context.next = 16;
          break;

        case 12:
          _context.prev = 12;
          _context.t0 = _context["catch"](0);
          _context.next = 16;
          return (0, _effects.put)((0, _actions.loadCommitFailed)(_context.t0));

        case 16:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 12]]);
}

function watchLoadCommit() {
  return regeneratorRuntime.wrap(function watchLoadCommit$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(_patterns.commitRoutePattern, handleLoadCommit);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}