"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchFetchRepoTree = watchFetchRepoTree;
exports.watchFetchRootRepoTree = watchFetchRootRepoTree;
exports.requestFile = requestFile;
exports.watchFetchBranchesAndCommits = watchFetchBranchesAndCommits;
exports.watchRepoRouteChange = watchRepoRouteChange;

var _new_platform = require("ui/new_platform");

var _url = _interopRequireDefault(require("url"));

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _selectors = require("../selectors");

var _patterns = require("./patterns");

var _saga_utils = require("../utils/saga_utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchRepoTree),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchFetchRepoTree),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchRootRepoTree),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchFetchRootRepoTree),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchBranches),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchCommits),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchMoreCommits),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchTreeCommits),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchFile),
    _marked10 =
/*#__PURE__*/
regeneratorRuntime.mark(handleFetchDirs),
    _marked11 =
/*#__PURE__*/
regeneratorRuntime.mark(watchFetchBranchesAndCommits),
    _marked12 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoRouteChange),
    _marked13 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoRouteChange);

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function handleFetchRepoTree(action) {
  var tree;
  return regeneratorRuntime.wrap(function handleFetchRepoTree$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return (0, _effects.call)(requestRepoTree, action.payload);

        case 3:
          tree = _context.sent;
          (tree.children || []).sort(function (a, b) {
            var typeDiff = a.type - b.type;

            if (typeDiff === 0) {
              return a.name > b.name ? 1 : -1;
            } else {
              return -typeDiff;
            }
          });
          tree.repoUri = action.payload.uri;
          _context.next = 8;
          return (0, _effects.put)((0, _actions.fetchRepoTreeSuccess)({
            revision: action.payload.revision,
            tree: tree,
            path: action.payload.path,
            withParents: action.payload.parents
          }));

        case 8:
          _context.next = 17;
          break;

        case 10:
          _context.prev = 10;
          _context.t0 = _context["catch"](0);

          if (!(action.payload.isDir && _context.t0.body && _context.t0.body.statusCode === 404)) {
            _context.next = 15;
            break;
          }

          _context.next = 15;
          return (0, _effects.put)((0, _actions.dirNotFound)(action.payload.path));

        case 15:
          _context.next = 17;
          return (0, _effects.put)((0, _actions.fetchRepoTreeFailed)(_objectSpread({}, _context.t0, {
            path: action.payload.path
          })));

        case 17:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 10]]);
}

function requestRepoTree(_ref) {
  var uri = _ref.uri,
      revision = _ref.revision,
      path = _ref.path,
      _ref$limit = _ref.limit,
      limit = _ref$limit === void 0 ? 1000 : _ref$limit,
      _ref$parents = _ref.parents,
      parents = _ref$parents === void 0 ? false : _ref$parents;
  var query = {
    limit: limit,
    flatten: true
  };

  if (parents) {
    query.parents = true;
  }

  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(uri, "/tree/").concat(encodeURIComponent(revision), "/").concat(path), {
    query: query
  });
}

function watchFetchRepoTree() {
  return regeneratorRuntime.wrap(function watchFetchRepoTree$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchRepoTree), handleFetchRepoTree);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function handleFetchRootRepoTree(action) {
  var _ref2, uri, revision, _tree;

  return regeneratorRuntime.wrap(function handleFetchRootRepoTree$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _ref2 = action.payload, uri = _ref2.uri, revision = _ref2.revision;
          _context3.next = 4;
          return (0, _effects.call)(requestRepoTree, {
            uri: uri,
            revision: revision,
            path: '',
            isDir: true
          });

        case 4:
          _tree = _context3.sent;
          _context3.next = 7;
          return (0, _effects.put)((0, _actions.fetchRootRepoTreeSuccess)({
            tree: _tree,
            revision: revision
          }));

        case 7:
          _context3.next = 13;
          break;

        case 9:
          _context3.prev = 9;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 13;
          return (0, _effects.put)((0, _actions.fetchRootRepoTreeFailed)(_context3.t0));

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 9]]);
}

function watchFetchRootRepoTree() {
  return regeneratorRuntime.wrap(function watchFetchRootRepoTree$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchRootRepoTree), handleFetchRootRepoTree);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function handleFetchBranches(action) {
  var results;
  return regeneratorRuntime.wrap(function handleFetchBranches$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.prev = 0;
          _context5.next = 3;
          return (0, _effects.call)(requestBranches, action.payload);

        case 3:
          results = _context5.sent;
          _context5.next = 6;
          return (0, _effects.put)((0, _actions.fetchRepoBranchesSuccess)(results));

        case 6:
          _context5.next = 12;
          break;

        case 8:
          _context5.prev = 8;
          _context5.t0 = _context5["catch"](0);
          _context5.next = 12;
          return (0, _effects.put)((0, _actions.fetchRepoBranchesFailed)(_context5.t0));

        case 12:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5, null, [[0, 8]]);
}

function requestBranches(_ref3) {
  var uri = _ref3.uri;
  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(uri, "/references"));
}

function handleFetchCommits(action) {
  var _results;

  return regeneratorRuntime.wrap(function handleFetchCommits$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.prev = 0;
          _context6.next = 3;
          return (0, _effects.call)(requestCommits, action.payload);

        case 3:
          _results = _context6.sent;
          _context6.next = 6;
          return (0, _effects.put)((0, _actions.fetchRepoCommitsSuccess)(_results));

        case 6:
          _context6.next = 12;
          break;

        case 8:
          _context6.prev = 8;
          _context6.t0 = _context6["catch"](0);
          _context6.next = 12;
          return (0, _effects.put)((0, _actions.fetchRepoCommitsFailed)(_context6.t0));

        case 12:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6, null, [[0, 8]]);
}

function handleFetchMoreCommits(action) {
  var path, commits, _revision, _uri, newCommits;

  return regeneratorRuntime.wrap(function handleFetchMoreCommits$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.prev = 0;
          _context7.next = 3;
          return (0, _effects.select)(_selectors.currentPathSelector);

        case 3:
          path = _context7.sent;
          _context7.next = 6;
          return (0, _effects.select)(_selectors.treeCommitsSelector);

        case 6:
          commits = _context7.sent;
          _revision = commits.length > 0 ? commits[commits.length - 1].id : 'head';
          _uri = action.payload; // @ts-ignore

          _context7.next = 11;
          return (0, _effects.call)(requestCommits, {
            uri: _uri,
            revision: _revision
          }, path, true);

        case 11:
          newCommits = _context7.sent;
          _context7.next = 14;
          return (0, _effects.put)((0, _actions.fetchTreeCommitsSuccess)({
            path: path,
            commits: newCommits,
            append: true
          }));

        case 14:
          _context7.next = 20;
          break;

        case 16:
          _context7.prev = 16;
          _context7.t0 = _context7["catch"](0);
          _context7.next = 20;
          return (0, _effects.put)((0, _actions.fetchTreeCommitsFailed)(_context7.t0));

        case 20:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7, null, [[0, 16]]);
}

function handleFetchTreeCommits(action, signal, task) {
  var _path, _commits;

  return regeneratorRuntime.wrap(function handleFetchTreeCommits$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.prev = 0;
          _path = action.payload.path;
          _context8.next = 4;
          return (0, _effects.call)(requestCommits, action.payload, _path, undefined, undefined, signal);

        case 4:
          _commits = _context8.sent;
          _context8.next = 7;
          return (0, _effects.put)((0, _actions.fetchTreeCommitsSuccess)({
            path: _path,
            commits: _commits
          }));

        case 7:
          _context8.next = 13;
          break;

        case 9:
          _context8.prev = 9;
          _context8.t0 = _context8["catch"](0);
          _context8.next = 13;
          return (0, _effects.put)((0, _actions.fetchTreeCommitsFailed)(_context8.t0));

        case 13:
          _context8.prev = 13;
          _context8.next = 16;
          return (0, _effects.cancel)(task);

        case 16:
          return _context8.finish(13);

        case 17:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8, null, [[0, 9, 13, 17]]);
}

function requestCommits(_ref4, path, loadMore, count, signal) {
  var uri = _ref4.uri,
      revision = _ref4.revision;
  var pathStr = path ? "/".concat(path) : '';
  var query = {};

  if (loadMore) {
    query = {
      after: 1
    };
  }

  if (count) {
    query = {
      count: count
    };
  }

  return _new_platform.npStart.core.http.get("/api/code/repo/".concat(uri, "/history/").concat(encodeURIComponent(revision)).concat(pathStr), {
    query: query,
    signal: signal
  });
}

function requestFile(_x, _x2, _x3) {
  return _requestFile.apply(this, arguments);
}

function _requestFile() {
  _requestFile = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(payload, line, signal) {
    var uri, revision, path, url, query, response, contentType, lang;
    return regeneratorRuntime.wrap(function _callee$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            uri = payload.uri, revision = payload.revision, path = payload.path;
            url = "/api/code/repo/".concat(uri, "/blob/").concat(encodeURIComponent(revision), "/").concat(path);
            query = {};

            if (line) {
              query.line = line;
            }

            _context14.next = 6;
            return fetch(_new_platform.npStart.core.http.basePath.prepend(_url.default.format({
              pathname: url,
              query: query
            })), {
              signal: signal
            });

          case 6:
            response = _context14.sent;

            if (!(response.status >= 200 && response.status < 300)) {
              _context14.next = 28;
              break;
            }

            contentType = response.headers.get('Content-Type');

            if (!(contentType && contentType.startsWith('text/'))) {
              _context14.next = 21;
              break;
            }

            lang = response.headers.get('lang') || undefined;

            if (!(lang === 'big')) {
              _context14.next = 13;
              break;
            }

            return _context14.abrupt("return", {
              payload: payload,
              content: '',
              isOversize: true
            });

          case 13:
            _context14.t0 = payload;
            _context14.t1 = lang;
            _context14.next = 17;
            return response.text();

          case 17:
            _context14.t2 = _context14.sent;
            return _context14.abrupt("return", {
              payload: _context14.t0,
              lang: _context14.t1,
              content: _context14.t2,
              isUnsupported: false
            });

          case 21:
            if (!(contentType && contentType.startsWith('image/'))) {
              _context14.next = 25;
              break;
            }

            return _context14.abrupt("return", {
              payload: payload,
              isImage: true,
              content: '',
              url: url,
              isUnsupported: false
            });

          case 25:
            return _context14.abrupt("return", {
              payload: payload,
              isImage: false,
              content: '',
              url: url,
              isUnsupported: true
            });

          case 26:
            _context14.next = 30;
            break;

          case 28:
            if (!(response.status === 404)) {
              _context14.next = 30;
              break;
            }

            return _context14.abrupt("return", {
              payload: payload,
              isNotFound: true
            });

          case 30:
            throw new Error('invalid file type');

          case 31:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee);
  }));
  return _requestFile.apply(this, arguments);
}

function handleFetchFile(action, signal, task) {
  var _results2, _path2;

  return regeneratorRuntime.wrap(function handleFetchFile$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.prev = 0;
          _context9.next = 3;
          return (0, _effects.call)(requestFile, action.payload, undefined, signal);

        case 3:
          _results2 = _context9.sent;

          if (!_results2.isNotFound) {
            _context9.next = 11;
            break;
          }

          _context9.next = 7;
          return (0, _effects.put)((0, _actions.setNotFound)(true));

        case 7:
          _context9.next = 9;
          return (0, _effects.put)((0, _actions.fetchFileFailed)(new Error('file not found')));

        case 9:
          _context9.next = 17;
          break;

        case 11:
          _context9.next = 13;
          return (0, _effects.select)(_selectors.currentPathSelector);

        case 13:
          _path2 = _context9.sent;

          if (!(_path2 === action.payload.path)) {
            _context9.next = 17;
            break;
          }

          _context9.next = 17;
          return (0, _effects.put)((0, _actions.fetchFileSuccess)(_results2));

        case 17:
          _context9.next = 23;
          break;

        case 19:
          _context9.prev = 19;
          _context9.t0 = _context9["catch"](0);
          _context9.next = 23;
          return (0, _effects.put)((0, _actions.fetchFileFailed)(_context9.t0));

        case 23:
          _context9.prev = 23;
          _context9.next = 26;
          return (0, _effects.cancel)(task);

        case 26:
          return _context9.finish(23);

        case 27:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9, null, [[0, 19, 23, 27]]);
}

function handleFetchDirs(action) {
  var dir;
  return regeneratorRuntime.wrap(function handleFetchDirs$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.prev = 0;
          _context10.next = 3;
          return (0, _effects.call)(requestRepoTree, action.payload);

        case 3:
          dir = _context10.sent;
          _context10.next = 6;
          return (0, _effects.put)((0, _actions.fetchDirectorySuccess)(dir));

        case 6:
          _context10.next = 12;
          break;

        case 8:
          _context10.prev = 8;
          _context10.t0 = _context10["catch"](0);
          _context10.next = 12;
          return (0, _actions.fetchDirectoryFailed)(_context10.t0);

        case 12:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10, null, [[0, 8]]);
}

function watchFetchBranchesAndCommits() {
  return regeneratorRuntime.wrap(function watchFetchBranchesAndCommits$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchRepoBranches), handleFetchBranches);

        case 2:
          _context11.next = 4;
          return (0, _effects.takeEvery)(String(_actions.fetchRepoCommits), handleFetchCommits);

        case 4:
          _context11.next = 6;
          return (0, _effects.takeLatest)(String(_actions.fetchFile), (0, _saga_utils.singletonRequestSaga)(handleFetchFile));

        case 6:
          _context11.next = 8;
          return (0, _effects.takeEvery)(String(_actions.fetchDirectory), handleFetchDirs);

        case 8:
          _context11.next = 10;
          return (0, _effects.takeLatest)(String(_actions.fetchTreeCommits), (0, _saga_utils.singletonRequestSaga)(handleFetchTreeCommits));

        case 10:
          _context11.next = 12;
          return (0, _effects.takeLatest)(String(_actions.fetchMoreCommits), handleFetchMoreCommits);

        case 12:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}

function handleRepoRouteChange(action) {
  var _params, repo, org, resource, uri;

  return regeneratorRuntime.wrap(function handleRepoRouteChange$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          _params = action.payload.params, repo = _params.repo, org = _params.org, resource = _params.resource;
          uri = "".concat(resource, "/").concat(org, "/").concat(repo);
          _context12.next = 4;
          return (0, _effects.put)((0, _actions.gotoRepo)(uri));

        case 4:
        case "end":
          return _context12.stop();
      }
    }
  }, _marked12);
}

function watchRepoRouteChange() {
  return regeneratorRuntime.wrap(function watchRepoRouteChange$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.next = 2;
          return (0, _effects.takeEvery)(_patterns.repoRoutePattern, handleRepoRouteChange);

        case 2:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked13);
}