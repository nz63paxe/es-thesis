"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLocation = createLocation;
exports.createMatch = createMatch;
exports.createHistory = createHistory;
exports.mockFunction = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createLocation(location) {
  return _objectSpread({
    pathname: '',
    search: '',
    hash: '',
    state: ''
  }, location);
}

function createMatch(m) {
  return _objectSpread({
    path: '',
    url: '',
    isExact: true
  }, m);
}

var mockFunction = jest.fn();
exports.mockFunction = mockFunction;

function createHistory(h) {
  return _objectSpread({
    length: 0,
    push: mockFunction,
    replace: mockFunction,
    go: mockFunction,
    goBack: mockFunction,
    goForward: mockFunction,
    listen: function listen() {
      return mockFunction;
    },
    block: function block() {
      return mockFunction;
    },
    createHref: mockFunction
  }, h);
}