"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ContentHoverWidget = void 0;

var _react = require("@kbn/i18n/react");

var _async = require("monaco-editor/esm/vs/base/common/async");

var _wordHighlighter = require("monaco-editor/esm/vs/editor/contrib/wordHighlighter/wordHighlighter");

var _react2 = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _lsp_error_codes = require("../../../common/lsp_error_codes");

var _hover_buttons = require("../../components/hover/hover_buttons");

var _hover_widget = require("../../components/hover/hover_widget");

var _content_widget = require("../content_widget");

var _monaco = require("../monaco");

var _operation = require("../operation");

var _hover_computer = require("./hover_computer");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ContentHoverWidget =
/*#__PURE__*/
function (_ContentWidget) {
  _inherits(ContentHoverWidget, _ContentWidget);

  function ContentHoverWidget(editor) {
    var _this;

    _classCallCheck(this, ContentHoverWidget);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ContentHoverWidget).call(this, ContentHoverWidget.ID, editor));

    _defineProperty(_assertThisInitialized(_this), "hoverOperation", void 0);

    _defineProperty(_assertThisInitialized(_this), "computer", void 0);

    _defineProperty(_assertThisInitialized(_this), "lastRange", null);

    _defineProperty(_assertThisInitialized(_this), "shouldFocus", false);

    _defineProperty(_assertThisInitialized(_this), "hoverResultAction", undefined);

    _defineProperty(_assertThisInitialized(_this), "highlightDecorations", []);

    _defineProperty(_assertThisInitialized(_this), "hoverState", _hover_widget.HoverState.LOADING);

    _this.containerDomNode.className = 'monaco-editor-hover hidden';
    _this.containerDomNode.tabIndex = 0;
    _this.domNode.className = 'monaco-editor-hover-content';
    _this.computer = new _hover_computer.HoverComputer();
    _this.hoverOperation = new _operation.Operation(_this.computer, function (result) {
      return _this.result(result);
    }, function (error) {
      // @ts-ignore
      if (error.code === _lsp_error_codes.ServerNotInitialized) {
        _this.hoverState = _hover_widget.HoverState.INITIALIZING;

        _this.render(_this.lastRange);
      }
    }, function () {
      _this.hoverState = _hover_widget.HoverState.LOADING;

      _this.render(_this.lastRange);
    });
    return _this;
  }

  _createClass(ContentHoverWidget, [{
    key: "startShowingAt",
    value: function startShowingAt(range, focus) {
      if (this.isVisible && this.lastRange && this.lastRange.containsRange(range)) {
        return;
      }

      this.hoverOperation.cancel();
      var url = this.editor.getModel().uri.toString();

      if (this.isVisible) {
        this.hide();
      }

      this.computer.setParams(url, range);
      this.hoverOperation.start();
      this.lastRange = range;
      this.shouldFocus = focus;
    }
  }, {
    key: "setHoverResultAction",
    value: function setHoverResultAction(hoverResultAction) {
      this.hoverResultAction = hoverResultAction;
    }
  }, {
    key: "hide",
    value: function hide() {
      _get(_getPrototypeOf(ContentHoverWidget.prototype), "hide", this).call(this);

      this.highlightDecorations = this.editor.deltaDecorations(this.highlightDecorations, []);
    }
  }, {
    key: "result",
    value: function result(_result) {
      if (this.hoverResultAction) {
        // pass the result to redux
        this.hoverResultAction(_result);
      }

      if (this.lastRange && _result && _result.contents) {
        this.render(this.lastRange, _result);
      } else {
        this.hide();
      }
    }
  }, {
    key: "render",
    value: function render(renderRange, result) {
      var fragment = document.createDocumentFragment();
      var props = {
        state: this.hoverState,
        gotoDefinition: this.gotoDefinition.bind(this),
        findReferences: this.findReferences.bind(this)
      };
      var startColumn = renderRange.startColumn;

      if (result) {
        var contents = [];

        if (Array.isArray(result.contents)) {
          contents = result.contents;
        } else {
          contents = [result.contents];
        }

        contents = contents.filter(function (v) {
          if (typeof v === 'string') {
            return !!v;
          } else {
            return !!v.value;
          }
        });

        if (contents.length === 0) {
          this.hide();
          return;
        }

        props = _objectSpread({}, props, {
          state: _hover_widget.HoverState.READY,
          contents: contents
        });

        if (result.range) {
          this.lastRange = this.toMonacoRange(result.range);
          this.highlightOccurrences(this.lastRange);
        }

        startColumn = Math.min(renderRange.startColumn, result.range ? result.range.start.character + 1 : Number.MAX_VALUE);
      }

      this.showAt(new _monaco.monaco.Position(renderRange.startLineNumber, startColumn), this.shouldFocus);

      var element = _react2.default.createElement(_react.I18nProvider, null, _react2.default.createElement(_hover_widget.HoverWidget, props)); // @ts-ignore


      _reactDom.default.render(element, fragment);

      var buttonFragment = document.createDocumentFragment();

      var buttons = _react2.default.createElement(_react.I18nProvider, null, _react2.default.createElement(_hover_buttons.HoverButtons, props)); // @ts-ignore


      _reactDom.default.render(buttons, buttonFragment);

      this.updateContents(fragment, buttonFragment);
    }
  }, {
    key: "toMonacoRange",
    value: function toMonacoRange(r) {
      return new _monaco.monaco.Range(r.start.line + 1, r.start.character + 1, r.end.line + 1, r.end.character + 1);
    }
  }, {
    key: "gotoDefinition",
    value: function gotoDefinition() {
      var _this2 = this;

      if (this.lastRange) {
        this.editor.setPosition({
          lineNumber: this.lastRange.startLineNumber,
          column: this.lastRange.startColumn
        });
        var action = this.editor.getAction('editor.action.revealDefinition');
        action.run().then(function () {
          return _this2.hide();
        });
      }
    }
  }, {
    key: "findReferences",
    value: function findReferences() {
      var _this3 = this;

      if (this.lastRange) {
        this.editor.setPosition({
          lineNumber: this.lastRange.startLineNumber,
          column: this.lastRange.startColumn
        });
        var action = this.editor.getAction('editor.action.referenceSearch.trigger');
        action.run().then(function () {
          return _this3.hide();
        });
      }
    }
  }, {
    key: "highlightOccurrences",
    value: function highlightOccurrences(range) {
      var _this4 = this;

      var pos = new _monaco.monaco.Position(range.startLineNumber, range.startColumn);
      return (0, _async.createCancelablePromise)(function (token) {
        return (0, _wordHighlighter.getOccurrencesAtPosition)(_this4.editor.getModel(), pos, token).then(function (data) {
          if (data) {
            if (_this4.isVisible) {
              var decorations = data.map(function (h) {
                return {
                  range: h.range,
                  options: ContentHoverWidget.DECORATION_OPTIONS
                };
              });
              _this4.highlightDecorations = _this4.editor.deltaDecorations(_this4.highlightDecorations, decorations);
            }
          } else {
            _this4.highlightDecorations = _this4.editor.deltaDecorations(_this4.highlightDecorations, [{
              range: range,
              options: ContentHoverWidget.DECORATION_OPTIONS
            }]);
          }
        });
      });
    }
  }]);

  return ContentHoverWidget;
}(_content_widget.ContentWidget);

exports.ContentHoverWidget = ContentHoverWidget;

_defineProperty(ContentHoverWidget, "ID", 'editor.contrib.contentHoverWidget');

_defineProperty(ContentHoverWidget, "DECORATION_OPTIONS", {
  className: 'wordHighlightStrong' //  hoverHighlight wordHighlightStrong

});