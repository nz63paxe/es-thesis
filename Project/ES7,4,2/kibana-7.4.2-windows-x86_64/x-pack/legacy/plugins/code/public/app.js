"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startApp = startApp;

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _reactRedux = require("react-redux");

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

require("ui/autoload/all");

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _modules = require("ui/modules");

var _constants = require("../common/constants");

var _app = require("./components/app");

var _help_menu = require("./components/help_menu");

var _stores = require("./stores");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function startApp(coreStart) {
  // `getInjected` is not currently available in new platform `coreStart.chrome`
  if (_chrome.default.getInjected('codeUiEnabled')) {
    var RootController = function RootController($scope, $element, $http) {
      var domNode = $element[0]; // render react to DOM

      (0, _reactDom.render)(_react.default.createElement(_react2.I18nProvider, null, _react.default.createElement(_reactRedux.Provider, {
        store: _stores.store
      }, _react.default.createElement(_app.App, null))), domNode); // unmount react on controller destroy

      $scope.$on('$destroy', function () {
        (0, _reactDom.unmountComponentAtNode)(domNode);
      });
    }; // `setRootController` is not available now in `coreStart.chrome`


    // TODO the entire Kibana uses moment, we might need to move it to a more common place
    _moment.default.locale(_i18n.i18n.getLocale());

    var app = _modules.uiModules.get('apps/code');

    app.config(function ($locationProvider) {
      $locationProvider.html5Mode({
        enabled: false,
        requireBase: false,
        rewriteLinks: false
      });
    });
    app.config(function (stateManagementConfigProvider) {
      return stateManagementConfigProvider.disable();
    });

    _chrome.default.setRootController('code', RootController);

    coreStart.chrome.setBreadcrumbs([{
      text: _constants.APP_TITLE,
      href: '#/'
    }]);
    coreStart.chrome.setHelpExtension(function (domNode) {
      (0, _reactDom.render)(_react.default.createElement(_help_menu.HelpMenu, null), domNode);
      return function () {
        (0, _reactDom.unmountComponentAtNode)(domNode);
      };
    });
  }
}