"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatusIndicator = exports.StatusIndicatorComponent = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _error = _interopRequireDefault(require("./error.svg"));

var _info = _interopRequireDefault(require("./info.svg"));

var _alert = _interopRequireDefault(require("./alert.svg"));

var _blank = _interopRequireDefault(require("./blank.svg"));

var _repo_file_status = require("../../../common/repo_file_status");

var _svgs;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var svgs = (_svgs = {}, _defineProperty(_svgs, _repo_file_status.Severity.NOTICE, _info.default), _defineProperty(_svgs, _repo_file_status.Severity.WARNING, _alert.default), _defineProperty(_svgs, _repo_file_status.Severity.ERROR, _error.default), _defineProperty(_svgs, _repo_file_status.Severity.NONE, _blank.default), _svgs);

var StatusIndicatorComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(StatusIndicatorComponent, _React$Component);

  function StatusIndicatorComponent(props) {
    var _this;

    _classCallCheck(this, StatusIndicatorComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(StatusIndicatorComponent).call(this, props));
    _this.state = {
      isPopoverOpen: false
    };
    return _this;
  }

  _createClass(StatusIndicatorComponent, [{
    key: "closePopover",
    value: function closePopover() {
      this.setState({
        isPopoverOpen: false
      });
    }
  }, {
    key: "openPopover",
    value: function openPopover() {
      this.setState({
        isPopoverOpen: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var statusReport = this.props.statusReport;
      var severity = _repo_file_status.Severity.NONE;
      var children = [];

      var addError = function addError(error) {
        // @ts-ignore
        var s = _repo_file_status.REPO_FILE_STATUS_SEVERITY[error];

        if (s) {
          if (s.severity > severity) {
            severity = s.severity;
          }

          var fix = s.fix;

          if (fix !== undefined) {
            var fixUrl = _this2.fixUrl(fix);

            children.push(_react.default.createElement("p", {
              key: "".concat(error, "_key")
            }, error, " You can ", fixUrl, "."));
          } else {
            children.push(_react.default.createElement("p", {
              key: "".concat(error, "_key")
            }, _repo_file_status.RepoFileStatusText[error]));
          }
        }
      };

      if (statusReport) {
        if (statusReport.repoStatus) {
          addError(statusReport.repoStatus);
        }

        if (statusReport.fileStatus) {
          addError(statusReport.fileStatus);
        }

        if (statusReport.langServerType === _repo_file_status.LangServerType.GENERIC) {
          addError(statusReport.langServerType);
        }

        if (statusReport.langServerStatus) {
          addError(statusReport.langServerStatus);
        }
      }

      var svg = svgs[severity];

      var icon = _react.default.createElement(_eui.EuiButtonIcon, {
        "aria-label": "status_indicator",
        iconType: svg,
        onClick: this.openPopover.bind(this)
      });

      if (children.length === 0) {
        return _react.default.createElement("div", null);
      }

      return _react.default.createElement(_eui.EuiPopover, {
        id: "trapFocus",
        isOpen: this.state.isPopoverOpen,
        closePopover: this.closePopover.bind(this),
        ownFocus: true,
        button: icon
      }, _react.default.createElement(_eui.EuiText, {
        size: "xs"
      }, children));
    }
  }, {
    key: "fixUrl",
    value: function fixUrl(fix) {
      switch (fix) {
        case _repo_file_status.CTA.GOTO_LANG_MANAGE_PAGE:
          return _react.default.createElement(_reactRouterDom.Link, {
            to: "/admin?tab=LanguageServers"
          }, "install it here");

        case _repo_file_status.CTA.SWITCH_TO_HEAD:
          var _ref = this.props.currentStatusPath,
              uri = _ref.uri,
              path = _ref.path;
          var headUrl = path ? "/".concat(uri, "/").concat(this.props.pathType, "/HEAD/").concat(path) : "/".concat(uri, "/").concat(this.props.pathType, "/HEAD/");
          return _react.default.createElement(_reactRouterDom.Link, {
            to: headUrl
          }, "switch to HEAD");
      }
    }
  }]);

  return StatusIndicatorComponent;
}(_react.default.Component);

exports.StatusIndicatorComponent = StatusIndicatorComponent;

var mapStateToProps = function mapStateToProps(state) {
  return {
    statusReport: state.status.repoFileStatus,
    currentStatusPath: state.status.currentStatusPath,
    pathType: state.route.match.params.pathType
  };
};

var StatusIndicator = (0, _reactRedux.connect)(mapStateToProps)(StatusIndicatorComponent);
exports.StatusIndicator = StatusIndicator;