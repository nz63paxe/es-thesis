"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmptyPlaceholder = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var EmptyPlaceholder = function EmptyPlaceholder(props) {
  return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, {
    size: "xxl"
  }), _react2.default.createElement(_eui.EuiSpacer, {
    size: "xxl"
  }), _react2.default.createElement(_eui.EuiText, {
    textAlign: "center",
    className: "eui-textTruncate",
    style: {
      fontSize: '24px',
      color: '#98A2B3'
    }
  }, "\"", _react2.default.createElement("span", {
    className: "eui-textTruncate eui-displayInlineBlock",
    style: {
      maxWidth: '90%'
    }
  }, props.query), "\""), _react2.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react2.default.createElement(_eui.EuiText, {
    textAlign: "center",
    style: {
      fontSize: '28px',
      color: '#1A1A1A'
    }
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.searchPage.emptyTitle",
    defaultMessage: "Hmmm... we looked for that, but couldn\u2019t find anything."
  })), _react2.default.createElement(_eui.EuiSpacer, {
    size: "xl"
  }), _react2.default.createElement(_eui.EuiText, {
    textAlign: "center",
    color: "subdued"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.searchPage.emptyText",
    defaultMessage: "You can search for something else or modify your search settings."
  })), _react2.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react2.default.createElement(_eui.EuiText, {
    textAlign: "center"
  }, _react2.default.createElement(_eui.EuiButton, {
    fill: true,
    onClick: function onClick() {
      if (props.toggleOptionsFlyout) {
        props.toggleOptionsFlyout();
      }
    }
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.searchPage.modifySearchSettingButtonLabel",
    defaultMessage: "Modify your search settings"
  }))));
};

exports.EmptyPlaceholder = EmptyPlaceholder;