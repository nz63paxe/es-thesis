"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Breadcrumb = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _uri_util = require("../../../common/uri_util");

var _ui_metric = require("../../services/ui_metric");

var _usage_telemetry_metrics = require("../../../model/usage_telemetry_metrics");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Breadcrumb =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Breadcrumb, _React$PureComponent);

  function Breadcrumb() {
    _classCallCheck(this, Breadcrumb);

    return _possibleConstructorReturn(this, _getPrototypeOf(Breadcrumb).apply(this, arguments));
  }

  _createClass(Breadcrumb, [{
    key: "render",
    value: function render() {
      var _this$props$routePara = this.props.routeParams,
          resource = _this$props$routePara.resource,
          org = _this$props$routePara.org,
          repo = _this$props$routePara.repo,
          revision = _this$props$routePara.revision,
          path = _this$props$routePara.path;
      var repoUri = "".concat(resource, "/").concat(org, "/").concat(repo);
      var breadcrumbs = [];
      var pathSegments = path ? path.split('/') : [];
      pathSegments.forEach(function (p, index, array) {
        var _breadcrumb;

        var paths = pathSegments.slice(0, index + 1);
        var href = "#".concat(repoUri, "/tree/").concat((0, _uri_util.encodeRevisionString)(revision), "/").concat(paths.join('/'));
        var breadcrumb = (_breadcrumb = {
          text: p,
          href: href,
          className: 'codeNoMinWidth'
        }, _defineProperty(_breadcrumb, 'data-test-subj', "codeFileBreadcrumb-".concat(p)), _defineProperty(_breadcrumb, "onClick", function onClick() {
          // track breadcrumb click count
          (0, _ui_metric.trackCodeUiMetric)(_ui_metric.METRIC_TYPE.COUNT, _usage_telemetry_metrics.CodeUIUsageMetrics.BREADCRUMB_CLICK_COUNT);
        }), _breadcrumb);

        if (index === array.length - 1) {
          delete breadcrumb.href;
        }

        breadcrumbs.push(breadcrumb);
      });
      return _react.default.createElement(_eui.EuiBreadcrumbs, {
        max: Number.MAX_VALUE,
        breadcrumbs: breadcrumbs
      });
    }
  }]);

  return Breadcrumb;
}(_react.default.PureComponent);

exports.Breadcrumb = Breadcrumb;