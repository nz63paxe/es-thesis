"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadRecentProjectsFailed = exports.loadRecentProjectsSuccess = exports.loadRecentProjects = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadRecentProjects = (0, _reduxActions.createAction)('LOAD RECENT PROJECTS');
exports.loadRecentProjects = loadRecentProjects;
var loadRecentProjectsSuccess = (0, _reduxActions.createAction)('LOAD RECENT PROJECTS SUCCESS');
exports.loadRecentProjectsSuccess = loadRecentProjectsSuccess;
var loadRecentProjectsFailed = (0, _reduxActions.createAction)('LOAD RECENT PROJECTS FAILED');
exports.loadRecentProjectsFailed = loadRecentProjectsFailed;