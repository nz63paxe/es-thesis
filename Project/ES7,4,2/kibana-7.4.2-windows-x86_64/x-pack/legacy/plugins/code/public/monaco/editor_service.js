"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EditorService = void 0;

var _standaloneCodeServiceImpl = require("monaco-editor/esm/vs/editor/standalone/browser/standaloneCodeServiceImpl.js");

var _new_platform = require("ui/new_platform");

var _uri_util = require("../../common/uri_util");

var _url = require("../utils/url");

var _jsonrpc = require("../../common/jsonrpc");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var EditorService =
/*#__PURE__*/
function (_StandaloneCodeEditor) {
  _inherits(EditorService, _StandaloneCodeEditor);

  function EditorService(getUrlQuery) {
    var _this;

    _classCallCheck(this, EditorService);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(EditorService).call(this));
    _this.getUrlQuery = getUrlQuery;

    _defineProperty(_assertThisInitialized(_this), "helper", void 0);

    return _this;
  }

  _createClass(EditorService, [{
    key: "openCodeEditor",
    value: function () {
      var _openCodeEditor = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(input, source, sideBySide) {
        var _input$resource, scheme, authority, path, uri, _input$options$select, startColumn, startLineNumber, url, currentPath;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _input$resource = input.resource, scheme = _input$resource.scheme, authority = _input$resource.authority, path = _input$resource.path;

                if (!(scheme === 'symbol')) {
                  _context.next = 6;
                  break;
                }

                _context.next = 4;
                return EditorService.handleSymbolUri(authority, this.getUrlQuery);

              case 4:
                _context.next = 8;
                break;

              case 6:
                uri = "/".concat(authority).concat(path);

                if (input.options && input.options.selection) {
                  _input$options$select = input.options.selection, startColumn = _input$options$select.startColumn, startLineNumber = _input$options$select.startLineNumber;
                  url = uri + "!L".concat(startLineNumber, ":").concat(startColumn);
                  currentPath = window.location.hash.substring(1);

                  if (currentPath === url) {
                    this.helper.revealPosition(startLineNumber, startColumn);
                  } else {
                    _url.history.push("".concat(url).concat(this.getUrlQuery()));
                  }
                }

              case 8:
                return _context.abrupt("return", source);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function openCodeEditor(_x, _x2, _x3) {
        return _openCodeEditor.apply(this, arguments);
      }

      return openCodeEditor;
    }()
  }, {
    key: "setMonacoHelper",
    value: function setMonacoHelper(helper) {
      this.helper = helper;
    }
  }], [{
    key: "handleSymbolUri",
    value: function () {
      var _handleSymbolUri = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(qname, getUrlQuery) {
        var result, symbol, _parseSchema, schema, uri, _symbol$location$rang, line, character, url;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return EditorService.findSymbolByQname(qname);

              case 2:
                result = _context2.sent;

                if (result.symbols.length > 0) {
                  symbol = result.symbols[0].symbolInformation;
                  _parseSchema = (0, _uri_util.parseSchema)(symbol.location.uri), schema = _parseSchema.schema, uri = _parseSchema.uri;

                  if (schema === 'git:') {
                    _symbol$location$rang = symbol.location.range.start, line = _symbol$location$rang.line, character = _symbol$location$rang.character;
                    url = uri + "!L".concat(line + 1, ":").concat(character + 1);

                    _url.history.push("".concat(url).concat(getUrlQuery()));
                  }
                }

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function handleSymbolUri(_x4, _x5) {
        return _handleSymbolUri.apply(this, arguments);
      }

      return handleSymbolUri;
    }()
  }, {
    key: "findSymbolByQname",
    value: function () {
      var _findSymbolByQname = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(qname) {
        var response, error;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _new_platform.npStart.core.http.get("/api/code/lsp/symbol/".concat(qname));

              case 3:
                response = _context3.sent;
                return _context3.abrupt("return", response);

              case 7:
                _context3.prev = 7;
                _context3.t0 = _context3["catch"](0);
                error = _context3.t0.body;
                throw new _jsonrpc.ResponseError(error.code, error.message, error.data);

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 7]]);
      }));

      function findSymbolByQname(_x6) {
        return _findSymbolByQname.apply(this, arguments);
      }

      return findSymbolByQname;
    }()
  }]);

  return EditorService;
}(_standaloneCodeServiceImpl.StandaloneCodeEditorServiceImpl);

exports.EditorService = EditorService;