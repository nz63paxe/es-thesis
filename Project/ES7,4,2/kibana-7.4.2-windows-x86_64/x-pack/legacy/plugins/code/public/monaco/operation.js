"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Operation = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var OperationState;

(function (OperationState) {
  OperationState[OperationState["IDLE"] = 0] = "IDLE";
  OperationState[OperationState["DELAYED"] = 1] = "DELAYED";
  OperationState[OperationState["RUNNING"] = 2] = "RUNNING";
})(OperationState || (OperationState = {}));

var Operation =
/*#__PURE__*/
function () {
  function Operation(computer, successCallback, errorCallback, progressCallback) {
    _classCallCheck(this, Operation);

    this.computer = computer;
    this.successCallback = successCallback;
    this.errorCallback = errorCallback;
    this.progressCallback = progressCallback;

    _defineProperty(this, "task", null);

    _defineProperty(this, "state", OperationState.IDLE);

    _defineProperty(this, "delay", Operation.DEFAULT_DELAY_TIME);

    _defineProperty(this, "timeout", void 0);
  }

  _createClass(Operation, [{
    key: "setDelay",
    value: function setDelay(delay) {
      this.delay = delay;
    }
  }, {
    key: "start",
    value: function start() {
      if (this.state === OperationState.IDLE) {
        this.task = this.computer.compute();
        this.triggerDelay();
      }
    }
  }, {
    key: "triggerDelay",
    value: function triggerDelay() {
      this.cancelDelay();
      this.timeout = setTimeout(this.triggerAsyncTask.bind(this), this.delay);
      this.state = OperationState.DELAYED;
    }
  }, {
    key: "cancel",
    value: function cancel() {
      if (this.state === OperationState.RUNNING) {
        if (this.task) {
          this.task.cancel();
          this.task = null;
        }
      } else if (this.state === OperationState.DELAYED) {
        this.cancelDelay();
      }

      this.state = OperationState.IDLE;
    }
  }, {
    key: "cancelDelay",
    value: function cancelDelay() {
      if (this.timeout) {
        clearTimeout(this.timeout);
        this.timeout = null;
      }
    }
  }, {
    key: "showLoading",
    value: function showLoading() {
      this.progressCallback(this.computer.loadingMessage());
    }
  }, {
    key: "triggerAsyncTask",
    value: function triggerAsyncTask() {
      var _this = this;

      if (this.task) {
        this.state = OperationState.RUNNING;
        var loadingDelay = setTimeout(this.showLoading.bind(this), this.delay);
        var task = this.task;
        task.promise().then(function (result) {
          clearTimeout(loadingDelay);

          if (task === _this.task) {
            _this.successCallback(result);
          }
        }, function (error) {
          clearTimeout(loadingDelay);

          if (task === _this.task) {
            _this.errorCallback(error);
          }
        });
      }
    }
  }]);

  return Operation;
}();

exports.Operation = Operation;

_defineProperty(Operation, "DEFAULT_DELAY_TIME", 300);