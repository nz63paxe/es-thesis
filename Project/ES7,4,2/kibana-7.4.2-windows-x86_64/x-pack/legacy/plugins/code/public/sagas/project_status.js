"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLoadRepoListStatus = watchLoadRepoListStatus;
exports.watchLoadRepoStatus = watchLoadRepoStatus;
exports.watchPollingRepoStatus = watchPollingRepoStatus;
exports.watchResetPollingStatus = watchResetPollingStatus;
exports.watchRepoCloneStatusPolling = watchRepoCloneStatusPolling;
exports.watchRepoIndexStatusPolling = watchRepoIndexStatusPolling;
exports.watchRepoDeleteStatusPolling = watchRepoDeleteStatusPolling;

var _moment = _interopRequireDefault(require("moment"));

var _reduxSaga = require("redux-saga");

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _model = require("../../model");

var ROUTES = _interopRequireWildcard(require("../components/routes"));

var _selectors = require("../selectors");

var _actions = require("../actions");

var _status2 = require("./status");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(loadRepoListStatus),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(loadRepoStatus),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoStatus),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(handleRepoListStatus),
    _marked5 =
/*#__PURE__*/
regeneratorRuntime.mark(triggerPollRepoStatus),
    _marked6 =
/*#__PURE__*/
regeneratorRuntime.mark(handleReposStatusLoaded),
    _marked7 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadRepoListStatus),
    _marked8 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadRepoStatus),
    _marked9 =
/*#__PURE__*/
regeneratorRuntime.mark(watchPollingRepoStatus),
    _marked10 =
/*#__PURE__*/
regeneratorRuntime.mark(handleResetPollingStatus),
    _marked11 =
/*#__PURE__*/
regeneratorRuntime.mark(watchResetPollingStatus),
    _marked12 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoCloneStatusPolling),
    _marked13 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoIndexStatusPolling),
    _marked14 =
/*#__PURE__*/
regeneratorRuntime.mark(watchRepoDeleteStatusPolling);

var REPO_STATUS_POLLING_FREQ_MS = 1000;

function fetchStatus(repoUri) {
  return _new_platform.npStart.core.http.get("/api/code/repo/status/".concat(repoUri));
}

function loadRepoListStatus(repos) {
  var promises, statuses;
  return regeneratorRuntime.wrap(function loadRepoListStatus$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          promises = repos.map(function (repo) {
            return (0, _effects.call)(fetchStatus, repo.uri);
          });
          _context.next = 4;
          return (0, _effects.all)(promises);

        case 4:
          statuses = _context.sent;
          _context.next = 7;
          return (0, _effects.put)((0, _actions.loadStatusSuccess)(statuses.reduce(function (acc, status) {
            acc[status.gitStatus.uri] = status;
            return acc;
          }, {})));

        case 7:
          _context.next = 13;
          break;

        case 9:
          _context.prev = 9;
          _context.t0 = _context["catch"](0);
          _context.next = 13;
          return (0, _effects.put)((0, _actions.loadStatusFailed)(_context.t0));

        case 13:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 9]]);
}

function loadRepoStatus(repo) {
  var repoStatus;
  return regeneratorRuntime.wrap(function loadRepoStatus$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return (0, _effects.call)(fetchStatus, repo.uri);

        case 3:
          repoStatus = _context2.sent;
          _context2.next = 6;
          return (0, _effects.put)((0, _actions.loadStatusSuccess)(_defineProperty({}, repo.uri, repoStatus)));

        case 6:
          _context2.next = 12;
          break;

        case 8:
          _context2.prev = 8;
          _context2.t0 = _context2["catch"](0);
          _context2.next = 12;
          return (0, _effects.put)((0, _actions.loadStatusFailed)(_context2.t0));

        case 12:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2, null, [[0, 8]]);
}

function handleRepoStatus(action) {
  var repository;
  return regeneratorRuntime.wrap(function handleRepoStatus$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          repository = action.payload;
          _context3.next = 3;
          return (0, _effects.call)(loadRepoStatus, repository);

        case 3:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3);
}

function handleRepoListStatus(action) {
  var route, repos;
  return regeneratorRuntime.wrap(function handleRepoListStatus$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.select)(_selectors.routeSelector);

        case 2:
          route = _context4.sent;

          if (!(route.path === ROUTES.ADMIN)) {
            _context4.next = 7;
            break;
          }

          // Only load repository list status in admin page, because in source view
          // page, we also need to load all repositories but not their status.
          repos = action.payload;
          _context4.next = 7;
          return (0, _effects.call)(loadRepoListStatus, repos);

        case 7:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}

function isInProgress(progress) {
  return progress < _model.WorkerReservedProgress.COMPLETED && progress >= _model.WorkerReservedProgress.INIT;
} // Try to trigger the repository status polling based on its current state.


function triggerPollRepoStatus(state, repoUri) {
  return regeneratorRuntime.wrap(function triggerPollRepoStatus$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.t0 = state;
          _context5.next = _context5.t0 === _actions.RepoState.CLONING ? 3 : _context5.t0 === _actions.RepoState.INDEXING ? 6 : _context5.t0 === _actions.RepoState.DELETING ? 9 : 12;
          break;

        case 3:
          _context5.next = 5;
          return (0, _effects.put)((0, _actions.pollRepoCloneStatusStart)(repoUri));

        case 5:
          return _context5.abrupt("break", 13);

        case 6:
          _context5.next = 8;
          return (0, _effects.put)((0, _actions.pollRepoIndexStatusStart)(repoUri));

        case 8:
          return _context5.abrupt("break", 13);

        case 9:
          _context5.next = 11;
          return (0, _effects.put)((0, _actions.pollRepoDeleteStatusStart)(repoUri));

        case 11:
          return _context5.abrupt("break", 13);

        case 12:
          return _context5.abrupt("break", 13);

        case 13:
        case "end":
          return _context5.stop();
      }
    }
  }, _marked5);
}

function handleReposStatusLoaded(action) {
  var route, allStatuses, _i, _Object$keys, repoUri, status, currentUri, _status;

  return regeneratorRuntime.wrap(function handleReposStatusLoaded$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return (0, _effects.select)(_selectors.routeSelector);

        case 2:
          route = _context6.sent;
          _context6.next = 5;
          return (0, _effects.select)(_selectors.allStatusSelector);

        case 5:
          allStatuses = _context6.sent;

          if (!(route.path === ROUTES.ADMIN)) {
            _context6.next = 18;
            break;
          }

          _i = 0, _Object$keys = Object.keys(allStatuses);

        case 8:
          if (!(_i < _Object$keys.length)) {
            _context6.next = 16;
            break;
          }

          repoUri = _Object$keys[_i];
          status = allStatuses[repoUri];
          _context6.next = 13;
          return triggerPollRepoStatus(status.state, repoUri);

        case 13:
          _i++;
          _context6.next = 8;
          break;

        case 16:
          _context6.next = 26;
          break;

        case 18:
          if (!(route.path === ROUTES.MAIN || route.path === ROUTES.MAIN_ROOT)) {
            _context6.next = 26;
            break;
          }

          _context6.next = 21;
          return (0, _effects.select)(_selectors.repoUriSelector);

        case 21:
          currentUri = _context6.sent;
          _status = allStatuses[currentUri];

          if (!_status) {
            _context6.next = 26;
            break;
          }

          _context6.next = 26;
          return triggerPollRepoStatus(_status.state, currentUri);

        case 26:
        case "end":
          return _context6.stop();
      }
    }
  }, _marked6);
}

function watchLoadRepoListStatus() {
  return regeneratorRuntime.wrap(function watchLoadRepoListStatus$(_context7) {
    while (1) {
      switch (_context7.prev = _context7.next) {
        case 0:
          _context7.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchReposSuccess), handleRepoListStatus);

        case 2:
        case "end":
          return _context7.stop();
      }
    }
  }, _marked7);
}

function watchLoadRepoStatus() {
  return regeneratorRuntime.wrap(function watchLoadRepoStatus$(_context8) {
    while (1) {
      switch (_context8.prev = _context8.next) {
        case 0:
          _context8.next = 2;
          return (0, _effects.takeLatest)(String(_actions.loadRepoSuccess), handleRepoStatus);

        case 2:
        case "end":
          return _context8.stop();
      }
    }
  }, _marked8);
}

function watchPollingRepoStatus() {
  return regeneratorRuntime.wrap(function watchPollingRepoStatus$(_context9) {
    while (1) {
      switch (_context9.prev = _context9.next) {
        case 0:
          _context9.next = 2;
          return (0, _effects.takeEvery)(String(_actions.loadStatusSuccess), handleReposStatusLoaded);

        case 2:
        case "end":
          return _context9.stop();
      }
    }
  }, _marked9);
}

function handleResetPollingStatus(action) {
  var statuses, _i2, _Object$keys2, _repoUri;

  return regeneratorRuntime.wrap(function handleResetPollingStatus$(_context10) {
    while (1) {
      switch (_context10.prev = _context10.next) {
        case 0:
          _context10.next = 2;
          return (0, _effects.select)(_selectors.allStatusSelector);

        case 2:
          statuses = _context10.sent;
          _i2 = 0, _Object$keys2 = Object.keys(statuses);

        case 4:
          if (!(_i2 < _Object$keys2.length)) {
            _context10.next = 15;
            break;
          }

          _repoUri = _Object$keys2[_i2];
          _context10.next = 8;
          return (0, _effects.put)((0, _actions.pollRepoCloneStatusStop)(_repoUri));

        case 8:
          _context10.next = 10;
          return (0, _effects.put)((0, _actions.pollRepoIndexStatusStop)(_repoUri));

        case 10:
          _context10.next = 12;
          return (0, _effects.put)((0, _actions.pollRepoDeleteStatusStop)(_repoUri));

        case 12:
          _i2++;
          _context10.next = 4;
          break;

        case 15:
        case "end":
          return _context10.stop();
      }
    }
  }, _marked10);
}

function watchResetPollingStatus() {
  return regeneratorRuntime.wrap(function watchResetPollingStatus$(_context11) {
    while (1) {
      switch (_context11.prev = _context11.next) {
        case 0:
          _context11.next = 2;
          return (0, _effects.takeEvery)(_actions.routeChange, handleResetPollingStatus);

        case 2:
        case "end":
          return _context11.stop();
      }
    }
  }, _marked11);
}

var parseCloneStatusPollingRequest = function parseCloneStatusPollingRequest(action) {
  if (action.type === String(_actions.importRepoSuccess)) {
    return action.payload.uri;
  } else if (action.type === String(_actions.pollRepoCloneStatusStart)) {
    return action.payload;
  }
};

var handleRepoCloneStatusProcess =
/*#__PURE__*/
regeneratorRuntime.mark(function handleRepoCloneStatusProcess(status, repoUri) {
  var _status$gitStatus, progress, cloneProgress, errorMessage, timestamp;

  return regeneratorRuntime.wrap(function handleRepoCloneStatusProcess$(_context12) {
    while (1) {
      switch (_context12.prev = _context12.next) {
        case 0:
          if (!( // Repository has been deleted during the clone
          !status.gitStatus && !status.indexStatus && !status.deleteStatus || // Repository is in delete during the clone
          status.deleteStatus)) {
            _context12.next = 2;
            break;
          }

          return _context12.abrupt("return", false);

        case 2:
          if (!status.gitStatus) {
            _context12.next = 9;
            break;
          }

          _status$gitStatus = status.gitStatus, progress = _status$gitStatus.progress, cloneProgress = _status$gitStatus.cloneProgress, errorMessage = _status$gitStatus.errorMessage, timestamp = _status$gitStatus.timestamp;
          _context12.next = 6;
          return (0, _effects.put)((0, _actions.updateCloneProgress)({
            progress: progress,
            timestamp: (0, _moment.default)(timestamp).toDate(),
            uri: repoUri,
            errorMessage: errorMessage,
            cloneProgress: cloneProgress
          }));

        case 6:
          return _context12.abrupt("return", isInProgress(progress));

        case 9:
          return _context12.abrupt("return", true);

        case 10:
        case "end":
          return _context12.stop();
      }
    }
  }, handleRepoCloneStatusProcess);
});

function watchRepoCloneStatusPolling() {
  return regeneratorRuntime.wrap(function watchRepoCloneStatusPolling$(_context13) {
    while (1) {
      switch (_context13.prev = _context13.next) {
        case 0:
          _context13.next = 2;
          return (0, _effects.takeEvery)([String(_actions.importRepoSuccess), String(_actions.pollRepoCloneStatusStart)], pollRepoCloneStatusRunner);

        case 2:
        case "end":
          return _context13.stop();
      }
    }
  }, _marked12);
}

var parseIndexStatusPollingRequest = function parseIndexStatusPollingRequest(action) {
  if (action.type === String(_actions.indexRepo) || action.type === String(_actions.pollRepoIndexStatusStart)) {
    return action.payload;
  } else if (action.type === String(_actions.updateCloneProgress)) {
    return action.payload.uri;
  }
};

var handleRepoIndexStatusProcess =
/*#__PURE__*/
regeneratorRuntime.mark(function handleRepoIndexStatusProcess(status, repoUri) {
  return regeneratorRuntime.wrap(function handleRepoIndexStatusProcess$(_context14) {
    while (1) {
      switch (_context14.prev = _context14.next) {
        case 0:
          if (!( // Repository has been deleted during the index
          !status.gitStatus && !status.indexStatus && !status.deleteStatus || // Repository is in delete during the index
          status.deleteStatus)) {
            _context14.next = 2;
            break;
          }

          return _context14.abrupt("return", false);

        case 2:
          if (!status.indexStatus) {
            _context14.next = 8;
            break;
          }

          _context14.next = 5;
          return (0, _effects.put)((0, _actions.updateIndexProgress)({
            progress: status.indexStatus.progress,
            timestamp: (0, _moment.default)(status.indexStatus.timestamp).toDate(),
            uri: repoUri
          }));

        case 5:
          return _context14.abrupt("return", isInProgress(status.indexStatus.progress));

        case 8:
          return _context14.abrupt("return", true);

        case 9:
        case "end":
          return _context14.stop();
      }
    }
  }, handleRepoIndexStatusProcess);
});

function watchRepoIndexStatusPolling() {
  return regeneratorRuntime.wrap(function watchRepoIndexStatusPolling$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          _context15.next = 2;
          return (0, _effects.takeEvery)([String(_actions.indexRepo), _status2.cloneCompletedPattern, String(_actions.pollRepoIndexStatusStart)], pollRepoIndexStatusRunner);

        case 2:
        case "end":
          return _context15.stop();
      }
    }
  }, _marked13);
}

var parseDeleteStatusPollingRequest = function parseDeleteStatusPollingRequest(action) {
  return action.payload;
};

var handleRepoDeleteStatusProcess =
/*#__PURE__*/
regeneratorRuntime.mark(function handleRepoDeleteStatusProcess(status, repoUri) {
  return regeneratorRuntime.wrap(function handleRepoDeleteStatusProcess$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          if (!(!status.gitStatus && !status.indexStatus && !status.deleteStatus)) {
            _context16.next = 4;
            break;
          }

          _context16.next = 3;
          return (0, _effects.put)((0, _actions.updateDeleteProgress)({
            progress: _model.WorkerReservedProgress.COMPLETED,
            uri: repoUri
          }));

        case 3:
          return _context16.abrupt("return", false);

        case 4:
          if (!status.deleteStatus) {
            _context16.next = 10;
            break;
          }

          _context16.next = 7;
          return (0, _effects.put)((0, _actions.updateDeleteProgress)({
            progress: status.deleteStatus.progress,
            timestamp: (0, _moment.default)(status.deleteStatus.timestamp).toDate(),
            uri: repoUri
          }));

        case 7:
          return _context16.abrupt("return", isInProgress(status.deleteStatus.progress));

        case 10:
          return _context16.abrupt("return", true);

        case 11:
        case "end":
          return _context16.stop();
      }
    }
  }, handleRepoDeleteStatusProcess);
});

function watchRepoDeleteStatusPolling() {
  return regeneratorRuntime.wrap(function watchRepoDeleteStatusPolling$(_context17) {
    while (1) {
      switch (_context17.prev = _context17.next) {
        case 0:
          _context17.next = 2;
          return (0, _effects.takeEvery)([String(_actions.deleteRepo), String(_actions.pollRepoDeleteStatusStart)], pollRepoDeleteStatusRunner);

        case 2:
        case "end":
          return _context17.stop();
      }
    }
  }, _marked14);
}

function createRepoStatusPollingRun(handleStatus, pollingStopActionFunction) {
  return (
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(repoUri) {
      var _repoStatus, keepPolling;

      return regeneratorRuntime.wrap(function _callee$(_context18) {
        while (1) {
          switch (_context18.prev = _context18.next) {
            case 0:
              _context18.prev = 0;

            case 1:
              if (!true) {
                _context18.next = 15;
                break;
              }

              _context18.next = 4;
              return (0, _effects.call)(_reduxSaga.delay, REPO_STATUS_POLLING_FREQ_MS);

            case 4:
              _context18.next = 6;
              return (0, _effects.call)(fetchStatus, repoUri);

            case 6:
              _repoStatus = _context18.sent;
              _context18.next = 9;
              return handleStatus(_repoStatus, repoUri);

            case 9:
              keepPolling = _context18.sent;

              if (keepPolling) {
                _context18.next = 13;
                break;
              }

              _context18.next = 13;
              return (0, _effects.put)(pollingStopActionFunction(repoUri));

            case 13:
              _context18.next = 1;
              break;

            case 15:
              _context18.prev = 15;
              _context18.next = 18;
              return (0, _effects.cancelled)();

            case 18:
              if (!_context18.sent) {
                _context18.next = 19;
                break;
              }

            case 19:
              return _context18.finish(15);

            case 20:
            case "end":
              return _context18.stop();
          }
        }
      }, _callee, null, [[0,, 15, 20]]);
    })
  );
}

function createRepoStatusPollingRunner(parseRepoUri, pollStatusRun, pollingStopActionFunction, pollingStopActionFunctionPattern) {
  return (
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(action) {
      var repoUri, task;
      return regeneratorRuntime.wrap(function _callee2$(_context19) {
        while (1) {
          switch (_context19.prev = _context19.next) {
            case 0:
              repoUri = parseRepoUri(action); // Cancel existing runner to deduplicate the polling

              _context19.next = 3;
              return (0, _effects.put)(pollingStopActionFunction(repoUri));

            case 3:
              _context19.next = 5;
              return (0, _effects.fork)(pollStatusRun, repoUri);

            case 5:
              task = _context19.sent;
              _context19.next = 8;
              return (0, _effects.take)(pollingStopActionFunctionPattern(repoUri));

            case 8:
              _context19.next = 10;
              return (0, _effects.cancel)(task);

            case 10:
            case "end":
              return _context19.stop();
          }
        }
      }, _callee2);
    })
  );
}

var runPollRepoCloneStatus = createRepoStatusPollingRun(handleRepoCloneStatusProcess, _actions.pollRepoCloneStatusStop);
var runPollRepoIndexStatus = createRepoStatusPollingRun(handleRepoIndexStatusProcess, _actions.pollRepoIndexStatusStop);
var runPollRepoDeleteStatus = createRepoStatusPollingRun(handleRepoDeleteStatusProcess, _actions.pollRepoDeleteStatusStop);
var pollRepoCloneStatusRunner = createRepoStatusPollingRunner(parseCloneStatusPollingRequest, runPollRepoCloneStatus, _actions.pollRepoCloneStatusStop, _status2.cloneRepoStatusPollingStopPattern);
var pollRepoIndexStatusRunner = createRepoStatusPollingRunner(parseIndexStatusPollingRequest, runPollRepoIndexStatus, _actions.pollRepoIndexStatusStop, _status2.indexRepoStatusPollingStopPattern);
var pollRepoDeleteStatusRunner = createRepoStatusPollingRunner(parseDeleteStatusPollingRequest, runPollRepoDeleteStatus, _actions.pollRepoDeleteStatusStop, _status2.deleteRepoStatusPollingStopPattern);