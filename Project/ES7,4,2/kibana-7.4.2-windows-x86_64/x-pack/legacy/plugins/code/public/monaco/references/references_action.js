"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerReferencesAction = registerReferencesAction;

var _querystring = _interopRequireDefault(require("querystring"));

var _url = _interopRequireDefault(require("url"));

var _uri_util = require("../../../common/uri_util");

var _url2 = require("../../utils/url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function registerReferencesAction(e, getUrlQuery) {
  e.addAction({
    id: 'editor.action.referenceSearch.trigger',
    label: 'Find All References',
    contextMenuGroupId: 'navigation',
    contextMenuOrder: 1.5,
    run: function run(ed) {
      var position = ed.getPosition();
      var model = ed.getModel();

      if (model && position) {
        var _parseSchema = (0, _uri_util.parseSchema)(model.uri.toString()),
            uri = _parseSchema.uri;

        var refUrl = "git:/".concat(uri, "!L").concat(position.lineNumber - 1, ":").concat(position.column - 1);

        var queries = _url.default.parse(getUrlQuery(), true).query;

        var query = _querystring.default.stringify(_objectSpread({}, queries, {
          tab: 'references',
          refUrl: refUrl
        }));

        _url2.history.push("".concat(uri, "?").concat(query));
      }
    }
  });
}