"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProjectTab = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _capabilities = require("ui/capabilities");

var _actions = require("../../actions");

var _url = require("../../utils/url");

var _project_item = require("./project_item");

var _project_settings = require("./project_settings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SortOptionsValue;

(function (SortOptionsValue) {
  SortOptionsValue["AlphabeticalAsc"] = "alphabetical_asc";
  SortOptionsValue["AlphabeticalDesc"] = "alphabetical_desc";
  SortOptionsValue["UpdatedAsc"] = "updated_asc";
  SortOptionsValue["UpdatedDesc"] = "updated_desc";
  SortOptionsValue["RecentlyAdded"] = "recently_added";
})(SortOptionsValue || (SortOptionsValue = {}));

var sortFunctionsFactory = function sortFunctionsFactory(status) {
  var _sortFunctions;

  var sortFunctions = (_sortFunctions = {}, _defineProperty(_sortFunctions, SortOptionsValue.AlphabeticalAsc, function (a, b) {
    return a.name.localeCompare(b.name);
  }), _defineProperty(_sortFunctions, SortOptionsValue.AlphabeticalDesc, function (a, b) {
    return b.name.localeCompare(a.name);
  }), _defineProperty(_sortFunctions, SortOptionsValue.UpdatedAsc, function (a, b) {
    return (0, _moment.default)(status[b.uri].timestamp).diff((0, _moment.default)(status[a.uri].timestamp));
  }), _defineProperty(_sortFunctions, SortOptionsValue.UpdatedDesc, function (a, b) {
    return (0, _moment.default)(status[a.uri].timestamp).diff((0, _moment.default)(status[b.uri].timestamp));
  }), _defineProperty(_sortFunctions, SortOptionsValue.RecentlyAdded, function () {
    return -1;
  }), _sortFunctions);
  return sortFunctions;
};

var sortOptions = [{
  value: SortOptionsValue.AlphabeticalAsc,
  inputDisplay: _i18n.i18n.translate('xpack.code.adminPage.repoTab.sort.aToZDropDownOptionLabel', {
    defaultMessage: 'A to Z'
  })
}, {
  value: SortOptionsValue.AlphabeticalDesc,
  inputDisplay: _i18n.i18n.translate('xpack.code.adminPage.repoTab.sort.zToADropDownOptionLabel', {
    defaultMessage: 'Z to A'
  })
}, {
  value: SortOptionsValue.UpdatedAsc,
  inputDisplay: _i18n.i18n.translate('xpack.code.adminPage.repoTab.sort.updatedAscDropDownOptionLabel', {
    defaultMessage: 'Last Updated ASC'
  })
}, {
  value: SortOptionsValue.UpdatedDesc,
  inputDisplay: _i18n.i18n.translate('xpack.code.adminPage.repoTab.sort.updatedDescDropDownOptionLabel', {
    defaultMessage: 'Last Updated DESC'
  })
}];

var CodeProjectTab =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeProjectTab, _React$PureComponent);

  _createClass(CodeProjectTab, null, [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (state.importLoading && !props.importLoading) {
        return {
          showImportProjectModal: false,
          importLoading: props.importLoading,
          repoURL: ''
        };
      }

      return {
        importLoading: props.importLoading
      };
    }
  }]);

  function CodeProjectTab(props) {
    var _this;

    _classCallCheck(this, CodeProjectTab);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CodeProjectTab).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "closeModal", function () {
      _this.setState({
        showImportProjectModal: false,
        repoURL: '',
        isInvalid: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "openModal", function () {
      _this.setState({
        showImportProjectModal: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "openSettingModal", function (uri, url) {
      _this.setState({
        settingModal: {
          uri: uri,
          url: url,
          show: true
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closeSettingModal", function () {
      _this.setState({
        settingModal: {
          show: false
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (e) {
      _this.setState({
        repoURL: e.target.value,
        isInvalid: (0, _url.isImportRepositoryURLInvalid)(e.target.value)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "submitImportProject", function () {
      if (!(0, _url.isImportRepositoryURLInvalid)(_this.state.repoURL)) {
        _this.props.importRepo(_this.state.repoURL);
      } else if (!_this.state.isInvalid) {
        _this.setState({
          isInvalid: true
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "updateIsInvalid", function () {
      _this.setState({
        isInvalid: (0, _url.isImportRepositoryURLInvalid)(_this.state.repoURL)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "renderImportModal", function () {
      return _react2.default.createElement(_eui.EuiOverlayMask, null, _react2.default.createElement(_eui.EuiModal, {
        onClose: _this.closeModal
      }, _react2.default.createElement(_eui.EuiModalHeader, null, _react2.default.createElement(_eui.EuiModalHeaderTitle, null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.importRepoTitle",
        defaultMessage: "Import a new repo"
      }))), _react2.default.createElement(_eui.EuiModalBody, null, _react2.default.createElement(_eui.EuiTitle, {
        size: "xs"
      }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.repositoryUrlFormLabel",
        defaultMessage: "Repository URL"
      }))), _react2.default.createElement(_eui.EuiForm, null, _react2.default.createElement(_eui.EuiFormRow, {
        isInvalid: _this.state.isInvalid,
        error: _i18n.i18n.translate('xpack.code.adminPage.repoTab.repositoryUrlEmptyText', {
          defaultMessage: "The URL shouldn't be empty."
        })
      }, _react2.default.createElement(_eui.EuiFieldText, {
        value: _this.state.repoURL,
        onChange: _this.onChange,
        onBlur: _this.updateIsInvalid,
        placeholder: "https://github.com/Microsoft/TypeScript-Node-Starter",
        "aria-label": "input project url",
        "data-test-subj": "importRepositoryUrlInputBox",
        isLoading: _this.props.importLoading,
        fullWidth: true,
        isInvalid: _this.state.isInvalid,
        autoFocus: true
      })))), _react2.default.createElement(_eui.EuiModalFooter, null, _react2.default.createElement(_eui.EuiButtonEmpty, {
        onClick: _this.closeModal
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.cancelButtonLabel",
        defaultMessage: "Cancel"
      })), _react2.default.createElement(_eui.EuiButton, {
        fill: true,
        onClick: _this.submitImportProject,
        disabled: _this.props.importLoading
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.importButtonLabel",
        defaultMessage: "Import"
      })))));
    });

    _defineProperty(_assertThisInitialized(_this), "setSortOption", function (value) {
      _this.setState({
        sortOption: value
      });
    });

    _this.state = {
      importLoading: false,
      showImportProjectModal: false,
      settingModal: {
        show: false
      },
      repoURL: '',
      sortOption: SortOptionsValue.AlphabeticalAsc,
      isInvalid: false
    };
    return _this;
  }

  _createClass(CodeProjectTab, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          projects = _this$props.projects,
          status = _this$props.status,
          toastMessage = _this$props.toastMessage,
          showToast = _this$props.showToast,
          toastType = _this$props.toastType;
      var projectsCount = projects.length;
      var modal = this.state.showImportProjectModal && this.renderImportModal();
      var sortedProjects = projects.sort(sortFunctionsFactory(status)[this.state.sortOption]);
      var repoList = sortedProjects.map(function (repo) {
        return _react2.default.createElement(_project_item.ProjectItem, {
          openSettings: _this2.openSettingModal,
          key: repo.uri,
          project: repo,
          showStatus: true,
          status: status[repo.uri],
          enableManagement: _capabilities.capabilities.get().code.admin
        });
      });
      var settings = null;

      if (this.state.settingModal.show) {
        settings = _react2.default.createElement(_project_settings.ProjectSettings, {
          onClose: this.closeSettingModal,
          repoUri: this.state.settingModal.uri,
          url: this.state.settingModal.url
        });
      }

      return _react2.default.createElement("div", {
        className: "code-sidebar",
        "data-test-subj": "codeRepositoryList"
      }, showToast && _react2.default.createElement(_eui.EuiGlobalToastList, {
        toasts: [{
          title: '',
          color: toastType,
          text: toastMessage,
          id: toastMessage || ''
        }],
        dismissToast: this.props.closeToast,
        toastLifeTimeMs: 6000
      }), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFormRow, {
        label: _i18n.i18n.translate('xpack.code.adminPage.repoTab.sort.sortByFormLabel', {
          defaultMessage: 'Sort By'
        })
      }, _react2.default.createElement(_eui.EuiSuperSelect, {
        options: sortOptions,
        valueOfSelected: this.state.sortOption,
        onChange: this.setSortOption
      }))), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: true
      }), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: true
      }), _react2.default.createElement(_eui.EuiFlexItem, null, _capabilities.capabilities.get().code.admin && // @ts-ignore
      _react2.default.createElement(_eui.EuiButton, {
        className: "codeButton__projectImport",
        onClick: this.openModal,
        "data-test-subj": "newProjectButton"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.importRepoButtonLabel",
        defaultMessage: "Import a new repo"
      })))), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.repoDescription",
        defaultMessage: "{projectsCount} {projectsCount, plural, one {Repo} other {Repos}}",
        values: {
          projectsCount: projectsCount
        }
      }))), _react2.default.createElement(_eui.EuiSpacer, null), repoList, modal, settings);
    }
  }]);

  return CodeProjectTab;
}(_react2.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return {
    projects: state.repositoryManagement.repositories,
    status: state.status.status,
    importLoading: state.repositoryManagement.importLoading,
    toastMessage: state.repositoryManagement.toastMessage,
    toastType: state.repositoryManagement.toastType,
    showToast: state.repositoryManagement.showToast
  };
};

var mapDispatchToProps = {
  importRepo: _actions.importRepo,
  closeToast: _actions.closeToast
};
var ProjectTab = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(CodeProjectTab);
exports.ProjectTab = ProjectTab;