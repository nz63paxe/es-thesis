"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HoverController = void 0;

var _monaco = require("../monaco");

var _content_hover_widget = require("./content_hover_widget");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HoverController =
/*#__PURE__*/
function () {
  _createClass(HoverController, null, [{
    key: "get",
    value: function get(editor) {
      return editor.getContribution(HoverController.ID);
    }
  }]);

  function HoverController(editor) {
    var _this = this;

    _classCallCheck(this, HoverController);

    this.editor = editor;

    _defineProperty(this, "contentWidget", void 0);

    _defineProperty(this, "disposables", void 0);

    this.disposables = [this.editor.onMouseMove(function (e) {
      return _this.onEditorMouseMove(e);
    }), this.editor.onKeyDown(function (e) {
      return _this.onKeyDown(e);
    })];
    this.contentWidget = new _content_hover_widget.ContentHoverWidget(editor);
  }

  _createClass(HoverController, [{
    key: "dispose",
    value: function dispose() {
      this.disposables.forEach(function (d) {
        return d.dispose();
      });
    }
  }, {
    key: "getId",
    value: function getId() {
      return HoverController.ID;
    }
  }, {
    key: "setReduxActions",
    value: function setReduxActions(actions) {
      this.contentWidget.setHoverResultAction(actions.hoverResult);
    }
  }, {
    key: "onEditorMouseMove",
    value: function onEditorMouseMove(mouseEvent) {
      var targetType = mouseEvent.target.type;
      var MouseTargetType = _monaco.monaco.editor.MouseTargetType;

      if (targetType === MouseTargetType.CONTENT_WIDGET && mouseEvent.target.detail === _content_hover_widget.ContentHoverWidget.ID) {
        return;
      }

      if (targetType === MouseTargetType.CONTENT_TEXT) {
        this.contentWidget.startShowingAt(mouseEvent.target.range, false);
      } else {
        this.contentWidget.hide();
      }
    }
  }, {
    key: "onKeyDown",
    value: function onKeyDown(e) {
      if (e.keyCode === _monaco.monaco.KeyCode.Escape) {
        // Do not hide hover when Ctrl/Meta is pressed
        this.contentWidget.hide();
      }
    }
  }]);

  return HoverController;
}();

exports.HoverController = HoverController;

_defineProperty(HoverController, "ID", 'code.editor.contrib.hover');