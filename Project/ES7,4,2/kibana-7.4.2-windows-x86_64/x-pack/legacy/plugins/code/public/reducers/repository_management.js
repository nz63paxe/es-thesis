"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.repositoryManagement = exports.ToastType = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _i18n = require("@kbn/i18n");

var _actions = require("../actions");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var ToastType;
exports.ToastType = ToastType;

(function (ToastType) {
  ToastType["danger"] = "danger";
  ToastType["success"] = "success";
  ToastType["warning"] = "warning";
})(ToastType || (exports.ToastType = ToastType = {}));

var initialState = {
  repositories: [],
  repoLangseverConfigs: {},
  loading: false,
  importLoading: false,
  showToast: false
};
var repositoryManagement = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.fetchRepos), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
  });
}), _defineProperty(_handleActions, String(_actions.fetchReposSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = false;
    draft.repositories = action.payload || [];
  });
}), _defineProperty(_handleActions, String(_actions.fetchReposFailed), function (state, action) {
  if (action.payload) {
    return (0, _immer.default)(state, function (draft) {
      draft.error = action.payload;
      draft.loading = false;
    });
  } else {
    return state;
  }
}), _defineProperty(_handleActions, String(_actions.deleteRepoFinished), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.repositories = state.repositories.filter(function (repo) {
      return repo.uri !== action.payload;
    });
  });
}), _defineProperty(_handleActions, String(_actions.importRepo), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.importLoading = true;
  });
}), _defineProperty(_handleActions, String(_actions.importRepoSuccess), function (state, action) {
  return (// TODO is it possible and how to deal with action.payload === undefined?
    (0, _immer.default)(state, function (draft) {
      draft.importLoading = false;
      draft.showToast = true;
      draft.toastType = ToastType.success;
      draft.toastMessage = _i18n.i18n.translate('xpack.code.repositoryManagement.repoSubmittedMessage', {
        defaultMessage: '{name} has been successfully submitted!',
        values: {
          name: action.payload.name
        }
      });
      draft.repositories = [].concat(_toConsumableArray(state.repositories), [action.payload]);
    })
  );
}), _defineProperty(_handleActions, String(_actions.importRepoFailed), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload) {
      if (action.payload.response && action.payload.response.status === 304) {
        draft.toastMessage = _i18n.i18n.translate('xpack.code.repositoryManagement.repoImportedMessage', {
          defaultMessage: 'This Repository has already been imported!'
        });
        draft.showToast = true;
        draft.toastType = ToastType.warning;
        draft.importLoading = false;
      } else {
        // TODO add localication for those messages
        draft.toastMessage = action.payload.body.message;
        draft.showToast = true;
        draft.toastType = ToastType.danger;
        draft.importLoading = false;
      }
    }
  });
}), _defineProperty(_handleActions, String(_actions.closeToast), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.showToast = false;
  });
}), _defineProperty(_handleActions, String(_actions.fetchRepoConfigSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.repoConfigs = action.payload;
  });
}), _defineProperty(_handleActions, String(_actions.loadConfigsSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.repoLangseverConfigs = action.payload;
  });
}), _handleActions), initialState);
exports.repositoryManagement = repositoryManagement;