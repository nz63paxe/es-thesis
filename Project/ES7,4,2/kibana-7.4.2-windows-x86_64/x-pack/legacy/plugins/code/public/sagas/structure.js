"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchLoadStructure = watchLoadStructure;

var _effects = require("redux-saga/effects");

var _lsp_client = require("../../common/lsp_client");

var _actions = require("../actions");

var _repo_file_status = require("../../common/repo_file_status");

var _uri_util = require("../../common/uri_util");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(statusChanged),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadStructure),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(fetchSymbols);

var sortSymbol = function sortSymbol(a, b) {
  var lineDiff = a.range.start.line - b.range.start.line;

  if (lineDiff === 0) {
    return a.range.start.character - b.range.start.character;
  } else {
    return lineDiff;
  }
};

var generateStructureTree = function generateStructureTree(documentSymbol, path) {
  var currentPath = path ? "".concat(path, "/").concat(documentSymbol.name) : documentSymbol.name;
  var structureTree = {
    name: documentSymbol.name,
    kind: documentSymbol.kind,
    path: currentPath,
    range: documentSymbol.range,
    selectionRange: documentSymbol.selectionRange
  };

  if (documentSymbol.children) {
    structureTree.members = documentSymbol.children.sort(sortSymbol).map(function (ds) {
      return generateStructureTree(ds, currentPath);
    });
  }

  return structureTree;
};

function requestStructure(uri) {
  var lspClient = new _lsp_client.LspRestClient('/api/code/lsp');
  var lspMethods = new _lsp_client.TextDocumentMethods(lspClient);
  return lspMethods.documentSymbol.send({
    textDocument: {
      uri: uri || ''
    }
  });
}

function statusChanged(action) {
  var _action$payload, prevStatus, currentStatus, _ref, revision, uri, _path, u;

  return regeneratorRuntime.wrap(function statusChanged$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _action$payload = action.payload, prevStatus = _action$payload.prevStatus, currentStatus = _action$payload.currentStatus;

          if (!(prevStatus && prevStatus.langServerStatus === _repo_file_status.RepoFileStatus.LANG_SERVER_IS_INITIALIZING && currentStatus.langServerStatus !== _repo_file_status.RepoFileStatus.LANG_SERVER_IS_INITIALIZING)) {
            _context.next = 11;
            break;
          }

          _context.next = 4;
          return (0, _effects.select)(function (state) {
            return state.status.currentStatusPath;
          });

        case 4:
          _ref = _context.sent;
          revision = _ref.revision;
          uri = _ref.uri;
          _path = _ref.path;
          u = (0, _uri_util.toCanonicalUrl)({
            repoUri: uri,
            file: _path,
            revision: revision
          });
          _context.next = 11;
          return (0, _effects.call)(fetchSymbols, (0, _actions.loadStructure)(u));

        case 11:
        case "end":
          return _context.stop();
      }
    }
  }, _marked);
}

function watchLoadStructure() {
  return regeneratorRuntime.wrap(function watchLoadStructure$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(String(_actions.loadStructure), fetchSymbols);

        case 2:
          _context2.next = 4;
          return (0, _effects.takeEvery)(String(_actions.StatusChanged), statusChanged);

        case 4:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function fetchSymbols(action) {
  var data, structureTree;
  return regeneratorRuntime.wrap(function fetchSymbols$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return (0, _effects.call)(requestStructure, "git:/".concat(action.payload));

        case 3:
          data = _context3.sent;
          structureTree = data.sort(sortSymbol).map(function (ds) {
            return generateStructureTree(ds, '');
          });
          _context3.next = 7;
          return (0, _effects.put)((0, _actions.loadStructureSuccess)({
            path: action.payload,
            data: data,
            structureTree: structureTree
          }));

        case 7:
          _context3.next = 13;
          break;

        case 9:
          _context3.prev = 9;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 13;
          return (0, _effects.put)((0, _actions.loadStructureFailed)(_context3.t0));

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 9]]);
}