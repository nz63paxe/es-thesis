"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.searchReposForScopeFailed = exports.searchReposForScopeSuccess = exports.searchReposForScope = exports.turnOffDefaultRepoScope = exports.turnOnDefaultRepoScope = exports.saveSearchOptions = exports.repositoryTypeaheadSearchFailed = exports.repositoryTypeaheadSearchSuccess = exports.repositorySearchQueryChanged = exports.suggestionSearch = exports.changeSearchScope = exports.repositorySearchFailed = exports.repositorySearchSuccess = exports.repositorySearch = exports.documentSearchFailed = exports.documentSearchSuccess = exports.documentSearch = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// For document search page
var documentSearch = (0, _reduxActions.createAction)('DOCUMENT SEARCH');
exports.documentSearch = documentSearch;
var documentSearchSuccess = (0, _reduxActions.createAction)('DOCUMENT SEARCH SUCCESS');
exports.documentSearchSuccess = documentSearchSuccess;
var documentSearchFailed = (0, _reduxActions.createAction)('DOCUMENT SEARCH FAILED'); // For repository search page

exports.documentSearchFailed = documentSearchFailed;
var repositorySearch = (0, _reduxActions.createAction)('REPOSITORY SEARCH');
exports.repositorySearch = repositorySearch;
var repositorySearchSuccess = (0, _reduxActions.createAction)('REPOSITORY SEARCH SUCCESS');
exports.repositorySearchSuccess = repositorySearchSuccess;
var repositorySearchFailed = (0, _reduxActions.createAction)('REPOSITORY SEARCH FAILED');
exports.repositorySearchFailed = repositorySearchFailed;
var changeSearchScope = (0, _reduxActions.createAction)('CHANGE SEARCH SCOPE');
exports.changeSearchScope = changeSearchScope;
var suggestionSearch = (0, _reduxActions.createAction)('SUGGESTION SEARCH'); // For repository search typeahead

exports.suggestionSearch = suggestionSearch;
var repositorySearchQueryChanged = (0, _reduxActions.createAction)('REPOSITORY SEARCH QUERY CHANGED');
exports.repositorySearchQueryChanged = repositorySearchQueryChanged;
var repositoryTypeaheadSearchSuccess = (0, _reduxActions.createAction)('REPOSITORY SEARCH SUCCESS');
exports.repositoryTypeaheadSearchSuccess = repositoryTypeaheadSearchSuccess;
var repositoryTypeaheadSearchFailed = (0, _reduxActions.createAction)('REPOSITORY SEARCH FAILED');
exports.repositoryTypeaheadSearchFailed = repositoryTypeaheadSearchFailed;
var saveSearchOptions = (0, _reduxActions.createAction)('SAVE SEARCH OPTIONS');
exports.saveSearchOptions = saveSearchOptions;
var turnOnDefaultRepoScope = (0, _reduxActions.createAction)('TURN ON DEFAULT REPO SCOPE');
exports.turnOnDefaultRepoScope = turnOnDefaultRepoScope;
var turnOffDefaultRepoScope = (0, _reduxActions.createAction)('TURN OFF DEFAULT REPO SCOPE');
exports.turnOffDefaultRepoScope = turnOffDefaultRepoScope;
var searchReposForScope = (0, _reduxActions.createAction)('SEARCH REPOS FOR SCOPE');
exports.searchReposForScope = searchReposForScope;
var searchReposForScopeSuccess = (0, _reduxActions.createAction)('SEARCH REPOS FOR SCOPE SUCCESS');
exports.searchReposForScopeSuccess = searchReposForScopeSuccess;
var searchReposForScopeFailed = (0, _reduxActions.createAction)('SEARCH REPOS FOR SCOPE FAILED');
exports.searchReposForScopeFailed = searchReposForScopeFailed;