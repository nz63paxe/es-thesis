"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.symbol = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

var _language_server = require("../actions/language_server");

var _route = require("../actions/route");

var _handleActions;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  symbols: {},
  loading: false,
  structureTree: {},
  closedPaths: [],
  languageServerInitializing: false
};

var clearState = function clearState(state) {
  return (0, _immer.default)(state, function (draft) {
    draft.symbols = initialState.symbols;
    draft.loading = initialState.loading;
    draft.structureTree = initialState.structureTree;
    draft.closedPaths = initialState.closedPaths;
    draft.languageServerInitializing = initialState.languageServerInitializing;
    draft.error = undefined;
    draft.lastRequestPath = undefined;
  });
};

var symbol = (0, _reduxActions.handleActions)((_handleActions = {}, _defineProperty(_handleActions, String(_actions.loadStructure), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = true;
    draft.lastRequestPath = action.payload || '';
  });
}), _defineProperty(_handleActions, String(_actions.loadStructureSuccess), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.loading = false;
    var _ref = action.payload,
        path = _ref.path,
        data = _ref.data,
        structureTree = _ref.structureTree;
    draft.structureTree[path] = structureTree;
    draft.symbols = _objectSpread({}, state.symbols, _defineProperty({}, path, data));
    draft.languageServerInitializing = false;
    draft.error = undefined;
  });
}), _defineProperty(_handleActions, String(_actions.loadStructureFailed), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    if (action.payload) {
      draft.loading = false;
      draft.error = action.payload;
    }

    draft.languageServerInitializing = false;
  });
}), _defineProperty(_handleActions, String(_actions.closeSymbolPath), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var path = action.payload;

    if (!state.closedPaths.includes(path)) {
      draft.closedPaths.push(path);
    }
  });
}), _defineProperty(_handleActions, String(_actions.openSymbolPath), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    var idx = state.closedPaths.indexOf(action.payload);

    if (idx >= 0) {
      draft.closedPaths.splice(idx, 1);
    }
  });
}), _defineProperty(_handleActions, String(_language_server.languageServerInitializing), function (state) {
  return (0, _immer.default)(state, function (draft) {
    draft.languageServerInitializing = true;
  });
}), _defineProperty(_handleActions, String(_route.routePathChange), clearState), _defineProperty(_handleActions, String(_route.repoChange), clearState), _defineProperty(_handleActions, String(_route.revisionChange), clearState), _defineProperty(_handleActions, String(_route.filePathChange), clearState), _handleActions), initialState);
exports.symbol = symbol;