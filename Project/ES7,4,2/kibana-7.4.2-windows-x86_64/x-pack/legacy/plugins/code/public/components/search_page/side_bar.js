"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SideBar = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _repository_utils = require("../../../common/repository_utils");

var _scope_tabs = require("./scope_tabs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SideBar =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(SideBar, _React$PureComponent);

  function SideBar() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, SideBar);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(SideBar)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "voidFunc", function () {
      return void 0;
    });

    _defineProperty(_assertThisInitialized(_this), "renderLangFacets", function () {
      return _this.props.langFacets.map(function (item, index) {
        var isSelected = _this.props.languages && _this.props.languages.has(item.name);

        return _react2.default.createElement(_eui.EuiFacetButton, {
          className: "codeFilter__item",
          key: "langstats".concat(index),
          onClick: _this.props.onLanguageFilterToggled(item.name),
          quantity: item.value,
          isSelected: isSelected,
          "data-test-subj": "codeSearchLanguageFilterItem",
          buttonRef: _this.voidFunc
        }, item.name);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "renderRepoFacets", function () {
      return _this.props.repoFacets.map(function (item, index) {
        var isSelected = !!_this.props.repositories && _this.props.repositories.has(item.name);

        return _react2.default.createElement(_eui.EuiFacetButton, {
          className: "codeFilter__item",
          key: "repostats".concat(index),
          onClick: _this.props.onRepositoryFilterToggled(item.name),
          quantity: item.value,
          isSelected: isSelected,
          buttonRef: _this.voidFunc
        }, _repository_utils.RepositoryUtils.repoNameFromUri(item.name));
      });
    });

    return _this;
  }

  _createClass(SideBar, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement("div", {
        className: "codeSidebar__container"
      }, _react2.default.createElement(_scope_tabs.ScopeTabs, {
        query: this.props.query,
        scope: this.props.scope
      }), _react2.default.createElement("div", {
        className: "codeFilter__group"
      }, _react2.default.createElement(_eui.EuiFlexGroup, {
        className: "codeFilter__title",
        gutterSize: "s",
        alignItems: "center",
        style: {
          marginBottom: '.5rem'
        }
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement(_eui.EuiToken, {
        iconType: "tokenRepo"
      })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiTitle, {
        size: "xxs"
      }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchPage.sideBar.repositoriesTitle",
        defaultMessage: "Repositories"
      }))))), _react2.default.createElement(_eui.EuiFacetGroup, null, this.renderRepoFacets()), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiFlexGroup, {
        className: "codeFilter__title",
        gutterSize: "s",
        alignItems: "center",
        style: {
          marginBottom: '.5rem'
        }
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement(_eui.EuiToken, {
        iconType: "tokenElement",
        displayOptions: {
          color: 'tokenTint07',
          shape: 'rectangle',
          fill: true
        }
      })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiTitle, {
        size: "xxs"
      }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchPage.sideBar.languagesTitle",
        defaultMessage: "Languages"
      }))))), _react2.default.createElement(_eui.EuiFacetGroup, {
        "data-test-subj": "codeSearchLanguageFilterList"
      }, this.renderLangFacets())));
    }
  }]);

  return SideBar;
}(_react2.default.PureComponent);

exports.SideBar = SideBar;