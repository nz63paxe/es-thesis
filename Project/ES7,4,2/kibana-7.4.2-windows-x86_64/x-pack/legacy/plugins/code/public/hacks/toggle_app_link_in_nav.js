"use strict";

var _new_platform = require("ui/new_platform");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var codeUiEnabled = _new_platform.npStart.core.injectedMetadata.getInjectedVar('codeUiEnabled');

if (codeUiEnabled === false) {
  _new_platform.npStart.core.chrome.navLinks.update('code', {
    hidden: true
  });
}