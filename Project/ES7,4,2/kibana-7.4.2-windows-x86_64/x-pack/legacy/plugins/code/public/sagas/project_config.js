"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchSwitchProjectLanguageServer = watchSwitchProjectLanguageServer;
exports.watchLoadConfigs = watchLoadConfigs;

var _new_platform = require("ui/new_platform");

var _effects = require("redux-saga/effects");

var _actions = require("../actions");

var _project_config = require("../actions/project_config");

var _marked =
/*#__PURE__*/
regeneratorRuntime.mark(switchProjectLanguageServer),
    _marked2 =
/*#__PURE__*/
regeneratorRuntime.mark(watchSwitchProjectLanguageServer),
    _marked3 =
/*#__PURE__*/
regeneratorRuntime.mark(loadConfigs),
    _marked4 =
/*#__PURE__*/
regeneratorRuntime.mark(watchLoadConfigs);

function putProjectConfig(repoUri, config) {
  return _new_platform.npStart.core.http.put("/api/code/repo/config/".concat(repoUri), {
    body: JSON.stringify(config)
  });
}

function switchProjectLanguageServer(action) {
  var _ref, repoUri, config;

  return regeneratorRuntime.wrap(function switchProjectLanguageServer$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _ref = action.payload, repoUri = _ref.repoUri, config = _ref.config;
          _context.next = 4;
          return (0, _effects.call)(putProjectConfig, repoUri, config);

        case 4:
          _context.next = 6;
          return (0, _effects.put)((0, _actions.switchLanguageServerSuccess)());

        case 6:
          _context.next = 12;
          break;

        case 8:
          _context.prev = 8;
          _context.t0 = _context["catch"](0);
          _context.next = 12;
          return (0, _effects.put)((0, _actions.switchLanguageServerFailed)(_context.t0));

        case 12:
        case "end":
          return _context.stop();
      }
    }
  }, _marked, null, [[0, 8]]);
}

function watchSwitchProjectLanguageServer() {
  return regeneratorRuntime.wrap(function watchSwitchProjectLanguageServer$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return (0, _effects.takeEvery)(String(_actions.switchLanguageServer), switchProjectLanguageServer);

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  }, _marked2);
}

function fetchConfigs(repoUri) {
  return _new_platform.npStart.core.http.get("/api/code/repo/config/".concat(repoUri));
}

function loadConfigs(action) {
  var repositories, promises, configs;
  return regeneratorRuntime.wrap(function loadConfigs$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          repositories = action.payload;
          promises = repositories.map(function (repo) {
            return (0, _effects.call)(fetchConfigs, repo.uri);
          });
          _context3.next = 5;
          return (0, _effects.all)(promises);

        case 5:
          configs = _context3.sent;
          _context3.next = 8;
          return (0, _effects.put)((0, _project_config.loadConfigsSuccess)(configs.reduce(function (acc, config) {
            acc[config.uri] = config;
            return acc;
          }, {})));

        case 8:
          _context3.next = 14;
          break;

        case 10:
          _context3.prev = 10;
          _context3.t0 = _context3["catch"](0);
          _context3.next = 14;
          return (0, _effects.put)((0, _project_config.loadConfigsFailed)(_context3.t0));

        case 14:
        case "end":
          return _context3.stop();
      }
    }
  }, _marked3, null, [[0, 10]]);
}

function watchLoadConfigs() {
  return regeneratorRuntime.wrap(function watchLoadConfigs$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return (0, _effects.takeEvery)(String(_actions.fetchReposSuccess), loadConfigs);

        case 2:
        case "end":
          return _context4.stop();
      }
    }
  }, _marked4);
}