"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LanguageSeverTab = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _installation = require("../../../common/installation");

var _language_server = require("../../../common/language_server");

var _language_server2 = require("../../actions/language_server");

var _icons = require("../shared/icons");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LanguageServerLi = function LanguageServerLi(props) {
  var _props$languageServer = props.languageServer,
      status = _props$languageServer.status,
      name = _props$languageServer.name;

  var languageIcon = function languageIcon() {
    if (name === 'TypeScript') {
      return _react2.default.createElement(_icons.TypeScriptIcon, null);
    } else if (name === 'Java') {
      return _react2.default.createElement(_icons.JavaIcon, null);
    } else if (name === 'Go') {
      return _react2.default.createElement(_icons.GoIcon, null);
    } else if (name === 'Ctags') {
      return _react2.default.createElement(_icons.CtagsIcon, null);
    }
  };

  var onInstallClick = function onInstallClick() {
    return props.requestInstallLanguageServer(name);
  };

  var button = null;
  var state = null;

  if (status === _language_server.LanguageServerStatus.RUNNING) {
    state = _react2.default.createElement(_eui.EuiText, {
      size: "xs"
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.code.adminPage.langserverTab.runningText",
      defaultMessage: "Running ..."
    }));
  } else if (status === _language_server.LanguageServerStatus.NOT_INSTALLED) {
    state = _react2.default.createElement(_eui.EuiText, {
      size: "xs",
      color: 'subdued'
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.code.adminPage.langserverTab.notInstalledText",
      defaultMessage: "Not Installed"
    }));
  } else if (status === _language_server.LanguageServerStatus.READY) {
    state = _react2.default.createElement(_eui.EuiText, {
      size: "xs",
      color: 'subdued'
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.code.adminPage.langserverTab.installedText",
      defaultMessage: "Installed"
    }));
  }

  if (props.languageServer.installationType === _installation.InstallationType.Plugin) {
    button = _react2.default.createElement(_eui.EuiButton, {
      size: "s",
      color: "secondary",
      onClick: onInstallClick
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.code.adminPage.langserverTab.setupButtonLabel",
      defaultMessage: "Setup"
    }));
  }

  return _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, " ", languageIcon(), " "), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("strong", null, name)), _react2.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react2.default.createElement("h6", null, " ", state, " "))))), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, " ", button, " "))));
};

var AdminLanguageSever =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(AdminLanguageSever, _React$PureComponent);

  function AdminLanguageSever(props, context) {
    var _this;

    _classCallCheck(this, AdminLanguageSever);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AdminLanguageSever).call(this, props, context));

    _defineProperty(_assertThisInitialized(_this), "toggleInstruction", function (showingInstruction, name, url, pluginName) {
      _this.setState({
        showingInstruction: showingInstruction,
        name: name,
        url: url,
        pluginName: pluginName
      });
    });

    _this.state = {
      showingInstruction: false
    };
    return _this;
  }

  _createClass(AdminLanguageSever, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var languageServers = this.props.languageServers.map(function (ls) {
        return _react2.default.createElement(LanguageServerLi, {
          languageServer: ls,
          key: ls.name,
          requestInstallLanguageServer: function requestInstallLanguageServer() {
            return _this2.toggleInstruction(true, ls.name, ls.downloadUrl, ls.pluginName);
          },
          loading: _this2.props.installLoading[ls.name]
        });
      });
      return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("h3", null, _react2.default.createElement("span", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.languageServersDescription",
        defaultMessage: "{serverCount} {serverCount, plural, one {Language server} other {Language servers}}",
        values: {
          serverCount: this.props.languageServers.length
        }
      })))), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiFlexGroup, {
        direction: "column",
        gutterSize: "s"
      }, languageServers), _react2.default.createElement(LanguageServerInstruction, {
        show: this.state.showingInstruction,
        name: this.state.name,
        pluginName: this.state.pluginName,
        url: this.state.url,
        close: function close() {
          return _this2.toggleInstruction(false);
        }
      }));
    }
  }]);

  return AdminLanguageSever;
}(_react2.default.PureComponent);

var SupportedOS = [{
  id: 'windows',
  name: 'Windows'
}, {
  id: 'linux',
  name: 'Linux'
}, {
  id: 'darwin',
  name: 'macOS'
}];

var LanguageServerInstruction = function LanguageServerInstruction(props) {
  var tabs = SupportedOS.map(function (_ref) {
    var id = _ref.id,
        name = _ref.name;
    var url = props.url ? props.url.replace('$OS', id) : '';
    var installCode = "bin/kibana-plugin install ".concat(url);
    return {
      id: id,
      name: name,
      content: _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiText, {
        grow: false
      }, _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.installTitle",
        defaultMessage: "Install"
      })), _react2.default.createElement("ol", null, _react2.default.createElement("li", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.stopKibanaDescription",
        defaultMessage: "Stop your kibana Code node."
      })), _react2.default.createElement("li", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.useFollowingCommandToInstallDescription",
        defaultMessage: "Use the following command to install the {name} language server.",
        values: {
          name: props.name
        }
      }))), _react2.default.createElement(_eui.EuiCodeBlock, {
        language: "shell"
      }, installCode), _react2.default.createElement("h3", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.uninstallTitle",
        defaultMessage: "Uninstall"
      })), _react2.default.createElement("ol", null, _react2.default.createElement("li", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.stopKibanaDescription",
        defaultMessage: "Stop your kibana Code node."
      })), _react2.default.createElement("li", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.langserverTab.setup.useFollowingCommandToRemoveDescription",
        defaultMessage: "Use the following command to remove the {name} language server.",
        values: {
          name: props.name
        }
      }))), _react2.default.createElement(_eui.EuiCodeBlock, {
        language: "shell"
      }, "bin/kibana-plugin remove ", props.pluginName)))
    };
  });
  return _react2.default.createElement(_react2.default.Fragment, null, ' ', props.show && _react2.default.createElement(_eui.EuiOverlayMask, null, _react2.default.createElement(_eui.EuiModal, {
    onClose: props.close,
    maxWidth: false
  }, _react2.default.createElement(_eui.EuiModalHeader, null, _react2.default.createElement(_eui.EuiModalHeaderTitle, null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.langserverTab.setup.installationInstructionTitle",
    defaultMessage: "Installation Instructions"
  }))), _react2.default.createElement(_eui.EuiModalBody, null, _react2.default.createElement(_eui.EuiTabbedContent, {
    tabs: tabs,
    initialSelectedTab: tabs[1],
    size: 'm'
  })), _react2.default.createElement(_eui.EuiModalFooter, null, _react2.default.createElement(_eui.EuiButton, {
    onClick: props.close,
    fill: true
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.adminPage.langserverTab.setup.closeButtonLabel",
    defaultMessage: "Close"
  }))))));
};

var mapStateToProps = function mapStateToProps(state) {
  return {
    languageServers: state.languageServer.languageServers,
    installLoading: state.languageServer.installServerLoading
  };
};

var mapDispatchToProps = {
  requestInstallLanguageServer: _language_server2.requestInstallLanguageServer
};
var LanguageSeverTab = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(AdminLanguageSever);
exports.LanguageSeverTab = LanguageSeverTab;