"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SymbolTree = void 0;

var _reactRedux = require("react-redux");

var _reactRouterDom = require("react-router-dom");

var _actions = require("../../actions");

var _selectors = require("../../selectors");

var _code_symbol_tree = require("./code_symbol_tree");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var mapStateToProps = function mapStateToProps(state) {
  return {
    structureTree: (0, _selectors.structureSelector)(state),
    closedPaths: state.symbol.closedPaths
  };
};

var mapDispatchToProps = {
  openSymbolPath: _actions.openSymbolPath,
  closeSymbolPath: _actions.closeSymbolPath
};
var SymbolTree = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(_code_symbol_tree.CodeSymbolTree));
exports.SymbolTree = SymbolTree;