"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScopeTabs = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _querystring = _interopRequireDefault(require("querystring"));

var _react2 = _interopRequireDefault(require("react"));

var _url = _interopRequireDefault(require("url"));

var _model = require("../../../model");

var _url2 = require("../../utils/url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ScopeTabs =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(ScopeTabs, _React$PureComponent);

  function ScopeTabs() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ScopeTabs);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ScopeTabs)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onTabClicked", function (scope) {
      return function () {
        var query = _this.props.query;

        var queries = _querystring.default.parse(_url2.history.location.search.replace('?', ''));

        _url2.history.push(_url.default.format({
          pathname: '/search',
          query: _objectSpread({}, queries, {
            q: query,
            scope: scope
          })
        }));
      };
    });

    return _this;
  }

  _createClass(ScopeTabs, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement("div", {
        className: "codeContainer__tabs"
      }, _react2.default.createElement(_eui.EuiTabs, {
        style: {
          height: '100%'
        }
      }, _react2.default.createElement(_eui.EuiTab, {
        className: "codeUtility__width--half",
        isSelected: this.props.scope !== _model.SearchScope.REPOSITORY,
        onClick: this.onTabClicked(_model.SearchScope.DEFAULT)
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchPage.scopeTabs.codeTabLabel",
        defaultMessage: "Code"
      })), _react2.default.createElement(_eui.EuiTab, {
        className: "codeUtility__width--half",
        isSelected: this.props.scope === _model.SearchScope.REPOSITORY,
        onClick: this.onTabClicked(_model.SearchScope.REPOSITORY)
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.searchPage.scopeTabs.repositoryTabLabel",
        defaultMessage: "Repository"
      }))));
    }
  }]);

  return ScopeTabs;
}(_react2.default.PureComponent);

exports.ScopeTabs = ScopeTabs;