"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rootReducer = void 0;

var _redux = require("redux");

var _blame = require("./blame");

var _commit = require("./commit");

var _editor = require("./editor");

var _file = require("./file");

var _language_server = require("./language_server");

var _repository_management = require("./repository_management");

var _route = require("./route");

var _search = require("./search");

var _setup = require("./setup");

var _shortcuts = require("./shortcuts");

var _status = require("./status");

var _symbol = require("./symbol");

var _revision = require("./revision");

var _repository = require("./repository");

var _file_tree = require("./file_tree");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var reducers = {
  blame: _blame.blame,
  commit: _commit.commit,
  editor: _editor.editor,
  file: _file.file,
  fileTree: _file_tree.fileTree,
  languageServer: _language_server.languageServer,
  repository: _repository.repository,
  repositoryManagement: _repository_management.repositoryManagement,
  revision: _revision.revision,
  route: _route.route,
  search: _search.search,
  setup: _setup.setup,
  shortcuts: _shortcuts.shortcuts,
  status: _status.status,
  symbol: _symbol.symbol
};
var rootReducer = (0, _redux.combineReducers)(reducers);
exports.rootReducer = rootReducer;