"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.route = void 0;

var _immer = _interopRequireDefault(require("immer"));

var _reduxActions = require("redux-actions");

var _actions = require("../actions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  match: {}
};
var route = (0, _reduxActions.handleActions)(_defineProperty({}, String(_actions.routeChange), function (state, action) {
  return (0, _immer.default)(state, function (draft) {
    draft.previousMatch = state.match;
    draft.match = action.payload;
  });
}), initialState);
exports.route = route;