"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ImportProject = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../actions");

var _url = require("../../utils/url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CodeImportProject =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CodeImportProject, _React$PureComponent);

  function CodeImportProject() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, CodeImportProject);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(CodeImportProject)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      value: '',
      isInvalid: false
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (e) {
      _this.setState({
        value: e.target.value,
        isInvalid: (0, _url.isImportRepositoryURLInvalid)(e.target.value)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "submitImportProject", function () {
      if (!(0, _url.isImportRepositoryURLInvalid)(_this.state.value)) {
        _this.props.importRepo(_this.state.value);
      } else if (!_this.state.isInvalid) {
        _this.setState({
          isInvalid: true
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "updateIsInvalid", function () {
      _this.setState({
        isInvalid: (0, _url.isImportRepositoryURLInvalid)(_this.state.value)
      });
    });

    return _this;
  }

  _createClass(CodeImportProject, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          importLoading = _this$props.importLoading,
          toastMessage = _this$props.toastMessage,
          showToast = _this$props.showToast,
          toastType = _this$props.toastType;
      return _react2.default.createElement("div", {
        className: "codeContainer__import"
      }, showToast && _react2.default.createElement(_eui.EuiGlobalToastList, {
        toasts: [{
          title: '',
          color: toastType,
          text: toastMessage,
          id: toastMessage || ''
        }],
        dismissToast: this.props.closeToast,
        toastLifeTimeMs: 6000
      }), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiFormRow, {
        label: _i18n.i18n.translate('xpack.code.adminPage.repoTab.repositoryUrlTitle', {
          defaultMessage: 'Repository URL'
        }),
        helpText: "e.g. https://github.com/Microsoft/TypeScript-Node-Starter",
        fullWidth: true,
        isInvalid: this.state.isInvalid,
        error: _i18n.i18n.translate('xpack.code.adminPage.repoTab.repositoryUrlEmptyText', {
          defaultMessage: "The URL shouldn't be empty."
        })
      }, _react2.default.createElement(_eui.EuiFieldText, {
        value: this.state.value,
        onChange: this.onChange,
        "aria-label": "input project url",
        "data-test-subj": "importRepositoryUrlInputBox",
        isLoading: importLoading,
        fullWidth: true,
        isInvalid: this.state.isInvalid,
        autoFocus: true
      }))), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement(_eui.EuiButton, {
        className: "codeButton__projectImport",
        onClick: this.submitImportProject,
        "data-test-subj": "importRepositoryButton"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.code.adminPage.repoTab.importButtonLabel",
        defaultMessage: "Import"
      })))));
    }
  }]);

  return CodeImportProject;
}(_react2.default.PureComponent);

var mapStateToProps = function mapStateToProps(state) {
  return {
    importLoading: state.repositoryManagement.importLoading,
    toastMessage: state.repositoryManagement.toastMessage,
    toastType: state.repositoryManagement.toastType,
    showToast: state.repositoryManagement.showToast
  };
};

var mapDispatchToProps = {
  importRepo: _actions.importRepo,
  closeToast: _actions.closeToast
};
var ImportProject = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(CodeImportProject);
exports.ImportProject = ImportProject;