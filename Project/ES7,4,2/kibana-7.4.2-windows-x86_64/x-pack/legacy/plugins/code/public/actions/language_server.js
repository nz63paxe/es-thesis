"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.languageServerInitializing = exports.installLanguageServerSuccess = exports.requestInstallLanguageServerFailed = exports.requestInstallLanguageServerSuccess = exports.requestInstallLanguageServer = exports.loadLanguageServersFailed = exports.loadLanguageServersSuccess = exports.loadLanguageServers = void 0;

var _reduxActions = require("redux-actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var loadLanguageServers = (0, _reduxActions.createAction)('LOAD LANGUAGE SERVERS');
exports.loadLanguageServers = loadLanguageServers;
var loadLanguageServersSuccess = (0, _reduxActions.createAction)('LOAD LANGUAGE SERVERS SUCCESS');
exports.loadLanguageServersSuccess = loadLanguageServersSuccess;
var loadLanguageServersFailed = (0, _reduxActions.createAction)('LOAD LANGUAGE SERVERS FAILED');
exports.loadLanguageServersFailed = loadLanguageServersFailed;
var requestInstallLanguageServer = (0, _reduxActions.createAction)('REQUEST INSTALL LANGUAGE SERVERS');
exports.requestInstallLanguageServer = requestInstallLanguageServer;
var requestInstallLanguageServerSuccess = (0, _reduxActions.createAction)('REQUEST INSTALL LANGUAGE SERVERS SUCCESS');
exports.requestInstallLanguageServerSuccess = requestInstallLanguageServerSuccess;
var requestInstallLanguageServerFailed = (0, _reduxActions.createAction)('REQUEST INSTALL LANGUAGE SERVERS FAILED');
exports.requestInstallLanguageServerFailed = requestInstallLanguageServerFailed;
var installLanguageServerSuccess = (0, _reduxActions.createAction)('INSTALL LANGUAGE SERVERS SUCCESS');
exports.installLanguageServerSuccess = installLanguageServerSuccess;
var languageServerInitializing = (0, _reduxActions.createAction)('LANGUAGE SERVER INITIALIZING');
exports.languageServerInitializing = languageServerInitializing;