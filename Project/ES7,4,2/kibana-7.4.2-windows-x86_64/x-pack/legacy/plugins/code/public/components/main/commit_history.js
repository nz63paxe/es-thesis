"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CommitHistory = exports.CommitHistoryComponent = exports.PageButtons = exports.CommitHistoryLoading = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = _interopRequireDefault(require("lodash"));

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _i18n = require("@kbn/i18n");

var _commit_link = require("../diff_page/commit_link");

var _selectors = require("../../selectors");

var _actions = require("../../actions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var COMMIT_ID_LENGTH = 8;

var Commit = function Commit(props) {
  var date = props.date,
      commit = props.commit;
  var message = commit.message,
      committer = commit.committer,
      id = commit.id;
  var commitId = id.split('').slice(0, COMMIT_ID_LENGTH).join('');
  return _react2.default.createElement(_eui.EuiPanel, {
    className: "code-timeline__commit--root"
  }, _react2.default.createElement("div", {
    className: "eui-textTruncate"
  }, _react2.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react2.default.createElement("p", {
    className: "eui-textTruncate"
  }, message)), _react2.default.createElement(_eui.EuiText, {
    size: "xs"
  }, _react2.default.createElement(_eui.EuiTextColor, {
    color: "subdued"
  }, committer, " \xB7 ", date))), _react2.default.createElement("div", {
    className: "code-commit-id"
  }, _react2.default.createElement(_commit_link.CommitLink, {
    repoUri: props.repoUri,
    commit: commitId
  })));
};

var CommitGroup = function CommitGroup(props) {
  var commitList = props.commits.map(function (commit) {
    return _react2.default.createElement(Commit, {
      commit: commit,
      key: commit.id,
      date: props.date,
      repoUri: props.repoUri
    });
  });
  return _react2.default.createElement("div", {
    className: "code-timeline__commit-container"
  }, _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexStart",
    gutterSize: "s"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement("div", {
    className: "code-timeline__marker"
  })), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("h4", null, _react2.default.createElement(_eui.EuiTextColor, {
    color: "subdued"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.mainPage.history.commitsOnTitle",
    defaultMessage: "Commits on {date}",
    values: {
      date: props.date
    }
  }), ' '))))), _react2.default.createElement("div", {
    className: "code-timeline"
  }, commitList));
};

var CommitHistoryLoading = function CommitHistoryLoading() {
  return _react2.default.createElement("div", {
    className: "codeLoader"
  }, _react2.default.createElement(_eui.EuiLoadingSpinner, {
    size: "xl"
  }));
};

exports.CommitHistoryLoading = CommitHistoryLoading;

var PageButtons = function PageButtons(props) {
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceAround"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_eui.EuiButton, {
    onClick: props.onClick,
    iconType: "arrowDown",
    isLoading: props.loading,
    isDisabled: props.disabled,
    size: "s"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.code.mainPage.history.moreButtonLabel",
    defaultMessage: "More"
  }))));
};

exports.PageButtons = PageButtons;
var commitDateFormatMap = {
  en: 'MMMM Do, YYYY',
  'zh-cn': 'YYYY年MoDo'
};

var CommitHistoryComponent = function CommitHistoryComponent(props) {
  var commits = _lodash.default.groupBy(props.commits, function (commit) {
    return (0, _moment.default)(commit.updated).format('YYYYMMDD');
  });

  var commitDates = Object.keys(commits).sort(function (a, b) {
    return b.localeCompare(a);
  }); // sort desc

  var locale = _i18n.i18n.getLocale();

  var commitDateFormat = locale in commitDateFormatMap ? commitDateFormatMap[locale] : commitDateFormatMap.en;
  var commitList = commitDates.map(function (cd) {
    return _react2.default.createElement(CommitGroup, {
      commits: commits[cd],
      date: (0, _moment.default)(cd).format(commitDateFormat),
      key: cd,
      repoUri: props.repoUri
    });
  });
  return _react2.default.createElement("div", {
    className: "codeContainer__commitMessages"
  }, _react2.default.createElement("div", {
    className: "codeHeader__commit"
  }, props.header), commitList, !props.showPagination && props.loadingCommits && _react2.default.createElement(CommitHistoryLoading, null), props.showPagination && _react2.default.createElement(PageButtons, {
    disabled: !props.hasMoreCommit || props.commits.length < 10,
    onClick: function onClick() {
      return props.fetchMoreCommits(props.repoUri);
    },
    loading: props.loadingCommits
  }));
};

exports.CommitHistoryComponent = CommitHistoryComponent;

var mapStateToProps = function mapStateToProps(state) {
  return {
    file: state.file.file,
    commits: (0, _selectors.treeCommitsSelector)(state) || [],
    loadingCommits: state.revision.loadingCommits,
    hasMoreCommit: (0, _selectors.hasMoreCommitsSelector)(state)
  };
};

var mapDispatchToProps = {
  fetchMoreCommits: _actions.fetchMoreCommits
};
var CommitHistory = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps // @ts-ignore
)(CommitHistoryComponent);
exports.CommitHistory = CommitHistory;