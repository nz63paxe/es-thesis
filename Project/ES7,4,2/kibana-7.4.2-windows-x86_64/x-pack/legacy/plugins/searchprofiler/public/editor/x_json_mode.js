"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.installXJsonMode = installXJsonMode;

var _brace = _interopRequireDefault(require("brace"));

var xJsonRules = _interopRequireWildcard(require("./x_json_highlight_rules"));

var _worker = require("./worker");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Copied and modified from src/legacy/core_plugins/console/public/src/sense_editor/mode/input.js
var oop = _brace.default.acequire('ace/lib/oop');

var _ace$acequire = _brace.default.acequire('ace/mode/json'),
    JSONMode = _ace$acequire.Mode;

var _ace$acequire2 = _brace.default.acequire('ace/tokenizer'),
    AceTokenizer = _ace$acequire2.Tokenizer;

var _ace$acequire3 = _brace.default.acequire('ace/mode/matching_brace_outdent'),
    MatchingBraceOutdent = _ace$acequire3.MatchingBraceOutdent;

var _ace$acequire4 = _brace.default.acequire('ace/mode/behaviour/cstyle'),
    CstyleBehaviour = _ace$acequire4.CstyleBehaviour;

var _ace$acequire5 = _brace.default.acequire('ace/mode/folding/cstyle'),
    CStyleFoldMode = _ace$acequire5.FoldMode;

var _ace$acequire6 = _brace.default.acequire('ace/worker/worker_client'),
    WorkerClient = _ace$acequire6.WorkerClient;

function XJsonMode() {
  this.$tokenizer = new AceTokenizer(xJsonRules.getRules());
  this.$outdent = new MatchingBraceOutdent();
  this.$behaviour = new CstyleBehaviour();
  this.foldingRules = new CStyleFoldMode();
} // Order here matters here:
// 1. We first inherit


oop.inherits(XJsonMode, JSONMode); // 2. Then clobber `createWorker` method to install our worker source. Per ace's wiki: https://github.com/ajaxorg/ace/wiki/Syntax-validation

XJsonMode.prototype.createWorker = function (session) {
  var xJsonWorker = new WorkerClient(['ace'], _worker.workerModule, 'JsonWorker');
  xJsonWorker.attachToDocument(session.getDocument());
  xJsonWorker.on('annotate', function (e) {
    session.setAnnotations(e.data);
  });
  xJsonWorker.on('terminate', function () {
    session.clearAnnotations();
  });
  return xJsonWorker;
};

function installXJsonMode(editor) {
  var session = editor.getSession();
  session.setMode(new XJsonMode());
}