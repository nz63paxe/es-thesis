"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initializeEditor = initializeEditor;

var _brace = _interopRequireDefault(require("brace"));

var _x_json_mode = require("./x_json_mode");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initializeEditor(_ref) {
  var el = _ref.el,
      licenseEnabled = _ref.licenseEnabled,
      initialContent = _ref.initialContent;

  var editor = _brace.default.acequire('ace/ace').edit(el);

  (0, _x_json_mode.installXJsonMode)(editor);
  editor.$blockScrolling = Infinity;

  if (!licenseEnabled) {
    editor.setReadOnly(true);
    editor.container.style.pointerEvents = 'none';
    editor.container.style.opacity = '0.5';
    editor.renderer.setStyle('disabled');
    editor.blur();
  }

  return editor;
}