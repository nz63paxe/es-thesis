"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMockKbnServer = exports.getMockConfig = exports.getMockTaskFetch = exports.getMockCallWithInternal = exports.getMockTaskInstance = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getMockTaskInstance = () => ({
  state: {
    runs: 0,
    stats: {}
  }
});

exports.getMockTaskInstance = getMockTaskInstance;
const defaultMockSavedObjects = [{
  _id: 'visualization:coolviz-123',
  _source: {
    type: 'visualization',
    visualization: {
      visState: '{"type": "shell_beads"}'
    }
  }
}];
const defaultMockTaskDocs = [getMockTaskInstance()];

const getMockCallWithInternal = (hits = defaultMockSavedObjects) => {
  return () => {
    return Promise.resolve({
      hits: {
        hits
      }
    });
  };
};

exports.getMockCallWithInternal = getMockCallWithInternal;

const getMockTaskFetch = (docs = defaultMockTaskDocs) => {
  return () => Promise.resolve({
    docs
  });
};

exports.getMockTaskFetch = getMockTaskFetch;

const getMockConfig = () => {
  return {
    get: () => ''
  };
};

exports.getMockConfig = getMockConfig;

const getMockKbnServer = (mockCallWithInternal = getMockCallWithInternal(), mockTaskFetch = getMockTaskFetch(), mockConfig = getMockConfig()) => ({
  plugins: {
    elasticsearch: {
      getCluster: cluster => ({
        callWithInternalUser: mockCallWithInternal
      })
    },
    xpack_main: {},
    task_manager: {
      registerTaskDefinitions: opts => undefined,
      schedule: opts => Promise.resolve(),
      fetch: mockTaskFetch
    }
  },
  usage: {
    collectorSet: {
      makeUsageCollector: () => '',
      register: () => undefined
    }
  },
  config: () => mockConfig,
  log: () => undefined
});

exports.getMockKbnServer = getMockKbnServer;