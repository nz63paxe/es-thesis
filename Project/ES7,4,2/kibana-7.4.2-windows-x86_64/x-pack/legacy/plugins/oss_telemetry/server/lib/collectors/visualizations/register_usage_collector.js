"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerVisualizationsCollector = registerVisualizationsCollector;

var _get_usage_collector = require("./get_usage_collector");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerVisualizationsCollector(server) {
  const {
    usage
  } = server;
  const collector = usage.collectorSet.makeUsageCollector((0, _get_usage_collector.getUsageCollector)(server));
  usage.collectorSet.register(collector);
}