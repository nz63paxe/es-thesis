"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerTasks = registerTasks;
exports.scheduleTasks = scheduleTasks;

var _constants = require("../../../constants");

var _task_runner = require("./visualizations/task_runner");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerTasks(server) {
  const taskManager = server.plugins.task_manager;
  taskManager.registerTaskDefinitions({
    [_constants.VIS_TELEMETRY_TASK]: {
      title: 'X-Pack telemetry calculator for Visualizations',
      type: _constants.VIS_TELEMETRY_TASK,
      numWorkers: _constants.VIS_TELEMETRY_TASK_NUM_WORKERS,

      // by default it's 100% their workers
      createTaskRunner({
        taskInstance
      }) {
        return {
          run: (0, _task_runner.visualizationsTaskRunner)(taskInstance, server)
        };
      }

    }
  });
}

function scheduleTasks(server) {
  const taskManager = server.plugins.task_manager;
  const {
    kbnServer
  } = server.plugins.xpack_main.status.plugin;
  kbnServer.afterPluginsInit(() => {
    // The code block below can't await directly within "afterPluginsInit"
    // callback due to circular dependency. The server isn't "ready" until
    // this code block finishes. Migrations wait for server to be ready before
    // executing. Saved objects repository waits for migrations to finish before
    // finishing the request. To avoid this, we'll await within a separate
    // function block.
    (async () => {
      try {
        await taskManager.schedule({
          id: `${_constants.PLUGIN_ID}-${_constants.VIS_TELEMETRY_TASK}`,
          taskType: _constants.VIS_TELEMETRY_TASK,
          state: {
            stats: {},
            runs: 0
          }
        });
      } catch (e) {
        server.log(['warning', 'telemetry'], `Error scheduling task, received ${e.message}`);
      }
    })();
  });
}