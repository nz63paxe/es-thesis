"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createCspCollector = createCspCollector;
exports.registerCspCollector = registerCspCollector;

var _csp = require("../../../../../../../../src/legacy/server/csp");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createCspCollector(server) {
  return {
    type: 'csp',
    isReady: () => true,

    async fetch() {
      const config = server.config(); // It's important that we do not send the value of csp.rules here as it
      // can be customized with values that can be identifiable to given
      // installs, such as URLs

      const defaultRulesString = (0, _csp.createCSPRuleString)([..._csp.DEFAULT_CSP_RULES]);
      const actualRulesString = (0, _csp.createCSPRuleString)(config.get('csp.rules'));
      return {
        strict: config.get('csp.strict'),
        warnLegacyBrowsers: config.get('csp.warnLegacyBrowsers'),
        rulesChangedFromDefault: defaultRulesString !== actualRulesString
      };
    }

  };
}

function registerCspCollector(server) {
  const {
    usage
  } = server;
  const collector = usage.collectorSet.makeUsageCollector(createCspCollector(server));
  usage.collectorSet.register(collector);
}