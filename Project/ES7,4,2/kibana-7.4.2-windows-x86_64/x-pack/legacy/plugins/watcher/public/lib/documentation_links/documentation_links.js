"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.watchActionsConfigurationMap = exports.watcherGettingStartedUrl = exports.executeWatchApiUrl = exports.putWatchApiUrl = void 0;

var _documentation_links = require("ui/documentation_links");

var _constants = require("../../../common/constants");

var _watchActionsConfigur;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var elasticDocLinkBase = "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "guide/en/");
var esBase = "".concat(elasticDocLinkBase, "elasticsearch/reference/").concat(_documentation_links.DOC_LINK_VERSION);
var esStackBase = "".concat(elasticDocLinkBase, "elastic-stack-overview/").concat(_documentation_links.DOC_LINK_VERSION);
var kibanaBase = "".concat(elasticDocLinkBase, "kibana/").concat(_documentation_links.DOC_LINK_VERSION);
var putWatchApiUrl = "".concat(esBase, "/watcher-api-put-watch.html");
exports.putWatchApiUrl = putWatchApiUrl;
var executeWatchApiUrl = "".concat(esBase, "/watcher-api-execute-watch.html#watcher-api-execute-watch-action-mode");
exports.executeWatchApiUrl = executeWatchApiUrl;
var watcherGettingStartedUrl = "".concat(kibanaBase, "/watcher-ui.html");
exports.watcherGettingStartedUrl = watcherGettingStartedUrl;
var watchActionsConfigurationMap = (_watchActionsConfigur = {}, _defineProperty(_watchActionsConfigur, _constants.ACTION_TYPES.SLACK, "".concat(esStackBase, "/actions-slack.html#configuring-slack")), _defineProperty(_watchActionsConfigur, _constants.ACTION_TYPES.PAGERDUTY, "".concat(esStackBase, "/actions-pagerduty.html#configuring-pagerduty")), _defineProperty(_watchActionsConfigur, _constants.ACTION_TYPES.JIRA, "".concat(esStackBase, "/actions-jira.html#configuring-jira")), _watchActionsConfigur);
exports.watchActionsConfigurationMap = watchActionsConfigurationMap;