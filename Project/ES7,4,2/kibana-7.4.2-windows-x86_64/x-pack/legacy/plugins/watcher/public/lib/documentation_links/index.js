"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _documentation_links = require("./documentation_links");

Object.keys(_documentation_links).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _documentation_links[key];
    }
  });
});