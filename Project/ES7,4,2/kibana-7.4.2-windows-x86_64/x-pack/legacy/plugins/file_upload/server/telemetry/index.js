"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  makeUsageCollector: true
};
Object.defineProperty(exports, "makeUsageCollector", {
  enumerable: true,
  get: function () {
    return _make_usage_collector.makeUsageCollector;
  }
});

var _telemetry = require("./telemetry");

Object.keys(_telemetry).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _telemetry[key];
    }
  });
});

var _make_usage_collector = require("./make_usage_collector");