"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeUsageCollector = makeUsageCollector;

var _telemetry = require("./telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function makeUsageCollector(server) {
  const fileUploadUsageCollector = server.usage.collectorSet.makeUsageCollector({
    type: 'fileUploadTelemetry',
    isReady: () => true,
    fetch: async () => (await (0, _telemetry.getTelemetry)(server)) || (0, _telemetry.initTelemetry)()
  });
  server.usage.collectorSet.register(fileUploadUsageCollector);
}