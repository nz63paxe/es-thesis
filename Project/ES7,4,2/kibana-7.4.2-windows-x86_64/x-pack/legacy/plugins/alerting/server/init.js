"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = init;

var _uuid = _interopRequireDefault(require("uuid"));

var _alert_type_registry = require("./alert_type_registry");

var _alerts_client = require("./alerts_client");

var _server = require("../../../../../src/core/server");

var _optional_plugin = require("../../../server/lib/optional_plugin");

var _routes = require("./routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function init(server) {
  const config = server.config();
  const kbnServer = server;
  const taskManager = server.plugins.task_manager;
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');
  const spaces = (0, _optional_plugin.createOptionalPlugin)(config, 'xpack.spaces', server.plugins, 'spaces');
  const security = (0, _optional_plugin.createOptionalPlugin)(config, 'xpack.security', kbnServer.newPlatform.setup.plugins, 'security');
  server.plugins.xpack_main.registerFeature({
    id: 'alerting',
    name: 'Alerting',
    app: ['alerting', 'kibana'],
    privileges: {
      all: {
        savedObject: {
          all: ['alert'],
          read: []
        },
        ui: [],
        api: ['alerting-read', 'alerting-all']
      },
      read: {
        savedObject: {
          all: [],
          read: ['alert']
        },
        ui: [],
        api: ['alerting-read']
      }
    }
  }); // Encrypted attributes

  server.plugins.encrypted_saved_objects.registerType({
    type: 'alert',
    attributesToEncrypt: new Set(['apiKey']),
    attributesToExcludeFromAAD: new Set(['scheduledTaskId'])
  });

  function getServices(request) {
    return {
      log: (...args) => server.log(...args),
      callCluster: (...args) => callWithRequest(request, ...args),
      savedObjectsClient: server.savedObjects.getScopedSavedObjectsClient(request)
    };
  }

  function getBasePath(spaceId) {
    return spaces.isEnabled && spaceId ? spaces.getBasePath(spaceId) : server.config().get('server.basePath') || '';
  }

  function spaceIdToNamespace(spaceId) {
    return spaces.isEnabled && spaceId ? spaces.spaceIdToNamespace(spaceId) : undefined;
  }

  const alertTypeRegistry = new _alert_type_registry.AlertTypeRegistry({
    getServices,
    isSecurityEnabled: security.isEnabled,
    taskManager,
    executeAction: server.plugins.actions.execute,
    encryptedSavedObjectsPlugin: server.plugins.encrypted_saved_objects,
    getBasePath,
    spaceIdToNamespace
  }); // Register routes

  (0, _routes.createAlertRoute)(server);
  (0, _routes.deleteAlertRoute)(server);
  (0, _routes.findRoute)(server);
  (0, _routes.getRoute)(server);
  (0, _routes.listAlertTypesRoute)(server);
  (0, _routes.updateAlertRoute)(server);
  (0, _routes.enableAlertRoute)(server);
  (0, _routes.disableAlertRoute)(server); // Expose functions

  server.decorate('request', 'getAlertsClient', function () {
    const request = this;
    const savedObjectsClient = request.getSavedObjectsClient();
    const alertsClient = new _alerts_client.AlertsClient({
      log: server.log.bind(server),
      savedObjectsClient,
      alertTypeRegistry,
      taskManager,
      spaceId: spaces.isEnabled ? spaces.getSpaceId(request) : undefined,

      async getUserName() {
        if (!security.isEnabled) {
          return null;
        }

        const user = await security.authc.getCurrentUser(_server.KibanaRequest.from(request));
        return user ? user.username : null;
      },

      async createAPIKey() {
        if (!security.isEnabled) {
          return {
            created: false
          };
        }

        return {
          created: true,
          result: await security.authc.createAPIKey(_server.KibanaRequest.from(request), {
            name: `source: alerting, generated uuid: "${_uuid.default.v4()}"`,
            role_descriptors: {}
          })
        };
      }

    });
    return alertsClient;
  });
  const exposedFunctions = {
    registerType: alertTypeRegistry.register.bind(alertTypeRegistry),
    listTypes: alertTypeRegistry.list.bind(alertTypeRegistry)
  };
  server.expose(exposedFunctions);
}