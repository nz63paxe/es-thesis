"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformActionParams = transformActionParams;

var _mustache = _interopRequireDefault(require("mustache"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function transformActionParams(params, state, context) {
  const result = (0, _lodash.cloneDeep)(params, value => {
    if (!(0, _lodash.isString)(value)) return;
    return _mustache.default.render(value, {
      context,
      state
    });
  }); // The return type signature for `cloneDeep()` ends up taking the return
  // type signature for the customizer, but rather than pollute the customizer
  // with casts, seemed better to just do it in one place, here.

  return result;
}