"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteAlertRoute = deleteAlertRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function deleteAlertRoute(server) {
  server.route({
    method: 'DELETE',
    path: '/api/alert/{id}',
    options: {
      tags: ['access:alerting-all'],
      validate: {
        params: _joi.default.object().keys({
          id: _joi.default.string().required()
        }).required()
      }
    },

    async handler(request, h) {
      const {
        id
      } = request.params;
      const alertsClient = request.getAlertsClient();
      await alertsClient.delete({
        id
      });
      return h.response().code(204);
    }

  });
}