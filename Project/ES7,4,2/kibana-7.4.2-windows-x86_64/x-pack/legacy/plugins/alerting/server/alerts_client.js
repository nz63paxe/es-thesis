"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AlertsClient = void 0;

var _lodash = require("lodash");

var _lib = require("./lib");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AlertsClient {
  constructor({
    alertTypeRegistry,
    savedObjectsClient,
    taskManager,
    log,
    spaceId,
    getUserName,
    createAPIKey
  }) {
    _defineProperty(this, "log", void 0);

    _defineProperty(this, "getUserName", void 0);

    _defineProperty(this, "spaceId", void 0);

    _defineProperty(this, "taskManager", void 0);

    _defineProperty(this, "savedObjectsClient", void 0);

    _defineProperty(this, "alertTypeRegistry", void 0);

    _defineProperty(this, "createAPIKey", void 0);

    this.log = log;
    this.getUserName = getUserName;
    this.spaceId = spaceId;
    this.taskManager = taskManager;
    this.alertTypeRegistry = alertTypeRegistry;
    this.savedObjectsClient = savedObjectsClient;
    this.createAPIKey = createAPIKey;
  }

  async create({
    data,
    options
  }) {
    // Throws an error if alert type isn't registered
    const alertType = this.alertTypeRegistry.get(data.alertTypeId);
    const validatedAlertTypeParams = (0, _lib.validateAlertTypeParams)(alertType, data.alertTypeParams);
    const apiKey = await this.createAPIKey();
    const username = await this.getUserName();
    const {
      alert: rawAlert,
      references
    } = this.getRawAlert({ ...data,
      createdBy: username,
      updatedBy: username,
      apiKeyOwner: apiKey.created && username ? username : undefined,
      apiKey: apiKey.created ? Buffer.from(`${apiKey.result.id}:${apiKey.result.api_key}`).toString('base64') : undefined,
      alertTypeParams: validatedAlertTypeParams
    });
    const createdAlert = await this.savedObjectsClient.create('alert', rawAlert, { ...options,
      references
    });

    if (data.enabled) {
      let scheduledTask;

      try {
        scheduledTask = await this.scheduleAlert(createdAlert.id, rawAlert.alertTypeId, rawAlert.interval);
      } catch (e) {
        // Cleanup data, something went wrong scheduling the task
        try {
          await this.savedObjectsClient.delete('alert', createdAlert.id);
        } catch (err) {
          // Skip the cleanup error and throw the task manager error to avoid confusion
          this.log(['alerting', 'error'], `Failed to cleanup alert "${createdAlert.id}" after scheduling task failed. Error: ${err.message}`);
        }

        throw e;
      }

      await this.savedObjectsClient.update('alert', createdAlert.id, {
        scheduledTaskId: scheduledTask.id
      }, {
        references
      });
      createdAlert.attributes.scheduledTaskId = scheduledTask.id;
    }

    return this.getAlertFromRaw(createdAlert.id, createdAlert.attributes, references);
  }

  async get({
    id
  }) {
    const result = await this.savedObjectsClient.get('alert', id);
    return this.getAlertFromRaw(result.id, result.attributes, result.references);
  }

  async find({
    options = {}
  } = {}) {
    const results = await this.savedObjectsClient.find({ ...options,
      type: 'alert'
    });
    const data = results.saved_objects.map(result => this.getAlertFromRaw(result.id, result.attributes, result.references));
    return {
      page: results.page,
      perPage: results.per_page,
      total: results.total,
      data
    };
  }

  async delete({
    id
  }) {
    const alertSavedObject = await this.savedObjectsClient.get('alert', id);
    const removeResult = await this.savedObjectsClient.delete('alert', id);

    if (alertSavedObject.attributes.scheduledTaskId) {
      await this.taskManager.remove(alertSavedObject.attributes.scheduledTaskId);
    }

    return removeResult;
  }

  async update({
    id,
    data,
    options = {}
  }) {
    const existingObject = await this.savedObjectsClient.get('alert', id);
    const {
      alertTypeId
    } = existingObject.attributes;
    const alertType = this.alertTypeRegistry.get(alertTypeId);
    const apiKey = await this.createAPIKey(); // Validate

    const validatedAlertTypeParams = (0, _lib.validateAlertTypeParams)(alertType, data.alertTypeParams);
    const {
      actions,
      references
    } = this.extractReferences(data.actions);
    const username = await this.getUserName();
    const updatedObject = await this.savedObjectsClient.update('alert', id, { ...data,
      alertTypeParams: validatedAlertTypeParams,
      actions,
      updatedBy: username,
      apiKeyOwner: apiKey.created ? username : null,
      apiKey: apiKey.created ? Buffer.from(`${apiKey.result.id}:${apiKey.result.api_key}`).toString('base64') : null
    }, { ...options,
      references
    });
    return this.getAlertFromRaw(id, updatedObject.attributes, updatedObject.references);
  }

  async enable({
    id
  }) {
    const existingObject = await this.savedObjectsClient.get('alert', id);

    if (existingObject.attributes.enabled === false) {
      const apiKey = await this.createAPIKey();
      const scheduledTask = await this.scheduleAlert(id, existingObject.attributes.alertTypeId, existingObject.attributes.interval);
      const username = await this.getUserName();
      await this.savedObjectsClient.update('alert', id, {
        enabled: true,
        updatedBy: username,
        apiKeyOwner: apiKey.created ? username : null,
        scheduledTaskId: scheduledTask.id,
        apiKey: apiKey.created ? Buffer.from(`${apiKey.result.id}:${apiKey.result.api_key}`).toString('base64') : null
      }, {
        references: existingObject.references
      });
    }
  }

  async disable({
    id
  }) {
    const existingObject = await this.savedObjectsClient.get('alert', id);

    if (existingObject.attributes.enabled === true) {
      await this.savedObjectsClient.update('alert', id, {
        enabled: false,
        scheduledTaskId: null,
        apiKey: null,
        apiKeyOwner: null,
        updatedBy: await this.getUserName()
      }, {
        references: existingObject.references
      });
      await this.taskManager.remove(existingObject.attributes.scheduledTaskId);
    }
  }

  async scheduleAlert(id, alertTypeId, interval) {
    return await this.taskManager.schedule({
      taskType: `alerting:${alertTypeId}`,
      params: {
        alertId: id,
        spaceId: this.spaceId
      },
      state: {
        previousStartedAt: null,
        alertTypeState: {},
        alertInstances: {}
      },
      scope: ['alerting']
    });
  }

  extractReferences(actions) {
    const references = [];
    const rawActions = actions.map((action, i) => {
      const actionRef = `action_${i}`;
      references.push({
        name: actionRef,
        type: 'action',
        id: action.id
      });
      return { ...(0, _lodash.omit)(action, 'id'),
        actionRef
      };
    });
    return {
      actions: rawActions,
      references
    };
  }

  injectReferencesIntoActions(actions, references) {
    return actions.map((action, i) => {
      const reference = references.find(ref => ref.name === action.actionRef);

      if (!reference) {
        throw new Error(`Reference ${action.actionRef} not found`);
      }

      return { ...(0, _lodash.omit)(action, 'actionRef'),
        id: reference.id
      };
    });
  }

  getAlertFromRaw(id, rawAlert, references) {
    if (!rawAlert.actions) {
      return {
        id,
        ...rawAlert
      };
    }

    const actions = this.injectReferencesIntoActions(rawAlert.actions, references || []);
    return {
      id,
      ...rawAlert,
      actions
    };
  }

  getRawAlert(alert) {
    const {
      references,
      actions
    } = this.extractReferences(alert.actions);
    return {
      alert: { ...alert,
        actions
      },
      references
    };
  }

}

exports.AlertsClient = AlertsClient;