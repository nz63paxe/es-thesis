"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AlertTypeRegistry = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _i18n = require("@kbn/i18n");

var _lib = require("./lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class AlertTypeRegistry {
  constructor({
    encryptedSavedObjectsPlugin,
    executeAction,
    taskManager,
    getServices,
    spaceIdToNamespace,
    getBasePath,
    isSecurityEnabled
  }) {
    _defineProperty(this, "getServices", void 0);

    _defineProperty(this, "taskManager", void 0);

    _defineProperty(this, "executeAction", void 0);

    _defineProperty(this, "alertTypes", new Map());

    _defineProperty(this, "encryptedSavedObjectsPlugin", void 0);

    _defineProperty(this, "spaceIdToNamespace", void 0);

    _defineProperty(this, "getBasePath", void 0);

    _defineProperty(this, "isSecurityEnabled", void 0);

    this.taskManager = taskManager;
    this.executeAction = executeAction;
    this.encryptedSavedObjectsPlugin = encryptedSavedObjectsPlugin;
    this.getServices = getServices;
    this.getBasePath = getBasePath;
    this.spaceIdToNamespace = spaceIdToNamespace;
    this.isSecurityEnabled = isSecurityEnabled;
  }

  has(id) {
    return this.alertTypes.has(id);
  }

  register(alertType) {
    if (this.has(alertType.id)) {
      throw new Error(_i18n.i18n.translate('xpack.alerting.alertTypeRegistry.register.duplicateAlertTypeError', {
        defaultMessage: 'Alert type "{id}" is already registered.',
        values: {
          id: alertType.id
        }
      }));
    }

    this.alertTypes.set(alertType.id, alertType);
    this.taskManager.registerTaskDefinitions({
      [`alerting:${alertType.id}`]: {
        title: alertType.name,
        type: `alerting:${alertType.id}`,
        createTaskRunner: (0, _lib.getCreateTaskRunnerFunction)({
          alertType,
          isSecurityEnabled: this.isSecurityEnabled,
          getServices: this.getServices,
          executeAction: this.executeAction,
          encryptedSavedObjectsPlugin: this.encryptedSavedObjectsPlugin,
          getBasePath: this.getBasePath,
          spaceIdToNamespace: this.spaceIdToNamespace
        })
      }
    });
  }

  get(id) {
    if (!this.has(id)) {
      throw _boom.default.badRequest(_i18n.i18n.translate('xpack.alerting.alertTypeRegistry.get.missingAlertTypeError', {
        defaultMessage: 'Alert type "{id}" is not registered.',
        values: {
          id
        }
      }));
    }

    return this.alertTypes.get(id);
  }

  list() {
    return Array.from(this.alertTypes).map(([alertTypeId, alertType]) => ({
      id: alertTypeId,
      name: alertType.name
    }));
  }

}

exports.AlertTypeRegistry = AlertTypeRegistry;