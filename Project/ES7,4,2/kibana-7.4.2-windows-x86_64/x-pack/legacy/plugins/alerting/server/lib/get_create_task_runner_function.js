"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCreateTaskRunnerFunction = getCreateTaskRunnerFunction;

var _create_fire_handler = require("./create_fire_handler");

var _create_alert_instance_factory = require("./create_alert_instance_factory");

var _alert_instance = require("./alert_instance");

var _get_next_run_at = require("./get_next_run_at");

var _validate_alert_type_params = require("./validate_alert_type_params");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getCreateTaskRunnerFunction({
  getServices,
  alertType,
  executeAction,
  encryptedSavedObjectsPlugin,
  spaceIdToNamespace,
  getBasePath,
  isSecurityEnabled
}) {
  return ({
    taskInstance
  }) => {
    return {
      run: async () => {
        const {
          alertId,
          spaceId
        } = taskInstance.params;
        const requestHeaders = {};
        const namespace = spaceIdToNamespace(spaceId); // Only fetch encrypted attributes here, we'll create a saved objects client
        // scoped with the API key to fetch the remaining data.

        const {
          attributes: {
            apiKey
          }
        } = await encryptedSavedObjectsPlugin.getDecryptedAsInternalUser('alert', alertId, {
          namespace
        });

        if (isSecurityEnabled && !apiKey) {
          throw new Error('API key is required. The attribute "apiKey" is missing.');
        } else if (isSecurityEnabled) {
          requestHeaders.authorization = `ApiKey ${apiKey}`;
        }

        const fakeRequest = {
          headers: requestHeaders,
          getBasePath: () => getBasePath(spaceId)
        };
        const services = getServices(fakeRequest); // Ensure API key is still valid and user has access

        const {
          attributes: {
            alertTypeParams,
            actions,
            interval
          },
          references
        } = await services.savedObjectsClient.get('alert', alertId); // Validate

        const validatedAlertTypeParams = (0, _validate_alert_type_params.validateAlertTypeParams)(alertType, alertTypeParams); // Inject ids into actions

        const actionsWithIds = actions.map(action => {
          const actionReference = references.find(obj => obj.name === action.actionRef);

          if (!actionReference) {
            throw new Error(`Action reference "${action.actionRef}" not found in alert id: ${alertId}`);
          }

          return { ...action,
            id: actionReference.id
          };
        });
        const fireHandler = (0, _create_fire_handler.createFireHandler)({
          executeAction,
          apiKey,
          actions: actionsWithIds,
          spaceId
        });
        const alertInstances = {};
        const alertInstancesData = taskInstance.state.alertInstances || {};

        for (const id of Object.keys(alertInstancesData)) {
          alertInstances[id] = new _alert_instance.AlertInstance(alertInstancesData[id]);
        }

        const alertInstanceFactory = (0, _create_alert_instance_factory.createAlertInstanceFactory)(alertInstances);
        const alertTypeServices = { ...services,
          alertInstanceFactory
        };
        const alertTypeState = await alertType.executor({
          services: alertTypeServices,
          params: validatedAlertTypeParams,
          state: taskInstance.state.alertTypeState || {},
          startedAt: taskInstance.startedAt,
          previousStartedAt: taskInstance.state.previousStartedAt
        });
        await Promise.all(Object.keys(alertInstances).map(alertInstanceId => {
          const alertInstance = alertInstances[alertInstanceId]; // Unpersist any alert instances that were not explicitly fired in this alert execution

          if (!alertInstance.shouldFire()) {
            delete alertInstances[alertInstanceId];
            return;
          }

          const {
            actionGroup,
            context,
            state
          } = alertInstance.getFireOptions();
          alertInstance.replaceMeta({
            lastFired: Date.now()
          });
          alertInstance.resetFire();
          return fireHandler(actionGroup, context, state);
        }));
        const nextRunAt = (0, _get_next_run_at.getNextRunAt)(new Date(taskInstance.startedAt), interval);
        return {
          state: {
            alertTypeState,
            alertInstances,
            previousStartedAt: taskInstance.startedAt
          },
          runAt: nextRunAt
        };
      }
    };
  };
}