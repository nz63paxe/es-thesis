"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.disableAlertRoute = disableAlertRoute;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function disableAlertRoute(server) {
  server.route({
    method: 'POST',
    path: '/api/alert/{id}/_disable',
    options: {
      tags: ['access:alerting-all'],
      response: {
        emptyStatusCode: 204
      }
    },

    async handler(request, h) {
      const alertsClient = request.getAlertsClient();
      await alertsClient.disable({
        id: request.params.id
      });
      return h.response();
    }

  });
}