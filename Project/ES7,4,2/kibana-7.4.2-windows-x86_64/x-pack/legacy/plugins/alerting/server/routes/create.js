"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAlertRoute = createAlertRoute;

var _joi = _interopRequireDefault(require("joi"));

var _lib = require("../lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createAlertRoute(server) {
  server.route({
    method: 'POST',
    path: '/api/alert',
    options: {
      tags: ['access:alerting-all'],
      validate: {
        options: {
          abortEarly: false
        },
        payload: _joi.default.object().keys({
          enabled: _joi.default.boolean().default(true),
          alertTypeId: _joi.default.string().required(),
          interval: _joi.default.alternatives().try(_joi.default.string().regex(_lib.SECONDS_REGEX, 'seconds (5s)').required(), _joi.default.string().regex(_lib.MINUTES_REGEX, 'minutes (5m)').required(), _joi.default.string().regex(_lib.HOURS_REGEX, 'hours (5h)').required(), _joi.default.string().regex(_lib.DAYS_REGEX, 'days (5d)').required()).required(),
          alertTypeParams: _joi.default.object().required(),
          actions: _joi.default.array().items(_joi.default.object().keys({
            group: _joi.default.string().required(),
            id: _joi.default.string().required(),
            params: _joi.default.object().required()
          })).required()
        }).required()
      }
    },

    async handler(request) {
      const alertsClient = request.getAlertsClient();
      return await alertsClient.create({
        data: request.payload
      });
    }

  });
}