"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AlertInstance = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class AlertInstance {
  constructor({
    state = {},
    meta = {}
  } = {}) {
    _defineProperty(this, "fireOptions", void 0);

    _defineProperty(this, "meta", void 0);

    _defineProperty(this, "state", void 0);

    this.state = state;
    this.meta = meta;
  }

  shouldFire() {
    return this.fireOptions !== undefined;
  }

  getFireOptions() {
    return this.fireOptions;
  }

  resetFire() {
    this.fireOptions = undefined;
    return this;
  }

  getState() {
    return this.state;
  }

  getMeta() {
    return this.meta;
  }

  fire(actionGroup, context = {}) {
    if (this.shouldFire()) {
      throw new Error('Alert instance already fired, cannot fire twice');
    }

    this.fireOptions = {
      actionGroup,
      context,
      state: this.state
    };
    return this;
  }

  replaceState(state) {
    this.state = state;
    return this;
  }

  replaceMeta(meta) {
    this.meta = meta;
    return this;
  }
  /**
   * Used to serialize alert instance state
   */


  toJSON() {
    return {
      state: this.state,
      meta: this.meta
    };
  }

}

exports.AlertInstance = AlertInstance;