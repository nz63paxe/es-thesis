"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listAlertTypesRoute = listAlertTypesRoute;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function listAlertTypesRoute(server) {
  server.route({
    method: 'GET',
    path: `/api/alert/types`,
    options: {
      tags: ['access:alerting-read']
    },

    async handler(request) {
      return request.server.plugins.alerting.listTypes();
    }

  });
}