"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseDuration = parseDuration;
exports.DAYS_REGEX = exports.HOURS_REGEX = exports.MINUTES_REGEX = exports.SECONDS_REGEX = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const SECONDS_REGEX = /^[1-9][0-9]*s$/;
exports.SECONDS_REGEX = SECONDS_REGEX;
const MINUTES_REGEX = /^[1-9][0-9]*m$/;
exports.MINUTES_REGEX = MINUTES_REGEX;
const HOURS_REGEX = /^[1-9][0-9]*h$/;
exports.HOURS_REGEX = HOURS_REGEX;
const DAYS_REGEX = /^[1-9][0-9]*d$/;
exports.DAYS_REGEX = DAYS_REGEX;

function parseDuration(duration) {
  const parsed = parseInt(duration, 10);

  if (isSeconds(duration)) {
    return parsed * 1000;
  } else if (isMinutes(duration)) {
    return parsed * 60 * 1000;
  } else if (isHours(duration)) {
    return parsed * 60 * 60 * 1000;
  } else if (isDays(duration)) {
    return parsed * 24 * 60 * 60 * 1000;
  }

  throw new Error(`Invalid duration "${duration}". Durations must be of the form {number}x. Example: 5s, 5m, 5h or 5d"`);
}

function isSeconds(duration) {
  return SECONDS_REGEX.test(duration);
}

function isMinutes(duration) {
  return MINUTES_REGEX.test(duration);
}

function isHours(duration) {
  return HOURS_REGEX.test(duration);
}

function isDays(duration) {
  return DAYS_REGEX.test(duration);
}