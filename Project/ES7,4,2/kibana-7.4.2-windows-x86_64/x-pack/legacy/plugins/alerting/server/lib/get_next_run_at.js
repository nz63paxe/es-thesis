"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNextRunAt = getNextRunAt;

var _parse_duration = require("./parse_duration");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getNextRunAt(currentRunAt, interval) {
  let nextRunAt = currentRunAt.getTime() + (0, _parse_duration.parseDuration)(interval);

  if (nextRunAt < Date.now()) {
    // To prevent returning dates in the past, we'll return now instead
    nextRunAt = Date.now();
  }

  return new Date(nextRunAt);
}