"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFireHandler = createFireHandler;

var _transform_action_params = require("./transform_action_params");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createFireHandler({
  executeAction,
  actions: alertActions,
  spaceId,
  apiKey
}) {
  return async (actionGroup, context, state) => {
    const actions = alertActions.filter(({
      group
    }) => group === actionGroup).map(action => {
      return { ...action,
        params: (0, _transform_action_params.transformActionParams)(action.params, state, context)
      };
    });

    for (const action of actions) {
      await executeAction({
        id: action.id,
        params: action.params,
        spaceId,
        apiKey
      });
    }
  };
}