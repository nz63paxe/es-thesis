"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMockServer = createMockServer;

var _hapi = _interopRequireDefault(require("hapi"));

var _alerts_client = require("../alerts_client.mock");

var _alert_type_registry = require("../alert_type_registry.mock");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const defaultConfig = {
  'kibana.index': '.kibana'
};

function createMockServer(config = defaultConfig) {
  const server = new _hapi.default.Server({
    port: 0
  });

  const alertsClient = _alerts_client.alertsClientMock.create();

  const alertTypeRegistry = _alert_type_registry.alertTypeRegistryMock.create();

  server.config = () => {
    return {
      get(key) {
        return config[key];
      },

      has(key) {
        return config.hasOwnProperty(key);
      }

    };
  };

  server.register({
    name: 'alerting',

    register(pluginServer) {
      pluginServer.expose('registerType', alertTypeRegistry.register);
      pluginServer.expose('listTypes', alertTypeRegistry.list);
    }

  });
  server.decorate('request', 'getAlertsClient', () => alertsClient);
  server.decorate('request', 'getBasePath', () => '/s/default');
  return {
    server,
    alertsClient,
    alertTypeRegistry
  };
}