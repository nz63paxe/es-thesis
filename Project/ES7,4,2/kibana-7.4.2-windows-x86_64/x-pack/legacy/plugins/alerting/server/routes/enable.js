"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.enableAlertRoute = enableAlertRoute;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function enableAlertRoute(server) {
  server.route({
    method: 'POST',
    path: '/api/alert/{id}/_enable',
    options: {
      tags: ['access:alerting-all'],
      response: {
        emptyStatusCode: 204
      }
    },

    async handler(request, h) {
      const alertsClient = request.getAlertsClient();
      await alertsClient.enable({
        id: request.params.id
      });
      return h.response();
    }

  });
}