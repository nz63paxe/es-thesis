"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.siem = siem;

var _i18n = require("@kbn/i18n");

var _path = require("path");

var _kibana = require("./server/kibana.index");

var _saved_objects = require("./server/saved_objects");

var _constants = require("./common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
function siem(kibana) {
  return new kibana.Plugin({
    id: _constants.APP_ID,
    configPrefix: 'xpack.siem',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    require: ['kibana', 'elasticsearch'],
    uiExports: {
      app: {
        description: _i18n.i18n.translate('xpack.siem.securityDescription', {
          defaultMessage: 'Explore your SIEM App'
        }),
        main: 'plugins/siem/app',
        euiIconType: 'securityAnalyticsApp',
        title: _constants.APP_NAME,
        listed: false,
        url: `/app/${_constants.APP_ID}`
      },
      home: ['plugins/siem/register_feature'],
      links: [{
        description: _i18n.i18n.translate('xpack.siem.linkSecurityDescription', {
          defaultMessage: 'Explore your SIEM App'
        }),
        euiIconType: 'securityAnalyticsApp',
        id: 'siem',
        order: 9000,
        title: _constants.APP_NAME,
        url: `/app/${_constants.APP_ID}`
      }],
      uiSettingDefaults: {
        [_constants.DEFAULT_SIEM_REFRESH_INTERVAL]: {
          type: 'json',
          name: _i18n.i18n.translate('xpack.siem.uiSettings.defaultRefreshIntervalLabel', {
            defaultMessage: 'Time picker refresh interval'
          }),
          value: `{
  "pause": ${_constants.DEFAULT_INTERVAL_PAUSE},
  "value": ${_constants.DEFAULT_INTERVAL_VALUE}
}`,
          description: _i18n.i18n.translate('xpack.siem.uiSettings.defaultRefreshIntervalDescription', {
            defaultMessage: "The SIEM timefilter's default refresh interval"
          }),
          category: ['siem'],
          requiresPageReload: true
        },
        [_constants.DEFAULT_SIEM_TIME_RANGE]: {
          type: 'json',
          name: _i18n.i18n.translate('xpack.siem.uiSettings.defaultTimeRangeLabel', {
            defaultMessage: 'Time picker defaults'
          }),
          value: `{
  "from": "${_constants.DEFAULT_FROM}",
  "to": "${_constants.DEFAULT_TO}"
}`,
          description: _i18n.i18n.translate('xpack.siem.uiSettings.defaultTimeRangeDescription', {
            defaultMessage: 'The SIEM timefilter selection to use when Kibana is started without one'
          }),
          category: ['siem'],
          requiresPageReload: true
        },
        [_constants.DEFAULT_INDEX_KEY]: {
          name: _i18n.i18n.translate('xpack.siem.uiSettings.defaultIndexLabel', {
            defaultMessage: 'Default index'
          }),
          value: ['auditbeat-*', 'filebeat-*', 'packetbeat-*', 'winlogbeat-*'],
          description: _i18n.i18n.translate('xpack.siem.uiSettings.defaultIndexDescription', {
            defaultMessage: 'Default Elasticsearch index to search'
          }),
          category: ['siem'],
          requiresPageReload: true
        },
        [_constants.DEFAULT_ANOMALY_SCORE]: {
          name: _i18n.i18n.translate('xpack.siem.uiSettings.defaultAnomalyScoreLabel', {
            defaultMessage: 'Default anomaly threshold'
          }),
          value: 50,
          type: 'number',
          description: _i18n.i18n.translate('xpack.siem.uiSettings.defaultAnomalyScoreDescription', {
            defaultMessage: 'Default anomaly score threshold to exceed before showing anomalies. Valid values are between 0 and 100'
          }),
          category: ['siem'],
          requiresPageReload: true
        }
      },
      mappings: _saved_objects.savedObjectMappings
    },

    init(server) {
      server.injectUiAppVars('siem', async () => server.getInjectedUiAppVars('kibana'));
      (0, _kibana.initServerWithKibana)(server);
    }

  });
}