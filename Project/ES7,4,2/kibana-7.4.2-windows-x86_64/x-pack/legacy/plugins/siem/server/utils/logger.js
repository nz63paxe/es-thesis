"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLogger = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const LOGGING_TAGS = ['siem']; // Definition is from:
// https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/hapi/v16/index.d.ts#L318

const createLogger = logger => ({
  debug: message => logger(['debug', ...LOGGING_TAGS], message),
  info: message => logger(['info', ...LOGGING_TAGS], message),
  warn: message => logger(['warning', ...LOGGING_TAGS], message),
  error: message => logger(['error', ...LOGGING_TAGS], message)
});

exports.createLogger = createLogger;