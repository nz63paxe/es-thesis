"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertSavedObjectToSavedTimeline = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

var _types = require("./types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const convertSavedObjectToSavedTimeline = savedObject => {
  return _types.TimelineSavedObjectRuntimeType.decode(savedObject).map(savedTimeline => ({
    savedObjectId: savedTimeline.id,
    version: savedTimeline.version,
    ...savedTimeline.attributes
  })).getOrElseL(errors => {
    throw new Error((0, _PathReporter.failure)(errors).join('\n'));
  });
};

exports.convertSavedObjectToSavedTimeline = convertSavedObjectToSavedTimeline;