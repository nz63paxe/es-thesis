"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.graphiqlHapi = exports.graphqlHapi = void 0;

var GraphiQL = _interopRequireWildcard(require("apollo-server-module-graphiql"));

var _boom = _interopRequireDefault(require("boom"));

var _apolloServerCore = require("apollo-server-core");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const graphqlHapi = {
  name: 'graphql-siem',
  register: (server, options) => {
    if (!options || !options.graphqlOptions) {
      throw new Error('Apollo Server requires options.');
    }

    server.route({
      options: options.route || {},
      handler: async (request, h) => {
        try {
          const query = request.method === 'post' ? request.payload : // eslint-disable-line @typescript-eslint/no-explicit-any
          request.query; // eslint-disable-line @typescript-eslint/no-explicit-any

          const gqlResponse = await (0, _apolloServerCore.runHttpQuery)([request], {
            method: request.method.toUpperCase(),
            options: options.graphqlOptions,
            query
          });
          return h.response(gqlResponse).type('application/json');
        } catch (error) {
          if ('HttpQueryError' !== error.name) {
            const queryError = _boom.default.boomify(error);

            queryError.output.payload.message = error.message;
            return queryError;
          }

          if (error.isGraphQLError === true) {
            return h.response(error.message).code(error.statusCode).type('application/json');
          }

          const genericError = new _boom.default(error.message, {
            statusCode: error.statusCode
          });

          if (error.headers) {
            Object.keys(error.headers).forEach(header => {
              genericError.output.headers[header] = error.headers[header];
            });
          } // Boom hides the error when status code is 500


          genericError.output.payload.message = error.message;
          throw genericError;
        }
      },
      method: ['GET', 'POST'],
      path: options.path || '/graphql',
      vhost: options.vhost || undefined
    });
  }
};
exports.graphqlHapi = graphqlHapi;
const graphiqlHapi = {
  name: 'graphiql-siem',
  register: (server, options) => {
    if (!options || !options.graphiqlOptions) {
      throw new Error('Apollo Server GraphiQL requires options.');
    }

    server.route({
      options: options.route || {},
      handler: async (request, h) => {
        const graphiqlString = await GraphiQL.resolveGraphiQLString(request.query, options.graphiqlOptions, request);
        return h.response(graphiqlString).type('text/html');
      },
      method: 'GET',
      path: options.path || '/graphiql'
    });
  }
};
exports.graphiqlHapi = graphiqlHapi;