"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PinnedEvent = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

var _fp = require("lodash/fp");

var _framework = require("../framework");

var _types = require("./types");

var _saved_objects = require("../../saved_objects");

var _pick_saved_timeline = require("../timeline/pick_saved_timeline");

var _convert_saved_object_to_savedtimeline = require("../timeline/convert_saved_object_to_savedtimeline");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class PinnedEvent {
  constructor(libs) {
    this.libs = libs;
  }

  async deletePinnedEventOnTimeline(request, pinnedEventIds) {
    await Promise.all(pinnedEventIds.map(pinnedEventId => this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).delete(_saved_objects.pinnedEventSavedObjectType, pinnedEventId)));
  }

  async deleteAllPinnedEventsOnTimeline(request, timelineId) {
    const options = {
      type: _saved_objects.pinnedEventSavedObjectType,
      search: timelineId,
      searchFields: ['timelineId']
    };
    const pinnedEventToBeDeleted = await this.getAllSavedPinnedEvents(request, options);
    await Promise.all(pinnedEventToBeDeleted.map(pinnedEvent => this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).delete(_saved_objects.pinnedEventSavedObjectType, pinnedEvent.pinnedEventId)));
  }

  async getPinnedEvent(request, pinnedEventId) {
    return await this.getSavedPinnedEvent(request, pinnedEventId);
  }

  async getAllPinnedEventsByTimelineId(request, timelineId) {
    const options = {
      type: _saved_objects.pinnedEventSavedObjectType,
      search: timelineId,
      searchFields: ['timelineId']
    };
    return await this.getAllSavedPinnedEvents(request, options);
  }

  async getAllPinnedEvents(request, pageInfo, search, sort) {
    const options = {
      type: _saved_objects.pinnedEventSavedObjectType,
      perPage: pageInfo != null ? pageInfo.pageSize : undefined,
      page: pageInfo != null ? pageInfo.pageIndex : undefined,
      search: search != null ? search : undefined,
      searchFields: ['timelineId', 'eventId'],
      sortField: sort != null ? sort.sortField : undefined,
      sortOrder: sort != null ? sort.sortOrder : undefined
    };
    return await this.getAllSavedPinnedEvents(request, options);
  }

  async persistPinnedEventOnTimeline(request, pinnedEventId, eventId, timelineId) {
    try {
      if (pinnedEventId == null) {
        const timelineVersionSavedObject = timelineId == null ? await (async () => {
          const timelineResult = (0, _convert_saved_object_to_savedtimeline.convertSavedObjectToSavedTimeline)((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).create(_saved_objects.timelineSavedObjectType, (0, _pick_saved_timeline.pickSavedTimeline)(null, {}, request[_framework.internalFrameworkRequest].auth || null))));
          timelineId = timelineResult.savedObjectId;
          return timelineResult.version;
        })() : null;

        if (timelineId != null) {
          const allPinnedEventId = await this.getAllPinnedEventsByTimelineId(request, timelineId);
          const isPinnedAlreadyExisting = allPinnedEventId.filter(pinnedEvent => pinnedEvent.eventId === eventId);

          if (isPinnedAlreadyExisting.length === 0) {
            const savedPinnedEvent = {
              eventId,
              timelineId
            }; // create Pinned Event on Timeline

            return convertSavedObjectToSavedPinnedEvent((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).create(_saved_objects.pinnedEventSavedObjectType, pickSavedPinnedEvent(pinnedEventId, savedPinnedEvent, request[_framework.internalFrameworkRequest].auth || null))), timelineVersionSavedObject != null ? timelineVersionSavedObject : undefined);
          }

          return isPinnedAlreadyExisting[0];
        }

        throw new Error('You can NOT pinned event without a timelineID');
      } // Delete Pinned Event on Timeline


      await this.deletePinnedEventOnTimeline(request, [pinnedEventId]);
      return null;
    } catch (err) {
      if ((0, _fp.getOr)(null, 'output.statusCode', err) === 404) {
        /*
         * Why we are doing that, because if it is not found for sure that it will be unpinned
         * There is no need to bring back this error since we can assume that it is unpinned
         */
        return null;
      }

      if ((0, _fp.getOr)(null, 'output.statusCode', err) === 403) {
        return pinnedEventId != null ? {
          code: 403,
          message: err.message,
          pinnedEventId: eventId,
          timelineId: '',
          timelineVersion: ''
        } : null;
      }

      throw err;
    }
  }

  async getSavedPinnedEvent(request, pinnedEventId) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]);
    const savedObject = await savedObjectsClient.get(_saved_objects.pinnedEventSavedObjectType, pinnedEventId);
    return convertSavedObjectToSavedPinnedEvent(savedObject);
  }

  async getAllSavedPinnedEvents(request, options) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]);
    const savedObjects = await savedObjectsClient.find(options);
    return savedObjects.saved_objects.map(savedObject => convertSavedObjectToSavedPinnedEvent(savedObject));
  }

}

exports.PinnedEvent = PinnedEvent;

const convertSavedObjectToSavedPinnedEvent = (savedObject, timelineVersion) => _types.PinnedEventSavedObjectRuntimeType.decode(savedObject).map(savedPinnedEvent => ({
  pinnedEventId: savedPinnedEvent.id,
  version: savedPinnedEvent.version,
  timelineVersion,
  ...savedPinnedEvent.attributes
})).getOrElseL(errors => {
  throw new Error((0, _PathReporter.failure)(errors).join('\n'));
}); // we have to use any here because the SavedObjectAttributes interface is like below
// export interface SavedObjectAttributes {
//   [key: string]: SavedObjectAttributes | string | number | boolean | null;
// }
// then this interface does not allow types without index signature
// this is limiting us with our type for now so the easy way was to use any


const pickSavedPinnedEvent = (pinnedEventId, savedPinnedEvent, userInfo) => {
  if (pinnedEventId == null) {
    savedPinnedEvent.created = new Date().valueOf();
    savedPinnedEvent.createdBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
    savedPinnedEvent.updated = new Date().valueOf();
    savedPinnedEvent.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  } else if (pinnedEventId != null) {
    savedPinnedEvent.updated = new Date().valueOf();
    savedPinnedEvent.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  }

  return savedPinnedEvent;
};