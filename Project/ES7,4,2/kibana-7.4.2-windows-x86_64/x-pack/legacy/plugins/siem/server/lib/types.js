"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _hosts = require("./hosts");

Object.keys(_hosts).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _hosts[key];
    }
  });
});