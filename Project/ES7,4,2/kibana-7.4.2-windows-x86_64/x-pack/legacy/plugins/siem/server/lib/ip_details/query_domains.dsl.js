"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildDomainsQuery = void 0;

var _types = require("../../graphql/types");

var _build_query = require("../../utils/build_query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getAggs = (ip, flowTarget, flowDirection, domainsSortField, querySize) => {
  return {
    domain_count: {
      cardinality: {
        field: `${flowTarget}.domain`
      }
    },
    [`${flowTarget}_domains`]: {
      terms: {
        field: `${flowTarget}.domain`,
        size: querySize,
        order: { ...getQueryOrder(domainsSortField)
        }
      },
      aggs: {
        lastSeen: {
          max: {
            field: '@timestamp'
          }
        },
        bytes: {
          sum: {
            field: flowDirection === _types.FlowDirection.uniDirectional ? 'network.bytes' : `${flowTarget}.bytes`
          }
        },
        direction: {
          terms: {
            field: 'network.direction'
          }
        },
        uniqueIpCount: {
          cardinality: {
            field: `${getOppositeField(flowTarget)}.ip`
          }
        },
        packets: {
          sum: {
            field: flowDirection === _types.FlowDirection.uniDirectional ? 'network.packets' : `${flowTarget}.packets`
          }
        }
      }
    }
  };
};

const getUniDirectionalFilter = flowDirection => flowDirection === _types.FlowDirection.uniDirectional ? {
  must_not: [{
    exists: {
      field: 'destination.bytes'
    }
  }]
} : {};

const getBiDirectionalFilter = (flowDirection, flowTarget) => {
  if (flowDirection === _types.FlowDirection.biDirectional && [_types.FlowTarget.source, _types.FlowTarget.destination].includes(flowTarget)) {
    return [{
      exists: {
        field: 'source.bytes'
      }
    }, {
      exists: {
        field: 'destination.bytes'
      }
    }];
  } else if (flowDirection === _types.FlowDirection.biDirectional && [_types.FlowTarget.client, _types.FlowTarget.server].includes(flowTarget)) {
    return [{
      exists: {
        field: 'client.bytes'
      }
    }, {
      exists: {
        field: 'server.bytes'
      }
    }];
  }

  return [];
};

const buildDomainsQuery = ({
  ip,
  domainsSortField,
  filterQuery,
  flowDirection,
  flowTarget,
  pagination: {
    querySize
  },
  defaultIndex,
  sourceConfiguration: {
    fields: {
      timestamp
    }
  },
  timerange: {
    from,
    to
  }
}) => {
  const filter = [...(0, _build_query.createQueryFilterClauses)(filterQuery), {
    range: {
      [timestamp]: {
        gte: from,
        lte: to
      }
    }
  }, {
    term: {
      [`${flowTarget}.ip`]: ip
    }
  }, ...getBiDirectionalFilter(flowDirection, flowTarget)];
  const dslQuery = {
    allowNoIndices: true,
    index: defaultIndex,
    ignoreUnavailable: true,
    body: {
      aggs: { ...getAggs(ip, flowTarget, flowDirection, domainsSortField, querySize)
      },
      query: {
        bool: {
          filter,
          ...getUniDirectionalFilter(flowDirection)
        }
      },
      size: 0,
      track_total_hits: false
    }
  };
  return dslQuery;
};

exports.buildDomainsQuery = buildDomainsQuery;

const getOppositeField = flowTarget => {
  switch (flowTarget) {
    case _types.FlowTarget.source:
      return _types.FlowTarget.destination;

    case _types.FlowTarget.destination:
      return _types.FlowTarget.source;

    case _types.FlowTarget.server:
      return _types.FlowTarget.client;

    case _types.FlowTarget.client:
      return _types.FlowTarget.server;

    default:
      return (0, _build_query.assertUnreachable)(flowTarget);
  }
};

const getQueryOrder = domainsSortField => {
  switch (domainsSortField.field) {
    case _types.DomainsFields.bytes:
      return {
        bytes: domainsSortField.direction
      };

    case _types.DomainsFields.packets:
      return {
        packets: domainsSortField.direction
      };

    case _types.DomainsFields.uniqueIpCount:
      return {
        uniqueIpCount: domainsSortField.direction
      };

    case _types.DomainsFields.domainName:
      return {
        _key: domainsSortField.direction
      };

    case _types.DomainsFields.direction:
      return {
        _key: domainsSortField.direction
      };

    default:
      return (0, _build_query.assertUnreachable)(domainsSortField.field);
  }
};