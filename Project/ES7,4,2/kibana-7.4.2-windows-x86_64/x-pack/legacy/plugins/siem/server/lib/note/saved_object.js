"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Note = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

var _fp = require("lodash/fp");

var _uuid = _interopRequireDefault(require("uuid"));

var _framework = require("../framework");

var _types = require("./types");

var _saved_object_mappings = require("./saved_object_mappings");

var _saved_objects = require("../../saved_objects");

var _pick_saved_timeline = require("../timeline/pick_saved_timeline");

var _convert_saved_object_to_savedtimeline = require("../timeline/convert_saved_object_to_savedtimeline");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class Note {
  constructor(libs) {
    this.libs = libs;
  }

  async deleteNote(request, noteIds) {
    await Promise.all(noteIds.map(noteId => this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).delete(_saved_object_mappings.noteSavedObjectType, noteId)));
  }

  async deleteNoteByTimelineId(request, timelineId) {
    const options = {
      type: _saved_object_mappings.noteSavedObjectType,
      search: timelineId,
      searchFields: ['timelineId']
    };
    const notesToBeDeleted = await this.getAllSavedNote(request, options);
    await Promise.all(notesToBeDeleted.notes.map(note => this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).delete(_saved_object_mappings.noteSavedObjectType, note.noteId)));
  }

  async getNote(request, noteId) {
    return await this.getSavedNote(request, noteId);
  }

  async getNotesByEventId(request, eventId) {
    const options = {
      type: _saved_object_mappings.noteSavedObjectType,
      search: eventId,
      searchFields: ['eventId']
    };
    const notesByEventId = await this.getAllSavedNote(request, options);
    return notesByEventId.notes;
  }

  async getNotesByTimelineId(request, timelineId) {
    const options = {
      type: _saved_object_mappings.noteSavedObjectType,
      search: timelineId,
      searchFields: ['timelineId']
    };
    const notesByTimelineId = await this.getAllSavedNote(request, options);
    return notesByTimelineId.notes;
  }

  async getAllNotes(request, pageInfo, search, sort) {
    const options = {
      type: _saved_object_mappings.noteSavedObjectType,
      perPage: pageInfo != null ? pageInfo.pageSize : undefined,
      page: pageInfo != null ? pageInfo.pageIndex : undefined,
      search: search != null ? search : undefined,
      searchFields: ['note'],
      sortField: sort != null ? sort.sortField : undefined,
      sortOrder: sort != null ? sort.sortOrder : undefined
    };
    return await this.getAllSavedNote(request, options);
  }

  async persistNote(request, noteId, version, note) {
    try {
      if (noteId == null) {
        const timelineVersionSavedObject = note.timelineId == null ? await (async () => {
          const timelineResult = (0, _convert_saved_object_to_savedtimeline.convertSavedObjectToSavedTimeline)((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).create(_saved_objects.timelineSavedObjectType, (0, _pick_saved_timeline.pickSavedTimeline)(null, {}, request[_framework.internalFrameworkRequest].auth || null))));
          note.timelineId = timelineResult.savedObjectId;
          return timelineResult.version;
        })() : null; // Create new note

        return {
          code: 200,
          message: 'success',
          note: convertSavedObjectToSavedNote((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).create(_saved_object_mappings.noteSavedObjectType, pickSavedNote(noteId, note, request[_framework.internalFrameworkRequest].auth || null))), timelineVersionSavedObject != null ? timelineVersionSavedObject : undefined)
        };
      } // Update new note


      return {
        code: 200,
        message: 'success',
        note: convertSavedObjectToSavedNote((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]).update(_saved_object_mappings.noteSavedObjectType, noteId, pickSavedNote(noteId, note, request[_framework.internalFrameworkRequest].auth || null), {
          version: version || undefined
        })))
      };
    } catch (err) {
      if ((0, _fp.getOr)(null, 'output.statusCode', err) === 403) {
        const noteToReturn = { ...note,
          noteId: _uuid.default.v1(),
          version: '',
          timelineId: '',
          timelineVersion: ''
        };
        return {
          code: 403,
          message: err.message,
          note: noteToReturn
        };
      }

      throw err;
    }
  }

  async getSavedNote(request, NoteId) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]);
    const savedObject = await savedObjectsClient.get(_saved_object_mappings.noteSavedObjectType, NoteId);
    return convertSavedObjectToSavedNote(savedObject);
  }

  async getAllSavedNote(request, options) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalFrameworkRequest]);
    const savedObjects = await savedObjectsClient.find(options);
    return {
      totalCount: savedObjects.total,
      notes: savedObjects.saved_objects.map(savedObject => convertSavedObjectToSavedNote(savedObject))
    };
  }

}

exports.Note = Note;

const convertSavedObjectToSavedNote = (savedObject, timelineVersion) => _types.NoteSavedObjectRuntimeType.decode(savedObject).map(savedNote => ({
  noteId: savedNote.id,
  version: savedNote.version,
  timelineVersion,
  ...savedNote.attributes
})).getOrElseL(errors => {
  throw new Error((0, _PathReporter.failure)(errors).join('\n'));
}); // we have to use any here because the SavedObjectAttributes interface is like below
// export interface SavedObjectAttributes {
//   [key: string]: SavedObjectAttributes | string | number | boolean | null;
// }
// then this interface does not allow types without index signature
// this is limiting us with our type for now so the easy way was to use any


const pickSavedNote = (noteId, savedNote, userInfo) => {
  if (noteId == null) {
    savedNote.created = new Date().valueOf();
    savedNote.createdBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
    savedNote.updated = new Date().valueOf();
    savedNote.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  } else if (noteId != null) {
    savedNote.updated = new Date().valueOf();
    savedNote.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  }

  return savedNote;
};