"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pickSavedTimeline = void 0;

var _fp = require("lodash/fp");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const pickSavedTimeline = (timelineId, savedTimeline, userInfo) => {
  if (timelineId == null) {
    savedTimeline.created = new Date().valueOf();
    savedTimeline.createdBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
    savedTimeline.updated = new Date().valueOf();
    savedTimeline.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  } else if (timelineId != null) {
    savedTimeline.updated = new Date().valueOf();
    savedTimeline.updatedBy = (0, _fp.getOr)(null, 'credentials.username', userInfo);
  }

  return savedTimeline;
};

exports.pickSavedTimeline = pickSavedTimeline;