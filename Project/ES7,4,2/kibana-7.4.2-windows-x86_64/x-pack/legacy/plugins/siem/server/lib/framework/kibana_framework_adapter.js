"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.wrapRequest = wrapRequest;
exports.KibanaBackendFrameworkAdapter = void 0;

var _apollo_server_hapi = require("./apollo_server_hapi");

var _types = require("./types");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class KibanaBackendFrameworkAdapter {
  constructor(server) {
    this.server = server;

    _defineProperty(this, "version", void 0);

    this.version = server.config().get('pkg.version');
  }

  async callWithRequest(req, endpoint, params, // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...rest) {
    const internalRequest = req[_types.internalFrameworkRequest];
    const {
      elasticsearch
    } = internalRequest.server.plugins;
    const {
      callWithRequest
    } = elasticsearch.getCluster('data');
    const includeFrozen = await internalRequest.getUiSettingsService().get('search:includeFrozen');

    if (endpoint === 'msearch') {
      const maxConcurrentShardRequests = await internalRequest.getUiSettingsService().get('courier:maxConcurrentShardRequests');

      if (maxConcurrentShardRequests > 0) {
        params = { ...params,
          max_concurrent_shard_requests: maxConcurrentShardRequests
        };
      }
    }

    const fields = await callWithRequest(internalRequest, endpoint, { ...params,
      ignore_throttled: !includeFrozen
    }, ...rest);
    return fields;
  }

  exposeStaticDir(urlPath, dir) {
    this.server.route({
      handler: {
        directory: {
          path: dir
        }
      },
      method: 'GET',
      path: urlPath
    });
  }

  registerGraphQLEndpoint(routePath, schema) {
    this.server.register({
      options: {
        graphqlOptions: req => ({
          context: {
            req: wrapRequest(req)
          },
          schema
        }),
        path: routePath,
        route: {
          tags: ['access:siem']
        }
      },
      plugin: _apollo_server_hapi.graphqlHapi
    });
    this.server.register({
      options: {
        graphiqlOptions: {
          endpointURL: routePath,
          passHeader: `'kbn-version': '${this.version}'`
        },
        path: `${routePath}/graphiql`,
        route: {
          tags: ['access:siem']
        }
      },
      plugin: _apollo_server_hapi.graphiqlHapi
    });
  }

  getIndexPatternsService(request) {
    return this.server.indexPatternsServiceFactory({
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      callCluster: async (method, args, ...rest) => {
        const fieldCaps = await this.callWithRequest(request, method, { ...args,
          allowNoIndices: true
        }, ...rest);
        return fieldCaps;
      }
    });
  }

  getSavedObjectsService() {
    return this.server.savedObjects;
  }

}

exports.KibanaBackendFrameworkAdapter = KibanaBackendFrameworkAdapter;

function wrapRequest(req) {
  const {
    auth,
    params,
    payload,
    query
  } = req;
  return {
    [_types.internalFrameworkRequest]: req,
    auth,
    params,
    payload,
    query
  };
}