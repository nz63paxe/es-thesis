"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initServerWithKibana = exports.amMocking = void 0;

var _i18n = require("@kbn/i18n");

var _init_server = require("./init_server");

var _kibana = require("./lib/compose/kibana");

var _logger = require("./utils/logger");

var _saved_objects = require("./saved_objects");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const APP_ID = 'siem';

const amMocking = () => process.env.INGEST_MOCKS === 'true';

exports.amMocking = amMocking;

const initServerWithKibana = kbnServer => {
  // bind is so "this" binds correctly to the logger since hapi server does not auto-bind its methods
  const logger = (0, _logger.createLogger)(kbnServer.log.bind(kbnServer));
  logger.info('Plugin initializing');
  const mocking = amMocking();

  if (mocking) {
    logger.info(`Mocks for ${APP_ID} is activated. No real ${APP_ID} data will be used, only mocks will be used.`);
  }

  const libs = (0, _kibana.compose)(kbnServer);
  (0, _init_server.initServer)(libs, {
    mocking,
    logger
  });
  logger.info('Plugin done initializing');
  const xpackMainPlugin = kbnServer.plugins.xpack_main;
  xpackMainPlugin.registerFeature({
    id: APP_ID,
    name: _i18n.i18n.translate('xpack.siem.featureRegistry.linkSiemTitle', {
      defaultMessage: 'SIEM'
    }),
    icon: 'securityAnalyticsApp',
    navLinkId: 'siem',
    app: ['siem', 'kibana'],
    catalogue: ['siem'],
    privileges: {
      all: {
        api: ['siem'],
        savedObject: {
          all: [_saved_objects.noteSavedObjectType, _saved_objects.pinnedEventSavedObjectType, _saved_objects.timelineSavedObjectType],
          read: ['config']
        },
        ui: ['show']
      },
      read: {
        api: ['siem'],
        savedObject: {
          all: [],
          read: ['config', _saved_objects.noteSavedObjectType, _saved_objects.pinnedEventSavedObjectType, _saved_objects.timelineSavedObjectType]
        },
        ui: ['show']
      }
    }
  });
};

exports.initServerWithKibana = initServerWithKibana;