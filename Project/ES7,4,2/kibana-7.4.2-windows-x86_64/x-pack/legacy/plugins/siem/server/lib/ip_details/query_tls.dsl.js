"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildTlsQuery = void 0;

var _build_query = require("../../utils/build_query");

var _types = require("../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getAggs = (querySize, tlsSortField) => ({
  count: {
    cardinality: {
      field: 'tls.server_certificate.fingerprint.sha1'
    }
  },
  sha1: {
    terms: {
      field: 'tls.server_certificate.fingerprint.sha1',
      size: querySize,
      order: { ...getQueryOrder(tlsSortField)
      }
    },
    aggs: {
      issuer_names: {
        terms: {
          field: 'tls.server_certificate.issuer.common_name'
        }
      },
      common_names: {
        terms: {
          field: 'tls.server_certificate.subject.common_name'
        }
      },
      alternative_names: {
        terms: {
          field: 'tls.server_certificate.alternative_names'
        }
      },
      not_after: {
        terms: {
          field: 'tls.server_certificate.not_after'
        }
      },
      ja3: {
        terms: {
          field: 'tls.fingerprints.ja3.hash'
        }
      }
    }
  }
});

const buildTlsQuery = ({
  ip,
  tlsSortField,
  filterQuery,
  flowTarget,
  pagination: {
    querySize
  },
  defaultIndex,
  sourceConfiguration: {
    fields: {
      timestamp
    }
  },
  timerange: {
    from,
    to
  }
}) => {
  const filter = [...(0, _build_query.createQueryFilterClauses)(filterQuery), {
    range: {
      [timestamp]: {
        gte: from,
        lte: to
      }
    }
  }, {
    term: {
      [`${flowTarget}.ip`]: ip
    }
  }];
  const dslQuery = {
    allowNoIndices: true,
    index: defaultIndex,
    ignoreUnavailable: true,
    body: {
      aggs: { ...getAggs(querySize, tlsSortField)
      },
      query: {
        bool: {
          filter
        }
      },
      size: 0,
      track_total_hits: false
    }
  };
  return dslQuery;
};

exports.buildTlsQuery = buildTlsQuery;

const getQueryOrder = tlsSortField => {
  switch (tlsSortField.field) {
    case _types.TlsFields._id:
      return {
        _key: tlsSortField.direction
      };

    default:
      return (0, _build_query.assertUnreachable)(tlsSortField.field);
  }
};