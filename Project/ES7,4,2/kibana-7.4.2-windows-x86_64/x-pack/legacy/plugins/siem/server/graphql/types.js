"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SortFieldTimeline = exports.NetworkDnsFields = exports.NetworkTopNFlowFields = exports.FlowTargetNew = exports.UsersFields = exports.TlsFields = exports.NetworkDirectionEcs = exports.FlowTarget = exports.FlowDirection = exports.DomainsFields = exports.HostsFields = exports.LastEventIndexKey = exports.Direction = exports.SortFieldNote = void 0;

/* tslint:disable */

/* eslint-disable */

/*
     * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
     * or more contributor license agreements. Licensed under the Elastic License;
     * you may not use this file except in compliance with the Elastic License.
     */
// ====================================================
// START: Typescript template
// ====================================================
// ====================================================
// Scalars
// ====================================================
// ====================================================
// Types
// ====================================================

/** A set of configuration options for a security data source */

/** A mapping of semantic fields to their document counterparts */

/** The status of an infrastructure data source */

/** A descriptor of a field in an index */
// ====================================================
// InputTypes
// ====================================================
// ====================================================
// Arguments
// ====================================================
// ====================================================
// Enums
// ====================================================
let SortFieldNote;
exports.SortFieldNote = SortFieldNote;

(function (SortFieldNote) {
  SortFieldNote["updatedBy"] = "updatedBy";
  SortFieldNote["updated"] = "updated";
})(SortFieldNote || (exports.SortFieldNote = SortFieldNote = {}));

let Direction;
exports.Direction = Direction;

(function (Direction) {
  Direction["asc"] = "asc";
  Direction["desc"] = "desc";
})(Direction || (exports.Direction = Direction = {}));

let LastEventIndexKey;
exports.LastEventIndexKey = LastEventIndexKey;

(function (LastEventIndexKey) {
  LastEventIndexKey["hostDetails"] = "hostDetails";
  LastEventIndexKey["hosts"] = "hosts";
  LastEventIndexKey["ipDetails"] = "ipDetails";
  LastEventIndexKey["network"] = "network";
})(LastEventIndexKey || (exports.LastEventIndexKey = LastEventIndexKey = {}));

let HostsFields;
exports.HostsFields = HostsFields;

(function (HostsFields) {
  HostsFields["hostName"] = "hostName";
  HostsFields["lastSeen"] = "lastSeen";
})(HostsFields || (exports.HostsFields = HostsFields = {}));

let DomainsFields;
exports.DomainsFields = DomainsFields;

(function (DomainsFields) {
  DomainsFields["domainName"] = "domainName";
  DomainsFields["direction"] = "direction";
  DomainsFields["bytes"] = "bytes";
  DomainsFields["packets"] = "packets";
  DomainsFields["uniqueIpCount"] = "uniqueIpCount";
})(DomainsFields || (exports.DomainsFields = DomainsFields = {}));

let FlowDirection;
exports.FlowDirection = FlowDirection;

(function (FlowDirection) {
  FlowDirection["uniDirectional"] = "uniDirectional";
  FlowDirection["biDirectional"] = "biDirectional";
})(FlowDirection || (exports.FlowDirection = FlowDirection = {}));

let FlowTarget;
exports.FlowTarget = FlowTarget;

(function (FlowTarget) {
  FlowTarget["client"] = "client";
  FlowTarget["destination"] = "destination";
  FlowTarget["server"] = "server";
  FlowTarget["source"] = "source";
})(FlowTarget || (exports.FlowTarget = FlowTarget = {}));

let NetworkDirectionEcs;
exports.NetworkDirectionEcs = NetworkDirectionEcs;

(function (NetworkDirectionEcs) {
  NetworkDirectionEcs["inbound"] = "inbound";
  NetworkDirectionEcs["outbound"] = "outbound";
  NetworkDirectionEcs["internal"] = "internal";
  NetworkDirectionEcs["external"] = "external";
  NetworkDirectionEcs["incoming"] = "incoming";
  NetworkDirectionEcs["outgoing"] = "outgoing";
  NetworkDirectionEcs["listening"] = "listening";
  NetworkDirectionEcs["unknown"] = "unknown";
})(NetworkDirectionEcs || (exports.NetworkDirectionEcs = NetworkDirectionEcs = {}));

let TlsFields;
exports.TlsFields = TlsFields;

(function (TlsFields) {
  TlsFields["_id"] = "_id";
})(TlsFields || (exports.TlsFields = TlsFields = {}));

let UsersFields;
exports.UsersFields = UsersFields;

(function (UsersFields) {
  UsersFields["name"] = "name";
  UsersFields["count"] = "count";
})(UsersFields || (exports.UsersFields = UsersFields = {}));

let FlowTargetNew;
exports.FlowTargetNew = FlowTargetNew;

(function (FlowTargetNew) {
  FlowTargetNew["destination"] = "destination";
  FlowTargetNew["source"] = "source";
})(FlowTargetNew || (exports.FlowTargetNew = FlowTargetNew = {}));

let NetworkTopNFlowFields;
exports.NetworkTopNFlowFields = NetworkTopNFlowFields;

(function (NetworkTopNFlowFields) {
  NetworkTopNFlowFields["bytes_in"] = "bytes_in";
  NetworkTopNFlowFields["bytes_out"] = "bytes_out";
  NetworkTopNFlowFields["flows"] = "flows";
  NetworkTopNFlowFields["destination_ips"] = "destination_ips";
  NetworkTopNFlowFields["source_ips"] = "source_ips";
})(NetworkTopNFlowFields || (exports.NetworkTopNFlowFields = NetworkTopNFlowFields = {}));

let NetworkDnsFields;
exports.NetworkDnsFields = NetworkDnsFields;

(function (NetworkDnsFields) {
  NetworkDnsFields["dnsName"] = "dnsName";
  NetworkDnsFields["queryCount"] = "queryCount";
  NetworkDnsFields["uniqueDomains"] = "uniqueDomains";
  NetworkDnsFields["dnsBytesIn"] = "dnsBytesIn";
  NetworkDnsFields["dnsBytesOut"] = "dnsBytesOut";
})(NetworkDnsFields || (exports.NetworkDnsFields = NetworkDnsFields = {}));

let SortFieldTimeline; // ====================================================
// END: Typescript template
// ====================================================
// ====================================================
// Resolvers
// ====================================================

exports.SortFieldTimeline = SortFieldTimeline;

(function (SortFieldTimeline) {
  SortFieldTimeline["title"] = "title";
  SortFieldTimeline["description"] = "description";
  SortFieldTimeline["updated"] = "updated";
  SortFieldTimeline["created"] = "created";
})(SortFieldTimeline || (exports.SortFieldTimeline = SortFieldTimeline = {}));