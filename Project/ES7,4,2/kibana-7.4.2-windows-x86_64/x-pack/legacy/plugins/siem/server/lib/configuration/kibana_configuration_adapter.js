"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaConfigurationAdapter = void 0;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class KibanaConfigurationAdapter {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(server) {
    _defineProperty(this, "server", void 0);

    if (!isServerWithConfig(server)) {
      throw new Error('Failed to find configuration on server.');
    }

    this.server = server;
  }

  async get() {
    const config = this.server.config();

    if (!isKibanaConfiguration(config)) {
      throw new Error('Failed to access configuration of server.');
    }

    const configuration = config.get('xpack.siem') || {};
    const configurationWithDefaults = {
      enabled: true,
      query: {
        partitionSize: 75,
        partitionFactor: 1.2,
        ...(configuration.query || {})
      },
      sources: {},
      ...configuration
    }; // we assume this to be the configuration because Kibana would have already validated it

    return configurationWithDefaults;
  }

}

exports.KibanaConfigurationAdapter = KibanaConfigurationAdapter;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function isServerWithConfig(maybeServer) {
  return _joi.default.validate(maybeServer, _joi.default.object({
    config: _joi.default.func().required()
  }).unknown()).error === null;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function isKibanaConfiguration(maybeConfiguration) {
  return _joi.default.validate(maybeConfiguration, _joi.default.object({
    get: _joi.default.func().required()
  }).unknown()).error === null;
}