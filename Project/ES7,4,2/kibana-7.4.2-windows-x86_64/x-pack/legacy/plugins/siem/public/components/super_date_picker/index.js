"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuperDatePicker = exports.makeMapStateToProps = exports.SuperDatePickerComponent = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../store/actions");

var _selectors = require("./selectors");

var _temp;

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MAX_RECENTLY_USED_RANGES = 9;
var MyEuiSuperDatePicker = _eui.EuiSuperDatePicker;
var SuperDatePickerComponent = (_temp =
/*#__PURE__*/
function (_Component) {
  _inherits(SuperDatePickerComponent, _Component);

  function SuperDatePickerComponent(props) {
    var _this;

    _classCallCheck(this, SuperDatePickerComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SuperDatePickerComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onRefresh", function (_ref) {
      var start = _ref.start,
          end = _ref.end;

      var _this$props$updateRed = _this.props.updateReduxTime({
        end: end,
        id: _this.props.id,
        isInvalid: false,
        isQuickSelection: _this.state.isQuickSelection,
        kql: _this.props.kqlQuery,
        start: start,
        timelineId: _this.props.timelineId
      }),
          kqlHasBeenUpdated = _this$props$updateRed.kqlHasBeenUpdated;

      var currentStart = formatDate(start);
      var currentEnd = _this.state.isQuickSelection ? formatDate(end, {
        roundUp: true
      }) : formatDate(end);

      if (!kqlHasBeenUpdated && (!_this.state.isQuickSelection || _this.props.start === currentStart && _this.props.end === currentEnd)) {
        _this.refetchQuery(_this.props.queries);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onRefreshChange", function (_ref2) {
      var isPaused = _ref2.isPaused,
          refreshInterval = _ref2.refreshInterval;
      var _this$props = _this.props,
          id = _this$props.id,
          duration = _this$props.duration,
          policy = _this$props.policy,
          stopAutoReload = _this$props.stopAutoReload,
          startAutoReload = _this$props.startAutoReload;

      if (duration !== refreshInterval) {
        _this.props.setDuration({
          id: id,
          duration: refreshInterval
        });
      }

      if (isPaused && policy === 'interval') {
        stopAutoReload({
          id: id
        });
      } else if (!isPaused && policy === 'manual') {
        startAutoReload({
          id: id
        });
      }

      if (!isPaused && (!_this.state.isQuickSelection || _this.state.isQuickSelection && _this.props.toStr !== 'now')) {
        _this.refetchQuery(_this.props.queries);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "refetchQuery", function (queries) {
      queries.forEach(function (q) {
        return q.refetch && q.refetch();
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onTimeChange", function (_ref3) {
      var start = _ref3.start,
          end = _ref3.end,
          isQuickSelection = _ref3.isQuickSelection,
          isInvalid = _ref3.isInvalid;

      if (!isInvalid) {
        _this.props.updateReduxTime({
          end: end,
          id: _this.props.id,
          isInvalid: isInvalid,
          isQuickSelection: isQuickSelection,
          kql: _this.props.kqlQuery,
          start: start,
          timelineId: _this.props.timelineId
        });

        _this.setState(function (prevState) {
          var recentlyUsedRanges = [{
            start: start,
            end: end
          }].concat(_toConsumableArray((0, _fp.take)(MAX_RECENTLY_USED_RANGES, prevState.recentlyUsedRanges.filter(function (recentlyUsedRange) {
            return !(recentlyUsedRange.start === start && recentlyUsedRange.end === end);
          }))));
          return {
            recentlyUsedRanges: recentlyUsedRanges,
            isQuickSelection: isQuickSelection
          };
        });
      }
    });

    _this.state = {
      isQuickSelection: true,
      recentlyUsedRanges: [],
      showUpdateButton: true
    };
    return _this;
  }

  _createClass(SuperDatePickerComponent, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          duration = _this$props2.duration,
          end = _this$props2.end,
          start = _this$props2.start,
          kind = _this$props2.kind,
          fromStr = _this$props2.fromStr,
          policy = _this$props2.policy,
          toStr = _this$props2.toStr,
          isLoading = _this$props2.isLoading;
      var endDate = kind === 'relative' ? toStr : new Date(end).toISOString();
      var startDate = kind === 'relative' ? fromStr : new Date(start).toISOString();
      return _react.default.createElement(MyEuiSuperDatePicker, {
        end: endDate,
        isLoading: isLoading,
        isPaused: policy === 'manual',
        onTimeChange: this.onTimeChange,
        onRefreshChange: this.onRefreshChange,
        onRefresh: this.onRefresh,
        recentlyUsedRanges: this.state.recentlyUsedRanges,
        refreshInterval: duration,
        showUpdateButton: this.state.showUpdateButton,
        start: startDate
      });
    }
  }]);

  return SuperDatePickerComponent;
}(_react.Component), _temp);
exports.SuperDatePickerComponent = SuperDatePickerComponent;

var formatDate = function formatDate(date, options) {
  var momentDate = _datemath.default.parse(date, options);

  return momentDate != null && momentDate.isValid() ? momentDate.valueOf() : 0;
};

var dispatchUpdateReduxTime = function dispatchUpdateReduxTime(dispatch) {
  return function (_ref4) {
    var end = _ref4.end,
        id = _ref4.id,
        isQuickSelection = _ref4.isQuickSelection,
        kql = _ref4.kql,
        start = _ref4.start,
        timelineId = _ref4.timelineId;
    var fromDate = formatDate(start);
    var toDate = formatDate(end, {
      roundUp: true
    });

    if (isQuickSelection) {
      dispatch(_actions.inputsActions.setRelativeRangeDatePicker({
        id: id,
        fromStr: start,
        toStr: end,
        from: fromDate,
        to: toDate
      }));
    } else {
      toDate = formatDate(end);
      dispatch(_actions.inputsActions.setAbsoluteRangeDatePicker({
        id: id,
        from: formatDate(start),
        to: formatDate(end)
      }));
    }

    if (timelineId != null) {
      dispatch(_actions.timelineActions.updateRange({
        id: timelineId,
        start: fromDate,
        end: toDate
      }));
    }

    var kqlHasBeenUpdated = false;

    if (kql) {
      // if refetch is successful, it will have a side effect
      // to set all the tables on its activePage to zero (meaning pagination)
      kqlHasBeenUpdated = kql.refetch(dispatch);
    }

    if (!kqlHasBeenUpdated) {
      // Date picker is global to all the page in the app
      // (Hosts/Network are the only one having tables)
      dispatch(_actions.hostsActions.setHostTablesActivePageToZero());
      dispatch(_actions.networkActions.setNetworkTablesActivePageToZero());
    }

    return {
      kqlHasBeenUpdated: kqlHasBeenUpdated
    };
  };
};

var makeMapStateToProps = function makeMapStateToProps() {
  var getPolicySelector = (0, _selectors.policySelector)();
  var getDurationSelector = (0, _selectors.durationSelector)();
  var getKindSelector = (0, _selectors.kindSelector)();
  var getStartSelector = (0, _selectors.startSelector)();
  var getEndSelector = (0, _selectors.endSelector)();
  var getFromStrSelector = (0, _selectors.fromStrSelector)();
  var getToStrSelector = (0, _selectors.toStrSelector)();
  var getIsLoadingSelector = (0, _selectors.isLoadingSelector)();
  var getQueriesSelector = (0, _selectors.queriesSelector)();
  var getKqlQuerySelector = (0, _selectors.kqlQuerySelector)();
  return function (state, _ref5) {
    var id = _ref5.id;
    var inputsRange = (0, _fp.getOr)({}, "inputs.".concat(id), state);
    return {
      policy: getPolicySelector(inputsRange),
      duration: getDurationSelector(inputsRange),
      kind: getKindSelector(inputsRange),
      start: getStartSelector(inputsRange),
      end: getEndSelector(inputsRange),
      fromStr: getFromStrSelector(inputsRange),
      toStr: getToStrSelector(inputsRange),
      isLoading: getIsLoadingSelector(inputsRange),
      queries: getQueriesSelector(inputsRange),
      kqlQuery: getKqlQuerySelector(inputsRange)
    };
  };
};

exports.makeMapStateToProps = makeMapStateToProps;

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    startAutoReload: function startAutoReload(_ref6) {
      var id = _ref6.id;
      return dispatch(_actions.inputsActions.startAutoReload({
        id: id
      }));
    },
    stopAutoReload: function stopAutoReload(_ref7) {
      var id = _ref7.id;
      return dispatch(_actions.inputsActions.stopAutoReload({
        id: id
      }));
    },
    setDuration: function setDuration(_ref8) {
      var id = _ref8.id,
          duration = _ref8.duration;
      return dispatch(_actions.inputsActions.setDuration({
        id: id,
        duration: duration
      }));
    },
    updateReduxTime: dispatchUpdateReduxTime(dispatch)
  };
};

var SuperDatePicker = (0, _reactRedux.connect)(makeMapStateToProps, mapDispatchToProps)(SuperDatePickerComponent);
exports.SuperDatePicker = SuperDatePicker;