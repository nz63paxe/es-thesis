"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExpandableEvent = void 0;

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _stateful_event_details = require("../../event_details/stateful_event_details");

var _lazy_accordion = require("../../lazy_accordion");

var _timeline_context = require("../timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ExpandableDetails = _styledComponents.default.div(_templateObject(), function (_ref) {
  var hideExpandButton = _ref.hideExpandButton;
  return hideExpandButton ? "\n  .euiAccordion__button {\n    display: none;\n  }\n  " : '';
});

ExpandableDetails.displayName = 'ExpandableDetails';
var ExpandableEvent = React.memo(function (_ref2) {
  var browserFields = _ref2.browserFields,
      columnHeaders = _ref2.columnHeaders,
      event = _ref2.event,
      _ref2$forceExpand = _ref2.forceExpand,
      forceExpand = _ref2$forceExpand === void 0 ? false : _ref2$forceExpand,
      id = _ref2.id,
      timelineId = _ref2.timelineId,
      toggleColumn = _ref2.toggleColumn,
      onUpdateColumns = _ref2.onUpdateColumns;
  var width = (0, _timeline_context.useTimelineWidthContext)(); // Passing the styles directly to the component of LazyAccordion because the width is
  // being calculated and is recommended by Styled Components for performance
  // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291

  return React.createElement(ExpandableDetails, {
    hideExpandButton: true
  }, React.createElement(_lazy_accordion.LazyAccordion, {
    style: {
      width: "".concat(width, "px")
    },
    id: "timeline-".concat(timelineId, "-row-").concat(id),
    renderExpandedContent: function renderExpandedContent() {
      return React.createElement(_stateful_event_details.StatefulEventDetails, {
        browserFields: browserFields,
        columnHeaders: columnHeaders,
        data: event,
        id: id,
        onUpdateColumns: onUpdateColumns,
        timelineId: timelineId,
        toggleColumn: toggleColumn
      });
    },
    forceExpand: forceExpand,
    paddingSize: "none"
  }));
});
exports.ExpandableEvent = ExpandableEvent;
ExpandableEvent.displayName = 'ExpandableEvent';