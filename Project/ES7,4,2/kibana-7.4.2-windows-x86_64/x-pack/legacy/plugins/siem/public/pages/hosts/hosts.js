"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Hosts = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _redux = require("redux");

var _reactRedux = require("react-redux");

var _reactSticky = require("react-sticky");

var _header_page = require("../../components/header_page");

var _last_event_time = require("../../components/last_event_time");

var _hosts = require("../../components/page/hosts");

var _manage_query = require("../../components/page/manage_query");

var _kpi_hosts = require("../../containers/kpi_hosts");

var _source = require("../../containers/source");

var _types = require("../../graphql/types");

var _store = require("../../store");

var _hosts_empty_page = require("./hosts_empty_page");

var _kql = require("./kql");

var _actions = require("../../store/inputs/actions");

var _navigation = require("../../components/navigation");

var _spy_routes = require("../../utils/route/spy_routes");

var _filters_global = require("../../components/filters_global");

var i18n = _interopRequireWildcard(require("./translations"));

var _hosts_navigations = require("./hosts_navigations");

var _has_ml_user_permissions = require("../../components/ml/permissions/has_ml_user_permissions");

var _ml_capabilities_provider = require("../../components/ml/permissions/ml_capabilities_provider");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var KpiHostsComponentManage = (0, _manage_query.manageQuery)(_hosts.KpiHostsComponent);
var HostsComponent = React.memo(function (_ref) {
  var isInitializing = _ref.isInitializing,
      filterQuery = _ref.filterQuery,
      from = _ref.from,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker,
      setQuery = _ref.setQuery,
      to = _ref.to;
  var capabilities = React.useContext(_ml_capabilities_provider.MlCapabilitiesContext);
  return React.createElement(React.Fragment, null, React.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? React.createElement(_reactSticky.StickyContainer, null, React.createElement(_filters_global.FiltersGlobal, null, React.createElement(_kql.HostsKql, {
      indexPattern: indexPattern,
      setQuery: setQuery,
      type: _store.hostsModel.HostsType.page
    })), React.createElement(_header_page.HeaderPage, {
      subtitle: React.createElement(_last_event_time.LastEventTime, {
        indexKey: _types.LastEventIndexKey.hosts
      }),
      title: i18n.PAGE_TITLE
    }), React.createElement(React.Fragment, null, React.createElement(_kpi_hosts.KpiHostsQuery, {
      endDate: to,
      filterQuery: filterQuery,
      skip: isInitializing,
      sourceId: "default",
      startDate: from
    }, function (_ref3) {
      var kpiHosts = _ref3.kpiHosts,
          loading = _ref3.loading,
          id = _ref3.id,
          inspect = _ref3.inspect,
          refetch = _ref3.refetch;
      return React.createElement(KpiHostsComponentManage, {
        data: kpiHosts,
        from: from,
        id: id,
        inspect: inspect,
        loading: loading,
        refetch: refetch,
        setQuery: setQuery,
        to: to,
        narrowDateRange: function narrowDateRange(min, max) {
          setAbsoluteRangeDatePicker({
            id: 'global',
            from: min,
            to: max
          });
        }
      });
    }), React.createElement(_eui.EuiSpacer, null), React.createElement(_navigation.SiemNavigation, {
      navTabs: (0, _hosts_navigations.navTabsHosts)((0, _has_ml_user_permissions.hasMlUserPermissions)(capabilities)),
      display: "default",
      showBorder: true
    }), React.createElement(_eui.EuiSpacer, null))) : React.createElement(React.Fragment, null, React.createElement(_header_page.HeaderPage, {
      title: i18n.PAGE_TITLE
    }), React.createElement(_hosts_empty_page.HostsEmptyPage, null));
  }), React.createElement(_spy_routes.SpyRoute, null));
});
HostsComponent.displayName = 'HostsComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getHostsFilterQueryAsJson = _store.hostsSelectors.hostsFilterQueryAsJson();

  var mapStateToProps = function mapStateToProps(state) {
    return {
      filterQuery: getHostsFilterQueryAsJson(state, _store.hostsModel.HostsType.page) || ''
    };
  };

  return mapStateToProps;
}; // eslint-disable-next-line @typescript-eslint/no-explicit-any


var Hosts = (0, _redux.compose)((0, _reactRedux.connect)(makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
}))(HostsComponent);
exports.Hosts = Hosts;