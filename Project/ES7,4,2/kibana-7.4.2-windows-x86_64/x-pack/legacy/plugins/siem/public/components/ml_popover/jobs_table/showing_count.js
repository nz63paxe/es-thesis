"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ShowingCount = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  user-select: none;\n  margin-top: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ShowingContainer = _styledComponents.default.div(_templateObject());

ShowingContainer.displayName = 'ShowingContainer';

var ShowingCount = _react2.default.memo(function (_ref) {
  var filterResultsLength = _ref.filterResultsLength;
  return _react2.default.createElement(ShowingContainer, {
    "data-test-subj": "showing"
  }, _react2.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "xs"
  }, _react2.default.createElement(_react.FormattedMessage, {
    "data-test-subj": "query-message",
    id: "xpack.siem.components.mlPopup.showingLabel",
    defaultMessage: "Showing: {filterResultsLength} {filterResultsLength, plural, one {job} other {jobs}}",
    values: {
      filterResultsLength: filterResultsLength
    }
  })));
});

exports.ShowingCount = ShowingCount;
ShowingCount.displayName = 'ShowingCount';