"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MESSAGE_FIELD_NAME = exports.IP_FIELD_TYPE = exports.HOST_NAME_FIELD_NAME = exports.DATE_FIELD_TYPE = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DATE_FIELD_TYPE = 'date';
exports.DATE_FIELD_TYPE = DATE_FIELD_TYPE;
var HOST_NAME_FIELD_NAME = 'host.name';
exports.HOST_NAME_FIELD_NAME = HOST_NAME_FIELD_NAME;
var IP_FIELD_TYPE = 'ip';
exports.IP_FIELD_TYPE = IP_FIELD_TYPE;
var MESSAGE_FIELD_NAME = 'message';
exports.MESSAGE_FIELD_NAME = MESSAGE_FIELD_NAME;