"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsKql = void 0;

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _autocomplete_field = require("../../components/autocomplete_field");

var _hosts = require("../../containers/hosts");

var _kuery_autocompletion = require("../../containers/kuery_autocompletion");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HostsKql = (0, _recompose.pure)(function (_ref) {
  var indexPattern = _ref.indexPattern,
      setQuery = _ref.setQuery,
      type = _ref.type;
  return _react.default.createElement(_kuery_autocompletion.KueryAutocompletion, {
    indexPattern: indexPattern
  }, function (_ref2) {
    var isLoadingSuggestions = _ref2.isLoadingSuggestions,
        loadSuggestions = _ref2.loadSuggestions,
        suggestions = _ref2.suggestions;
    return _react.default.createElement(_hosts.HostsFilter, {
      indexPattern: indexPattern,
      setQuery: setQuery,
      type: type
    }, function (_ref3) {
      var applyFilterQueryFromKueryExpression = _ref3.applyFilterQueryFromKueryExpression,
          filterQueryDraft = _ref3.filterQueryDraft,
          isFilterQueryDraftValid = _ref3.isFilterQueryDraftValid,
          setFilterQueryDraftFromKueryExpression = _ref3.setFilterQueryDraftFromKueryExpression;
      return _react.default.createElement(_autocomplete_field.AutocompleteField, {
        "data-test-subj": "kqlInput",
        isLoadingSuggestions: isLoadingSuggestions,
        isValid: isFilterQueryDraftValid,
        loadSuggestions: loadSuggestions,
        onChange: setFilterQueryDraftFromKueryExpression,
        onSubmit: applyFilterQueryFromKueryExpression,
        placeholder: i18n.KQL_PLACEHOLDER,
        suggestions: suggestions,
        value: filterQueryDraft ? filterQueryDraft.expression : ''
      });
    });
  });
});
exports.HostsKql = HostsKql;
HostsKql.displayName = 'HostsKql';