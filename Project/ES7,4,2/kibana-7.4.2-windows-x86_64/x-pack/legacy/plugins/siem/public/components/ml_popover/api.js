"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getIndexPatterns = exports.jobsSummary = exports.stopDatafeeds = exports.startDatafeeds = exports.setupMlJob = exports.groupsData = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _throw_if_not_ok = require("../ml/api/throw_if_not_ok");

var _use_kibana_ui_setting = require("../../lib/settings/use_kibana_ui_setting");

var _constants = require("../../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var emptyIndexPattern = [];
/**
 * Fetches ML Groups Data
 *
 * @param headers
 */

var groupsData =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(headers, signal) {
    var _useKibanaUiSetting, _useKibanaUiSetting2, kbnVersion, response;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1), kbnVersion = _useKibanaUiSetting2[0];
            _context.next = 3;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/jobs/groups"), {
              method: 'GET',
              credentials: 'same-origin',
              headers: _objectSpread({
                'content-type': 'application/json',
                'kbn-system-api': 'true',
                'kbn-xsrf': kbnVersion
              }, headers),
              signal: signal
            });

          case 3:
            response = _context.sent;
            _context.next = 6;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 6:
            _context.next = 8;
            return response.json();

          case 8:
            return _context.abrupt("return", _context.sent);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function groupsData(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Creates ML Jobs + Datafeeds for the given configTemplate + indexPatternName
 *
 * @param configTemplate - name of configTemplate to setup
 * @param indexPatternName - default index pattern configTemplate should be installed with
 * @param groups - list of groups to add to jobs being installed
 * @param prefix - prefix to be added to job name
 * @param headers
 */


exports.groupsData = groupsData;

var setupMlJob =
/*#__PURE__*/
function () {
  var _ref3 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(_ref2) {
    var configTemplate, _ref2$indexPatternNam, indexPatternName, _ref2$groups, groups, _ref2$prefix, prefix, _ref2$headers, headers, _useKibanaUiSetting3, _useKibanaUiSetting4, kbnVersion, response, json;

    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            configTemplate = _ref2.configTemplate, _ref2$indexPatternNam = _ref2.indexPatternName, indexPatternName = _ref2$indexPatternNam === void 0 ? 'auditbeat-*' : _ref2$indexPatternNam, _ref2$groups = _ref2.groups, groups = _ref2$groups === void 0 ? ['siem'] : _ref2$groups, _ref2$prefix = _ref2.prefix, prefix = _ref2$prefix === void 0 ? '' : _ref2$prefix, _ref2$headers = _ref2.headers, headers = _ref2$headers === void 0 ? {} : _ref2$headers;
            _useKibanaUiSetting3 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting4 = _slicedToArray(_useKibanaUiSetting3, 1), kbnVersion = _useKibanaUiSetting4[0];
            _context2.next = 4;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/modules/setup/").concat(configTemplate), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify({
                prefix: prefix,
                groups: groups,
                indexPatternName: indexPatternName,
                startDatafeed: false,
                useDedicatedIndex: true
              }),
              headers: _objectSpread({
                'kbn-system-api': 'true',
                'content-type': 'application/json',
                'kbn-xsrf': kbnVersion
              }, headers)
            });

          case 4:
            response = _context2.sent;
            _context2.next = 7;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 7:
            _context2.next = 9;
            return response.json();

          case 9:
            json = _context2.sent;
            (0, _throw_if_not_ok.throwIfErrorAttachedToSetup)(json);
            return _context2.abrupt("return", json);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function setupMlJob(_x3) {
    return _ref3.apply(this, arguments);
  };
}();
/**
 * Starts the given dataFeedIds
 *
 * @param datafeedIds
 * @param start
 * @param headers
 */


exports.setupMlJob = setupMlJob;

var startDatafeeds =
/*#__PURE__*/
function () {
  var _ref4 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(datafeedIds, headers) {
    var start,
        _useKibanaUiSetting5,
        _useKibanaUiSetting6,
        kbnVersion,
        response,
        json,
        _args3 = arguments;

    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            start = _args3.length > 2 && _args3[2] !== undefined ? _args3[2] : 0;
            _useKibanaUiSetting5 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting6 = _slicedToArray(_useKibanaUiSetting5, 1), kbnVersion = _useKibanaUiSetting6[0];
            _context3.next = 4;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/jobs/force_start_datafeeds"), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify(_objectSpread({
                datafeedIds: datafeedIds
              }, start !== 0 && {
                start: start
              })),
              headers: _objectSpread({
                'kbn-system-api': 'true',
                'content-type': 'application/json',
                'kbn-xsrf': kbnVersion
              }, headers)
            });

          case 4:
            response = _context3.sent;
            _context3.next = 7;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 7:
            _context3.next = 9;
            return response.json();

          case 9:
            json = _context3.sent;
            (0, _throw_if_not_ok.throwIfErrorAttached)(json, datafeedIds);
            return _context3.abrupt("return", json);

          case 12:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function startDatafeeds(_x4, _x5) {
    return _ref4.apply(this, arguments);
  };
}();
/**
 * Stops the given dataFeedIds and sets the corresponding Job's jobState to closed
 *
 * @param datafeedIds
 * @param headers
 */


exports.startDatafeeds = startDatafeeds;

var stopDatafeeds =
/*#__PURE__*/
function () {
  var _ref5 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(datafeedIds, headers) {
    var _useKibanaUiSetting7, _useKibanaUiSetting8, kbnVersion, stopDatafeedsResponse, stopDatafeedsResponseJson, datafeedPrefix, closeJobsResponse;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _useKibanaUiSetting7 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting8 = _slicedToArray(_useKibanaUiSetting7, 1), kbnVersion = _useKibanaUiSetting8[0];
            _context4.next = 3;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/jobs/stop_datafeeds"), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify({
                datafeedIds: datafeedIds
              }),
              headers: _objectSpread({
                'kbn-system-api': 'true',
                'content-type': 'application/json',
                'kbn-xsrf': kbnVersion
              }, headers)
            });

          case 3:
            stopDatafeedsResponse = _context4.sent;
            _context4.next = 6;
            return (0, _throw_if_not_ok.throwIfNotOk)(stopDatafeedsResponse);

          case 6:
            _context4.next = 8;
            return stopDatafeedsResponse.json();

          case 8:
            stopDatafeedsResponseJson = _context4.sent;
            datafeedPrefix = 'datafeed-';
            _context4.next = 12;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/jobs/close_jobs"), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify({
                jobIds: datafeedIds.map(function (dataFeedId) {
                  return dataFeedId.startsWith(datafeedPrefix) ? dataFeedId.substring(datafeedPrefix.length) : dataFeedId;
                })
              }),
              headers: _objectSpread({
                'content-type': 'application/json',
                'kbn-system-api': 'true',
                'kbn-xsrf': kbnVersion
              }, headers)
            });

          case 12:
            closeJobsResponse = _context4.sent;
            _context4.next = 15;
            return (0, _throw_if_not_ok.throwIfNotOk)(closeJobsResponse);

          case 15:
            _context4.t0 = stopDatafeedsResponseJson;
            _context4.next = 18;
            return closeJobsResponse.json();

          case 18:
            _context4.t1 = _context4.sent;
            return _context4.abrupt("return", [_context4.t0, _context4.t1]);

          case 20:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function stopDatafeeds(_x6, _x7) {
    return _ref5.apply(this, arguments);
  };
}();
/**
 * Fetches Job Details for given jobIds
 *
 * @param jobIds
 * @param headers
 */


exports.stopDatafeeds = stopDatafeeds;

var jobsSummary =
/*#__PURE__*/
function () {
  var _ref6 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(jobIds, headers, signal) {
    var _useKibanaUiSetting9, _useKibanaUiSetting10, kbnVersion, response;

    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _useKibanaUiSetting9 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting10 = _slicedToArray(_useKibanaUiSetting9, 1), kbnVersion = _useKibanaUiSetting10[0];
            _context5.next = 3;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/jobs/jobs_summary"), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify({
                jobIds: jobIds
              }),
              headers: _objectSpread({
                'content-type': 'application/json',
                'kbn-xsrf': kbnVersion,
                'kbn-system-api': 'true'
              }, headers),
              signal: signal
            });

          case 3:
            response = _context5.sent;
            _context5.next = 6;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 6:
            _context5.next = 8;
            return response.json();

          case 8:
            return _context5.abrupt("return", _context5.sent);

          case 9:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function jobsSummary(_x8, _x9, _x10) {
    return _ref6.apply(this, arguments);
  };
}();
/**
 * Fetches Configured Index Patterns from the Kibana saved objects API (as ML does during create job flow)
 * TODO: Used by more than just ML now -- refactor to shared component https://github.com/elastic/siem-team/issues/448
 * @param headers
 */


exports.jobsSummary = jobsSummary;

var getIndexPatterns =
/*#__PURE__*/
function () {
  var _ref7 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee6(headers, signal) {
    var _useKibanaUiSetting11, _useKibanaUiSetting12, kbnVersion, response, results;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _useKibanaUiSetting11 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting12 = _slicedToArray(_useKibanaUiSetting11, 1), kbnVersion = _useKibanaUiSetting12[0];
            _context6.next = 3;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/saved_objects/_find?type=index-pattern&fields=title&fields=type&per_page=10000"), {
              method: 'GET',
              credentials: 'same-origin',
              headers: _objectSpread({
                'content-type': 'application/json',
                'kbn-xsrf': kbnVersion,
                'kbn-system-api': 'true'
              }, headers),
              signal: signal
            });

          case 3:
            response = _context6.sent;
            _context6.next = 6;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 6:
            _context6.next = 8;
            return response.json();

          case 8:
            results = _context6.sent;

            if (!(results.saved_objects && Array.isArray(results.saved_objects))) {
              _context6.next = 13;
              break;
            }

            return _context6.abrupt("return", results.saved_objects);

          case 13:
            return _context6.abrupt("return", emptyIndexPattern);

          case 14:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function getIndexPatterns(_x11, _x12) {
    return _ref7.apply(this, arguments);
  };
}();

exports.getIndexPatterns = getIndexPatterns;