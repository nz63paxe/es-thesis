"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateUsersSort = exports.updateUsersLimit = exports.updateTlsLimit = exports.updateTlsSort = exports.updateDomainsSort = exports.updateDomainsFlowDirection = exports.updateDomainsLimit = exports.updateIpDetailsFlowTarget = exports.applyNetworkFilterQuery = exports.setNetworkFilterQueryDraft = exports.updateTopNFlowSort = exports.updateTopNFlowLimit = exports.updateIsPtrIncluded = exports.updateDnsSort = exports.updateDnsLimit = exports.updateIpDetailsTableActivePage = exports.setNetworkTablesActivePageToZero = exports.updateNetworkPageTableActivePage = void 0;

var _typescriptFsa = _interopRequireDefault(require("typescript-fsa"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var actionCreator = (0, _typescriptFsa.default)('x-pack/siem/local/network');
var updateNetworkPageTableActivePage = actionCreator('UPDATE_NETWORK_PAGE_TABLE_ACTIVE_PAGE');
exports.updateNetworkPageTableActivePage = updateNetworkPageTableActivePage;
var setNetworkTablesActivePageToZero = actionCreator('SET_NETWORK_TABLES_ACTIVE_PAGE_TO_ZERO');
exports.setNetworkTablesActivePageToZero = setNetworkTablesActivePageToZero;
var updateIpDetailsTableActivePage = actionCreator('UPDATE_NETWORK_DETAILS_TABLE_ACTIVE_PAGE');
exports.updateIpDetailsTableActivePage = updateIpDetailsTableActivePage;
var updateDnsLimit = actionCreator('UPDATE_DNS_LIMIT');
exports.updateDnsLimit = updateDnsLimit;
var updateDnsSort = actionCreator('UPDATE_DNS_SORT');
exports.updateDnsSort = updateDnsSort;
var updateIsPtrIncluded = actionCreator('UPDATE_DNS_IS_PTR_INCLUDED');
exports.updateIsPtrIncluded = updateIsPtrIncluded;
var updateTopNFlowLimit = actionCreator('UPDATE_TOP_N_FLOW_LIMIT');
exports.updateTopNFlowLimit = updateTopNFlowLimit;
var updateTopNFlowSort = actionCreator('UPDATE_TOP_N_FLOW_SORT');
exports.updateTopNFlowSort = updateTopNFlowSort;
var setNetworkFilterQueryDraft = actionCreator('SET_NETWORK_FILTER_QUERY_DRAFT');
exports.setNetworkFilterQueryDraft = setNetworkFilterQueryDraft;
var applyNetworkFilterQuery = actionCreator('APPLY_NETWORK_FILTER_QUERY'); // IP Details Actions

exports.applyNetworkFilterQuery = applyNetworkFilterQuery;
var updateIpDetailsFlowTarget = actionCreator('UPDATE_IP_DETAILS_TARGET'); // Domains Table Actions

exports.updateIpDetailsFlowTarget = updateIpDetailsFlowTarget;
var updateDomainsLimit = actionCreator('UPDATE_DOMAINS_LIMIT');
exports.updateDomainsLimit = updateDomainsLimit;
var updateDomainsFlowDirection = actionCreator('UPDATE_DOMAINS_DIRECTION');
exports.updateDomainsFlowDirection = updateDomainsFlowDirection;
var updateDomainsSort = actionCreator('UPDATE_DOMAINS_SORT'); // TLS Table Actions

exports.updateDomainsSort = updateDomainsSort;
var updateTlsSort = actionCreator('UPDATE_TLS_SORT');
exports.updateTlsSort = updateTlsSort;
var updateTlsLimit = actionCreator('UPDATE_TLS_LIMIT'); // Users Table Actions

exports.updateTlsLimit = updateTlsLimit;
var updateUsersLimit = actionCreator('UPDATE_USERS_LIMIT');
exports.updateUsersLimit = updateUsersLimit;
var updateUsersSort = actionCreator('UPDATE_USERS_SORT');
exports.updateUsersSort = updateUsersSort;