"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeaderPanel = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _inspect = require("../inspect");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n    margin-bottom: ", ";\n    user-select: text;\n\n    ", "\n  "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Header = _styledComponents.default.header(_templateObject(), function (props) {
  return (0, _styledComponents.css)(_templateObject2(), props.theme.eui.euiSizeL, props.border && "\n      border-bottom: ".concat(props.theme.eui.euiBorderThin, ";\n      padding-bottom: ").concat(props.theme.eui.euiSizeL, ";\n    "));
});

Header.displayName = 'Header';
var HeaderPanel = (0, _recompose.pure)(function (_ref) {
  var border = _ref.border,
      children = _ref.children,
      id = _ref.id,
      _ref$showInspect = _ref.showInspect,
      showInspect = _ref$showInspect === void 0 ? false : _ref$showInspect,
      subtitle = _ref.subtitle,
      title = _ref.title,
      tooltip = _ref.tooltip;
  return _react.default.createElement(Header, {
    border: border
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    responsive: false
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h2", {
    "data-test-subj": "panel_headline_title"
  }, title, tooltip && _react.default.createElement(_react.default.Fragment, null, ' ', _react.default.createElement(_eui.EuiIconTip, {
    color: "subdued",
    content: tooltip,
    size: "l",
    type: "iInCircle"
  })))), _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    "data-test-subj": "subtitle",
    size: "xs"
  }, subtitle)), id && _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_inspect.InspectButton, {
    queryId: id,
    inspectIndex: 0,
    show: showInspect,
    title: title
  })))), children && _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, children)));
});
exports.HeaderPanel = HeaderPanel;
HeaderPanel.displayName = 'HeaderPanel';