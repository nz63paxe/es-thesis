"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NetworkDnsTable = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../../../store/actions");

var _store = require("../../../../store");

var _paginated_table = require("../../../paginated_table");

var _columns = require("./columns");

var _is_ptr_included = require("./is_ptr_included");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var tableType = _store.networkModel.NetworkTableType.dns;
var rowItems = [{
  text: i18n.ROWS_5,
  numberOfRow: 5
}, {
  text: i18n.ROWS_10,
  numberOfRow: 10
}];

var NetworkDnsTableComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(NetworkDnsTableComponent, _React$PureComponent);

  function NetworkDnsTableComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NetworkDnsTableComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NetworkDnsTableComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onChange", function (criteria) {
      if (criteria.sort != null) {
        var newDnsSortField = {
          field: criteria.sort.field.split('.')[1],
          direction: criteria.sort.direction
        };

        if (!(0, _fp.isEqual)(newDnsSortField, _this.props.dnsSortField)) {
          _this.props.updateDnsSort({
            dnsSortField: newDnsSortField,
            networkType: _this.props.type
          });
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onChangePtrIncluded", function () {
      return _this.props.updateIsPtrIncluded({
        isPtrIncluded: !_this.props.isPtrIncluded,
        networkType: _this.props.type
      });
    });

    return _this;
  }

  _createClass(NetworkDnsTableComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          activePage = _this$props.activePage,
          data = _this$props.data,
          dnsSortField = _this$props.dnsSortField,
          fakeTotalCount = _this$props.fakeTotalCount,
          id = _this$props.id,
          isInspect = _this$props.isInspect,
          isPtrIncluded = _this$props.isPtrIncluded,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _loadPage = _this$props.loadPage,
          showMorePagesIndicator = _this$props.showMorePagesIndicator,
          totalCount = _this$props.totalCount,
          type = _this$props.type,
          updateDnsLimit = _this$props.updateDnsLimit,
          updateTableActivePage = _this$props.updateTableActivePage;
      return _react.default.createElement(_paginated_table.PaginatedTable, {
        activePage: activePage,
        columns: (0, _columns.getNetworkDnsColumns)(type),
        headerCount: totalCount,
        headerSupplement: _react.default.createElement(_is_ptr_included.IsPtrIncluded, {
          isPtrIncluded: isPtrIncluded,
          onChange: this.onChangePtrIncluded
        }),
        headerTitle: i18n.TOP_DNS_DOMAINS,
        headerTooltip: i18n.TOOLTIP,
        headerUnit: i18n.UNIT(totalCount),
        id: id,
        itemsPerRow: rowItems,
        isInspect: isInspect,
        limit: limit,
        loading: loading,
        loadPage: function loadPage(newActivePage) {
          return _loadPage(newActivePage);
        },
        onChange: this.onChange,
        pageOfItems: data,
        showMorePagesIndicator: showMorePagesIndicator,
        sorting: {
          field: "node.".concat(dnsSortField.field),
          direction: dnsSortField.direction
        },
        totalCount: fakeTotalCount,
        updateActivePage: function updateActivePage(newPage) {
          return updateTableActivePage({
            activePage: newPage,
            tableType: tableType
          });
        },
        updateLimitPagination: function updateLimitPagination(newLimit) {
          return updateDnsLimit({
            limit: newLimit,
            networkType: type
          });
        }
      });
    }
  }]);

  return NetworkDnsTableComponent;
}(_react.default.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getNetworkDnsSelector = _store.networkSelectors.dnsSelector();

  var mapStateToProps = function mapStateToProps(state) {
    return getNetworkDnsSelector(state);
  };

  return mapStateToProps;
};

var NetworkDnsTable = (0, _reactRedux.connect)(makeMapStateToProps, {
  updateTableActivePage: _actions.networkActions.updateNetworkPageTableActivePage,
  updateDnsLimit: _actions.networkActions.updateDnsLimit,
  updateDnsSort: _actions.networkActions.updateDnsSort,
  updateIsPtrIncluded: _actions.networkActions.updateIsPtrIncluded
})(NetworkDnsTableComponent);
exports.NetworkDnsTable = NetworkDnsTable;