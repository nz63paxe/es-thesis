"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEmbeddable = exports.setupEmbeddablesAPI = exports.displayErrorToast = void 0;

var _uuid = _interopRequireDefault(require("uuid"));

var _legacy = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public/legacy");

var _triggers = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public/lib/triggers");

var _apply_siem_filter_action = require("./actions/apply_siem_filter_action");

var _actions = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public/lib/actions");

var _map_config = require("./map_config");

var _public = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public");

var _constants = require("../../../../maps/common/constants");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * Displays an error toast for the provided title and message
 *
 * @param errorTitle Title of error to display in toaster and modal
 * @param errorMessage Message to display in error modal when clicked
 * @param dispatchToaster provided by useStateToaster()
 */
var displayErrorToast = function displayErrorToast(errorTitle, errorMessage, dispatchToaster) {
  var toast = {
    id: _uuid.default.v4(),
    title: errorTitle,
    color: 'danger',
    iconType: 'alert',
    errors: [errorMessage]
  };
  dispatchToaster({
    type: 'addToaster',
    toast: toast
  });
};
/**
 * Temporary Embeddables API configuration override until ability to edit actions is addressed:
 * https://github.com/elastic/kibana/issues/43643
 *
 * @param applyFilterQueryFromKueryExpression function for updating KQL as provided by NetworkFilter
 *
 * @throws Error if action is already registered
 */


exports.displayErrorToast = displayErrorToast;

var setupEmbeddablesAPI = function setupEmbeddablesAPI(applyFilterQueryFromKueryExpression) {
  try {
    var actions = _legacy.start.getTriggerActions(_triggers.APPLY_FILTER_TRIGGER);

    var actionLoaded = actions.some(function (a) {
      return a.id === _apply_siem_filter_action.APPLY_SIEM_FILTER_ACTION_ID;
    });

    if (!actionLoaded) {
      var siemFilterAction = new _apply_siem_filter_action.ApplySiemFilterAction({
        applyFilterQueryFromKueryExpression: applyFilterQueryFromKueryExpression
      });

      _legacy.start.registerAction(siemFilterAction);

      _legacy.start.attachAction(_triggers.APPLY_FILTER_TRIGGER, siemFilterAction.id);

      _legacy.start.detachAction(_triggers.CONTEXT_MENU_TRIGGER, 'CUSTOM_TIME_RANGE');

      _legacy.start.detachAction(_triggers.PANEL_BADGE_TRIGGER, 'CUSTOM_TIME_RANGE_BADGE');

      _legacy.start.detachAction(_triggers.APPLY_FILTER_TRIGGER, _actions.APPLY_FILTER_ACTION);
    }
  } catch (e) {
    throw e;
  }
};
/**
 * Creates MapEmbeddable with provided initial configuration
 *
 * @param indexPatterns list of index patterns to configure layers for
 * @param queryExpression initial query constraints as an expression
 * @param startDate
 * @param endDate
 * @param setQuery function as provided by the GlobalTime component for reacting to refresh
 *
 * @throws Error if EmbeddableFactory does not exist
 */


exports.setupEmbeddablesAPI = setupEmbeddablesAPI;

var createEmbeddable =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(indexPatterns, queryExpression, startDate, endDate, setQuery) {
    var factory, state, input, embeddableObject;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            factory = _legacy.start.getEmbeddableFactory(_constants.MAP_SAVED_OBJECT_TYPE);
            state = {
              layerList: (0, _map_config.getLayerList)(indexPatterns),
              title: i18n.MAP_TITLE
            };
            input = {
              id: _uuid.default.v4(),
              filters: [],
              hidePanelTitles: true,
              query: {
                query: queryExpression,
                language: 'kuery'
              },
              refreshConfig: {
                value: 0,
                pause: true
              },
              timeRange: {
                from: new Date(startDate).toISOString(),
                to: new Date(endDate).toISOString()
              },
              viewMode: _public.ViewMode.VIEW,
              isLayerTOCOpen: false,
              openTOCDetails: [],
              hideFilterActions: false,
              mapCenter: {
                lon: -1.05469,
                lat: 15.96133,
                zoom: 1
              }
            }; // @ts-ignore method added in https://github.com/elastic/kibana/pull/43878

            _context.next = 5;
            return factory.createFromState(state, input);

          case 5:
            embeddableObject = _context.sent;
            // Wire up to app refresh action
            setQuery({
              id: 'embeddedMap',
              // Scope to page type if using map elsewhere
              inspect: null,
              loading: false,
              refetch: function refetch() {
                return embeddableObject.reload();
              }
            });
            return _context.abrupt("return", embeddableObject);

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function createEmbeddable(_x, _x2, _x3, _x4, _x5) {
    return _ref.apply(this, arguments);
  };
}();

exports.createEmbeddable = createEmbeddable;