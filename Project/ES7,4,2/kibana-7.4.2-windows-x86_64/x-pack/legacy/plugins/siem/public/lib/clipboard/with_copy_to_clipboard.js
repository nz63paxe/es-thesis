"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithCopyToClipboard = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _clipboard = require("./clipboard");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  user-select: text;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WithCopyToClipboardContainer = _styledComponents.default.div(_templateObject());

WithCopyToClipboardContainer.displayName = 'WithCopyToClipboardContainer';
/**
 * Renders `children` with an adjacent icon that when clicked, copies `text` to
 * the clipboard and displays a confirmation toast
 */

var WithCopyToClipboard = (0, _recompose.pure)(function (_ref) {
  var text = _ref.text,
      titleSummary = _ref.titleSummary,
      children = _ref.children;
  return React.createElement(WithCopyToClipboardContainer, null, React.createElement(React.Fragment, null, children), React.createElement(_clipboard.Clipboard, {
    content: text,
    titleSummary: titleSummary,
    toastLifeTimeMs: 800
  }));
});
exports.WithCopyToClipboard = WithCopyToClipboard;
WithCopyToClipboard.displayName = 'WithCopyToClipboard';