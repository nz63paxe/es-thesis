"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ArrowHead = exports.ArrowBody = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  height: ", ";\n  width: 25px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/** Renders the body (non-pointy part) of an arrow */
var ArrowBody = _styledComponents.default.span(_templateObject(), function (props) {
  return props.theme.eui.euiColorLightShade;
}, function (_ref) {
  var height = _ref.height;
  return "".concat(height, "px");
});

exports.ArrowBody = ArrowBody;
ArrowBody.displayName = 'ArrowBody';

/** Renders the head of an arrow */
var ArrowHead = (0, _recompose.pure)(function (_ref2) {
  var direction = _ref2.direction;
  return React.createElement(_eui.EuiIcon, {
    color: "subdued",
    "data-test-subj": "arrow-icon",
    size: "s",
    type: direction
  });
});
exports.ArrowHead = ArrowHead;
ArrowHead.displayName = 'ArrowHead';