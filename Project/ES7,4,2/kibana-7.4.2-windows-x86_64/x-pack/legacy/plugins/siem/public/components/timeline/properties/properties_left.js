"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PropertiesLeft = exports.DatePicker = exports.LockIconContainer = exports.PropertiesLeftStyle = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("./helpers");

var _super_date_picker = require("../../super_date_picker");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  .euiSuperDatePicker__flexWrapper {\n    max-width: none;\n    width: auto;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 2px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PropertiesLeftStyle = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
exports.PropertiesLeftStyle = PropertiesLeftStyle;
PropertiesLeftStyle.displayName = 'PropertiesLeftStyle';
var LockIconContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
exports.LockIconContainer = LockIconContainer;
LockIconContainer.displayName = 'LockIconContainer';
var DatePicker = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject3());
exports.DatePicker = DatePicker;
DatePicker.displayName = 'DatePicker';

var PropertiesLeft = _react.default.memo(function (_ref) {
  var isFavorite = _ref.isFavorite,
      timelineId = _ref.timelineId,
      updateIsFavorite = _ref.updateIsFavorite,
      showDescription = _ref.showDescription,
      description = _ref.description,
      title = _ref.title,
      updateTitle = _ref.updateTitle,
      updateDescription = _ref.updateDescription,
      showNotes = _ref.showNotes,
      showNotesFromWidth = _ref.showNotesFromWidth,
      associateNote = _ref.associateNote,
      getNotesByIds = _ref.getNotesByIds,
      noteIds = _ref.noteIds,
      onToggleShowNotes = _ref.onToggleShowNotes,
      updateNote = _ref.updateNote,
      isDatepickerLocked = _ref.isDatepickerLocked,
      toggleLock = _ref.toggleLock,
      datePickerWidth = _ref.datePickerWidth;
  return _react.default.createElement(PropertiesLeftStyle, {
    alignItems: "center",
    "data-test-subj": "properties-left",
    gutterSize: "s"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_helpers.StarIcon, {
    isFavorite: isFavorite,
    timelineId: timelineId,
    updateIsFavorite: updateIsFavorite
  })), _react.default.createElement(_helpers.Name, {
    timelineId: timelineId,
    title: title,
    updateTitle: updateTitle
  }), showDescription ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: 2
  }, _react.default.createElement(_helpers.Description, {
    description: description,
    timelineId: timelineId,
    updateDescription: updateDescription
  })) : null, showNotesFromWidth ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_helpers.NotesButton, {
    animate: true,
    associateNote: associateNote,
    getNotesByIds: getNotesByIds,
    noteIds: noteIds,
    showNotes: showNotes,
    size: "l",
    text: i18n.NOTES,
    toggleShowNotes: onToggleShowNotes,
    toolTip: i18n.NOTES_TOOL_TIP,
    updateNote: updateNote
  })) : null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: 1
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none",
    "data-test-subj": "timeline-date-picker-container"
  }, _react.default.createElement(LockIconContainer, {
    grow: false
  }, _react.default.createElement(_eui.EuiToolTip, {
    "data-test-subj": "timeline-date-picker-lock-tooltip",
    position: "top",
    content: isDatepickerLocked ? i18n.LOCK_SYNC_MAIN_DATE_PICKER_TOOL_TIP : i18n.UNLOCK_SYNC_MAIN_DATE_PICKER_TOOL_TIP
  }, _react.default.createElement(_eui.EuiButtonIcon, {
    "data-test-subj": "timeline-date-picker-".concat(isDatepickerLocked ? 'lock' : 'unlock', "-button"),
    color: "primary",
    onClick: toggleLock,
    iconType: isDatepickerLocked ? 'lock' : 'lockOpen',
    "aria-label": isDatepickerLocked ? i18n.UNLOCK_SYNC_MAIN_DATE_PICKER_ARIA : i18n.LOCK_SYNC_MAIN_DATE_PICKER_ARIA
  }))), _react.default.createElement(DatePicker, {
    grow: 1,
    style: {
      width: datePickerWidth
    }
  }, _react.default.createElement(_super_date_picker.SuperDatePicker, {
    id: "timeline",
    timelineId: timelineId
  })))));
});

exports.PropertiesLeft = PropertiesLeft;
PropertiesLeft.displayName = 'PropertiesLeft';