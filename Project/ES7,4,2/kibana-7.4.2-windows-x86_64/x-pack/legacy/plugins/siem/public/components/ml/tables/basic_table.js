"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BasicTable = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  tbody {\n    th,\n    td {\n      vertical-align: top;\n    }\n\n    .euiTableCellContent {\n      display: block;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Extended = _eui.EuiInMemoryTable;
var BasicTable = (0, _styledComponents.default)(Extended)(_templateObject());
exports.BasicTable = BasicTable;
BasicTable.displayName = 'BasicTable';