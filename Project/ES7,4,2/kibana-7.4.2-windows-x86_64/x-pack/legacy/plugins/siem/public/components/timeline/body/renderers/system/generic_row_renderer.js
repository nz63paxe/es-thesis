"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.systemRowRenderers = exports.createGenericFileRowRenderer = exports.createGenericSystemRowRenderer = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var i18n = _interopRequireWildcard(require("./translations"));

var _row_renderer = require("../row_renderer");

var _helpers = require("../helpers");

var _generic_details = require("./generic_details");

var _generic_file_details = require("./generic_file_details");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var createGenericSystemRowRenderer = function createGenericSystemRowRenderer(_ref) {
  var actionName = _ref.actionName,
      text = _ref.text;
  return {
    isInstance: function isInstance(ecs) {
      var module = (0, _fp.get)('event.module[0]', ecs);
      var action = (0, _fp.get)('event.action[0]', ecs);
      return module != null && module.toLowerCase() === 'system' && action != null && action.toLowerCase() === actionName;
    },
    renderRow: function renderRow(_ref2) {
      var browserFields = _ref2.browserFields,
          data = _ref2.data,
          children = _ref2.children,
          timelineId = _ref2.timelineId;
      return _react.default.createElement(_helpers.Row, {
        className: "euiTableRow"
      }, children, _react.default.createElement(_row_renderer.RowRendererContainer, null, _react.default.createElement(_generic_details.SystemGenericDetails, {
        browserFields: browserFields,
        data: data,
        contextId: "".concat(actionName, "-").concat(timelineId),
        text: text,
        timelineId: timelineId
      })));
    }
  };
};

exports.createGenericSystemRowRenderer = createGenericSystemRowRenderer;

var createGenericFileRowRenderer = function createGenericFileRowRenderer(_ref3) {
  var actionName = _ref3.actionName,
      text = _ref3.text;
  return {
    isInstance: function isInstance(ecs) {
      var module = (0, _fp.get)('event.module[0]', ecs);
      var action = (0, _fp.get)('event.action[0]', ecs);
      return module != null && module.toLowerCase() === 'system' && action != null && action.toLowerCase() === actionName;
    },
    renderRow: function renderRow(_ref4) {
      var browserFields = _ref4.browserFields,
          data = _ref4.data,
          children = _ref4.children,
          timelineId = _ref4.timelineId;
      return _react.default.createElement(_helpers.Row, {
        className: "euiTableRow"
      }, children, _react.default.createElement(_row_renderer.RowRendererContainer, null, _react.default.createElement(_generic_file_details.SystemGenericFileDetails, {
        browserFields: browserFields,
        data: data,
        contextId: "".concat(actionName, "-").concat(timelineId),
        text: text,
        timelineId: timelineId
      })));
    }
  };
};

exports.createGenericFileRowRenderer = createGenericFileRowRenderer;
var systemLoginRowRenderer = createGenericSystemRowRenderer({
  actionName: 'user_login',
  text: i18n.ATTEMPTED_LOGIN
});
var systemProcessStartedRowRenderer = createGenericFileRowRenderer({
  actionName: 'process_started',
  text: i18n.PROCESS_STARTED
});
var systemProcessStoppedRowRenderer = createGenericFileRowRenderer({
  actionName: 'process_stopped',
  text: i18n.PROCESS_STOPPED
});
var systemExistingRowRenderer = createGenericFileRowRenderer({
  actionName: 'existing_process',
  text: i18n.EXISTING_PROCESS
});
var systemSocketOpenedRowRenderer = createGenericFileRowRenderer({
  actionName: 'socket_opened',
  text: i18n.SOCKET_OPENED
});
var systemSocketClosedRowRenderer = createGenericFileRowRenderer({
  actionName: 'socket_closed',
  text: i18n.SOCKET_CLOSED
});
var systemExistingUserRowRenderer = createGenericSystemRowRenderer({
  actionName: 'existing_user',
  text: i18n.EXISTING_USER
});
var systemExistingSocketRowRenderer = createGenericFileRowRenderer({
  actionName: 'existing_socket',
  text: i18n.EXISTING_SOCKET
});
var systemExistingPackageRowRenderer = createGenericSystemRowRenderer({
  actionName: 'existing_package',
  text: i18n.EXISTING_PACKAGE
});
var systemInvalidRowRenderer = createGenericFileRowRenderer({
  actionName: 'invalid',
  text: i18n.INVALID
});
var systemUserChangedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'user_changed',
  text: i18n.USER_CHANGED
});
var systemHostChangedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'host',
  text: i18n.HOST_CHANGED
});
var systemUserAddedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'user_added',
  text: i18n.USER_ADDED
});
var systemLogoutRowRenderer = createGenericSystemRowRenderer({
  actionName: 'user_logout',
  text: i18n.LOGGED_OUT
});
var systemProcessErrorRowRenderer = createGenericFileRowRenderer({
  actionName: 'process_error',
  text: i18n.PROCESS_ERROR
}); // TODO: Remove this once this has been replaced everywhere with "error" below

var systemErrorRowRendererDeprecated = createGenericSystemRowRenderer({
  actionName: 'error:',
  text: i18n.ERROR
});
var systemErrorRowRenderer = createGenericSystemRowRenderer({
  actionName: 'error',
  text: i18n.ERROR
});
var systemPackageInstalledRowRenderer = createGenericSystemRowRenderer({
  actionName: 'package_installed',
  text: i18n.PACKAGE_INSTALLED
});
var systemBootRowRenderer = createGenericSystemRowRenderer({
  actionName: 'boot',
  text: i18n.BOOT
});
var systemAcceptedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'accepted',
  text: i18n.ACCEPTED
});
var systemPackageUpdatedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'package_updated',
  text: i18n.PACKAGE_UPDATED
});
var systemPackageRemovedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'package_removed',
  text: i18n.PACKAGE_REMOVED
});
var systemUserRemovedRowRenderer = createGenericSystemRowRenderer({
  actionName: 'user_removed',
  text: i18n.USER_REMOVED
});
var systemRowRenderers = [systemAcceptedRowRenderer, systemBootRowRenderer, systemErrorRowRenderer, systemErrorRowRendererDeprecated, systemExistingPackageRowRenderer, systemExistingRowRenderer, systemExistingSocketRowRenderer, systemExistingUserRowRenderer, systemHostChangedRowRenderer, systemInvalidRowRenderer, systemLoginRowRenderer, systemLogoutRowRenderer, systemPackageInstalledRowRenderer, systemPackageUpdatedRowRenderer, systemPackageRemovedRowRenderer, systemProcessErrorRowRenderer, systemProcessStartedRowRenderer, systemProcessStoppedRowRenderer, systemSocketClosedRowRenderer, systemSocketOpenedRowRenderer, systemUserAddedRowRenderer, systemUserChangedRowRenderer, systemUserRemovedRowRenderer];
exports.systemRowRenderers = systemRowRenderers;