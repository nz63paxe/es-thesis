"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "HeaderPage", {
  enumerable: true,
  get: function get() {
    return _header_page.HeaderPage;
  }
});

var _header_page = require("./header_page");