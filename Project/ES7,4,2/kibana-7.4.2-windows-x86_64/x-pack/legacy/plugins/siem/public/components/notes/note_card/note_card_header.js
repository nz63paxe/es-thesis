"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteCardHeader = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var i18n = _interopRequireWildcard(require("../translations"));

var _note_created = require("./note_created");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  font-weight: 700;\n  margin: 5px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  user-select: none;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Action = _styledComponents.default.span(_templateObject());

Action.displayName = 'Action';
var Avatar = (0, _styledComponents.default)(_eui.EuiAvatar)(_templateObject2());
Avatar.displayName = 'Avatar';

var HeaderContainer = _styledComponents.default.div(_templateObject3());

HeaderContainer.displayName = 'HeaderContainer';

var User = _styledComponents.default.span(_templateObject4());

var NoteCardHeader = (0, _recompose.pure)(function (_ref) {
  var created = _ref.created,
      user = _ref.user;
  return React.createElement(_eui.EuiPanel, {
    "data-test-subj": "note-card-header",
    hasShadow: false,
    paddingSize: "s"
  }, React.createElement(HeaderContainer, null, React.createElement(Avatar, {
    "data-test-subj": "avatar",
    size: "s",
    name: user
  }), React.createElement(User, {
    "data-test-subj": "user"
  }, user), React.createElement(Action, {
    "data-test-subj": "action"
  }, i18n.ADDED_A_NOTE), React.createElement(_note_created.NoteCreated, {
    "data-test-subj": "created",
    created: created
  })));
});
exports.NoteCardHeader = NoteCardHeader;
NoteCardHeader.displayName = 'NoteCardHeader';