"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AndOrBadge = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  border-radius: 100%;\n  display: inline-flex;\n  font-size: 9px;\n  height: 34px;\n  justify-content: center;\n  margin: 0 5px 0 5px;\n  padding: 7px 6px 4px 6px;\n  user-select: none;\n  width: 34px;\n\n  .euiBadge__content {\n    position: relative;\n    top: -1px;\n  }\n\n  .euiBadge__text {\n    text-overflow: clip;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var RoundedBadge = (0, _styledComponents.default)(_eui.EuiBadge)(_templateObject());
RoundedBadge.displayName = 'RoundedBadge';

/** Displays AND / OR in a round badge */
var AndOrBadge = (0, _recompose.pure)(function (_ref) {
  var type = _ref.type;
  return (// Ref: https://github.com/elastic/eui/issues/1655
    // @ts-ignore
    React.createElement(RoundedBadge, {
      "data-test-subj": "and-or-badge",
      color: "hollow"
    }, type === 'and' ? i18n.AND : i18n.OR)
  );
});
exports.AndOrBadge = AndOrBadge;
AndOrBadge.displayName = 'AndOrBadge';