"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedDuration = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _helpers = require("./helpers");

var _tooltip = require("./tooltip");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var FormattedDuration = (0, _recompose.pure)(function (_ref) {
  var maybeDurationNanoseconds = _ref.maybeDurationNanoseconds,
      tooltipTitle = _ref.tooltipTitle;
  return React.createElement(_tooltip.FormattedDurationTooltip, {
    maybeDurationNanoseconds: maybeDurationNanoseconds,
    tooltipTitle: tooltipTitle
  }, React.createElement("div", {
    "data-test-subj": "formatted-duration"
  }, (0, _helpers.getFormattedDurationString)(maybeDurationNanoseconds)));
});
exports.FormattedDuration = FormattedDuration;
FormattedDuration.displayName = 'FormattedDuration';