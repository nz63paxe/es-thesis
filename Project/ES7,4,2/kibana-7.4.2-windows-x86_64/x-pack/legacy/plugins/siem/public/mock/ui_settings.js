"use strict";

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
_chrome.default.getUiSettingsClient().get.mockImplementation(function (key) {
  switch (key) {
    case _constants.DEFAULT_TIME_RANGE:
      return {
        from: 'now-15m',
        to: 'now',
        mode: 'quick'
      };

    case _constants.DEFAULT_REFRESH_RATE_INTERVAL:
      return {
        pause: false,
        value: 0
      };

    case _constants.DEFAULT_SIEM_TIME_RANGE:
      return {
        from: _constants.DEFAULT_FROM,
        to: _constants.DEFAULT_TO
      };

    case _constants.DEFAULT_SIEM_REFRESH_INTERVAL:
      return {
        pause: _constants.DEFAULT_INTERVAL_PAUSE,
        value: _constants.DEFAULT_INTERVAL_VALUE
      };

    case _constants.DEFAULT_INDEX_KEY:
      return ['auditbeat-*', 'filebeat-*', 'packetbeat-*', 'winlogbeat-*'];

    case _constants.DEFAULT_DATE_FORMAT_TZ:
      return 'Asia/Taipei';

    case _constants.DEFAULT_DARK_MODE:
      return false;

    default:
      throw new Error("Unexpected config key: ".concat(key));
  }
});