"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getColumns = void 0;

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _default_headers = require("../timeline/body/column_headers/default_headers");

var _draggable_wrapper = require("../drag_and_drop/draggable_wrapper");

var _helpers = require("../timeline/body/helpers");

var _draggables = require("../draggables");

var _droppable_wrapper = require("../drag_and_drop/droppable_wrapper");

var _field_badge = require("../draggables/field_badge");

var _formatted_field = require("../timeline/body/renderers/formatted_field");

var _field_name = require("../fields_browser/field_name");

var _helpers2 = require("./helpers");

var _helpers3 = require("../drag_and_drop/helpers");

var _selectable_text = require("../selectable_text");

var _with_copy_to_clipboard = require("../../lib/clipboard/with_copy_to_clipboard");

var _with_hover_actions = require("../with_hover_actions");

var i18n = _interopRequireWildcard(require("./translations"));

var _helpers4 = require("../tables/helpers");

var _constants = require("../timeline/body/renderers/constants");

var _duration = require("../duration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  height: 25px;\n  justify-content: center;\n  left: 5px;\n  position: absolute;\n  top: -10px;\n  width: 30px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HoverActionsContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject());
HoverActionsContainer.displayName = 'HoverActionsContainer';

var getColumns = function getColumns(_ref) {
  var browserFields = _ref.browserFields,
      columnHeaders = _ref.columnHeaders,
      eventId = _ref.eventId,
      onUpdateColumns = _ref.onUpdateColumns,
      contextId = _ref.contextId,
      toggleColumn = _ref.toggleColumn;
  return [{
    field: 'field',
    name: '',
    sortable: false,
    truncateText: false,
    width: '30px',
    render: function render(field) {
      return React.createElement(_eui.EuiToolTip, {
        content: i18n.TOGGLE_COLUMN_TOOLTIP
      }, React.createElement(_eui.EuiCheckbox, {
        checked: columnHeaders.findIndex(function (c) {
          return c.id === field;
        }) !== -1,
        "data-test-subj": "toggle-field-".concat(field),
        id: field,
        onChange: function onChange() {
          return toggleColumn({
            columnHeaderType: _default_headers.defaultColumnHeaderType,
            id: field,
            width: _helpers.DEFAULT_COLUMN_MIN_WIDTH
          });
        }
      }));
    }
  }, {
    field: 'field',
    name: i18n.FIELD,
    sortable: true,
    truncateText: false,
    render: function render(field, data) {
      return React.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        gutterSize: "none"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiToolTip, {
        content: data.type
      }, React.createElement(_eui.EuiIcon, {
        "data-test-subj": "field-type-icon",
        type: (0, _helpers2.getIconFromType)(data.type)
      }))), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_droppable_wrapper.DroppableWrapper, {
        droppableId: (0, _helpers3.getDroppableId)("event-details-field-droppable-wrapper-".concat(contextId, "-").concat(eventId, "-").concat(data.category, "-").concat(field)),
        key: (0, _helpers3.getDroppableId)("event-details-field-droppable-wrapper-".concat(contextId, "-").concat(eventId, "-").concat(data.category, "-").concat(field)),
        isDropDisabled: true,
        type: _helpers3.DRAG_TYPE_FIELD
      }, React.createElement(_reactBeautifulDnd.Draggable, {
        draggableId: (0, _helpers3.getDraggableFieldId)({
          contextId: "event-details-field-draggable-".concat(contextId, "-").concat(eventId, "-").concat(data.category, "-").concat(field),
          fieldId: field
        }),
        index: 0,
        type: _helpers3.DRAG_TYPE_FIELD
      }, function (provided, snapshot) {
        return React.createElement("div", _extends({}, provided.draggableProps, provided.dragHandleProps, {
          ref: provided.innerRef
        }), !snapshot.isDragging ? React.createElement(_field_name.FieldName, {
          categoryId: data.category,
          categoryColumns: (0, _helpers2.getColumnsWithTimestamp)({
            browserFields: browserFields,
            category: data.category
          }),
          "data-test-subj": "field-name",
          fieldId: field,
          onUpdateColumns: onUpdateColumns
        }) : React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_field_badge.DraggableFieldBadge, {
          fieldId: field
        })));
      }))));
    }
  }, {
    field: 'values',
    name: i18n.VALUE,
    sortable: true,
    truncateText: false,
    render: function render(values, data) {
      return React.createElement(_eui.EuiFlexGroup, {
        direction: "column",
        alignItems: "flexStart",
        component: "span",
        gutterSize: "none"
      }, values != null && values.map(function (value, i) {
        return React.createElement(_eui.EuiFlexItem, {
          grow: false,
          component: "span",
          key: "event-details-value-flex-item-".concat(contextId, "-").concat(eventId, "-").concat(data.field, "-").concat(i, "-").concat(value)
        }, React.createElement(_with_hover_actions.WithHoverActions, {
          hoverContent: React.createElement(HoverActionsContainer, {
            "data-test-subj": "hover-actions-container"
          }, React.createElement(_eui.EuiToolTip, {
            content: i18n.COPY_TO_CLIPBOARD
          }, React.createElement(_with_copy_to_clipboard.WithCopyToClipboard, {
            text: value,
            titleSummary: i18n.VALUE.toLowerCase()
          }))),
          render: function render() {
            return data.field === _constants.MESSAGE_FIELD_NAME ? React.createElement(_helpers4.OverflowField, {
              value: value
            }) : React.createElement(_draggables.DefaultDraggable, {
              "data-test-subj": "ip",
              field: data.field,
              id: "event-details-value-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(data.field, "-").concat(i, "-").concat(value),
              tooltipContent: data.type === _constants.DATE_FIELD_TYPE || data.field === _duration.EVENT_DURATION_FIELD_NAME ? null : data.field,
              value: value
            }, React.createElement(_eui.EuiText, {
              size: "xs"
            }, React.createElement(_formatted_field.FormattedFieldValue, {
              contextId: "event-details-value-formatted-field-value-".concat(contextId, "-").concat(eventId, "-").concat(data.field, "-").concat(i, "-").concat(value),
              eventId: eventId,
              fieldFormat: data.format,
              fieldName: data.field,
              fieldType: data.type,
              value: value
            })));
          }
        }));
      }));
    }
  }, {
    field: 'description',
    name: i18n.DESCRIPTION,
    render: function render(description, data) {
      return React.createElement(_selectable_text.SelectableText, null, React.createElement(_eui.EuiText, {
        size: "xs"
      }, "".concat(description || '', " ").concat((0, _helpers2.getExampleText)(data.example))));
    },
    sortable: true,
    truncateText: true,
    width: '50%'
  }, {
    field: 'valuesConcatenated',
    name: i18n.BLANK,
    render: function render() {
      return null;
    },
    sortable: false,
    truncateText: true,
    width: '1px'
  }];
};

exports.getColumns = getColumns;