"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulEditDataProvider = exports.getInitialOperatorLabel = exports.HeaderContainer = void 0;

var _fp = require("lodash/fp");

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  .euiComboBoxOptionsList {\n    z-index: 9999;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EDIT_DATA_PROVIDER_WIDTH = 400;
var FIELD_COMBO_BOX_WIDTH = 195;
var OPERATOR_COMBO_BOX_WIDTH = 160;
var VALUE_INPUT_CLASS_NAME = 'edit-data-provider-value';
var SAVE_CLASS_NAME = 'edit-data-provider-save';

var HeaderContainer = _styledComponents.default.div(_templateObject(), EDIT_DATA_PROVIDER_WIDTH);

exports.HeaderContainer = HeaderContainer;
HeaderContainer.displayName = 'HeaderContainer'; // SIDE EFFECT: the following `injectGlobal` overrides the default styling
// of euiComboBoxOptionsList because it's implemented as a popover, so it's
// not selectable as a child of the styled component
// eslint-disable-next-line no-unused-expressions

(0, _styledComponents.injectGlobal)(_templateObject2());

var sanatizeValue = function sanatizeValue(value) {
  return Array.isArray(value) ? "".concat(value[0]) : "".concat(value);
}; // fun fact: value should never be an array


var getInitialOperatorLabel = function getInitialOperatorLabel(isExcluded, operator) {
  if (operator === ':') {
    return isExcluded ? [{
      label: i18n.IS_NOT
    }] : [{
      label: i18n.IS
    }];
  } else {
    return isExcluded ? [{
      label: i18n.DOES_NOT_EXIST
    }] : [{
      label: i18n.EXISTS
    }];
  }
};

exports.getInitialOperatorLabel = getInitialOperatorLabel;

var StatefulEditDataProvider =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(StatefulEditDataProvider, _React$PureComponent);

  function StatefulEditDataProvider(props) {
    var _this;

    _classCallCheck(this, StatefulEditDataProvider);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(StatefulEditDataProvider).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "focusInput", function () {
      var elements = document.getElementsByClassName(VALUE_INPUT_CLASS_NAME);

      if (elements.length > 0) {
        elements[0].focus(); // this cast is required because focus() does not exist on every `Element` returned by `getElementsByClassName`
      } else {
        var saveElements = document.getElementsByClassName(SAVE_CLASS_NAME);

        if (saveElements.length > 0) {
          saveElements[0].focus();
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onFieldSelected", function (selectedField) {
      _this.setState({
        updatedField: selectedField
      });

      _this.focusInput();
    });

    _defineProperty(_assertThisInitialized(_this), "onOperatorSelected", function (updatedOperator) {
      _this.setState({
        updatedOperator: updatedOperator
      });

      _this.focusInput();
    });

    _defineProperty(_assertThisInitialized(_this), "onValueChange", function (e) {
      _this.setState({
        updatedValue: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "disableScrolling", function () {
      var x = window.pageXOffset !== undefined ? window.pageXOffset : (document.documentElement || document.body.parentNode || document.body).scrollLeft;
      var y = window.pageYOffset !== undefined ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

      window.onscroll = function () {
        return window.scrollTo(x, y);
      };
    });

    _defineProperty(_assertThisInitialized(_this), "enableScrolling", function () {
      return window.onscroll = function () {
        return _fp.noop;
      };
    });

    var field = props.field,
        isExcluded = props.isExcluded,
        operator = props.operator,
        value = props.value;
    _this.state = {
      updatedField: [{
        label: field
      }],
      updatedOperator: getInitialOperatorLabel(isExcluded, operator),
      updatedValue: value
    };
    return _this;
  }

  _createClass(StatefulEditDataProvider, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.disableScrolling();
      this.focusInput();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this.enableScrolling();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          andProviderId = _this$props.andProviderId,
          browserFields = _this$props.browserFields,
          onDataProviderEdited = _this$props.onDataProviderEdited,
          providerId = _this$props.providerId,
          timelineId = _this$props.timelineId;
      var _this$state = this.state,
          updatedField = _this$state.updatedField,
          updatedOperator = _this$state.updatedOperator,
          updatedValue = _this$state.updatedValue;
      return React.createElement(_eui.EuiPanel, {
        paddingSize: "s"
      }, React.createElement(_eui.EuiFlexGroup, {
        direction: "column",
        gutterSize: "none"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFlexGroup, {
        direction: "row",
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFormRow, {
        label: i18n.FIELD
      }, React.createElement(_eui.EuiToolTip, {
        content: this.state.updatedField.length > 0 ? this.state.updatedField[0].label : null
      }, React.createElement(_eui.EuiComboBox, {
        "data-test-subj": "field",
        isClearable: false,
        onChange: this.onFieldSelected,
        options: (0, _helpers.getCategorizedFieldNames)(browserFields),
        placeholder: i18n.FIELD_PLACEHOLDER,
        selectedOptions: this.state.updatedField,
        singleSelection: {
          asPlainText: true
        },
        style: {
          width: "".concat(FIELD_COMBO_BOX_WIDTH, "px")
        }
      })))), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFormRow, {
        label: i18n.OPERATOR
      }, React.createElement(_eui.EuiComboBox, {
        "data-test-subj": "operator",
        isClearable: false,
        onChange: this.onOperatorSelected,
        options: _helpers.operatorLabels,
        placeholder: i18n.SELECT_AN_OPERATOR,
        selectedOptions: this.state.updatedOperator,
        singleSelection: {
          asPlainText: true
        },
        style: {
          width: "".concat(OPERATOR_COMBO_BOX_WIDTH, "px")
        }
      }))))), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiSpacer, null)), this.state.updatedOperator.length > 0 && this.state.updatedOperator[0].label !== i18n.EXISTS && this.state.updatedOperator[0].label !== i18n.DOES_NOT_EXIST ? React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFormRow, {
        label: i18n.VALUE_LABEL
      }, React.createElement(_eui.EuiFieldText, {
        className: VALUE_INPUT_CLASS_NAME,
        "data-test-subj": "value",
        placeholder: i18n.VALUE,
        onChange: this.onValueChange,
        value: sanatizeValue(this.state.updatedValue)
      }))) : null, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiSpacer, null)), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFlexGroup, {
        justifyContent: "flexEnd",
        gutterSize: "none"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiButton, {
        autoFocus: true,
        className: SAVE_CLASS_NAME,
        color: "primary",
        "data-test-subj": "save",
        fill: true,
        onClick: function onClick() {
          onDataProviderEdited({
            andProviderId: andProviderId,
            excluded: (0, _helpers.getExcludedFromSelection)(updatedOperator),
            field: updatedField.length > 0 ? updatedField[0].label : '',
            id: timelineId,
            operator: (0, _helpers.getQueryOperatorFromSelection)(updatedOperator),
            providerId: providerId,
            value: updatedValue
          });
        },
        isDisabled: !(0, _helpers.selectionsAreValid)({
          browserFields: this.props.browserFields,
          selectedField: updatedField,
          selectedOperator: updatedOperator
        }),
        size: "s"
      }, i18n.SAVE))))));
    }
    /** Focuses the Value input if it is visible, falling back to the Save button if it's not */

  }]);

  return StatefulEditDataProvider;
}(React.PureComponent);

exports.StatefulEditDataProvider = StatefulEditDataProvider;