"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventsSelect = exports.EVENTS_SELECT_WIDTH = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _helpers = require("./helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  left: 7px;\n  position: absolute;\n  top: -28px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .eventsSelectItem {\n    width: 100% !important;\n\n    .euiContextMenu__icon {\n      display: none !important;\n    }\n  }\n\n  .eventsSelectDropdown {\n    width: ", "px;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EVENTS_SELECT_WIDTH = 60; // px
// SIDE EFFECT: the following `injectGlobal` overrides
// the style of the select items
// eslint-disable-next-line

exports.EVENTS_SELECT_WIDTH = EVENTS_SELECT_WIDTH;
(0, _styledComponents.injectGlobal)(_templateObject(), EVENTS_SELECT_WIDTH);

var CheckboxContainer = _styledComponents.default.div(_templateObject2());

CheckboxContainer.displayName = 'CheckboxContainer';

var PositionedCheckbox = _styledComponents.default.div(_templateObject3());

PositionedCheckbox.displayName = 'PositionedCheckbox';
var EventsSelect = (0, _recompose.pure)(function (_ref) {
  var checkState = _ref.checkState,
      timelineId = _ref.timelineId;
  return React.createElement("div", {
    "data-test-subj": "events-select"
  }, React.createElement(_eui.EuiSuperSelect, {
    className: "eventsSelectDropdown",
    "data-test-subj": "events-select-dropdown",
    itemClassName: "eventsSelectItem",
    onChange: _fp.noop,
    options: (0, _helpers.getEventsSelectOptions)()
  }), React.createElement(CheckboxContainer, {
    "data-test-subj": "timeline-events-select-checkbox-container"
  }, React.createElement(PositionedCheckbox, {
    "data-test-subj": "timeline-events-select-positioned-checkbox"
  }, React.createElement(_eui.EuiCheckbox, {
    checked: checkState === 'checked',
    "data-test-subj": "events-select-checkbox",
    disabled: true,
    id: "timeline-".concat(timelineId, "-events-select"),
    indeterminate: checkState === 'indeterminate',
    onChange: _fp.noop
  }))));
});
exports.EventsSelect = EventsSelect;
EventsSelect.displayName = 'EventsSelect';