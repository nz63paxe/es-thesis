"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EMPTY_ACTION_SECONDARY = exports.EMPTY_ACTION_PRIMARY = exports.EMPTY_TITLE = exports.PAGE_TITLE = exports.KQL_PLACEHOLDER = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var KQL_PLACEHOLDER = _i18n.i18n.translate('xpack.siem.network.kqlPlaceholder', {
  defaultMessage: 'e.g. source.ip: "foo"'
});

exports.KQL_PLACEHOLDER = KQL_PLACEHOLDER;

var PAGE_TITLE = _i18n.i18n.translate('xpack.siem.network.pageTitle', {
  defaultMessage: 'Network'
});

exports.PAGE_TITLE = PAGE_TITLE;

var EMPTY_TITLE = _i18n.i18n.translate('xpack.siem.network.emptyTitle', {
  defaultMessage: 'It looks like you don’t have any indices relevant to network in the SIEM application'
});

exports.EMPTY_TITLE = EMPTY_TITLE;

var EMPTY_ACTION_PRIMARY = _i18n.i18n.translate('xpack.siem.network.emptyActionPrimary', {
  defaultMessage: 'View setup instructions'
});

exports.EMPTY_ACTION_PRIMARY = EMPTY_ACTION_PRIMARY;

var EMPTY_ACTION_SECONDARY = _i18n.i18n.translate('xpack.siem.network.emptyActionSecondary', {
  defaultMessage: 'Go to documentation'
});

exports.EMPTY_ACTION_SECONDARY = EMPTY_ACTION_SECONDARY;