"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedDate = exports.PreferenceFormattedDate = void 0;

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _constants = require("../../../common/constants");

var _use_kibana_ui_setting = require("../../lib/settings/use_kibana_ui_setting");

var _empty_value = require("../empty_value");

var _localized_date_tooltip = require("../localized_date_tooltip");

var _maybe_date = require("./maybe_date");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var PreferenceFormattedDate = (0, _recompose.pure)(function (_ref) {
  var value = _ref.value;

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_DATE_FORMAT),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      dateFormat = _useKibanaUiSetting2[0];

  var _useKibanaUiSetting3 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_DATE_FORMAT_TZ),
      _useKibanaUiSetting4 = _slicedToArray(_useKibanaUiSetting3, 1),
      dateFormatTz = _useKibanaUiSetting4[0];

  var _useKibanaUiSetting5 = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_TIMEZONE_BROWSER),
      _useKibanaUiSetting6 = _slicedToArray(_useKibanaUiSetting5, 1),
      timezone = _useKibanaUiSetting6[0];

  return React.createElement(React.Fragment, null, dateFormat && dateFormatTz && timezone ? _momentTimezone.default.tz(value, dateFormatTz === 'Browser' ? timezone : dateFormatTz).format(dateFormat) : _momentTimezone.default.utc(value).toISOString());
});
exports.PreferenceFormattedDate = PreferenceFormattedDate;
PreferenceFormattedDate.displayName = 'PreferenceFormattedDate';
/**
 * Renders the specified date value in a format determined by the user's preferences,
 * with a tooltip that renders:
 * - the name of the field
 * - a humanized relative date (e.g. 16 minutes ago)
 * - a long representation of the date that includes the day of the week (e.g. Thursday, March 21, 2019 6:47pm)
 * - the raw date value (e.g. 2019-03-22T00:47:46Z)
 */

var FormattedDate = (0, _recompose.pure)(function (_ref2) {
  var value = _ref2.value,
      fieldName = _ref2.fieldName;

  if (value == null) {
    return (0, _empty_value.getOrEmptyTagFromValue)(value);
  }

  var maybeDate = (0, _maybe_date.getMaybeDate)(value);
  return maybeDate.isValid() ? React.createElement(_localized_date_tooltip.LocalizedDateTooltip, {
    date: maybeDate.toDate(),
    fieldName: fieldName
  }, React.createElement(PreferenceFormattedDate, {
    value: maybeDate.toDate()
  })) : (0, _empty_value.getOrEmptyTagFromValue)(value);
});
exports.FormattedDate = FormattedDate;
FormattedDate.displayName = 'FormattedDate';