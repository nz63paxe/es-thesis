"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoadingStaticContentPanel = exports.LoadingStaticPanel = exports.LoadingPanel = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  flex: 0 0 auto;\n  align-self: center;\n  text-align: center;\n  height: fit-content;\n  .euiPanel.euiPanel--paddingMedium {\n    padding: 10px;\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  height: ", ";\n  position: ", ";\n  width: ", ";\n  overflow: hidden;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  z-index: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiPanel-loading-hide-border {\n    border: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// SIDE EFFECT: the following `injectGlobal` overrides default styling in angular code that was not theme-friendly
// eslint-disable-next-line no-unused-expressions
(0, _styledComponents.injectGlobal)(_templateObject());
var SpinnerFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
SpinnerFlexItem.displayName = 'SpinnerFlexItem';
var LoadingPanel = (0, _recompose.pure)(function (_ref) {
  var _ref$height = _ref.height,
      height = _ref$height === void 0 ? 'auto' : _ref$height,
      _ref$showBorder = _ref.showBorder,
      showBorder = _ref$showBorder === void 0 ? true : _ref$showBorder,
      text = _ref.text,
      width = _ref.width,
      _ref$position = _ref.position,
      position = _ref$position === void 0 ? 'relative' : _ref$position,
      _ref$zIndex = _ref.zIndex,
      zIndex = _ref$zIndex === void 0 ? 'inherit' : _ref$zIndex;
  return React.createElement(LoadingStaticPanel, {
    className: "app-loading",
    height: height,
    width: width,
    position: position,
    zIndex: zIndex
  }, React.createElement(LoadingStaticContentPanel, null, React.createElement(_eui.EuiPanel, {
    className: showBorder ? '' : 'euiPanel-loading-hide-border'
  }, React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    direction: "row",
    gutterSize: "none"
  }, React.createElement(SpinnerFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiLoadingSpinner, {
    size: "m"
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiText, null, text))))));
});
exports.LoadingPanel = LoadingPanel;
LoadingPanel.displayName = 'LoadingPanel';

var LoadingStaticPanel = _styledComponents.default.div(_templateObject3(), function (_ref2) {
  var height = _ref2.height;
  return height;
}, function (_ref3) {
  var position = _ref3.position;
  return position;
}, function (_ref4) {
  var width = _ref4.width;
  return width;
}, function (_ref5) {
  var zIndex = _ref5.zIndex;
  return zIndex;
});

exports.LoadingStaticPanel = LoadingStaticPanel;
LoadingStaticPanel.displayName = 'LoadingStaticPanel';

var LoadingStaticContentPanel = _styledComponents.default.div(_templateObject4());

exports.LoadingStaticContentPanel = LoadingStaticContentPanel;
LoadingStaticContentPanel.displayName = 'LoadingStaticContentPanel';