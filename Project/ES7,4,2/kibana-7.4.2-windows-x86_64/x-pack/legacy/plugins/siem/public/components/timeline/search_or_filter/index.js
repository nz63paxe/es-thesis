"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulSearchOrFilter = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _keury = require("../../../lib/keury");

var _store = require("../../../store");

var _search_or_filter = require("./search_or_filter");

var _actions = require("../../../store/actions");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var StatefulSearchOrFilterComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(StatefulSearchOrFilterComponent, _React$PureComponent);

  function StatefulSearchOrFilterComponent() {
    _classCallCheck(this, StatefulSearchOrFilterComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(StatefulSearchOrFilterComponent).apply(this, arguments));
  }

  _createClass(StatefulSearchOrFilterComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          applyKqlFilterQuery = _this$props.applyKqlFilterQuery,
          indexPattern = _this$props.indexPattern,
          filterQueryDraft = _this$props.filterQueryDraft,
          isFilterQueryDraftValid = _this$props.isFilterQueryDraftValid,
          kqlMode = _this$props.kqlMode,
          timelineId = _this$props.timelineId,
          setKqlFilterQueryDraft = _this$props.setKqlFilterQueryDraft,
          updateKqlMode = _this$props.updateKqlMode;

      var applyFilterQueryFromKueryExpression = function applyFilterQueryFromKueryExpression(expression) {
        return applyKqlFilterQuery({
          id: timelineId,
          filterQuery: {
            kuery: {
              kind: 'kuery',
              expression: expression
            },
            serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(expression, indexPattern)
          }
        });
      };

      var setFilterQueryDraftFromKueryExpression = function setFilterQueryDraftFromKueryExpression(expression) {
        return setKqlFilterQueryDraft({
          id: timelineId,
          filterQueryDraft: {
            kind: 'kuery',
            expression: expression
          }
        });
      };

      return React.createElement(_search_or_filter.SearchOrFilter, {
        applyKqlFilterQuery: applyFilterQueryFromKueryExpression,
        filterQueryDraft: filterQueryDraft,
        indexPattern: indexPattern,
        isFilterQueryDraftValid: isFilterQueryDraftValid,
        kqlMode: kqlMode,
        timelineId: timelineId,
        updateKqlMode: updateKqlMode,
        setKqlFilterQueryDraft: setFilterQueryDraftFromKueryExpression
      });
    }
  }]);

  return StatefulSearchOrFilterComponent;
}(React.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getTimeline = _store.timelineSelectors.getTimelineByIdSelector();

  var getKqlFilterQueryDraft = _store.timelineSelectors.getKqlFilterQueryDraftSelector();

  var isFilterQueryDraftValid = _store.timelineSelectors.isFilterQueryDraftValidSelector();

  var mapStateToProps = function mapStateToProps(state, _ref) {
    var timelineId = _ref.timelineId;
    var timeline = getTimeline(state, timelineId);
    return {
      kqlMode: (0, _fp.getOr)('filter', 'kqlMode', timeline),
      filterQueryDraft: getKqlFilterQueryDraft(state, timelineId),
      isFilterQueryDraftValid: isFilterQueryDraftValid(state, timelineId)
    };
  };

  return mapStateToProps;
};

var StatefulSearchOrFilter = (0, _reactRedux.connect)(makeMapStateToProps, {
  applyKqlFilterQuery: _actions.timelineActions.applyKqlFilterQuery,
  setKqlFilterQueryDraft: _actions.timelineActions.setKqlFilterQueryDraft,
  updateKqlMode: _actions.timelineActions.updateKqlMode
})(StatefulSearchOrFilterComponent);
exports.StatefulSearchOrFilter = StatefulSearchOrFilter;