"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EMPTY_ACTION_SECONDARY = exports.EMPTY_ACTION_PRIMARY = exports.EMPTY_TITLE = exports.PAGE_BADGE_TOOLTIP = exports.PAGE_BADGE_LABEL = exports.PAGE_SUBTITLE = exports.PAGE_TITLE = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var PAGE_TITLE = _i18n.i18n.translate('xpack.siem.overview.pageTitle', {
  defaultMessage: 'SIEM'
});

exports.PAGE_TITLE = PAGE_TITLE;

var PAGE_SUBTITLE = _i18n.i18n.translate('xpack.siem.overview.pageSubtitle', {
  defaultMessage: 'Security Information & Event Management with the Elastic Stack'
});

exports.PAGE_SUBTITLE = PAGE_SUBTITLE;

var PAGE_BADGE_LABEL = _i18n.i18n.translate('xpack.siem.overview.pageBadgeLabel', {
  defaultMessage: 'Beta'
});

exports.PAGE_BADGE_LABEL = PAGE_BADGE_LABEL;

var PAGE_BADGE_TOOLTIP = _i18n.i18n.translate('xpack.siem.overview.pageBadgeTooltip', {
  defaultMessage: 'SIEM is still in beta. Please help us improve by reporting issues or bugs in the Kibana repo.'
});

exports.PAGE_BADGE_TOOLTIP = PAGE_BADGE_TOOLTIP;

var EMPTY_TITLE = _i18n.i18n.translate('xpack.siem.overview.emptyTitle', {
  defaultMessage: 'It looks like you don’t have any indices relevant to the SIEM application'
});

exports.EMPTY_TITLE = EMPTY_TITLE;

var EMPTY_ACTION_PRIMARY = _i18n.i18n.translate('xpack.siem.overview.emptyActionPrimary', {
  defaultMessage: 'View setup instructions'
});

exports.EMPTY_ACTION_PRIMARY = EMPTY_ACTION_PRIMARY;

var EMPTY_ACTION_SECONDARY = _i18n.i18n.translate('xpack.siem.overview.emptyActionSecondary', {
  defaultMessage: 'Go to documentation'
});

exports.EMPTY_ACTION_SECONDARY = EMPTY_ACTION_SECONDARY;