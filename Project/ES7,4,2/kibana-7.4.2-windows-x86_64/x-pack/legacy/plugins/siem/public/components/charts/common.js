"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.browserTimezone = exports.getTheme = exports.getSeriesStyle = exports.SeriesType = exports.WrappedByAutoSizer = exports.chartDefaultSettings = exports.ChartHolder = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _charts = require("@elastic/charts");

var _i18n = require("@kbn/i18n");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _constants = require("../../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: ", "px;\n  position: relative;\n\n  &:hover {\n    z-index: 100;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var chartHeight = 74;
var chartDefaultRotation = 0;
var chartDefaultRendering = 'canvas';
var FlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
FlexGroup.displayName = 'FlexGroup';

var ChartHolder = function ChartHolder() {
  return _react.default.createElement(FlexGroup, {
    justifyContent: "center",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    textAlign: "center",
    color: "subdued"
  }, _i18n.i18n.translate('xpack.siem.chart.dataNotAvailableTitle', {
    defaultMessage: 'Chart Data Not Available'
  }))));
};

exports.ChartHolder = ChartHolder;
var chartDefaultSettings = {
  rotation: chartDefaultRotation,
  rendering: chartDefaultRendering,
  animatedData: false,
  showLegend: false,
  showLegendDisplayValue: false,
  debug: false
};
exports.chartDefaultSettings = chartDefaultSettings;

var WrappedByAutoSizer = _styledComponents.default.div(_templateObject2(), chartHeight);

exports.WrappedByAutoSizer = WrappedByAutoSizer;
WrappedByAutoSizer.displayName = 'WrappedByAutoSizer';
var SeriesType; // Customize colors: https://ela.st/custom-colors

exports.SeriesType = SeriesType;

(function (SeriesType) {
  SeriesType["BAR"] = "bar";
  SeriesType["AREA"] = "area";
  SeriesType["LINE"] = "line";
})(SeriesType || (exports.SeriesType = SeriesType = {}));

var getSeriesStyle = function getSeriesStyle(seriesKey, color, seriesType) {
  if (!color) return undefined;
  var customSeriesColors = new Map();
  var dataSeriesColorValues = {
    colorValues: seriesType === SeriesType.BAR ? [seriesKey] : [],
    specId: (0, _charts.getSpecId)(seriesKey)
  };
  customSeriesColors.set(dataSeriesColorValues, color);
  return customSeriesColors;
}; // Apply margins and paddings: https://ela.st/charts-spacing


exports.getSeriesStyle = getSeriesStyle;

var getTheme = function getTheme() {
  var theme = {
    chartMargins: {
      left: 0,
      right: 0,
      // Apply some paddings to the top to avoid chopping the y tick https://ela.st/chopping-edge
      top: 4,
      bottom: 0
    },
    chartPaddings: {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0
    },
    scales: {
      barsPadding: 0.5
    }
  };

  var isDarkMode = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_DARK_MODE);

  var defaultTheme = isDarkMode ? _charts.DARK_THEME : _charts.LIGHT_THEME;
  return (0, _charts.mergeWithDefaultTheme)(theme, defaultTheme);
};

exports.getTheme = getTheme;

var kibanaTimezone = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_DATE_FORMAT_TZ);

var browserTimezone = kibanaTimezone === 'Browser' ? _momentTimezone.default.tz.guess() : kibanaTimezone;
exports.browserTimezone = browserTimezone;