"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverviewComponent = void 0;

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _documentation_links = require("ui/documentation_links");

var _header_page = require("../../components/header_page");

var _overview_host = require("../../components/page/overview/overview_host");

var _overview_network = require("../../components/page/overview/overview_network");

var _global_time = require("../../containers/global_time");

var _summary = require("./summary");

var _empty_page = require("../../components/empty_page");

var _source = require("../../containers/source");

var _spy_routes = require("../../utils/route/spy_routes");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var basePath = _chrome.default.getBasePath();

var OverviewComponent = (0, _recompose.pure)(function () {
  var dateEnd = Date.now();

  var dateRange = _moment.default.duration(24, 'hours').asMilliseconds();

  var dateStart = dateEnd - dateRange;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_header_page.HeaderPage, {
    badgeLabel: i18n.PAGE_BADGE_LABEL,
    badgeTooltip: i18n.PAGE_BADGE_TOOLTIP,
    subtitle: i18n.PAGE_SUBTITLE,
    title: i18n.PAGE_TITLE
  }), _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref) {
    var indicesExist = _ref.indicesExist;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_global_time.GlobalTime, null, function (_ref2) {
      var setQuery = _ref2.setQuery;
      return _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_summary.Summary, null), _react.default.createElement(_overview_host.OverviewHost, {
        endDate: dateEnd,
        startDate: dateStart,
        setQuery: setQuery
      }), _react.default.createElement(_overview_network.OverviewNetwork, {
        endDate: dateEnd,
        startDate: dateStart,
        setQuery: setQuery
      }));
    }) : _react.default.createElement(_empty_page.EmptyPage, {
      actionPrimaryIcon: "gear",
      actionPrimaryLabel: i18n.EMPTY_ACTION_PRIMARY,
      actionPrimaryUrl: "".concat(basePath, "/app/kibana#/home/tutorial_directory/siem"),
      actionSecondaryIcon: "popout",
      actionSecondaryLabel: i18n.EMPTY_ACTION_SECONDARY,
      actionSecondaryTarget: "_blank",
      actionSecondaryUrl: _documentation_links.documentationLinks.siem,
      "data-test-subj": "empty-page",
      title: i18n.EMPTY_TITLE
    });
  }), _react.default.createElement(_spy_routes.SpyRoute, null));
});
exports.OverviewComponent = OverviewComponent;
OverviewComponent.displayName = 'OverviewComponent';