"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DeleteTimelineModalButton = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _delete_timeline_modal = require("./delete_timeline_modal");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Renders a button that when clicked, displays the `Delete Timeline` modal
 */
var DeleteTimelineModalButton =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(DeleteTimelineModalButton, _React$PureComponent);

  function DeleteTimelineModalButton(props) {
    var _this;

    _classCallCheck(this, DeleteTimelineModalButton);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(DeleteTimelineModalButton).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "toggleShowModal", function () {
      _this.setState(function (state) {
        return {
          showModal: !state.showModal
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onDelete", function () {
      var _this$props = _this.props,
          deleteTimelines = _this$props.deleteTimelines,
          savedObjectId = _this$props.savedObjectId;

      if (deleteTimelines != null && savedObjectId != null) {
        deleteTimelines([savedObjectId]);
      }

      _this.toggleShowModal();
    });

    _this.state = {
      showModal: false
    };
    return _this;
  }

  _createClass(DeleteTimelineModalButton, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          deleteTimelines = _this$props2.deleteTimelines,
          savedObjectId = _this$props2.savedObjectId,
          title = _this$props2.title;
      return React.createElement(React.Fragment, null, React.createElement(_eui.EuiToolTip, {
        content: i18n.DELETE
      }, React.createElement(_eui.EuiButtonIcon, {
        "aria-label": i18n.DELETE,
        color: "danger",
        "data-test-subj": "delete-timeline",
        iconSize: "s",
        iconType: "trash",
        isDisabled: deleteTimelines == null || savedObjectId == null || savedObjectId === '',
        onClick: this.toggleShowModal,
        size: "s"
      })), this.state.showModal ? React.createElement(_eui.EuiOverlayMask, null, React.createElement(_eui.EuiModal, {
        maxWidth: _delete_timeline_modal.DELETE_TIMELINE_MODAL_WIDTH,
        onClose: this.toggleShowModal
      }, React.createElement(_delete_timeline_modal.DeleteTimelineModal, {
        "data-test-subj": "delete-timeline-modal",
        onDelete: this.onDelete,
        title: title,
        toggleShowModal: this.toggleShowModal
      }))) : null);
    }
  }]);

  return DeleteTimelineModalButton;
}(React.PureComponent);

exports.DeleteTimelineModalButton = DeleteTimelineModalButton;