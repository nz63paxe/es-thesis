"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.anomaliesTableData = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _use_kibana_ui_setting = require("../../../lib/settings/use_kibana_ui_setting");

var _constants = require("../../../../common/constants");

var _throw_if_not_ok = require("./throw_if_not_ok");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var anomaliesTableData =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(body, headers, signal) {
    var _useKibanaUiSetting, _useKibanaUiSetting2, kbnVersion, response;

    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION), _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1), kbnVersion = _useKibanaUiSetting2[0];
            _context.next = 3;
            return fetch("".concat(_chrome.default.getBasePath(), "/api/ml/results/anomalies_table_data"), {
              method: 'POST',
              credentials: 'same-origin',
              body: JSON.stringify(body),
              headers: _objectSpread({
                'kbn-system-api': 'true',
                'content-Type': 'application/json',
                'kbn-xsrf': kbnVersion
              }, headers),
              signal: signal
            });

          case 3:
            response = _context.sent;
            _context.next = 6;
            return (0, _throw_if_not_ok.throwIfNotOk)(response);

          case 6:
            _context.next = 8;
            return response.json();

          case 8:
            return _context.abrupt("return", _context.sent);

          case 9:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function anomaliesTableData(_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}();

exports.anomaliesTableData = anomaliesTableData;