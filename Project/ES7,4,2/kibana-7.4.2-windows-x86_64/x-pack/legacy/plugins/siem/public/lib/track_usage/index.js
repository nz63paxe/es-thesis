"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "METRIC_TYPE", {
  enumerable: true,
  get: function get() {
    return _public.METRIC_TYPE;
  }
});
exports.TELEMETRY_EVENT = exports.trackUiAction = void 0;

var _public = require("../../../../../../../src/legacy/core_plugins/ui_metric/public");

var _constants = require("../../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var trackUiAction = (0, _public.createUiStatsReporter)(_constants.APP_ID);
exports.trackUiAction = trackUiAction;
var TELEMETRY_EVENT;
exports.TELEMETRY_EVENT = TELEMETRY_EVENT;

(function (TELEMETRY_EVENT) {
  TELEMETRY_EVENT["SIEM_JOB_ENABLED"] = "siem_job_enabled";
  TELEMETRY_EVENT["SIEM_JOB_DISABLED"] = "siem_job_disabled";
  TELEMETRY_EVENT["CUSTOM_JOB_ENABLED"] = "custom_job_enabled";
  TELEMETRY_EVENT["CUSTOM_JOB_DISABLED"] = "custom_job_disabled";
  TELEMETRY_EVENT["JOB_ENABLE_FAILURE"] = "job_enable_failure";
  TELEMETRY_EVENT["JOB_DISABLE_FAILURE"] = "job_disable_failure";
  TELEMETRY_EVENT["TIMELINE_OPENED"] = "open_timeline";
  TELEMETRY_EVENT["TAB_CLICKED"] = "tab_";
})(TELEMETRY_EVENT || (exports.TELEMETRY_EVENT = TELEMETRY_EVENT = {}));