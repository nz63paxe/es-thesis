"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBreadcrumbsForRoute = exports.siemRootBreadcrumb = exports.setBreadcrumbs = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _fp = require("lodash/fp");

var _constants = require("../../../../common/constants");

var _utils = require("../../../pages/hosts/details/utils");

var _ip_details = require("../../../pages/network/ip_details");

var _home_navigations = require("../../../pages/home/home_navigations");

var _link_to = require("../../link_to");

var _helpers = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var setBreadcrumbs = function setBreadcrumbs(object) {
  var breadcrumbs = getBreadcrumbsForRoute(object);

  if (breadcrumbs) {
    _chrome.default.breadcrumbs.set(breadcrumbs);
  }
};

exports.setBreadcrumbs = setBreadcrumbs;
var siemRootBreadcrumb = [{
  text: _constants.APP_NAME,
  href: (0, _link_to.getOverviewUrl)()
}];
exports.siemRootBreadcrumb = siemRootBreadcrumb;

var getBreadcrumbsForRoute = function getBreadcrumbsForRoute(object) {
  if (object != null && object.navTabs && object.pageName === _home_navigations.SiemPageName.hosts) {
    var tempNav = {
      urlKey: 'host',
      isDetailPage: false
    };
    var urlStateKeys = [(0, _fp.getOr)(tempNav, object.pageName, object.navTabs)];

    if (object.tabName != null) {
      urlStateKeys = [].concat(_toConsumableArray(urlStateKeys), [(0, _fp.getOr)(tempNav, object.tabName, object.navTabs)]);
    }

    return [].concat(siemRootBreadcrumb, _toConsumableArray((0, _utils.getBreadcrumbs)(object, urlStateKeys.reduce(function (acc, item) {
      acc = [].concat(_toConsumableArray(acc), [(0, _helpers.getSearch)(item, object)]);
      return acc;
    }, []))));
  }

  if (object != null && object.navTabs && object.pageName === _home_navigations.SiemPageName.network) {
    var _tempNav = {
      urlKey: 'network',
      isDetailPage: false
    };
    var _urlStateKeys = [(0, _fp.getOr)(_tempNav, object.pageName, object.navTabs)];
    return [].concat(siemRootBreadcrumb, _toConsumableArray((0, _ip_details.getBreadcrumbs)(object.detailName, _urlStateKeys.reduce(function (acc, item) {
      acc = [].concat(_toConsumableArray(acc), [(0, _helpers.getSearch)(item, object)]);
      return acc;
    }, []))));
  }

  if (object != null && object.navTabs && object.pageName && object.navTabs[object.pageName]) {
    return [].concat(siemRootBreadcrumb, [{
      text: object.navTabs[object.pageName].name,
      href: ''
    }]);
  }

  return null;
};

exports.getBreadcrumbsForRoute = getBreadcrumbsForRoute;