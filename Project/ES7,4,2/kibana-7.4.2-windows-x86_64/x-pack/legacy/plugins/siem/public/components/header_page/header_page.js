"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeaderPage = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggables = require("../draggables");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Header = _styledComponents.default.header(_templateObject(), function (_ref) {
  var theme = _ref.theme;
  return "\n    border-bottom: ".concat(theme.eui.euiBorderThin, ";\n    padding-bottom: ").concat(theme.eui.euiSizeL, ";\n    margin: ").concat(theme.eui.euiSizeL, " 0;\n  ");
});

Header.displayName = 'Header';
var HeaderPage = (0, _recompose.pure)(function (_ref2) {
  var badgeLabel = _ref2.badgeLabel,
      badgeTooltip = _ref2.badgeTooltip,
      children = _ref2.children,
      draggableArguments = _ref2.draggableArguments,
      subtitle = _ref2.subtitle,
      title = _ref2.title,
      rest = _objectWithoutProperties(_ref2, ["badgeLabel", "badgeTooltip", "children", "draggableArguments", "subtitle", "title"]);

  return _react.default.createElement(Header, rest, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiTitle, {
    size: "l"
  }, _react.default.createElement("h1", {
    "data-test-subj": "page_headline_title"
  }, !draggableArguments ? title : _react.default.createElement(_draggables.DefaultDraggable, {
    "data-test-subj": "page_headline_draggable",
    id: "header-page-draggable-".concat(draggableArguments.field, "-").concat(draggableArguments.value),
    field: draggableArguments.field,
    value: "".concat(draggableArguments.value)
  }), badgeLabel && _react.default.createElement(_react.default.Fragment, null, ' ', _react.default.createElement(_eui.EuiBetaBadge, {
    label: badgeLabel,
    tooltipContent: badgeTooltip,
    tooltipPosition: "bottom"
  })))), _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "xs"
  }, subtitle)), children && _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, children)));
});
exports.HeaderPage = HeaderPage;
HeaderPage.displayName = 'HeaderPage';