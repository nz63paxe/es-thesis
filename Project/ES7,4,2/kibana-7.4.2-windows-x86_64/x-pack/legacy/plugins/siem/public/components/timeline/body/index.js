"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Body = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _footer = require("../footer");

var _column_headers = require("./column_headers");

var _events = require("./events");

var _helpers = require("./helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  height: ", ";\n  overflow: hidden;\n  overflow-y: auto;\n  min-width: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  height: ", ";\n  overflow: hidden;\n  overflow-x: auto;\n  min-height: 0px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HorizontalScroll = _styledComponents.default.div(_templateObject(), function (_ref) {
  var height = _ref.height;
  return "".concat(height, "px");
});

HorizontalScroll.displayName = 'HorizontalScroll';

var VerticalScrollContainer = _styledComponents.default.div(_templateObject2(), function (_ref2) {
  var height = _ref2.height;
  return "".concat(height - _footer.footerHeight - 12, "px");
}, function (_ref3) {
  var minWidth = _ref3.minWidth;
  return "".concat(minWidth, "px");
});

VerticalScrollContainer.displayName = 'VerticalScrollContainer';
/** Renders the timeline body */

var Body = React.memo(function (_ref4) {
  var addNoteToEvent = _ref4.addNoteToEvent,
      browserFields = _ref4.browserFields,
      columnHeaders = _ref4.columnHeaders,
      columnRenderers = _ref4.columnRenderers,
      data = _ref4.data,
      eventIdToNoteIds = _ref4.eventIdToNoteIds,
      getNotesByIds = _ref4.getNotesByIds,
      height = _ref4.height,
      id = _ref4.id,
      _ref4$isEventViewer = _ref4.isEventViewer,
      isEventViewer = _ref4$isEventViewer === void 0 ? false : _ref4$isEventViewer,
      onColumnRemoved = _ref4.onColumnRemoved,
      onColumnResized = _ref4.onColumnResized,
      onColumnSorted = _ref4.onColumnSorted,
      onFilterChange = _ref4.onFilterChange,
      onPinEvent = _ref4.onPinEvent,
      onUpdateColumns = _ref4.onUpdateColumns,
      onUnPinEvent = _ref4.onUnPinEvent,
      pinnedEventIds = _ref4.pinnedEventIds,
      rowRenderers = _ref4.rowRenderers,
      sort = _ref4.sort,
      toggleColumn = _ref4.toggleColumn,
      updateNote = _ref4.updateNote;
  var columnWidths = columnHeaders.reduce(function (totalWidth, header) {
    return totalWidth + header.width;
  }, (0, _helpers.getActionsColumnWidth)(isEventViewer));
  return React.createElement(HorizontalScroll, {
    "data-test-subj": "horizontal-scroll",
    height: height
  }, React.createElement(_eui.EuiText, {
    size: "s"
  }, React.createElement(_column_headers.ColumnHeaders, {
    actionsColumnWidth: (0, _helpers.getActionsColumnWidth)(isEventViewer),
    browserFields: browserFields,
    columnHeaders: columnHeaders,
    isEventViewer: isEventViewer,
    onColumnRemoved: onColumnRemoved,
    onColumnResized: onColumnResized,
    onColumnSorted: onColumnSorted,
    onFilterChange: onFilterChange,
    onUpdateColumns: onUpdateColumns,
    showEventsSelect: false,
    sort: sort,
    timelineId: id,
    toggleColumn: toggleColumn,
    minWidth: columnWidths
  }), React.createElement(VerticalScrollContainer, {
    "data-test-subj": "vertical-scroll-container",
    height: height,
    minWidth: columnWidths
  }, React.createElement(_events.Events, {
    actionsColumnWidth: (0, _helpers.getActionsColumnWidth)(isEventViewer),
    addNoteToEvent: addNoteToEvent,
    browserFields: browserFields,
    columnHeaders: columnHeaders,
    columnRenderers: columnRenderers,
    data: data,
    eventIdToNoteIds: eventIdToNoteIds,
    getNotesByIds: getNotesByIds,
    id: id,
    isEventViewer: isEventViewer,
    onColumnResized: onColumnResized,
    onPinEvent: onPinEvent,
    onUpdateColumns: onUpdateColumns,
    onUnPinEvent: onUnPinEvent,
    pinnedEventIds: pinnedEventIds,
    rowRenderers: rowRenderers,
    toggleColumn: toggleColumn,
    updateNote: updateNote,
    minWidth: columnWidths
  }))));
});
exports.Body = Body;
Body.displayName = 'Body';