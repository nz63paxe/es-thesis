"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MarkdownHint = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  font-family: monospace;\n  margin-right: 5px;\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  text-decoration: line-through;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  font-style: italic;\n  margin-right: 5px;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  visibility: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n  margin-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Heading = _styledComponents.default.span(_templateObject());

Heading.displayName = 'Heading';

var Bold = _styledComponents.default.span(_templateObject2());

Bold.displayName = 'Bold';
var MarkdownHintContainer = (0, _styledComponents.default)(_eui.EuiText)(_templateObject3(), function (_ref) {
  var visibility = _ref.visibility;
  return visibility;
});
MarkdownHintContainer.displayName = 'MarkdownHintContainer';

var ImageUrl = _styledComponents.default.span(_templateObject4());

ImageUrl.displayName = 'ImageUrl';

var Italic = _styledComponents.default.span(_templateObject5());

Italic.displayName = 'Italic';

var Strikethrough = _styledComponents.default.span(_templateObject6());

Strikethrough.displayName = 'Strikethrough';

var Code = _styledComponents.default.span(_templateObject7());

Code.displayName = 'Code';

var TrailingWhitespace = _styledComponents.default.span(_templateObject8());

TrailingWhitespace.displayName = 'TrailingWhitespace';
var MarkdownHint = (0, _recompose.pure)(function (_ref2) {
  var show = _ref2.show;
  return React.createElement(MarkdownHintContainer, {
    color: "subdued",
    "data-test-subj": "markdown-hint",
    size: "xs",
    visibility: show ? 'inline' : 'hidden'
  }, React.createElement(Heading, {
    "data-test-subj": "heading-hint"
  }, i18n.MARKDOWN_HINT_HEADING), React.createElement(Bold, {
    "data-test-subj": "bold-hint"
  }, i18n.MARKDOWN_HINT_BOLD), React.createElement(Italic, {
    "data-test-subj": "italic-hint"
  }, i18n.MARKDOWN_HINT_ITALICS), React.createElement(Code, {
    "data-test-subj": "code-hint"
  }, i18n.MARKDOWN_HINT_CODE), React.createElement(TrailingWhitespace, null, i18n.MARKDOWN_HINT_URL), React.createElement(TrailingWhitespace, null, i18n.MARKDOWN_HINT_BULLET), React.createElement(Code, {
    "data-test-subj": "preformatted-hint"
  }, i18n.MARKDOWN_HINT_PREFORMATTED), React.createElement(TrailingWhitespace, null, i18n.MARKDOWN_HINT_QUOTE), '~~', React.createElement(Strikethrough, {
    "data-test-subj": "strikethrough-hint"
  }, i18n.MARKDOWN_HINT_STRIKETHROUGH), '~~', React.createElement(ImageUrl, null, i18n.MARKDOWN_HINT_IMAGE_URL));
});
exports.MarkdownHint = MarkdownHint;
MarkdownHint.displayName = 'MarkdownHint';