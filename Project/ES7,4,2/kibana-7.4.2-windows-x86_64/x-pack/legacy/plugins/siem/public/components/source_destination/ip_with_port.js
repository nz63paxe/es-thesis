"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IpWithPort = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ip = require("../ip");

var _port = require("../port");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 3px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var IpPortSeparator = _styledComponents.default.span(_templateObject());

IpPortSeparator.displayName = 'IpPortSeparator';
/**
 * Renders a separator (i.e. `:`) and a draggable, hyperlinked port when
 * a port is specified
 */

var PortWithSeparator = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      port = _ref.port,
      portFieldName = _ref.portFieldName;
  return port != null ? React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(IpPortSeparator, {
    "data-test-subj": "ip-port-separator"
  }, ':')), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_port.Port, {
    contextId: contextId,
    "data-test-subj": "port",
    eventId: eventId,
    fieldName: portFieldName,
    value: port
  }))) : null;
});
PortWithSeparator.displayName = 'PortWithSeparator';
/**
 * Renders a draggable, hyperlinked IP address, and if provided, an associated
 * draggable, hyperlinked port (with a separator between the IP address and port)
 */

var IpWithPort = (0, _recompose.pure)(function (_ref2) {
  var contextId = _ref2.contextId,
      eventId = _ref2.eventId,
      ip = _ref2.ip,
      ipFieldName = _ref2.ipFieldName,
      port = _ref2.port,
      portFieldName = _ref2.portFieldName;
  return React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_ip.Ip, {
    contextId: contextId,
    "data-test-subj": "ip",
    eventId: eventId,
    fieldName: ipFieldName,
    value: ip
  })), React.createElement(_eui.EuiFlexItem, null, React.createElement(PortWithSeparator, {
    contextId: contextId,
    eventId: eventId,
    port: port,
    portFieldName: portFieldName
  })));
});
exports.IpWithPort = IpWithPort;
IpWithPort.displayName = 'IpWithPort';