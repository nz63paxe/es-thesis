"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getEventsSelectOptions = exports.DropdownDisplay = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _pin = require("../../../../pin");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: row;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var InputDisplay = _styledComponents.default.div(_templateObject());

InputDisplay.displayName = 'InputDisplay';

var PinIconContainer = _styledComponents.default.div(_templateObject2());

PinIconContainer.displayName = 'PinIconContainer';

var PinActionItem = _styledComponents.default.div(_templateObject3());

PinActionItem.displayName = 'PinActionItem';
var DropdownDisplay = (0, _recompose.pure)(function (_ref) {
  var text = _ref.text;
  return React.createElement(_eui.EuiText, {
    size: "s",
    color: "subdued"
  }, text);
});
exports.DropdownDisplay = DropdownDisplay;
DropdownDisplay.displayName = 'DropdownDisplay';

var getEventsSelectOptions = function getEventsSelectOptions() {
  return [{
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(DropdownDisplay, {
      text: i18n.SELECT_ALL
    }),
    value: 'select-all'
  }, {
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(DropdownDisplay, {
      text: i18n.SELECT_NONE
    }),
    value: 'select-none'
  }, {
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(DropdownDisplay, {
      text: i18n.SELECT_PINNED
    }),
    value: 'select-pinned'
  }, {
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(DropdownDisplay, {
      text: i18n.SELECT_UNPINNED
    }),
    value: 'select-unpinned'
  }, {
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(PinActionItem, null, React.createElement(PinIconContainer, null, React.createElement(_pin.Pin, {
      allowUnpinning: true,
      pinned: true
    })), React.createElement(DropdownDisplay, {
      text: i18n.PIN_SELECTED
    })),
    value: 'pin-selected'
  }, {
    inputDisplay: React.createElement(InputDisplay, null),
    disabled: true,
    dropdownDisplay: React.createElement(PinActionItem, null, React.createElement(PinIconContainer, null, React.createElement(_pin.Pin, {
      allowUnpinning: true,
      pinned: false
    })), React.createElement(DropdownDisplay, {
      text: i18n.UNPIN_SELECTED
    })),
    value: 'unpin-selected'
  }];
};

exports.getEventsSelectOptions = getEventsSelectOptions;