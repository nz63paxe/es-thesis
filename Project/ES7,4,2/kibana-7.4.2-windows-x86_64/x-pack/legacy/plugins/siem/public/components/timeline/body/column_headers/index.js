"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ColumnHeaders = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _draggable_wrapper = require("../../../drag_and_drop/draggable_wrapper");

var _droppable_wrapper = require("../../../drag_and_drop/droppable_wrapper");

var _helpers = require("../../../drag_and_drop/helpers");

var _field_badge = require("../../../draggables/field_badge");

var _fields_browser = require("../../../fields_browser");

var _events_select = require("./events_select");

var _header = require("./header");

var _helpers2 = require("../../../fields_browser/helpers");

var _is_resizing = require("../../../resize_handle/is_resizing");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  {\n    border-radius: 4px;\n    position: relative;\n\n    &::before {\n      background-image: linear-gradient(\n          135deg,\n          ", " 25%,\n          transparent 25%\n        ),\n        linear-gradient(-135deg, ", " 25%, transparent 25%),\n        linear-gradient(135deg, transparent 75%, ", " 75%),\n        linear-gradient(-135deg, transparent 75%, ", " 75%);\n      background-position: 0 0, 1px 0, 1px -1px, 0px 1px;\n      background-size: 2px 2px;\n      bottom: 2px;\n      content: '';\n      display: block;\n      left: 2px;\n      position: absolute;\n      top: 2px;\n      width: 4px;\n    }\n\n    &:hover,\n    &:focus {\n      transition: background-color 0.7s ease;\n      background-color: #000;\n      color: #fff;\n\n      &::before {\n        background-image: linear-gradient(\n            135deg,\n            ", " 25%,\n            transparent 25%\n          ),\n          linear-gradient(-135deg, ", " 25%, transparent 25%),\n          linear-gradient(135deg, transparent 75%, ", " 75%),\n          linear-gradient(-135deg, transparent 75%, ", " 75%);\n      }\n    }\n  "]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 4px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  height: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  border-bottom: 1px solid ", "\n  display: block;\n  height: ", ";\n  overflow: hidden;\n  min-width: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  width: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ActionsContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var actionsColumnWidth = _ref.actionsColumnWidth;
  return actionsColumnWidth;
});

ActionsContainer.displayName = 'ActionsContainer';
var COLUMN_HEADERS_HEIGHT = '39px';

var ColumnHeadersContainer = _styledComponents.default.div(_templateObject2(), function (_ref2) {
  var theme = _ref2.theme;
  return "".concat(theme.eui.euiColorLightShade, ";");
}, COLUMN_HEADERS_HEIGHT, function (_ref3) {
  var minWidth = _ref3.minWidth;
  return "".concat(minWidth, "px");
});

ColumnHeadersContainer.displayName = 'ColumnHeadersContainer';
var ColumnHeadersFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject3(), COLUMN_HEADERS_HEIGHT);
ColumnHeadersFlexGroup.displayName = 'ColumnHeadersFlexGroup';
var EventsSelectContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject4());
EventsSelectContainer.displayName = 'EventsSelectContainer';

var HeaderContainer = _styledComponents.default.div(_templateObject5(), function (_ref4) {
  var theme = _ref4.theme;
  return (0, _styledComponents.css)(_templateObject6(), theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorEmptyShade, theme.eui.euiColorEmptyShade, theme.eui.euiColorEmptyShade, theme.eui.euiColorEmptyShade);
});

HeaderContainer.displayName = 'HeaderContainer';
/** Renders the timeline header columns */

var ColumnHeaders = React.memo(function (_ref5) {
  var actionsColumnWidth = _ref5.actionsColumnWidth,
      browserFields = _ref5.browserFields,
      columnHeaders = _ref5.columnHeaders,
      _ref5$isEventViewer = _ref5.isEventViewer,
      isEventViewer = _ref5$isEventViewer === void 0 ? false : _ref5$isEventViewer,
      onColumnRemoved = _ref5.onColumnRemoved,
      onColumnResized = _ref5.onColumnResized,
      onColumnSorted = _ref5.onColumnSorted,
      onUpdateColumns = _ref5.onUpdateColumns,
      _ref5$onFilterChange = _ref5.onFilterChange,
      onFilterChange = _ref5$onFilterChange === void 0 ? _fp.noop : _ref5$onFilterChange,
      showEventsSelect = _ref5.showEventsSelect,
      sort = _ref5.sort,
      timelineId = _ref5.timelineId,
      toggleColumn = _ref5.toggleColumn,
      minWidth = _ref5.minWidth;

  var _isContainerResizing = (0, _is_resizing.isContainerResizing)(),
      isResizing = _isContainerResizing.isResizing,
      setIsResizing = _isContainerResizing.setIsResizing;

  return React.createElement(ColumnHeadersContainer, {
    "data-test-subj": "column-headers",
    minWidth: minWidth
  }, React.createElement(ColumnHeadersFlexGroup, {
    alignItems: "center",
    "data-test-subj": "column-headers-group",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "actions-item",
    grow: false
  }, React.createElement(ActionsContainer, {
    actionsColumnWidth: actionsColumnWidth,
    "data-test-subj": "actions-container"
  }, React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, showEventsSelect && React.createElement(EventsSelectContainer, {
    grow: false
  }, React.createElement(_events_select.EventsSelect, {
    checkState: "unchecked",
    timelineId: timelineId
  })), React.createElement(_eui.EuiFlexItem, {
    grow: true
  }, React.createElement(_fields_browser.StatefulFieldsBrowser, {
    browserFields: browserFields,
    columnHeaders: columnHeaders,
    "data-test-subj": "field-browser",
    height: _helpers2.FIELD_BROWSER_HEIGHT,
    isEventViewer: isEventViewer,
    onUpdateColumns: onUpdateColumns,
    timelineId: timelineId,
    toggleColumn: toggleColumn,
    width: _helpers2.FIELD_BROWSER_WIDTH
  }))))), React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "headers-item",
    grow: false
  }, React.createElement(_droppable_wrapper.DroppableWrapper, {
    droppableId: "".concat(_helpers.droppableTimelineColumnsPrefix).concat(timelineId),
    height: COLUMN_HEADERS_HEIGHT,
    isDropDisabled: false,
    type: _helpers.DRAG_TYPE_FIELD
  }, React.createElement(_eui.EuiFlexGroup, {
    "data-test-subj": "headers-group",
    gutterSize: "none"
  }, columnHeaders.map(function (header, i) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: header.id
    }, React.createElement(_reactBeautifulDnd.Draggable, {
      "data-test-subj": "draggable",
      draggableId: (0, _helpers.getDraggableFieldId)({
        contextId: "timeline-column-headers-".concat(timelineId),
        fieldId: header.id
      }),
      index: i,
      type: _helpers.DRAG_TYPE_FIELD,
      isDragDisabled: isResizing
    }, function (provided, snapshot) {
      return React.createElement(HeaderContainer, _extends({}, provided.draggableProps, provided.dragHandleProps, {
        "data-test-subj": "draggable-header",
        innerRef: provided.innerRef,
        isDragging: snapshot.isDragging
      }), !snapshot.isDragging ? React.createElement(_header.Header, {
        timelineId: timelineId,
        header: header,
        onColumnRemoved: onColumnRemoved,
        onColumnResized: onColumnResized,
        onColumnSorted: onColumnSorted,
        onFilterChange: onFilterChange,
        setIsResizing: setIsResizing,
        sort: sort
      }) : React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_field_badge.DraggableFieldBadge, {
        fieldId: header.id
      })));
    }));
  }))))));
});
exports.ColumnHeaders = ColumnHeaders;
ColumnHeaders.displayName = 'ColumnHeaders';