"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.URL_STATE_KEYS = exports.ALL_URL_STATE_KEYS = void 0;

var _constants = require("./constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ALL_URL_STATE_KEYS = [_constants.CONSTANTS.kqlQuery, _constants.CONSTANTS.timerange, _constants.CONSTANTS.timelineId];
exports.ALL_URL_STATE_KEYS = ALL_URL_STATE_KEYS;
var URL_STATE_KEYS = {
  host: [_constants.CONSTANTS.kqlQuery, _constants.CONSTANTS.timerange, _constants.CONSTANTS.timelineId],
  network: [_constants.CONSTANTS.kqlQuery, _constants.CONSTANTS.timerange, _constants.CONSTANTS.timelineId],
  timeline: [_constants.CONSTANTS.timelineId, _constants.CONSTANTS.timerange],
  overview: [_constants.CONSTANTS.timelineId, _constants.CONSTANTS.timerange]
};
exports.URL_STATE_KEYS = URL_STATE_KEYS;