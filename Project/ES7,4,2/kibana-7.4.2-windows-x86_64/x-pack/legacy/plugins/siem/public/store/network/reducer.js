"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.networkReducer = exports.initialNetworkState = void 0;

var _typescriptFsaReducers = require("typescript-fsa-reducers");

var _types = require("../../graphql/types");

var _constants = require("../constants");

var _actions = require("./actions");

var _model = require("./model");

var _helpers = require("./helpers");

var _queries, _queries2;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialNetworkState = {
  page: {
    queries: (_queries = {}, _defineProperty(_queries, _model.NetworkTableType.topNFlowSource, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      topNFlowSort: {
        field: _types.NetworkTopNFlowFields.bytes_out,
        direction: _types.Direction.desc
      }
    }), _defineProperty(_queries, _model.NetworkTableType.topNFlowDestination, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      topNFlowSort: {
        field: _types.NetworkTopNFlowFields.bytes_out,
        direction: _types.Direction.desc
      }
    }), _defineProperty(_queries, _model.NetworkTableType.dns, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      dnsSortField: {
        field: _types.NetworkDnsFields.uniqueDomains,
        direction: _types.Direction.desc
      },
      isPtrIncluded: false
    }), _queries),
    filterQuery: null,
    filterQueryDraft: null
  },
  details: {
    queries: (_queries2 = {}, _defineProperty(_queries2, _model.IpDetailsTableType.domains, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      flowDirection: _types.FlowDirection.uniDirectional,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      domainsSortField: {
        field: _types.DomainsFields.bytes,
        direction: _types.Direction.desc
      }
    }), _defineProperty(_queries2, _model.IpDetailsTableType.tls, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      tlsSortField: {
        field: _types.TlsFields._id,
        direction: _types.Direction.desc
      }
    }), _defineProperty(_queries2, _model.IpDetailsTableType.users, {
      activePage: _constants.DEFAULT_TABLE_ACTIVE_PAGE,
      limit: _constants.DEFAULT_TABLE_LIMIT,
      usersSortField: {
        field: _types.UsersFields.name,
        direction: _types.Direction.asc
      }
    }), _queries2),
    filterQuery: null,
    filterQueryDraft: null,
    flowTarget: _types.FlowTarget.source
  }
};
exports.initialNetworkState = initialNetworkState;
var networkReducer = (0, _typescriptFsaReducers.reducerWithInitialState)(initialNetworkState).case(_actions.setNetworkTablesActivePageToZero, function (state) {
  return _objectSpread({}, state, {
    page: _objectSpread({}, state.page, {
      queries: (0, _helpers.setNetworkPageQueriesActivePageToZero)(state)
    }),
    details: _objectSpread({}, state.details, {
      queries: (0, _helpers.setNetworkDetailsQueriesActivePageToZero)(state)
    })
  });
}).case(_actions.updateIpDetailsTableActivePage, function (state, _ref) {
  var activePage = _ref.activePage,
      tableType = _ref.tableType;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, tableType, _objectSpread({}, state[_model.NetworkType.details].queries[tableType], {
      activePage: activePage
    })))
  })));
}).case(_actions.updateNetworkPageTableActivePage, function (state, _ref2) {
  var activePage = _ref2.activePage,
      tableType = _ref2.tableType;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.page, _objectSpread({}, state[_model.NetworkType.page], {
    queries: _objectSpread({}, state[_model.NetworkType.page].queries, _defineProperty({}, tableType, _objectSpread({}, state[_model.NetworkType.page].queries[tableType], {
      activePage: activePage
    })))
  })));
}).case(_actions.updateDnsLimit, function (state, _ref3) {
  var limit = _ref3.limit,
      networkType = _ref3.networkType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: _objectSpread({}, state[networkType].queries, _defineProperty({}, _model.NetworkTableType.dns, _objectSpread({}, state[_model.NetworkType.page].queries.dns, {
      limit: limit
    })))
  })));
}).case(_actions.updateDnsSort, function (state, _ref4) {
  var dnsSortField = _ref4.dnsSortField,
      networkType = _ref4.networkType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: _objectSpread({}, state[networkType].queries, _defineProperty({}, _model.NetworkTableType.dns, _objectSpread({}, state[_model.NetworkType.page].queries.dns, {
      dnsSortField: dnsSortField
    })))
  })));
}).case(_actions.updateIsPtrIncluded, function (state, _ref5) {
  var isPtrIncluded = _ref5.isPtrIncluded,
      networkType = _ref5.networkType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: _objectSpread({}, state[networkType].queries, _defineProperty({}, _model.NetworkTableType.dns, _objectSpread({}, state[_model.NetworkType.page].queries.dns, {
      isPtrIncluded: isPtrIncluded
    })))
  })));
}).case(_actions.updateTopNFlowLimit, function (state, _ref6) {
  var limit = _ref6.limit,
      networkType = _ref6.networkType,
      tableType = _ref6.tableType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: _objectSpread({}, state[networkType].queries, _defineProperty({}, tableType, _objectSpread({}, state[_model.NetworkType.page].queries[tableType], {
      limit: limit
    })))
  })));
}).case(_actions.updateTopNFlowSort, function (state, _ref7) {
  var topNFlowSort = _ref7.topNFlowSort,
      networkType = _ref7.networkType,
      tableType = _ref7.tableType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: _objectSpread({}, state[networkType].queries, _defineProperty({}, tableType, _objectSpread({}, state[_model.NetworkType.page].queries[tableType], {
      topNFlowSort: topNFlowSort
    })))
  })));
}).case(_actions.setNetworkFilterQueryDraft, function (state, _ref8) {
  var filterQueryDraft = _ref8.filterQueryDraft,
      networkType = _ref8.networkType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    filterQueryDraft: filterQueryDraft
  })));
}).case(_actions.applyNetworkFilterQuery, function (state, _ref9) {
  var filterQuery = _ref9.filterQuery,
      networkType = _ref9.networkType;
  return _objectSpread({}, state, _defineProperty({}, networkType, _objectSpread({}, state[networkType], {
    queries: (0, _helpers.setNetworkQueriesActivePageToZero)(state, networkType),
    filterQueryDraft: filterQuery.kuery,
    filterQuery: filterQuery
  })));
}).case(_actions.updateIpDetailsFlowTarget, function (state, _ref10) {
  var flowTarget = _ref10.flowTarget;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    flowTarget: flowTarget
  })));
}).case(_actions.updateDomainsLimit, function (state, _ref11) {
  var limit = _ref11.limit;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.domains, _objectSpread({}, state[_model.NetworkType.details].queries.domains, {
      limit: limit
    })))
  })));
}).case(_actions.updateTlsLimit, function (state, _ref12) {
  var limit = _ref12.limit;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.tls, _objectSpread({}, state[_model.NetworkType.details].queries.tls, {
      limit: limit
    })))
  })));
}).case(_actions.updateDomainsFlowDirection, function (state, _ref13) {
  var flowDirection = _ref13.flowDirection;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.domains, _objectSpread({}, state[_model.NetworkType.details].queries.domains, {
      flowDirection: flowDirection
    })))
  })));
}).case(_actions.updateDomainsSort, function (state, _ref14) {
  var domainsSortField = _ref14.domainsSortField;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.domains, _objectSpread({}, state[_model.NetworkType.details].queries.domains, {
      domainsSortField: domainsSortField
    })))
  })));
}).case(_actions.updateTlsSort, function (state, _ref15) {
  var tlsSortField = _ref15.tlsSortField;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.tls, _objectSpread({}, state[_model.NetworkType.details].queries.tls, {
      tlsSortField: tlsSortField
    })))
  })));
}).case(_actions.updateUsersLimit, function (state, _ref16) {
  var limit = _ref16.limit;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.users, _objectSpread({}, state[_model.NetworkType.details].queries.users, {
      limit: limit
    })))
  })));
}).case(_actions.updateUsersSort, function (state, _ref17) {
  var usersSortField = _ref17.usersSortField;
  return _objectSpread({}, state, _defineProperty({}, _model.NetworkType.details, _objectSpread({}, state[_model.NetworkType.details], {
    queries: _objectSpread({}, state[_model.NetworkType.details].queries, _defineProperty({}, _model.IpDetailsTableType.users, _objectSpread({}, state[_model.NetworkType.details].queries.users, {
      usersSortField: usersSortField
    })))
  })));
}).build();
exports.networkReducer = networkReducer;