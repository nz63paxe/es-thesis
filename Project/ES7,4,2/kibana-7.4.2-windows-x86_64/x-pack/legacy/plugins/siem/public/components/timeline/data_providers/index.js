"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataProviders = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _droppable_wrapper = require("../../drag_and_drop/droppable_wrapper");

var _helpers = require("../../drag_and_drop/helpers");

var _timeline_context = require("../timeline_context");

var _empty = require("./empty");

var _providers = require("./providers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  border: 0.2rem dashed ", ";\n  border-radius: 5px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  margin: 5px 0 5px 0;\n  min-height: 100px;\n  overflow-y: auto;\n  background-color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .", " & .drop-target-data-providers {\n    background: ", ";\n    border: 0.2rem dashed ", ";\n\n    & .euiTextColor--subdued {\n      color: ", ";\n    }\n\n    & .euiFormHelpText {\n      color: ", ";\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DropTargetDataProvidersContainer = _styledComponents.default.div(_templateObject(), _helpers.IS_DRAGGING_CLASS_NAME, function (_ref) {
  var theme = _ref.theme;
  return "".concat(theme.eui.euiColorSuccess).concat(_helpers.TEN_PERCENT_ALPHA_HEX_SUFFIX);
}, function (_ref2) {
  var theme = _ref2.theme;
  return theme.eui.euiColorSuccess;
}, function (_ref3) {
  var theme = _ref3.theme;
  return theme.eui.euiColorSuccess;
}, function (_ref4) {
  var theme = _ref4.theme;
  return theme.eui.euiColorSuccess;
});

var DropTargetDataProviders = _styledComponents.default.div(_templateObject2(), function (props) {
  return props.theme.eui.euiColorMediumShade;
}, function (props) {
  return props.theme.eui.euiFormBackgroundColor;
});

DropTargetDataProviders.displayName = 'DropTargetDataProviders';

var getDroppableId = function getDroppableId(id) {
  return "".concat(_helpers.droppableTimelineProvidersPrefix).concat(id);
};
/**
 * Renders the data providers section of the timeline.
 *
 * The data providers section is a drop target where users
 * can drag-and drop new data providers into the timeline.
 *
 * It renders an interactive card representation of the
 * data providers. It also provides uniform
 * UI controls for the following actions:
 * 1) removing a data provider
 * 2) temporarily disabling a data provider
 * 3) applying boolean negation to the data provider
 *
 * Given an empty collection of DataProvider[], it prompts
 * the user to drop anything with a facet count into
 * the data pro section.
 */


var DataProviders = (0, _recompose.pure)(function (_ref5) {
  var browserFields = _ref5.browserFields,
      id = _ref5.id,
      dataProviders = _ref5.dataProviders,
      onChangeDataProviderKqlQuery = _ref5.onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider = _ref5.onChangeDroppableAndProvider,
      onDataProviderEdited = _ref5.onDataProviderEdited,
      onDataProviderRemoved = _ref5.onDataProviderRemoved,
      onToggleDataProviderEnabled = _ref5.onToggleDataProviderEnabled,
      onToggleDataProviderExcluded = _ref5.onToggleDataProviderExcluded,
      show = _ref5.show;
  return React.createElement(DropTargetDataProvidersContainer, {
    className: "drop-target-data-providers-container"
  }, React.createElement(DropTargetDataProviders, {
    className: "drop-target-data-providers",
    "data-test-subj": "dataProviders"
  }, React.createElement(_timeline_context.TimelineContext.Consumer, null, function (isLoading) {
    return React.createElement(_droppable_wrapper.DroppableWrapper, {
      isDropDisabled: !show || isLoading,
      droppableId: getDroppableId(id)
    }, dataProviders != null && dataProviders.length ? React.createElement(_providers.Providers, {
      browserFields: browserFields,
      id: id,
      dataProviders: dataProviders,
      onChangeDataProviderKqlQuery: onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider: onChangeDroppableAndProvider,
      onDataProviderEdited: onDataProviderEdited,
      onDataProviderRemoved: onDataProviderRemoved,
      onToggleDataProviderEnabled: onToggleDataProviderEnabled,
      onToggleDataProviderExcluded: onToggleDataProviderExcluded
    }) : React.createElement(_empty.Empty, null));
  })));
});
exports.DataProviders = DataProviders;
DataProviders.displayName = 'DataProviders';