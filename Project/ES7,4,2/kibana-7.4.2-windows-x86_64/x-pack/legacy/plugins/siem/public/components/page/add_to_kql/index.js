"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddToKql = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _hosts = require("../../../containers/hosts");

var _network = require("../../../containers/network");

var _helpers = require("../../../lib/helpers");

var _with_hover_actions = require("../../with_hover_actions");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  height: 25px;\n  justify-content: center;\n  left: 5px;\n  position: absolute;\n  top: -10px;\n  width: 30px;\n  cursor: pointer;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AddToKqlComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(AddToKqlComponent, _React$PureComponent);

  function AddToKqlComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, AddToKqlComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(AddToKqlComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "addToKql", function () {
      var _this$props = _this.props,
          expression = _this$props.expression,
          filterQueryDraft = _this$props.filterQueryDraft,
          applyFilterQueryFromKueryExpression = _this$props.applyFilterQueryFromKueryExpression;
      applyFilterQueryFromKueryExpression(filterQueryDraft && !(0, _fp.isEmpty)(filterQueryDraft.expression) ? "".concat(filterQueryDraft.expression, " and ").concat(expression) : expression);
    });

    return _this;
  }

  _createClass(AddToKqlComponent, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return _react.default.createElement(_with_hover_actions.WithHoverActions, {
        hoverContent: _react.default.createElement(HoverActionsContainer, {
          "data-test-subj": "hover-actions-container"
        }, _react.default.createElement(_eui.EuiToolTip, {
          content: i18n.FILTER_FOR_VALUE
        }, _react.default.createElement(_eui.EuiIcon, {
          type: "filter",
          onClick: this.addToKql
        }))),
        render: function render() {
          return children;
        }
      });
    }
  }]);

  return AddToKqlComponent;
}(_react.default.PureComponent);

var HoverActionsContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject());
HoverActionsContainer.displayName = 'HoverActionsContainer';
var AddToKql = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children,
      expression = _ref.expression,
      type = _ref.type,
      componentFilterType = _ref.componentFilterType,
      indexPattern = _ref.indexPattern;

  switch (componentFilterType) {
    case 'hosts':
      return _react.default.createElement(_hosts.HostsFilter, {
        indexPattern: indexPattern,
        type: type
      }, function (_ref2) {
        var applyFilterQueryFromKueryExpression = _ref2.applyFilterQueryFromKueryExpression,
            filterQueryDraft = _ref2.filterQueryDraft;
        return _react.default.createElement(AddToKqlComponent, {
          applyFilterQueryFromKueryExpression: applyFilterQueryFromKueryExpression,
          expression: expression,
          filterQueryDraft: filterQueryDraft
        }, children);
      });

    case 'network':
      return _react.default.createElement(_network.NetworkFilter, {
        indexPattern: indexPattern,
        type: type
      }, function (_ref3) {
        var applyFilterQueryFromKueryExpression = _ref3.applyFilterQueryFromKueryExpression,
            filterQueryDraft = _ref3.filterQueryDraft;
        return _react.default.createElement(AddToKqlComponent, {
          applyFilterQueryFromKueryExpression: applyFilterQueryFromKueryExpression,
          expression: expression,
          filterQueryDraft: filterQueryDraft
        }, children);
      });
  }

  (0, _helpers.assertUnreachable)(componentFilterType, 'Unknown Filter Type in switch statement');
});
exports.AddToKql = AddToKql;
AddToKql.displayName = 'AddToKql';