"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateKql = void 0;

var _fp = require("lodash/fp");

var _model = require("../../store/hosts/model");

var _actions = require("../../store/hosts/actions");

var _actions2 = require("../../store/network/actions");

var _actions3 = require("../../store/timeline/actions");

var _keury = require("../../lib/keury");

var _model2 = require("../../store/network/model");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useUpdateKql = function useUpdateKql(_ref) {
  var indexPattern = _ref.indexPattern,
      kueryFilterQuery = _ref.kueryFilterQuery,
      kueryFilterQueryDraft = _ref.kueryFilterQueryDraft,
      storeType = _ref.storeType,
      timelineId = _ref.timelineId,
      type = _ref.type;

  var updateKql = function updateKql(dispatch) {
    if (kueryFilterQueryDraft != null && !(0, _fp.isEqual)(kueryFilterQuery, kueryFilterQueryDraft)) {
      if (storeType === 'hostsType' && (type === _model.HostsType.details || type === _model.HostsType.page)) {
        dispatch((0, _actions.applyHostsFilterQuery)({
          filterQuery: {
            kuery: {
              kind: 'kuery',
              expression: kueryFilterQueryDraft.expression
            },
            serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(kueryFilterQueryDraft.expression, indexPattern)
          },
          hostsType: type
        }));
      } else if (storeType === 'networkType' && (type === _model2.NetworkType.details || type === _model2.NetworkType.page)) {
        dispatch((0, _actions2.applyNetworkFilterQuery)({
          filterQuery: {
            kuery: {
              kind: 'kuery',
              expression: kueryFilterQueryDraft.expression
            },
            serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(kueryFilterQueryDraft.expression, indexPattern)
          },
          networkType: type
        }));
      } else if (storeType === 'timelineType' && timelineId != null) {
        dispatch((0, _actions3.applyKqlFilterQuery)({
          id: timelineId,
          filterQuery: {
            kuery: {
              kind: 'kuery',
              expression: kueryFilterQueryDraft.expression
            },
            serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(kueryFilterQueryDraft.expression, indexPattern)
          }
        }));
      }

      return true;
    }

    return false;
  };

  return updateKql;
};

exports.useUpdateKql = useUpdateKql;