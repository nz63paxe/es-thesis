"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Header = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _field_badge = require("../../../../draggables/field_badge");

var _resize_handle = require("../../../../resize_handle");

var _styled_handles = require("../../../../resize_handle/styled_handles");

var _truncatable_text = require("../../../../truncatable_text");

var _with_hover_actions = require("../../../../with_hover_actions");

var _actions = require("../actions");

var _styles = require("../common/styles");

var _filter = require("../filter");

var _header_tooltip_content = require("../header_tooltip_content");

var _helpers = require("./helpers");

var _timeline_context = require("../../../timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n  padding: 5px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  cursor: ", ";\n  display: flex;\n  height: 100%;\n  flex-direction: row;\n  overflow: hidden;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  width: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  height: 100%;\n  overflow: hidden;\n  width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TITLE_PADDING = 10; // px

var RESIZE_HANDLE_HEIGHT = 35; // px

var HeaderContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject(), function (_ref) {
  var width = _ref.width;
  return width;
});
HeaderContainer.displayName = 'HeaderContainer';
var HeaderFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2(), function (_ref2) {
  var width = _ref2.width;
  return width;
});
HeaderFlexItem.displayName = 'HeaderFlexItem';

var HeaderDiv = _styledComponents.default.div(_templateObject3(), function (_ref3) {
  var isLoading = _ref3.isLoading;
  return isLoading ? 'default' : 'grab';
});

HeaderDiv.displayName = 'HeaderDiv';
var HeaderComp = React.memo(function (_ref4) {
  var children = _ref4.children,
      onClick = _ref4.onClick,
      isResizing = _ref4.isResizing;
  var isLoading = (0, _timeline_context.useTimelineContext)();
  return React.createElement(HeaderDiv, {
    "data-test-subj": "header",
    onClick: !isResizing && !isLoading ? onClick : _fp.noop,
    isLoading: isLoading
  }, children);
});
HeaderComp.displayName = 'HeaderComp';
var TruncatableHeaderText = (0, _styledComponents.default)(_truncatable_text.TruncatableText)(_templateObject4());
TruncatableHeaderText.displayName = 'TruncatableHeaderText';

/** Renders a header */
var Header =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Header, _React$PureComponent);

  function Header() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Header);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Header)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "renderActions", function (isResizing) {
      var _this$props = _this.props,
          header = _this$props.header,
          onColumnRemoved = _this$props.onColumnRemoved,
          _this$props$onFilterC = _this$props.onFilterChange,
          onFilterChange = _this$props$onFilterC === void 0 ? _fp.noop : _this$props$onFilterC,
          setIsResizing = _this$props.setIsResizing,
          sort = _this$props.sort;
      setIsResizing(isResizing);
      return React.createElement(HeaderFlexItem, {
        grow: false,
        width: "".concat(header.width - _styled_handles.CELL_RESIZE_HANDLE_WIDTH, "px")
      }, React.createElement(_with_hover_actions.WithHoverActions, {
        render: function render(showHoverContent) {
          return React.createElement(React.Fragment, null, React.createElement(HeaderComp, {
            isResizing: isResizing,
            "data-test-subj": "header",
            onClick: _this.onClick
          }, React.createElement(_eui.EuiToolTip, {
            "data-test-subj": "header-tooltip",
            content: React.createElement(_header_tooltip_content.HeaderToolTipContent, {
              header: header
            })
          }, React.createElement(_styles.FullHeightFlexGroup, {
            "data-test-subj": "header-items",
            alignItems: "center",
            gutterSize: "none"
          }, React.createElement(_eui.EuiFlexItem, {
            grow: false
          }, React.createElement(_field_badge.FieldNameContainer, null, React.createElement(TruncatableHeaderText, {
            "data-test-subj": "header-text-".concat(header.id),
            size: "xs",
            width: "".concat(header.width - (_actions.ACTIONS_WIDTH + _styled_handles.CELL_RESIZE_HANDLE_WIDTH + TITLE_PADDING), "px")
          }, header.id))), React.createElement(_styles.FullHeightFlexItem, null, React.createElement(_actions.Actions, {
            header: header,
            onColumnRemoved: onColumnRemoved,
            show: header.id !== '@timestamp' ? showHoverContent : false,
            sort: sort
          }))))), React.createElement(_filter.Filter, {
            header: header,
            onFilterChange: onFilterChange
          }));
        }
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "onClick", function () {
      var _this$props2 = _this.props,
          header = _this$props2.header,
          onColumnSorted = _this$props2.onColumnSorted,
          sort = _this$props2.sort;

      if (header.aggregatable) {
        onColumnSorted({
          columnId: header.id,
          sortDirection: (0, _helpers.getNewSortDirectionOnClick)({
            clickedHeader: header,
            currentSort: sort
          })
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onResize", function (_ref5) {
      var delta = _ref5.delta,
          id = _ref5.id;

      _this.props.onColumnResized({
        columnId: id,
        delta: delta
      });
    });

    return _this;
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      var header = this.props.header;
      return React.createElement(HeaderContainer, {
        "data-test-subj": "header-container",
        gutterSize: "none",
        key: header.id,
        width: "".concat(header.width, "px")
      }, React.createElement(_resize_handle.Resizeable, {
        handle: React.createElement(_styles.FullHeightFlexItem, {
          grow: false
        }, React.createElement(_styled_handles.ColumnHeaderResizeHandle, null)),
        height: "".concat(RESIZE_HANDLE_HEIGHT, "px"),
        id: header.id,
        render: this.renderActions,
        onResize: this.onResize
      }));
    }
  }]);

  return Header;
}(React.PureComponent);

exports.Header = Header;