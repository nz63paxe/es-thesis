"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataDrivenColumns = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _resize_handle = require("../../../resize_handle");

var _styled_handles = require("../../../resize_handle/styled_handles");

var _styles = require("../column_headers/common/styles");

var _get_column_renderer = require("../renderers/get_column_renderer");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  height: 100%;\n  overflow: hidden;\n  width: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background: ", ";\n  height: 20px;\n  overflow: hidden;\n  user-select: none;\n  width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Cell = _styledComponents.default.div(_templateObject(), function (_ref) {
  var index = _ref.index,
      theme = _ref.theme;
  return index % 2 === 0 && theme.darkMode ? theme.eui.euiFormBackgroundColor : index % 2 === 0 && !theme.darkMode ? theme.eui.euiColorLightestShade : 'inherit';
}, function (_ref2) {
  var width = _ref2.width;
  return width;
});

Cell.displayName = 'Cell';
var CellContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2(), function (_ref3) {
  var width = _ref3.width;
  return width;
});
CellContainer.displayName = 'CellContainer';

var DataDrivenColumns =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(DataDrivenColumns, _React$PureComponent);

  function DataDrivenColumns() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DataDrivenColumns);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DataDrivenColumns)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "renderCell", function (header, index) {
      return function () {
        var _this$props = _this.props,
            columnRenderers = _this$props.columnRenderers,
            data = _this$props.data,
            _id = _this$props._id,
            timelineId = _this$props.timelineId;
        return React.createElement(_eui.EuiFlexItem, {
          grow: false
        }, React.createElement(Cell, {
          "data-test-subj": "column-cell",
          index: index,
          width: "".concat(header.width - _styled_handles.CELL_RESIZE_HANDLE_WIDTH, "px")
        }, (0, _get_column_renderer.getColumnRenderer)(header.id, columnRenderers, data).renderColumn({
          columnName: header.id,
          eventId: _id,
          values: getMappedNonEcsValue({
            data: data,
            fieldName: header.id
          }),
          field: header,
          width: "".concat(header.width - _styled_handles.CELL_RESIZE_HANDLE_WIDTH, "px"),
          timelineId: timelineId
        })));
      };
    });

    _defineProperty(_assertThisInitialized(_this), "onResize", function (_ref4) {
      var delta = _ref4.delta,
          id = _ref4.id;

      _this.props.onColumnResized({
        columnId: id,
        delta: delta
      });
    });

    return _this;
  }

  _createClass(DataDrivenColumns, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var columnHeaders = this.props.columnHeaders;
      return React.createElement(_eui.EuiFlexGroup, {
        "data-test-subj": "data-driven-columns",
        direction: "row",
        gutterSize: "none"
      }, columnHeaders.map(function (header, index) {
        return React.createElement(_eui.EuiFlexItem, {
          grow: false,
          key: header.id
        }, React.createElement(CellContainer, {
          "data-test-subj": "cell-container",
          gutterSize: "none",
          key: header.id,
          width: "".concat(header.width, "px")
        }, React.createElement(_resize_handle.Resizeable, {
          handle: React.createElement(_styles.FullHeightFlexItem, {
            grow: false
          }, React.createElement(_styled_handles.CellResizeHandle, {
            "data-test-subj": "cell-resize-handle"
          })),
          height: "100%",
          id: header.id,
          key: header.id,
          render: _this2.renderCell(header, index),
          onResize: _this2.onResize
        })));
      }));
    }
  }]);

  return DataDrivenColumns;
}(React.PureComponent);

exports.DataDrivenColumns = DataDrivenColumns;

var getMappedNonEcsValue = function getMappedNonEcsValue(_ref5) {
  var data = _ref5.data,
      fieldName = _ref5.fieldName;
  var item = data.find(function (d) {
    return d.field === fieldName;
  });

  if (item != null && item.value != null) {
    return item.value;
  }

  return undefined;
};