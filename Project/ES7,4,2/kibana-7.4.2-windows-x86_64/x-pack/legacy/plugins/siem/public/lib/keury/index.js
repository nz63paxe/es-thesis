"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.escapeKuery = exports.isFromKueryExpressionValid = exports.escapeQueryValue = exports.convertKueryToElasticSearchQuery = void 0;

var _esQuery = require("@kbn/es-query");

var _fp = require("lodash/fp");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var convertKueryToElasticSearchQuery = function convertKueryToElasticSearchQuery(kueryExpression, indexPattern) {
  try {
    return kueryExpression ? JSON.stringify((0, _esQuery.toElasticsearchQuery)((0, _esQuery.fromKueryExpression)(kueryExpression), indexPattern)) : '';
  } catch (err) {
    return '';
  }
};

exports.convertKueryToElasticSearchQuery = convertKueryToElasticSearchQuery;

var escapeQueryValue = function escapeQueryValue() {
  var val = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

  if ((0, _fp.isString)(val)) {
    if ((0, _fp.isEmpty)(val)) {
      return '""';
    }

    return "\"".concat(escapeKuery(val), "\"");
  }

  return val;
};

exports.escapeQueryValue = escapeQueryValue;

var isFromKueryExpressionValid = function isFromKueryExpressionValid(kqlFilterQuery) {
  if (kqlFilterQuery && kqlFilterQuery.kind === 'kuery') {
    try {
      (0, _esQuery.fromKueryExpression)(kqlFilterQuery.expression);
    } catch (err) {
      return false;
    }
  }

  return true;
};

exports.isFromKueryExpressionValid = isFromKueryExpressionValid;

var escapeWhitespace = function escapeWhitespace(val) {
  return val.replace(/\t/g, '\\t').replace(/\r/g, '\\r').replace(/\n/g, '\\n');
}; // See the SpecialCharacter rule in kuery.peg


var escapeSpecialCharacters = function escapeSpecialCharacters(val) {
  return val.replace(/["]/g, '\\$&');
}; // $& means the whole matched string
// See the Keyword rule in kuery.peg


var escapeAndOr = function escapeAndOr(val) {
  return val.replace(/(\s+)(and|or)(\s+)/gi, '$1\\$2$3');
};

var escapeNot = function escapeNot(val) {
  return val.replace(/not(\s+)/gi, '\\$&');
};

var escapeKuery = (0, _fp.flow)(escapeSpecialCharacters, escapeAndOr, escapeNot, escapeWhitespace);
exports.escapeKuery = escapeKuery;