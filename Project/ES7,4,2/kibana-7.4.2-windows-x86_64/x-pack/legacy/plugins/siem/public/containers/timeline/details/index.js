"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimelineDetailsComponentQuery = void 0;

var _fp = require("lodash/fp");

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _constants = require("../../../../common/constants");

var _index = require("./index.gql_query");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var TimelineDetailsComponentQuery =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(TimelineDetailsComponentQuery, _React$PureComponent);

  function TimelineDetailsComponentQuery(props) {
    var _this;

    _classCallCheck(this, TimelineDetailsComponentQuery);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TimelineDetailsComponentQuery).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedDetailsEvents", void 0);

    _defineProperty(_assertThisInitialized(_this), "getDetailsEvent", function (variables, detail) {
      return detail;
    });

    _this.memoizedDetailsEvents = (0, _memoizeOne.default)(_this.getDetailsEvent);
    return _this;
  }

  _createClass(TimelineDetailsComponentQuery, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          children = _this$props.children,
          indexName = _this$props.indexName,
          eventId = _this$props.eventId,
          executeQuery = _this$props.executeQuery,
          sourceId = _this$props.sourceId;
      var variables = {
        sourceId: sourceId,
        indexName: indexName,
        eventId: eventId,
        defaultIndex: _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_INDEX_KEY)
      };
      return executeQuery ? _react.default.createElement(_reactApollo.Query, {
        query: _index.timelineDetailsQuery,
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: variables
      }, function (_ref) {
        var data = _ref.data,
            loading = _ref.loading,
            refetch = _ref.refetch;
        return children({
          loading: loading,
          detailsData: _this2.memoizedDetailsEvents(JSON.stringify(variables), (0, _fp.getOr)([], 'source.TimelineDetails.data', data))
        });
      }) : children({
        loading: false,
        detailsData: null
      });
    }
  }]);

  return TimelineDetailsComponentQuery;
}(_react.default.PureComponent);

exports.TimelineDetailsComponentQuery = TimelineDetailsComponentQuery;