"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedFieldValue = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _fp = require("lodash/fp");

var _eui = require("@elastic/eui");

var _bytes = require("../../../bytes");

var _duration = require("../../../duration");

var _empty_value = require("../../../empty_value");

var _formatted_date = require("../../../formatted_date");

var _formatted_ip = require("../../../formatted_ip");

var _links = require("../../../links");

var _port = require("../../../port");

var _constants = require("./constants");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var FormattedFieldValue = (0, _recompose.pure)(function (_ref) {
  var eventId = _ref.eventId,
      contextId = _ref.contextId,
      fieldFormat = _ref.fieldFormat,
      fieldName = _ref.fieldName,
      fieldType = _ref.fieldType,
      value = _ref.value,
      width = _ref.width;

  if (fieldType === _constants.IP_FIELD_TYPE) {
    return React.createElement(_formatted_ip.FormattedIp, {
      eventId: eventId,
      contextId: contextId,
      fieldName: fieldName,
      value: !(0, _fp.isNumber)(value) ? value : String(value)
    });
  } else if (fieldType === _constants.DATE_FIELD_TYPE) {
    return React.createElement(_formatted_date.FormattedDate, {
      fieldName: fieldName,
      value: value
    });
  } else if (_port.PORT_NAMES.some(function (portName) {
    return fieldName === portName;
  })) {
    return React.createElement(_port.Port, {
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      value: "".concat(value)
    });
  } else if (fieldName === _duration.EVENT_DURATION_FIELD_NAME) {
    return React.createElement(_duration.Duration, {
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      value: "".concat(value)
    });
  } else if (fieldName === _constants.HOST_NAME_FIELD_NAME) {
    var hostname = "".concat(value);
    return (0, _fp.isString)(value) && hostname.length > 0 ? React.createElement(_eui.EuiToolTip, {
      content: value
    }, React.createElement(_links.HostDetailsLink, {
      "data-test-subj": "host-details-link",
      hostName: hostname
    }, value)) : (0, _empty_value.getEmptyTagValue)();
  } else if (fieldFormat === _bytes.BYTES_FORMAT) {
    return React.createElement(_bytes.Bytes, {
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      value: "".concat(value)
    });
  } else if (fieldName === _constants.MESSAGE_FIELD_NAME && value != null && value !== '') {
    return React.createElement(_eui.EuiToolTip, {
      position: "left",
      "data-test-subj": "message-tool-tip",
      content: React.createElement(_eui.EuiFlexGroup, {
        direction: "column",
        gutterSize: "none"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement("span", null, fieldName)), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement("span", null, value)))
    }, React.createElement("span", {
      "data-test-subj": "truncatable-message"
    }, value));
  } else {
    return (0, _empty_value.getOrEmptyTagFromValue)(value);
  }
});
exports.FormattedFieldValue = FormattedFieldValue;
FormattedFieldValue.displayName = 'FormattedFieldValue';