"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FieldsPane = void 0;

var _eui = require("@elastic/eui");

var _recompose = require("recompose");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _category = require("./category");

var _field_items = require("./field_items");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  width: ", "px;\n  height: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NoFieldsPanel = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.euiColorLightestShade;
}, _helpers.FIELDS_PANE_WIDTH, _helpers.TABLE_HEIGHT);

NoFieldsPanel.displayName = 'NoFieldsPanel';
var NoFieldsFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2());
NoFieldsFlexGroup.displayName = 'NoFieldsFlexGroup';
var FieldsPane = (0, _recompose.pure)(function (_ref) {
  var columnHeaders = _ref.columnHeaders,
      filteredBrowserFields = _ref.filteredBrowserFields,
      onCategorySelected = _ref.onCategorySelected,
      onUpdateColumns = _ref.onUpdateColumns,
      searchInput = _ref.searchInput,
      selectedCategoryId = _ref.selectedCategoryId,
      timelineId = _ref.timelineId,
      toggleColumn = _ref.toggleColumn,
      width = _ref.width;
  return React.createElement(React.Fragment, null, Object.keys(filteredBrowserFields).length > 0 ? React.createElement(_category.Category, {
    categoryId: selectedCategoryId,
    "data-test-subj": "category",
    filteredBrowserFields: filteredBrowserFields,
    fieldItems: (0, _field_items.getFieldItems)({
      browserFields: filteredBrowserFields,
      category: filteredBrowserFields[selectedCategoryId],
      categoryId: selectedCategoryId,
      columnHeaders: columnHeaders,
      highlight: searchInput,
      onUpdateColumns: onUpdateColumns,
      timelineId: timelineId,
      toggleColumn: toggleColumn
    }),
    width: width,
    onCategorySelected: onCategorySelected,
    timelineId: timelineId
  }) : React.createElement(NoFieldsPanel, null, React.createElement(NoFieldsFlexGroup, {
    alignItems: "center",
    gutterSize: "none",
    justifyContent: "center"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement("h3", {
    "data-test-subj": "no-fields-match"
  }, i18n.NO_FIELDS_MATCH_INPUT(searchInput))))));
});
exports.FieldsPane = FieldsPane;
FieldsPane.displayName = 'FieldsPane';