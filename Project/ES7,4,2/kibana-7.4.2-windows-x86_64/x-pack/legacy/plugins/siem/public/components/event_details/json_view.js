"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildJsonView = exports.JsonView = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("../timeline/body/helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var JsonEditor = _styledComponents.default.div(_templateObject());

JsonEditor.displayName = 'JsonEditor';
var JsonView = (0, _recompose.pure)(function (_ref) {
  var data = _ref.data;
  return React.createElement(JsonEditor, {
    "data-test-subj": "jsonView"
  }, React.createElement(_eui.EuiCodeEditor, {
    isReadOnly: true,
    mode: "javascript",
    setOptions: {
      fontSize: '12px'
    },
    value: JSON.stringify(buildJsonView(data), _helpers.omitTypenameAndEmpty, 2 // indent level
    ),
    width: "100%"
  }));
});
exports.JsonView = JsonView;
JsonView.displayName = 'JsonView';

var buildJsonView = function buildJsonView(data) {
  return data.reduce(function (accumulator, item) {
    return (0, _fp.set)(item.field, item.originalValue, accumulator);
  }, {});
};

exports.buildJsonView = buildJsonView;