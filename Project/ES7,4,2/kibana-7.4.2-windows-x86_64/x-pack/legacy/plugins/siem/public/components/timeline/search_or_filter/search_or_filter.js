"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchOrFilter = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _kuery_autocompletion = require("../../../containers/kuery_autocompletion");

var _autocomplete_field = require("../../autocomplete_field");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  user-select: none;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin: 5px 0 10px 0;\n  user-select: none;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .", " {\n    width: 350px !important;\n  }\n\n  .", "__popoverPanel {\n    width: ", ";\n\n    .euiSuperSelect__listbox {\n      width: ", " !important;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var timelineSelectModeItemsClassName = 'timelineSelectModeItemsClassName';
var searchOrFilterPopoverClassName = 'searchOrFilterPopover';
var searchOrFilterPopoverWidth = '352px'; // SIDE EFFECT: the following creates a global class selector
// eslint-disable-next-line no-unused-expressions

(0, _styledComponents.injectGlobal)(_templateObject(), timelineSelectModeItemsClassName, searchOrFilterPopoverClassName, searchOrFilterPopoverWidth, searchOrFilterPopoverWidth);

var SearchOrFilterContainer = _styledComponents.default.div(_templateObject2());

SearchOrFilterContainer.displayName = 'SearchOrFilterContainer';
var ModeFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject3());
ModeFlexItem.displayName = 'ModeFlexItem';
var SearchOrFilter = (0, _recompose.pure)(function (_ref) {
  var applyKqlFilterQuery = _ref.applyKqlFilterQuery,
      indexPattern = _ref.indexPattern,
      isFilterQueryDraftValid = _ref.isFilterQueryDraftValid,
      filterQueryDraft = _ref.filterQueryDraft,
      kqlMode = _ref.kqlMode,
      timelineId = _ref.timelineId,
      setKqlFilterQueryDraft = _ref.setKqlFilterQueryDraft,
      updateKqlMode = _ref.updateKqlMode;
  return React.createElement(SearchOrFilterContainer, null, React.createElement(_eui.EuiFlexGroup, {
    "data-test-subj": "timeline-search-or-filter",
    gutterSize: "xs"
  }, React.createElement(ModeFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiToolTip, {
    content: i18n.FILTER_OR_SEARCH_WITH_KQL
  }, React.createElement(_eui.EuiSuperSelect, {
    "data-test-subj": "timeline-select-search-or-filter",
    hasDividers: true,
    itemLayoutAlign: "top",
    itemClassName: timelineSelectModeItemsClassName,
    onChange: function onChange(mode) {
      return updateKqlMode({
        id: timelineId,
        kqlMode: mode
      });
    },
    options: _helpers.options,
    popoverClassName: searchOrFilterPopoverClassName,
    valueOfSelected: kqlMode
  }))), React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "timeline-search-or-filter-search-container"
  }, React.createElement(_eui.EuiToolTip, {
    content: _helpers.modes[kqlMode].kqlBarTooltip
  }, React.createElement(_kuery_autocompletion.KueryAutocompletion, {
    indexPattern: indexPattern
  }, function (_ref2) {
    var isLoadingSuggestions = _ref2.isLoadingSuggestions,
        loadSuggestions = _ref2.loadSuggestions,
        suggestions = _ref2.suggestions;
    return React.createElement(_autocomplete_field.AutocompleteField, {
      isLoadingSuggestions: isLoadingSuggestions,
      isValid: isFilterQueryDraftValid,
      loadSuggestions: loadSuggestions,
      onChange: setKqlFilterQueryDraft,
      onSubmit: applyKqlFilterQuery,
      placeholder: (0, _helpers.getPlaceholderText)(kqlMode),
      suggestions: suggestions,
      value: filterQueryDraft ? filterQueryDraft.expression : ''
    });
  })))));
});
exports.SearchOrFilter = SearchOrFilter;
SearchOrFilter.displayName = 'SearchOrFilter';