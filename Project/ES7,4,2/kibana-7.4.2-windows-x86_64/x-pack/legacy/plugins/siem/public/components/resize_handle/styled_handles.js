"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimelineResizeHandle = exports.ColumnHeaderResizeHandle = exports.CellResizeHandle = exports.CommonResizeHandle = exports.TIMELINE_RESIZE_HANDLE_WIDTH = exports.CELL_RESIZE_HANDLE_WIDTH = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  border: ", "px solid ", ";\n  z-index: 2;\n  height: ", ";\n  position: absolute;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  border: ", "px solid ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  border-right: ", "px solid\n    ", ";\n  border-top: ", "px solid ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  cursor: col-resize;\n  height: 100%;\n  min-height: 20px;\n  width: 0;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CELL_RESIZE_HANDLE_WIDTH = 2; // px;

exports.CELL_RESIZE_HANDLE_WIDTH = CELL_RESIZE_HANDLE_WIDTH;
var TIMELINE_RESIZE_HANDLE_WIDTH = 2; // px

exports.TIMELINE_RESIZE_HANDLE_WIDTH = TIMELINE_RESIZE_HANDLE_WIDTH;

var CommonResizeHandle = _styledComponents.default.div(_templateObject());

exports.CommonResizeHandle = CommonResizeHandle;
CommonResizeHandle.displayName = 'CommonResizeHandle';
var CellResizeHandle = (0, _styledComponents.default)(CommonResizeHandle)(_templateObject2(), CELL_RESIZE_HANDLE_WIDTH, function (_ref) {
  var theme = _ref.theme;
  return theme.darkMode ? theme.eui.euiFormBackgroundColor : theme.eui.euiColorLightestShade;
}, CELL_RESIZE_HANDLE_WIDTH, function (_ref2) {
  var theme = _ref2.theme;
  return theme.eui.euiColorLightShade;
});
exports.CellResizeHandle = CellResizeHandle;
CellResizeHandle.displayName = 'CellResizeHandle';
var ColumnHeaderResizeHandle = (0, _styledComponents.default)(CommonResizeHandle)(_templateObject3(), CELL_RESIZE_HANDLE_WIDTH, function (_ref3) {
  var theme = _ref3.theme;
  return theme.eui.euiColorLightestShade;
});
exports.ColumnHeaderResizeHandle = ColumnHeaderResizeHandle;
ColumnHeaderResizeHandle.displayName = 'ColumnHeaderResizeHandle';
var TimelineResizeHandle = (0, _styledComponents.default)(CommonResizeHandle)(_templateObject4(), TIMELINE_RESIZE_HANDLE_WIDTH, function (props) {
  return props.theme.eui.euiColorLightShade;
}, function (_ref4) {
  var height = _ref4.height;
  return "".concat(height, "px");
});
exports.TimelineResizeHandle = TimelineResizeHandle;
TimelineResizeHandle.displayName = 'TimelineResizeHandle';