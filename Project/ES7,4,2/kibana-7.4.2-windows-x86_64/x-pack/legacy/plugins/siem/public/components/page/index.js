"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverviewWrapper = exports.MoreRowItems = exports.Badge = exports.Spacer = exports.CountBadge = exports.Pane1FlexContent = exports.PaneHeader = exports.Pane = exports.PaneScrollContainer = exports.FooterContainer = exports.PageHeader = exports.FlexPage = exports.PageContent = exports.PageContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject13() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n\n  .euiButtonIcon {\n    position: absolute;\n    right: ", ";\n    top: 6px;\n    z-index: 2;\n  }\n"]);

  _templateObject13 = function _templateObject13() {
    return data;
  };

  return data;
}

function _templateObject12() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject12 = function _templateObject12() {
    return data;
  };

  return data;
}

function _templateObject11() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject11 = function _templateObject11() {
    return data;
  };

  return data;
}

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject10() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  height: 100%;\n"]);

  _templateObject10 = function _templateObject10() {
    return data;
  };

  return data;
}

function _templateObject9() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n"]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n  overflow: hidden;\n  user-select: none;\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n  overflow-y: scroll;\n  > div:last-child {\n    margin-bottom: 3rem;\n  }\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  bottom: 0;\n  color: #666;\n  left: 0;\n  padding: 8px;\n  position: fixed;\n  text-align: left;\n  user-select: none;\n  width: 100%;\n  background-color: #f5f7fa;\n  padding: 16px;\n  border-top: 1px solid #d3dae6;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  display: flex;\n  user-select: none;\n  padding: 1rem 1rem 0rem 1rem;\n  width: 100vw;\n  position: fixed;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  flex: 1 0 0;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  flex: 1 1 auto;\n  height: 100%;\n  position: relative;\n  overflow-y: hidden;\n  background-color: ", ";\n  margin-top: 62px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: column;\n  align-items: stretch;\n  background-color: ", ";\n  height: 100%;\n  padding: 1rem;\n  overflow: hidden;\n  margin: 0px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  div.app-wrapper {\n    background-color: rgba(0,0,0,0);\n  }\n\n  div.application {\n    background-color: rgba(0,0,0,0);\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// SIDE EFFECT: the following `injectGlobal` overrides default styling in angular code that was not theme-friendly
// eslint-disable-next-line no-unused-expressions
(0, _styledComponents.injectGlobal)(_templateObject());

var PageContainer = _styledComponents.default.div(_templateObject2(), function (props) {
  return props.theme.eui.euiColorEmptyShade;
});

exports.PageContainer = PageContainer;
PageContainer.displayName = 'PageContainer';

var PageContent = _styledComponents.default.div(_templateObject3(), function (props) {
  return props.theme.eui.euiColorEmptyShade;
});

exports.PageContent = PageContent;
PageContent.displayName = 'PageContent';
var FlexPage = (0, _styledComponents.default)(_eui.EuiPage)(_templateObject4());
exports.FlexPage = FlexPage;
FlexPage.displayName = 'FlexPage';

var PageHeader = _styledComponents.default.div(_templateObject5(), function (props) {
  return props.theme.eui.euiColorEmptyShade;
});

exports.PageHeader = PageHeader;
PageHeader.displayName = 'PageHeader';

var FooterContainer = _styledComponents.default.div(_templateObject6());

exports.FooterContainer = FooterContainer;
FooterContainer.displayName = 'FooterContainer';

var PaneScrollContainer = _styledComponents.default.div(_templateObject7());

exports.PaneScrollContainer = PaneScrollContainer;
PaneScrollContainer.displayName = 'PaneScrollContainer';

var Pane = _styledComponents.default.div(_templateObject8());

exports.Pane = Pane;
Pane.displayName = 'Pane';

var PaneHeader = _styledComponents.default.div(_templateObject9());

exports.PaneHeader = PaneHeader;
PaneHeader.displayName = 'PaneHeader';

var Pane1FlexContent = _styledComponents.default.div(_templateObject10());

exports.Pane1FlexContent = Pane1FlexContent;
Pane1FlexContent.displayName = 'Pane1FlexContent'; // Ref: https://github.com/elastic/eui/issues/1655
// const Badge = styled(EuiBadge)`
//   margin-left: 5px;
// `;

var CountBadge = function CountBadge(props) {
  return _react.default.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      marginLeft: '5px'
    }
  }));
};

exports.CountBadge = CountBadge;
CountBadge.displayName = 'CountBadge';

var Spacer = _styledComponents.default.span(_templateObject11());

exports.Spacer = Spacer;
Spacer.displayName = 'Spacer'; // Ref: https://github.com/elastic/eui/issues/1655
// export const Badge = styled(EuiBadge)`
//   vertical-align: top;
// `;

var Badge = function Badge(props) {
  return _react.default.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      verticalAlign: 'top'
    }
  }));
};

exports.Badge = Badge;
Badge.displayName = 'Badge';
var MoreRowItems = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject12());
exports.MoreRowItems = MoreRowItems;
MoreRowItems.displayName = 'MoreRowItems';
var OverviewWrapper = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject13(), function (props) {
  return props.theme.eui.euiSizeM;
});
exports.OverviewWrapper = OverviewWrapper;
OverviewWrapper.displayName = 'OverviewWrapper';