"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Loader = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n    padding: ", ";\n\n    ", "\n  "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Aside = _styledComponents.default.aside(_templateObject(), function (props) {
  return (0, _styledComponents.css)(_templateObject2(), props.theme.eui.paddingSizes.m, props.overlay && "\n      background: ".concat(props.overlayBackground ? props.overlayBackground : props.theme.eui.euiColorEmptyShade, ";\n      bottom: 0;\n      left: 0;\n      opacity: 0.9; // Michael - Using opacity instead of rgba because styled components don't support hex colors in rgba\n      position: absolute;\n      right: 0;\n      top: 0;\n      z-index: 3;\n    "));
});

Aside.displayName = 'Aside';
var FlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup).attrs({
  alignItems: 'center',
  direction: 'column',
  gutterSize: 's',
  justifyContent: 'center'
})(_templateObject3(), function (_ref) {
  var overlay = _ref.overlay;
  return overlay && "\n    height: 100%;\n  ";
});
FlexGroup.displayName = 'FlexGroup';
var Loader = (0, _recompose.pure)(function (_ref2) {
  var children = _ref2.children,
      overlay = _ref2.overlay,
      overlayBackground = _ref2.overlayBackground,
      size = _ref2.size;
  return _react.default.createElement(Aside, {
    overlay: overlay,
    overlayBackground: overlayBackground
  }, _react.default.createElement(FlexGroup, {
    overlay: {
      overlay: overlay
    }
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: size
  })), children && _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _react.default.createElement("p", null, children)))));
});
exports.Loader = Loader;
Loader.displayName = 'Loader';