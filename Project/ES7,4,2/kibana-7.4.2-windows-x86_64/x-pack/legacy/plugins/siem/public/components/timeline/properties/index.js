"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Properties = exports.showDescriptionThreshold = exports.showNotesThreshold = exports.datePickerThreshold = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _styles = require("./styles");

var _properties_right = require("./properties_right");

var _properties_left = require("./properties_left");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  display: none;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 4px;\n  cursor: pointer;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 15px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiPopover__panel.euiPopover__panel-isOpen {\n    z-index: 9900 !important;\n  }\n  .euiToolTip {\n    z-index: 9950 !important;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// SIDE EFFECT: the following `injectGlobal` overrides `EuiPopover`
// and `EuiToolTip` global styles:
// eslint-disable-next-line no-unused-expressions
(0, _styledComponents.injectGlobal)(_templateObject());
var Avatar = (0, _styledComponents.default)(_eui.EuiAvatar)(_templateObject2());
Avatar.displayName = 'Avatar';

var DescriptionPopoverMenuContainer = _styledComponents.default.div(_templateObject3());

DescriptionPopoverMenuContainer.displayName = 'DescriptionPopoverMenuContainer';
var SettingsIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject4());
SettingsIcon.displayName = 'SettingsIcon';
var HiddenFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject5());
HiddenFlexItem.displayName = 'HiddenFlexItem';
var rightGutter = 60; // px

var datePickerThreshold = 600;
exports.datePickerThreshold = datePickerThreshold;
var showNotesThreshold = 810;
exports.showNotesThreshold = showNotesThreshold;
var showDescriptionThreshold = 970;
exports.showDescriptionThreshold = showDescriptionThreshold;
var starIconWidth = 30;
var nameWidth = 155;
var descriptionWidth = 165;
var noteWidth = 130;
var settingsWidth = 50;
/** Displays the properties of a timeline, i.e. name, description, notes, etc */

var Properties =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Properties, _React$PureComponent);

  function Properties(props) {
    var _this;

    _classCallCheck(this, Properties);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Properties).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onButtonClick", function () {
      _this.setState(function (prevState) {
        return {
          showActions: !prevState.showActions
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleShowNotes", function () {
      _this.setState(function (state) {
        return {
          showNotes: !state.showNotes
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onClosePopover", function () {
      _this.setState({
        showActions: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggleLock", function () {
      _this.props.toggleLock({
        linkToId: 'timeline'
      });
    });

    _this.state = {
      showActions: false,
      showNotes: false
    };
    return _this;
  }

  _createClass(Properties, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          associateNote = _this$props.associateNote,
          createTimeline = _this$props.createTimeline,
          description = _this$props.description,
          getNotesByIds = _this$props.getNotesByIds,
          isFavorite = _this$props.isFavorite,
          isDataInTimeline = _this$props.isDataInTimeline,
          isDatepickerLocked = _this$props.isDatepickerLocked,
          title = _this$props.title,
          noteIds = _this$props.noteIds,
          timelineId = _this$props.timelineId,
          updateDescription = _this$props.updateDescription,
          updateIsFavorite = _this$props.updateIsFavorite,
          updateTitle = _this$props.updateTitle,
          updateNote = _this$props.updateNote,
          usersViewing = _this$props.usersViewing,
          width = _this$props.width;
      var datePickerWidth = width - rightGutter - starIconWidth - nameWidth - (width >= showDescriptionThreshold ? descriptionWidth : 0) - noteWidth - settingsWidth; // Passing the styles directly to the component because the width is
      // being calculated and is recommended by Styled Components for performance
      // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291

      return React.createElement(_styles.TimelineProperties, {
        style: {
          width: width
        },
        "data-test-subj": "timeline-properties"
      }, React.createElement(_properties_left.PropertiesLeft, {
        isFavorite: isFavorite,
        timelineId: timelineId,
        updateIsFavorite: updateIsFavorite,
        showDescription: width >= showDescriptionThreshold,
        description: description,
        title: title,
        updateTitle: updateTitle,
        updateDescription: updateDescription,
        showNotes: this.state.showNotes,
        showNotesFromWidth: width >= showNotesThreshold,
        associateNote: associateNote,
        getNotesByIds: getNotesByIds,
        noteIds: noteIds,
        onToggleShowNotes: this.onToggleShowNotes,
        updateNote: updateNote,
        isDatepickerLocked: isDatepickerLocked,
        toggleLock: this.toggleLock,
        datePickerWidth: datePickerWidth > datePickerThreshold ? datePickerThreshold : datePickerWidth
      }), React.createElement(_properties_right.PropertiesRight, {
        onButtonClick: this.onButtonClick,
        onClosePopover: this.onClosePopover,
        showActions: this.state.showActions,
        createTimeline: createTimeline,
        timelineId: timelineId,
        isDataInTimeline: isDataInTimeline,
        showNotesFromWidth: width < showNotesThreshold,
        showNotes: this.state.showNotes,
        showDescription: width < showDescriptionThreshold,
        showUsersView: title.length > 0,
        usersViewing: usersViewing,
        description: description,
        updateDescription: updateDescription,
        associateNote: associateNote,
        getNotesByIds: getNotesByIds,
        noteIds: noteIds,
        onToggleShowNotes: this.onToggleShowNotes,
        updateNote: updateNote
      }));
    }
  }]);

  return Properties;
}(React.PureComponent);

exports.Properties = Properties;