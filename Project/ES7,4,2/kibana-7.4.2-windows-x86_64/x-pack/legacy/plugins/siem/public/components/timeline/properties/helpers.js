"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotesButton = exports.NewTimeline = exports.Name = exports.Description = exports.StarIcon = exports.newTimelineToolTip = exports.streamLiveToolTip = exports.historyToolTip = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _uuid = _interopRequireDefault(require("uuid"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _notes = require("../../notes");

var _styles = require("./styles");

var i18n = _interopRequireWildcard(require("./translations"));

var _notes_size = require("./notes_size");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  svg {\n    height: 19px;\n    width: 19px;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var historyToolTip = 'The chronological history of actions related to this timeline';
exports.historyToolTip = historyToolTip;
var streamLiveToolTip = 'Update the Timeline as new data arrives';
exports.streamLiveToolTip = streamLiveToolTip;
var newTimelineToolTip = 'Create a new timeline'; // Ref: https://github.com/elastic/eui/issues/1655
// const NotesCountBadge = styled(EuiBadge)`
//   margin-left: 5px;
// `;

exports.newTimelineToolTip = newTimelineToolTip;

var NotesCountBadge = function NotesCountBadge(props) {
  return React.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      marginLeft: '5px'
    }
  }));
};

NotesCountBadge.displayName = 'NotesCountBadge';
var StarIcon = (0, _recompose.pure)(function (_ref) {
  var isFavorite = _ref.isFavorite,
      id = _ref.timelineId,
      updateIsFavorite = _ref.updateIsFavorite;
  return (// TODO: 1 error is: Visible, non-interactive elements with click handlers must have at least one keyboard listener
    // TODO: 2 error is: Elements with the 'button' interactive role must be focusable
    // TODO: Investigate this error
    // eslint-disable-next-line
    React.createElement("div", {
      role: "button",
      onClick: function onClick() {
        return updateIsFavorite({
          id: id,
          isFavorite: !isFavorite
        });
      }
    }, isFavorite ? React.createElement(_eui.EuiToolTip, {
      "data-test-subj": "timeline-favorite-filled-star-tool-tip",
      content: i18n.FAVORITE
    }, React.createElement(_styles.StyledStar, {
      "data-test-subj": "timeline-favorite-filled-star",
      type: "starFilled",
      size: "l"
    })) : React.createElement(_eui.EuiToolTip, {
      content: i18n.NOT_A_FAVORITE
    }, React.createElement(_styles.StyledStar, {
      "data-test-subj": "timeline-favorite-empty-star",
      type: "starEmpty",
      size: "l"
    })))
  );
});
exports.StarIcon = StarIcon;
StarIcon.displayName = 'StarIcon';
var Description = (0, _recompose.pure)(function (_ref2) {
  var description = _ref2.description,
      timelineId = _ref2.timelineId,
      updateDescription = _ref2.updateDescription;
  return React.createElement(_eui.EuiToolTip, {
    "data-test-subj": "timeline-description-tool-tip",
    content: i18n.DESCRIPTION_TOOL_TIP
  }, React.createElement(_styles.DescriptionContainer, {
    "data-test-subj": "description-container"
  }, React.createElement(_eui.EuiFieldText, {
    "aria-label": i18n.TIMELINE_DESCRIPTION,
    "data-test-subj": "timeline-description",
    fullWidth: true,
    onChange: function onChange(e) {
      return updateDescription({
        id: timelineId,
        description: e.target.value
      });
    },
    placeholder: i18n.DESCRIPTION,
    spellCheck: true,
    value: description
  })));
});
exports.Description = Description;
Description.displayName = 'Description';
var Name = (0, _recompose.pure)(function (_ref3) {
  var timelineId = _ref3.timelineId,
      title = _ref3.title,
      updateTitle = _ref3.updateTitle;
  return React.createElement(_eui.EuiToolTip, {
    "data-test-subj": "timeline-title-tool-tip",
    content: i18n.TITLE
  }, React.createElement(_styles.NameField, {
    "aria-label": i18n.TIMELINE_TITLE,
    "data-test-subj": "timeline-title",
    onChange: function onChange(e) {
      return updateTitle({
        id: timelineId,
        title: e.target.value
      });
    },
    placeholder: i18n.UNTITLED_TIMELINE,
    spellCheck: true,
    value: title
  }));
});
exports.Name = Name;
Name.displayName = 'Name';
var NewTimeline = (0, _recompose.pure)(function (_ref4) {
  var createTimeline = _ref4.createTimeline,
      onClosePopover = _ref4.onClosePopover,
      timelineId = _ref4.timelineId;
  return React.createElement(_eui.EuiButtonEmpty, {
    "data-test-subj": "timeline-new",
    color: "text",
    iconSide: "left",
    iconType: "plusInCircle",
    onClick: function onClick() {
      createTimeline({
        id: timelineId,
        show: true
      });
      onClosePopover();
    }
  }, i18n.NEW_TIMELINE);
});
exports.NewTimeline = NewTimeline;
NewTimeline.displayName = 'NewTimeline';

var getNewNoteId = function getNewNoteId() {
  return _uuid.default.v4();
};

var NotesButtonIcon = (0, _styledComponents.default)(_eui.EuiButtonIcon)(_templateObject());
var NotesIcon = (0, _recompose.pure)(function (_ref5) {
  var count = _ref5.count;
  return React.createElement(NotesButtonIcon, {
    "aria-label": i18n.NOTES,
    color: count > 0 ? 'primary' : 'subdued',
    "data-test-subj": "timeline-notes-icon",
    size: "l",
    iconType: "editorComment"
  });
});
NotesIcon.displayName = 'NotesIcon';
var LargeNotesButton = (0, _recompose.pure)(function (_ref6) {
  var noteIds = _ref6.noteIds,
      text = _ref6.text,
      toggleShowNotes = _ref6.toggleShowNotes;
  return React.createElement(_eui.EuiButton, {
    "data-test-subj": "timeline-notes-button-large",
    onClick: function onClick() {
      return toggleShowNotes();
    },
    size: "m"
  }, React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none",
    justifyContent: "center"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiIcon, {
    color: "subdued",
    size: "m",
    type: "editorComment"
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, text && text.length ? React.createElement(_styles.LabelText, null, text) : null), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(NotesCountBadge, {
    "data-test-subj": "timeline-notes-count",
    color: "hollow"
  }, noteIds.length))));
});
LargeNotesButton.displayName = 'LargeNotesButton';
var SmallNotesButton = (0, _recompose.pure)(function (_ref7) {
  var noteIds = _ref7.noteIds,
      toggleShowNotes = _ref7.toggleShowNotes;
  return React.createElement(_styles.SmallNotesButtonContainer, {
    "data-test-subj": "timeline-notes-button-small",
    onClick: function onClick() {
      return toggleShowNotes();
    },
    role: "button"
  }, React.createElement(NotesIcon, {
    count: noteIds.length
  }));
});
SmallNotesButton.displayName = 'SmallNotesButton';
/**
 * The internal implementation of the `NotesButton`
 */

var NotesButtonComponent = (0, _recompose.pure)(function (_ref8) {
  var _ref8$animate = _ref8.animate,
      animate = _ref8$animate === void 0 ? true : _ref8$animate,
      associateNote = _ref8.associateNote,
      getNotesByIds = _ref8.getNotesByIds,
      noteIds = _ref8.noteIds,
      showNotes = _ref8.showNotes,
      size = _ref8.size,
      toggleShowNotes = _ref8.toggleShowNotes,
      text = _ref8.text,
      updateNote = _ref8.updateNote;
  return React.createElement(_styles.ButtonContainer, {
    animate: animate,
    "data-test-subj": "timeline-notes-button-container"
  }, React.createElement(React.Fragment, null, size === 'l' ? React.createElement(LargeNotesButton, {
    noteIds: noteIds,
    text: text,
    toggleShowNotes: toggleShowNotes
  }) : React.createElement(SmallNotesButton, {
    noteIds: noteIds,
    toggleShowNotes: toggleShowNotes
  }), size === 'l' && showNotes ? React.createElement(_eui.EuiOverlayMask, null, React.createElement(_eui.EuiModal, {
    maxWidth: _notes_size.NOTES_PANEL_WIDTH,
    onClose: toggleShowNotes
  }, React.createElement(_notes.Notes, {
    associateNote: associateNote,
    getNotesByIds: getNotesByIds,
    noteIds: noteIds,
    getNewNoteId: getNewNoteId,
    updateNote: updateNote
  }))) : null));
});
NotesButtonComponent.displayName = 'NotesButtonComponent';
var NotesButton = (0, _recompose.pure)(function (_ref9) {
  var _ref9$animate = _ref9.animate,
      animate = _ref9$animate === void 0 ? true : _ref9$animate,
      associateNote = _ref9.associateNote,
      getNotesByIds = _ref9.getNotesByIds,
      noteIds = _ref9.noteIds,
      showNotes = _ref9.showNotes,
      size = _ref9.size,
      toggleShowNotes = _ref9.toggleShowNotes,
      toolTip = _ref9.toolTip,
      text = _ref9.text,
      updateNote = _ref9.updateNote;
  return showNotes ? React.createElement(NotesButtonComponent, {
    animate: animate,
    associateNote: associateNote,
    getNotesByIds: getNotesByIds,
    noteIds: noteIds,
    showNotes: showNotes,
    size: size,
    toggleShowNotes: toggleShowNotes,
    text: text,
    updateNote: updateNote
  }) : React.createElement(_eui.EuiToolTip, {
    content: toolTip || '',
    "data-test-subj": "timeline-notes-tool-tip"
  }, React.createElement(NotesButtonComponent, {
    animate: animate,
    associateNote: associateNote,
    getNotesByIds: getNotesByIds,
    noteIds: noteIds,
    showNotes: showNotes,
    size: size,
    toggleShowNotes: toggleShowNotes,
    text: text,
    updateNote: updateNote
  }));
});
exports.NotesButton = NotesButton;
NotesButton.displayName = 'NotesButton';