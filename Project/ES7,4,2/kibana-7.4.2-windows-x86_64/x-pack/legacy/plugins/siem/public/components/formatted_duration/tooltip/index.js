"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedDurationTooltip = exports.FormattedDurationTooltipContent = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _react2 = require("@kbn/i18n/react");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var P = _styledComponents.default.p(_templateObject());

P.displayName = 'P';
var FormattedDurationTooltipContent = (0, _recompose.pure)(function (_ref) {
  var maybeDurationNanoseconds = _ref.maybeDurationNanoseconds,
      tooltipTitle = _ref.tooltipTitle;
  return React.createElement(React.Fragment, null, tooltipTitle != null ? React.createElement(P, {
    "data-test-subj": "title"
  }, tooltipTitle) : null, React.createElement(P, {
    "data-test-subj": "humanized"
  }, (0, _helpers.getHumanizedDuration)(maybeDurationNanoseconds)), React.createElement(P, {
    "data-test-subj": "raw-value"
  }, React.createElement(_react2.FormattedMessage, {
    id: "xpack.siem.formattedDuration.tooltipLabel",
    defaultMessage: "raw"
  }), ': ', maybeDurationNanoseconds));
});
exports.FormattedDurationTooltipContent = FormattedDurationTooltipContent;
FormattedDurationTooltipContent.displayName = 'FormattedDurationTooltipContent';
var FormattedDurationTooltip = (0, _recompose.pure)(function (_ref2) {
  var children = _ref2.children,
      maybeDurationNanoseconds = _ref2.maybeDurationNanoseconds,
      tooltipTitle = _ref2.tooltipTitle;
  return React.createElement(_eui.EuiToolTip, {
    "data-test-subj": "formatted-duration-tooltip",
    content: React.createElement(FormattedDurationTooltipContent, {
      maybeDurationNanoseconds: maybeDurationNanoseconds,
      tooltipTitle: tooltipTitle
    })
  }, React.createElement(React.Fragment, null, children));
});
exports.FormattedDurationTooltip = FormattedDurationTooltip;
FormattedDurationTooltip.displayName = 'FormattedDurationTooltip';