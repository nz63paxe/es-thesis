"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LazyAccordion = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * An accordion that doesn't render it's content unless it's expanded.
 * This component was created because `EuiAccordion`'s eager rendering of
 * accordion content was creating performance issues when used in repeating
 * content on the page.
 *
 * The current implementation actually renders the content *outside* of the
 * actual EuiAccordion when the accordion is expanded! It does this because
 * EuiAccordian applies a `translate` style to the content that causes
 * any draggable content (inside `EuiAccordian`) to have a `translate` style
 * that messes up rendering while the user drags it.
 *
 * TODO: animate the expansion and collapse of content rendered "below"
 * the real `EuiAccordion`.
 */
var LazyAccordion =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(LazyAccordion, _React$PureComponent);

  function LazyAccordion(props) {
    var _this;

    _classCallCheck(this, LazyAccordion);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LazyAccordion).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onCollapsedClick", function () {
      var onExpand = _this.props.onExpand;

      _this.setState({
        expanded: true
      });

      if (onExpand != null) {
        onExpand();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onExpandedClick", function () {
      var onCollapse = _this.props.onCollapse;

      _this.setState({
        expanded: false
      });

      if (onCollapse != null) {
        onCollapse();
      }
    });

    _this.state = {
      expanded: false
    };
    return _this;
  }

  _createClass(LazyAccordion, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          id = _this$props.id,
          buttonContentClassName = _this$props.buttonContentClassName,
          buttonContent = _this$props.buttonContent,
          forceExpand = _this$props.forceExpand,
          extraAction = _this$props.extraAction,
          renderExpandedContent = _this$props.renderExpandedContent,
          paddingSize = _this$props.paddingSize;
      return React.createElement(React.Fragment, null, forceExpand || this.state.expanded ? React.createElement(React.Fragment, null, React.createElement(_eui.EuiAccordion, {
        buttonContent: buttonContent,
        buttonContentClassName: buttonContentClassName,
        "data-test-subj": "lazy-accordion-expanded",
        extraAction: extraAction,
        id: id,
        initialIsOpen: true,
        onClick: this.onExpandedClick,
        paddingSize: paddingSize
      }, React.createElement(React.Fragment, null)), renderExpandedContent(this.state.expanded)) : React.createElement(_eui.EuiAccordion, {
        buttonContent: buttonContent,
        buttonContentClassName: buttonContentClassName,
        "data-test-subj": "lazy-accordion-placeholder",
        extraAction: extraAction,
        id: id,
        onClick: this.onCollapsedClick,
        paddingSize: paddingSize
      }));
    }
  }]);

  return LazyAccordion;
}(React.PureComponent);

exports.LazyAccordion = LazyAccordion;