"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Network = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _direction = require("../direction");

var _draggables = require("../draggables");

var i18n = _interopRequireWildcard(require("./translations"));

var _field_names = require("./field_names");

var _formatted_bytes = require("../formatted_bytes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 3px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EuiFlexItemMarginRight = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
EuiFlexItemMarginRight.displayName = 'EuiFlexItemMarginRight';
var Stats = (0, _styledComponents.default)(_eui.EuiText)(_templateObject2());
Stats.displayName = 'Stats';
/**
 * Renders a row of draggable badges containing fields from the
 * `Network` category of fields
 */

var Network = (0, _recompose.pure)(function (_ref) {
  var bytes = _ref.bytes,
      communityId = _ref.communityId,
      contextId = _ref.contextId,
      direction = _ref.direction,
      eventId = _ref.eventId,
      packets = _ref.packets,
      protocol = _ref.protocol,
      transport = _ref.transport;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "center",
    gutterSize: "none"
  }, direction != null ? (0, _fp.uniq)(direction).map(function (dir) {
    return React.createElement(EuiFlexItemMarginRight, {
      grow: false,
      key: dir
    }, React.createElement(_direction.DirectionBadge, {
      contextId: contextId,
      eventId: eventId,
      direction: dir
    }));
  }) : null, protocol != null ? (0, _fp.uniq)(protocol).map(function (proto) {
    return React.createElement(EuiFlexItemMarginRight, {
      grow: false,
      key: proto
    }, React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      "data-test-subj": "network-protocol",
      eventId: eventId,
      field: _field_names.NETWORK_PROTOCOL_FIELD_NAME,
      value: proto
    }));
  }) : null, bytes != null ? (0, _fp.uniq)(bytes).map(function (b) {
    return !isNaN(Number(b)) ? React.createElement(EuiFlexItemMarginRight, {
      grow: false,
      key: b
    }, React.createElement(_draggables.DefaultDraggable, {
      field: _field_names.NETWORK_BYTES_FIELD_NAME,
      id: "network-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(_field_names.NETWORK_BYTES_FIELD_NAME, "-").concat(b),
      value: b
    }, React.createElement(Stats, {
      size: "xs"
    }, React.createElement("span", {
      "data-test-subj": "network-bytes"
    }, React.createElement(_formatted_bytes.PreferenceFormattedBytes, {
      value: b
    }))))) : null;
  }) : null, packets != null ? (0, _fp.uniq)(packets).map(function (p) {
    return React.createElement(EuiFlexItemMarginRight, {
      grow: false,
      key: p
    }, React.createElement(_draggables.DefaultDraggable, {
      field: _field_names.NETWORK_PACKETS_FIELD_NAME,
      id: "network-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(_field_names.NETWORK_PACKETS_FIELD_NAME, "-").concat(p),
      value: p
    }, React.createElement(Stats, {
      size: "xs"
    }, React.createElement("span", {
      "data-test-subj": "network-packets"
    }, "".concat(p, " ").concat(i18n.PACKETS)))));
  }) : null, transport != null ? (0, _fp.uniq)(transport).map(function (trans) {
    return React.createElement(EuiFlexItemMarginRight, {
      grow: false,
      key: trans
    }, React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      "data-test-subj": "network-transport",
      eventId: eventId,
      field: _field_names.NETWORK_TRANSPORT_FIELD_NAME,
      value: trans
    }));
  }) : null, communityId != null ? (0, _fp.uniq)(communityId).map(function (trans) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: trans
    }, React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      "data-test-subj": "network-community-id",
      eventId: eventId,
      field: _field_names.NETWORK_COMMUNITY_ID_FIELD_NAME,
      value: trans
    }));
  }) : null);
});
exports.Network = Network;
Network.displayName = 'Network';