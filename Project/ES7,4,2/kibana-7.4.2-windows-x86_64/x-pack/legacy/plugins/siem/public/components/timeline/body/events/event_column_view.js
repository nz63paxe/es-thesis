"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventColumnView = exports.getNewNoteId = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _uuid = _interopRequireDefault(require("uuid"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _actions = require("../actions");

var _data_driven_columns = require("../data_driven_columns");

var _helpers = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  border-top: 1px solid ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var getNewNoteId = function getNewNoteId() {
  return _uuid.default.v4();
};

exports.getNewNoteId = getNewNoteId;
var emptyNotes = [];
var EventColumnViewFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject(), function (_ref) {
  var theme = _ref.theme;
  return theme.eui.euiColorLightShade;
});
var EventColumnView = React.memo(function (_ref2) {
  var id = _ref2.id,
      actionsColumnWidth = _ref2.actionsColumnWidth,
      associateNote = _ref2.associateNote,
      columnHeaders = _ref2.columnHeaders,
      columnRenderers = _ref2.columnRenderers,
      data = _ref2.data,
      eventIdToNoteIds = _ref2.eventIdToNoteIds,
      expanded = _ref2.expanded,
      getNotesByIds = _ref2.getNotesByIds,
      _ref2$isEventViewer = _ref2.isEventViewer,
      isEventViewer = _ref2$isEventViewer === void 0 ? false : _ref2$isEventViewer,
      loading = _ref2.loading,
      onColumnResized = _ref2.onColumnResized,
      onEventToggled = _ref2.onEventToggled,
      onPinEvent = _ref2.onPinEvent,
      onUnPinEvent = _ref2.onUnPinEvent,
      pinnedEventIds = _ref2.pinnedEventIds,
      showNotes = _ref2.showNotes,
      timelineId = _ref2.timelineId,
      toggleShowNotes = _ref2.toggleShowNotes,
      updateNote = _ref2.updateNote;
  return React.createElement(EventColumnViewFlexGroup, {
    alignItems: "center",
    "data-test-subj": "event-column-view",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "actions-column-item",
    grow: false
  }, React.createElement(_actions.Actions, {
    actionsColumnWidth: actionsColumnWidth,
    associateNote: associateNote,
    checked: false,
    expanded: expanded,
    "data-test-subj": "actions",
    eventId: id,
    eventIsPinned: (0, _helpers.eventIsPinned)({
      eventId: id,
      pinnedEventIds: pinnedEventIds
    }),
    getNotesByIds: getNotesByIds,
    isEventViewer: isEventViewer,
    loading: loading,
    noteIds: eventIdToNoteIds[id] || emptyNotes,
    onEventToggled: onEventToggled,
    onPinClicked: (0, _helpers.getPinOnClick)({
      allowUnpinning: !(0, _helpers.eventHasNotes)(eventIdToNoteIds[id]),
      eventId: id,
      onPinEvent: onPinEvent,
      onUnPinEvent: onUnPinEvent,
      pinnedEventIds: pinnedEventIds
    }),
    showCheckboxes: false,
    showNotes: showNotes,
    toggleShowNotes: toggleShowNotes,
    updateNote: updateNote
  })), React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "event-columns-item",
    grow: false
  }, React.createElement(_data_driven_columns.DataDrivenColumns, {
    _id: id,
    columnHeaders: columnHeaders,
    columnRenderers: columnRenderers,
    data: data,
    onColumnResized: onColumnResized,
    timelineId: timelineId
  })));
}, function (prevProps, nextProps) {
  return prevProps.id === nextProps.id && prevProps.actionsColumnWidth === nextProps.actionsColumnWidth && prevProps.columnHeaders === nextProps.columnHeaders && prevProps.columnRenderers === nextProps.columnRenderers && prevProps.data === nextProps.data && prevProps.eventIdToNoteIds === nextProps.eventIdToNoteIds && prevProps.expanded === nextProps.expanded && prevProps.loading === nextProps.loading && prevProps.pinnedEventIds === nextProps.pinnedEventIds && prevProps.showNotes === nextProps.showNotes && prevProps.timelineId === nextProps.timelineId;
});
exports.EventColumnView = EventColumnView;
EventColumnView.displayName = 'EventColumnView';