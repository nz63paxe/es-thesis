"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DraggableWrapper = exports.DragEffects = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _reactRedux = require("react-redux");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _drag_and_drop = require("../../store/drag_and_drop");

var _helpers = require("../timeline/helpers");

var _truncatable_text = require("../truncatable_text");

var _helpers2 = require("./helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n    // ALL DRAGGABLES\n\n    &,\n    &::before,\n    &::after {\n      transition: background ", " ease,\n        color ", " ease;\n    }\n\n    .euiBadge,\n    .euiBadge__text {\n      cursor: grab;\n    }\n\n    // PAGE DRAGGABLES\n\n    ", "\n\n    ", "\n  "]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  .euiPageBody & {\n    display: inline-block;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

// As right now, we do not know what we want there, we will keep it as a placeholder
var DragEffects = _styledComponents.default.div(_templateObject());

exports.DragEffects = DragEffects;
DragEffects.displayName = 'DragEffects';

var Wrapper = _styledComponents.default.div(_templateObject2());

Wrapper.displayName = 'Wrapper';

var ProviderContainer = _styledComponents.default.div(_templateObject3(), function (_ref) {
  var theme = _ref.theme,
      isDragging = _ref.isDragging;
  return (0, _styledComponents.css)(_templateObject4(), theme.eui.euiAnimSpeedFast, theme.eui.euiAnimSpeedFast, !isDragging && "\n      .euiPageBody & {\n        z-index: ".concat(theme.eui.euiZLevel0, " !important;\n      }\n\n      {\n        border-radius: 2px;\n        padding: 0 4px 0 8px;\n        position: relative;\n\n        &::before {\n          background-image: linear-gradient(\n              135deg,\n              ").concat(theme.eui.euiColorMediumShade, " 25%,\n              transparent 25%\n            ),\n            linear-gradient(-135deg, ").concat(theme.eui.euiColorMediumShade, " 25%, transparent 25%),\n            linear-gradient(135deg, transparent 75%, ").concat(theme.eui.euiColorMediumShade, " 75%),\n            linear-gradient(-135deg, transparent 75%, ").concat(theme.eui.euiColorMediumShade, " 75%);\n          background-position: 0 0, 1px 0, 1px -1px, 0px 1px;\n          background-size: 2px 2px;\n          bottom: 2px;\n          content: '';\n          display: block;\n          left: 2px;\n          position: absolute;\n          top: 2px;\n          width: 4px;\n        }\n      }\n\n\n      .").concat(_helpers.STATEFUL_EVENT_CSS_CLASS_NAME, ":hover &,\n      tr:hover & {\n        background-color: ").concat(theme.eui.euiColorLightShade, ";\n\n        &::before {\n          background-image: linear-gradient(\n              135deg,\n              ").concat(theme.eui.euiColorDarkShade, " 25%,\n              transparent 25%\n            ),\n            linear-gradient(-135deg, ").concat(theme.eui.euiColorDarkShade, " 25%, transparent 25%),\n            linear-gradient(135deg, transparent 75%, ").concat(theme.eui.euiColorDarkShade, " 75%),\n            linear-gradient(-135deg, transparent 75%, ").concat(theme.eui.euiColorDarkShade, " 75%);\n        }\n      }\n\n      &:hover,\n      &:focus,\n      .").concat(_helpers.STATEFUL_EVENT_CSS_CLASS_NAME, ":hover &:hover,\n      .").concat(_helpers.STATEFUL_EVENT_CSS_CLASS_NAME, ":focus &:focus,\n      tr:hover &:hover,\n      tr:hover &:focus {\n        background-color: ").concat(theme.eui.euiColorPrimary, ";\n\n        &,\n        & a {\n          color: ").concat(theme.eui.euiColorEmptyShade, ";\n        }\n\n        &::before {\n          background-image: linear-gradient(\n              135deg,\n              ").concat(theme.eui.euiColorEmptyShade, " 25%,\n              transparent 25%\n            ),\n            linear-gradient(-135deg, ").concat(theme.eui.euiColorEmptyShade, " 25%, transparent 25%),\n            linear-gradient(135deg, transparent 75%, ").concat(theme.eui.euiColorEmptyShade, " 75%),\n            linear-gradient(-135deg, transparent 75%, ").concat(theme.eui.euiColorEmptyShade, " 75%);\n        }\n      }\n    "), isDragging && "\n      .euiPageBody & {\n        z-index: ".concat(theme.eui.euiZLevel9, " !important;\n      "));
});

ProviderContainer.displayName = 'ProviderContainer';

/**
 * Wraps a draggable component to handle registration / unregistration of the
 * data provider associated with the item being dropped
 */
var DraggableWrapperComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DraggableWrapperComponent, _React$Component);

  function DraggableWrapperComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DraggableWrapperComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DraggableWrapperComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "shouldComponentUpdate", function (_ref2) {
      var dataProvider = _ref2.dataProvider,
          render = _ref2.render,
          width = _ref2.width;
      return (0, _fp.isEqual)(dataProvider, _this.props.dataProvider) && render !== _this.props.render && width === _this.props.width ? false : true;
    });

    return _this;
  }

  _createClass(DraggableWrapperComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props = this.props,
          dataProvider = _this$props.dataProvider,
          registerProvider = _this$props.registerProvider;
      registerProvider({
        provider: dataProvider
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      var _this$props2 = this.props,
          dataProvider = _this$props2.dataProvider,
          unRegisterProvider = _this$props2.unRegisterProvider;
      unRegisterProvider({
        id: dataProvider.id
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          dataProvider = _this$props3.dataProvider,
          render = _this$props3.render,
          width = _this$props3.width;
      return React.createElement(Wrapper, {
        "data-test-subj": "draggableWrapperDiv"
      }, React.createElement(_reactBeautifulDnd.Droppable, {
        isDropDisabled: true,
        droppableId: (0, _helpers2.getDroppableId)(dataProvider.id)
      }, function (droppableProvided) {
        return React.createElement("div", _extends({
          ref: droppableProvided.innerRef
        }, droppableProvided.droppableProps), React.createElement(_reactBeautifulDnd.Draggable, {
          draggableId: (0, _helpers2.getDraggableId)(dataProvider.id),
          index: 0,
          key: (0, _helpers2.getDraggableId)(dataProvider.id)
        }, function (provided, snapshot) {
          return React.createElement(ProviderContainer, _extends({}, provided.draggableProps, provided.dragHandleProps, {
            innerRef: provided.innerRef,
            "data-test-subj": "providerContainer",
            isDragging: snapshot.isDragging,
            style: _objectSpread({}, provided.draggableProps.style)
          }), width != null && !snapshot.isDragging ? React.createElement(_truncatable_text.TruncatableText, {
            "data-test-subj": "draggable-truncatable-content",
            size: "xs",
            width: width
          }, render(dataProvider, provided, snapshot)) : React.createElement("span", {
            "data-test-subj": "draggable-content-".concat(dataProvider.queryMatch.field)
          }, render(dataProvider, provided, snapshot)));
        }), droppableProvided.placeholder);
      }));
    }
  }]);

  return DraggableWrapperComponent;
}(React.Component);

var DraggableWrapper = (0, _reactRedux.connect)(null, {
  registerProvider: _drag_and_drop.dragAndDropActions.registerProvider,
  unRegisterProvider: _drag_and_drop.dragAndDropActions.unRegisterProvider
})(DraggableWrapperComponent);
exports.DraggableWrapper = DraggableWrapper;