"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NewNote = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _markdown = require("../../markdown");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  min-height: ", ";\n  width: 100%;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: ", "px;\n  overflow: auto;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NewNoteTabs = (0, _styledComponents.default)(_eui.EuiTabbedContent)(_templateObject());
NewNoteTabs.displayName = 'NewNoteTabs';
var MarkdownContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject2(), function (_ref) {
  var height = _ref.height;
  return height;
});
MarkdownContainer.displayName = 'MarkdownContainer';
var TextArea = (0, _styledComponents.default)(_eui.EuiTextArea)(_templateObject3(), function (_ref2) {
  var height = _ref2.height;
  return "".concat(height, "px");
});
TextArea.displayName = 'TextArea';
TextArea.displayName = 'TextArea';
/** An input for entering a new note  */

var NewNote = (0, _recompose.pure)(function (_ref3) {
  var note = _ref3.note,
      noteInputHeight = _ref3.noteInputHeight,
      updateNewNote = _ref3.updateNewNote;
  var tabs = [{
    id: 'note',
    name: i18n.NOTE,
    content: React.createElement(TextArea, {
      autoFocus: true,
      "aria-label": i18n.NOTE,
      "data-test-subj": "add-a-note",
      fullWidth: true,
      height: noteInputHeight,
      onChange: function onChange(e) {
        return updateNewNote(e.target.value);
      },
      placeholder: i18n.ADD_A_NOTE,
      spellCheck: true,
      value: note
    })
  }, {
    id: 'preview',
    name: i18n.PREVIEW_MARKDOWN,
    content: React.createElement(MarkdownContainer, {
      "data-test-subj": "markdown-container",
      height: noteInputHeight,
      paddingSize: "s"
    }, React.createElement(_markdown.Markdown, {
      raw: note
    }))
  }];
  return React.createElement(NewNoteTabs, {
    "data-test-subj": "new-note-tabs",
    tabs: tabs,
    initialSelectedTab: tabs[0]
  });
});
exports.NewNote = NewNote;
NewNote.displayName = 'NewNote';