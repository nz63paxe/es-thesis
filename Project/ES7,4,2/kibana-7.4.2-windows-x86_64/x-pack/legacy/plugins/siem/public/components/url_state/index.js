"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UseUrlState = exports.UrlStateRedux = exports.UrlStateContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _redux = require("redux");

var _reactRedux = require("react-redux");

var _fp = require("lodash/fp");

var _store = require("../../store");

var _actions = require("../../store/actions");

var _use_route_spy = require("../../utils/route/use_route_spy");

var _constants = require("./constants");

var _use_url_state = require("./use_url_state");

var _helpers = require("../open_timeline/helpers");

var _helpers2 = require("./helpers");

var _initialize_redux_by_url = require("./initialize_redux_by_url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var UrlStateContainer = _react.default.memo(function (props) {
  (0, _use_url_state.useUrlStateHooks)(props);
  return null;
}, function (prevProps, nextProps) {
  return prevProps.pathName === nextProps.pathName && (0, _fp.isEqual)(prevProps.urlState, nextProps.urlState);
});

exports.UrlStateContainer = UrlStateContainer;
UrlStateContainer.displayName = 'UrlStateContainer';

var makeMapStateToProps = function makeMapStateToProps() {
  var getInputsSelector = _store.inputsSelectors.inputsSelector();

  var getHostsFilterQueryAsKuery = _store.hostsSelectors.hostsFilterQueryAsKuery();

  var getNetworkFilterQueryAsKuery = _store.networkSelectors.networkFilterQueryAsKuery();

  var getTimelines = _store.timelineSelectors.getTimelines();

  var mapStateToProps = function mapStateToProps(state, _ref) {
    var _global, _timeline, _urlState;

    var pageName = _ref.pageName,
        detailName = _ref.detailName;
    var inputState = getInputsSelector(state);
    var _inputState$global = inputState.global,
        globalLinkTo = _inputState$global.linkTo,
        globalTimerange = _inputState$global.timerange;
    var _inputState$timeline = inputState.timeline,
        timelineLinkTo = _inputState$timeline.linkTo,
        timelineTimerange = _inputState$timeline.timerange;
    var page = (0, _helpers2.getCurrentLocation)(pageName, detailName);
    var kqlQueryInitialState = {
      filterQuery: null,
      queryLocation: page
    };

    if (page === _constants.CONSTANTS.hostsPage) {
      kqlQueryInitialState.filterQuery = getHostsFilterQueryAsKuery(state, _store.hostsModel.HostsType.page);
    } else if (page === _constants.CONSTANTS.hostsDetails) {
      kqlQueryInitialState.filterQuery = getHostsFilterQueryAsKuery(state, _store.hostsModel.HostsType.details);
    } else if (page === _constants.CONSTANTS.networkPage) {
      kqlQueryInitialState.filterQuery = getNetworkFilterQueryAsKuery(state, _store.networkModel.NetworkType.page);
    } else if (page === _constants.CONSTANTS.networkDetails) {
      kqlQueryInitialState.filterQuery = getNetworkFilterQueryAsKuery(state, _store.networkModel.NetworkType.details);
    }

    var openTimelineId = Object.entries(getTimelines(state)).reduce(function (useTimelineId, _ref2) {
      var _ref3 = _slicedToArray(_ref2, 2),
          timelineId = _ref3[0],
          timelineObj = _ref3[1];

      if (timelineObj.savedObjectId != null) {
        useTimelineId = timelineObj.savedObjectId;
      }

      return useTimelineId;
    }, '');
    return {
      urlState: (_urlState = {}, _defineProperty(_urlState, _constants.CONSTANTS.timerange, {
        global: (_global = {}, _defineProperty(_global, _constants.CONSTANTS.timerange, globalTimerange), _defineProperty(_global, "linkTo", globalLinkTo), _global),
        timeline: (_timeline = {}, _defineProperty(_timeline, _constants.CONSTANTS.timerange, timelineTimerange), _defineProperty(_timeline, "linkTo", timelineLinkTo), _timeline)
      }), _defineProperty(_urlState, _constants.CONSTANTS.kqlQuery, kqlQueryInitialState), _defineProperty(_urlState, _constants.CONSTANTS.timelineId, openTimelineId), _urlState)
    };
  };

  return mapStateToProps;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    setInitialStateFromUrl: (0, _initialize_redux_by_url.dispatchSetInitialStateFromUrl)(dispatch),
    updateTimeline: (0, _helpers.dispatchUpdateTimeline)(dispatch),
    updateTimelineIsLoading: function updateTimelineIsLoading(_ref4) {
      var id = _ref4.id,
          isLoading = _ref4.isLoading;
      return dispatch(_actions.timelineActions.updateIsLoading({
        id: id,
        isLoading: isLoading
      }));
    }
  };
};

var UrlStateRedux = (0, _redux.compose)((0, _reactRedux.connect)(makeMapStateToProps, mapDispatchToProps))(UrlStateContainer);
exports.UrlStateRedux = UrlStateRedux;

var UseUrlState = _react.default.memo(function (props) {
  var _useRouteSpy = (0, _use_route_spy.useRouteSpy)(),
      _useRouteSpy2 = _slicedToArray(_useRouteSpy, 1),
      routeProps = _useRouteSpy2[0];

  var urlStateReduxProps = _objectSpread({}, routeProps, {}, props);

  return _react.default.createElement(UrlStateRedux, urlStateReduxProps);
});

exports.UseUrlState = UseUrlState;