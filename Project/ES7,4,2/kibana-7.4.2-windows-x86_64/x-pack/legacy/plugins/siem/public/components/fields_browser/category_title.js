"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CategoryTitle = void 0;

var _eui = require("@elastic/eui");

var _recompose = require("recompose");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("./helpers");

var _page = require("../page");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: -3px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CountBadgeContainer = _styledComponents.default.div(_templateObject());

CountBadgeContainer.displayName = 'CountBadgeContainer';
var CategoryTitle = (0, _recompose.pure)(function (_ref) {
  var filteredBrowserFields = _ref.filteredBrowserFields,
      categoryId = _ref.categoryId,
      timelineId = _ref.timelineId;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    "data-test-subj": "category-title-container",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiTitle, {
    className: (0, _helpers.getFieldBrowserCategoryTitleClassName)({
      categoryId: categoryId,
      timelineId: timelineId
    }),
    "data-test-subj": "selected-category-title",
    size: "xxs"
  }, React.createElement("h5", null, categoryId))), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(CountBadgeContainer, null, React.createElement(_page.CountBadge, {
    "data-test-subj": "selected-category-count-badge",
    color: "hollow"
  }, (0, _helpers.getFieldCount)(filteredBrowserFields[categoryId])))));
});
exports.CategoryTitle = CategoryTitle;
CategoryTitle.displayName = 'CategoryTitle';