"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getIndexPatternTitleIdMapping = exports.getStablePatternTitles = exports.getIndexPatternTitles = exports.searchFilter = exports.getJobsToDisplay = exports.getConfigTemplatesToInstall = exports.getJobsToInstall = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns all `jobIds` for each configTemplate provided
 *
 * @param templates ConfigTemplates as provided by ML Team (https://github.com/elastic/machine-learning-data/issues/194#issuecomment-505779406)
 */
var getJobsToInstall = function getJobsToInstall(templates) {
  return templates.reduce(function (jobs, template) {
    return [].concat(_toConsumableArray(jobs), _toConsumableArray(template.jobs));
  }, []);
};
/**
 * Returns which ConfigTemplates that need to be installed based off of which Jobs are already installed and the configured indexPattern
 *
 * @param templates ConfigTemplates as provided by ML Team
 * @param installedJobIds list of installed JobIds
 * @param indexPatterns list of the user's currently configured IndexPatterns
 */


exports.getJobsToInstall = getJobsToInstall;

var getConfigTemplatesToInstall = function getConfigTemplatesToInstall(templates, installedJobIds, indexPatterns) {
  return templates.filter(function (ct) {
    return !ct.jobs.every(function (ctJobId) {
      return installedJobIds.includes(ctJobId);
    });
  }).filter(function (ct) {
    return indexPatterns.includes(ct.defaultIndexPattern);
  });
};
/**
 * Returns a filtered array of Jobs that based on filterGroup selection (Elastic vs Custom Jobs) and any user provided filterQuery
 *
 * @param jobs to filter
 * @param embeddedJobIds jobIds as defined in the ConfigTemplates provided by the ML Team
 * @param showCustomJobs whether or not to show all Custom Jobs (Non-embedded Jobs in SIEM Group)
 * @param showElasticJobs whether or not to show Elastic Jobs (Embedded ConfigTemplate Jobs)
 * @param filterQuery user-provided search string to filter for occurrence in job names/description
 */


exports.getConfigTemplatesToInstall = getConfigTemplatesToInstall;

var getJobsToDisplay = function getJobsToDisplay(jobs, embeddedJobIds, showCustomJobs, showElasticJobs, filterQuery) {
  return jobs ? searchFilter(jobs.filter(function (job) {
    return showCustomJobs ? embeddedJobIds.includes(job.id) : true;
  }).filter(function (job) {
    return showElasticJobs ? !embeddedJobIds.includes(job.id) : true;
  }), filterQuery) : [];
};
/**
 * Returns filtered array of Jobs based on user-provided search string to filter for occurrence in job names/description
 *
 * @param jobs to filter
 * @param filterQuery user-provided search string to filter for occurrence in job names/description
 */


exports.getJobsToDisplay = getJobsToDisplay;

var searchFilter = function searchFilter(jobs, filterQuery) {
  return jobs.filter(function (job) {
    return filterQuery == null ? true : job.id.includes(filterQuery) || job.description.includes(filterQuery);
  });
};
/**
 * Returns a string array of Index Pattern Titles
 *
 * @param indexPatterns IndexPatternSavedObject[] as provided from the useIndexPatterns() hook
 */


exports.searchFilter = searchFilter;

var getIndexPatternTitles = function getIndexPatternTitles(indexPatterns) {
  return indexPatterns.reduce(function (acc, v) {
    return [].concat(_toConsumableArray(acc), [v.attributes.title]);
  }, []);
};
/**
 * Given an array of titles this will always return the same string for usage within
 * useEffect and other shallow compare areas.
 * This won't return a stable reference for case sensitive strings intentionally for speed.
 * @param patterns string[] string array that will return a stable reference regardless of ordering or case sensitivity.
 */


exports.getIndexPatternTitles = getIndexPatternTitles;

var getStablePatternTitles = function getStablePatternTitles(patterns) {
  return patterns.sort().join();
};
/**
 * Returns a mapping of indexPatternTitle to indexPatternId
 *
 * @param indexPatterns IndexPatternSavedObject[] as provided from the useIndexPatterns() hook
 */


exports.getStablePatternTitles = getStablePatternTitles;

var getIndexPatternTitleIdMapping = function getIndexPatternTitleIdMapping(indexPatterns) {
  return indexPatterns.reduce(function (acc, v) {
    if (v.attributes && v.attributes.title) {
      return [].concat(_toConsumableArray(acc), [{
        title: v.attributes.title,
        id: v.id
      }]);
    } else {
      return acc;
    }
  }, []);
};

exports.getIndexPatternTitleIdMapping = getIndexPatternTitleIdMapping;