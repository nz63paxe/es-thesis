"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulEventsViewer = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _source = require("../../containers/source");

var _store = require("../../store");

var _actions = require("../../store/actions");

var _events_viewer = require("./events_viewer");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var StatefulEventsViewerComponent = _react.default.memo(function (_ref) {
  var createTimeline = _ref.createTimeline,
      columns = _ref.columns,
      dataProviders = _ref.dataProviders,
      deleteEventQuery = _ref.deleteEventQuery,
      end = _ref.end,
      id = _ref.id,
      isLive = _ref.isLive,
      itemsPerPage = _ref.itemsPerPage,
      itemsPerPageOptions = _ref.itemsPerPageOptions,
      kqlMode = _ref.kqlMode,
      kqlQueryExpression = _ref.kqlQueryExpression,
      removeColumn = _ref.removeColumn,
      start = _ref.start,
      sort = _ref.sort,
      updateItemsPerPage = _ref.updateItemsPerPage,
      upsertColumn = _ref.upsertColumn;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showInspect = _useState2[0],
      setShowInspect = _useState2[1];

  (0, _react.useEffect)(function () {
    if (createTimeline != null) {
      createTimeline({
        id: id,
        columns: columns,
        sort: sort,
        itemsPerPage: itemsPerPage
      });
    }

    return function () {
      deleteEventQuery({
        id: id,
        inputId: 'global'
      });
    };
  }, []);

  var onChangeItemsPerPage = function onChangeItemsPerPage(itemsChangedPerPage) {
    return updateItemsPerPage({
      id: id,
      itemsPerPage: itemsChangedPerPage
    });
  };

  var toggleColumn = function toggleColumn(column) {
    var exists = columns.findIndex(function (c) {
      return c.id === column.id;
    }) !== -1;

    if (!exists && upsertColumn != null) {
      upsertColumn({
        column: column,
        id: id,
        index: 1
      });
    }

    if (exists && removeColumn != null) {
      removeColumn({
        columnId: column.id,
        id: id
      });
    }
  };

  return _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indexPattern = _ref2.indexPattern,
        browserFields = _ref2.browserFields;
    return _react.default.createElement("div", {
      onMouseEnter: function onMouseEnter() {
        return setShowInspect(true);
      },
      onMouseLeave: function onMouseLeave() {
        return setShowInspect(false);
      }
    }, _react.default.createElement(_events_viewer.EventsViewer, {
      browserFields: browserFields,
      columns: columns,
      id: id,
      dataProviders: dataProviders,
      end: end,
      indexPattern: indexPattern,
      isLive: isLive,
      itemsPerPage: itemsPerPage,
      itemsPerPageOptions: itemsPerPageOptions,
      kqlMode: kqlMode,
      kqlQueryExpression: kqlQueryExpression,
      onChangeItemsPerPage: onChangeItemsPerPage,
      showInspect: showInspect,
      start: start,
      sort: sort,
      toggleColumn: toggleColumn
    }));
  });
}, function (prevProps, nextProps) {
  return prevProps.id === nextProps.id && prevProps.activePage === nextProps.activePage && (0, _fp.isEqual)(prevProps.columns, nextProps.columns) && (0, _fp.isEqual)(prevProps.dataProviders, nextProps.dataProviders) && prevProps.end === nextProps.end && prevProps.isLive === nextProps.isLive && prevProps.itemsPerPage === nextProps.itemsPerPage && (0, _fp.isEqual)(prevProps.itemsPerPageOptions, nextProps.itemsPerPageOptions) && prevProps.kqlMode === nextProps.kqlMode && prevProps.kqlQueryExpression === nextProps.kqlQueryExpression && prevProps.pageCount === nextProps.pageCount && (0, _fp.isEqual)(prevProps.sort, nextProps.sort) && prevProps.start === nextProps.start;
});

StatefulEventsViewerComponent.displayName = 'StatefulEventsViewerComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getInputsTimeline = _store.inputsSelectors.getTimelineSelector();

  var getEvents = _store.timelineSelectors.getEventsByIdSelector();

  var mapStateToProps = function mapStateToProps(state, _ref3) {
    var id = _ref3.id;
    var input = getInputsTimeline(state);
    var events = getEvents(state, id);
    var columns = events.columns,
        dataProviders = events.dataProviders,
        itemsPerPage = events.itemsPerPage,
        itemsPerPageOptions = events.itemsPerPageOptions,
        kqlMode = events.kqlMode,
        sort = events.sort;
    return {
      columns: columns,
      dataProviders: dataProviders,
      id: id,
      isLive: input.policy.kind === 'interval',
      itemsPerPage: itemsPerPage,
      itemsPerPageOptions: itemsPerPageOptions,
      kqlMode: kqlMode,
      sort: sort
    };
  };

  return mapStateToProps;
};

var StatefulEventsViewer = (0, _reactRedux.connect)(makeMapStateToProps, {
  createTimeline: _actions.timelineActions.createTimeline,
  deleteEventQuery: _actions.inputsActions.deleteOneQuery,
  updateItemsPerPage: _actions.timelineActions.updateItemsPerPage,
  updateSort: _actions.timelineActions.updateSort,
  removeColumn: _actions.timelineActions.removeColumn,
  upsertColumn: _actions.timelineActions.upsertColumn
})(StatefulEventsViewerComponent);
exports.StatefulEventsViewer = StatefulEventsViewer;