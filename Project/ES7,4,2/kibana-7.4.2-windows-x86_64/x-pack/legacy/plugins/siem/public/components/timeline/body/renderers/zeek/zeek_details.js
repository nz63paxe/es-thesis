"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ZeekDetails = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _netflow = require("../netflow");

var _zeek_signature = require("./zeek_signature");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: 5px 0;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Details = _styledComponents.default.div(_templateObject());

Details.displayName = 'Details';
var ZeekDetails = (0, _recompose.pure)(function (_ref) {
  var data = _ref.data,
      timelineId = _ref.timelineId;
  return data.zeek != null ? React.createElement(Details, null, React.createElement(_zeek_signature.ZeekSignature, {
    data: data,
    timelineId: timelineId
  }), React.createElement(_eui.EuiSpacer, {
    size: "s"
  }), React.createElement(_netflow.NetflowRenderer, {
    data: data,
    timelineId: timelineId
  })) : null;
});
exports.ZeekDetails = ZeekDetails;
ZeekDetails.displayName = 'ZeekDetails';