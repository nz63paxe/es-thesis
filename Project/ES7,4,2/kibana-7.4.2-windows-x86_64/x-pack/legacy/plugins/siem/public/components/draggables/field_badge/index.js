"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DraggableFieldBadge = exports.FieldNameContainer = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  padding: 5px;\n  &:hover {\n    transition: background-color 0.7s ease;\n    background-color: #000;\n    color: #fff;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: 38px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FieldBadgeFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
FieldBadgeFlexGroup.displayName = 'FieldBadgeFlexGroup';
var FieldBadgeFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
FieldBadgeFlexItem.displayName = 'FieldBadgeFlexItem';
/**
 * The name of a (draggable) field
 */

var FieldNameContainer = _styledComponents.default.div(_templateObject3());

exports.FieldNameContainer = FieldNameContainer;
FieldNameContainer.displayName = 'FieldNameContainer';
/**
 * Renders a field (e.g. `event.action`) as a draggable badge
 */

var DraggableFieldBadge = (0, _recompose.pure)(function (_ref) {
  var fieldId = _ref.fieldId;
  return React.createElement(_eui.EuiBadge, {
    color: "#000"
  }, React.createElement(FieldBadgeFlexGroup, {
    alignItems: "center",
    justifyContent: "center",
    gutterSize: "none"
  }, React.createElement(FieldBadgeFlexItem, {
    "data-test-subj": "field",
    grow: false
  }, fieldId)));
});
exports.DraggableFieldBadge = DraggableFieldBadge;
DraggableFieldBadge.displayName = 'DraggableFieldBadge';