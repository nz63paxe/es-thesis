"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Header = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _default_headers = require("../events_viewer/default_headers");

var _default_headers2 = require("../timeline/body/column_headers/default_headers");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  input {\n    max-width: ", "px;\n    width: ", "px;\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  padding: 16px;\n  margin-bottom: 8px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CountsFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
CountsFlexGroup.displayName = 'CountsFlexGroup';
var CountFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
CountFlexItem.displayName = 'CountFlexItem'; // background-color: ${props => props.theme.eui.euiColorLightestShade};

var HeaderContainer = _styledComponents.default.div(_templateObject3());

HeaderContainer.displayName = 'HeaderContainer';

var SearchContainer = _styledComponents.default.div(_templateObject4(), _helpers.SEARCH_INPUT_WIDTH, _helpers.SEARCH_INPUT_WIDTH);

SearchContainer.displayName = 'SearchContainer';
var CountRow = (0, _recompose.pure)(function (_ref) {
  var filteredBrowserFields = _ref.filteredBrowserFields;
  return React.createElement(CountsFlexGroup, {
    alignItems: "center",
    "data-test-subj": "counts-flex-group",
    direction: "row",
    gutterSize: "none"
  }, React.createElement(CountFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiText, {
    color: "subdued",
    "data-test-subj": "categories-count",
    size: "xs"
  }, i18n.CATEGORIES_COUNT(Object.keys(filteredBrowserFields).length))), React.createElement(CountFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiText, {
    color: "subdued",
    "data-test-subj": "fields-count",
    size: "xs"
  }, i18n.FIELDS_COUNT(Object.keys(filteredBrowserFields).reduce(function (fieldsCount, category) {
    return (0, _helpers.getFieldCount)(filteredBrowserFields[category]) + fieldsCount;
  }, 0)))));
});
CountRow.displayName = 'CountRow';
var TitleRow = (0, _recompose.pure)(function (_ref2) {
  var isEventViewer = _ref2.isEventViewer,
      onOutsideClick = _ref2.onOutsideClick,
      onUpdateColumns = _ref2.onUpdateColumns;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "spaceBetween",
    direction: "row",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiTitle, {
    "data-test-subj": "field-browser-title",
    size: "s"
  }, React.createElement("h2", null, i18n.CUSTOMIZE_COLUMNS))), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiButtonEmpty, {
    "data-test-subj": "reset-fields",
    onClick: function onClick() {
      onUpdateColumns(isEventViewer ? _default_headers.defaultHeaders : _default_headers2.defaultHeaders);
      onOutsideClick();
    }
  }, i18n.RESET_FIELDS)));
});
TitleRow.displayName = 'TitleRow';
var Header = (0, _recompose.pure)(function (_ref3) {
  var isEventViewer = _ref3.isEventViewer,
      isSearching = _ref3.isSearching,
      filteredBrowserFields = _ref3.filteredBrowserFields,
      onOutsideClick = _ref3.onOutsideClick,
      onSearchInputChange = _ref3.onSearchInputChange,
      onUpdateColumns = _ref3.onUpdateColumns,
      searchInput = _ref3.searchInput,
      timelineId = _ref3.timelineId;
  return React.createElement(HeaderContainer, null, React.createElement(TitleRow, {
    isEventViewer: isEventViewer,
    onUpdateColumns: onUpdateColumns,
    onOutsideClick: onOutsideClick
  }), React.createElement(SearchContainer, null, React.createElement(_eui.EuiFieldSearch, {
    className: (0, _helpers.getFieldBrowserSearchInputClassName)(timelineId),
    "data-test-subj": "field-search",
    isLoading: isSearching,
    onChange: onSearchInputChange,
    placeholder: i18n.FILTER_PLACEHOLDER,
    value: searchInput
  })), React.createElement(CountRow, {
    filteredBrowserFields: filteredBrowserFields
  }));
});
exports.Header = Header;
Header.displayName = 'Header';