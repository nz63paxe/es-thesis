"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GeoFields = exports.getGeoFieldPropNameToFieldNameMap = exports.DESTINATION_GEO_CITY_NAME_FIELD_NAME = exports.DESTINATION_GEO_REGION_NAME_FIELD_NAME = exports.DESTINATION_GEO_COUNTRY_ISO_CODE_FIELD_NAME = exports.DESTINATION_GEO_COUNTRY_NAME_FIELD_NAME = exports.DESTINATION_GEO_CONTINENT_NAME_FIELD_NAME = exports.SOURCE_GEO_CITY_NAME_FIELD_NAME = exports.SOURCE_GEO_REGION_NAME_FIELD_NAME = exports.SOURCE_GEO_COUNTRY_ISO_CODE_FIELD_NAME = exports.SOURCE_GEO_COUNTRY_NAME_FIELD_NAME = exports.SOURCE_GEO_CONTINENT_NAME_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggables = require("../draggables");

var _country_flag = require("./country_flag");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SOURCE_GEO_CONTINENT_NAME_FIELD_NAME = 'source.geo.continent_name';
exports.SOURCE_GEO_CONTINENT_NAME_FIELD_NAME = SOURCE_GEO_CONTINENT_NAME_FIELD_NAME;
var SOURCE_GEO_COUNTRY_NAME_FIELD_NAME = 'source.geo.country_name';
exports.SOURCE_GEO_COUNTRY_NAME_FIELD_NAME = SOURCE_GEO_COUNTRY_NAME_FIELD_NAME;
var SOURCE_GEO_COUNTRY_ISO_CODE_FIELD_NAME = 'source.geo.country_iso_code';
exports.SOURCE_GEO_COUNTRY_ISO_CODE_FIELD_NAME = SOURCE_GEO_COUNTRY_ISO_CODE_FIELD_NAME;
var SOURCE_GEO_REGION_NAME_FIELD_NAME = 'source.geo.region_name';
exports.SOURCE_GEO_REGION_NAME_FIELD_NAME = SOURCE_GEO_REGION_NAME_FIELD_NAME;
var SOURCE_GEO_CITY_NAME_FIELD_NAME = 'source.geo.city_name';
exports.SOURCE_GEO_CITY_NAME_FIELD_NAME = SOURCE_GEO_CITY_NAME_FIELD_NAME;
var DESTINATION_GEO_CONTINENT_NAME_FIELD_NAME = 'destination.geo.continent_name';
exports.DESTINATION_GEO_CONTINENT_NAME_FIELD_NAME = DESTINATION_GEO_CONTINENT_NAME_FIELD_NAME;
var DESTINATION_GEO_COUNTRY_NAME_FIELD_NAME = 'destination.geo.country_name';
exports.DESTINATION_GEO_COUNTRY_NAME_FIELD_NAME = DESTINATION_GEO_COUNTRY_NAME_FIELD_NAME;
var DESTINATION_GEO_COUNTRY_ISO_CODE_FIELD_NAME = 'destination.geo.country_iso_code';
exports.DESTINATION_GEO_COUNTRY_ISO_CODE_FIELD_NAME = DESTINATION_GEO_COUNTRY_ISO_CODE_FIELD_NAME;
var DESTINATION_GEO_REGION_NAME_FIELD_NAME = 'destination.geo.region_name';
exports.DESTINATION_GEO_REGION_NAME_FIELD_NAME = DESTINATION_GEO_REGION_NAME_FIELD_NAME;
var DESTINATION_GEO_CITY_NAME_FIELD_NAME = 'destination.geo.city_name';
exports.DESTINATION_GEO_CITY_NAME_FIELD_NAME = DESTINATION_GEO_CITY_NAME_FIELD_NAME;
var geoPropNameToFieldNameSuffix = [{
  prop: 'GeoContinentName',
  fieldName: 'geo.continent_name'
}, {
  prop: 'GeoCountryName',
  fieldName: 'geo.country_name'
}, {
  prop: 'GeoCountryIsoCode',
  fieldName: 'geo.country_iso_code'
}, {
  prop: 'GeoRegionName',
  fieldName: 'geo.region_name'
}, {
  prop: 'GeoCityName',
  fieldName: 'geo.city_name'
}];

var getGeoFieldPropNameToFieldNameMap = function getGeoFieldPropNameToFieldNameMap(type) {
  return geoPropNameToFieldNameSuffix.map(function (_ref) {
    var prop = _ref.prop,
        fieldName = _ref.fieldName;
    return {
      prop: "".concat(type).concat(prop),
      fieldName: "".concat(type, ".").concat(fieldName)
    };
  });
};

exports.getGeoFieldPropNameToFieldNameMap = getGeoFieldPropNameToFieldNameMap;
var GeoFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
GeoFlexItem.displayName = 'GeoFlexItem';
var GeoFieldValues = (0, _recompose.pure)(function (_ref2) {
  var contextId = _ref2.contextId,
      eventId = _ref2.eventId,
      fieldName = _ref2.fieldName,
      values = _ref2.values;
  return values != null ? React.createElement(React.Fragment, null, (0, _fp.uniq)(values).map(function (value) {
    return React.createElement(GeoFlexItem, {
      grow: false,
      key: "".concat(contextId, "-").concat(eventId, "-").concat(fieldName, "-").concat(value)
    }, React.createElement(_eui.EuiFlexGroup, {
      alignItems: "center",
      gutterSize: "none"
    }, fieldName === SOURCE_GEO_COUNTRY_ISO_CODE_FIELD_NAME || fieldName === DESTINATION_GEO_COUNTRY_ISO_CODE_FIELD_NAME ? React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement(_country_flag.CountryFlag, {
      countryCode: value
    })) : null, React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement(_draggables.DefaultDraggable, {
      "data-test-subj": fieldName,
      field: fieldName,
      id: "geo-field-values-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(fieldName, "-").concat(value),
      tooltipContent: fieldName,
      value: value
    }))));
  })) : null;
});
GeoFieldValues.displayName = 'GeoFieldValues';
/**
 * Renders a row of draggable text containing geographic fields, such as:
 * - `source|destination.geo.continent_name`
 * - `source|destination.geo.country_name`
 * - `source|destination.geo.country_iso_code`
 * - `source|destination.geo.region_iso_code`
 * - `source|destination.geo.city_name`
 */

var GeoFields = (0, _recompose.pure)(function (props) {
  var contextId = props.contextId,
      eventId = props.eventId,
      type = props.type;
  var propNameToFieldName = getGeoFieldPropNameToFieldNameMap(type);
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none"
  }, (0, _fp.uniq)(propNameToFieldName).map(function (geo) {
    return React.createElement(GeoFieldValues, {
      contextId: contextId,
      eventId: eventId,
      fieldName: geo.fieldName,
      key: geo.fieldName,
      values: (0, _fp.get)(geo.prop, props)
    });
  }));
});
exports.GeoFields = GeoFields;
GeoFields.displayName = 'GeoFields';