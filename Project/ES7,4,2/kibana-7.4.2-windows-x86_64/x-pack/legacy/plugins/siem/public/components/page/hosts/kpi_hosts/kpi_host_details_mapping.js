"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.kpiHostDetailsMapping = void 0;

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var euiColorVis0 = '#00B3A4';
var euiColorVis2 = '#DB1374';
var euiColorVis3 = '#490092';
var euiColorVis9 = '#920000';
var kpiHostDetailsMapping = [{
  key: 'authentication',
  index: 0,
  fields: [{
    key: 'authSuccess',
    name: i18n.SUCCESS_CHART_LABEL,
    description: i18n.SUCCESS_UNIT_LABEL,
    value: null,
    color: euiColorVis0,
    icon: 'check'
  }, {
    key: 'authFailure',
    name: i18n.FAIL_CHART_LABEL,
    description: i18n.FAIL_UNIT_LABEL,
    value: null,
    color: euiColorVis9,
    icon: 'cross'
  }],
  enableAreaChart: true,
  enableBarChart: true,
  grow: 1,
  description: i18n.USER_AUTHENTICATIONS
}, {
  key: 'uniqueIps',
  index: 1,
  fields: [{
    key: 'uniqueSourceIps',
    name: i18n.SOURCE_CHART_LABEL,
    description: i18n.SOURCE_UNIT_LABEL,
    value: null,
    color: euiColorVis2,
    icon: 'visMapCoordinate'
  }, {
    key: 'uniqueDestinationIps',
    name: i18n.DESTINATION_CHART_LABEL,
    description: i18n.DESTINATION_UNIT_LABEL,
    value: null,
    color: euiColorVis3,
    icon: 'visMapCoordinate'
  }],
  enableAreaChart: true,
  enableBarChart: true,
  grow: 1,
  description: i18n.UNIQUE_IPS
}];
exports.kpiHostDetailsMapping = kpiHostDetailsMapping;