"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUrlStateHooks = void 0;

var _fp = require("lodash/fp");

var _react = require("react");

var _apollo_context = require("../../utils/apollo_context");

var _constants = require("./constants");

var _helpers = require("./helpers");

var _types = require("./types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function usePrevious(value) {
  var ref = (0, _react.useRef)(value);
  (0, _react.useEffect)(function () {
    ref.current = value;
  });
  return ref.current;
}

var useUrlStateHooks = function useUrlStateHooks(_ref) {
  var detailName = _ref.detailName,
      indexPattern = _ref.indexPattern,
      history = _ref.history,
      navTabs = _ref.navTabs,
      pageName = _ref.pageName,
      pathName = _ref.pathName,
      search = _ref.search,
      setInitialStateFromUrl = _ref.setInitialStateFromUrl,
      tabName = _ref.tabName,
      updateTimeline = _ref.updateTimeline,
      updateTimelineIsLoading = _ref.updateTimelineIsLoading,
      urlState = _ref.urlState;

  var _useState = (0, _react.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      isInitializing = _useState2[0],
      setIsInitializing = _useState2[1];

  var apolloClient = (0, _apollo_context.useApolloClient)();
  var prevProps = usePrevious({
    pathName: pathName,
    urlState: urlState
  });

  var replaceStateInLocation = function replaceStateInLocation(urlStateToReplace, urlStateKey) {
    var latestLocation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {
      hash: '',
      pathname: pathName,
      search: search,
      state: ''
    };
    var newLocation = (0, _helpers.replaceQueryStringInLocation)({
      hash: '',
      pathname: pathName,
      search: search,
      state: ''
    }, (0, _helpers.replaceStateKeyInQueryString)(urlStateKey, urlStateToReplace)((0, _helpers.getQueryStringFromLocation)(latestLocation)));

    if (history) {
      history.replace(newLocation);
    }

    return newLocation;
  };

  var handleInitialize = function handleInitialize(initLocation, type) {
    var myLocation = initLocation;
    var urlStateToUpdate = [];

    _types.URL_STATE_KEYS[type].forEach(function (urlKey) {
      var newUrlStateString = (0, _helpers.getParamFromQueryString)((0, _helpers.getQueryStringFromLocation)(initLocation), urlKey);

      if (newUrlStateString) {
        var kqlQueryStateData = (0, _helpers.decodeRisonUrlState)(newUrlStateString);

        if (urlKey === _constants.CONSTANTS.kqlQuery && !(0, _helpers.isKqlForRoute)(pageName, detailName, kqlQueryStateData.queryLocation) && urlState[urlKey].queryLocation === kqlQueryStateData.queryLocation) {
          myLocation = replaceStateInLocation({
            filterQuery: null,
            queryLocation: null
          }, urlKey, myLocation);
        }

        if (isInitializing) {
          urlStateToUpdate = [].concat(_toConsumableArray(urlStateToUpdate), [{
            urlKey: urlKey,
            newUrlStateString: newUrlStateString
          }]);
        }
      } else {
        myLocation = replaceStateInLocation(urlState[urlKey], urlKey, myLocation);
      }
    });

    (0, _fp.difference)(_types.ALL_URL_STATE_KEYS, _types.URL_STATE_KEYS[type]).forEach(function (urlKey) {
      myLocation = replaceStateInLocation('', urlKey, myLocation);
    });
    setInitialStateFromUrl({
      apolloClient: apolloClient,
      detailName: detailName,
      indexPattern: indexPattern,
      pageName: pageName,
      updateTimeline: updateTimeline,
      updateTimelineIsLoading: updateTimelineIsLoading,
      urlStateToUpdate: urlStateToUpdate
    })();
  };

  (0, _react.useEffect)(function () {
    var type = (0, _helpers.getUrlType)(pageName);
    var location = {
      hash: '',
      pathname: pathName,
      search: search,
      state: ''
    };

    if (isInitializing && pageName != null && pageName !== '') {
      handleInitialize(location, type);
      setIsInitializing(false);
    } else if (!(0, _fp.isEqual)(urlState, prevProps.urlState) && !isInitializing) {
      var newLocation = location;

      _types.URL_STATE_KEYS[type].forEach(function (urlKey) {
        newLocation = replaceStateInLocation(urlState[urlKey], urlKey, newLocation);
      });
    } else if (pathName !== prevProps.pathName) {
      handleInitialize(location, type);
    }
  });
  (0, _react.useEffect)(function () {
    document.title = "".concat((0, _helpers.getTitle)(pageName, detailName, navTabs), " - Kibana");
  }, [pageName]);
  return null;
};

exports.useUrlStateHooks = useUrlStateHooks;