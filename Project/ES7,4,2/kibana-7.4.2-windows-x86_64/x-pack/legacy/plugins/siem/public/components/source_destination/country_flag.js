"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CountryFlag = exports.getFlag = void 0;

var _react = _interopRequireWildcard(require("react"));

var _fp = require("lodash/fp");

var _eui = require("@elastic/eui");

var _i18nIsoCountries = _interopRequireDefault(require("i18n-iso-countries"));

var _en = _interopRequireDefault(require("i18n-iso-countries/langs/en.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns the flag for the specified country code, or null if the specified
 * country code could not be converted
 * Example: `US` -> 🇺🇸
 */
var getFlag = function getFlag(countryCode) {
  return countryCode && countryCode.length === 2 ? countryCode.toUpperCase().replace(/./g, function (c) {
    return String.fromCharCode(55356, 56741 + c.charCodeAt(0));
  }) : null;
};
/** Renders an emjoi flag for the specified country code */


exports.getFlag = getFlag;
var CountryFlag = (0, _react.memo)(function (_ref) {
  var countryCode = _ref.countryCode,
      _ref$displayCountryNa = _ref.displayCountryNameOnHover,
      displayCountryNameOnHover = _ref$displayCountryNa === void 0 ? false : _ref$displayCountryNa;
  (0, _react.useEffect)(function () {
    if (displayCountryNameOnHover && (0, _fp.isEmpty)(_i18nIsoCountries.default.getNames('en'))) {
      _i18nIsoCountries.default.registerLocale(_en.default);
    }
  }, []);
  var flag = getFlag(countryCode);

  if (flag !== null) {
    return displayCountryNameOnHover ? _react.default.createElement(_eui.EuiToolTip, {
      position: "top",
      content: _i18nIsoCountries.default.getName(countryCode, 'en')
    }, _react.default.createElement("span", {
      "data-test-subj": "country-flag"
    }, flag)) : _react.default.createElement("span", {
      "data-test-subj": "country-flag"
    }, flag);
  }

  return null;
});
exports.CountryFlag = CountryFlag;
CountryFlag.displayName = 'CountryFlag';