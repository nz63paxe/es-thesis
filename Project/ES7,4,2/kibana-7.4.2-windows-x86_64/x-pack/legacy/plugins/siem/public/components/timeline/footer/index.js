"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Footer = exports.PagingControl = exports.EventsCount = exports.footerHeight = exports.ServerSideEventCount = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _loading = require("../../loading");

var _last_updated = require("./last_updated");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 5px 0 5px;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  .euiButtonEmpty__content {\n    padding: 0px 0px;\n  }\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  padding-top: 3px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  height: 35px;\n  width: 100%;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: ", "px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: ", "px;\n  overflow: hidden;\n  text-align: end;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FixedWidthLastUpdated = _styledComponents.default.div(_templateObject(), function (_ref) {
  var compact = _ref.compact;
  return !compact ? 200 : 25;
});

FixedWidthLastUpdated.displayName = 'FixedWidthLastUpdated';
var FooterContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2(), function (_ref2) {
  var height = _ref2.height;
  return height;
});
FooterContainer.displayName = 'FooterContainer';
var FooterFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject3());
FooterFlexGroup.displayName = 'FooterFlexGroup';

var LoadingPanelContainer = _styledComponents.default.div(_templateObject4());

LoadingPanelContainer.displayName = 'LoadingPanelContainer';
var PopoverRowItems = (0, _styledComponents.default)(_eui.EuiPopover)(_templateObject5());
PopoverRowItems.displayName = 'PopoverRowItems';

var ServerSideEventCount = _styledComponents.default.div(_templateObject6());

exports.ServerSideEventCount = ServerSideEventCount;
ServerSideEventCount.displayName = 'ServerSideEventCount';
/** The height of the footer, exported for use in height calculations */

var footerHeight = 40; // px

exports.footerHeight = footerHeight;

/** Displays the server-side count of events */
var EventsCount = (0, _recompose.pure)(function (_ref3) {
  var closePopover = _ref3.closePopover,
      isOpen = _ref3.isOpen,
      items = _ref3.items,
      itemsCount = _ref3.itemsCount,
      onClick = _ref3.onClick,
      serverSideEventCount = _ref3.serverSideEventCount;
  return React.createElement("h5", null, React.createElement(PopoverRowItems, {
    className: "footer-popover",
    id: "customizablePagination",
    "data-test-subj": "timelineSizeRowPopover",
    button: React.createElement(React.Fragment, null, React.createElement(_eui.EuiBadge, {
      "data-test-subj": "local-events-count",
      color: "hollow"
    }, itemsCount, React.createElement(_eui.EuiButtonEmpty, {
      size: "s",
      color: "text",
      iconType: "arrowDown",
      iconSide: "right",
      onClick: onClick
    })), " ".concat(i18n.OF, " ")),
    isOpen: isOpen,
    closePopover: closePopover,
    panelPaddingSize: "none"
  }, React.createElement(_eui.EuiContextMenuPanel, {
    items: items,
    "data-test-subj": "timelinePickSizeRow"
  })), React.createElement(_eui.EuiToolTip, {
    content: "".concat(serverSideEventCount, " ").concat(i18n.TOTAL_COUNT_OF_EVENTS)
  }, React.createElement(ServerSideEventCount, null, React.createElement(_eui.EuiBadge, {
    color: "hollow",
    "data-test-subj": "server-side-event-count"
  }, serverSideEventCount), ' ', i18n.EVENTS)));
});
exports.EventsCount = EventsCount;
EventsCount.displayName = 'EventsCount';
var PagingControl = (0, _recompose.pure)(function (_ref4) {
  var hasNextPage = _ref4.hasNextPage,
      isLoading = _ref4.isLoading,
      loadMore = _ref4.loadMore;
  return React.createElement(React.Fragment, null, hasNextPage && React.createElement(_eui.EuiButton, {
    "data-test-subj": "TimelineMoreButton",
    isLoading: isLoading,
    onClick: loadMore,
    size: "s"
  }, isLoading ? "".concat(i18n.LOADING, "...") : i18n.LOAD_MORE));
});
exports.PagingControl = PagingControl;
PagingControl.displayName = 'PagingControl';
/** Renders a loading indicator and paging controls */

var Footer =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Footer, _React$Component);

  function Footer() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Footer);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Footer)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isPopoverOpen: false,
      paginationLoading: false,
      updatedAt: null
    });

    _defineProperty(_assertThisInitialized(_this), "loadMore", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          paginationLoading: true
        });
      });

      _this.props.onLoadMore(_this.props.nextCursor, _this.props.tieBreaker);
    });

    _defineProperty(_assertThisInitialized(_this), "onButtonClick", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          isPopoverOpen: !prevState.isPopoverOpen
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closePopover", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          isPopoverOpen: false
        });
      });
    });

    return _this;
  }

  _createClass(Footer, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(_ref5, _ref6) {
      var compact = _ref5.compact,
          hasNextPage = _ref5.hasNextPage,
          height = _ref5.height,
          isEventViewer = _ref5.isEventViewer,
          isLive = _ref5.isLive,
          isLoading = _ref5.isLoading,
          itemsCount = _ref5.itemsCount,
          itemsPerPage = _ref5.itemsPerPage,
          itemsPerPageOptions = _ref5.itemsPerPageOptions,
          serverSideEventCount = _ref5.serverSideEventCount;
      var isPopoverOpen = _ref6.isPopoverOpen,
          paginationLoading = _ref6.paginationLoading,
          updatedAt = _ref6.updatedAt;
      return compact !== this.props.compact || hasNextPage !== this.props.hasNextPage || height !== this.props.height || isEventViewer !== this.props.isEventViewer || isLive !== this.props.isLive || isLoading !== this.props.isLoading || isPopoverOpen !== this.state.isPopoverOpen || itemsCount !== this.props.itemsCount || itemsPerPage !== this.props.itemsPerPage || itemsPerPageOptions !== this.props.itemsPerPageOptions || paginationLoading !== this.state.paginationLoading || serverSideEventCount !== this.props.serverSideEventCount || updatedAt !== this.state.updatedAt;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$state = this.state,
          paginationLoading = _this$state.paginationLoading,
          updatedAt = _this$state.updatedAt;
      var _this$props = this.props,
          isLoading = _this$props.isLoading,
          getUpdatedAt = _this$props.getUpdatedAt;

      if (paginationLoading && prevProps.isLoading && !isLoading) {
        this.setState(function (prevState) {
          return _objectSpread({}, prevState, {
            paginationLoading: false,
            updatedAt: getUpdatedAt()
          });
        });
      }

      if (updatedAt === null || prevProps.isLoading && !isLoading) {
        this.setState(function (prevState) {
          return _objectSpread({}, prevState, {
            updatedAt: getUpdatedAt()
          });
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          height = _this$props2.height,
          isEventViewer = _this$props2.isEventViewer,
          isLive = _this$props2.isLive,
          isLoading = _this$props2.isLoading,
          itemsCount = _this$props2.itemsCount,
          itemsPerPage = _this$props2.itemsPerPage,
          itemsPerPageOptions = _this$props2.itemsPerPageOptions,
          onChangeItemsPerPage = _this$props2.onChangeItemsPerPage,
          serverSideEventCount = _this$props2.serverSideEventCount,
          hasNextPage = _this$props2.hasNextPage,
          getUpdatedAt = _this$props2.getUpdatedAt,
          compact = _this$props2.compact;

      if (isLoading && !this.state.paginationLoading) {
        return React.createElement(LoadingPanelContainer, null, React.createElement(_loading.LoadingPanel, {
          "data-test-subj": "LoadingPanelTimeline",
          height: "35px",
          showBorder: false,
          text: isEventViewer ? "".concat(i18n.LOADING_EVENTS, "...") : "".concat(i18n.LOADING_TIMELINE_DATA, "..."),
          width: "100%"
        }));
      }

      var rowItems = itemsPerPageOptions && itemsPerPageOptions.map(function (item) {
        return React.createElement(_eui.EuiContextMenuItem, {
          key: item,
          icon: itemsPerPage === item ? 'check' : 'empty',
          onClick: function onClick() {
            _this2.closePopover();

            onChangeItemsPerPage(item);
          }
        }, "".concat(item, " ").concat(i18n.ROWS));
      });
      return React.createElement(React.Fragment, null, React.createElement(FooterContainer, {
        "data-test-subj": "timeline-footer",
        direction: "column",
        height: height,
        gutterSize: "none",
        justifyContent: "spaceAround"
      }, React.createElement(FooterFlexGroup, {
        alignItems: "center",
        "data-test-subj": "footer-flex-group",
        direction: "row",
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, React.createElement(_eui.EuiFlexItem, {
        "data-test-subj": "event-count-container",
        grow: false
      }, React.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        "data-test-subj": "events-count",
        direction: "row",
        gutterSize: "none"
      }, React.createElement(EventsCount, {
        closePopover: this.closePopover,
        isOpen: this.state.isPopoverOpen,
        items: rowItems,
        itemsCount: itemsCount,
        onClick: this.onButtonClick,
        serverSideEventCount: serverSideEventCount
      }))), React.createElement(_eui.EuiFlexItem, {
        "data-test-subj": "paging-control-container",
        grow: false
      }, isLive ? React.createElement(_eui.EuiText, {
        size: "s",
        "data-test-subj": "is-live-on-message"
      }, React.createElement("b", null, i18n.AUTO_REFRESH_ACTIVE, ' ', React.createElement(_eui.EuiIconTip, {
        color: "subdued",
        content: React.createElement(_react.FormattedMessage, {
          id: "xpack.siem.footer.autoRefreshActiveTooltip",
          defaultMessage: "While auto-refresh is enabled, timeline will show you the latest {numberOfItems} events that match your query.",
          values: {
            numberOfItems: itemsCount
          }
        }),
        type: "iInCircle"
      }))) : React.createElement(PagingControl, {
        "data-test-subj": "paging-control",
        hasNextPage: hasNextPage,
        isLoading: isLoading,
        loadMore: this.loadMore
      })), React.createElement(_eui.EuiFlexItem, {
        "data-test-subj": "last-updated-container",
        grow: false
      }, React.createElement(FixedWidthLastUpdated, {
        "data-test-subj": "fixed-width-last-updated",
        compact: compact
      }, React.createElement(_last_updated.LastUpdatedAt, {
        updatedAt: this.state.updatedAt || getUpdatedAt(),
        compact: compact
      }))))));
    }
  }]);

  return Footer;
}(React.Component);

exports.Footer = Footer;