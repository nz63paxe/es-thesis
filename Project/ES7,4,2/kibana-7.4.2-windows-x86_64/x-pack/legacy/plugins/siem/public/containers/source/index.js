"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.indicesExistOrDataTemporarilyUnavailable = exports.WithSource = exports.getAllFieldsByName = exports.getAllBrowserFields = void 0;

var _lodash = require("lodash");

var _fp = require("lodash/fp");

var _reactApollo = require("react-apollo");

var _react = _interopRequireDefault(require("react"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _constants = require("../../../common/constants");

var _index = require("./index.gql_query");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var getAllBrowserFields = function getAllBrowserFields(browserFields) {
  return Object.values(browserFields).reduce(function (acc, namespace) {
    return [].concat(_toConsumableArray(acc), _toConsumableArray(Object.values(namespace.fields != null ? namespace.fields : {})));
  }, []);
};

exports.getAllBrowserFields = getAllBrowserFields;

var getAllFieldsByName = function getAllFieldsByName(browserFields) {
  return (0, _fp.keyBy)('name', getAllBrowserFields(browserFields));
};

exports.getAllFieldsByName = getAllFieldsByName;

var WithSource =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(WithSource, _React$PureComponent);

  function WithSource(props) {
    var _this;

    _classCallCheck(this, WithSource);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(WithSource).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedIndexFields", void 0);

    _defineProperty(_assertThisInitialized(_this), "memoizedBrowserFields", void 0);

    _defineProperty(_assertThisInitialized(_this), "getIndexFields", function (title, fields) {
      return fields && fields.length > 0 ? {
        fields: fields.map(function (field) {
          return (0, _fp.pick)(['name', 'searchable', 'type', 'aggregatable'], field);
        }),
        title: title
      } : {
        fields: [],
        title: title
      };
    });

    _defineProperty(_assertThisInitialized(_this), "getBrowserFields", function (fields) {
      return fields && fields.length > 0 ? fields.reduce(function (accumulator, field) {
        return (0, _fp.set)([field.category, 'fields', field.name], field, accumulator);
      }, {}) : {};
    });

    _this.memoizedIndexFields = (0, _memoizeOne.default)(_this.getIndexFields);
    _this.memoizedBrowserFields = (0, _memoizeOne.default)(_this.getBrowserFields);
    return _this;
  }

  _createClass(WithSource, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          children = _this$props.children,
          sourceId = _this$props.sourceId;
      return _react.default.createElement(_reactApollo.Query, {
        query: _index.sourceQuery,
        fetchPolicy: "cache-first",
        notifyOnNetworkStatusChange: true,
        variables: {
          sourceId: sourceId,
          defaultIndex: _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_INDEX_KEY)
        }
      }, function (_ref) {
        var data = _ref.data;
        return children({
          indicesExist: (0, _fp.get)('source.status.indicesExist', data),
          browserFields: _this2.memoizedBrowserFields((0, _fp.get)('source.status.indexFields', data)),
          indexPattern: _this2.memoizedIndexFields(_chrome.default.getUiSettingsClient().get(_constants.DEFAULT_INDEX_KEY).join(), (0, _fp.get)('source.status.indexFields', data))
        });
      });
    }
  }]);

  return WithSource;
}(_react.default.PureComponent);

exports.WithSource = WithSource;

var indicesExistOrDataTemporarilyUnavailable = function indicesExistOrDataTemporarilyUnavailable(indicesExist) {
  return indicesExist || (0, _lodash.isUndefined)(indicesExist);
};

exports.indicesExistOrDataTemporarilyUnavailable = indicesExistOrDataTemporarilyUnavailable;