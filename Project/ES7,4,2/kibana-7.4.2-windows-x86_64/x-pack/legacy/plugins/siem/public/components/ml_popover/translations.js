"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CREATE_JOB_FAILURE = exports.STOP_JOB_FAILURE = exports.START_JOB_FAILURE = exports.CREATE_CUSTOM_JOB = exports.NO_ITEMS_TEXT = exports.COLUMN_RUN_JOB = exports.COLUMN_JOB_NAME = exports.SHOW_CUSTOM_JOBS = exports.SHOW_ELASTIC_JOBS = exports.FILTER_PLACEHOLDER = exports.UPGRADE_BUTTON = exports.UPGRADE_DESCRIPTION = exports.UPGRADE_TITLE = exports.ANOMALY_DETECTION_TITLE = exports.ANOMALY_DETECTION = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ANOMALY_DETECTION = _i18n.i18n.translate('xpack.siem.components.mlPopup.anomalyDetectionButtonLabel', {
  defaultMessage: 'Anomaly detection'
});

exports.ANOMALY_DETECTION = ANOMALY_DETECTION;

var ANOMALY_DETECTION_TITLE = _i18n.i18n.translate('xpack.siem.components.mlPopup.anomalyDetectionTitle', {
  defaultMessage: 'Anomaly detection settings'
});

exports.ANOMALY_DETECTION_TITLE = ANOMALY_DETECTION_TITLE;

var UPGRADE_TITLE = _i18n.i18n.translate('xpack.siem.components.mlPopup.upgradeTitle', {
  defaultMessage: 'Upgrade to Elastic Platinum'
});

exports.UPGRADE_TITLE = UPGRADE_TITLE;

var UPGRADE_DESCRIPTION = _i18n.i18n.translate('xpack.siem.components.mlPopup.upgradeDescription', {
  defaultMessage: 'In order to access SIEM’s anomaly detection features, you must be subscribed to an Elastic Platinum license. With it, you’ll have the ability to run Machine Learning jobs to view anomalous events throughout SIEM.'
});

exports.UPGRADE_DESCRIPTION = UPGRADE_DESCRIPTION;

var UPGRADE_BUTTON = _i18n.i18n.translate('xpack.siem.components.mlPopup.upgradeButtonLabel', {
  defaultMessage: 'Subscription options'
});

exports.UPGRADE_BUTTON = UPGRADE_BUTTON;

var FILTER_PLACEHOLDER = _i18n.i18n.translate('xpack.siem.components.mlPopup.filterPlaceholder', {
  defaultMessage: 'e.g. rare_process_linux'
});

exports.FILTER_PLACEHOLDER = FILTER_PLACEHOLDER;

var SHOW_ELASTIC_JOBS = _i18n.i18n.translate('xpack.siem.components.mlPopup.showAllJobsLabel', {
  defaultMessage: 'Elastic jobs'
});

exports.SHOW_ELASTIC_JOBS = SHOW_ELASTIC_JOBS;

var SHOW_CUSTOM_JOBS = _i18n.i18n.translate('xpack.siem.components.mlPopup.showSiemJobsLabel', {
  defaultMessage: 'Custom jobs'
});

exports.SHOW_CUSTOM_JOBS = SHOW_CUSTOM_JOBS;

var COLUMN_JOB_NAME = _i18n.i18n.translate('xpack.siem.components.mlPopup.jobsTable.jobNameColumn', {
  defaultMessage: 'Job name'
});

exports.COLUMN_JOB_NAME = COLUMN_JOB_NAME;

var COLUMN_RUN_JOB = _i18n.i18n.translate('xpack.siem.components.mlPopup.jobsTable.runJobColumn', {
  defaultMessage: 'Run job'
});

exports.COLUMN_RUN_JOB = COLUMN_RUN_JOB;

var NO_ITEMS_TEXT = _i18n.i18n.translate('xpack.siem.components.mlPopup.jobsTable.noItemsDescription', {
  defaultMessage: 'No SIEM Machine Learning jobs found'
});

exports.NO_ITEMS_TEXT = NO_ITEMS_TEXT;

var CREATE_CUSTOM_JOB = _i18n.i18n.translate('xpack.siem.components.mlPopup.jobsTable.createCustomJobButtonLabel', {
  defaultMessage: 'Create custom job'
});

exports.CREATE_CUSTOM_JOB = CREATE_CUSTOM_JOB;

var START_JOB_FAILURE = _i18n.i18n.translate('xpack.siem.components.mlPopup.errors.startJobFailureTitle', {
  defaultMessage: 'Start job failure'
});

exports.START_JOB_FAILURE = START_JOB_FAILURE;

var STOP_JOB_FAILURE = _i18n.i18n.translate('xpack.siem.containers.errors.stopJobFailureTitle', {
  defaultMessage: 'Stop job failure'
});

exports.STOP_JOB_FAILURE = STOP_JOB_FAILURE;

var CREATE_JOB_FAILURE = _i18n.i18n.translate('xpack.siem.components.mlPopup.errors.createJobFailureTitle', {
  defaultMessage: 'Create job failure'
});

exports.CREATE_JOB_FAILURE = CREATE_JOB_FAILURE;