"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulEvent = exports.EmptyRow = exports.getNewNoteId = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _uuid = _interopRequireDefault(require("uuid"));

var _reactVisibilitySensor = _interopRequireDefault(require("react-visibility-sensor"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _details = require("../../../../containers/timeline/details");

var _expandable_event = require("../../expandable_event");

var _helpers = require("../../helpers");

var _get_row_renderer = require("../renderers/get_row_renderer");

var _scheduler = require("../../../../lib/helpers/scheduler");

var _stateful_event_child = require("./stateful_event_child");

var _helpers2 = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  width: 100%;\n  border: ", ";\n  border-radius: 5px;\n  margin: 5px 0 5px 0;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var getNewNoteId = function getNewNoteId() {
  return _uuid.default.v4();
};

exports.getNewNoteId = getNewNoteId;
var emptyDetails = [];
/**
 * This is the default row height whenever it is a plain row renderer and not a custom row height.
 * We use this value when we do not know the height of a particular row.
 */

var DEFAULT_ROW_HEIGHT = '27px';
/**
 * This is the default margin size from the EmptyRow below and is the top and bottom
 * margin added together.
 * If you change margin: 5px 0 5px 0; within EmptyRow styled component, then please
 * update this value
 */

var EMPTY_ROW_MARGIN_TOP_BOTTOM = 10;
/**
 * This is the top offset in pixels of the top part of the timeline. The UI area where you do your
 * drag and drop and filtering.  It is a positive number in pixels of _PART_ of the header but not
 * the entire header. We leave room for some rows to render behind the drag and drop so they might be
 * visible by the time the user scrolls upwards. All other DOM elements are replaced with their "blank"
 * rows.
 */

var TOP_OFFSET = 50;
/**
 * This is the bottom offset in pixels of the bottom part of the timeline. The UI area right below the
 * timeline which is the footer.  Since the footer is so incredibly small we don't have enough room to
 * render around 5 rows below the timeline to get the user the best chance of always scrolling without seeing
 * "blank rows". The negative number is to give the bottom of the browser window a bit of invisible space to
 * keep around 5 rows rendering below it. All other DOM elements are replaced with their "blank"
 * rows.
 */

var BOTTOM_OFFSET = -500;
/**
 * This is missing the height props intentionally for performance reasons that
 * you can see here:
 * https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291
 *
 * We inline the height style for performance reasons directly on the component.
 */

var EmptyRow = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.euiColorLightestShade;
}, function (props) {
  return "1px solid ".concat(props.theme.eui.euiColorLightShade);
});

exports.EmptyRow = EmptyRow;

var StatefulEvent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(StatefulEvent, _React$Component);

  function StatefulEvent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, StatefulEvent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(StatefulEvent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "_isMounted", false);

    _defineProperty(_assertThisInitialized(_this), "state", {
      expanded: {},
      showNotes: {},
      initialRender: false
    });

    _defineProperty(_assertThisInitialized(_this), "divElement", null);

    _defineProperty(_assertThisInitialized(_this), "onToggleShowNotes", function (eventId) {
      return function () {
        _this.setState(function (state) {
          return {
            showNotes: _objectSpread({}, state.showNotes, _defineProperty({}, eventId, !state.showNotes[eventId]))
          };
        });
      };
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleExpanded", function (eventId) {
      return function () {
        _this.setState(function (state) {
          return {
            expanded: _objectSpread({}, state.expanded, _defineProperty({}, eventId, !state.expanded[eventId]))
          };
        });
      };
    });

    _defineProperty(_assertThisInitialized(_this), "associateNote", function (eventId, addNoteToEvent, onPinEvent) {
      return function (noteId) {
        addNoteToEvent({
          eventId: eventId,
          noteId: noteId
        });

        if (!(0, _helpers2.eventIsPinned)({
          eventId: eventId,
          pinnedEventIds: _this.props.pinnedEventIds
        })) {
          onPinEvent(eventId); // pin the event, because it has notes
        }
      };
    });

    return _this;
  }

  _createClass(StatefulEvent, [{
    key: "componentDidMount",

    /**
     * Incrementally loads the events when it mounts by trying to
     * see if it resides within a window frame and if it is it will
     * indicate to React that it should render its self by setting
     * its initialRender to true.
     */
    value: function componentDidMount() {
      var _this2 = this;

      this._isMounted = true;
      (0, _scheduler.requestIdleCallbackViaScheduler)(function () {
        if (!_this2.state.initialRender && _this2._isMounted) {
          _this2.setState({
            initialRender: true
          });
        }
      }, {
        timeout: this.props.maxDelay ? this.props.maxDelay : 0
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      this._isMounted = false;
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var _this$props = this.props,
          actionsColumnWidth = _this$props.actionsColumnWidth,
          addNoteToEvent = _this$props.addNoteToEvent,
          browserFields = _this$props.browserFields,
          columnHeaders = _this$props.columnHeaders,
          columnRenderers = _this$props.columnRenderers,
          event = _this$props.event,
          eventIdToNoteIds = _this$props.eventIdToNoteIds,
          getNotesByIds = _this$props.getNotesByIds,
          _this$props$isEventVi = _this$props.isEventViewer,
          isEventViewer = _this$props$isEventVi === void 0 ? false : _this$props$isEventVi,
          onColumnResized = _this$props.onColumnResized,
          onPinEvent = _this$props.onPinEvent,
          onUpdateColumns = _this$props.onUpdateColumns,
          onUnPinEvent = _this$props.onUnPinEvent,
          pinnedEventIds = _this$props.pinnedEventIds,
          rowRenderers = _this$props.rowRenderers,
          timelineId = _this$props.timelineId,
          toggleColumn = _this$props.toggleColumn,
          updateNote = _this$props.updateNote; // If we are not ready to render yet, just return null
      // see componentDidMount() for when it schedules the first
      // time this stateful component should be rendered.

      if (!this.state.initialRender) {
        // height is being inlined directly in here because of performance with StyledComponents
        // involving quick and constant changes to the DOM.
        // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291
        return React.createElement(EmptyRow, {
          style: {
            height: DEFAULT_ROW_HEIGHT
          }
        });
      }

      return React.createElement(_reactVisibilitySensor.default, {
        partialVisibility: true,
        scrollCheck: true,
        offset: {
          top: TOP_OFFSET,
          bottom: BOTTOM_OFFSET
        }
      }, function (_ref) {
        var isVisible = _ref.isVisible;

        if (isVisible) {
          return React.createElement(_details.TimelineDetailsComponentQuery, {
            sourceId: "default",
            indexName: event._index,
            eventId: event._id,
            executeQuery: !!_this3.state.expanded[event._id]
          }, function (_ref2) {
            var detailsData = _ref2.detailsData,
                loading = _ref2.loading;
            return React.createElement("div", {
              className: _helpers.STATEFUL_EVENT_CSS_CLASS_NAME,
              "data-test-subj": "event",
              ref: function ref(divElement) {
                if (divElement != null) {
                  _this3.divElement = divElement;
                }
              }
            }, (0, _get_row_renderer.getRowRenderer)(event.ecs, rowRenderers).renderRow({
              browserFields: browserFields,
              data: event.ecs,
              children: React.createElement(_stateful_event_child.StatefulEventChild, {
                id: event._id,
                actionsColumnWidth: actionsColumnWidth,
                associateNote: _this3.associateNote,
                addNoteToEvent: addNoteToEvent,
                onPinEvent: onPinEvent,
                columnHeaders: columnHeaders,
                columnRenderers: columnRenderers,
                expanded: !!_this3.state.expanded[event._id],
                data: event.data,
                eventIdToNoteIds: eventIdToNoteIds,
                getNotesByIds: getNotesByIds,
                isEventViewer: isEventViewer,
                loading: loading,
                onColumnResized: onColumnResized,
                onToggleExpanded: _this3.onToggleExpanded,
                onUnPinEvent: onUnPinEvent,
                pinnedEventIds: pinnedEventIds,
                showNotes: !!_this3.state.showNotes[event._id],
                timelineId: timelineId,
                onToggleShowNotes: _this3.onToggleShowNotes,
                updateNote: updateNote
              }),
              timelineId: timelineId
            }), React.createElement(_eui.EuiFlexItem, {
              "data-test-subj": "event-details",
              grow: true
            }, React.createElement(_expandable_event.ExpandableEvent, {
              browserFields: browserFields,
              columnHeaders: columnHeaders,
              id: event._id,
              event: detailsData || emptyDetails,
              forceExpand: !!_this3.state.expanded[event._id] && !loading,
              onUpdateColumns: onUpdateColumns,
              timelineId: timelineId,
              toggleColumn: toggleColumn
            })));
          });
        } else {
          // Height place holder for visibility detection as well as re-rendering sections.
          var height = _this3.divElement != null ? "".concat(_this3.divElement.clientHeight - EMPTY_ROW_MARGIN_TOP_BOTTOM, "px") : DEFAULT_ROW_HEIGHT; // height is being inlined directly in here because of performance with StyledComponents
          // involving quick and constant changes to the DOM.
          // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291

          return React.createElement(EmptyRow, {
            style: {
              height: height
            }
          });
        }
      });
    }
  }]);

  return StatefulEvent;
}(React.Component);

exports.StatefulEvent = StatefulEvent;