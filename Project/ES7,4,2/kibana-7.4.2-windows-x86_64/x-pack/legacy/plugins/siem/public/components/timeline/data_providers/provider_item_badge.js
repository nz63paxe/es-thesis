"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProviderItemBadge = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireWildcard(require("react"));

var _provider_badge = require("./provider_badge");

var _provider_item_actions = require("./provider_item_actions");

var _timeline_context = require("../timeline_context");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ProviderItemBadge =
/*#__PURE__*/
function (_PureComponent) {
  _inherits(ProviderItemBadge, _PureComponent);

  function ProviderItemBadge() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ProviderItemBadge);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ProviderItemBadge)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isPopoverOpen: false
    });

    _defineProperty(_assertThisInitialized(_this), "togglePopover", function () {
      _this.setState(function (prevState) {
        return {
          isPopoverOpen: !prevState.isPopoverOpen
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closePopover", function () {
      _this.setState({
        isPopoverOpen: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggleEnabledProvider", function () {
      _this.props.toggleEnabledProvider();

      _this.closePopover();
    });

    _defineProperty(_assertThisInitialized(_this), "toggleExcludedProvider", function () {
      _this.props.toggleExcludedProvider();

      _this.closePopover();
    });

    return _this;
  }

  _createClass(ProviderItemBadge, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          andProviderId = _this$props.andProviderId,
          browserFields = _this$props.browserFields,
          deleteProvider = _this$props.deleteProvider,
          field = _this$props.field,
          kqlQuery = _this$props.kqlQuery,
          isEnabled = _this$props.isEnabled,
          isExcluded = _this$props.isExcluded,
          onDataProviderEdited = _this$props.onDataProviderEdited,
          operator = _this$props.operator,
          providerId = _this$props.providerId,
          timelineId = _this$props.timelineId,
          val = _this$props.val;
      return _react.default.createElement(_timeline_context.TimelineContext.Consumer, null, function (isLoading) {
        return _react.default.createElement(_provider_item_actions.ProviderItemActions, {
          andProviderId: andProviderId,
          browserFields: browserFields,
          button: _react.default.createElement(_provider_badge.ProviderBadge, {
            deleteProvider: !isLoading ? deleteProvider : _fp.noop,
            field: field,
            kqlQuery: kqlQuery,
            isEnabled: isEnabled,
            isExcluded: isExcluded,
            providerId: providerId,
            togglePopover: _this2.togglePopover,
            val: val,
            operator: operator
          }),
          closePopover: _this2.closePopover,
          deleteProvider: deleteProvider,
          field: field,
          kqlQuery: kqlQuery,
          isEnabled: isEnabled,
          isExcluded: isExcluded,
          isLoading: isLoading,
          isOpen: _this2.state.isPopoverOpen,
          onDataProviderEdited: onDataProviderEdited,
          operator: operator,
          providerId: providerId,
          timelineId: timelineId,
          toggleEnabledProvider: _this2.toggleEnabledProvider,
          toggleExcludedProvider: _this2.toggleExcludedProvider,
          value: val
        });
      });
    }
  }]);

  return ProviderItemBadge;
}(_react.PureComponent);

exports.ProviderItemBadge = ProviderItemBadge;