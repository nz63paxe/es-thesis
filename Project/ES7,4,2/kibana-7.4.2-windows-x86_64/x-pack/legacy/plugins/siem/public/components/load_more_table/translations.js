"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ROWS = exports.SHOWING = exports.LOAD_MORE = exports.LOADING = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LOADING = _i18n.i18n.translate('xpack.siem.loadMoreTable.loadingButtonLabel', {
  defaultMessage: 'Loading…'
});

exports.LOADING = LOADING;

var LOAD_MORE = _i18n.i18n.translate('xpack.siem.loadMoreTable.loadMoreButtonLabel', {
  defaultMessage: 'Load more'
});

exports.LOAD_MORE = LOAD_MORE;

var SHOWING = _i18n.i18n.translate('xpack.siem.loadMoreTable.showingSubtitle', {
  defaultMessage: 'Showing'
});

exports.SHOWING = SHOWING;

var ROWS = _i18n.i18n.translate('xpack.siem.loadMoreTable.rowsButtonLabel', {
  defaultMessage: 'Rows per page'
});

exports.ROWS = ROWS;