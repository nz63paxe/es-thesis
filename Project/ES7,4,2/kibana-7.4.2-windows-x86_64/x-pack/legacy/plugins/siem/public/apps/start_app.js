"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SiemRootController = void 0;

var _history = require("history");

var _react = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _reactDom = require("react-dom");

var _reactRedux = require("react-redux");

var _styledComponents = require("styled-components");

var _eui = require("@elastic/eui");

var _eui_theme_dark = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_dark.json"));

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _i18n = require("ui/i18n");

var _constants = require("../../common/constants");

var _error_toast_dispatcher = require("../components/error_toast_dispatcher");

var _kibana_compose = require("../lib/compose/kibana_compose");

var _routes = require("../routes");

var _store = require("../store/store");

var _toasters = require("../components/toasters");

var _ml_capabilities_provider = require("../components/ml/permissions/ml_capabilities_provider");

var _use_kibana_ui_setting = require("../lib/settings/use_kibana_ui_setting");

var _apollo_context = require("../utils/apollo_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var StartApp = (0, _react.memo)(function (libs) {
  var history = (0, _history.createHashHistory)();
  var libs$ = new _rxjs.BehaviorSubject(libs);
  var store = (0, _store.createStore)(undefined, libs$.pipe((0, _operators.pluck)('apolloClient')));
  var AppPluginRoot = (0, _react.memo)(function () {
    var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_DARK_MODE),
        _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
        darkMode = _useKibanaUiSetting2[0];

    return _react.default.createElement(_eui.EuiErrorBoundary, null, _react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_toasters.ManageGlobalToaster, null, _react.default.createElement(_reactRedux.Provider, {
      store: store
    }, _react.default.createElement(_reactApollo.ApolloProvider, {
      client: libs.apolloClient
    }, _react.default.createElement(_apollo_context.ApolloClientContext.Provider, {
      value: libs.apolloClient
    }, _react.default.createElement(_styledComponents.ThemeProvider, {
      theme: function theme() {
        return {
          eui: darkMode ? _eui_theme_dark.default : _eui_theme_light.default,
          darkMode: darkMode
        };
      }
    }, _react.default.createElement(_ml_capabilities_provider.MlCapabilitiesProvider, null, _react.default.createElement(_routes.PageRouter, {
      history: history
    }))), _react.default.createElement(_error_toast_dispatcher.ErrorToastDispatcher, null), _react.default.createElement(_toasters.GlobalToaster, null)))))));
  });
  return _react.default.createElement(AppPluginRoot, null);
});
var ROOT_ELEMENT_ID = 'react-siem-root';
var App = (0, _react.memo)(function () {
  return _react.default.createElement("div", {
    id: ROOT_ELEMENT_ID
  }, _react.default.createElement(StartApp, (0, _kibana_compose.compose)()));
}); // eslint-disable-next-line @typescript-eslint/no-explicit-any

var SiemRootController = function SiemRootController($scope, $element) {
  var domNode = $element[0];
  (0, _reactDom.render)(_react.default.createElement(App, null), domNode);
  $scope.$on('$destroy', function () {
    (0, _reactDom.unmountComponentAtNode)(domNode);
  });
};

exports.SiemRootController = SiemRootController;