"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCategoryColumns = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("../event_details/helpers");

var _with_hover_actions = require("../with_hover_actions");

var i18n = _interopRequireWildcard(require("./translations"));

var _page = require("../page");

var _helpers2 = require("./helpers");

var _timeline_context = require("../timeline/timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n  .euiLink {\n    width: 100%;\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  cursor: pointer;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  cursor: default;\n  left: 5px;\n  padding: 8px;\n  position: absolute;\n  top: -8px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-weight: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CategoryName = _styledComponents.default.span(_templateObject(), function (_ref) {
  var bold = _ref.bold;
  return bold ? 'bold' : 'normal';
});

CategoryName.displayName = 'CategoryName';
var HoverActionsContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject2());
HoverActionsContainer.displayName = 'HoverActionsContainer';
var HoverActionsFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject3());
HoverActionsFlexGroup.displayName = 'HoverActionsFlexGroup';

var LinkContainer = _styledComponents.default.div(_templateObject4());

LinkContainer.displayName = 'LinkContainer';
var ToolTip = React.memo(function (_ref2) {
  var categoryId = _ref2.categoryId,
      browserFields = _ref2.browserFields,
      onUpdateColumns = _ref2.onUpdateColumns;
  var isLoading = (0, React.useContext)(_timeline_context.TimelineContext);
  return React.createElement(_eui.EuiToolTip, {
    content: i18n.VIEW_CATEGORY(categoryId)
  }, !isLoading ? React.createElement(_eui.EuiIcon, {
    "aria-label": i18n.VIEW_CATEGORY(categoryId),
    color: "text",
    onClick: function onClick() {
      onUpdateColumns((0, _helpers.getColumnsWithTimestamp)({
        browserFields: browserFields,
        category: categoryId
      }));
    },
    type: "visTable"
  }) : React.createElement(_helpers2.LoadingSpinner, {
    size: "m"
  }));
});
ToolTip.displayName = 'ToolTip';
/**
 * Returns the column definition for the (single) column that displays all the
 * category names in the field browser */

var getCategoryColumns = function getCategoryColumns(_ref3) {
  var browserFields = _ref3.browserFields,
      filteredBrowserFields = _ref3.filteredBrowserFields,
      onCategorySelected = _ref3.onCategorySelected,
      onUpdateColumns = _ref3.onUpdateColumns,
      selectedCategoryId = _ref3.selectedCategoryId,
      timelineId = _ref3.timelineId;
  return [{
    field: 'categoryId',
    name: '',
    sortable: true,
    truncateText: false,
    render: function render(categoryId) {
      return React.createElement(LinkContainer, null, React.createElement(_eui.EuiLink, {
        "data-test-subj": "category-link",
        onClick: function onClick() {
          return onCategorySelected(categoryId);
        }
      }, React.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_with_hover_actions.WithHoverActions, {
        hoverContent: React.createElement(HoverActionsContainer, {
          "data-test-subj": "hover-actions-container",
          paddingSize: "none"
        }, React.createElement(HoverActionsFlexGroup, {
          alignItems: "center",
          direction: "row",
          gutterSize: "none",
          justifyContent: "spaceBetween"
        }, React.createElement(_eui.EuiFlexItem, {
          grow: false
        }, React.createElement(ToolTip, {
          categoryId: categoryId,
          browserFields: browserFields,
          onUpdateColumns: onUpdateColumns
        })))),
        render: function render() {
          return React.createElement(CategoryName, {
            bold: categoryId === selectedCategoryId,
            className: (0, _helpers2.getCategoryPaneCategoryClassName)({
              categoryId: categoryId,
              timelineId: timelineId
            })
          }, React.createElement(_eui.EuiText, {
            size: "xs"
          }, categoryId));
        }
      })), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_page.CountBadge, {
        "data-test-subj": "".concat(categoryId, "-category-count"),
        color: "hollow"
      }, (0, _helpers2.getFieldCount)(filteredBrowserFields[categoryId]))))));
    }
  }];
};

exports.getCategoryColumns = getCategoryColumns;