"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reducer = exports.initialState = void 0;

var _redux = require("redux");

var _app = require("./app");

var _drag_and_drop = require("./drag_and_drop");

var _hosts = require("./hosts");

var _inputs = require("./inputs");

var _network = require("./network");

var _reducer = require("./timeline/reducer");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialState = {
  app: _app.initialAppState,
  dragAndDrop: _drag_and_drop.initialDragAndDropState,
  hosts: _hosts.initialHostsState,
  inputs: _inputs.initialInputsState,
  network: _network.initialNetworkState,
  timeline: _reducer.initialTimelineState
};
exports.initialState = initialState;
var reducer = (0, _redux.combineReducers)({
  app: _app.appReducer,
  dragAndDrop: _drag_and_drop.dragAndDropReducer,
  hosts: _hosts.hostsReducer,
  inputs: _inputs.inputsReducer,
  network: _network.networkReducer,
  timeline: _reducer.timelineReducer
});
exports.reducer = reducer;