"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLineLayer = exports.getDestinationLayer = exports.getSourceLayer = exports.getLayerList = void 0;

var _uuid = _interopRequireDefault(require("uuid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

/**
 * Returns `Source/Destination Point-to-point` Map LayerList configuration, with a source,
 * destination, and line layer for each of the provided indexPatterns
 *
 * @param indexPatternIds array of indexPatterns' title and id
 */
var getLayerList = function getLayerList(indexPatternIds) {
  return [{
    sourceDescriptor: {
      type: 'EMS_TMS',
      isAutoSelect: true
    },
    id: _uuid.default.v4(),
    label: null,
    minZoom: 0,
    maxZoom: 24,
    alpha: 1,
    visible: true,
    applyGlobalQuery: true,
    style: {
      type: 'TILE',
      properties: {}
    },
    type: 'TILE'
  }].concat(_toConsumableArray(indexPatternIds.reduce(function (acc, _ref) {
    var title = _ref.title,
        id = _ref.id;
    return [].concat(_toConsumableArray(acc), [getLineLayer(title, id), getDestinationLayer(title, id), getSourceLayer(title, id)]);
  }, [])));
};
/**
 * Returns Document Data Source layer configuration ('source.geo.location') for the given
 * indexPattern title/id
 *
 * @param indexPatternTitle used as layer name in LayerToC UI: "${indexPatternTitle} | Source point"
 * @param indexPatternId used as layer's indexPattern to query for data
 */


exports.getLayerList = getLayerList;

var getSourceLayer = function getSourceLayer(indexPatternTitle, indexPatternId) {
  return {
    sourceDescriptor: {
      id: _uuid.default.v4(),
      type: 'ES_SEARCH',
      geoField: 'source.geo.location',
      filterByMapBounds: false,
      tooltipProperties: ['host.name', 'source.ip', 'source.domain', 'source.as.organization.name'],
      useTopHits: false,
      topHitsTimeField: '@timestamp',
      topHitsSize: 1,
      indexPatternId: indexPatternId
    },
    style: {
      type: 'VECTOR',
      properties: {
        fillColor: {
          type: 'STATIC',
          options: {
            color: '#3185FC'
          }
        },
        lineColor: {
          type: 'STATIC',
          options: {
            color: '#FFFFFF'
          }
        },
        lineWidth: {
          type: 'STATIC',
          options: {
            size: 1
          }
        },
        iconSize: {
          type: 'STATIC',
          options: {
            size: 6
          }
        },
        iconOrientation: {
          type: 'STATIC',
          options: {
            orientation: 0
          }
        },
        symbol: {
          options: {
            symbolizeAs: 'circle',
            symbolId: 'arrow-es'
          }
        }
      }
    },
    id: _uuid.default.v4(),
    label: "".concat(indexPatternTitle, " | Source Point"),
    minZoom: 0,
    maxZoom: 24,
    alpha: 0.75,
    visible: true,
    applyGlobalQuery: true,
    type: 'VECTOR',
    query: {
      query: '',
      language: 'kuery'
    },
    joins: []
  };
};
/**
 * Returns Document Data Source layer configuration ('destination.geo.location') for the given
 * indexPattern title/id
 *
 * @param indexPatternTitle used as layer name in LayerToC UI: "${indexPatternTitle} | Destination point"
 * @param indexPatternId used as layer's indexPattern to query for data
 */


exports.getSourceLayer = getSourceLayer;

var getDestinationLayer = function getDestinationLayer(indexPatternTitle, indexPatternId) {
  return {
    sourceDescriptor: {
      id: _uuid.default.v4(),
      type: 'ES_SEARCH',
      geoField: 'destination.geo.location',
      filterByMapBounds: true,
      tooltipProperties: ['host.name', 'destination.ip', 'destination.domain', 'destination.as.organization.name'],
      useTopHits: false,
      topHitsTimeField: '@timestamp',
      topHitsSize: 1,
      indexPatternId: indexPatternId
    },
    style: {
      type: 'VECTOR',
      properties: {
        fillColor: {
          type: 'STATIC',
          options: {
            color: '#DB1374'
          }
        },
        lineColor: {
          type: 'STATIC',
          options: {
            color: '#FFFFFF'
          }
        },
        lineWidth: {
          type: 'STATIC',
          options: {
            size: 1
          }
        },
        iconSize: {
          type: 'STATIC',
          options: {
            size: 6
          }
        },
        iconOrientation: {
          type: 'STATIC',
          options: {
            orientation: 0
          }
        },
        symbol: {
          options: {
            symbolizeAs: 'circle',
            symbolId: 'airfield'
          }
        }
      }
    },
    id: _uuid.default.v4(),
    label: "".concat(indexPatternTitle, " | Destination Point"),
    minZoom: 0,
    maxZoom: 24,
    alpha: 0.75,
    visible: true,
    applyGlobalQuery: true,
    type: 'VECTOR',
    query: {
      query: '',
      language: 'kuery'
    }
  };
};
/**
 * Returns Point-to-point Data Source layer configuration ('source.geo.location' &
 * 'source.geo.location') for the given indexPattern title/id
 *
 * @param indexPatternTitle used as layer name in LayerToC UI: "${indexPatternTitle} | Line"
 * @param indexPatternId used as layer's indexPattern to query for data
 */


exports.getDestinationLayer = getDestinationLayer;

var getLineLayer = function getLineLayer(indexPatternTitle, indexPatternId) {
  return {
    sourceDescriptor: {
      type: 'ES_PEW_PEW',
      id: _uuid.default.v4(),
      indexPatternId: indexPatternId,
      sourceGeoField: 'source.geo.location',
      destGeoField: 'destination.geo.location',
      metrics: [{
        type: 'sum',
        field: 'source.bytes',
        label: 'Total Src Bytes'
      }, {
        type: 'sum',
        field: 'destination.bytes',
        label: 'Total Dest Bytes'
      }, {
        type: 'count',
        label: 'Total Documents'
      }]
    },
    style: {
      type: 'VECTOR',
      properties: {
        fillColor: {
          type: 'STATIC',
          options: {
            color: '#e6194b'
          }
        },
        lineColor: {
          type: 'DYNAMIC',
          options: {
            color: 'Green to Red',
            field: {
              label: 'count',
              name: 'doc_count',
              origin: 'source'
            },
            useCustomColorRamp: false
          }
        },
        lineWidth: {
          type: 'DYNAMIC',
          options: {
            minSize: 1,
            maxSize: 4,
            field: {
              label: 'count',
              name: 'doc_count',
              origin: 'source'
            }
          }
        },
        iconSize: {
          type: 'STATIC',
          options: {
            size: 10
          }
        },
        iconOrientation: {
          type: 'STATIC',
          options: {
            orientation: 0
          }
        },
        symbol: {
          options: {
            symbolizeAs: 'circle',
            symbolId: 'airfield'
          }
        }
      }
    },
    id: _uuid.default.v4(),
    label: "".concat(indexPatternTitle, " | Line"),
    minZoom: 0,
    maxZoom: 24,
    alpha: 1,
    visible: true,
    applyGlobalQuery: true,
    type: 'VECTOR',
    query: {
      query: '',
      language: 'kuery'
    }
  };
};

exports.getLineLayer = getLineLayer;