"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverviewHostStats = exports.DescriptionListDescription = void 0;

var _eui = require("@elastic/eui");

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _react = require("@kbn/i18n/react");

var _fp = require("lodash/fp");

var _react2 = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _empty_value = require("../../../empty_value");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  text-align: right;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var overviewHostStats = function overviewHostStats(data) {
  return [{
    description: (0, _fp.has)('auditbeatAuditd', data) && data.auditbeatAuditd !== null ? (0, _numeral.default)(data.auditbeatAuditd).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatAuditTitle",
      defaultMessage: "Auditbeat Audit"
    }),
    id: 'auditbeatAuditd'
  }, {
    description: (0, _fp.has)('auditbeatFIM', data) && data.auditbeatFIM !== null ? (0, _numeral.default)(data.auditbeatFIM).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatFimTitle",
      defaultMessage: "Auditbeat File Integrity Module"
    }),
    id: 'auditbeatFIM'
  }, {
    description: (0, _fp.has)('auditbeatLogin', data) && data.auditbeatLogin !== null ? (0, _numeral.default)(data.auditbeatLogin).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatLoginTitle",
      defaultMessage: "Auditbeat Login"
    }),
    id: 'auditbeatLogin'
  }, {
    description: (0, _fp.has)('auditbeatPackage', data) && data.auditbeatPackage !== null ? (0, _numeral.default)(data.auditbeatPackage).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatPackageTitle",
      defaultMessage: "Auditbeat Package"
    }),
    id: 'auditbeatPackage'
  }, {
    description: (0, _fp.has)('auditbeatProcess', data) && data.auditbeatProcess !== null ? (0, _numeral.default)(data.auditbeatProcess).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatProcessTitle",
      defaultMessage: "Auditbeat Process"
    }),
    id: 'auditbeatProcess'
  }, {
    description: (0, _fp.has)('auditbeatUser', data) && data.auditbeatUser !== null ? (0, _numeral.default)(data.auditbeatUser).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatUserTitle",
      defaultMessage: "Auditbeat User"
    }),
    id: 'auditbeatUser'
  }, {
    description: (0, _fp.has)('filebeatSystemModule', data) && data.filebeatSystemModule !== null ? (0, _numeral.default)(data.filebeatSystemModule).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.filebeatSystemModuleTitle",
      defaultMessage: "Filebeat System Module"
    }),
    id: 'filebeatSystemModule'
  }, {
    description: (0, _fp.has)('winlogbeat', data) && data.winlogbeat !== null ? (0, _numeral.default)(data.winlogbeat).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.winlogbeatTitle",
      defaultMessage: "Winlogbeat"
    }),
    id: 'winlogbeat'
  }];
};

var DescriptionListDescription = (0, _styledComponents.default)(_eui.EuiDescriptionListDescription)(_templateObject());
exports.DescriptionListDescription = DescriptionListDescription;
DescriptionListDescription.displayName = 'DescriptionListDescription';
var StatValue = (0, _recompose.pure)(function (_ref) {
  var isLoading = _ref.isLoading,
      value = _ref.value;
  return _react2.default.createElement(_react2.default.Fragment, null, isLoading ? _react2.default.createElement(_eui.EuiLoadingSpinner, {
    size: "m"
  }) : value != null ? value : (0, _empty_value.getEmptyTagValue)());
});
StatValue.displayName = 'StatValue';
var OverviewHostStats = (0, _recompose.pure)(function (_ref2) {
  var data = _ref2.data,
      loading = _ref2.loading;
  return _react2.default.createElement(_eui.EuiDescriptionList, {
    type: "column"
  }, overviewHostStats(data).map(function (item, index) {
    return _react2.default.createElement(_react2.default.Fragment, {
      key: index
    }, _react2.default.createElement(_eui.EuiDescriptionListTitle, null, item.title), _react2.default.createElement(DescriptionListDescription, {
      "data-test-subj": "host-stat-".concat(item.id)
    }, _react2.default.createElement(StatValue, {
      isLoading: loading,
      value: item.description
    })));
  }));
});
exports.OverviewHostStats = OverviewHostStats;
OverviewHostStats.displayName = 'OverviewHostStats';