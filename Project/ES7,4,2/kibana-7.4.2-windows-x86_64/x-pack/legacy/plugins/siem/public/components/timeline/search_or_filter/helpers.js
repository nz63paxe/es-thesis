"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPlaceholderText = exports.options = exports.modes = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _and_or_badge = require("../../and_or_badge");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: -1px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var AndOrContainer = _styledComponents.default.div(_templateObject());

AndOrContainer.displayName = 'AndOrContainer';
var modes = {
  filter: {
    mode: 'filter',
    description: i18n.FILTER_DESCRIPTION,
    kqlBarTooltip: i18n.FILTER_KQL_TOOLTIP,
    placeholder: i18n.FILTER_KQL_PLACEHOLDER,
    selectText: i18n.FILTER_KQL_SELECTED_TEXT
  },
  search: {
    mode: 'search',
    description: i18n.SEARCH_DESCRIPTION,
    kqlBarTooltip: i18n.SEARCH_KQL_TOOLTIP,
    placeholder: i18n.SEARCH_KQL_PLACEHOLDER,
    selectText: i18n.SEARCH_KQL_SELECTED_TEXT
  }
};
exports.modes = modes;
var options = [{
  value: modes.filter.mode,
  inputDisplay: React.createElement(AndOrContainer, null, React.createElement(_and_or_badge.AndOrBadge, {
    type: "and"
  }), modes.filter.selectText),
  dropdownDisplay: React.createElement(React.Fragment, null, React.createElement(_and_or_badge.AndOrBadge, {
    type: "and"
  }), React.createElement("strong", null, modes.filter.selectText), React.createElement(_eui.EuiSpacer, {
    size: "xs"
  }), React.createElement(_eui.EuiText, {
    size: "s",
    color: "subdued"
  }, React.createElement("p", {
    className: "euiTextColor--subdued"
  }, modes.filter.description)))
}, {
  value: modes.search.mode,
  inputDisplay: React.createElement(AndOrContainer, null, React.createElement(_and_or_badge.AndOrBadge, {
    type: "or"
  }), modes.search.selectText),
  dropdownDisplay: React.createElement(React.Fragment, null, React.createElement(_and_or_badge.AndOrBadge, {
    type: "or"
  }), React.createElement("strong", null, modes.search.selectText), React.createElement(_eui.EuiSpacer, {
    size: "xs"
  }), React.createElement(_eui.EuiText, {
    size: "s",
    color: "subdued"
  }, React.createElement("p", {
    className: "euiTextColor--subdued"
  }, modes.search.description)))
}];
exports.options = options;

var getPlaceholderText = function getPlaceholderText(kqlMode) {
  return kqlMode === 'filter' ? i18n.FILTER_KQL_PLACEHOLDER : i18n.SEARCH_KQL_PLACEHOLDER;
};

exports.getPlaceholderText = getPlaceholderText;