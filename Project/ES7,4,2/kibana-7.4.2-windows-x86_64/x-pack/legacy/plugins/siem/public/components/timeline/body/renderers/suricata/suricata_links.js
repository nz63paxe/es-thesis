"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBeginningTokens = exports.getLinksFromSignature = void 0;

var _fp = require("lodash/fp");

var _suricataSidDb = require("suricata-sid-db");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getLinksFromSignature = function getLinksFromSignature(id) {
  var refs = _suricataSidDb.db[id];

  if (refs != null) {
    return (0, _fp.uniq)(refs);
  } else {
    return [];
  }
};

exports.getLinksFromSignature = getLinksFromSignature;
var specialTokenRules = ['IPv4', 'IPv6'];

var getBeginningTokens = function getBeginningTokens(signature) {
  var signatureSplit = signature.trim().split(' ');
  return signatureSplit.reduce(function (accum, curr, index) {
    if (accum.length === index && curr === curr.toUpperCase() && curr !== '' || specialTokenRules.includes(curr)) {
      accum = accum.concat(curr);
    }

    return accum;
  }, []);
};

exports.getBeginningTokens = getBeginningTokens;