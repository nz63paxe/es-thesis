"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isKqlForRoute = exports.getCurrentLocation = exports.getTitle = exports.getUrlType = exports.replaceQueryStringInLocation = exports.replaceStateKeyInQueryString = exports.getParamFromQueryString = exports.getQueryStringFromLocation = exports.encodeRisonUrlState = exports.decodeRisonUrlState = void 0;

var _risonNode = require("rison-node");

var _query_string = require("ui/utils/query_string");

var _home_navigations = require("../../pages/home/home_navigations");

var _constants = require("./constants");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// eslint-disable-next-line @typescript-eslint/no-explicit-any
var decodeRisonUrlState = function decodeRisonUrlState(value) {
  try {
    return value ? (0, _risonNode.decode)(value) : undefined;
  } catch (error) {
    if (error instanceof Error && error.message.startsWith('rison decoder error')) {
      return {};
    }

    throw error;
  }
}; // eslint-disable-next-line @typescript-eslint/no-explicit-any


exports.decodeRisonUrlState = decodeRisonUrlState;

var encodeRisonUrlState = function encodeRisonUrlState(state) {
  return (0, _risonNode.encode)(state);
};

exports.encodeRisonUrlState = encodeRisonUrlState;

var getQueryStringFromLocation = function getQueryStringFromLocation(location) {
  return location.search.substring(1);
};

exports.getQueryStringFromLocation = getQueryStringFromLocation;

var getParamFromQueryString = function getParamFromQueryString(queryString, key) {
  var queryParam = _query_string.QueryString.decode(queryString)[key];

  return Array.isArray(queryParam) ? queryParam[0] : queryParam;
}; // eslint-disable-next-line @typescript-eslint/no-explicit-any


exports.getParamFromQueryString = getParamFromQueryString;

var replaceStateKeyInQueryString = function replaceStateKeyInQueryString(stateKey, urlState) {
  return function (queryString) {
    var previousQueryValues = _query_string.QueryString.decode(queryString);

    if (urlState == null || typeof urlState === 'string' && urlState === '' || urlState && urlState.filterQuery === null || urlState && urlState.filterQuery != null && urlState.filterQuery.expression === '') {
      delete previousQueryValues[stateKey];
      return _query_string.QueryString.encode(_objectSpread({}, previousQueryValues));
    } // ಠ_ಠ Code was copied from x-pack/legacy/plugins/infra/public/utils/url_state.tsx ಠ_ಠ
    // Remove this if these utilities are promoted to kibana core


    var encodedUrlState = typeof urlState !== 'undefined' ? encodeRisonUrlState(urlState) : undefined;
    return _query_string.QueryString.encode(_objectSpread({}, previousQueryValues, _defineProperty({}, stateKey, encodedUrlState)));
  };
};

exports.replaceStateKeyInQueryString = replaceStateKeyInQueryString;

var replaceQueryStringInLocation = function replaceQueryStringInLocation(location, queryString) {
  if (queryString === getQueryStringFromLocation(location)) {
    return location;
  } else {
    return _objectSpread({}, location, {
      search: "?".concat(queryString)
    });
  }
};

exports.replaceQueryStringInLocation = replaceQueryStringInLocation;

var getUrlType = function getUrlType(pageName) {
  if (pageName === _home_navigations.SiemPageName.hosts) {
    return 'host';
  } else if (pageName === _home_navigations.SiemPageName.network) {
    return 'network';
  } else if (pageName === _home_navigations.SiemPageName.overview) {
    return 'overview';
  } else if (pageName === _home_navigations.SiemPageName.timelines) {
    return 'timeline';
  }

  return 'overview';
};

exports.getUrlType = getUrlType;

var getTitle = function getTitle(pageName, detailName, navTabs) {
  if (detailName != null) return detailName;
  return navTabs[pageName] != null ? navTabs[pageName].name : '';
};

exports.getTitle = getTitle;

var getCurrentLocation = function getCurrentLocation(pageName, detailName) {
  if (pageName === _home_navigations.SiemPageName.hosts) {
    if (detailName != null) {
      return _constants.CONSTANTS.hostsDetails;
    }

    return _constants.CONSTANTS.hostsPage;
  } else if (pageName === _home_navigations.SiemPageName.network) {
    if (detailName != null) {
      return _constants.CONSTANTS.networkDetails;
    }

    return _constants.CONSTANTS.networkPage;
  } else if (pageName === _home_navigations.SiemPageName.overview) {
    return _constants.CONSTANTS.overviewPage;
  } else if (pageName === _home_navigations.SiemPageName.timelines) {
    return _constants.CONSTANTS.timelinePage;
  }

  return _constants.CONSTANTS.unknown;
};

exports.getCurrentLocation = getCurrentLocation;

var isKqlForRoute = function isKqlForRoute(pageName, detailName) {
  var queryLocation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var currentLocation = getCurrentLocation(pageName, detailName);

  if (currentLocation === _constants.CONSTANTS.hostsPage && queryLocation === _constants.CONSTANTS.hostsPage || currentLocation === _constants.CONSTANTS.networkPage && queryLocation === _constants.CONSTANTS.networkPage || currentLocation === _constants.CONSTANTS.hostsDetails && queryLocation === _constants.CONSTANTS.hostsDetails || currentLocation === _constants.CONSTANTS.networkDetails && queryLocation === _constants.CONSTANTS.networkDetails) {
    return true;
  }

  return false;
};

exports.isKqlForRoute = isKqlForRoute;