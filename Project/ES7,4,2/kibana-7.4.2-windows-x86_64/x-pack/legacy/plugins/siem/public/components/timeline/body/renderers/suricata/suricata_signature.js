"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuricataSignature = exports.DraggableSignatureId = exports.Tokens = exports.SURICATA_SIGNATURE_ID_FIELD_NAME = exports.SURICATA_SIGNATURE_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggable_wrapper = require("../../../../drag_and_drop/draggable_wrapper");

var _helpers = require("../../../../drag_and_drop/helpers");

var _external_link_icon = require("../../../../external_link_icon");

var _links = require("../../../../links");

var _provider = require("../../../../timeline/data_providers/provider");

var _helpers2 = require("../helpers");

var _suricata_links = require("./suricata_links");

var _draggables = require("../../../../draggables");

var _data_provider = require("../../../data_providers/data_provider");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 6px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  min-width: 77px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SURICATA_SIGNATURE_FIELD_NAME = 'suricata.eve.alert.signature';
exports.SURICATA_SIGNATURE_FIELD_NAME = SURICATA_SIGNATURE_FIELD_NAME;
var SURICATA_SIGNATURE_ID_FIELD_NAME = 'suricata.eve.alert.signature_id';
exports.SURICATA_SIGNATURE_ID_FIELD_NAME = SURICATA_SIGNATURE_ID_FIELD_NAME;
var SignatureFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
SignatureFlexItem.displayName = 'SignatureFlexItem'; // Ref: https://github.com/elastic/eui/issues/1655
// const Badge = styled(EuiBadge)`
//   vertical-align: top;
// `;

var Badge = function Badge(props) {
  return React.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      verticalAlign: 'top'
    }
  }));
};

Badge.displayName = 'Badge';
var LinkFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
LinkFlexItem.displayName = 'LinkFlexItem';
var Tokens = (0, _recompose.pure)(function (_ref) {
  var tokens = _ref.tokens;
  return React.createElement(React.Fragment, null, tokens.map(function (token) {
    return React.createElement(_helpers2.TokensFlexItem, {
      key: token,
      grow: false
    }, React.createElement(_eui.EuiBadge, {
      iconType: "tag",
      color: "hollow"
    }, token));
  }));
});
exports.Tokens = Tokens;
Tokens.displayName = 'Tokens';
var DraggableSignatureId = (0, _recompose.pure)(function (_ref2) {
  var id = _ref2.id,
      signatureId = _ref2.signatureId;
  return React.createElement(SignatureFlexItem, {
    grow: false
  }, React.createElement(_draggable_wrapper.DraggableWrapper, {
    dataProvider: {
      and: [],
      enabled: true,
      id: (0, _helpers.escapeDataProviderId)("suricata-draggable-signature-id-".concat(id, "-sig-").concat(signatureId)),
      name: String(signatureId),
      excluded: false,
      kqlQuery: '',
      queryMatch: {
        field: SURICATA_SIGNATURE_ID_FIELD_NAME,
        value: signatureId,
        operator: _data_provider.IS_OPERATOR
      }
    },
    render: function render(dataProvider, _, snapshot) {
      return snapshot.isDragging ? React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_provider.Provider, {
        dataProvider: dataProvider
      })) : React.createElement(_eui.EuiToolTip, {
        "data-test-subj": "signature-id-tooltip",
        content: SURICATA_SIGNATURE_ID_FIELD_NAME
      }, React.createElement(Badge, {
        iconType: "number",
        color: "hollow"
      }, signatureId));
    }
  }));
});
exports.DraggableSignatureId = DraggableSignatureId;
DraggableSignatureId.displayName = 'DraggableSignatureId';
var SuricataSignature = (0, _recompose.pure)(function (_ref3) {
  var contextId = _ref3.contextId,
      id = _ref3.id,
      signature = _ref3.signature,
      signatureId = _ref3.signatureId;
  var tokens = (0, _suricata_links.getBeginningTokens)(signature);
  return React.createElement(_eui.EuiFlexGroup, {
    justifyContent: "center",
    gutterSize: "none",
    wrap: true
  }, React.createElement(DraggableSignatureId, {
    id: "draggable-signature-id-".concat(contextId, "-").concat(id),
    signatureId: signatureId
  }), React.createElement(Tokens, {
    tokens: tokens
  }), React.createElement(LinkFlexItem, {
    grow: false
  }, React.createElement(_draggables.DefaultDraggable, {
    "data-test-subj": "draggable-signature-link",
    field: SURICATA_SIGNATURE_FIELD_NAME,
    id: "suricata-signature-default-draggable-".concat(contextId, "-").concat(id, "-").concat(SURICATA_SIGNATURE_FIELD_NAME),
    name: name,
    value: signature
  }, React.createElement("div", null, React.createElement(_links.GoogleLink, {
    link: signature
  }, signature.split(' ').splice(tokens.length).join(' ')), React.createElement(_external_link_icon.ExternalLinkIcon, null)))));
});
exports.SuricataSignature = SuricataSignature;
SuricataSignature.displayName = 'SuricataSignature';