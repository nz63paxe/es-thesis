"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.plainColumnRenderer = exports.dataExistsAtColumn = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _draggable_wrapper = require("../../../drag_and_drop/draggable_wrapper");

var _helpers = require("../../../drag_and_drop/helpers");

var _empty_value = require("../../../empty_value");

var _formatted_ip = require("../../../formatted_ip");

var _data_provider = require("../../data_providers/data_provider");

var _provider = require("../../data_providers/provider");

var _formatted_field = require("./formatted_field");

var _parse_query_value = require("./parse_query_value");

var _parse_value = require("./parse_value");

var _truncatable_text = require("../../../truncatable_text");

var _constants = require("./constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var dataExistsAtColumn = function dataExistsAtColumn(columnName, data) {
  return data.findIndex(function (item) {
    return item.field === columnName;
  }) !== -1;
}; // simple black-list to prevent dragging and dropping fields such as message name


exports.dataExistsAtColumn = dataExistsAtColumn;
var columnNamesNotDraggable = [_constants.MESSAGE_FIELD_NAME];
var plainColumnRenderer = {
  isInstance: function isInstance(columnName, data) {
    return dataExistsAtColumn(columnName, data);
  },
  renderColumn: function renderColumn(_ref) {
    var columnName = _ref.columnName,
        eventId = _ref.eventId,
        values = _ref.values,
        field = _ref.field,
        width = _ref.width,
        timelineId = _ref.timelineId;
    return values != null ? values.map(function (value) {
      var itemDataProvider = {
        enabled: true,
        id: (0, _helpers.escapeDataProviderId)("plain-column-renderer-data-provider-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id, "-").concat(value)),
        name: "".concat(columnName, ": ").concat((0, _parse_query_value.parseQueryValue)(value)),
        queryMatch: {
          field: field.id,
          value: (0, _parse_query_value.parseQueryValue)(value),
          operator: _data_provider.IS_OPERATOR
        },
        excluded: false,
        kqlQuery: '',
        and: []
      };

      if (field.type === _constants.IP_FIELD_TYPE) {
        // since ip fields may contain multiple IP addresses, return a FormattedIp here to avoid a "draggable of draggables"
        return _react.default.createElement(_formatted_ip.FormattedIp, {
          contextId: "plain-column-renderer-formatted-ip-".concat(timelineId),
          eventId: eventId,
          fieldName: field.id,
          key: "plain-column-renderer-formatted-ip-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id, "-").concat(value),
          value: !(0, _fp.isNumber)(value) ? value : String(value),
          width: width
        });
      }

      if (columnNamesNotDraggable.includes(columnName)) {
        if (width != null) {
          return _react.default.createElement(_truncatable_text.TruncatableText, {
            size: "xs",
            width: width,
            key: "plain-column-renderer-truncatable-formatted-field-value-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id, "-").concat(value)
          }, _react.default.createElement(_formatted_field.FormattedFieldValue, {
            contextId: "plain-column-renderer-truncatable-formatted-field-value-".concat(timelineId),
            eventId: eventId,
            fieldFormat: field.format || '',
            fieldName: columnName,
            fieldType: field.type || '',
            value: (0, _parse_value.parseValue)(value),
            width: width
          }));
        } else {
          return _react.default.createElement(_eui.EuiText, {
            "data-test-subj": "draggable-content",
            size: "xs",
            key: "plain-column-renderer-text-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id, "-").concat(value)
          }, _react.default.createElement(_formatted_field.FormattedFieldValue, {
            contextId: "plain-column-renderer-text-formatted-field-value-".concat(timelineId),
            eventId: eventId,
            fieldFormat: field.format || '',
            fieldName: columnName,
            fieldType: field.type || '',
            value: (0, _parse_value.parseValue)(value),
            width: width
          }));
        }
      } // note: we use a raw DraggableWrapper here instead of a DefaultDraggable,
      // because we pass a width to enable text truncation, and we will show empty values


      return _react.default.createElement(_draggable_wrapper.DraggableWrapper, {
        key: "plain-column-renderer-draggable-wrapper-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id, "-").concat(value),
        dataProvider: itemDataProvider,
        render: function render(dataProvider, _, snapshot) {
          return snapshot.isDragging ? _react.default.createElement(_draggable_wrapper.DragEffects, null, _react.default.createElement(_provider.Provider, {
            dataProvider: dataProvider
          })) : _react.default.createElement(_formatted_field.FormattedFieldValue, {
            contextId: "plain-column-renderer-formatted-field-value-".concat(timelineId),
            eventId: eventId,
            fieldFormat: field.format || '',
            fieldName: columnName,
            fieldType: field.type || '',
            value: (0, _parse_value.parseValue)(value),
            width: width
          });
        },
        width: width
      });
    }) : (0, _empty_value.getEmptyTagValue)();
  }
};
exports.plainColumnRenderer = plainColumnRenderer;