"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventsTabBody = exports.AnomaliesTabBody = exports.UncommonProcessTabBody = exports.AuthenticationsQueryTabBody = exports.HostsQueryTabBody = exports.navTabsHostDetails = exports.navTabsHosts = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var i18n = _interopRequireWildcard(require("./translations"));

var _hosts = require("../../components/page/hosts");

var _hosts2 = require("../../containers/hosts");

var _authentications_table = require("../../components/page/hosts/authentications_table");

var _anomalies_host_table = require("../../components/ml/tables/anomalies_host_table");

var _uncommon_processes = require("../../containers/uncommon_processes");

var _manage_query = require("../../components/page/manage_query");

var _authentications = require("../../containers/authentications");

var _model = require("../../store/hosts/model");

var _events_viewer = require("../../components/events_viewer");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getTabsOnHostsUrl = function getTabsOnHostsUrl(tabName) {
  return "#/hosts/".concat(tabName);
};

var getTabsOnHostDetailsUrl = function getTabsOnHostDetailsUrl(hostName, tabName) {
  return "#/hosts/".concat(hostName, "/").concat(tabName);
};

var navTabsHosts = function navTabsHosts(hasMlUserPermissions) {
  var _hostsNavTabs;

  var hostsNavTabs = (_hostsNavTabs = {}, _defineProperty(_hostsNavTabs, _model.HostsTableType.hosts, {
    id: _model.HostsTableType.hosts,
    name: i18n.NAVIGATION_ALL_HOSTS_TITLE,
    href: getTabsOnHostsUrl(_model.HostsTableType.hosts),
    disabled: false,
    urlKey: 'host'
  }), _defineProperty(_hostsNavTabs, _model.HostsTableType.authentications, {
    id: _model.HostsTableType.authentications,
    name: i18n.NAVIGATION_AUTHENTICATIONS_TITLE,
    href: getTabsOnHostsUrl(_model.HostsTableType.authentications),
    disabled: false,
    urlKey: 'host'
  }), _defineProperty(_hostsNavTabs, _model.HostsTableType.uncommonProcesses, {
    id: _model.HostsTableType.uncommonProcesses,
    name: i18n.NAVIGATION_UNCOMMON_PROCESSES_TITLE,
    href: getTabsOnHostsUrl(_model.HostsTableType.uncommonProcesses),
    disabled: false,
    urlKey: 'host'
  }), _defineProperty(_hostsNavTabs, _model.HostsTableType.anomalies, {
    id: _model.HostsTableType.anomalies,
    name: i18n.NAVIGATION_ANOMALIES_TITLE,
    href: getTabsOnHostsUrl(_model.HostsTableType.anomalies),
    disabled: false,
    urlKey: 'host'
  }), _defineProperty(_hostsNavTabs, _model.HostsTableType.events, {
    id: _model.HostsTableType.events,
    name: i18n.NAVIGATION_EVENTS_TITLE,
    href: getTabsOnHostsUrl(_model.HostsTableType.events),
    disabled: false,
    urlKey: 'host'
  }), _hostsNavTabs);
  return hasMlUserPermissions ? hostsNavTabs : (0, _fp.omit)([_model.HostsTableType.anomalies], hostsNavTabs);
};

exports.navTabsHosts = navTabsHosts;

var navTabsHostDetails = function navTabsHostDetails(hostName, hasMlUserPermissions) {
  var _hostDetailsNavTabs;

  var hostDetailsNavTabs = (_hostDetailsNavTabs = {}, _defineProperty(_hostDetailsNavTabs, _model.HostsTableType.authentications, {
    id: _model.HostsTableType.authentications,
    name: i18n.NAVIGATION_AUTHENTICATIONS_TITLE,
    href: getTabsOnHostDetailsUrl(hostName, _model.HostsTableType.authentications),
    disabled: false,
    urlKey: 'host',
    isDetailPage: true
  }), _defineProperty(_hostDetailsNavTabs, _model.HostsTableType.uncommonProcesses, {
    id: _model.HostsTableType.uncommonProcesses,
    name: i18n.NAVIGATION_UNCOMMON_PROCESSES_TITLE,
    href: getTabsOnHostDetailsUrl(hostName, _model.HostsTableType.uncommonProcesses),
    disabled: false,
    urlKey: 'host',
    isDetailPage: true
  }), _defineProperty(_hostDetailsNavTabs, _model.HostsTableType.anomalies, {
    id: _model.HostsTableType.anomalies,
    name: i18n.NAVIGATION_ANOMALIES_TITLE,
    href: getTabsOnHostDetailsUrl(hostName, _model.HostsTableType.anomalies),
    disabled: false,
    urlKey: 'host',
    isDetailPage: true
  }), _defineProperty(_hostDetailsNavTabs, _model.HostsTableType.events, {
    id: _model.HostsTableType.events,
    name: i18n.NAVIGATION_EVENTS_TITLE,
    href: getTabsOnHostDetailsUrl(hostName, _model.HostsTableType.events),
    disabled: false,
    urlKey: 'host',
    isDetailPage: true
  }), _hostDetailsNavTabs);
  return hasMlUserPermissions ? hostDetailsNavTabs : (0, _fp.omit)(_model.HostsTableType.anomalies, hostDetailsNavTabs);
};

exports.navTabsHostDetails = navTabsHostDetails;
var AuthenticationTableManage = (0, _manage_query.manageQuery)(_authentications_table.AuthenticationTable);
var HostsTableManage = (0, _manage_query.manageQuery)(_hosts.HostsTable);
var UncommonProcessTableManage = (0, _manage_query.manageQuery)(_hosts.UncommonProcessTable);

var HostsQueryTabBody = function HostsQueryTabBody(_ref) {
  var deleteQuery = _ref.deleteQuery,
      endDate = _ref.endDate,
      filterQuery = _ref.filterQuery,
      indexPattern = _ref.indexPattern,
      skip = _ref.skip,
      setQuery = _ref.setQuery,
      startDate = _ref.startDate,
      type = _ref.type;
  return _react.default.createElement(_hosts2.HostsQuery, {
    endDate: endDate,
    filterQuery: filterQuery,
    skip: skip,
    sourceId: "default",
    startDate: startDate,
    type: type
  }, function (_ref2) {
    var hosts = _ref2.hosts,
        totalCount = _ref2.totalCount,
        loading = _ref2.loading,
        pageInfo = _ref2.pageInfo,
        loadPage = _ref2.loadPage,
        id = _ref2.id,
        inspect = _ref2.inspect,
        isInspected = _ref2.isInspected,
        refetch = _ref2.refetch;
    return _react.default.createElement(HostsTableManage, {
      deleteQuery: deleteQuery,
      data: hosts,
      fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
      id: id,
      indexPattern: indexPattern,
      inspect: inspect,
      isInspect: isInspected,
      loading: loading,
      loadPage: loadPage,
      refetch: refetch,
      setQuery: setQuery,
      showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
      totalCount: totalCount,
      type: type
    });
  });
};

exports.HostsQueryTabBody = HostsQueryTabBody;

var AuthenticationsQueryTabBody = function AuthenticationsQueryTabBody(_ref3) {
  var deleteQuery = _ref3.deleteQuery,
      endDate = _ref3.endDate,
      filterQuery = _ref3.filterQuery,
      skip = _ref3.skip,
      setQuery = _ref3.setQuery,
      startDate = _ref3.startDate,
      type = _ref3.type;
  return _react.default.createElement(_authentications.AuthenticationsQuery, {
    endDate: endDate,
    filterQuery: filterQuery,
    skip: skip,
    sourceId: "default",
    startDate: startDate,
    type: type
  }, function (_ref4) {
    var authentications = _ref4.authentications,
        totalCount = _ref4.totalCount,
        loading = _ref4.loading,
        pageInfo = _ref4.pageInfo,
        loadPage = _ref4.loadPage,
        id = _ref4.id,
        inspect = _ref4.inspect,
        isInspected = _ref4.isInspected,
        refetch = _ref4.refetch;
    return _react.default.createElement(AuthenticationTableManage, {
      data: authentications,
      deleteQuery: deleteQuery,
      fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
      id: id,
      inspect: inspect,
      isInspect: isInspected,
      loading: loading,
      loadPage: loadPage,
      refetch: refetch,
      showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
      setQuery: setQuery,
      totalCount: totalCount,
      type: type
    });
  });
};

exports.AuthenticationsQueryTabBody = AuthenticationsQueryTabBody;

var UncommonProcessTabBody = function UncommonProcessTabBody(_ref5) {
  var deleteQuery = _ref5.deleteQuery,
      endDate = _ref5.endDate,
      filterQuery = _ref5.filterQuery,
      skip = _ref5.skip,
      setQuery = _ref5.setQuery,
      startDate = _ref5.startDate,
      type = _ref5.type;
  return _react.default.createElement(_uncommon_processes.UncommonProcessesQuery, {
    endDate: endDate,
    filterQuery: filterQuery,
    skip: skip,
    sourceId: "default",
    startDate: startDate,
    type: type
  }, function (_ref6) {
    var uncommonProcesses = _ref6.uncommonProcesses,
        totalCount = _ref6.totalCount,
        loading = _ref6.loading,
        pageInfo = _ref6.pageInfo,
        loadPage = _ref6.loadPage,
        id = _ref6.id,
        inspect = _ref6.inspect,
        isInspected = _ref6.isInspected,
        refetch = _ref6.refetch;
    return _react.default.createElement(UncommonProcessTableManage, {
      deleteQuery: deleteQuery,
      data: uncommonProcesses,
      fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
      id: id,
      inspect: inspect,
      isInspect: isInspected,
      loading: loading,
      loadPage: loadPage,
      refetch: refetch,
      setQuery: setQuery,
      showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
      totalCount: totalCount,
      type: type
    });
  });
};

exports.UncommonProcessTabBody = UncommonProcessTabBody;

var AnomaliesTabBody = function AnomaliesTabBody(_ref7) {
  var endDate = _ref7.endDate,
      skip = _ref7.skip,
      startDate = _ref7.startDate,
      type = _ref7.type,
      narrowDateRange = _ref7.narrowDateRange,
      hostName = _ref7.hostName;
  return _react.default.createElement(_anomalies_host_table.AnomaliesHostTable, {
    startDate: startDate,
    endDate: endDate,
    skip: skip,
    type: type,
    hostName: hostName,
    narrowDateRange: narrowDateRange
  });
};

exports.AnomaliesTabBody = AnomaliesTabBody;

var EventsTabBody = function EventsTabBody(_ref8) {
  var endDate = _ref8.endDate,
      kqlQueryExpression = _ref8.kqlQueryExpression,
      startDate = _ref8.startDate;
  var HOSTS_PAGE_TIMELINE_ID = 'hosts-page';
  return _react.default.createElement(_events_viewer.StatefulEventsViewer, {
    end: endDate,
    id: HOSTS_PAGE_TIMELINE_ID,
    kqlQueryExpression: kqlQueryExpression,
    start: startDate
  });
};

exports.EventsTabBody = EventsTabBody;