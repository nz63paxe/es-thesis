"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BarChart = exports.BarChartWithCustomPrompt = exports.BarChartBaseComponent = void 0;

var _react = _interopRequireDefault(require("react"));

var _charts = require("@elastic/charts");

var _fp = require("lodash/fp");

var _common = require("./common");

var _auto_sizer = require("../auto_sizer");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// Bar chart rotation: https://ela.st/chart-rotations
var BarChartBaseComponent = _react.default.memo(function (_ref) {
  var data = _ref.data,
      chartConfigs = _objectWithoutProperties(_ref, ["data"]);

  var xTickFormatter = (0, _fp.get)('configs.axis.xTickFormatter', chartConfigs);
  var yTickFormatter = (0, _fp.get)('configs.axis.yTickFormatter', chartConfigs);
  var xAxisId = (0, _charts.getAxisId)("stat-items-barchart-".concat(data[0].key, "-x"));
  var yAxisId = (0, _charts.getAxisId)("stat-items-barchart-".concat(data[0].key, "-y"));

  var settings = _objectSpread({}, _common.chartDefaultSettings, {}, (0, _fp.get)('configs.settings', chartConfigs));

  return chartConfigs.width && chartConfigs.height ? _react.default.createElement(_charts.Chart, null, _react.default.createElement(_charts.Settings, _extends({}, settings, {
    theme: (0, _common.getTheme)()
  })), data.map(function (series) {
    var barSeriesKey = series.key;
    var barSeriesSpecId = (0, _charts.getSpecId)(barSeriesKey);
    var seriesType = _common.SeriesType.BAR;
    return _react.default.createElement(_charts.BarSeries, {
      id: barSeriesSpecId,
      key: barSeriesKey,
      name: series.key,
      xScaleType: (0, _fp.getOr)(_charts.ScaleType.Linear, 'configs.series.xScaleType', chartConfigs),
      yScaleType: (0, _fp.getOr)(_charts.ScaleType.Linear, 'configs.series.yScaleType', chartConfigs),
      xAccessor: "x",
      yAccessors: ['y'],
      timeZone: _common.browserTimezone,
      splitSeriesAccessors: ['g'],
      data: series.value,
      stackAccessors: ['y'],
      customSeriesColors: (0, _common.getSeriesStyle)(barSeriesKey, series.color, seriesType)
    });
  }), xTickFormatter ? _react.default.createElement(_charts.Axis, {
    id: xAxisId,
    position: _charts.Position.Bottom,
    showOverlappingTicks: false,
    tickSize: 0,
    tickFormat: xTickFormatter
  }) : _react.default.createElement(_charts.Axis, {
    id: xAxisId,
    position: _charts.Position.Bottom,
    showOverlappingTicks: false,
    tickSize: 0
  }), yTickFormatter ? _react.default.createElement(_charts.Axis, {
    id: yAxisId,
    position: _charts.Position.Left,
    tickSize: 0,
    tickFormat: yTickFormatter
  }) : _react.default.createElement(_charts.Axis, {
    id: yAxisId,
    position: _charts.Position.Left,
    tickSize: 0
  })) : null;
});

exports.BarChartBaseComponent = BarChartBaseComponent;
BarChartBaseComponent.displayName = 'BarChartBaseComponent';

var BarChartWithCustomPrompt = _react.default.memo(function (_ref2) {
  var data = _ref2.data,
      height = _ref2.height,
      width = _ref2.width,
      configs = _ref2.configs;
  return data && data.length && data.some(function (_ref3) {
    var value = _ref3.value;
    return value != null && value.length > 0 && value.every(function (chart) {
      return chart.y != null && chart.y > 0;
    });
  }) ? _react.default.createElement(BarChartBaseComponent, {
    height: height,
    width: width,
    data: data,
    configs: configs
  }) : _react.default.createElement(_common.ChartHolder, null);
});

exports.BarChartWithCustomPrompt = BarChartWithCustomPrompt;
BarChartWithCustomPrompt.displayName = 'BarChartWithCustomPrompt';

var BarChart = _react.default.memo(function (_ref4) {
  var barChart = _ref4.barChart,
      configs = _ref4.configs;
  return _react.default.createElement(_auto_sizer.AutoSizer, {
    detectAnyWindowResize: false,
    content: true
  }, function (_ref5) {
    var measureRef = _ref5.measureRef,
        _ref5$content = _ref5.content,
        height = _ref5$content.height,
        width = _ref5$content.width;
    return _react.default.createElement(_common.WrappedByAutoSizer, {
      innerRef: measureRef
    }, _react.default.createElement(BarChartWithCustomPrompt, {
      height: height,
      width: width,
      data: barChart,
      configs: configs
    }));
  });
});

exports.BarChart = BarChart;
BarChart.displayName = 'BarChart';