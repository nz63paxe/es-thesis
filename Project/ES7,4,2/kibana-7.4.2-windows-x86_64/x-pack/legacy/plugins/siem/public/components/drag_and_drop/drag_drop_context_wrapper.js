"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DragDropContextWrapper = exports.DragDropContextWrapperComponent = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _reactRedux = require("react-redux");

var _store = require("../../store");

var _helpers = require("./helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var onDragEndHandler = function onDragEndHandler(_ref) {
  var browserFields = _ref.browserFields,
      dataProviders = _ref.dataProviders,
      dispatch = _ref.dispatch,
      result = _ref.result;

  if ((0, _helpers.providerWasDroppedOnTimeline)(result)) {
    (0, _helpers.addProviderToTimeline)({
      dataProviders: dataProviders,
      result: result,
      dispatch: dispatch
    });
  } else if ((0, _helpers.providerWasDroppedOnTimelineButton)(result)) {
    (0, _helpers.addProviderToTimeline)({
      dataProviders: dataProviders,
      result: result,
      dispatch: dispatch
    });
  } else if ((0, _helpers.fieldWasDroppedOnTimelineColumns)(result)) {
    (0, _helpers.addFieldToTimelineColumns)({
      browserFields: browserFields,
      dispatch: dispatch,
      result: result
    });
  }
};
/**
 * DragDropContextWrapperComponent handles all drag end events
 */


var DragDropContextWrapperComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(DragDropContextWrapperComponent, _React$Component);

  function DragDropContextWrapperComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DragDropContextWrapperComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DragDropContextWrapperComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "shouldComponentUpdate", function (_ref2) {
      var children = _ref2.children,
          dataProviders = _ref2.dataProviders;
      return children === _this.props.children && dataProviders !== _this.props.dataProviders // prevent re-renders when data providers are added or removed, but all other props are the same
      ? false : true;
    });

    _defineProperty(_assertThisInitialized(_this), "onDragEnd", function (result) {
      var _this$props = _this.props,
          browserFields = _this$props.browserFields,
          dataProviders = _this$props.dataProviders,
          dispatch = _this$props.dispatch;
      enableScrolling();

      if (dataProviders != null) {
        onDragEndHandler({
          browserFields: browserFields,
          result: result,
          dataProviders: dataProviders,
          dispatch: dispatch
        });
      }

      if (!(0, _helpers.draggableIsField)(result)) {
        document.body.classList.remove(_helpers.IS_DRAGGING_CLASS_NAME);
      }
    });

    return _this;
  }

  _createClass(DragDropContextWrapperComponent, [{
    key: "render",
    value: function render() {
      var children = this.props.children;
      return React.createElement(_reactBeautifulDnd.DragDropContext, {
        onDragEnd: this.onDragEnd,
        onDragStart: onDragStart
      }, children);
    }
  }]);

  return DragDropContextWrapperComponent;
}(React.Component);

exports.DragDropContextWrapperComponent = DragDropContextWrapperComponent;
var emptyDataProviders = {}; // stable reference

var mapStateToProps = function mapStateToProps(state) {
  var dataProviders = (0, _fp.defaultTo)(emptyDataProviders, _store.dragAndDropSelectors.dataProvidersSelector(state));
  return {
    dataProviders: dataProviders
  };
};

var DragDropContextWrapper = (0, _reactRedux.connect)(mapStateToProps)(DragDropContextWrapperComponent);
exports.DragDropContextWrapper = DragDropContextWrapper;

var onDragStart = function onDragStart(initial) {
  var x = window.pageXOffset !== undefined ? window.pageXOffset : (document.documentElement || document.body.parentNode || document.body).scrollLeft;
  var y = window.pageYOffset !== undefined ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

  window.onscroll = function () {
    return window.scrollTo(x, y);
  };

  if (!(0, _helpers.draggableIsField)(initial)) {
    document.body.classList.add(_helpers.IS_DRAGGING_CLASS_NAME);
  }
};

var enableScrolling = function enableScrolling() {
  return window.onscroll = function () {
    return _fp.noop;
  };
};