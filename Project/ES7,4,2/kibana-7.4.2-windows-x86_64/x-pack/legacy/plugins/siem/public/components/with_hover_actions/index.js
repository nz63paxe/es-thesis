"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithHoverActions = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-direction: row;\n  height: 100%;\n  padding-right: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  color: ", "\n  height: 100%;\n  position: relative;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HoverActionsPanelContainer = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.textColors.default;
});

HoverActionsPanelContainer.displayName = 'HoverActionsPanelContainer';
var HoverActionsPanel = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children,
      show = _ref.show;
  return React.createElement(HoverActionsPanelContainer, {
    "data-test-subj": "hover-actions-panel-container"
  }, show ? children : null);
});
HoverActionsPanel.displayName = 'HoverActionsPanel';

var WithHoverActionsContainer = _styledComponents.default.div(_templateObject2());

WithHoverActionsContainer.displayName = 'WithHoverActionsContainer';
/**
 * Decorates it's children with actions that are visible on hover.
 * This component does not enforce an opinion on the styling and
 * positioning of the hover content, but see the documentation for
 * the `hoverContent` for tips on (not) effecting layout on-hover.
 *
 * In addition to rendering the `hoverContent` prop on hover, this
 * component also passes `showHoverContent` as a render prop, which
 * provides a signal to the content that the user is in a hover state.
 */

var WithHoverActions =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(WithHoverActions, _React$PureComponent);

  function WithHoverActions(props) {
    var _this;

    _classCallCheck(this, WithHoverActions);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(WithHoverActions).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onMouseEnter", function () {
      _this.setState({
        showHoverContent: true
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onMouseLeave", function () {
      _this.setState({
        showHoverContent: false
      });
    });

    _this.state = {
      showHoverContent: false
    };
    return _this;
  }

  _createClass(WithHoverActions, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          _this$props$alwaysSho = _this$props.alwaysShow,
          alwaysShow = _this$props$alwaysSho === void 0 ? false : _this$props$alwaysSho,
          hoverContent = _this$props.hoverContent,
          render = _this$props.render;
      return React.createElement(WithHoverActionsContainer, {
        onMouseEnter: this.onMouseEnter,
        onMouseLeave: this.onMouseLeave
      }, React.createElement(React.Fragment, null, render(this.state.showHoverContent)), React.createElement(HoverActionsPanel, {
        show: this.state.showHoverContent || alwaysShow
      }, hoverContent != null ? hoverContent : React.createElement(React.Fragment, null)));
    }
  }]);

  return WithHoverActions;
}(React.PureComponent);

exports.WithHoverActions = WithHoverActions;