"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NetworkContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _ip_details = require("./ip_details");

var _network = require("./network");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var networkPath = "/:pageName(network)";

var NetworkContainer = _react.default.memo(function () {
  return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
    strict: true,
    exact: true,
    path: networkPath,
    render: function render() {
      return _react.default.createElement(_network.Network, null);
    }
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(networkPath, "/ip/:detailName"),
    render: function render(_ref) {
      var detailName = _ref.match.params.detailName;
      return _react.default.createElement(_ip_details.IPDetails, {
        detailName: detailName
      });
    }
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "/network/",
    render: function render(_ref2) {
      var _ref2$location$search = _ref2.location.search,
          search = _ref2$location$search === void 0 ? '' : _ref2$location$search;
      return _react.default.createElement(_reactRouterDom.Redirect, {
        from: "/network/",
        to: "/network".concat(search)
      });
    }
  }));
});

exports.NetworkContainer = NetworkContainer;
NetworkContainer.displayName = 'NetworkContainer';