"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SIEM_JOB_FETCH_FAILURE = exports.JOB_SUMMARY_FETCH_FAILURE = exports.INDEX_PATTERN_FETCH_FAILURE = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var INDEX_PATTERN_FETCH_FAILURE = _i18n.i18n.translate('xpack.siem.components.mlPopup.hooks.errors.indexPatternFetchFailureTitle', {
  defaultMessage: 'Index pattern fetch failure'
});

exports.INDEX_PATTERN_FETCH_FAILURE = INDEX_PATTERN_FETCH_FAILURE;

var JOB_SUMMARY_FETCH_FAILURE = _i18n.i18n.translate('xpack.siem.components.mlPopup.hooks.errors.jobSummaryFetchFailureTitle', {
  defaultMessage: 'Job summary fetch failure'
});

exports.JOB_SUMMARY_FETCH_FAILURE = JOB_SUMMARY_FETCH_FAILURE;

var SIEM_JOB_FETCH_FAILURE = _i18n.i18n.translate('xpack.siem.components.mlPopup.hooks.errors.siemJobFetchFailureTitle', {
  defaultMessage: 'SIEM job fetch failure'
});

exports.SIEM_JOB_FETCH_FAILURE = SIEM_JOB_FETCH_FAILURE;