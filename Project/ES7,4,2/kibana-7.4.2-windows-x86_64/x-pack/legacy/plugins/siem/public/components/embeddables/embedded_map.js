"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EmbeddedMap = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _new_platform = require("ui/new_platform");

var _saved_object_finder = require("ui/saved_objects/components/saved_object_finder");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _legacy = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public/legacy");

var _public = require("../../../../../../../src/legacy/core_plugins/embeddable_api/public/np_ready/public");

var _loader = require("../loader");

var _use_index_patterns = require("../ml_popover/hooks/use_index_patterns");

var _use_kibana_ui_setting = require("../../lib/settings/use_kibana_ui_setting");

var _constants = require("../../../common/constants");

var _helpers = require("../ml_popover/helpers");

var _index_patterns_missing_prompt = require("./index_patterns_missing_prompt");

var i18n = _interopRequireWildcard(require("./translations"));

var _toasters = require("../toasters");

var _embedded_map_helpers = require("./embedded_map_helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  height: 400px;\n  margin: 0;\n\n  .mapToolbarOverlay__button {\n    display: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EmbeddableWrapper = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
var EmbeddedMap = React.memo(function (_ref) {
  var applyFilterQueryFromKueryExpression = _ref.applyFilterQueryFromKueryExpression,
      queryExpression = _ref.queryExpression,
      startDate = _ref.startDate,
      endDate = _ref.endDate,
      setQuery = _ref.setQuery;

  var _React$useState = React.useState(null),
      _React$useState2 = _slicedToArray(_React$useState, 2),
      embeddable = _React$useState2[0],
      setEmbeddable = _React$useState2[1];

  var _useState = (0, React.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      isLoading = _useState2[0],
      setIsLoading = _useState2[1];

  var _useState3 = (0, React.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isError = _useState4[0],
      setIsError = _useState4[1];

  var _useState5 = (0, React.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isIndexError = _useState6[0],
      setIsIndexError = _useState6[1];

  var _useStateToaster = (0, _toasters.useStateToaster)(),
      _useStateToaster2 = _slicedToArray(_useStateToaster, 2),
      dispatchToaster = _useStateToaster2[1];

  var _useIndexPatterns = (0, _use_index_patterns.useIndexPatterns)(),
      _useIndexPatterns2 = _slicedToArray(_useIndexPatterns, 2),
      loadingKibanaIndexPatterns = _useIndexPatterns2[0],
      kibanaIndexPatterns = _useIndexPatterns2[1];

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_INDEX_KEY),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      siemDefaultIndices = _useKibanaUiSetting2[0]; // Initial Load useEffect


  (0, React.useEffect)(function () {
    var isSubscribed = true;

    function setupEmbeddable() {
      return _setupEmbeddable.apply(this, arguments);
    }

    function _setupEmbeddable() {
      _setupEmbeddable = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var matchingIndexPatterns, embeddableObject;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                (0, _embedded_map_helpers.setupEmbeddablesAPI)(applyFilterQueryFromKueryExpression);
                _context.next = 10;
                break;

              case 4:
                _context.prev = 4;
                _context.t0 = _context["catch"](0);
                (0, _embedded_map_helpers.displayErrorToast)(i18n.ERROR_CONFIGURING_EMBEDDABLES_API, _context.t0.message, dispatchToaster);
                setIsLoading(false);
                setIsError(true);
                return _context.abrupt("return", false);

              case 10:
                // Ensure at least one `siem:defaultIndex` index pattern exists before trying to import
                matchingIndexPatterns = kibanaIndexPatterns.filter(function (ip) {
                  return siemDefaultIndices.includes(ip.attributes.title);
                });

                if (!(matchingIndexPatterns.length === 0 && isSubscribed)) {
                  _context.next = 15;
                  break;
                }

                setIsLoading(false);
                setIsIndexError(true);
                return _context.abrupt("return");

              case 15:
                _context.prev = 15;
                _context.next = 18;
                return (0, _embedded_map_helpers.createEmbeddable)((0, _helpers.getIndexPatternTitleIdMapping)(matchingIndexPatterns), queryExpression, startDate, endDate, setQuery);

              case 18:
                embeddableObject = _context.sent;

                if (isSubscribed) {
                  setEmbeddable(embeddableObject);
                }

                _context.next = 25;
                break;

              case 22:
                _context.prev = 22;
                _context.t1 = _context["catch"](15);

                if (isSubscribed) {
                  (0, _embedded_map_helpers.displayErrorToast)(i18n.ERROR_CREATING_EMBEDDABLE, _context.t1.message, dispatchToaster);
                  setIsError(true);
                }

              case 25:
                if (isSubscribed) {
                  setIsLoading(false);
                }

              case 26:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 4], [15, 22]]);
      }));
      return _setupEmbeddable.apply(this, arguments);
    }

    if (!loadingKibanaIndexPatterns) {
      setupEmbeddable();
    }

    return function () {
      isSubscribed = false;
    };
  }, [loadingKibanaIndexPatterns, kibanaIndexPatterns]); // queryExpression updated useEffect

  (0, React.useEffect)(function () {
    if (embeddable != null && queryExpression != null) {
      var query = {
        query: queryExpression,
        language: 'kuery'
      };
      embeddable.updateInput({
        query: query
      });
    }
  }, [queryExpression]); // DateRange updated useEffect

  (0, React.useEffect)(function () {
    if (embeddable != null && startDate != null && endDate != null) {
      var timeRange = {
        from: new Date(startDate).toISOString(),
        to: new Date(endDate).toISOString()
      };
      embeddable.updateInput({
        timeRange: timeRange
      });
    }
  }, [startDate, endDate]);
  return isError ? null : React.createElement(React.Fragment, null, React.createElement(EmbeddableWrapper, null, embeddable != null ? React.createElement(_public.EmbeddablePanel, {
    "data-test-subj": "embeddable-panel",
    embeddable: embeddable,
    getActions: _legacy.start.getTriggerCompatibleActions,
    getEmbeddableFactory: _legacy.start.getEmbeddableFactory,
    getAllEmbeddableFactories: _legacy.start.getEmbeddableFactories,
    notifications: _new_platform.npStart.core.notifications,
    overlays: _new_platform.npStart.core.overlays,
    inspector: _new_platform.npStart.plugins.inspector,
    SavedObjectFinder: _saved_object_finder.SavedObjectFinder
  }) : !isLoading && isIndexError ? React.createElement(_index_patterns_missing_prompt.IndexPatternsMissingPrompt, {
    "data-test-subj": "missing-prompt"
  }) : React.createElement(_loader.Loader, {
    "data-test-subj": "loading-panel",
    overlay: true,
    size: "xl"
  })), React.createElement(_eui.EuiSpacer, null));
});
exports.EmbeddedMap = EmbeddedMap;
EmbeddedMap.displayName = 'EmbeddedMap';