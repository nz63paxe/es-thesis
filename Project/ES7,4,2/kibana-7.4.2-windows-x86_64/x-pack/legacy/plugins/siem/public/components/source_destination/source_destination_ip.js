"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceDestinationIp = exports.isIpFieldPopulated = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _ip = require("../ip");

var _port = require("../port");

var i18n = _interopRequireWildcard(require("../timeline/body/renderers/translations"));

var _geo_fields = require("./geo_fields");

var _ip_with_port = require("./ip_with_port");

var _label = require("./label");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns `true` if the ip field (i.e. `sourceIp`, `destinationIp`) that
 * corresponds with the specified `type` (i.e. `source`, `destination`) is
 * populated
 */
var isIpFieldPopulated = function isIpFieldPopulated(_ref) {
  var destinationIp = _ref.destinationIp,
      sourceIp = _ref.sourceIp,
      type = _ref.type;
  return type === 'source' && sourceIp != null || type === 'destination' && destinationIp != null;
};

exports.isIpFieldPopulated = isIpFieldPopulated;
var IpAdressesWithPorts = (0, _recompose.pure)(function (_ref2) {
  var contextId = _ref2.contextId,
      destinationIp = _ref2.destinationIp,
      destinationPort = _ref2.destinationPort,
      eventId = _ref2.eventId,
      sourceIp = _ref2.sourceIp,
      sourcePort = _ref2.sourcePort,
      type = _ref2.type;
  var ip = type === 'source' ? sourceIp : destinationIp;
  var ipFieldName = type === 'source' ? _ip.SOURCE_IP_FIELD_NAME : _ip.DESTINATION_IP_FIELD_NAME;
  var port = type === 'source' ? sourcePort : destinationPort;
  var portFieldName = type === 'source' ? _port.SOURCE_PORT_FIELD_NAME : _port.DESTINATION_PORT_FIELD_NAME;

  if (ip == null) {
    return null; // if ip is not populated as an array, ports will be ignored
  } // IMPORTANT: The ip and port arrays are parallel arrays; the port at
  // index `i` corresponds with the ip address at index `i`. We must
  // preserve the relationships between the parallel arrays:


  var ipPortPairs = port != null && ip.length === port.length ? ip.map(function (address, i) {
    return {
      ip: address,
      port: port[i] // use the corresponding port in the parallel array

    };
  }) : ip.map(function (address) {
    return {
      ip: address,
      port: null // drop the port, because the length of the ip and port arrays is different

    };
  });
  return ip != null ? React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, (0, _fp.uniqWith)(_fp.isEqual, ipPortPairs).map(function (ipPortPair) {
    return ipPortPair.ip != null && React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: ipPortPair.ip
    }, React.createElement(_ip_with_port.IpWithPort, {
      contextId: contextId,
      "data-test-subj": "".concat(type, "-ip-and-port"),
      eventId: eventId,
      ip: ipPortPair.ip,
      ipFieldName: ipFieldName,
      port: ipPortPair.port,
      portFieldName: portFieldName
    }));
  })) : null;
});
IpAdressesWithPorts.displayName = 'IpAdressesWithPorts';
/**
 * When the ip field (i.e. `sourceIp`, `destinationIp`) that corresponds with
 * the specified `type` (i.e. `source`, `destination`) is populated, this component
 * renders:
 * - a label (i.e. `Source` or `Destination`)
 * - a draggable / hyperlinked IP address, when it's populated
 * - a port, hyperlinked to a port lookup service, when it's populated
 * - a summary of geolocation details, when they are populated
 */

var SourceDestinationIp = (0, _recompose.pure)(function (_ref3) {
  var contextId = _ref3.contextId,
      destinationGeoContinentName = _ref3.destinationGeoContinentName,
      destinationGeoCountryName = _ref3.destinationGeoCountryName,
      destinationGeoCountryIsoCode = _ref3.destinationGeoCountryIsoCode,
      destinationGeoRegionName = _ref3.destinationGeoRegionName,
      destinationGeoCityName = _ref3.destinationGeoCityName,
      destinationIp = _ref3.destinationIp,
      destinationPort = _ref3.destinationPort,
      eventId = _ref3.eventId,
      sourceGeoContinentName = _ref3.sourceGeoContinentName,
      sourceGeoCountryName = _ref3.sourceGeoCountryName,
      sourceGeoCountryIsoCode = _ref3.sourceGeoCountryIsoCode,
      sourceGeoRegionName = _ref3.sourceGeoRegionName,
      sourceGeoCityName = _ref3.sourceGeoCityName,
      sourceIp = _ref3.sourceIp,
      sourcePort = _ref3.sourcePort,
      type = _ref3.type;
  var label = type === 'source' ? i18n.SOURCE : i18n.DESTINATION;
  return isIpFieldPopulated({
    destinationIp: destinationIp,
    sourceIp: sourceIp,
    type: type
  }) ? React.createElement(_eui.EuiBadge, {
    "data-test-subj": "".concat(type, "-ip-badge"),
    color: "hollow"
  }, React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    "data-test-subj": "".concat(type, "-ip-group"),
    direction: "column",
    gutterSize: "xs"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_label.Label, {
    "data-test-subj": "".concat(type, "-label")
  }, label)), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(IpAdressesWithPorts, {
    contextId: contextId,
    destinationIp: destinationIp,
    destinationPort: destinationPort,
    eventId: eventId,
    sourceIp: sourceIp,
    sourcePort: sourcePort,
    type: type
  })), React.createElement(_eui.EuiFlexItem, null, React.createElement(_geo_fields.GeoFields, {
    contextId: contextId,
    destinationGeoContinentName: destinationGeoContinentName,
    destinationGeoCountryName: destinationGeoCountryName,
    destinationGeoCountryIsoCode: destinationGeoCountryIsoCode,
    destinationGeoRegionName: destinationGeoRegionName,
    destinationGeoCityName: destinationGeoCityName,
    eventId: eventId,
    sourceGeoContinentName: sourceGeoContinentName,
    sourceGeoCountryName: sourceGeoCountryName,
    sourceGeoCountryIsoCode: sourceGeoCountryIsoCode,
    sourceGeoRegionName: sourceGeoRegionName,
    sourceGeoCityName: sourceGeoCityName,
    type: type
  })))) : null;
});
exports.SourceDestinationIp = SourceDestinationIp;
SourceDestinationIp.displayName = 'SourceDestinationIp';