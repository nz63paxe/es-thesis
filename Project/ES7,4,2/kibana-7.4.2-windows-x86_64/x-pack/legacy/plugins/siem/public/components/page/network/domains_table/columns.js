"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDomainsColumns = void 0;

var _eui = require("@elastic/eui");

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _fp = require("lodash/fp");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _types = require("../../../../graphql/types");

var _helpers = require("../../../../lib/helpers");

var _keury = require("../../../../lib/keury");

var _draggable_wrapper = require("../../../drag_and_drop/draggable_wrapper");

var _helpers2 = require("../../../drag_and_drop/helpers");

var _empty_value = require("../../../empty_value");

var _formatted_date = require("../../../formatted_date");

var _localized_date_tooltip = require("../../../localized_date_tooltip");

var _data_provider = require("../../../timeline/data_providers/data_provider");

var _formatted_bytes = require("../../../formatted_bytes");

var _provider = require("../../../timeline/data_providers/provider");

var _add_to_kql = require("../../add_to_kql");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getDomainsColumns = function getDomainsColumns(indexPattern, ip, flowDirection, flowTarget, type, tableId) {
  return [{
    field: "node.".concat(flowTarget, ".domainName"),
    name: i18n.DOMAIN_NAME,
    truncateText: false,
    hideForMobile: false,
    sortable: true,
    render: function render(domainName) {
      var domainNameAttr = "".concat(flowTarget, ".domain");

      if (domainName != null) {
        var id = (0, _helpers2.escapeDataProviderId)("".concat(tableId, "-table-").concat(flowTarget, "-").concat(flowDirection, "-domain-").concat(domainName));
        return _react.default.createElement(_draggable_wrapper.DraggableWrapper, {
          key: id,
          dataProvider: {
            and: [],
            enabled: true,
            id: id,
            name: domainName,
            excluded: false,
            kqlQuery: '',
            queryMatch: {
              field: domainNameAttr,
              value: domainName,
              operator: _data_provider.IS_OPERATOR
            }
          },
          render: function render(dataProvider, _, snapshot) {
            return snapshot.isDragging ? _react.default.createElement(_draggable_wrapper.DragEffects, null, _react.default.createElement(_provider.Provider, {
              dataProvider: dataProvider
            })) : _react.default.createElement(_react.default.Fragment, null, domainName);
          }
        });
      } else {
        return (0, _empty_value.getEmptyTagValue)();
      }
    }
  }, {
    field: 'node.network.direction',
    name: i18n.DIRECTION,
    truncateText: false,
    hideForMobile: false,
    render: function render(directions) {
      return (0, _fp.isEmpty)(directions) ? (0, _empty_value.getEmptyTagValue)() : directions && directions.map(function (direction, index) {
        return _react.default.createElement(_add_to_kql.AddToKql, {
          indexPattern: indexPattern,
          key: (0, _helpers2.escapeDataProviderId)("".concat(tableId, "-table-").concat(flowTarget, "-").concat(flowDirection, "-direction-").concat(direction)),
          expression: "network.direction: ".concat((0, _keury.escapeQueryValue)(direction)),
          type: type,
          componentFilterType: 'network'
        }, _react.default.createElement(_react.default.Fragment, null, (0, _empty_value.defaultToEmptyTag)(direction), index < directions.length - 1 ? "\xA0" : null));
      });
    }
  }, {
    field: 'node.network.bytes',
    name: i18n.BYTES,
    truncateText: false,
    hideForMobile: false,
    sortable: true,
    render: function render(bytes) {
      if (bytes != null) {
        return _react.default.createElement(_formatted_bytes.PreferenceFormattedBytes, {
          value: bytes
        });
      } else {
        return (0, _empty_value.getEmptyTagValue)();
      }
    }
  }, {
    field: 'node.network.packets',
    name: i18n.PACKETS,
    truncateText: false,
    hideForMobile: false,
    sortable: true,
    render: function render(packets) {
      if (packets != null) {
        return (0, _numeral.default)(packets).format('0,000');
      } else {
        return (0, _empty_value.getEmptyTagValue)();
      }
    }
  }, {
    field: "node.".concat(flowTarget, ".uniqueIpCount"),
    name: getFlowTargetTitle(flowTarget),
    truncateText: false,
    hideForMobile: false,
    sortable: true,
    render: function render(uniqueIpCount) {
      if (uniqueIpCount != null) {
        return (0, _numeral.default)(uniqueIpCount).format('0,000');
      } else {
        return (0, _empty_value.getEmptyTagValue)();
      }
    }
  }, {
    name: _react.default.createElement(_eui.EuiToolTip, {
      content: i18n.FIRST_LAST_SEEN_TOOLTIP
    }, _react.default.createElement(_react.default.Fragment, null, i18n.LAST_SEEN, ' ', _react.default.createElement(_eui.EuiIcon, {
      size: "s",
      color: "subdued",
      type: "iInCircle",
      className: "eui-alignTop"
    }))),
    truncateText: false,
    hideForMobile: false,
    render: function render(_ref) {
      var node = _ref.node;
      var lastSeenAttr = "".concat(flowTarget, ".lastSeen");
      var lastSeen = (0, _fp.getOr)(null, lastSeenAttr, node);

      if (lastSeen != null) {
        return _react.default.createElement(_localized_date_tooltip.LocalizedDateTooltip, {
          date: (0, _moment.default)(new Date(lastSeen)).toDate()
        }, _react.default.createElement(_formatted_date.PreferenceFormattedDate, {
          value: new Date(lastSeen)
        }));
      }

      return (0, _empty_value.getEmptyTagValue)();
    }
  }];
};

exports.getDomainsColumns = getDomainsColumns;

var getFlowTargetTitle = function getFlowTargetTitle(flowTarget) {
  switch (flowTarget) {
    case _types.FlowTarget.client:
      return i18n.UNIQUE_CLIENTS;

    case _types.FlowTarget.server:
      return i18n.UNIQUE_SERVERS;

    case _types.FlowTarget.source:
      return i18n.UNIQUE_DESTINATIONS;

    case _types.FlowTarget.destination:
      return i18n.UNIQUE_SOURCES;
  }

  (0, _helpers.assertUnreachable)(flowTarget);
};