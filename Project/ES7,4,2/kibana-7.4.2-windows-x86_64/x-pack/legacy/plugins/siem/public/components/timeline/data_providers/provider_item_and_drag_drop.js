"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProviderItemAndDragDrop = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _and_or_badge = require("../../and_or_badge");

var _helpers = require("../../drag_and_drop/helpers");

var _provider_item_and = require("./provider_item_and");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  min-width: 230px;\n  width: auto;\n  border: 0.1rem dashed ", ";\n  border-radius: 5px;\n  text-align: center;\n  padding: 3px 10px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  ", ";\n  cursor: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: 0px 8px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DropAndTargetDataProvidersContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
DropAndTargetDataProvidersContainer.displayName = 'DropAndTargetDataProvidersContainer';

var DropAndTargetDataProviders = _styledComponents.default.div(_templateObject2(), function (props) {
  return props.theme.eui.euiColorSuccess;
}, function (props) {
  return props.hasAndItem ? "&:hover {\n    transition: background-color 0.7s ease;\n    background-color: ".concat(function () {
    return props.theme.eui.euiColorSuccess;
  }).concat(_helpers.TWENTY_PERCENT_ALPHA_HEX_SUFFIX, ";\n  }") : '';
}, function (_ref) {
  var hasAndItem = _ref.hasAndItem;
  return !hasAndItem ? "default" : 'inherit';
});

DropAndTargetDataProviders.displayName = 'DropAndTargetDataProviders'; // Ref: https://github.com/elastic/eui/issues/1655
// const NumberProviderAndBadge = styled(EuiBadge)`
//   margin: 0px 5px;
// `;

var NumberProviderAndBadge = function NumberProviderAndBadge(props) {
  return React.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      margin: '0px 5px'
    }
  }));
};

NumberProviderAndBadge.displayName = 'NumberProviderAndBadge';
var ProviderItemAndDragDrop = (0, _recompose.pure)(function (_ref2) {
  var browserFields = _ref2.browserFields,
      dataProvider = _ref2.dataProvider,
      onChangeDataProviderKqlQuery = _ref2.onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider = _ref2.onChangeDroppableAndProvider,
      onDataProviderEdited = _ref2.onDataProviderEdited,
      onDataProviderRemoved = _ref2.onDataProviderRemoved,
      onToggleDataProviderEnabled = _ref2.onToggleDataProviderEnabled,
      onToggleDataProviderExcluded = _ref2.onToggleDataProviderExcluded,
      timelineId = _ref2.timelineId;

  var onMouseEnter = function onMouseEnter() {
    return onChangeDroppableAndProvider(dataProvider.id);
  };

  var onMouseLeave = function onMouseLeave() {
    return onChangeDroppableAndProvider('');
  };

  var hasAndItem = dataProvider.and.length > 0;
  return React.createElement(_eui.EuiFlexGroup, {
    direction: "row",
    gutterSize: "none",
    justifyContent: "flexStart",
    alignItems: "center"
  }, React.createElement(DropAndTargetDataProvidersContainer, {
    className: "drop-and-provider-timeline"
  }, React.createElement(DropAndTargetDataProviders, {
    hasAndItem: hasAndItem,
    onMouseEnter: onMouseEnter,
    onMouseLeave: onMouseLeave
  }, hasAndItem && React.createElement(NumberProviderAndBadge, {
    color: "primary"
  }, dataProvider.and.length), React.createElement(_eui.EuiText, {
    color: "subdued",
    size: "xs"
  }, i18n.DROP_HERE_TO_ADD_AN), React.createElement(_and_or_badge.AndOrBadge, {
    type: "and"
  }))), React.createElement(_provider_item_and.ProviderItemAnd, {
    browserFields: browserFields,
    dataProvidersAnd: dataProvider.and,
    providerId: dataProvider.id,
    onChangeDataProviderKqlQuery: onChangeDataProviderKqlQuery,
    onDataProviderEdited: onDataProviderEdited,
    onDataProviderRemoved: onDataProviderRemoved,
    onToggleDataProviderEnabled: onToggleDataProviderEnabled,
    onToggleDataProviderExcluded: onToggleDataProviderExcluded,
    timelineId: timelineId
  }));
});
exports.ProviderItemAndDragDrop = ProviderItemAndDragDrop;
ProviderItemAndDragDrop.displayName = 'ProviderItemAndDragDrop';