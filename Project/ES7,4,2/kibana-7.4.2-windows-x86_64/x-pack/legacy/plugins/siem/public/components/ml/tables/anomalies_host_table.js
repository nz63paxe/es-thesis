"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnomaliesHostTable = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _use_anomalies_table_data = require("../anomaly/use_anomalies_table_data");

var _header_panel = require("../../header_panel");

var i18n = _interopRequireWildcard(require("./translations"));

var _get_anomalies_host_table_columns = require("./get_anomalies_host_table_columns");

var _convert_anomalies_to_hosts = require("./convert_anomalies_to_hosts");

var _loader = require("../../loader");

var _get_interval_from_anomalies = require("../anomaly/get_interval_from_anomalies");

var _get_size_from_anomalies = require("../anomaly/get_size_from_anomalies");

var _has_ml_user_permissions = require("../permissions/has_ml_user_permissions");

var _ml_capabilities_provider = require("../permissions/ml_capabilities_provider");

var _basic_table = require("./basic_table");

var _host_equality = require("./host_equality");

var _get_criteria_from_host_type = require("../criteria/get_criteria_from_host_type");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var sorting = {
  sort: {
    field: 'anomaly.severity',
    direction: 'desc'
  }
};

var AnomaliesHostTable = _react.default.memo(function (_ref) {
  var startDate = _ref.startDate,
      endDate = _ref.endDate,
      narrowDateRange = _ref.narrowDateRange,
      hostName = _ref.hostName,
      skip = _ref.skip,
      type = _ref.type;
  var capabilities = (0, _react.useContext)(_ml_capabilities_provider.MlCapabilitiesContext);

  var _useAnomaliesTableDat = (0, _use_anomalies_table_data.useAnomaliesTableData)({
    startDate: startDate,
    endDate: endDate,
    skip: skip,
    criteriaFields: (0, _get_criteria_from_host_type.getCriteriaFromHostType)(type, hostName)
  }),
      _useAnomaliesTableDat2 = _slicedToArray(_useAnomaliesTableDat, 2),
      loading = _useAnomaliesTableDat2[0],
      tableData = _useAnomaliesTableDat2[1];

  var hosts = (0, _convert_anomalies_to_hosts.convertAnomaliesToHosts)(tableData, hostName);
  var interval = (0, _get_interval_from_anomalies.getIntervalFromAnomalies)(tableData);
  var columns = (0, _get_anomalies_host_table_columns.getAnomaliesHostTableColumnsCurated)(type, startDate, endDate, interval, narrowDateRange);
  var pagination = {
    pageIndex: 0,
    pageSize: 10,
    totalItemCount: (0, _get_size_from_anomalies.getSizeFromAnomalies)(tableData),
    pageSizeOptions: [5, 10, 20, 50],
    hidePerPageOptions: false
  };

  if (!(0, _has_ml_user_permissions.hasMlUserPermissions)(capabilities)) {
    return null;
  } else {
    return _react.default.createElement(Panel, {
      loading: {
        loading: loading
      }
    }, _react.default.createElement(_header_panel.HeaderPanel, {
      subtitle: "".concat(i18n.SHOWING, ": ").concat(pagination.totalItemCount.toLocaleString(), " ").concat(i18n.UNIT(pagination.totalItemCount)),
      title: i18n.ANOMALIES,
      tooltip: i18n.TOOLTIP
    }), _react.default.createElement(_basic_table.BasicTable, {
      columns: columns,
      compressed: true,
      items: hosts,
      pagination: pagination,
      sorting: sorting
    }), loading && _react.default.createElement(_loader.Loader, {
      "data-test-subj": "anomalies-host-table-loading-panel",
      overlay: true,
      size: "xl"
    }));
  }
}, _host_equality.hostEquality);

exports.AnomaliesHostTable = AnomaliesHostTable;
var Panel = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject(), function (_ref2) {
  var loading = _ref2.loading;
  return loading && "\n    overflow: hidden;\n  ";
});
Panel.displayName = 'Panel';
AnomaliesHostTable.displayName = 'AnomaliesHostTable';