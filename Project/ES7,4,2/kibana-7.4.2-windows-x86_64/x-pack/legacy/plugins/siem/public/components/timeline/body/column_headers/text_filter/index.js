"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TextFilter = exports.DEFAULT_PLACEHOLDER = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  min-width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DEFAULT_PLACEHOLDER = 'Filter';
exports.DEFAULT_PLACEHOLDER = DEFAULT_PLACEHOLDER;
var FieldText = (0, _styledComponents.default)(_eui.EuiFieldText)(_templateObject(), function (props) {
  return props.minwidth;
});
FieldText.displayName = 'FieldText';
/** Renders a text-based column filter */

var TextFilter = (0, _recompose.pure)(function (_ref) {
  var columnId = _ref.columnId,
      minWidth = _ref.minWidth,
      _ref$filter = _ref.filter,
      filter = _ref$filter === void 0 ? '' : _ref$filter,
      _ref$onFilterChange = _ref.onFilterChange,
      onFilterChange = _ref$onFilterChange === void 0 ? _fp.noop : _ref$onFilterChange,
      _ref$placeholder = _ref.placeholder,
      placeholder = _ref$placeholder === void 0 ? DEFAULT_PLACEHOLDER : _ref$placeholder;

  var onChange = function onChange(event) {
    onFilterChange({
      columnId: columnId,
      filter: event.target.value
    });
  };

  return React.createElement(FieldText, {
    "data-test-subj": "textFilter",
    minwidth: "".concat(minWidth, "px"),
    placeholder: placeholder,
    value: filter,
    onChange: onChange
  });
});
exports.TextFilter = TextFilter;
TextFilter.displayName = 'TextFilter';