"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventsViewerHeader = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _header_panel = require("../header_panel");

var _inspect = require("../inspect");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var EventsViewerHeader = _react.default.memo(function (_ref) {
  var id = _ref.id,
      showInspect = _ref.showInspect,
      totalCount = _ref.totalCount;
  return _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    "data-test-subj": "events-viewer-header",
    gutterSize: "none",
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_header_panel.HeaderPanel, {
    subtitle: "".concat(i18n.SHOWING, ": ").concat(totalCount.toLocaleString(), " ").concat(i18n.UNIT(totalCount)),
    title: i18n.EVENTS
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_inspect.InspectButton, {
    compact: true,
    isDisabled: false,
    inputId: "global",
    inspectIndex: 0,
    queryId: id,
    show: showInspect,
    title: i18n.EVENTS
  })));
});

exports.EventsViewerHeader = EventsViewerHeader;
EventsViewerHeader.displayName = 'EventsViewerHeader';