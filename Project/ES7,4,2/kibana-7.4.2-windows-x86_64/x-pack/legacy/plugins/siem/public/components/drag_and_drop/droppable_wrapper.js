"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DroppableWrapper = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("./helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  transition: background-color 0.7s ease;\n  width: 100%;\n  height: ", ";\n  .flyout-overlay {\n    .euiPanel {\n      background-color: ", ";\n    }\n  }\n  ", "\n  > div.timeline-drop-area {\n    .drop-and-provider-timeline {\n      display: none;\n    }\n    & + div {\n      // Override dragNdrop beautiful so we do not have our droppable moving around for no good reason\n      display: none !important;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ReactDndDropTarget = _styledComponents.default.div(_templateObject(), function (_ref) {
  var height = _ref.height;
  return height;
}, function (props) {
  return props.theme.eui.euiFormBackgroundColor;
}, function (props) {
  return props.isDraggingOver ? "\n    .drop-and-provider-timeline {\n      &:hover {\n        background-color: ".concat(props.theme.eui.euiColorSuccess).concat(_helpers.THIRTY_PERCENT_ALPHA_HEX_SUFFIX, ";\n      }\n    }\n    .drop-and-provider-timeline:hover {\n        background-color: ").concat(props.theme.eui.euiColorSuccess).concat(_helpers.THIRTY_PERCENT_ALPHA_HEX_SUFFIX, ";\n    }\n  > div.timeline-drop-area-empty {\n     color: ").concat(props.theme.eui.euiColorSuccess, "\n     background-color: ").concat(props.theme.eui.euiColorSuccess).concat(_helpers.TWENTY_PERCENT_ALPHA_HEX_SUFFIX, ";\n\n     & .euiTextColor--subdued {\n      color: ").concat(props.theme.eui.euiColorSuccess, ";\n     }\n  }\n  > div.timeline-drop-area {\n    background-color: ").concat(props.theme.eui.euiColorSuccess).concat(_helpers.TWENTY_PERCENT_ALPHA_HEX_SUFFIX, ";\n    .provider-item-filter-container div:first-child{\n      // Override dragNdrop beautiful so we do not have our droppable moving around for no good reason\n      transform: none !important;\n    }\n    .drop-and-provider-timeline {\n      display: block !important;\n      + div {\n        display: none;\n      }\n    }\n\n    & .euiFormHelpText {\n      color: ").concat(props.theme.eui.euiColorSuccess, ";\n    }\n  }\n  .flyout-overlay {\n    .euiPanel {\n      background-color: ").concat(props.theme.eui.euiColorLightShade, ";\n    }\n    + div {\n      // Override dragNdrop beautiful so we do not have our droppable moving around for no good reason\n      display: none !important;\n    }\n  }\n  ") : '';
});

ReactDndDropTarget.displayName = 'ReactDndDropTarget';
var DroppableWrapper = (0, _recompose.pure)(function (_ref2) {
  var _ref2$children = _ref2.children,
      children = _ref2$children === void 0 ? null : _ref2$children,
      droppableId = _ref2.droppableId,
      _ref2$height = _ref2.height,
      height = _ref2$height === void 0 ? '100%' : _ref2$height,
      _ref2$isDropDisabled = _ref2.isDropDisabled,
      isDropDisabled = _ref2$isDropDisabled === void 0 ? false : _ref2$isDropDisabled,
      type = _ref2.type,
      _ref2$render = _ref2.render,
      render = _ref2$render === void 0 ? null : _ref2$render;
  return React.createElement(_reactBeautifulDnd.Droppable, {
    isDropDisabled: isDropDisabled,
    droppableId: droppableId,
    direction: 'horizontal',
    type: type
  }, function (provided, snapshot) {
    return React.createElement(ReactDndDropTarget, _extends({
      height: height,
      innerRef: provided.innerRef
    }, provided.droppableProps, {
      isDraggingOver: snapshot.isDraggingOver
    }), render == null ? children : render({
      isDraggingOver: snapshot.isDraggingOver
    }), provided.placeholder);
  });
});
exports.DroppableWrapper = DroppableWrapper;
DroppableWrapper.displayName = 'DroppableWrapper';