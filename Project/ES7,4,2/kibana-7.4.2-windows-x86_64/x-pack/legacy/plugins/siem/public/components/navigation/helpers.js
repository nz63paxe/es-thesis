"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSearch = void 0;

var _constants = require("../url_state/constants");

var _types = require("../url_state/types");

var _helpers = require("../url_state/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getSearch = function getSearch(tab, urlState) {
  if (tab && tab.urlKey != null && _types.URL_STATE_KEYS[tab.urlKey] != null) {
    return _types.URL_STATE_KEYS[tab.urlKey].reduce(function (myLocation, urlKey) {
      var urlStateToReplace = urlState[_constants.CONSTANTS.timelineId];

      if (urlKey === _constants.CONSTANTS.kqlQuery && tab.urlKey === 'host') {
        urlStateToReplace = tab.isDetailPage ? urlState.hostDetails : urlState.hosts;
      } else if (urlKey === _constants.CONSTANTS.kqlQuery && tab.urlKey === 'network') {
        urlStateToReplace = urlState.network;
      } else if (urlKey === _constants.CONSTANTS.timerange) {
        urlStateToReplace = urlState[_constants.CONSTANTS.timerange];
      }

      myLocation = (0, _helpers.replaceQueryStringInLocation)(myLocation, (0, _helpers.replaceStateKeyInQueryString)(urlKey, urlStateToReplace)((0, _helpers.getQueryStringFromLocation)(myLocation)));
      return myLocation;
    }, {
      pathname: urlState.pathName,
      hash: '',
      search: '',
      state: ''
    }).search;
  }

  return '';
};

exports.getSearch = getSearch;