"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchRow = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  &:not(:last-child) {\n    margin-bottom: ", ";\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SearchRowContainer = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.euiSizeL;
});

SearchRowContainer.displayName = 'SearchRowContainer';
var SearchRowFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2(), function (props) {
  return props.theme.eui.euiSizeXS;
});
SearchRowFlexGroup.displayName = 'SearchRowFlexGroup';

/**
 * Renders the row containing the search input and Only Favorites filter
 */
var SearchRow = (0, _recompose.pure)(function (_ref) {
  var onlyFavorites = _ref.onlyFavorites,
      onQueryChange = _ref.onQueryChange,
      onToggleOnlyFavorites = _ref.onToggleOnlyFavorites,
      query = _ref.query,
      totalSearchResultsCount = _ref.totalSearchResultsCount;
  return React.createElement(SearchRowContainer, null, React.createElement(SearchRowFlexGroup, {
    gutterSize: "s"
  }, React.createElement(_eui.EuiFlexItem, null, React.createElement(_eui.EuiSearchBar, {
    "data-test-subj": "search-bar",
    box: {
      placeholder: i18n.SEARCH_PLACEHOLDER,
      incremental: false
    },
    onChange: onQueryChange
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiFilterGroup, null, React.createElement(_eui.EuiFilterButton, {
    "data-test-subj": "only-favorites-toggle",
    hasActiveFilters: onlyFavorites,
    onClick: onToggleOnlyFavorites
  }, i18n.ONLY_FAVORITES)))), React.createElement(_eui.EuiText, {
    color: "subdued",
    size: "xs"
  }, React.createElement("p", null, React.createElement(_react.FormattedMessage, {
    "data-test-subj": "query-message",
    id: "xpack.siem.open.timeline.showingNTimelinesLabel",
    defaultMessage: "Showing: {totalSearchResultsCount} {totalSearchResultsCount, plural, one {timeline} other {timelines}} {with}",
    values: {
      totalSearchResultsCount: totalSearchResultsCount,
      with: React.createElement("span", {
        "data-test-subj": "selectable-query-text"
      }, query.trim().length ? "".concat(i18n.WITH, " \"").concat(query.trim(), "\"") : '')
    }
  }))));
});
exports.SearchRow = SearchRow;
SearchRow.displayName = 'SearchRow';