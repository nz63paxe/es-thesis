"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimelineRefetch = void 0;

var _react = require("react");

var _reactRedux = require("react-redux");

var _redux = require("redux");

var _actions = require("../../store/actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var TimelineRefetchComponent = (0, _react.memo)(function (_ref) {
  var id = _ref.id,
      inputId = _ref.inputId,
      inspect = _ref.inspect,
      loading = _ref.loading,
      refetch = _ref.refetch,
      setTimelineQuery = _ref.setTimelineQuery;
  (0, _react.useEffect)(function () {
    setTimelineQuery({
      id: id,
      inputId: inputId,
      inspect: inspect,
      loading: loading,
      refetch: refetch
    });
  }, [id, inputId, loading, refetch, inspect]);
  return null;
});
var TimelineRefetch = (0, _redux.compose)((0, _reactRedux.connect)(null, {
  setTimelineQuery: _actions.inputsActions.setQuery
}))(TimelineRefetchComponent);
exports.TimelineRefetch = TimelineRefetch;