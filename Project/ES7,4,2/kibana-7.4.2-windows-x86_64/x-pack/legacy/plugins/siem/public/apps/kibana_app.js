"use strict";

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _start_app = require("./start_app");

require("uiExports/autocompleteProviders");

require("uiExports/embeddableFactories");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// load the application
_chrome.default.setRootController('siem', _start_app.SiemRootController);