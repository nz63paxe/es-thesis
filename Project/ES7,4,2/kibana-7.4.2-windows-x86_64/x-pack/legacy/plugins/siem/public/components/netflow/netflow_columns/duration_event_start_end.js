"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DurationEventStartEnd = exports.EVENT_END_FIELD_NAME = exports.EVENT_START_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggables = require("../../draggables");

var _duration = require("../../duration");

var _formatted_date = require("../../formatted_date");

var _formatted_duration = require("../../formatted_duration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 3px;\n  position: relative;\n  top: -1px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EVENT_START_FIELD_NAME = 'event.start';
exports.EVENT_START_FIELD_NAME = EVENT_START_FIELD_NAME;
var EVENT_END_FIELD_NAME = 'event.end';
exports.EVENT_END_FIELD_NAME = EVENT_END_FIELD_NAME;
var TimeIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject());
TimeIcon.displayName = 'TimeIcon';
/**
 * Renders a column of draggable badges containing:
 * - `event.duration`
 * - `event.start`
 * - `event.end`
 */

var DurationEventStartEnd = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventDuration = _ref.eventDuration,
      eventId = _ref.eventId,
      eventEnd = _ref.eventEnd,
      eventStart = _ref.eventStart;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "flexStart",
    "data-test-subj": "duration-and-start-group",
    direction: "column",
    justifyContent: "center",
    gutterSize: "none"
  }, eventDuration != null ? (0, _fp.uniq)(eventDuration).map(function (duration) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: duration
    }, React.createElement(_draggables.DefaultDraggable, {
      "data-test-subj": "event-duration",
      field: _duration.EVENT_DURATION_FIELD_NAME,
      id: "duration-event-start-end-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(_duration.EVENT_DURATION_FIELD_NAME, "-").concat(duration),
      name: name,
      tooltipContent: null,
      value: duration
    }, React.createElement(_eui.EuiText, {
      size: "xs"
    }, React.createElement(TimeIcon, {
      size: "m",
      type: "clock"
    }), React.createElement(_formatted_duration.FormattedDuration, {
      maybeDurationNanoseconds: duration,
      tooltipTitle: _duration.EVENT_DURATION_FIELD_NAME
    }))));
  }) : null, eventStart != null ? (0, _fp.uniq)(eventStart).map(function (start) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: start
    }, React.createElement(_draggables.DefaultDraggable, {
      "data-test-subj": "event-start",
      field: EVENT_START_FIELD_NAME,
      id: "duration-event-start-end-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(EVENT_START_FIELD_NAME, "-").concat(start),
      tooltipContent: null,
      value: start
    }, React.createElement(_eui.EuiText, {
      size: "xs"
    }, React.createElement(TimeIcon, {
      size: "m",
      type: "clock"
    }), React.createElement(_formatted_date.FormattedDate, {
      fieldName: EVENT_START_FIELD_NAME,
      value: start
    }))));
  }) : null, eventEnd != null ? (0, _fp.uniq)(eventEnd).map(function (end) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: end
    }, React.createElement(_draggables.DefaultDraggable, {
      "data-test-subj": "event-end",
      field: EVENT_END_FIELD_NAME,
      id: "duration-event-start-end-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(EVENT_END_FIELD_NAME, "-").concat(end),
      tooltipContent: null,
      value: end
    }, React.createElement(_eui.EuiText, {
      size: "xs"
    }, React.createElement(TimeIcon, {
      size: "m",
      type: "clock"
    }), React.createElement(_formatted_date.FormattedDate, {
      fieldName: EVENT_END_FIELD_NAME,
      value: end
    }))));
  }) : null);
});
exports.DurationEventStartEnd = DurationEventStartEnd;
DurationEventStartEnd.displayName = 'DurationEventStartEnd';