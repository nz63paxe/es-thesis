"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AllTimelinesQuery = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _index = require("./index.gql_query");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var AllTimelinesQuery =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(AllTimelinesQuery, _React$PureComponent);

  function AllTimelinesQuery(props) {
    var _this;

    _classCallCheck(this, AllTimelinesQuery);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AllTimelinesQuery).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedAllTimeline", void 0);

    _defineProperty(_assertThisInitialized(_this), "getAllTimeline", function (variables, timelines) {
      return timelines.map(function (timeline) {
        return {
          created: timeline.created,
          description: timeline.description,
          eventIdToNoteIds: timeline.eventIdToNoteIds != null ? timeline.eventIdToNoteIds.reduce(function (acc, note) {
            if (note.eventId != null) {
              var notes = (0, _fp.getOr)([], note.eventId, acc);
              return _objectSpread({}, acc, _defineProperty({}, note.eventId, [].concat(_toConsumableArray(notes), [note.noteId])));
            }

            return acc;
          }, {}) : null,
          favorite: timeline.favorite,
          noteIds: timeline.noteIds,
          notes: timeline.notes != null ? timeline.notes.map(function (note) {
            return _objectSpread({}, note, {
              savedObjectId: note.noteId
            });
          }) : null,
          pinnedEventIds: timeline.pinnedEventIds != null ? timeline.pinnedEventIds.reduce(function (acc, pinnedEventId) {
            return _objectSpread({}, acc, _defineProperty({}, pinnedEventId, true));
          }, {}) : null,
          savedObjectId: timeline.savedObjectId,
          title: timeline.title,
          updated: timeline.updated,
          updatedBy: timeline.updatedBy
        };
      });
    });

    _this.memoizedAllTimeline = (0, _memoizeOne.default)(_this.getAllTimeline);
    return _this;
  }

  _createClass(AllTimelinesQuery, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          children = _this$props.children,
          onlyUserFavorite = _this$props.onlyUserFavorite,
          pageInfo = _this$props.pageInfo,
          search = _this$props.search,
          sort = _this$props.sort;
      var variables = {
        onlyUserFavorite: onlyUserFavorite,
        pageInfo: pageInfo,
        search: search,
        sort: sort
      };
      return _react.default.createElement(_reactApollo.Query, {
        query: _index.allTimelinesQuery,
        fetchPolicy: "network-only",
        notifyOnNetworkStatusChange: true,
        variables: variables
      }, function (_ref) {
        var data = _ref.data,
            loading = _ref.loading;
        return children({
          loading: loading,
          totalCount: (0, _fp.getOr)(0, 'getAllTimeline.totalCount', data),
          timelines: _this2.memoizedAllTimeline(JSON.stringify(variables), (0, _fp.getOr)([], 'getAllTimeline.timeline', data))
        });
      });
    }
  }]);

  return AllTimelinesQuery;
}(_react.default.PureComponent);

exports.AllTimelinesQuery = AllTimelinesQuery;