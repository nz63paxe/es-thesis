"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Category = void 0;

var _eui = require("@elastic/eui");

var _recompose = require("recompose");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _category_title = require("./category_title");

var _field_items = require("./field_items");

var _helpers = require("./helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", ";\n  overflow-x: hidden;\n  overflow-y: auto;\n  ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TableContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var height = _ref.height;
  return "height: ".concat(height, "px");
}, function (_ref2) {
  var width = _ref2.width;
  return "width: ".concat(width, "px");
});

TableContainer.displayName = 'TableContainer';
var Category = (0, _recompose.pure)(function (_ref3) {
  var categoryId = _ref3.categoryId,
      filteredBrowserFields = _ref3.filteredBrowserFields,
      fieldItems = _ref3.fieldItems,
      timelineId = _ref3.timelineId,
      width = _ref3.width;
  return React.createElement(React.Fragment, null, React.createElement(_category_title.CategoryTitle, {
    categoryId: categoryId,
    filteredBrowserFields: filteredBrowserFields,
    timelineId: timelineId
  }), React.createElement(TableContainer, {
    className: "euiTable--compressed",
    "data-test-subj": "category-table-container",
    height: _helpers.TABLE_HEIGHT,
    width: width
  }, React.createElement(_eui.EuiInMemoryTable, {
    items: fieldItems,
    columns: (0, _field_items.getFieldColumns)(),
    pagination: false,
    sorting: true
  })));
});
exports.Category = Category;
Category.displayName = 'Category';