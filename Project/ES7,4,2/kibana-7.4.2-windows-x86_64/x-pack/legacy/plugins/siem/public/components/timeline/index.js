"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulTimeline = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _source = require("../../containers/source");

var _store = require("../../store");

var _actions = require("../../store/actions");

var _default_headers = require("./body/column_headers/default_headers");

var _timeline = require("./timeline");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var StatefulTimelineComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(StatefulTimelineComponent, _React$Component);

  function StatefulTimelineComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, StatefulTimelineComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(StatefulTimelineComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "shouldComponentUpdate", function (_ref) {
      var id = _ref.id,
          flyoutHeaderHeight = _ref.flyoutHeaderHeight,
          flyoutHeight = _ref.flyoutHeight,
          activePage = _ref.activePage,
          columns = _ref.columns,
          dataProviders = _ref.dataProviders,
          end = _ref.end,
          isLive = _ref.isLive,
          itemsPerPage = _ref.itemsPerPage,
          itemsPerPageOptions = _ref.itemsPerPageOptions,
          kqlMode = _ref.kqlMode,
          kqlQueryExpression = _ref.kqlQueryExpression,
          pageCount = _ref.pageCount,
          sort = _ref.sort,
          start = _ref.start,
          show = _ref.show,
          showCallOutUnauthorizedMsg = _ref.showCallOutUnauthorizedMsg;
      return id !== _this.props.id || flyoutHeaderHeight !== _this.props.flyoutHeaderHeight || flyoutHeight !== _this.props.flyoutHeight || activePage !== _this.props.activePage || !(0, _fp.isEqual)(columns, _this.props.columns) || !(0, _fp.isEqual)(dataProviders, _this.props.dataProviders) || end !== _this.props.end || isLive !== _this.props.isLive || itemsPerPage !== _this.props.itemsPerPage || !(0, _fp.isEqual)(itemsPerPageOptions, _this.props.itemsPerPageOptions) || kqlMode !== _this.props.kqlMode || kqlQueryExpression !== _this.props.kqlQueryExpression || pageCount !== _this.props.pageCount || !(0, _fp.isEqual)(sort, _this.props.sort) || start !== _this.props.start || show !== _this.props.show || showCallOutUnauthorizedMsg !== _this.props.showCallOutUnauthorizedMsg;
    });

    _defineProperty(_assertThisInitialized(_this), "onDataProviderRemoved", function (providerId, andProviderId) {
      return _this.props.removeProvider({
        id: _this.props.id,
        providerId: providerId,
        andProviderId: andProviderId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleDataProviderEnabled", function (_ref2) {
      var providerId = _ref2.providerId,
          enabled = _ref2.enabled,
          andProviderId = _ref2.andProviderId;
      return _this.props.updateDataProviderEnabled({
        id: _this.props.id,
        enabled: enabled,
        providerId: providerId,
        andProviderId: andProviderId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleDataProviderExcluded", function (_ref3) {
      var providerId = _ref3.providerId,
          excluded = _ref3.excluded,
          andProviderId = _ref3.andProviderId;
      return _this.props.updateDataProviderExcluded({
        id: _this.props.id,
        excluded: excluded,
        providerId: providerId,
        andProviderId: andProviderId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onDataProviderEdited", function (_ref4) {
      var andProviderId = _ref4.andProviderId,
          excluded = _ref4.excluded,
          field = _ref4.field,
          operator = _ref4.operator,
          providerId = _ref4.providerId,
          value = _ref4.value;
      return _this.props.onDataProviderEdited({
        andProviderId: andProviderId,
        excluded: excluded,
        field: field,
        id: _this.props.id,
        operator: operator,
        providerId: providerId,
        value: value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeDataProviderKqlQuery", function (_ref5) {
      var providerId = _ref5.providerId,
          kqlQuery = _ref5.kqlQuery;
      return _this.props.updateDataProviderKqlQuery({
        id: _this.props.id,
        kqlQuery: kqlQuery,
        providerId: providerId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeItemsPerPage", function (itemsChangedPerPage) {
      return _this.props.updateItemsPerPage({
        id: _this.props.id,
        itemsPerPage: itemsChangedPerPage
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeDroppableAndProvider", function (providerId) {
      return _this.props.updateHighlightedDropAndProviderId({
        id: _this.props.id,
        providerId: providerId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "toggleColumn", function (column) {
      var _this$props = _this.props,
          columns = _this$props.columns,
          removeColumn = _this$props.removeColumn,
          id = _this$props.id,
          upsertColumn = _this$props.upsertColumn;
      var exists = columns.findIndex(function (c) {
        return c.id === column.id;
      }) !== -1;

      if (!exists && upsertColumn != null) {
        upsertColumn({
          column: column,
          id: id,
          index: 1
        });
      }

      if (exists && removeColumn != null) {
        removeColumn({
          columnId: column.id,
          id: id
        });
      }
    });

    return _this;
  }

  _createClass(StatefulTimelineComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this$props2 = this.props,
          createTimeline = _this$props2.createTimeline,
          id = _this$props2.id;

      if (createTimeline != null) {
        createTimeline({
          id: id,
          columns: _default_headers.defaultHeaders,
          show: false
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          columns = _this$props3.columns,
          dataProviders = _this$props3.dataProviders,
          end = _this$props3.end,
          flyoutHeight = _this$props3.flyoutHeight,
          flyoutHeaderHeight = _this$props3.flyoutHeaderHeight,
          id = _this$props3.id,
          isLive = _this$props3.isLive,
          itemsPerPage = _this$props3.itemsPerPage,
          itemsPerPageOptions = _this$props3.itemsPerPageOptions,
          kqlMode = _this$props3.kqlMode,
          kqlQueryExpression = _this$props3.kqlQueryExpression,
          show = _this$props3.show,
          showCallOutUnauthorizedMsg = _this$props3.showCallOutUnauthorizedMsg,
          start = _this$props3.start,
          sort = _this$props3.sort;
      return React.createElement(_source.WithSource, {
        sourceId: "default"
      }, function (_ref6) {
        var indexPattern = _ref6.indexPattern,
            browserFields = _ref6.browserFields;
        return React.createElement(_timeline.Timeline, {
          browserFields: browserFields,
          columns: columns,
          id: id,
          dataProviders: dataProviders,
          end: end,
          flyoutHeaderHeight: flyoutHeaderHeight,
          flyoutHeight: flyoutHeight,
          indexPattern: indexPattern,
          isLive: isLive,
          itemsPerPage: itemsPerPage,
          itemsPerPageOptions: itemsPerPageOptions,
          kqlMode: kqlMode,
          kqlQueryExpression: kqlQueryExpression,
          onChangeDataProviderKqlQuery: _this2.onChangeDataProviderKqlQuery,
          onChangeDroppableAndProvider: _this2.onChangeDroppableAndProvider,
          onChangeItemsPerPage: _this2.onChangeItemsPerPage,
          onDataProviderEdited: _this2.onDataProviderEdited,
          onDataProviderRemoved: _this2.onDataProviderRemoved,
          onToggleDataProviderEnabled: _this2.onToggleDataProviderEnabled,
          onToggleDataProviderExcluded: _this2.onToggleDataProviderExcluded,
          show: show,
          showCallOutUnauthorizedMsg: showCallOutUnauthorizedMsg,
          start: start,
          sort: sort,
          toggleColumn: _this2.toggleColumn
        });
      });
    }
  }]);

  return StatefulTimelineComponent;
}(React.Component);

var makeMapStateToProps = function makeMapStateToProps() {
  var getShowCallOutUnauthorizedMsg = _store.timelineSelectors.getShowCallOutUnauthorizedMsg();

  var getTimeline = _store.timelineSelectors.getTimelineByIdSelector();

  var getKqlQueryTimeline = _store.timelineSelectors.getKqlFilterQuerySelector();

  var getInputsTimeline = _store.inputsSelectors.getTimelineSelector();

  var mapStateToProps = function mapStateToProps(state, _ref7) {
    var id = _ref7.id;
    var timeline = getTimeline(state, id);
    var input = getInputsTimeline(state);
    var columns = timeline.columns,
        dataProviders = timeline.dataProviders,
        itemsPerPage = timeline.itemsPerPage,
        itemsPerPageOptions = timeline.itemsPerPageOptions,
        kqlMode = timeline.kqlMode,
        sort = timeline.sort,
        show = timeline.show;
    var kqlQueryExpression = getKqlQueryTimeline(state, id);
    return {
      columns: columns,
      dataProviders: dataProviders,
      end: input.timerange.to,
      id: id,
      isLive: input.policy.kind === 'interval',
      itemsPerPage: itemsPerPage,
      itemsPerPageOptions: itemsPerPageOptions,
      kqlMode: kqlMode,
      kqlQueryExpression: kqlQueryExpression,
      sort: sort,
      start: input.timerange.from,
      showCallOutUnauthorizedMsg: getShowCallOutUnauthorizedMsg(state),
      show: show
    };
  };

  return mapStateToProps;
};

var StatefulTimeline = (0, _reactRedux.connect)(makeMapStateToProps, {
  addProvider: _actions.timelineActions.addProvider,
  createTimeline: _actions.timelineActions.createTimeline,
  onDataProviderEdited: _actions.timelineActions.dataProviderEdited,
  updateColumns: _actions.timelineActions.updateColumns,
  updateDataProviderEnabled: _actions.timelineActions.updateDataProviderEnabled,
  updateDataProviderExcluded: _actions.timelineActions.updateDataProviderExcluded,
  updateDataProviderKqlQuery: _actions.timelineActions.updateDataProviderKqlQuery,
  updateHighlightedDropAndProviderId: _actions.timelineActions.updateHighlightedDropAndProviderId,
  updateItemsPerPage: _actions.timelineActions.updateItemsPerPage,
  updateItemsPerPageOptions: _actions.timelineActions.updateItemsPerPageOptions,
  updateSort: _actions.timelineActions.updateSort,
  removeProvider: _actions.timelineActions.removeProvider,
  removeColumn: _actions.timelineActions.removeColumn,
  upsertColumn: _actions.timelineActions.upsertColumn
})(StatefulTimelineComponent);
exports.StatefulTimeline = StatefulTimeline;