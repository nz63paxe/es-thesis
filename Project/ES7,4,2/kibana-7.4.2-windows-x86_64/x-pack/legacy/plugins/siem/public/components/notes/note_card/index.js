"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteCard = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _note_card_body = require("./note_card_body");

var _note_card_header = require("./note_card_header");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NoteCardContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject());
NoteCardContainer.displayName = 'NoteCardContainer';
var NoteCard = (0, _recompose.pure)(function (_ref) {
  var created = _ref.created,
      rawNote = _ref.rawNote,
      user = _ref.user;
  return React.createElement(NoteCardContainer, {
    "data-test-subj": "note-card",
    hasShadow: false,
    paddingSize: "none"
  }, React.createElement(_note_card_header.NoteCardHeader, {
    created: created,
    user: user
  }), React.createElement(_note_card_body.NoteCardBody, {
    rawNote: rawNote
  }));
});
exports.NoteCard = NoteCard;
NoteCard.displayName = 'NoteCard';