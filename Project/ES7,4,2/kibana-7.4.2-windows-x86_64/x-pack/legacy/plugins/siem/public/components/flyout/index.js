"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Flyout = exports.FlyoutComponent = exports.Badge = exports.flyoutHeaderHeight = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _store = require("../../store");

var _button = require("./button");

var _pane = require("./pane");

var _actions = require("../../store/actions");

var _helpers = require("../timeline/body/helpers");

var _track_usage = require("../../lib/track_usage");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  visibility: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: absolute;\n  padding-left: 4px;\n  padding-right: 4px;\n  right: 0%;\n  top: 0%;\n  border-bottom-left-radius: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/** The height in pixels of the flyout header, exported for use in height calculations */
var flyoutHeaderHeight = 60;
exports.flyoutHeaderHeight = flyoutHeaderHeight;
var Badge = (0, _styledComponents.default)(_eui.EuiBadge)(_templateObject());
exports.Badge = Badge;
Badge.displayName = 'Badge';

var Visible = _styledComponents.default.div(_templateObject2(), function (_ref) {
  var show = _ref.show;
  return show ? 'visible' : 'hidden';
});

Visible.displayName = 'Visible';
var FlyoutComponent = React.memo(function (_ref2) {
  var children = _ref2.children,
      dataProviders = _ref2.dataProviders,
      flyoutHeight = _ref2.flyoutHeight,
      headerHeight = _ref2.headerHeight,
      show = _ref2.show,
      showTimeline = _ref2.showTimeline,
      timelineId = _ref2.timelineId,
      usersViewing = _ref2.usersViewing,
      width = _ref2.width;
  return React.createElement(React.Fragment, null, React.createElement(Visible, {
    show: show
  }, React.createElement(_pane.Pane, {
    flyoutHeight: flyoutHeight,
    headerHeight: headerHeight,
    onClose: function onClose() {
      return showTimeline({
        id: timelineId,
        show: false
      });
    },
    timelineId: timelineId,
    usersViewing: usersViewing,
    width: width
  }, children)), React.createElement(_button.FlyoutButton, {
    dataProviders: dataProviders,
    show: !show,
    timelineId: timelineId,
    onOpen: function onOpen() {
      (0, _track_usage.trackUiAction)(_track_usage.METRIC_TYPE.LOADED, _track_usage.TELEMETRY_EVENT.TIMELINE_OPENED);
      showTimeline({
        id: timelineId,
        show: true
      });
    }
  }));
});
exports.FlyoutComponent = FlyoutComponent;
FlyoutComponent.displayName = 'FlyoutComponent';

var mapStateToProps = function mapStateToProps(state, _ref3) {
  var timelineId = _ref3.timelineId;
  var timelineById = (0, _fp.defaultTo)({}, _store.timelineSelectors.timelineByIdSelector(state));
  var dataProviders = (0, _fp.getOr)([], "".concat(timelineId, ".dataProviders"), timelineById);
  var show = (0, _fp.getOr)('false', "".concat(timelineId, ".show"), timelineById);
  var width = (0, _fp.getOr)(_helpers.DEFAULT_TIMELINE_WIDTH, "".concat(timelineId, ".width"), timelineById);
  return {
    dataProviders: dataProviders,
    show: show,
    width: width
  };
};

var Flyout = (0, _reactRedux.connect)(mapStateToProps, {
  showTimeline: _actions.timelineActions.showTimeline
})(FlyoutComponent);
exports.Flyout = Flyout;