"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Events = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _stateful_event = require("./stateful_event");

var _scheduler = require("../../../../lib/helpers/scheduler");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  overflow: hidden;\n  min-width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EventsContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var minWidth = _ref.minWidth;
  return "".concat(minWidth, "px");
});

EventsContainer.displayName = 'EventsContainer';
var Events = React.memo(function (_ref2) {
  var actionsColumnWidth = _ref2.actionsColumnWidth,
      addNoteToEvent = _ref2.addNoteToEvent,
      browserFields = _ref2.browserFields,
      columnHeaders = _ref2.columnHeaders,
      columnRenderers = _ref2.columnRenderers,
      data = _ref2.data,
      eventIdToNoteIds = _ref2.eventIdToNoteIds,
      getNotesByIds = _ref2.getNotesByIds,
      id = _ref2.id,
      _ref2$isEventViewer = _ref2.isEventViewer,
      isEventViewer = _ref2$isEventViewer === void 0 ? false : _ref2$isEventViewer,
      minWidth = _ref2.minWidth,
      onColumnResized = _ref2.onColumnResized,
      onPinEvent = _ref2.onPinEvent,
      onUpdateColumns = _ref2.onUpdateColumns,
      onUnPinEvent = _ref2.onUnPinEvent,
      pinnedEventIds = _ref2.pinnedEventIds,
      rowRenderers = _ref2.rowRenderers,
      toggleColumn = _ref2.toggleColumn,
      updateNote = _ref2.updateNote;
  return React.createElement(EventsContainer, {
    "data-test-subj": "events",
    minWidth: minWidth
  }, React.createElement(_eui.EuiFlexGroup, {
    "data-test-subj": "events-flex-group",
    direction: "column",
    gutterSize: "none"
  }, data.map(function (event, i) {
    return React.createElement(_eui.EuiFlexItem, {
      "data-test-subj": "event-flex-item",
      key: event._id
    }, React.createElement(_stateful_event.StatefulEvent, {
      actionsColumnWidth: actionsColumnWidth,
      addNoteToEvent: addNoteToEvent,
      browserFields: browserFields,
      columnHeaders: columnHeaders,
      columnRenderers: columnRenderers,
      event: event,
      eventIdToNoteIds: eventIdToNoteIds,
      getNotesByIds: getNotesByIds,
      isEventViewer: isEventViewer,
      onColumnResized: onColumnResized,
      onPinEvent: onPinEvent,
      onUpdateColumns: onUpdateColumns,
      onUnPinEvent: onUnPinEvent,
      pinnedEventIds: pinnedEventIds,
      rowRenderers: rowRenderers,
      timelineId: id,
      toggleColumn: toggleColumn,
      updateNote: updateNote,
      maxDelay: (0, _scheduler.maxDelay)(i)
    }));
  })));
});
exports.Events = Events;
Events.displayName = 'Events';