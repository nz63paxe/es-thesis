"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FieldName = exports.FieldNameContainer = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _with_copy_to_clipboard = require("../../lib/clipboard/with_copy_to_clipboard");

var _with_hover_actions = require("../with_hover_actions");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

var _timeline_context = require("../timeline/timeline_context");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  cursor: pointer;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  cursor: default;\n  left: 5px;\n  padding: 4px;\n  position: absolute;\n  top: -6px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n    padding: 5px;\n    {\n      border-radius: 4px;\n      padding: 0 4px 0 8px;\n      position: relative;\n\n      &::before {\n        background-image: linear-gradient(\n            135deg,\n            ", " 25%,\n            transparent 25%\n          ),\n          linear-gradient(-135deg, ", " 25%, transparent 25%),\n          linear-gradient(135deg, transparent 75%, ", " 75%),\n          linear-gradient(-135deg, transparent 75%, ", " 75%);\n        background-position: 0 0, 1px 0, 1px -1px, 0px 1px;\n        background-size: 2px 2px;\n        bottom: 2px;\n        content: '';\n        display: block;\n        left: 2px;\n        position: absolute;\n        top: 2px;\n        width: 4px;\n      }\n\n      &:hover,\n      &:focus {\n        transition: background-color 0.7s ease;\n        background-color: #000;\n        color: #fff;\n\n        &::before {\n          background-image: linear-gradient(\n              135deg,\n              #fff 25%,\n              transparent 25%\n            ),\n            linear-gradient(-135deg, ", " 25%, transparent 25%),\n            linear-gradient(135deg, transparent 75%, ", " 75%),\n            linear-gradient(-135deg, transparent 75%, ", " 75%);\n        }\n      }\n  "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/**
 * The name of a (draggable) field
 */
var FieldNameContainer = _styledComponents.default.span(_templateObject(), function (_ref) {
  var theme = _ref.theme;
  return (0, _styledComponents.css)(_templateObject2(), theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorMediumShade, theme.eui.euiColorLightestShade, theme.eui.euiColorLightestShade, theme.eui.euiColorLightestShade);
});

exports.FieldNameContainer = FieldNameContainer;
FieldNameContainer.displayName = 'FieldNameContainer';
var HoverActionsContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject3());
HoverActionsContainer.displayName = 'HoverActionsContainer';
var HoverActionsFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject4());
HoverActionsFlexGroup.displayName = 'HoverActionsFlexGroup';
var ViewCategoryIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject5());
ViewCategoryIcon.displayName = 'ViewCategoryIcon';
var ViewCategory = React.memo(function (_ref2) {
  var categoryId = _ref2.categoryId,
      onUpdateColumns = _ref2.onUpdateColumns,
      categoryColumns = _ref2.categoryColumns;
  var isLoading = (0, React.useContext)(_timeline_context.TimelineContext);
  return React.createElement(_eui.EuiToolTip, {
    content: i18n.VIEW_CATEGORY(categoryId)
  }, !isLoading ? React.createElement(_eui.EuiButtonIcon, {
    "aria-label": i18n.VIEW_CATEGORY(categoryId),
    color: "text",
    "data-test-subj": "view-category",
    onClick: function onClick() {
      onUpdateColumns(categoryColumns);
    },
    iconType: "visTable"
  }) : React.createElement(_helpers.LoadingSpinner, {
    size: "m"
  }));
});
ViewCategory.displayName = 'ViewCategory';
/** Renders a field name in it's non-dragging state */

var FieldName = React.memo(function (_ref3) {
  var categoryId = _ref3.categoryId,
      categoryColumns = _ref3.categoryColumns,
      fieldId = _ref3.fieldId,
      _ref3$highlight = _ref3.highlight,
      highlight = _ref3$highlight === void 0 ? '' : _ref3$highlight,
      onUpdateColumns = _ref3.onUpdateColumns;
  return React.createElement(_with_hover_actions.WithHoverActions, {
    hoverContent: React.createElement(HoverActionsContainer, {
      "data-test-subj": "hover-actions-container",
      paddingSize: "none"
    }, React.createElement(HoverActionsFlexGroup, {
      alignItems: "center",
      direction: "row",
      gutterSize: "none",
      justifyContent: "spaceBetween"
    }, React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement(_eui.EuiToolTip, {
      content: i18n.COPY_TO_CLIPBOARD
    }, React.createElement(_with_copy_to_clipboard.WithCopyToClipboard, {
      "data-test-subj": "copy-to-clipboard",
      text: fieldId,
      titleSummary: i18n.FIELD
    }))), categoryColumns.length > 0 && React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement(ViewCategory, {
      categoryId: categoryId,
      categoryColumns: categoryColumns,
      onUpdateColumns: onUpdateColumns
    })))),
    render: function render() {
      return React.createElement(FieldNameContainer, null, React.createElement(_eui.EuiText, {
        size: "xs"
      }, React.createElement(_eui.EuiHighlight, {
        "data-test-subj": "field-name-".concat(fieldId),
        search: highlight
      }, fieldId)));
    }
  });
});
exports.FieldName = FieldName;
FieldName.displayName = 'FieldName';