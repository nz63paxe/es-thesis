"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteCards = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _add_note = require("../add_note");

var _note_card = require("../note_card");

var _timeline_context = require("../../timeline/timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  padding: 0 5px;\n  margin-bottom: 5px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var AddNoteContainer = _styledComponents.default.div(_templateObject());

AddNoteContainer.displayName = 'AddNoteContainer';

var NoteContainer = _styledComponents.default.div(_templateObject2());

NoteContainer.displayName = 'NoteContainer';
var NoteCardsComp = React.memo(function (_ref) {
  var children = _ref.children;
  var width = (0, _timeline_context.useTimelineWidthContext)(); // Passing the styles directly to the component because the width is
  // being calculated and is recommended by Styled Components for performance
  // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291

  return React.createElement(_eui.EuiPanel, {
    "data-test-subj": "note-cards",
    hasShadow: false,
    paddingSize: "none",
    style: {
      width: "".concat(width - 10, "px"),
      border: 'none'
    }
  }, children);
});
NoteCardsComp.displayName = 'NoteCardsComp';
var NotesContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject3());
NotesContainer.displayName = 'NotesContainer';

/** A view for entering and reviewing notes */
var NoteCards =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(NoteCards, _React$PureComponent);

  function NoteCards(props) {
    var _this;

    _classCallCheck(this, NoteCards);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(NoteCards).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "associateNoteAndToggleShow", function (noteId) {
      _this.props.associateNote(noteId);

      _this.props.toggleShowAddNote();
    });

    _defineProperty(_assertThisInitialized(_this), "updateNewNote", function (newNote) {
      _this.setState({
        newNote: newNote
      });
    });

    _this.state = {
      newNote: ''
    };
    return _this;
  }

  _createClass(NoteCards, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          getNotesByIds = _this$props.getNotesByIds,
          getNewNoteId = _this$props.getNewNoteId,
          noteIds = _this$props.noteIds,
          showAddNote = _this$props.showAddNote,
          toggleShowAddNote = _this$props.toggleShowAddNote,
          updateNote = _this$props.updateNote;
      return React.createElement(NoteCardsComp, null, noteIds.length ? React.createElement(NotesContainer, {
        "data-test-subj": "notes",
        direction: "column",
        gutterSize: "none"
      }, getNotesByIds(noteIds).map(function (note) {
        return React.createElement(NoteContainer, {
          "data-test-subj": "note-container",
          key: note.id
        }, React.createElement(_note_card.NoteCard, {
          created: note.created,
          rawNote: note.note,
          user: note.user
        }));
      })) : null, showAddNote ? React.createElement(AddNoteContainer, {
        "data-test-subj": "add-note-container"
      }, React.createElement(_add_note.AddNote, {
        associateNote: this.associateNoteAndToggleShow,
        getNewNoteId: getNewNoteId,
        newNote: this.state.newNote,
        onCancelAddNote: toggleShowAddNote,
        updateNewNote: this.updateNewNote,
        updateNote: updateNote
      })) : null);
    }
  }]);

  return NoteCards;
}(React.PureComponent);

exports.NoteCards = NoteCards;