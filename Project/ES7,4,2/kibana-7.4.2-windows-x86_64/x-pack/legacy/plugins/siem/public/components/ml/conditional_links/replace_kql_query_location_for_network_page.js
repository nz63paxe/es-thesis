"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceKqlQueryLocationForNetworkPage = void 0;

var _risonNode = require("rison-node");

var _rison_helpers = require("./rison_helpers");

var _constants = require("../../url_state/constants");

var _model = require("../../../store/network/model");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var replaceKqlQueryLocationForNetworkPage = function replaceKqlQueryLocationForNetworkPage(kqlQuery) {
  var value = (0, _rison_helpers.decodeRison)(kqlQuery);

  if ((0, _rison_helpers.isRisonObject)(value)) {
    value.queryLocation = _constants.CONSTANTS.networkPage;
    value.type = _model.NetworkType.page;
    return (0, _risonNode.encode)(value);
  } else {
    return kqlQuery;
  }
};

exports.replaceKqlQueryLocationForNetworkPage = replaceKqlQueryLocationForNetworkPage;