"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Resizeable = exports.removeGlobalResizeCursorStyleFromBody = exports.addGlobalResizeCursorStyleToBody = exports.calculateDeltaX = exports.globalResizeCursorClassName = exports.resizeCursorStyle = void 0;

var React = _interopRequireWildcard(require("react"));

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  cursor: ", ";\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var resizeCursorStyle = 'col-resize';
exports.resizeCursorStyle = resizeCursorStyle;
var globalResizeCursorClassName = 'global-resize-cursor';
/** This polyfill is for Safari and IE-11 only. `movementX` is more accurate and "feels" better, so only use this function on Safari and IE-11 */

exports.globalResizeCursorClassName = globalResizeCursorClassName;

var calculateDeltaX = function calculateDeltaX(_ref) {
  var prevX = _ref.prevX,
      screenX = _ref.screenX;
  return prevX !== 0 ? screenX - prevX : 0;
};

exports.calculateDeltaX = calculateDeltaX;
var isSafari = /^((?!chrome|android|crios|fxios|Firefox).)*safari/i.test(navigator.userAgent);

var ResizeHandleContainer = _styledComponents.default.div(_templateObject(), resizeCursorStyle, function (_ref2) {
  var height = _ref2.height;
  return height != null ? "height: ".concat(height) : '';
});

ResizeHandleContainer.displayName = 'ResizeHandleContainer';

var addGlobalResizeCursorStyleToBody = function addGlobalResizeCursorStyleToBody() {
  document.body.classList.add(globalResizeCursorClassName);
};

exports.addGlobalResizeCursorStyleToBody = addGlobalResizeCursorStyleToBody;

var removeGlobalResizeCursorStyleFromBody = function removeGlobalResizeCursorStyleFromBody() {
  document.body.classList.remove(globalResizeCursorClassName);
};

exports.removeGlobalResizeCursorStyleFromBody = removeGlobalResizeCursorStyleFromBody;

var Resizeable =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Resizeable, _React$PureComponent);

  function Resizeable(props) {
    var _this;

    _classCallCheck(this, Resizeable);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Resizeable).call(this, props)); // NOTE: the ref and observable below are NOT stored in component `State`

    _defineProperty(_assertThisInitialized(_this), "drag$", void 0);

    _defineProperty(_assertThisInitialized(_this), "dragEventTargets", void 0);

    _defineProperty(_assertThisInitialized(_this), "dragSubscription", void 0);

    _defineProperty(_assertThisInitialized(_this), "prevX", 0);

    _defineProperty(_assertThisInitialized(_this), "ref", void 0);

    _defineProperty(_assertThisInitialized(_this), "upSubscription", void 0);

    _defineProperty(_assertThisInitialized(_this), "calculateDelta", function (e) {
      var deltaX = calculateDeltaX({
        prevX: _this.prevX,
        screenX: e.screenX
      });
      _this.prevX = e.screenX;
      return deltaX;
    });

    _this.ref = React.createRef();
    _this.drag$ = null;
    _this.dragSubscription = null;
    _this.upSubscription = null;
    _this.dragEventTargets = [];
    _this.state = {
      isResizing: false
    };
    return _this;
  }

  _createClass(Resizeable, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var _this$props = this.props,
          id = _this$props.id,
          onResize = _this$props.onResize;
      var move$ = (0, _rxjs.fromEvent)(document, 'mousemove');
      var down$ = (0, _rxjs.fromEvent)(this.ref.current, 'mousedown');
      var up$ = (0, _rxjs.fromEvent)(document, 'mouseup');
      this.drag$ = down$.pipe((0, _operators.concatMap)(function () {
        return move$.pipe((0, _operators.takeUntil)(up$));
      }));
      this.dragSubscription = this.drag$.subscribe(function (event) {
        // We do a feature detection of event.movementX here and if it is missing
        // we calculate the delta manually. Browsers IE-11 and Safari will call calculateDelta
        var delta = event.movementX == null || isSafari ? _this2.calculateDelta(event) : event.movementX;

        if (!_this2.state.isResizing) {
          _this2.setState({
            isResizing: true
          });
        }

        onResize({
          id: id,
          delta: delta
        });

        if (event.target != null && event.target instanceof HTMLElement) {
          var htmlElement = event.target;
          _this2.dragEventTargets = [].concat(_toConsumableArray(_this2.dragEventTargets), [{
            htmlElement: htmlElement,
            prevCursor: htmlElement.style.cursor
          }]);
          htmlElement.style.cursor = resizeCursorStyle;
        }
      });
      this.upSubscription = up$.subscribe(function () {
        if (_this2.state.isResizing) {
          _this2.dragEventTargets.reverse().forEach(function (eventTarget) {
            eventTarget.htmlElement.style.cursor = eventTarget.prevCursor;
          });

          _this2.dragEventTargets = [];

          _this2.setState({
            isResizing: false
          });
        }
      });
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.dragSubscription != null) {
        this.dragSubscription.unsubscribe();
      }

      if (this.upSubscription != null) {
        this.upSubscription.unsubscribe();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          handle = _this$props2.handle,
          height = _this$props2.height,
          render = _this$props2.render;
      return React.createElement(React.Fragment, null, render(this.state.isResizing), React.createElement(ResizeHandleContainer, {
        "data-test-subj": "resize-handle-container",
        height: height,
        innerRef: this.ref
      }, handle));
    }
  }]);

  return Resizeable;
}(React.PureComponent);

exports.Resizeable = Resizeable;