"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsEmptyPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _documentation_links = require("ui/documentation_links");

var _empty_page = require("../../components/empty_page");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var basePath = _chrome.default.getBasePath();

var HostsEmptyPage = (0, _recompose.pure)(function () {
  return _react.default.createElement(_empty_page.EmptyPage, {
    actionPrimaryIcon: "gear",
    actionPrimaryLabel: i18n.EMPTY_ACTION_PRIMARY,
    actionPrimaryUrl: "".concat(basePath, "/app/kibana#/home/tutorial_directory/siem"),
    actionSecondaryIcon: "popout",
    actionSecondaryLabel: i18n.EMPTY_ACTION_SECONDARY,
    actionSecondaryTarget: "_blank",
    actionSecondaryUrl: _documentation_links.documentationLinks.siem,
    "data-test-subj": "empty-page",
    title: i18n.EMPTY_TITLE
  });
});
exports.HostsEmptyPage = HostsEmptyPage;
HostsEmptyPage.displayName = 'HostsEmptyPage';