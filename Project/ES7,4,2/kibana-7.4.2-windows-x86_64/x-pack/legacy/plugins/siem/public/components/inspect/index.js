"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InspectButton = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _recompose = require("recompose");

var _store = require("../../store");

var _inputs = require("../../store/inputs");

var _modal = require("./modal");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiButtonIcon {\n    ", "\n    transition: opacity ", " ease;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var InspectContainer = _styledComponents.default.div(_templateObject(), function (props) {
  return props.showInspect ? 'opacity: 1;' : 'opacity: 0';
}, function (props) {
  return (0, _fp.getOr)(250, 'theme.eui.euiAnimSpeedNormal', props);
});

InspectContainer.displayName = 'InspectContainer';
var InspectButtonComponent = (0, _recompose.pure)(function (_ref) {
  var _ref$compact = _ref.compact,
      compact = _ref$compact === void 0 ? false : _ref$compact,
      _ref$inputId = _ref.inputId,
      inputId = _ref$inputId === void 0 ? 'global' : _ref$inputId,
      inspect = _ref.inspect,
      isDisabled = _ref.isDisabled,
      isInspected = _ref.isInspected,
      loading = _ref.loading,
      _ref$inspectIndex = _ref.inspectIndex,
      inspectIndex = _ref$inspectIndex === void 0 ? 0 : _ref$inspectIndex,
      onCloseInspect = _ref.onCloseInspect,
      _ref$queryId = _ref.queryId,
      queryId = _ref$queryId === void 0 ? '' : _ref$queryId,
      selectedInspectIndex = _ref.selectedInspectIndex,
      setIsInspected = _ref.setIsInspected,
      show = _ref.show,
      _ref$title = _ref.title,
      title = _ref$title === void 0 ? '' : _ref$title;
  return _react.default.createElement(InspectContainer, {
    "data-test-subj": "".concat(show ? 'opaque' : 'transparent', "-inspect-container"),
    showInspect: show
  }, inputId === 'timeline' && !compact && _react.default.createElement(_eui.EuiButtonEmpty, {
    "aria-label": i18n.INSPECT,
    "data-test-subj": "inspect-empty-button",
    color: "text",
    iconSide: "left",
    iconType: "inspect",
    isDisabled: loading || isDisabled,
    isLoading: loading,
    onClick: function onClick() {
      setIsInspected({
        id: queryId,
        inputId: inputId,
        isInspected: true,
        selectedInspectIndex: inspectIndex
      });
    }
  }, i18n.INSPECT), (inputId === 'global' || compact) && _react.default.createElement(_eui.EuiButtonIcon, {
    "aria-label": i18n.INSPECT,
    "data-test-subj": "inspect-icon-button",
    iconSize: "m",
    iconType: "inspect",
    isDisabled: loading || isDisabled,
    title: i18n.INSPECT,
    onClick: function onClick() {
      setIsInspected({
        id: queryId,
        inputId: inputId,
        isInspected: true,
        selectedInspectIndex: inspectIndex
      });
    }
  }), _react.default.createElement(_modal.ModalInspectQuery, {
    closeModal: function closeModal() {
      if (onCloseInspect != null) {
        onCloseInspect();
      }

      setIsInspected({
        id: queryId,
        inputId: inputId,
        isInspected: false,
        selectedInspectIndex: inspectIndex
      });
    },
    isShowing: !loading && selectedInspectIndex === inspectIndex && isInspected,
    request: inspect != null && inspect.dsl.length > 0 ? inspect.dsl[inspectIndex] : null,
    response: inspect != null && inspect.response.length > 0 ? inspect.response[inspectIndex] : null,
    title: title,
    "data-test-subj": "inspect-modal"
  }));
});
InspectButtonComponent.displayName = 'InspectButtonComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getGlobalQuery = _store.inputsSelectors.globalQueryByIdSelector();

  var getTimelineQuery = _store.inputsSelectors.timelineQueryByIdSelector();

  var mapStateToProps = function mapStateToProps(state, _ref2) {
    var _ref2$inputId = _ref2.inputId,
        inputId = _ref2$inputId === void 0 ? 'global' : _ref2$inputId,
        queryId = _ref2.queryId;
    return inputId === 'global' ? getGlobalQuery(state, queryId) : getTimelineQuery(state, queryId);
  };

  return mapStateToProps;
};

var InspectButton = (0, _reactRedux.connect)(makeMapStateToProps, {
  setIsInspected: _inputs.inputsActions.setInspectionParameter
})(InspectButtonComponent);
exports.InspectButton = InspectButton;