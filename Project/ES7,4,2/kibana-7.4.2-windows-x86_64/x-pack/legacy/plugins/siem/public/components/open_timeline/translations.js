"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ZERO_TIMELINES_MATCH = exports.WITH = exports.UNTITLED_TIMELINE = exports.TIMELINE_NAME = exports.SEARCH_PLACEHOLDER = exports.POSTED = exports.PINNED_EVENTS = exports.OPEN_TIMELINE_TITLE = exports.OPEN_TIMELINE = exports.OPEN_AS_DUPLICATE = exports.ONLY_FAVORITES = exports.NOTES = exports.MODIFIED_BY = exports.MISSING_SAVED_OBJECT_ID = exports.LAST_MODIFIED = exports.FAVORITES = exports.FAVORITE_SELECTED = exports.EXPAND = exports.DESCRIPTION = exports.DELETE_WARNING = exports.DELETE_SELECTED = exports.DELETE = exports.COLLAPSE = exports.CANCEL = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CANCEL = _i18n.i18n.translate('xpack.siem.open.timeline.cancelButton', {
  defaultMessage: 'Cancel'
});

exports.CANCEL = CANCEL;

var COLLAPSE = _i18n.i18n.translate('xpack.siem.open.timeline.collapseButton', {
  defaultMessage: 'Collapse'
});

exports.COLLAPSE = COLLAPSE;

var DELETE = _i18n.i18n.translate('xpack.siem.open.timeline.deleteButton', {
  defaultMessage: 'Delete'
});

exports.DELETE = DELETE;

var DELETE_SELECTED = _i18n.i18n.translate('xpack.siem.open.timeline.deleteSelectedButton', {
  defaultMessage: 'Delete selected'
});

exports.DELETE_SELECTED = DELETE_SELECTED;

var DELETE_WARNING = _i18n.i18n.translate('xpack.siem.open.timeline.deleteWarningLabel', {
  defaultMessage: 'You will not be able to recover this timeline or its notes once deleted.'
});

exports.DELETE_WARNING = DELETE_WARNING;

var DESCRIPTION = _i18n.i18n.translate('xpack.siem.open.timeline.descriptionTableHeader', {
  defaultMessage: 'Description'
});

exports.DESCRIPTION = DESCRIPTION;

var EXPAND = _i18n.i18n.translate('xpack.siem.open.timeline.expandButton', {
  defaultMessage: 'Expand'
});

exports.EXPAND = EXPAND;

var FAVORITE_SELECTED = _i18n.i18n.translate('xpack.siem.open.timeline.favoriteSelectedButton', {
  defaultMessage: 'Favorite selected'
});

exports.FAVORITE_SELECTED = FAVORITE_SELECTED;

var FAVORITES = _i18n.i18n.translate('xpack.siem.open.timeline.favoritesTooltip', {
  defaultMessage: 'Favorites'
});

exports.FAVORITES = FAVORITES;

var LAST_MODIFIED = _i18n.i18n.translate('xpack.siem.open.timeline.lastModifiedTableHeader', {
  defaultMessage: 'Last modified'
});

exports.LAST_MODIFIED = LAST_MODIFIED;

var MISSING_SAVED_OBJECT_ID = _i18n.i18n.translate('xpack.siem.open.timeline.missingSavedObjectIdTooltip', {
  defaultMessage: 'Missing savedObjectId'
});

exports.MISSING_SAVED_OBJECT_ID = MISSING_SAVED_OBJECT_ID;

var MODIFIED_BY = _i18n.i18n.translate('xpack.siem.open.timeline.modifiedByTableHeader', {
  defaultMessage: 'Modified by'
});

exports.MODIFIED_BY = MODIFIED_BY;

var NOTES = _i18n.i18n.translate('xpack.siem.open.timeline.notesTooltip', {
  defaultMessage: 'Notes'
});

exports.NOTES = NOTES;

var ONLY_FAVORITES = _i18n.i18n.translate('xpack.siem.open.timeline.onlyFavoritesButtonLabel', {
  defaultMessage: 'Only favorites'
});

exports.ONLY_FAVORITES = ONLY_FAVORITES;

var OPEN_AS_DUPLICATE = _i18n.i18n.translate('xpack.siem.open.timeline.openAsDuplicateTooltip', {
  defaultMessage: 'Open as a duplicate timeline'
});

exports.OPEN_AS_DUPLICATE = OPEN_AS_DUPLICATE;

var OPEN_TIMELINE = _i18n.i18n.translate('xpack.siem.open.timeline.openTimelineButton', {
  defaultMessage: 'Open Timeline…'
});

exports.OPEN_TIMELINE = OPEN_TIMELINE;

var OPEN_TIMELINE_TITLE = _i18n.i18n.translate('xpack.siem.open.timeline.openTimelineTitle', {
  defaultMessage: 'Open Timeline'
});

exports.OPEN_TIMELINE_TITLE = OPEN_TIMELINE_TITLE;

var PINNED_EVENTS = _i18n.i18n.translate('xpack.siem.open.timeline.pinnedEventsTooltip', {
  defaultMessage: 'Pinned events'
});

exports.PINNED_EVENTS = PINNED_EVENTS;

var POSTED = _i18n.i18n.translate('xpack.siem.open.timeline.postedLabel', {
  defaultMessage: 'Posted:'
});

exports.POSTED = POSTED;

var SEARCH_PLACEHOLDER = _i18n.i18n.translate('xpack.siem.open.timeline.searchPlaceholder', {
  defaultMessage: 'e.g. timeline name, or description'
});

exports.SEARCH_PLACEHOLDER = SEARCH_PLACEHOLDER;

var TIMELINE_NAME = _i18n.i18n.translate('xpack.siem.open.timeline.timelineNameTableHeader', {
  defaultMessage: 'Timeline name'
});

exports.TIMELINE_NAME = TIMELINE_NAME;

var UNTITLED_TIMELINE = _i18n.i18n.translate('xpack.siem.open.timeline.untitledTimelineLabel', {
  defaultMessage: 'Untitled timeline'
});

exports.UNTITLED_TIMELINE = UNTITLED_TIMELINE;

var WITH = _i18n.i18n.translate('xpack.siem.open.timeline.withLabel', {
  defaultMessage: 'with'
});

exports.WITH = WITH;

var ZERO_TIMELINES_MATCH = _i18n.i18n.translate('xpack.siem.open.timeline.zeroTimelinesMatchLabel', {
  defaultMessage: '0 timelines match the search criteria'
});

exports.ZERO_TIMELINES_MATCH = ZERO_TIMELINES_MATCH;