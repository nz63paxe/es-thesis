"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Ja3Fingerprint = exports.JA3_HASH_FIELD_NAME = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggables = require("../draggables");

var _external_link_icon = require("../external_link_icon");

var _links = require("../links");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var JA3_HASH_FIELD_NAME = 'tls.fingerprints.ja3.hash';
exports.JA3_HASH_FIELD_NAME = JA3_HASH_FIELD_NAME;

var Ja3FingerprintLabel = _styledComponents.default.span(_templateObject());

Ja3FingerprintLabel.displayName = 'Ja3FingerprintLabel';
/**
 * Renders a ja3 fingerprint, which enables (some) clients and servers communicating
 * using TLS traffic to be identified, which is possible because SSL
 * negotiations happen in the clear
 */

var Ja3Fingerprint = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      fieldName = _ref.fieldName,
      value = _ref.value;
  return React.createElement(_draggables.DraggableBadge, {
    contextId: contextId,
    "data-test-subj": "ja3-hash",
    eventId: eventId,
    field: fieldName,
    iconType: "snowflake",
    value: value
  }, React.createElement(Ja3FingerprintLabel, {
    "data-test-subj": "ja3-fingerprint-label"
  }, i18n.JA3_FINGERPRINT_LABEL), React.createElement(_links.Ja3FingerprintLink, {
    "data-test-subj": "ja3-hash-link",
    ja3Fingerprint: value || ''
  }), React.createElement(_external_link_icon.ExternalLinkIcon, null));
});
exports.Ja3Fingerprint = Ja3Fingerprint;
Ja3Fingerprint.displayName = 'Ja3Fingerprint';