"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Args = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _draggables = require("../../../draggables");

var _helpers = require("./helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var Args = (0, _recompose.pure)(function (_ref) {
  var eventId = _ref.eventId,
      contextId = _ref.contextId,
      args = _ref.args,
      processTitle = _ref.processTitle;
  return args != null ? React.createElement(_helpers.TokensFlexItem, {
    grow: false,
    component: "span"
  }, React.createElement(_draggables.DraggableBadge, {
    contextId: contextId,
    eventId: eventId,
    field: "process.title",
    queryValue: processTitle != null ? processTitle : '',
    value: args
  })) : null;
});
exports.Args = Args;
Args.displayName = 'Args';