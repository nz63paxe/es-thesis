"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "HeaderPanel", {
  enumerable: true,
  get: function get() {
    return _header_panel.HeaderPanel;
  }
});

var _header_panel = require("./header_panel");