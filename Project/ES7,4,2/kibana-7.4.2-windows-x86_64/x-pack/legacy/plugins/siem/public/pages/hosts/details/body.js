"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostDetailsBody = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _source = require("../../../containers/source");

var _actions = require("../../../store/inputs/actions");

var _score_interval_to_datetime = require("../../../components/ml/score/score_interval_to_datetime");

var _helpers = require("../helpers");

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HostDetailsBodyComponent = _react.default.memo(function (_ref) {
  var children = _ref.children,
      deleteQuery = _ref.deleteQuery,
      filterQueryExpression = _ref.filterQueryExpression,
      from = _ref.from,
      isInitializing = _ref.isInitializing,
      detailName = _ref.detailName,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker,
      setQuery = _ref.setQuery,
      to = _ref.to;
  return _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_react.default.Fragment, null, children({
      deleteQuery: deleteQuery,
      endDate: to,
      filterQuery: (0, _utils.getFilterQuery)(detailName, filterQueryExpression, indexPattern),
      kqlQueryExpression: (0, _helpers.getHostDetailsEventsKqlQueryExpression)({
        filterQueryExpression: filterQueryExpression,
        hostName: detailName
      }),
      skip: isInitializing,
      setQuery: setQuery,
      startDate: from,
      type: _utils.type,
      indexPattern: indexPattern,
      hostName: detailName,
      narrowDateRange: function narrowDateRange(score, interval) {
        var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
        setAbsoluteRangeDatePicker({
          id: 'global',
          from: fromTo.from,
          to: fromTo.to
        });
      }
    })) : null;
  });
});

HostDetailsBodyComponent.displayName = 'HostDetailsBodyComponent';
var HostDetailsBody = (0, _reactRedux.connect)(_utils.makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
})(HostDetailsBodyComponent);
exports.HostDetailsBody = HostDetailsBody;