"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Actions = exports.CloseButton = exports.ACTIONS_WIDTH = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _sort_indicator = require("../../sort/sort_indicator");

var _helpers = require("../header/helpers");

var i18n = _interopRequireWildcard(require("../translations"));

var _timeline_context = require("../../../timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  visibility: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n  width: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CLOSE_BUTTON_SIZE = 25; // px

var SORT_INDICATOR_SIZE = 25; // px

var ACTIONS_WIDTH = SORT_INDICATOR_SIZE + CLOSE_BUTTON_SIZE; // px

exports.ACTIONS_WIDTH = ACTIONS_WIDTH;
var ActionsContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject(), ACTIONS_WIDTH);
ActionsContainer.displayName = 'ActionsContainer';

var WrappedCloseButton = _styledComponents.default.div(_templateObject2(), function (_ref) {
  var show = _ref.show;
  return show ? 'visible' : 'hidden';
});

WrappedCloseButton.displayName = 'WrappedCloseButton';

/** Given a `header`, returns the `SortDirection` applicable to it */
var CloseButton = (0, _recompose.pure)(function (_ref2) {
  var columnId = _ref2.columnId,
      onColumnRemoved = _ref2.onColumnRemoved,
      show = _ref2.show;
  return React.createElement(WrappedCloseButton, {
    "data-test-subj": "wrapped-close-button",
    show: show
  }, React.createElement(_eui.EuiButtonIcon, {
    "aria-label": i18n.REMOVE_COLUMN,
    color: "subdued",
    "data-test-subj": "remove-column",
    iconType: "cross",
    onClick: function onClick(event) {
      // To avoid a re-sorting when you delete a column
      event.preventDefault();
      event.stopPropagation();
      onColumnRemoved(columnId);
    }
  }));
});
exports.CloseButton = CloseButton;
CloseButton.displayName = 'CloseButton';
var Actions = React.memo(function (_ref3) {
  var header = _ref3.header,
      onColumnRemoved = _ref3.onColumnRemoved,
      show = _ref3.show,
      sort = _ref3.sort;
  var isLoading = (0, _timeline_context.useTimelineContext)();
  return React.createElement(ActionsContainer, {
    alignItems: "center",
    "data-test-subj": "header-actions",
    justifyContent: "center",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_sort_indicator.SortIndicator, {
    "data-test-subj": "header-sort-indicator",
    sortDirection: (0, _helpers.getSortDirection)({
      header: header,
      sort: sort
    })
  })), sort.columnId === header.id && isLoading ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiLoadingSpinner, {
    size: "l"
  })) : React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(CloseButton, {
    columnId: header.id,
    onColumnRemoved: onColumnRemoved,
    show: show
  })));
});
exports.Actions = Actions;
Actions.displayName = 'Actions';