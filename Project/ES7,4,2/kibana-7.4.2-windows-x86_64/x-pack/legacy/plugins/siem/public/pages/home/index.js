"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HomePage = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _reactRouterDom = require("react-router-dom");

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _auto_sizer = require("../../components/auto_sizer");

var _drag_drop_context_wrapper = require("../../components/drag_and_drop/drag_drop_context_wrapper");

var _flyout = require("../../components/flyout");

var _help_menu = require("../../components/help_menu");

var _link_to = require("../../components/link_to");

var _navigation = require("../../components/navigation");

var _timeline = require("../../components/timeline");

var _auto_save_warning = require("../../components/timeline/auto_save_warning");

var _ = require("../404");

var _hosts = require("../hosts");

var _network = require("../network");

var _overview = require("../overview");

var _timelines = require("../timelines");

var _source = require("../../containers/source");

var _ml_popover = require("../../components/ml_popover/ml_popover");

var _ml_host_conditional_container = require("../../components/ml/conditional_links/ml_host_conditional_container");

var _ml_network_conditional_container = require("../../components/ml/conditional_links/ml_network_conditional_container");

var _home_navigations = require("./home_navigations");

var _url_state = require("../../components/url_state");

var _spy_routes = require("../../utils/route/spy_routes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WrappedByAutoSizer = _styledComponents.default.div(_templateObject());

WrappedByAutoSizer.displayName = 'WrappedByAutoSizer';
var gutterTimeline = '70px'; // Temporary until timeline is moved - MichaelMarcialis

var Page = (0, _styledComponents.default)(_eui.EuiPage)(_templateObject2(), function (_ref) {
  var theme = _ref.theme;
  return "\n    padding: 0 ".concat(gutterTimeline, " ").concat(theme.eui.euiSizeL, " ").concat(theme.eui.euiSizeL, ";\n  ");
});
Page.displayName = 'Page';

var NavGlobal = _styledComponents.default.nav(_templateObject3(), function (_ref2) {
  var theme = _ref2.theme;
  return "\n    background: ".concat(theme.eui.euiColorEmptyShade, ";\n    border-bottom: ").concat(theme.eui.euiBorderThin, ";\n    margin: 0 -").concat(gutterTimeline, " 0 -").concat(theme.eui.euiSizeL, ";\n    padding: ").concat(theme.eui.euiSize, " ").concat(gutterTimeline, " ").concat(theme.eui.euiSize, " ").concat(theme.eui.euiSizeL, ";\n  ");
});

NavGlobal.displayName = 'NavGlobal';
var usersViewing = ['elastic']; // TODO: get the users viewing this timeline from Elasticsearch (persistance)

/** the global Kibana navigation at the top of every page */

var globalHeaderHeightPx = 48;

var calculateFlyoutHeight = function calculateFlyoutHeight(_ref3) {
  var globalHeaderSize = _ref3.globalHeaderSize,
      windowHeight = _ref3.windowHeight;
  return Math.max(0, windowHeight - globalHeaderSize);
};

var HomePage = (0, _recompose.pure)(function () {
  return React.createElement(_auto_sizer.AutoSizer, {
    detectAnyWindowResize: true,
    content: true
  }, function (_ref4) {
    var measureRef = _ref4.measureRef,
        _ref4$windowMeasureme = _ref4.windowMeasurement.height,
        windowHeight = _ref4$windowMeasureme === void 0 ? 0 : _ref4$windowMeasureme;
    return React.createElement(WrappedByAutoSizer, {
      "data-test-subj": "wrapped-by-auto-sizer",
      innerRef: measureRef
    }, React.createElement(Page, {
      "data-test-subj": "pageContainer"
    }, React.createElement(_help_menu.HelpMenu, null), React.createElement(_source.WithSource, {
      sourceId: "default"
    }, function (_ref5) {
      var browserFields = _ref5.browserFields,
          indexPattern = _ref5.indexPattern;
      return React.createElement(_drag_drop_context_wrapper.DragDropContextWrapper, {
        browserFields: browserFields
      }, React.createElement(_url_state.UseUrlState, {
        indexPattern: indexPattern,
        navTabs: _home_navigations.navTabs
      }), React.createElement(_auto_save_warning.AutoSaveWarningMsg, null), React.createElement(_flyout.Flyout, {
        flyoutHeight: calculateFlyoutHeight({
          globalHeaderSize: globalHeaderHeightPx,
          windowHeight: windowHeight
        }),
        headerHeight: _flyout.flyoutHeaderHeight,
        timelineId: "timeline-1",
        usersViewing: usersViewing
      }, React.createElement(_timeline.StatefulTimeline, {
        flyoutHeaderHeight: _flyout.flyoutHeaderHeight,
        flyoutHeight: calculateFlyoutHeight({
          globalHeaderSize: globalHeaderHeightPx,
          windowHeight: windowHeight
        }),
        id: "timeline-1"
      })), React.createElement(_eui.EuiPageBody, null, React.createElement(NavGlobal, null, React.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        gutterSize: "m",
        justifyContent: "spaceBetween"
      }, React.createElement(_eui.EuiFlexItem, null, React.createElement(_navigation.SiemNavigation, {
        navTabs: _home_navigations.navTabs
      })), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiFlexGroup, {
        alignItems: "center",
        gutterSize: "m",
        responsive: false,
        wrap: true
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_ml_popover.MlPopover, null)), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_eui.EuiButton, {
        "data-test-subj": "add-data",
        href: "kibana#home/tutorial_directory/siem",
        iconType: "plusInCircle"
      }, React.createElement(_react.FormattedMessage, {
        id: "xpack.siem.global.addData",
        defaultMessage: "Add data"
      }))))))), React.createElement(_reactRouterDom.Switch, null, React.createElement(_reactRouterDom.Redirect, {
        from: "/",
        exact: true,
        to: "/overview"
      }), React.createElement(_reactRouterDom.Route, {
        path: "/:pageName(overview)",
        render: function render() {
          return React.createElement(_overview.Overview, null);
        }
      }), React.createElement(_reactRouterDom.Route, {
        path: "/:pageName(hosts)",
        render: function render(_ref6) {
          var match = _ref6.match,
              location = _ref6.location;
          return React.createElement(_hosts.HostsContainer, {
            url: match.url,
            location: location
          });
        }
      }), React.createElement(_reactRouterDom.Route, {
        path: "/:pageName(network)",
        render: function render(_ref7) {
          var match = _ref7.match,
              location = _ref7.location;
          return React.createElement(_network.NetworkContainer, {
            url: match.url,
            location: location
          });
        }
      }), React.createElement(_reactRouterDom.Route, {
        path: "/:pageName(timelines)",
        render: function render() {
          return React.createElement(_timelines.Timelines, null);
        }
      }), React.createElement(_reactRouterDom.Route, {
        path: "/link-to",
        component: _link_to.LinkToPage
      }), React.createElement(_reactRouterDom.Route, {
        path: "/ml-hosts",
        render: function render(_ref8) {
          var match = _ref8.match,
              location = _ref8.location;
          return React.createElement(_ml_host_conditional_container.MlHostConditionalContainer, {
            url: match.url,
            location: location
          });
        }
      }), React.createElement(_reactRouterDom.Route, {
        path: "/ml-network",
        render: function render(_ref9) {
          var match = _ref9.match,
              location = _ref9.location;
          return React.createElement(_ml_network_conditional_container.MlNetworkConditionalContainer, {
            url: match.url,
            location: location
          });
        }
      }), React.createElement(_reactRouterDom.Route, {
        component: _.NotFoundPage
      }))));
    })), React.createElement(_spy_routes.SpyRoute, null));
  });
});
exports.HomePage = HomePage;
HomePage.displayName = 'HomePage';