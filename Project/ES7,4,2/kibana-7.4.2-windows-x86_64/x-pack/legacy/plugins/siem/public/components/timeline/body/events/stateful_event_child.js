"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulEventChild = exports.getNewNoteId = void 0;

var React = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _uuid = _interopRequireDefault(require("uuid"));

var _event_column_view = require("./event_column_view");

var _note_cards = require("../../../notes/note_cards");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getNewNoteId = function getNewNoteId() {
  return _uuid.default.v4();
};

exports.getNewNoteId = getNewNoteId;
var emptyNotes = [];
var StatefulEventChild = React.memo(function (_ref) {
  var id = _ref.id,
      actionsColumnWidth = _ref.actionsColumnWidth,
      associateNote = _ref.associateNote,
      addNoteToEvent = _ref.addNoteToEvent,
      onPinEvent = _ref.onPinEvent,
      columnHeaders = _ref.columnHeaders,
      columnRenderers = _ref.columnRenderers,
      expanded = _ref.expanded,
      data = _ref.data,
      eventIdToNoteIds = _ref.eventIdToNoteIds,
      getNotesByIds = _ref.getNotesByIds,
      _ref$isEventViewer = _ref.isEventViewer,
      isEventViewer = _ref$isEventViewer === void 0 ? false : _ref$isEventViewer,
      loading = _ref.loading,
      onColumnResized = _ref.onColumnResized,
      onToggleExpanded = _ref.onToggleExpanded,
      onUnPinEvent = _ref.onUnPinEvent,
      pinnedEventIds = _ref.pinnedEventIds,
      showNotes = _ref.showNotes,
      timelineId = _ref.timelineId,
      onToggleShowNotes = _ref.onToggleShowNotes,
      updateNote = _ref.updateNote;
  return React.createElement(_eui.EuiFlexGroup, {
    "data-test-subj": "event-rows",
    direction: "column",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "event-column-data",
    grow: false
  }, React.createElement(_event_column_view.EventColumnView, {
    id: id,
    actionsColumnWidth: actionsColumnWidth,
    associateNote: associateNote(id, addNoteToEvent, onPinEvent),
    columnHeaders: columnHeaders,
    columnRenderers: columnRenderers,
    data: data,
    expanded: expanded,
    eventIdToNoteIds: eventIdToNoteIds,
    getNotesByIds: getNotesByIds,
    isEventViewer: isEventViewer,
    loading: loading,
    onColumnResized: onColumnResized,
    onEventToggled: onToggleExpanded(id),
    onPinEvent: onPinEvent,
    onUnPinEvent: onUnPinEvent,
    pinnedEventIds: pinnedEventIds,
    showNotes: showNotes,
    timelineId: timelineId,
    toggleShowNotes: onToggleShowNotes(id),
    updateNote: updateNote
  })), React.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "event-notes-flex-item",
    grow: false
  }, React.createElement(_note_cards.NoteCards, {
    associateNote: associateNote(id, addNoteToEvent, onPinEvent),
    "data-test-subj": "note-cards",
    getNewNoteId: getNewNoteId,
    getNotesByIds: getNotesByIds,
    noteIds: eventIdToNoteIds[id] || emptyNotes,
    showAddNote: showNotes,
    toggleShowAddNote: onToggleShowNotes(id),
    updateNote: updateNote
  })));
});
exports.StatefulEventChild = StatefulEventChild;
StatefulEventChild.displayName = 'StatefulEventChild';