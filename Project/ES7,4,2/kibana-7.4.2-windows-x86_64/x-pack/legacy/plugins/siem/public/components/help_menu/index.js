"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HelpMenu = void 0;

var _reactDom = require("react-dom");

var _react = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _help_menu = require("./help_menu");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HelpMenu = (0, _recompose.pure)(function () {
  (0, _react.useEffect)(function () {
    _chrome.default.helpExtension.set(function (domNode) {
      (0, _reactDom.render)(_react.default.createElement(_help_menu.HelpMenuComponent, null), domNode);
      return function () {
        (0, _reactDom.unmountComponentAtNode)(domNode);
      };
    });
  }, []);
  return null;
});
exports.HelpMenu = HelpMenu;
HelpMenu.displayName = 'HelpMenu';