"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LocalizedDateTooltip = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LocalizedDateTooltip = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children,
      date = _ref.date,
      fieldName = _ref.fieldName;
  return React.createElement(_eui.EuiToolTip, {
    "data-test-subj": "localized-date-tool-tip",
    content: React.createElement(_eui.EuiFlexGroup, {
      "data-test-subj": "dates-container",
      direction: "column",
      gutterSize: "none"
    }, fieldName != null ? React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement("span", {
      "data-test-subj": "field-name"
    }, fieldName)) : null, React.createElement(_eui.EuiFlexItem, {
      grow: false
    }, React.createElement(_react.FormattedRelative, {
      "data-test-subj": "humanized-relative-date",
      value: _moment.default.utc(date).toDate()
    })), React.createElement(_eui.EuiFlexItem, {
      "data-test-subj": "with-day-of-week",
      grow: false
    }, _moment.default.utc(date).local().format('llll')), React.createElement(_eui.EuiFlexItem, {
      "data-test-subj": "with-time-zone-offset-in-hours",
      grow: false
    }, (0, _moment.default)(date).format()))
  }, React.createElement(React.Fragment, null, children));
});
exports.LocalizedDateTooltip = LocalizedDateTooltip;
LocalizedDateTooltip.displayName = 'LocalizedDateTooltip';