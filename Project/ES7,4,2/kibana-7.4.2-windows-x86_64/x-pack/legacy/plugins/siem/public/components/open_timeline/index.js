"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulOpenTimeline = exports.StatefulOpenTimelineComponent = exports.getSelectedTimelineIds = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _default_headers = require("../../components/timeline/body/column_headers/default_headers");

var _persist = require("../../containers/timeline/delete/persist.gql_query");

var _index = require("../../containers/timeline/all/index.gql_query");

var _store = require("../../store");

var _actions = require("../../store/timeline/actions");

var _open_timeline = require("./open_timeline");

var _helpers = require("./helpers");

var _open_timeline_modal = require("./open_timeline_modal/open_timeline_modal");

var _all = require("../../containers/timeline/all");

var _constants = require("./constants");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

/** Returns a collection of selected timeline ids */
var getSelectedTimelineIds = function getSelectedTimelineIds(selectedItems) {
  return selectedItems.reduce(function (validSelections, timelineResult) {
    return timelineResult.savedObjectId != null ? [].concat(_toConsumableArray(validSelections), [timelineResult.savedObjectId]) : validSelections;
  }, []);
};
/** Manages the state (e.g table selection) of the (pure) `OpenTimeline` component */


exports.getSelectedTimelineIds = getSelectedTimelineIds;

var StatefulOpenTimelineComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(StatefulOpenTimelineComponent, _React$PureComponent);

  function StatefulOpenTimelineComponent(props) {
    var _this;

    _classCallCheck(this, StatefulOpenTimelineComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(StatefulOpenTimelineComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "onQueryChange", function (query) {
      _this.setState({
        search: query.queryText.trim()
      });
    });

    _defineProperty(_assertThisInitialized(_this), "focusInput", function () {
      var elements = document.querySelector(".".concat(_helpers.OPEN_TIMELINE_CLASS_NAME, " input"));

      if (elements != null) {
        elements.focus();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onDeleteOneTimeline", function (timelineIds) {
      var _this$state = _this.state,
          onlyFavorites = _this$state.onlyFavorites,
          pageIndex = _this$state.pageIndex,
          pageSize = _this$state.pageSize,
          search = _this$state.search,
          sortDirection = _this$state.sortDirection,
          sortField = _this$state.sortField;

      _this.deleteTimelines(timelineIds, {
        search: search,
        pageInfo: {
          pageIndex: pageIndex + 1,
          pageSize: pageSize
        },
        sort: {
          sortField: sortField,
          sortOrder: sortDirection
        },
        onlyUserFavorite: onlyFavorites
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onDeleteSelected", function () {
      var _this$state2 = _this.state,
          selectedItems = _this$state2.selectedItems,
          onlyFavorites = _this$state2.onlyFavorites;

      _this.deleteTimelines(getSelectedTimelineIds(selectedItems), {
        search: _this.state.search,
        pageInfo: {
          pageIndex: _this.state.pageIndex + 1,
          pageSize: _this.state.pageSize
        },
        sort: {
          sortField: _this.state.sortField,
          sortOrder: _this.state.sortDirection
        },
        onlyUserFavorite: onlyFavorites
      }); // NOTE: we clear the selection state below, but if the server fails to
      // delete a timeline, it will remain selected in the table:


      _this.resetSelectionState(); // TODO: the query must re-execute to show the results of the deletion

    });

    _defineProperty(_assertThisInitialized(_this), "onSelectionChange", function (selectedItems) {
      _this.setState({
        selectedItems: selectedItems
      }); // <-- this is NOT passed down as props to the table: https://github.com/elastic/eui/issues/1077

    });

    _defineProperty(_assertThisInitialized(_this), "onTableChange", function (_ref) {
      var page = _ref.page,
          sort = _ref.sort;
      var pageIndex = page.index,
          pageSize = page.size;
      var sortField = sort.field,
          sortDirection = sort.direction;

      _this.setState({
        pageIndex: pageIndex,
        pageSize: pageSize,
        sortDirection: sortDirection,
        sortField: sortField
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleOnlyFavorites", function () {
      _this.setState(function (state) {
        return {
          onlyFavorites: !state.onlyFavorites
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onToggleShowNotes", function (itemIdToExpandedNotesRowMap) {
      _this.setState(function () {
        return {
          itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "resetSelectionState", function () {
      _this.setState({
        selectedItems: []
      });
    });

    _defineProperty(_assertThisInitialized(_this), "openTimeline", function (_ref2) {
      var duplicate = _ref2.duplicate,
          timelineId = _ref2.timelineId;
      var _this$props = _this.props,
          apolloClient = _this$props.apolloClient,
          closeModalTimeline = _this$props.closeModalTimeline,
          isModal = _this$props.isModal,
          updateTimeline = _this$props.updateTimeline,
          updateIsLoading = _this$props.updateIsLoading;

      if (isModal && closeModalTimeline != null) {
        closeModalTimeline();
      }

      (0, _helpers.queryTimelineById)({
        apolloClient: apolloClient,
        duplicate: duplicate,
        timelineId: timelineId,
        updateIsLoading: updateIsLoading,
        updateTimeline: updateTimeline
      });
    });

    _defineProperty(_assertThisInitialized(_this), "deleteTimelines", function (timelineIds, variables) {
      if (timelineIds.includes(_this.props.timeline.savedObjectId || '')) {
        _this.props.createNewTimeline({
          id: 'timeline-1',
          columns: _default_headers.defaultHeaders,
          show: false
        });
      }

      _this.props.apolloClient.mutate({
        mutation: _persist.deleteTimelineMutation,
        fetchPolicy: 'no-cache',
        variables: {
          id: timelineIds
        },
        refetchQueries: [{
          query: _index.allTimelinesQuery,
          variables: variables
        }]
      });
    });

    _this.state = {
      itemIdToExpandedNotesRowMap: {},
      onlyFavorites: false,
      search: '',
      pageIndex: 0,
      pageSize: props.defaultPageSize,
      sortField: _constants.DEFAULT_SORT_FIELD,
      sortDirection: _constants.DEFAULT_SORT_DIRECTION,
      selectedItems: []
    };
    return _this;
  }

  _createClass(StatefulOpenTimelineComponent, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.focusInput();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          defaultPageSize = _this$props2.defaultPageSize,
          _this$props2$isModal = _this$props2.isModal,
          isModal = _this$props2$isModal === void 0 ? false : _this$props2$isModal,
          title = _this$props2.title;
      var _this$state3 = this.state,
          itemIdToExpandedNotesRowMap = _this$state3.itemIdToExpandedNotesRowMap,
          onlyFavorites = _this$state3.onlyFavorites,
          pageIndex = _this$state3.pageIndex,
          pageSize = _this$state3.pageSize,
          query = _this$state3.search,
          selectedItems = _this$state3.selectedItems,
          sortDirection = _this$state3.sortDirection,
          sortField = _this$state3.sortField;
      return React.createElement(_all.AllTimelinesQuery, {
        pageInfo: {
          pageIndex: pageIndex + 1,
          pageSize: pageSize
        },
        search: query,
        sort: {
          sortField: sortField,
          sortOrder: sortDirection
        },
        onlyUserFavorite: onlyFavorites
      }, function (_ref3) {
        var timelines = _ref3.timelines,
            loading = _ref3.loading,
            totalCount = _ref3.totalCount;
        return !isModal ? React.createElement(_open_timeline.OpenTimeline, {
          deleteTimelines: _this2.onDeleteOneTimeline,
          defaultPageSize: defaultPageSize,
          isLoading: loading,
          itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap,
          onAddTimelinesToFavorites: undefined,
          onDeleteSelected: _this2.onDeleteSelected,
          onlyFavorites: onlyFavorites,
          onOpenTimeline: _this2.openTimeline,
          onQueryChange: _this2.onQueryChange,
          onSelectionChange: _this2.onSelectionChange,
          onTableChange: _this2.onTableChange,
          onToggleOnlyFavorites: _this2.onToggleOnlyFavorites,
          onToggleShowNotes: _this2.onToggleShowNotes,
          pageIndex: pageIndex,
          pageSize: pageSize,
          query: query,
          searchResults: timelines,
          selectedItems: selectedItems,
          sortDirection: sortDirection,
          sortField: sortField,
          title: title,
          totalSearchResultsCount: totalCount
        }) : React.createElement(_open_timeline_modal.OpenTimelineModal, {
          deleteTimelines: _this2.onDeleteOneTimeline,
          defaultPageSize: defaultPageSize,
          isLoading: loading,
          itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap,
          onAddTimelinesToFavorites: undefined,
          onlyFavorites: onlyFavorites,
          onOpenTimeline: _this2.openTimeline,
          onQueryChange: _this2.onQueryChange,
          onSelectionChange: _this2.onSelectionChange,
          onTableChange: _this2.onTableChange,
          onToggleOnlyFavorites: _this2.onToggleOnlyFavorites,
          onToggleShowNotes: _this2.onToggleShowNotes,
          pageIndex: pageIndex,
          pageSize: pageSize,
          query: query,
          searchResults: timelines,
          selectedItems: selectedItems,
          sortDirection: sortDirection,
          sortField: sortField,
          title: title,
          totalSearchResultsCount: totalCount
        });
      });
    }
    /** Invoked when the user presses enters to submit the text in the search input */

  }]);

  return StatefulOpenTimelineComponent;
}(React.PureComponent);

exports.StatefulOpenTimelineComponent = StatefulOpenTimelineComponent;

var makeMapStateToProps = function makeMapStateToProps() {
  var getTimeline = _store.timelineSelectors.getTimelineByIdSelector();

  var mapStateToProps = function mapStateToProps(state) {
    var timeline = getTimeline(state, 'timeline-1');
    return {
      timeline: timeline
    };
  };

  return mapStateToProps;
};

var mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return {
    createNewTimeline: function createNewTimeline(_ref4) {
      var id = _ref4.id,
          columns = _ref4.columns,
          show = _ref4.show;
      return dispatch((0, _actions.createTimeline)({
        id: id,
        columns: columns,
        show: show
      }));
    },
    updateIsLoading: function updateIsLoading(_ref5) {
      var id = _ref5.id,
          isLoading = _ref5.isLoading;
      return dispatch((0, _actions.updateIsLoading)({
        id: id,
        isLoading: isLoading
      }));
    },
    updateTimeline: (0, _helpers.dispatchUpdateTimeline)(dispatch)
  };
};

var StatefulOpenTimeline = (0, _reactRedux.connect)(makeMapStateToProps, mapDispatchToProps)(StatefulOpenTimelineComponent);
exports.StatefulOpenTimeline = StatefulOpenTimeline;