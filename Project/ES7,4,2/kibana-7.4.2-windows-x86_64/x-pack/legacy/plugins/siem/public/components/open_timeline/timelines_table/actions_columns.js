"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getActionsColumns = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _common_styles = require("./common_styles");

var _delete_timeline_modal = require("../delete_timeline_modal");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns the action columns (e.g. delete, open duplicate timeline)
 */
var getActionsColumns = function getActionsColumns(_ref) {
  var onOpenTimeline = _ref.onOpenTimeline,
      deleteTimelines = _ref.deleteTimelines,
      showDeleteAction = _ref.showDeleteAction;
  var openAsDuplicateColumn = {
    align: 'center',
    field: 'savedObjectId',
    name: '',
    render: function render(savedObjectId, timelineResult) {
      return React.createElement(_eui.EuiToolTip, {
        content: i18n.OPEN_AS_DUPLICATE
      }, React.createElement(_eui.EuiButtonIcon, {
        "aria-label": i18n.OPEN_AS_DUPLICATE,
        "data-test-subj": "open-duplicate",
        isDisabled: savedObjectId == null,
        iconSize: "s",
        iconType: "copy",
        onClick: function onClick() {
          return onOpenTimeline({
            duplicate: true,
            timelineId: "".concat(timelineResult.savedObjectId)
          });
        },
        size: "s"
      }));
    },
    sortable: false,
    width: _common_styles.ACTION_COLUMN_WIDTH
  };
  var deleteTimelineColumn = {
    align: 'center',
    field: 'savedObjectId',
    name: '',
    render: function render(savedObjectId, _ref2) {
      var title = _ref2.title;
      return React.createElement(_delete_timeline_modal.DeleteTimelineModalButton, {
        deleteTimelines: deleteTimelines,
        savedObjectId: savedObjectId,
        title: title
      });
    },
    sortable: false,
    width: _common_styles.ACTION_COLUMN_WIDTH
  };
  return showDeleteAction && deleteTimelines != null ? [openAsDuplicateColumn, deleteTimelineColumn] : [openAsDuplicateColumn];
};

exports.getActionsColumns = getActionsColumns;