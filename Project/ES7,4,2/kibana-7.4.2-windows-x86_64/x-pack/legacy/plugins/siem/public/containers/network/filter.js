"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NetworkFilter = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _keury = require("../../lib/keury");

var _store = require("../../store");

var _actions = require("../../store/actions");

var _use_update_kql = require("../../utils/kql/use_update_kql");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var NetworkFilterComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(NetworkFilterComponent, _React$PureComponent);

  function NetworkFilterComponent(props) {
    var _this;

    _classCallCheck(this, NetworkFilterComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(NetworkFilterComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedApplyFilterQueryFromKueryExpression", void 0);

    _defineProperty(_assertThisInitialized(_this), "memoizedSetFilterQueryDraftFromKueryExpression", void 0);

    _defineProperty(_assertThisInitialized(_this), "applyFilterQueryFromKueryExpression", function (expression) {
      return _this.props.applyNetworkFilterQuery({
        filterQuery: {
          kuery: {
            kind: 'kuery',
            expression: expression
          },
          serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(expression, _this.props.indexPattern)
        },
        networkType: _this.props.type
      });
    });

    _defineProperty(_assertThisInitialized(_this), "setFilterQueryDraftFromKueryExpression", function (expression) {
      return _this.props.setNetworkFilterQueryDraft({
        filterQueryDraft: {
          kind: 'kuery',
          expression: expression
        },
        networkType: _this.props.type
      });
    });

    _this.memoizedApplyFilterQueryFromKueryExpression = (0, _memoizeOne.default)(_this.applyFilterQueryFromKueryExpression);
    _this.memoizedSetFilterQueryDraftFromKueryExpression = (0, _memoizeOne.default)(_this.setFilterQueryDraftFromKueryExpression);
    return _this;
  }

  _createClass(NetworkFilterComponent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props = this.props,
          indexPattern = _this$props.indexPattern,
          networkFilterQueryDraft = _this$props.networkFilterQueryDraft,
          kueryFilterQuery = _this$props.kueryFilterQuery,
          setQuery = _this$props.setQuery,
          type = _this$props.type;

      if (setQuery && (!(0, _fp.isEqual)(prevProps.networkFilterQueryDraft, networkFilterQueryDraft) || !(0, _fp.isEqual)(prevProps.kueryFilterQuery, kueryFilterQuery) || prevProps.type !== type)) {
        setQuery({
          id: 'kql',
          inspect: null,
          loading: false,
          refetch: (0, _use_update_kql.useUpdateKql)({
            indexPattern: indexPattern,
            kueryFilterQuery: kueryFilterQuery,
            kueryFilterQueryDraft: networkFilterQueryDraft,
            storeType: 'networkType',
            type: type
          })
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          children = _this$props2.children,
          networkFilterQueryDraft = _this$props2.networkFilterQueryDraft,
          isNetworkFilterQueryDraftValid = _this$props2.isNetworkFilterQueryDraftValid;
      return _react.default.createElement(_react.default.Fragment, null, children({
        applyFilterQueryFromKueryExpression: this.memoizedApplyFilterQueryFromKueryExpression,
        filterQueryDraft: networkFilterQueryDraft,
        isFilterQueryDraftValid: isNetworkFilterQueryDraftValid,
        setFilterQueryDraftFromKueryExpression: this.memoizedSetFilterQueryDraftFromKueryExpression
      }));
    }
  }]);

  return NetworkFilterComponent;
}(_react.default.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getNetworkFilterQueryDraft = _store.networkSelectors.networkFilterQueryDraft();

  var getIsNetworkFilterQueryDraftValid = _store.networkSelectors.isNetworkFilterQueryDraftValid();

  var getNetworkKueryFilterQuery = _store.networkSelectors.networkFilterQueryAsKuery();

  var mapStateToProps = function mapStateToProps(state, _ref) {
    var type = _ref.type;
    return {
      networkFilterQueryDraft: getNetworkFilterQueryDraft(state, type),
      isNetworkFilterQueryDraftValid: getIsNetworkFilterQueryDraftValid(state, type),
      kueryFilterQuery: getNetworkKueryFilterQuery(state, type)
    };
  };

  return mapStateToProps;
};

var NetworkFilter = (0, _reactRedux.connect)(makeMapStateToProps, {
  applyNetworkFilterQuery: _actions.networkActions.applyNetworkFilterQuery,
  setNetworkFilterQueryDraft: _actions.networkActions.setNetworkFilterQueryDraft
})(NetworkFilterComponent);
exports.NetworkFilter = NetworkFilter;