"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UsersTable = exports.usersTableId = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _network = require("../../../../store/network");

var _types = require("../../../../graphql/types");

var _store = require("../../../../store");

var _paginated_table = require("../../../paginated_table");

var _columns = require("./columns");

var i18n = _interopRequireWildcard(require("./translations"));

var _helpers = require("../../../../lib/helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var tableType = _store.networkModel.IpDetailsTableType.users;
var rowItems = [{
  text: i18n.ROWS_5,
  numberOfRow: 5
}, {
  text: i18n.ROWS_10,
  numberOfRow: 10
}];
var usersTableId = 'users-table';
exports.usersTableId = usersTableId;

var UsersTableComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(UsersTableComponent, _React$PureComponent);

  function UsersTableComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, UsersTableComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(UsersTableComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onChange", function (criteria) {
      if (criteria.sort != null) {
        var splitField = criteria.sort.field.split('.');
        var newUsersSort = {
          field: getSortFromString(splitField[splitField.length - 1]),
          direction: criteria.sort.direction
        };

        if (!(0, _fp.isEqual)(newUsersSort, _this.props.usersSortField)) {
          _this.props.updateUsersSort({
            usersSortField: newUsersSort,
            networkType: _this.props.type
          });
        }
      }
    });

    return _this;
  }

  _createClass(UsersTableComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          activePage = _this$props.activePage,
          data = _this$props.data,
          fakeTotalCount = _this$props.fakeTotalCount,
          flowTarget = _this$props.flowTarget,
          id = _this$props.id,
          isInspect = _this$props.isInspect,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _loadPage = _this$props.loadPage,
          showMorePagesIndicator = _this$props.showMorePagesIndicator,
          totalCount = _this$props.totalCount,
          type = _this$props.type,
          updateTableActivePage = _this$props.updateTableActivePage,
          updateUsersLimit = _this$props.updateUsersLimit,
          usersSortField = _this$props.usersSortField;
      return _react.default.createElement(_paginated_table.PaginatedTable, {
        activePage: activePage,
        columns: (0, _columns.getUsersColumns)(flowTarget, usersTableId),
        showMorePagesIndicator: showMorePagesIndicator,
        headerCount: totalCount,
        headerTitle: i18n.USERS,
        headerUnit: i18n.UNIT(totalCount),
        id: id,
        isInspect: isInspect,
        itemsPerRow: rowItems,
        limit: limit,
        loading: loading,
        loadPage: function loadPage(newActivePage) {
          return _loadPage(newActivePage);
        },
        onChange: this.onChange,
        pageOfItems: data,
        sorting: getSortField(usersSortField),
        totalCount: fakeTotalCount,
        updateActivePage: function updateActivePage(newPage) {
          return updateTableActivePage({
            activePage: newPage,
            tableType: tableType
          });
        },
        updateLimitPagination: function updateLimitPagination(newLimit) {
          return updateUsersLimit({
            limit: newLimit,
            networkType: type
          });
        }
      });
    }
  }]);

  return UsersTableComponent;
}(_react.default.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getUsersSelector = _store.networkSelectors.usersSelector();

  return function (state) {
    return _objectSpread({}, getUsersSelector(state));
  };
};

var UsersTable = (0, _reactRedux.connect)(makeMapStateToProps, {
  updateTableActivePage: _network.networkActions.updateIpDetailsTableActivePage,
  updateUsersLimit: _network.networkActions.updateUsersLimit,
  updateUsersSort: _network.networkActions.updateUsersSort
})(UsersTableComponent);
exports.UsersTable = UsersTable;

var getSortField = function getSortField(sortField) {
  switch (sortField.field) {
    case _types.UsersFields.name:
      return {
        field: "node.user.".concat(sortField.field),
        direction: sortField.direction
      };

    case _types.UsersFields.count:
      return {
        field: "node.user.".concat(sortField.field),
        direction: sortField.direction
      };
  }

  return (0, _helpers.assertUnreachable)(sortField.field);
};

var getSortFromString = function getSortFromString(sortField) {
  switch (sortField) {
    case _types.UsersFields.name.valueOf():
      return _types.UsersFields.name;

    case _types.UsersFields.count.valueOf():
      return _types.UsersFields.count;

    default:
      return _types.UsersFields.name;
  }
};