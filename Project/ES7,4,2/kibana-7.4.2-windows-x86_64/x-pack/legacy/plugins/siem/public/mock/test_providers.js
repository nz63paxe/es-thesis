"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TestProviderWithoutDragAndDrop = exports.TestProviders = exports.apolloClientObservable = exports.apolloClient = void 0;

var _eui_theme_dark = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_dark.json"));

var _react = require("@kbn/i18n/react");

var _apolloCacheInmemory = require("apollo-cache-inmemory");

var _apolloClient = _interopRequireDefault(require("apollo-client"));

var _apolloLink = require("apollo-link");

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _reactRedux = require("react-redux");

var _recompose = require("recompose");

var _rxjs = require("rxjs");

var _styledComponents = require("styled-components");

var _store = require("../store");

var _global_state = require("./global_state");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var state = _global_state.mockGlobalState;
var apolloClient = new _apolloClient.default({
  cache: new _apolloCacheInmemory.InMemoryCache(),
  link: new _apolloLink.ApolloLink(function (o, f) {
    return f ? f(o) : null;
  })
});
exports.apolloClient = apolloClient;
var apolloClientObservable = new _rxjs.BehaviorSubject(apolloClient);
/** A utility for wrapping children in the providers required to run most tests */

exports.apolloClientObservable = apolloClientObservable;
var TestProviders = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children,
      _ref$store = _ref.store,
      store = _ref$store === void 0 ? (0, _store.createStore)(state, apolloClientObservable) : _ref$store,
      _ref$onDragEnd = _ref.onDragEnd,
      onDragEnd = _ref$onDragEnd === void 0 ? jest.fn() : _ref$onDragEnd;
  return React.createElement(_react.I18nProvider, null, React.createElement(_reactApollo.ApolloProvider, {
    client: apolloClient
  }, React.createElement(_reactRedux.Provider, {
    store: store
  }, React.createElement(_styledComponents.ThemeProvider, {
    theme: function theme() {
      return {
        eui: _eui_theme_dark.default,
        darkMode: true
      };
    }
  }, React.createElement(_reactBeautifulDnd.DragDropContext, {
    onDragEnd: onDragEnd
  }, children)))));
});
exports.TestProviders = TestProviders;
var TestProviderWithoutDragAndDrop = (0, _recompose.pure)(function (_ref2) {
  var children = _ref2.children,
      _ref2$store = _ref2.store,
      store = _ref2$store === void 0 ? (0, _store.createStore)(state, apolloClientObservable) : _ref2$store;
  return React.createElement(_react.I18nProvider, null, React.createElement(_reactRedux.Provider, {
    store: store
  }, children));
});
exports.TestProviderWithoutDragAndDrop = TestProviderWithoutDragAndDrop;