"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Ip = exports.DESTINATION_IP_FIELD_NAME = exports.SOURCE_IP_FIELD_NAME = void 0;

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _formatted_field = require("../timeline/body/renderers/formatted_field");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SOURCE_IP_FIELD_NAME = 'source.ip';
exports.SOURCE_IP_FIELD_NAME = SOURCE_IP_FIELD_NAME;
var DESTINATION_IP_FIELD_NAME = 'destination.ip';
exports.DESTINATION_IP_FIELD_NAME = DESTINATION_IP_FIELD_NAME;
var IP_FIELD_TYPE = 'ip';
/**
 * Renders text containing a draggable IP address (e.g. `source.ip`,
 * `destination.ip`) that contains a hyperlink
 */

var Ip = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      fieldName = _ref.fieldName,
      value = _ref.value;
  return React.createElement(_formatted_field.FormattedFieldValue, {
    contextId: contextId,
    "data-test-subj": "formatted-ip",
    eventId: eventId,
    fieldName: fieldName,
    fieldType: IP_FIELD_TYPE,
    value: value
  });
});
exports.Ip = Ip;
Ip.displayName = 'Ip';