"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoadMoreTable = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _header_panel = require("../header_panel");

var _loader = require("../loader");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin-top: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  tbody {\n    th,\n    td {\n      vertical-align: top;\n    }\n\n    .euiTableCellContent {\n      display: block;\n    }\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DEFAULT_DATA_TEST_SUBJ = 'load-more-table';

var LoadMoreTable =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(LoadMoreTable, _React$PureComponent);

  function LoadMoreTable() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LoadMoreTable);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LoadMoreTable)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      loadingInitial: _this.props.headerCount === -1,
      isPopoverOpen: false,
      showInspect: false
    });

    _defineProperty(_assertThisInitialized(_this), "mouseEnter", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          showInspect: true
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "mouseLeave", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          showInspect: false
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onButtonClick", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          isPopoverOpen: !prevState.isPopoverOpen
        });
      });
    });

    _defineProperty(_assertThisInitialized(_this), "closePopover", function () {
      _this.setState(function (prevState) {
        return _objectSpread({}, prevState, {
          isPopoverOpen: false
        });
      });
    });

    return _this;
  }

  _createClass(LoadMoreTable, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          columns = _this$props.columns,
          _this$props$dataTestS = _this$props.dataTestSubj,
          dataTestSubj = _this$props$dataTestS === void 0 ? DEFAULT_DATA_TEST_SUBJ : _this$props$dataTestS,
          hasNextPage = _this$props.hasNextPage,
          headerCount = _this$props.headerCount,
          headerSupplement = _this$props.headerSupplement,
          headerTitle = _this$props.headerTitle,
          headerTooltip = _this$props.headerTooltip,
          headerUnit = _this$props.headerUnit,
          id = _this$props.id,
          itemsPerRow = _this$props.itemsPerRow,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _this$props$onChange = _this$props.onChange,
          onChange = _this$props$onChange === void 0 ? _fp.noop : _this$props$onChange,
          pageOfItems = _this$props.pageOfItems,
          _this$props$sorting = _this$props.sorting,
          sorting = _this$props$sorting === void 0 ? null : _this$props$sorting,
          updateLimitPagination = _this$props.updateLimitPagination;
      var loadingInitial = this.state.loadingInitial;

      var button = _react.default.createElement(_eui.EuiButtonEmpty, {
        size: "xs",
        color: "text",
        iconType: "arrowDown",
        iconSide: "right",
        onClick: this.onButtonClick
      }, "".concat(i18n.ROWS, ": ").concat(limit));

      var rowItems = itemsPerRow && itemsPerRow.map(function (item) {
        return _react.default.createElement(_eui.EuiContextMenuItem, {
          key: item.text,
          icon: limit === item.numberOfRow ? 'check' : 'empty',
          onClick: function onClick() {
            _this2.closePopover();

            updateLimitPagination(item.numberOfRow);
          }
        }, item.text);
      });
      return _react.default.createElement(Panel, {
        "data-test-subj": dataTestSubj,
        loading: {
          loading: loading
        },
        onMouseEnter: this.mouseEnter,
        onMouseLeave: this.mouseLeave
      }, _react.default.createElement(_header_panel.HeaderPanel, {
        id: id,
        showInspect: !loadingInitial && this.state.showInspect,
        subtitle: !loadingInitial && "".concat(i18n.SHOWING, ": ").concat(headerCount >= 0 ? headerCount.toLocaleString() : 0, " ").concat(headerUnit),
        title: headerTitle,
        tooltip: headerTooltip
      }, !loadingInitial && headerSupplement), loadingInitial ? _react.default.createElement(_eui.EuiLoadingContent, {
        "data-test-subj": "initialLoadingPanelLoadMoreTable",
        lines: 10
      }) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(BasicTable, {
        columns: columns,
        compressed: true,
        items: pageOfItems,
        onChange: onChange,
        sorting: sorting ? {
          sort: {
            field: sorting.field,
            direction: sorting.direction
          }
        } : null
      }), hasNextPage && _react.default.createElement(FooterAction, null, _react.default.createElement(_eui.EuiFlexItem, null, !(0, _fp.isEmpty)(itemsPerRow) && _react.default.createElement(_eui.EuiPopover, {
        id: "customizablePagination",
        "data-test-subj": "loadingMoreSizeRowPopover",
        button: button,
        isOpen: this.state.isPopoverOpen,
        closePopover: this.closePopover,
        panelPaddingSize: "none"
      }, _react.default.createElement(_eui.EuiContextMenuPanel, {
        items: rowItems,
        "data-test-subj": "loadingMorePickSizeRow"
      }))), _react.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": "loadingMoreButton",
        isLoading: loading,
        onClick: this.props.loadMore,
        size: "s"
      }, loading ? "".concat(i18n.LOADING) : i18n.LOAD_MORE))), loading && _react.default.createElement(_loader.Loader, {
        "data-test-subj": "loadingPanelLoadMoreTable",
        overlay: true,
        size: "xl"
      })));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (state.loadingInitial && props.headerCount >= 0) {
        return _objectSpread({}, state, {
          loadingInitial: false
        });
      }

      return null;
    }
  }]);

  return LoadMoreTable;
}(_react.default.PureComponent);

exports.LoadMoreTable = LoadMoreTable;
var Panel = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject(), function (_ref) {
  var loading = _ref.loading;
  return loading && "\n    overflow: hidden;\n  ";
});
Panel.displayName = 'Panel';
var BasicTable = (0, _styledComponents.default)(_eui.EuiBasicTable)(_templateObject2());
BasicTable.displayName = 'BasicTable';
var FooterAction = (0, _styledComponents.default)(_eui.EuiFlexGroup).attrs({
  alignItems: 'center',
  responsive: false
})(_templateObject3(), function (props) {
  return props.theme.eui.euiSizeXS;
});
FooterAction.displayName = 'FooterAction';