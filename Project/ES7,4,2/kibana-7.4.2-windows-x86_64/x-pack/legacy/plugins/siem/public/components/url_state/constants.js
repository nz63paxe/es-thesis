"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CONSTANTS = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CONSTANTS;
exports.CONSTANTS = CONSTANTS;

(function (CONSTANTS) {
  CONSTANTS["hostsDetails"] = "hosts.details";
  CONSTANTS["hostsPage"] = "hosts.page";
  CONSTANTS["kqlQuery"] = "kqlQuery";
  CONSTANTS["networkDetails"] = "network.details";
  CONSTANTS["networkPage"] = "network.page";
  CONSTANTS["overviewPage"] = "overview.page";
  CONSTANTS["timelinePage"] = "timeline.page";
  CONSTANTS["timerange"] = "timerange";
  CONSTANTS["timelineId"] = "timelineId";
  CONSTANTS["unknown"] = "unknown";
})(CONSTANTS || (exports.CONSTANTS = CONSTANTS = {}));