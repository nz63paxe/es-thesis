"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ROWS_10 = exports.ROWS_5 = exports.FIRST_LAST_SEEN_TOOLTIP = exports.LAST_SEEN = exports.UNIQUE_SERVERS = exports.UNIQUE_CLIENTS = exports.UNIQUE_SOURCES = exports.UNIQUE_DESTINATIONS = exports.PACKETS = exports.BYTES = exports.DIRECTION = exports.DOMAIN_NAME = exports.UNIT = exports.DOMAINS = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DOMAINS = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.domainsTitle', {
  defaultMessage: 'Domains'
});

exports.DOMAINS = DOMAINS;

var UNIT = function UNIT(totalCount) {
  return _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.unit', {
    values: {
      totalCount: totalCount
    },
    defaultMessage: "{totalCount, plural, =1 {domain} other {domains}}"
  });
}; // Columns


exports.UNIT = UNIT;

var DOMAIN_NAME = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.domainNameTitle', {
  defaultMessage: 'Domain name'
});

exports.DOMAIN_NAME = DOMAIN_NAME;

var DIRECTION = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.directionTitle', {
  defaultMessage: 'Direction'
});

exports.DIRECTION = DIRECTION;

var BYTES = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.bytesTitle', {
  defaultMessage: 'Bytes'
});

exports.BYTES = BYTES;

var PACKETS = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.packetsTitle', {
  defaultMessage: 'Packets'
});

exports.PACKETS = PACKETS;

var UNIQUE_DESTINATIONS = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.uniqueDestinationsTitle', {
  defaultMessage: 'Unique destinations'
});

exports.UNIQUE_DESTINATIONS = UNIQUE_DESTINATIONS;

var UNIQUE_SOURCES = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.uniqueSourcesTitle', {
  defaultMessage: 'Unique sources'
});

exports.UNIQUE_SOURCES = UNIQUE_SOURCES;

var UNIQUE_CLIENTS = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.uniqueClientsTitle', {
  defaultMessage: 'Unique servers'
});

exports.UNIQUE_CLIENTS = UNIQUE_CLIENTS;

var UNIQUE_SERVERS = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.uniqueServersTitle', {
  defaultMessage: 'Unique clients'
});

exports.UNIQUE_SERVERS = UNIQUE_SERVERS;

var LAST_SEEN = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.lastSeenTitle', {
  defaultMessage: 'Last seen'
});

exports.LAST_SEEN = LAST_SEEN;

var FIRST_LAST_SEEN_TOOLTIP = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.columns.firstLastSeenToolTip', {
  defaultMessage: 'Relative to the selected date range'
}); // Row Select


exports.FIRST_LAST_SEEN_TOOLTIP = FIRST_LAST_SEEN_TOOLTIP;

var ROWS_5 = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.rows', {
  values: {
    numRows: 5
  },
  defaultMessage: '{numRows} {numRows, plural, =0 {rows} =1 {row} other {rows}}'
});

exports.ROWS_5 = ROWS_5;

var ROWS_10 = _i18n.i18n.translate('xpack.siem.network.ipDetails.domainsTable.rows', {
  values: {
    numRows: 10
  },
  defaultMessage: '{numRows} {numRows, plural, =0 {rows} =1 {row} other {rows}}'
});

exports.ROWS_10 = ROWS_10;