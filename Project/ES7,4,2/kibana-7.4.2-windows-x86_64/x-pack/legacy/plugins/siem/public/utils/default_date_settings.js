"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseDateWithDefault = exports.parseDateString = exports.getDefaultIntervalDuration = exports.getDefaultIntervalKind = exports.getDefaultToMoment = exports.getDefaultToValue = exports.getDefaultFromMoment = exports.getDefaultFromValue = exports.getDefaultToString = exports.getDefaultFromString = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _moment = _interopRequireDefault(require("moment"));

var _fp = require("lodash/fp");

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Defaults for if everything fails including dateMath.parse(DEFAULT_FROM) or dateMath.parse(DEFAULT_TO)
// These should not really be hit unless we are in an extreme buggy state.
var DEFAULT_FROM_MOMENT = (0, _moment.default)().subtract(24, 'hours');
var DEFAULT_TO_MOMENT = (0, _moment.default)();
/**
 * Returns the default SIEM time range "from" string. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */

var getDefaultFromString = function getDefaultFromString() {
  var defaultTimeRange = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_TIME_RANGE);

  if (defaultTimeRange != null && (0, _fp.isString)(defaultTimeRange.from)) {
    return defaultTimeRange.from;
  } else {
    return _constants.DEFAULT_FROM;
  }
};
/**
 * Returns the default SIEM time range "to" string. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultFromString = getDefaultFromString;

var getDefaultToString = function getDefaultToString() {
  var defaultTimeRange = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_TIME_RANGE);

  if (defaultTimeRange != null && (0, _fp.isString)(defaultTimeRange.to)) {
    return defaultTimeRange.to;
  } else {
    return _constants.DEFAULT_TO;
  }
};
/**
 * Returns the default SIEM time range "from" Epoch. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultToString = getDefaultToString;

var getDefaultFromValue = function getDefaultFromValue() {
  return getDefaultFromMoment().valueOf();
};
/**
 * Returns the default SIEM time range "from" moment. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultFromValue = getDefaultFromValue;

var getDefaultFromMoment = function getDefaultFromMoment() {
  var defaultTimeRange = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_TIME_RANGE);

  if (defaultTimeRange != null && (0, _fp.isString)(defaultTimeRange.from)) {
    return parseDateString(defaultTimeRange.from, _constants.DEFAULT_FROM, DEFAULT_FROM_MOMENT);
  } else {
    return parseDateWithDefault(_constants.DEFAULT_FROM, DEFAULT_FROM_MOMENT);
  }
};
/**
 * Returns the default SIEM time range "to" Epoch. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultFromMoment = getDefaultFromMoment;

var getDefaultToValue = function getDefaultToValue() {
  return getDefaultToMoment().valueOf();
};
/**
 * Returns the default SIEM time range "to" moment. This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultToValue = getDefaultToValue;

var getDefaultToMoment = function getDefaultToMoment() {
  var defaultTimeRange = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_TIME_RANGE);

  if (defaultTimeRange != null && (0, _fp.isString)(defaultTimeRange.to)) {
    return parseDateString(defaultTimeRange.to, _constants.DEFAULT_TO, DEFAULT_TO_MOMENT);
  } else {
    return parseDateWithDefault(_constants.DEFAULT_TO, DEFAULT_TO_MOMENT);
  }
};
/**
 * Returns the default SIEM interval "kind". This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultToMoment = getDefaultToMoment;

var getDefaultIntervalKind = function getDefaultIntervalKind() {
  var defaultInterval = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_REFRESH_INTERVAL);

  if (defaultInterval != null && (0, _fp.isBoolean)(defaultInterval.pause)) {
    return defaultInterval.pause ? 'manual' : 'interval';
  } else {
    return _constants.DEFAULT_INTERVAL_TYPE;
  }
};
/**
 * Returns the default SIEM interval "duration". This should be used only in
 * non-ReactJS code. For ReactJS code, use the settings context hook instead
 */


exports.getDefaultIntervalKind = getDefaultIntervalKind;

var getDefaultIntervalDuration = function getDefaultIntervalDuration() {
  var defaultInterval = _chrome.default.getUiSettingsClient().get(_constants.DEFAULT_SIEM_REFRESH_INTERVAL);

  if (defaultInterval != null && (0, _fp.isNumber)(defaultInterval.value)) {
    return defaultInterval.value;
  } else {
    return _constants.DEFAULT_INTERVAL_VALUE;
  }
};

exports.getDefaultIntervalDuration = getDefaultIntervalDuration;

var parseDateString = function parseDateString(dateString, defaultDateString, defaultDate) {
  if (dateString != null) {
    return parseDateWithDefault(dateString, defaultDate);
  } else {
    return parseDateWithDefault(defaultDateString, defaultDate);
  }
};

exports.parseDateString = parseDateString;

var parseDateWithDefault = function parseDateWithDefault(dateString, defaultDate) {
  var date = _datemath.default.parse(dateString);

  if (date != null && date.isValid()) {
    return date;
  } else {
    return defaultDate;
  }
};

exports.parseDateWithDefault = parseDateWithDefault;