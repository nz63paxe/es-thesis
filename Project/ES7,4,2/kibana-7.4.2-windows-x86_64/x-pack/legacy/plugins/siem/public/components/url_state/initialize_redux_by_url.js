"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dispatchSetInitialStateFromUrl = void 0;

var _fp = require("lodash/fp");

var _actions = require("../../store/actions");

var _constants = require("./constants");

var _helpers = require("./helpers");

var _normalize_time_range = require("./normalize_time_range");

var _keury = require("../../lib/keury");

var _model = require("../../store/hosts/model");

var _model2 = require("../../store/network/model");

var _helpers2 = require("../open_timeline/helpers");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var dispatchSetInitialStateFromUrl = function dispatchSetInitialStateFromUrl(dispatch) {
  return function (_ref) {
    var apolloClient = _ref.apolloClient,
        detailName = _ref.detailName,
        indexPattern = _ref.indexPattern,
        pageName = _ref.pageName,
        updateTimeline = _ref.updateTimeline,
        updateTimelineIsLoading = _ref.updateTimelineIsLoading,
        urlStateToUpdate = _ref.urlStateToUpdate;
    return function () {
      urlStateToUpdate.forEach(function (_ref2) {
        var urlKey = _ref2.urlKey,
            newUrlStateString = _ref2.newUrlStateString;

        if (urlKey === _constants.CONSTANTS.timerange) {
          var timerangeStateData = (0, _helpers.decodeRisonUrlState)(newUrlStateString);
          var globalId = 'global';
          var globalLinkTo = {
            linkTo: (0, _fp.get)('global.linkTo', timerangeStateData)
          };
          var globalType = (0, _fp.get)('global.timerange.kind', timerangeStateData);
          var timelineId = 'timeline';
          var timelineLinkTo = {
            linkTo: (0, _fp.get)('timeline.linkTo', timerangeStateData)
          };
          var timelineType = (0, _fp.get)('timeline.timerange.kind', timerangeStateData);

          if ((0, _fp.isEmpty)(globalLinkTo.linkTo)) {
            dispatch(_actions.inputsActions.removeGlobalLinkTo());
          } else {
            dispatch(_actions.inputsActions.addGlobalLinkTo({
              linkToId: 'timeline'
            }));
          }

          if ((0, _fp.isEmpty)(timelineLinkTo.linkTo)) {
            dispatch(_actions.inputsActions.removeTimelineLinkTo());
          } else {
            dispatch(_actions.inputsActions.addTimelineLinkTo({
              linkToId: 'global'
            }));
          }

          if (timelineType) {
            if (timelineType === 'absolute') {
              var absoluteRange = (0, _normalize_time_range.normalizeTimeRange)((0, _fp.get)('timeline.timerange', timerangeStateData));
              dispatch(_actions.inputsActions.setAbsoluteRangeDatePicker(_objectSpread({}, absoluteRange, {
                id: timelineId
              })));
            }

            if (timelineType === 'relative') {
              var relativeRange = (0, _normalize_time_range.normalizeTimeRange)((0, _fp.get)('timeline.timerange', timerangeStateData));
              dispatch(_actions.inputsActions.setRelativeRangeDatePicker(_objectSpread({}, relativeRange, {
                id: timelineId
              })));
            }
          }

          if (globalType) {
            if (globalType === 'absolute') {
              var _absoluteRange = (0, _normalize_time_range.normalizeTimeRange)((0, _fp.get)('global.timerange', timerangeStateData));

              dispatch(_actions.inputsActions.setAbsoluteRangeDatePicker(_objectSpread({}, _absoluteRange, {
                id: globalId
              })));
            }

            if (globalType === 'relative') {
              var _relativeRange = (0, _normalize_time_range.normalizeTimeRange)((0, _fp.get)('global.timerange', timerangeStateData));

              dispatch(_actions.inputsActions.setRelativeRangeDatePicker(_objectSpread({}, _relativeRange, {
                id: globalId
              })));
            }
          }
        }

        if (urlKey === _constants.CONSTANTS.kqlQuery && indexPattern != null) {
          var kqlQueryStateData = (0, _helpers.decodeRisonUrlState)(newUrlStateString);

          if ((0, _helpers.isKqlForRoute)(pageName, detailName, kqlQueryStateData.queryLocation)) {
            var filterQuery = {
              kuery: kqlQueryStateData.filterQuery,
              serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(kqlQueryStateData.filterQuery ? kqlQueryStateData.filterQuery.expression : '', indexPattern)
            };
            var page = (0, _helpers.getCurrentLocation)(pageName, detailName);

            if ([_constants.CONSTANTS.hostsPage, _constants.CONSTANTS.hostsDetails].includes(page)) {
              dispatch(_actions.hostsActions.applyHostsFilterQuery({
                filterQuery: filterQuery,
                hostsType: page === _constants.CONSTANTS.hostsPage ? _model.HostsType.page : _model.HostsType.details
              }));
            } else if ([_constants.CONSTANTS.networkPage, _constants.CONSTANTS.networkDetails].includes(page)) {
              dispatch(_actions.networkActions.applyNetworkFilterQuery({
                filterQuery: filterQuery,
                networkType: page === _constants.CONSTANTS.networkPage ? _model2.NetworkType.page : _model2.NetworkType.details
              }));
            }
          }
        }

        if (urlKey === _constants.CONSTANTS.timelineId) {
          var _timelineId = (0, _helpers.decodeRisonUrlState)(newUrlStateString);

          if (_timelineId != null) {
            (0, _helpers2.queryTimelineById)({
              apolloClient: apolloClient,
              duplicate: false,
              timelineId: _timelineId,
              updateIsLoading: updateTimelineIsLoading,
              updateTimeline: updateTimeline
            });
          }
        }
      });
    };
  };
};

exports.dispatchSetInitialStateFromUrl = dispatchSetInitialStateFromUrl;