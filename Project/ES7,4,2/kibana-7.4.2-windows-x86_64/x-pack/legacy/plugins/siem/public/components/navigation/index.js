"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SiemNavigation = exports.SiemNavigationRedux = exports.SiemNavigationComponent = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _redux = require("redux");

var _reactRedux = require("react-redux");

var _use_route_spy = require("../../utils/route/use_route_spy");

var _constants = require("../url_state/constants");

var _store = require("../../store");

var _breadcrumbs = require("./breadcrumbs");

var _tab_navigation = require("./tab_navigation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var SiemNavigationComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(SiemNavigationComponent, _React$Component);

  function SiemNavigationComponent() {
    _classCallCheck(this, SiemNavigationComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(SiemNavigationComponent).apply(this, arguments));
  }

  _createClass(SiemNavigationComponent, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      if (this.props.pathName === nextProps.pathName && this.props.search === nextProps.search && (0, _fp.isEqual)(this.props.hosts, nextProps.hosts) && (0, _fp.isEqual)(this.props.hostDetails, nextProps.hostDetails) && (0, _fp.isEqual)(this.props.network, nextProps.network) && (0, _fp.isEqual)(this.props.navTabs, nextProps.navTabs) && (0, _fp.isEqual)(this.props.timerange, nextProps.timerange) && (0, _fp.isEqual)(this.props.timelineId, nextProps.timelineId)) {
        return false;
      }

      return true;
    }
  }, {
    key: "componentWillMount",
    value: function componentWillMount() {
      var _this$props = this.props,
          detailName = _this$props.detailName,
          hosts = _this$props.hosts,
          hostDetails = _this$props.hostDetails,
          navTabs = _this$props.navTabs,
          network = _this$props.network,
          pageName = _this$props.pageName,
          pathName = _this$props.pathName,
          search = _this$props.search,
          tabName = _this$props.tabName,
          timerange = _this$props.timerange,
          timelineId = _this$props.timelineId;

      if (pathName) {
        (0, _breadcrumbs.setBreadcrumbs)({
          detailName: detailName,
          hosts: hosts,
          hostDetails: hostDetails,
          navTabs: navTabs,
          network: network,
          pageName: pageName,
          pathName: pathName,
          search: search,
          tabName: tabName,
          timerange: timerange,
          timelineId: timelineId
        });
      }
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      if (this.props.pathName !== nextProps.pathName || this.props.search !== nextProps.search || !(0, _fp.isEqual)(this.props.hosts, nextProps.hosts) || !(0, _fp.isEqual)(this.props.hostDetails, nextProps.hostDetails) || !(0, _fp.isEqual)(this.props.network, nextProps.network) || !(0, _fp.isEqual)(this.props.navTabs, nextProps.navTabs) || !(0, _fp.isEqual)(this.props.timerange, nextProps.timerange) || !(0, _fp.isEqual)(this.props.timelineId, nextProps.timelineId)) {
        var detailName = nextProps.detailName,
            hosts = nextProps.hosts,
            hostDetails = nextProps.hostDetails,
            navTabs = nextProps.navTabs,
            network = nextProps.network,
            pageName = nextProps.pageName,
            pathName = nextProps.pathName,
            search = nextProps.search,
            tabName = nextProps.tabName,
            timelineId = nextProps.timelineId,
            timerange = nextProps.timerange;

        if (pathName) {
          (0, _breadcrumbs.setBreadcrumbs)({
            detailName: detailName,
            hosts: hosts,
            hostDetails: hostDetails,
            navTabs: navTabs,
            network: network,
            pageName: pageName,
            pathName: pathName,
            search: search,
            tabName: tabName,
            timerange: timerange,
            timelineId: timelineId
          });
        }
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          display = _this$props2.display,
          hostDetails = _this$props2.hostDetails,
          hosts = _this$props2.hosts,
          navTabs = _this$props2.navTabs,
          network = _this$props2.network,
          pageName = _this$props2.pageName,
          pathName = _this$props2.pathName,
          showBorder = _this$props2.showBorder,
          tabName = _this$props2.tabName,
          timelineId = _this$props2.timelineId,
          timerange = _this$props2.timerange;
      return _react.default.createElement(_tab_navigation.TabNavigation, {
        display: display,
        hosts: hosts,
        hostDetails: hostDetails,
        navTabs: navTabs,
        network: network,
        pageName: pageName,
        pathName: pathName,
        showBorder: showBorder,
        tabName: tabName,
        timelineId: timelineId,
        timerange: timerange
      });
    }
  }]);

  return SiemNavigationComponent;
}(_react.default.Component);

exports.SiemNavigationComponent = SiemNavigationComponent;

var makeMapStateToProps = function makeMapStateToProps() {
  var getInputsSelector = _store.inputsSelectors.inputsSelector();

  var getHostsFilterQueryAsKuery = _store.hostsSelectors.hostsFilterQueryAsKuery();

  var getNetworkFilterQueryAsKuery = _store.networkSelectors.networkFilterQueryAsKuery();

  var getTimelines = _store.timelineSelectors.getTimelines();

  var mapStateToProps = function mapStateToProps(state) {
    var _global, _timeline, _ref3;

    var inputState = getInputsSelector(state);
    var _inputState$global = inputState.global,
        globalLinkTo = _inputState$global.linkTo,
        globalTimerange = _inputState$global.timerange;
    var _inputState$timeline = inputState.timeline,
        timelineLinkTo = _inputState$timeline.linkTo,
        timelineTimerange = _inputState$timeline.timerange;
    var openTimelineId = Object.entries(getTimelines(state)).reduce(function (useTimelineId, _ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          timelineId = _ref2[0],
          timelineObj = _ref2[1];

      if (timelineObj.savedObjectId != null) {
        useTimelineId = timelineObj.savedObjectId;
      }

      return useTimelineId;
    }, '');
    return _ref3 = {
      hosts: {
        filterQuery: getHostsFilterQueryAsKuery(state, _store.hostsModel.HostsType.page),
        queryLocation: _constants.CONSTANTS.hostsPage
      },
      hostDetails: {
        filterQuery: getHostsFilterQueryAsKuery(state, _store.hostsModel.HostsType.details),
        queryLocation: _constants.CONSTANTS.hostsDetails
      },
      network: {
        filterQuery: getNetworkFilterQueryAsKuery(state, _store.networkModel.NetworkType.page),
        queryLocation: _constants.CONSTANTS.networkPage
      }
    }, _defineProperty(_ref3, _constants.CONSTANTS.timerange, {
      global: (_global = {}, _defineProperty(_global, _constants.CONSTANTS.timerange, globalTimerange), _defineProperty(_global, "linkTo", globalLinkTo), _global),
      timeline: (_timeline = {}, _defineProperty(_timeline, _constants.CONSTANTS.timerange, timelineTimerange), _defineProperty(_timeline, "linkTo", timelineLinkTo), _timeline)
    }), _defineProperty(_ref3, _constants.CONSTANTS.timelineId, openTimelineId), _ref3;
  };

  return mapStateToProps;
};

var SiemNavigationRedux = (0, _redux.compose)((0, _reactRedux.connect)(makeMapStateToProps))(SiemNavigationComponent);
exports.SiemNavigationRedux = SiemNavigationRedux;

var SiemNavigation = _react.default.memo(function (props) {
  var _useRouteSpy = (0, _use_route_spy.useRouteSpy)(),
      _useRouteSpy2 = _slicedToArray(_useRouteSpy, 1),
      routeProps = _useRouteSpy2[0];

  var stateNavReduxProps = _objectSpread({}, routeProps, {}, props);

  return _react.default.createElement(SiemNavigationRedux, stateNavReduxProps);
});

exports.SiemNavigation = SiemNavigation;