"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSiemJobs = exports.getSiemJobIdsFromGroupsData = void 0;

var _react = require("react");

var _api = require("../api");

var _has_ml_user_permissions = require("../../ml/permissions/has_ml_user_permissions");

var _ml_capabilities_provider = require("../../ml/permissions/ml_capabilities_provider");

var _toasters = require("../../toasters");

var _error_to_toaster = require("../../ml/api/error_to_toaster");

var _use_kibana_ui_setting = require("../../../lib/settings/use_kibana_ui_setting");

var _constants = require("../../../../common/constants");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var getSiemJobIdsFromGroupsData = function getSiemJobIdsFromGroupsData(data) {
  return data.reduce(function (jobIds, group) {
    return group.id === 'siem' ? [].concat(_toConsumableArray(jobIds), _toConsumableArray(group.jobIds)) : jobIds;
  }, []);
};

exports.getSiemJobIdsFromGroupsData = getSiemJobIdsFromGroupsData;

var useSiemJobs = function useSiemJobs(refetchData) {
  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      siemJobs = _useState2[0],
      setSiemJobs = _useState2[1];

  var _useState3 = (0, _react.useState)(true),
      _useState4 = _slicedToArray(_useState3, 2),
      loading = _useState4[0],
      setLoading = _useState4[1];

  var capabilities = (0, _react.useContext)(_ml_capabilities_provider.MlCapabilitiesContext);
  var userPermissions = (0, _has_ml_user_permissions.hasMlUserPermissions)(capabilities);

  var _useStateToaster = (0, _toasters.useStateToaster)(),
      _useStateToaster2 = _slicedToArray(_useStateToaster, 2),
      dispatchToaster = _useStateToaster2[1];

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      kbnVersion = _useKibanaUiSetting2[0];

  (0, _react.useEffect)(function () {
    var isSubscribed = true;
    var abortCtrl = new AbortController();
    setLoading(true);

    function fetchSiemJobIdsFromGroupsData() {
      return _fetchSiemJobIdsFromGroupsData.apply(this, arguments);
    }

    function _fetchSiemJobIdsFromGroupsData() {
      _fetchSiemJobIdsFromGroupsData = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var data, siemJobIds;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (!userPermissions) {
                  _context.next = 12;
                  break;
                }

                _context.prev = 1;
                _context.next = 4;
                return (0, _api.groupsData)({
                  'kbn-version': kbnVersion
                }, abortCtrl.signal);

              case 4:
                data = _context.sent;
                siemJobIds = getSiemJobIdsFromGroupsData(data);

                if (isSubscribed) {
                  setSiemJobs(siemJobIds);
                }

                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](1);

                if (isSubscribed) {
                  (0, _error_to_toaster.errorToToaster)({
                    title: i18n.SIEM_JOB_FETCH_FAILURE,
                    error: _context.t0,
                    dispatchToaster: dispatchToaster
                  });
                }

              case 12:
                if (isSubscribed) {
                  setLoading(false);
                }

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 9]]);
      }));
      return _fetchSiemJobIdsFromGroupsData.apply(this, arguments);
    }

    fetchSiemJobIdsFromGroupsData();
    return function () {
      isSubscribed = false;
      abortCtrl.abort();
    };
  }, [refetchData, userPermissions]);
  return [loading, siemJobs];
};

exports.useSiemJobs = useSiemJobs;