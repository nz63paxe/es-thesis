"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsBody = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _source = require("../../containers/source");

var _store = require("../../store");

var _score_interval_to_datetime = require("../../components/ml/score/score_interval_to_datetime");

var _actions = require("../../store/inputs/actions");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HostsBodyComponent = (0, _react.memo)(function (_ref) {
  var deleteQuery = _ref.deleteQuery,
      filterQuery = _ref.filterQuery,
      kqlQueryExpression = _ref.kqlQueryExpression,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker,
      children = _ref.children,
      to = _ref.to,
      from = _ref.from,
      setQuery = _ref.setQuery,
      isInitializing = _ref.isInitializing;
  return _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_react.default.Fragment, null, children({
      deleteQuery: deleteQuery,
      endDate: to,
      filterQuery: filterQuery,
      kqlQueryExpression: kqlQueryExpression,
      skip: isInitializing,
      setQuery: setQuery,
      startDate: from,
      type: _store.hostsModel.HostsType.page,
      indexPattern: indexPattern,
      narrowDateRange: function narrowDateRange(score, interval) {
        var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
        setAbsoluteRangeDatePicker({
          id: 'global',
          from: fromTo.from,
          to: fromTo.to
        });
      }
    })) : null;
  });
});
HostsBodyComponent.displayName = 'HostsBodyComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getHostsFilterQueryAsJson = _store.hostsSelectors.hostsFilterQueryAsJson();

  var hostsFilterQueryExpression = _store.hostsSelectors.hostsFilterQueryExpression();

  var mapStateToProps = function mapStateToProps(state) {
    return {
      filterQuery: getHostsFilterQueryAsJson(state, _store.hostsModel.HostsType.page) || '',
      kqlQueryExpression: hostsFilterQueryExpression(state, _store.hostsModel.HostsType.page) || ''
    };
  };

  return mapStateToProps;
};

var HostsBody = (0, _reactRedux.connect)(makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
})(HostsBodyComponent);
exports.HostsBody = HostsBody;