"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotePreview = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _empty_value = require("../../empty_value");

var _formatted_date = require("../../formatted_date");

var _markdown = require("../../markdown");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  & + & {\n    margin-top: ", ";\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NotePreviewGroup = _styledComponents.default.article(_templateObject(), function (props) {
  return props.theme.eui.euiSizeL;
});

NotePreviewGroup.displayName = 'NotePreviewGroup';

var NotePreviewHeader = _styledComponents.default.header(_templateObject2(), function (props) {
  return props.theme.eui.euiSizeS;
});

NotePreviewHeader.displayName = 'NotePreviewHeader';
/**
 * Renders a preview of a note in the All / Open Timelines table
 */

var NotePreview = (0, _recompose.pure)(function (_ref) {
  var note = _ref.note,
      updated = _ref.updated,
      updatedBy = _ref.updatedBy;
  return React.createElement(NotePreviewGroup, null, React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "m",
    responsive: false
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiAvatar, {
    "data-test-subj": "avatar",
    name: updatedBy != null ? updatedBy : '?',
    size: "l"
  })), React.createElement(_eui.EuiFlexItem, null, React.createElement(NotePreviewHeader, null, React.createElement(_eui.EuiTitle, {
    "data-test-subj": "updated-by",
    size: "xxs"
  }, React.createElement("h6", null, (0, _empty_value.defaultToEmptyTag)(updatedBy))), React.createElement(_eui.EuiText, {
    color: "subdued",
    "data-test-subj": "posted",
    size: "xs"
  }, React.createElement("p", null, i18n.POSTED, ' ', updated != null ? React.createElement(_eui.EuiToolTip, {
    content: React.createElement(_formatted_date.FormattedDate, {
      fieldName: "",
      value: updated
    })
  }, React.createElement(_react.FormattedRelative, {
    "data-test-subj": "updated",
    value: new Date(updated)
  })) : (0, _empty_value.getEmptyValue)()))), React.createElement(_markdown.Markdown, {
    raw: note || '',
    size: "s"
  }))));
});
exports.NotePreview = NotePreview;
NotePreview.displayName = 'NotePreview';