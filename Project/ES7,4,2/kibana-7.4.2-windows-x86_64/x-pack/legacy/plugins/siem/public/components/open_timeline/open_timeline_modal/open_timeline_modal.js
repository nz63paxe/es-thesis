"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OpenTimelineModal = exports.HeaderContainer = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _search_row = require("../search_row");

var _timelines_table = require("../timelines_table");

var _title_row = require("../title_row");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HeaderContainer = _styledComponents.default.div(_templateObject());

exports.HeaderContainer = HeaderContainer;
HeaderContainer.displayName = 'HeaderContainer';
var OpenTimelineModal = (0, _recompose.pure)(function (_ref) {
  var deleteTimelines = _ref.deleteTimelines,
      defaultPageSize = _ref.defaultPageSize,
      isLoading = _ref.isLoading,
      itemIdToExpandedNotesRowMap = _ref.itemIdToExpandedNotesRowMap,
      onAddTimelinesToFavorites = _ref.onAddTimelinesToFavorites,
      onDeleteSelected = _ref.onDeleteSelected,
      onlyFavorites = _ref.onlyFavorites,
      onOpenTimeline = _ref.onOpenTimeline,
      onQueryChange = _ref.onQueryChange,
      onSelectionChange = _ref.onSelectionChange,
      onTableChange = _ref.onTableChange,
      onToggleOnlyFavorites = _ref.onToggleOnlyFavorites,
      onToggleShowNotes = _ref.onToggleShowNotes,
      pageIndex = _ref.pageIndex,
      pageSize = _ref.pageSize,
      query = _ref.query,
      searchResults = _ref.searchResults,
      selectedItems = _ref.selectedItems,
      sortDirection = _ref.sortDirection,
      sortField = _ref.sortField,
      title = _ref.title,
      totalSearchResultsCount = _ref.totalSearchResultsCount;
  return React.createElement(React.Fragment, null, React.createElement(_eui.EuiModalHeader, null, React.createElement(HeaderContainer, null, React.createElement(_title_row.TitleRow, {
    "data-test-subj": "title-row",
    onDeleteSelected: onDeleteSelected,
    onAddTimelinesToFavorites: onAddTimelinesToFavorites,
    selectedTimelinesCount: selectedItems.length,
    title: title
  }), React.createElement(_search_row.SearchRow, {
    "data-test-subj": "search-row",
    onlyFavorites: onlyFavorites,
    onQueryChange: onQueryChange,
    onToggleOnlyFavorites: onToggleOnlyFavorites,
    query: query,
    totalSearchResultsCount: totalSearchResultsCount
  }))), React.createElement(_eui.EuiModalBody, null, React.createElement(_timelines_table.TimelinesTable, {
    "data-test-subj": "timelines-table",
    deleteTimelines: deleteTimelines,
    defaultPageSize: defaultPageSize,
    loading: isLoading,
    itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap,
    onOpenTimeline: onOpenTimeline,
    onSelectionChange: onSelectionChange,
    onTableChange: onTableChange,
    onToggleShowNotes: onToggleShowNotes,
    pageIndex: pageIndex,
    pageSize: pageSize,
    searchResults: searchResults,
    showExtendedColumnsAndActions: onDeleteSelected != null && deleteTimelines != null,
    sortDirection: sortDirection,
    sortField: sortField,
    totalSearchResultsCount: totalSearchResultsCount
  })));
});
exports.OpenTimelineModal = OpenTimelineModal;
OpenTimelineModal.displayName = 'OpenTimelineModal';