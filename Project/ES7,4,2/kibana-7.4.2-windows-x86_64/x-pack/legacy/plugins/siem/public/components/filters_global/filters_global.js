"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FiltersGlobal = void 0;

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react = _interopRequireDefault(require("react"));

var _reactSticky = require("react-sticky");

var _recompose = require("recompose");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

var _super_date_picker = require("../super_date_picker");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  .euiSuperDatePicker__flexWrapper {\n    max-width: none;\n    width: auto;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n    position: relative;\n    z-index: ", ";\n    background: ", ";\n    border-bottom: ", ";\n    box-sizing: content-box;\n    margin: 0 -", " 0 -", ";\n    padding: ", " ", " ", " ", ";\n\n    ", "\n\n    @media only ", " {\n      position: static !important;\n      z-index: ", " !important;\n    }\n  "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var offsetChrome = 49;
var gutterTimeline = '70px'; // Temporary until timeline is moved - MichaelMarcialis

var disableSticky = 'screen and (max-width: ' + _eui_theme_light.default.euiBreakpoints.s + ')';
var disableStickyMq = window.matchMedia(disableSticky);

var Aside = _styledComponents.default.aside(_templateObject(), function (props) {
  return (0, _styledComponents.css)(_templateObject2(), props.theme.eui.euiZNavigation, props.theme.eui.euiColorEmptyShade, props.theme.eui.euiBorderThin, gutterTimeline, props.theme.eui.euiSizeL, props.theme.eui.euiSize, gutterTimeline, props.theme.eui.euiSize, props.theme.eui.euiSizeL, props.isSticky && "\n      top: ".concat(offsetChrome, "px !important;\n    "), disableSticky, props.theme.eui.euiZContent);
});

Aside.displayName = 'Aside'; // Temporary fix for EuiSuperDatePicker whitespace bug and auto width - Michael Marcialis

var FlexItemWithDatePickerFix = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject3());
FlexItemWithDatePickerFix.displayName = 'FlexItemWithDatePickerFix';
var FiltersGlobal = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children;
  return _react.default.createElement(_reactSticky.Sticky, {
    disableCompensation: disableStickyMq.matches,
    topOffset: -offsetChrome
  }, function (_ref2) {
    var style = _ref2.style,
        isSticky = _ref2.isSticky;
    return _react.default.createElement(Aside, {
      isSticky: isSticky,
      style: style
    }, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
      grow: 8
    }, children), _react.default.createElement(FlexItemWithDatePickerFix, {
      grow: 4,
      "data-test-subj": "globalDatePicker"
    }, _react.default.createElement(_super_date_picker.SuperDatePicker, {
      id: "global"
    }))));
  });
});
exports.FiltersGlobal = FiltersGlobal;
FiltersGlobal.displayName = 'FiltersGlobal';