"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddNote = exports.CancelButton = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _markdown_hint = require("../../markdown/markdown_hint");

var _helpers = require("../helpers");

var i18n = _interopRequireWildcard(require("../translations"));

var _new_note = require("./new_note");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: 5px;\n  user-select: none;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var AddNotesContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
AddNotesContainer.displayName = 'AddNotesContainer';
var ButtonsContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2());
ButtonsContainer.displayName = 'ButtonsContainer';
var CancelButton = (0, _recompose.pure)(function (_ref) {
  var onCancelAddNote = _ref.onCancelAddNote;
  return React.createElement(_eui.EuiButtonEmpty, {
    "data-test-subj": "cancel",
    onClick: onCancelAddNote
  }, i18n.CANCEL);
});
exports.CancelButton = CancelButton;
CancelButton.displayName = 'CancelButton';
/** Displays an input for entering a new note, with an adjacent "Add" button */

var AddNote = (0, _recompose.pure)(function (_ref2) {
  var associateNote = _ref2.associateNote,
      getNewNoteId = _ref2.getNewNoteId,
      newNote = _ref2.newNote,
      onCancelAddNote = _ref2.onCancelAddNote,
      updateNewNote = _ref2.updateNewNote,
      updateNote = _ref2.updateNote;
  return React.createElement(AddNotesContainer, {
    alignItems: "flexEnd",
    direction: "column",
    gutterSize: "none"
  }, React.createElement(_new_note.NewNote, {
    note: newNote,
    noteInputHeight: 200,
    updateNewNote: updateNewNote
  }), React.createElement(_eui.EuiFlexItem, {
    grow: true
  }, React.createElement(_markdown_hint.MarkdownHint, {
    show: newNote.trim().length > 0
  })), React.createElement(ButtonsContainer, {
    gutterSize: "none"
  }, onCancelAddNote != null ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(CancelButton, {
    onCancelAddNote: onCancelAddNote
  })) : null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiButton, {
    "data-test-subj": "add-note",
    isDisabled: newNote.trim().length === 0,
    fill: true,
    onClick: function onClick() {
      return (0, _helpers.updateAndAssociateNode)({
        associateNote: associateNote,
        getNewNoteId: getNewNoteId,
        newNote: newNote,
        updateNewNote: updateNewNote,
        updateNote: updateNote
      });
    }
  }, i18n.ADD_NOTE))));
});
exports.AddNote = AddNote;
AddNote.displayName = 'AddNote';