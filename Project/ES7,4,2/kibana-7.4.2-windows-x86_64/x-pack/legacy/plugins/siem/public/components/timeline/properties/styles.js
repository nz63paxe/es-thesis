"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LockIconContainer = exports.Facet = exports.StyledStar = exports.LabelText = exports.ButtonContainer = exports.SmallNotesButtonContainer = exports.DescriptionContainer = exports.NameField = exports.DatePicker = exports.TimelineProperties = void 0;

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireWildcard(require("styled-components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject11() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 2px;\n"]);

  _templateObject11 = function _templateObject11() {
    return data;
  };

  return data;
}

function _templateObject10() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: inline-flex;\n  justify-content: center;\n  border-radius: 4px;\n  background: #e4e4e4;\n  color: #000;\n  font-size: 12px;\n  line-height: 16px;\n  height: 20px;\n  min-width: 20px;\n  padding-left: 8px;\n  padding-right: 8px;\n  user-select: none;\n"]);

  _templateObject10 = function _templateObject10() {
    return data;
  };

  return data;
}

function _templateObject9() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n  cursor: pointer;\n"]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 10px;\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  animation: ", " ", ";\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  cursor: pointer;\n  width: 35px;\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  animation: ", " 0.3s;\n  margin-right: 5px;\n  min-width: 150px;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  width: 150px;\n  margin-right: 5px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  width: ", ";\n  .euiSuperDatePicker__flexWrapper {\n    max-width: none;\n    width: auto;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  user-select: none;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  from { opacity: 0; }\n  to { opacity: 1; }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var fadeInEffect = (0, _styledComponents.keyframes)(_templateObject());

var TimelineProperties = _styledComponents.default.div(_templateObject2());

exports.TimelineProperties = TimelineProperties;
TimelineProperties.displayName = 'TimelineProperties';
var DatePicker = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject3(), function (_ref) {
  var width = _ref.width;
  return "".concat(width, "px");
});
exports.DatePicker = DatePicker;
DatePicker.displayName = 'DatePicker';
var NameField = (0, _styledComponents.default)(_eui.EuiFieldText)(_templateObject4());
exports.NameField = NameField;
NameField.displayName = 'NameField';

var DescriptionContainer = _styledComponents.default.div(_templateObject5(), fadeInEffect);

exports.DescriptionContainer = DescriptionContainer;
DescriptionContainer.displayName = 'DescriptionContainer';

var SmallNotesButtonContainer = _styledComponents.default.div(_templateObject6());

exports.SmallNotesButtonContainer = SmallNotesButtonContainer;
SmallNotesButtonContainer.displayName = 'SmallNotesButtonContainer';

var ButtonContainer = _styledComponents.default.div(_templateObject7(), fadeInEffect, function (_ref2) {
  var animate = _ref2.animate;
  return animate ? '0.3s' : '0s';
});

exports.ButtonContainer = ButtonContainer;
ButtonContainer.displayName = 'ButtonContainer';

var LabelText = _styledComponents.default.div(_templateObject8());

exports.LabelText = LabelText;
LabelText.displayName = 'LabelText';
var StyledStar = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject9());
exports.StyledStar = StyledStar;
StyledStar.displayName = 'StyledStar';

var Facet = _styledComponents.default.div(_templateObject10());

exports.Facet = Facet;
Facet.displayName = 'Facet';
var LockIconContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject11());
exports.LockIconContainer = LockIconContainer;
LockIconContainer.displayName = 'LockIconContainer';