"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedIp = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _draggable_wrapper = require("../drag_and_drop/draggable_wrapper");

var _helpers = require("../drag_and_drop/helpers");

var _empty_value = require("../empty_value");

var _links = require("../links");

var _data_provider = require("../timeline/data_providers/data_provider");

var _provider = require("../timeline/data_providers/provider");

var _truncatable_text = require("../truncatable_text");

var _parse_query_value = require("../timeline/body/renderers/parse_query_value");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var getUniqueId = function getUniqueId(_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      fieldName = _ref.fieldName,
      address = _ref.address;
  return "formatted-ip-data-provider-".concat(contextId, "-").concat(fieldName, "-").concat(address, "-").concat(eventId);
};

var tryStringify = function tryStringify(value) {
  try {
    return JSON.stringify(value);
  } catch (_) {
    return "".concat(value);
  }
};

var getDataProvider = function getDataProvider(_ref2) {
  var contextId = _ref2.contextId,
      eventId = _ref2.eventId,
      fieldName = _ref2.fieldName,
      address = _ref2.address;
  return {
    enabled: true,
    id: (0, _helpers.escapeDataProviderId)(getUniqueId({
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      address: address
    })),
    name: "".concat(fieldName, ": ").concat((0, _parse_query_value.parseQueryValue)(address)),
    queryMatch: {
      field: fieldName,
      value: (0, _parse_query_value.parseQueryValue)(address),
      operator: _data_provider.IS_OPERATOR
    },
    excluded: false,
    kqlQuery: '',
    and: []
  };
};

var NonDecoratedIp = (0, _recompose.pure)(function (_ref3) {
  var contextId = _ref3.contextId,
      eventId = _ref3.eventId,
      fieldName = _ref3.fieldName,
      value = _ref3.value,
      width = _ref3.width;
  return React.createElement(_draggable_wrapper.DraggableWrapper, {
    key: "non-decorated-ip-draggable-wrapper-".concat(getUniqueId({
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      address: value
    })),
    dataProvider: getDataProvider({
      contextId: contextId,
      eventId: eventId,
      fieldName: fieldName,
      address: value
    }),
    render: function render(dataProvider, _, snapshot) {
      return snapshot.isDragging ? React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_provider.Provider, {
        dataProvider: dataProvider
      })) : _typeof(value) !== 'object' ? (0, _empty_value.getOrEmptyTagFromValue)(value) : (0, _empty_value.getOrEmptyTagFromValue)(tryStringify(value));
    },
    width: width
  });
});
NonDecoratedIp.displayName = 'NonDecoratedIp';
var AddressLinks = (0, _recompose.pure)(function (_ref4) {
  var addresses = _ref4.addresses,
      eventId = _ref4.eventId,
      contextId = _ref4.contextId,
      fieldName = _ref4.fieldName,
      width = _ref4.width;
  return React.createElement(React.Fragment, null, (0, _fp.uniq)(addresses).map(function (address) {
    return React.createElement(_draggable_wrapper.DraggableWrapper, {
      key: "address-links-draggable-wrapper-".concat(getUniqueId({
        contextId: contextId,
        eventId: eventId,
        fieldName: fieldName,
        address: address
      })),
      dataProvider: getDataProvider({
        contextId: contextId,
        eventId: eventId,
        fieldName: fieldName,
        address: address
      }),
      render: function render(_, __, snapshot) {
        return snapshot.isDragging ? React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_provider.Provider, {
          dataProvider: getDataProvider({
            contextId: contextId,
            eventId: eventId,
            fieldName: fieldName,
            address: address
          })
        })) : width != null ? React.createElement(_links.IPDetailsLink, {
          "data-test-sub": "truncatable-ip-details-link",
          ip: address
        }, React.createElement(_truncatable_text.TruncatableText, {
          "data-test-sub": "truncatable-ip-details-text",
          size: "xs",
          width: width
        }, address)) : React.createElement(_links.IPDetailsLink, {
          "data-test-sub": "ip-details",
          ip: address
        });
      },
      width: width
    });
  }));
});
AddressLinks.displayName = 'AddressLinks';
var FormattedIp = (0, _recompose.pure)(function (_ref5) {
  var eventId = _ref5.eventId,
      contextId = _ref5.contextId,
      fieldName = _ref5.fieldName,
      value = _ref5.value,
      width = _ref5.width;

  if ((0, _fp.isString)(value) && !(0, _fp.isEmpty)(value)) {
    try {
      var addresses = JSON.parse(value);

      if ((0, _fp.isArray)(addresses)) {
        return React.createElement(AddressLinks, {
          addresses: addresses,
          eventId: eventId,
          contextId: contextId,
          fieldName: fieldName,
          width: width
        });
      }
    } catch (_) {} // fall back to formatting it as a single link
    // return a single draggable link


    return React.createElement(AddressLinks, {
      addresses: [value],
      eventId: eventId,
      contextId: contextId,
      fieldName: fieldName,
      width: width
    });
  } else {
    return React.createElement(NonDecoratedIp, {
      eventId: eventId,
      contextId: contextId,
      fieldName: fieldName,
      value: value,
      width: width
    });
  }
});
exports.FormattedIp = FormattedIp;
FormattedIp.displayName = 'FormattedIp';