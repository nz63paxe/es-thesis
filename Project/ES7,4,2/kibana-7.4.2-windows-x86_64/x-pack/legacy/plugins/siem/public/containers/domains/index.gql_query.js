"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.domainsQuery = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  query GetDomainsQuery(\n    $sourceId: ID!\n    $filterQuery: String\n    $flowDirection: FlowDirection!\n    $flowTarget: FlowTarget!\n    $ip: String!\n    $pagination: PaginationInputPaginated!\n    $sort: DomainsSortField!\n    $timerange: TimerangeInput!\n    $defaultIndex: [String!]!\n    $inspect: Boolean!\n  ) {\n    source(id: $sourceId) {\n      id\n      Domains(\n        filterQuery: $filterQuery\n        flowDirection: $flowDirection\n        flowTarget: $flowTarget\n        ip: $ip\n        pagination: $pagination\n        sort: $sort\n        timerange: $timerange\n        defaultIndex: $defaultIndex\n      ) {\n        totalCount\n        edges {\n          node {\n            source {\n              uniqueIpCount\n              domainName\n              firstSeen\n              lastSeen\n            }\n            destination {\n              uniqueIpCount\n              domainName\n              firstSeen\n              lastSeen\n            }\n            network {\n              bytes\n              direction\n              packets\n            }\n          }\n          cursor {\n            value\n          }\n        }\n        pageInfo {\n          activePage\n          fakeTotalCount\n          showMorePagesIndicator\n        }\n        inspect @include(if: $inspect) {\n          dsl\n          response\n        }\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var domainsQuery = (0, _graphqlTag.default)(_templateObject());
exports.domainsQuery = domainsQuery;