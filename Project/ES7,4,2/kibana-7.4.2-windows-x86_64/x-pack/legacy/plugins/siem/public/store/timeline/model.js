"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.eventsDefaults = exports.timelineDefaults = exports.DEFAULT_PAGE_COUNT = void 0;

var _helpers = require("../../components/timeline/body/helpers");

var _default_headers = require("../../components/timeline/body/column_headers/default_headers");

var _default_headers2 = require("../../components/events_viewer/default_headers");

var _types = require("../../graphql/types");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DEFAULT_PAGE_COUNT = 2; // Eui Pager will not render unless this is a minimum of 2 pages

exports.DEFAULT_PAGE_COUNT = DEFAULT_PAGE_COUNT;
var timelineDefaults = {
  columns: _default_headers.defaultHeaders,
  dataProviders: [],
  description: '',
  eventIdToNoteIds: {},
  highlightedDropAndProviderId: '',
  historyIds: [],
  isFavorite: false,
  isLive: false,
  isLoading: false,
  isSaving: false,
  itemsPerPage: 25,
  itemsPerPageOptions: [10, 25, 50, 100],
  kqlMode: 'filter',
  kqlQuery: {
    filterQuery: null,
    filterQueryDraft: null
  },
  title: '',
  noteIds: [],
  pinnedEventIds: {},
  pinnedEventsSaveObject: {},
  dateRange: {
    start: 0,
    end: 0
  },
  savedObjectId: null,
  show: false,
  sort: {
    columnId: '@timestamp',
    sortDirection: _types.Direction.desc
  },
  width: _helpers.DEFAULT_TIMELINE_WIDTH,
  version: null
};
exports.timelineDefaults = timelineDefaults;

var eventsDefaults = _objectSpread({}, timelineDefaults, {
  columns: _default_headers2.defaultHeaders
});

exports.eventsDefaults = eventsDefaults;