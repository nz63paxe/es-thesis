"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimelinesTable = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _actions_columns = require("./actions_columns");

var _common_columns = require("./common_columns");

var _extended_columns = require("./extended_columns");

var _icon_header_columns = require("./icon_header_columns");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiTableCellContent {\n    animation: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var BasicTable = (0, _styledComponents.default)(_eui.EuiBasicTable)(_templateObject());
BasicTable.displayName = 'BasicTable';

var getExtendedColumnsIfEnabled = function getExtendedColumnsIfEnabled(showExtendedColumnsAndActions) {
  return showExtendedColumnsAndActions ? _toConsumableArray((0, _extended_columns.getExtendedColumns)()) : [];
};
/**
 * Returns the column definitions (passed as the `columns` prop to
 * `EuiBasicTable`) that are displayed in the compact `Open Timeline` modal
 * view, and the full view shown in the `All Timelines` view of the
 * `Timelines` page
 */


var getTimelinesTableColumns = function getTimelinesTableColumns(_ref) {
  var deleteTimelines = _ref.deleteTimelines,
      itemIdToExpandedNotesRowMap = _ref.itemIdToExpandedNotesRowMap,
      onOpenTimeline = _ref.onOpenTimeline,
      onToggleShowNotes = _ref.onToggleShowNotes,
      showExtendedColumnsAndActions = _ref.showExtendedColumnsAndActions;
  return [].concat(_toConsumableArray((0, _common_columns.getCommonColumns)({
    itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap,
    onOpenTimeline: onOpenTimeline,
    onToggleShowNotes: onToggleShowNotes,
    showExtendedColumnsAndActions: showExtendedColumnsAndActions
  })), _toConsumableArray(getExtendedColumnsIfEnabled(showExtendedColumnsAndActions)), _toConsumableArray((0, _icon_header_columns.getIconHeaderColumns)()), _toConsumableArray((0, _actions_columns.getActionsColumns)({
    deleteTimelines: deleteTimelines,
    onOpenTimeline: onOpenTimeline,
    showDeleteAction: showExtendedColumnsAndActions
  })));
};

/**
 * Renders a table that displays metadata about timelines, (i.e. name,
 * description, etc.)
 */
var TimelinesTable = (0, _recompose.pure)(function (_ref2) {
  var deleteTimelines = _ref2.deleteTimelines,
      defaultPageSize = _ref2.defaultPageSize,
      isLoading = _ref2.loading,
      itemIdToExpandedNotesRowMap = _ref2.itemIdToExpandedNotesRowMap,
      onOpenTimeline = _ref2.onOpenTimeline,
      onSelectionChange = _ref2.onSelectionChange,
      onTableChange = _ref2.onTableChange,
      onToggleShowNotes = _ref2.onToggleShowNotes,
      pageIndex = _ref2.pageIndex,
      pageSize = _ref2.pageSize,
      searchResults = _ref2.searchResults,
      showExtendedColumnsAndActions = _ref2.showExtendedColumnsAndActions,
      sortField = _ref2.sortField,
      sortDirection = _ref2.sortDirection,
      totalSearchResultsCount = _ref2.totalSearchResultsCount;
  var pagination = {
    hidePerPageOptions: !showExtendedColumnsAndActions,
    pageIndex: pageIndex,
    pageSize: pageSize,
    pageSizeOptions: [Math.floor(Math.max(defaultPageSize, 1) / 2), defaultPageSize, defaultPageSize * 2],
    totalItemCount: totalSearchResultsCount
  };
  var sorting = {
    sort: {
      field: sortField,
      direction: sortDirection
    }
  };
  var selection = {
    selectable: function selectable(timelineResult) {
      return timelineResult.savedObjectId != null;
    },
    selectableMessage: function selectableMessage(selectable) {
      return !selectable ? i18n.MISSING_SAVED_OBJECT_ID : undefined;
    },
    onSelectionChange: onSelectionChange
  };
  return React.createElement(BasicTable, {
    columns: getTimelinesTableColumns({
      deleteTimelines: deleteTimelines,
      itemIdToExpandedNotesRowMap: itemIdToExpandedNotesRowMap,
      onOpenTimeline: onOpenTimeline,
      onToggleShowNotes: onToggleShowNotes,
      showExtendedColumnsAndActions: showExtendedColumnsAndActions
    }),
    "data-test-subj": "timelines-table",
    isExpandable: true,
    isSelectable: showExtendedColumnsAndActions,
    itemId: "savedObjectId",
    itemIdToExpandedRowMap: itemIdToExpandedNotesRowMap,
    items: searchResults,
    loading: isLoading,
    noItemsMessage: i18n.ZERO_TIMELINES_MATCH,
    onChange: onTableChange,
    pagination: pagination,
    selection: showExtendedColumnsAndActions ? selection : undefined,
    sorting: sorting
  });
});
exports.TimelinesTable = TimelinesTable;
TimelinesTable.displayName = 'TimelinesTable';