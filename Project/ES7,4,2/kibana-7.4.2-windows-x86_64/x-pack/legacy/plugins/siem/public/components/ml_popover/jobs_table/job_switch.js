"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JobSwitch = exports.isFailure = exports.isJobLoading = exports.isChecked = void 0;

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiSwitch__thumb,\n  .euiSwitch__icon {\n    transition: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StaticSwitch = (0, _styledComponents.default)(_eui.EuiSwitch)(_templateObject());
StaticSwitch.displayName = 'StaticSwitch';
// Based on ML Job/Datafeed States from x-pack/legacy/plugins/ml/common/constants/states.js
var enabledStates = ['started', 'opened'];
var loadingStates = ['starting', 'stopping', 'opening', 'closing'];
var failureStates = ['deleted', 'failed'];

var isChecked = function isChecked(jobState, datafeedState) {
  return enabledStates.includes(jobState) && enabledStates.includes(datafeedState);
};

exports.isChecked = isChecked;

var isJobLoading = function isJobLoading(jobState, datafeedState) {
  return loadingStates.includes(jobState) || loadingStates.includes(datafeedState);
};

exports.isJobLoading = isJobLoading;

var isFailure = function isFailure(jobState, datafeedState) {
  return failureStates.includes(jobState) || failureStates.includes(datafeedState);
};

exports.isFailure = isFailure;

var JobSwitch = _react.default.memo(function (_ref) {
  var job = _ref.job,
      isSummaryLoading = _ref.isSummaryLoading,
      onJobStateChange = _ref.onJobStateChange;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isLoading = _useState2[0],
      setIsLoading = _useState2[1];

  return _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceAround"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, isSummaryLoading || isLoading || isJobLoading(job.jobState, job.datafeedId) ? _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: "m",
    "data-test-subj": "job-switch-loader"
  }) : _react.default.createElement(StaticSwitch, {
    "data-test-subj": "job-switch",
    disabled: isFailure(job.jobState, job.datafeedState),
    checked: isChecked(job.jobState, job.datafeedState),
    onChange: function onChange(e) {
      setIsLoading(true);
      onJobStateChange(job.id, job.latestTimestampMs || 0, e.target.checked);
    }
  })));
});

exports.JobSwitch = JobSwitch;
JobSwitch.displayName = 'JobSwitch';