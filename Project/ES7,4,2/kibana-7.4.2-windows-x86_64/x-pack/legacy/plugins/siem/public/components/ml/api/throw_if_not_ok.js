"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isMlStartJobError = exports.throwIfErrorAttached = exports.throwIfErrorAttachedToSetup = exports.tryParseResponse = exports.parseJsonFromBody = exports.throwIfNotOk = exports.ToasterErrors = void 0;

var _fp = require("lodash/fp");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ToasterErrors =
/*#__PURE__*/
function (_Error) {
  _inherits(ToasterErrors, _Error);

  function ToasterErrors(messages) {
    var _this;

    _classCallCheck(this, ToasterErrors);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ToasterErrors).call(this, messages[0]));

    _defineProperty(_assertThisInitialized(_this), "messages", void 0);

    _this.name = 'ToasterErrors';
    _this.messages = messages;
    return _this;
  }

  return ToasterErrors;
}(_wrapNativeSuper(Error));

exports.ToasterErrors = ToasterErrors;

var throwIfNotOk =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(response) {
    var body;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (response.ok) {
              _context.next = 13;
              break;
            }

            _context.next = 3;
            return parseJsonFromBody(response);

          case 3:
            body = _context.sent;

            if (!(body != null && body.message)) {
              _context.next = 12;
              break;
            }

            if (!(body.statusCode != null)) {
              _context.next = 9;
              break;
            }

            throw new ToasterErrors([body.message, "".concat(i18n.STATUS_CODE, " ").concat(body.statusCode)]);

          case 9:
            throw new ToasterErrors([body.message]);

          case 10:
            _context.next = 13;
            break;

          case 12:
            throw new ToasterErrors(["".concat(i18n.NETWORK_ERROR, " ").concat(response.statusText)]);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function throwIfNotOk(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.throwIfNotOk = throwIfNotOk;

var parseJsonFromBody =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(response) {
    var text;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.prev = 0;
            _context2.next = 3;
            return response.text();

          case 3:
            text = _context2.sent;
            return _context2.abrupt("return", JSON.parse(text));

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](0);
            return _context2.abrupt("return", null);

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[0, 7]]);
  }));

  return function parseJsonFromBody(_x2) {
    return _ref2.apply(this, arguments);
  };
}();

exports.parseJsonFromBody = parseJsonFromBody;

var tryParseResponse = function tryParseResponse(response) {
  try {
    return JSON.stringify(JSON.parse(response), null, 2);
  } catch (error) {
    return response;
  }
};

exports.tryParseResponse = tryParseResponse;

var throwIfErrorAttachedToSetup = function throwIfErrorAttachedToSetup(setupResponse) {
  var jobErrors = setupResponse.jobs.reduce(function (accum, job) {
    if (job.error != null) {
      accum = [].concat(_toConsumableArray(accum), [job.error.msg, tryParseResponse(job.error.response), "".concat(i18n.STATUS_CODE, " ").concat(job.error.statusCode)]);
      return accum;
    } else {
      return accum;
    }
  }, []);
  var dataFeedErrors = setupResponse.datafeeds.reduce(function (accum, dataFeed) {
    if (dataFeed.error != null) {
      accum = [].concat(_toConsumableArray(accum), [dataFeed.error.msg, tryParseResponse(dataFeed.error.response), "".concat(i18n.STATUS_CODE, " ").concat(dataFeed.error.statusCode)]);
      return accum;
    } else {
      return accum;
    }
  }, []);
  var errors = [].concat(_toConsumableArray(jobErrors), _toConsumableArray(dataFeedErrors));

  if (errors.length > 0) {
    throw new ToasterErrors(errors);
  }
};

exports.throwIfErrorAttachedToSetup = throwIfErrorAttachedToSetup;

var throwIfErrorAttached = function throwIfErrorAttached(json, dataFeedIds) {
  var errors = dataFeedIds.reduce(function (accum, dataFeedId) {
    var dataFeed = json[dataFeedId];

    if (isMlStartJobError(dataFeed)) {
      accum = [].concat(_toConsumableArray(accum), [dataFeed.error.msg, tryParseResponse(dataFeed.error.response), "".concat(i18n.STATUS_CODE, " ").concat(dataFeed.error.statusCode)]);
      return accum;
    } else {
      return accum;
    }
  }, []);

  if (errors.length > 0) {
    throw new ToasterErrors(errors);
  }
}; // use the "in operator" and regular type guards to do a narrow once this issue is fixed below:
// https://github.com/microsoft/TypeScript/issues/21732
// Otherwise for now, has will work ok even though it casts 'unknown' to 'any'


exports.throwIfErrorAttached = throwIfErrorAttached;

var isMlStartJobError = function isMlStartJobError(value) {
  return (0, _fp.has)('error.msg', value) && (0, _fp.has)('error.response', value) && (0, _fp.has)('error.statusCode', value);
};

exports.isMlStartJobError = isMlStartJobError;