"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceDestination = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _network = require("./network");

var _source_destination_with_arrows = require("./source_destination_with_arrows");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 3px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EuiFlexItemMarginTop = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
EuiFlexItemMarginTop.displayName = 'EuiFlexItemMarginTop';
/**
 * Renders a visualization of network traffic between a source and a destination
 * This component is used by the Netflow row renderer
 */

var SourceDestination = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      destinationBytes = _ref.destinationBytes,
      destinationGeoContinentName = _ref.destinationGeoContinentName,
      destinationGeoCountryName = _ref.destinationGeoCountryName,
      destinationGeoCountryIsoCode = _ref.destinationGeoCountryIsoCode,
      destinationGeoRegionName = _ref.destinationGeoRegionName,
      destinationGeoCityName = _ref.destinationGeoCityName,
      destinationIp = _ref.destinationIp,
      destinationPackets = _ref.destinationPackets,
      destinationPort = _ref.destinationPort,
      eventId = _ref.eventId,
      networkBytes = _ref.networkBytes,
      networkCommunityId = _ref.networkCommunityId,
      networkDirection = _ref.networkDirection,
      networkPackets = _ref.networkPackets,
      networkProtocol = _ref.networkProtocol,
      sourceBytes = _ref.sourceBytes,
      sourceGeoContinentName = _ref.sourceGeoContinentName,
      sourceGeoCountryName = _ref.sourceGeoCountryName,
      sourceGeoCountryIsoCode = _ref.sourceGeoCountryIsoCode,
      sourceGeoRegionName = _ref.sourceGeoRegionName,
      sourceGeoCityName = _ref.sourceGeoCityName,
      sourceIp = _ref.sourceIp,
      sourcePackets = _ref.sourcePackets,
      sourcePort = _ref.sourcePort,
      transport = _ref.transport;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    direction: "column",
    justifyContent: "center",
    gutterSize: "none"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_network.Network, {
    bytes: networkBytes,
    packets: networkPackets,
    communityId: networkCommunityId,
    contextId: contextId,
    eventId: eventId,
    direction: networkDirection,
    protocol: networkProtocol,
    transport: transport
  })), React.createElement(EuiFlexItemMarginTop, {
    grow: false
  }, React.createElement(_source_destination_with_arrows.SourceDestinationWithArrows, {
    contextId: contextId,
    destinationBytes: destinationBytes,
    destinationGeoContinentName: destinationGeoContinentName,
    destinationGeoCountryName: destinationGeoCountryName,
    destinationGeoCountryIsoCode: destinationGeoCountryIsoCode,
    destinationGeoRegionName: destinationGeoRegionName,
    destinationGeoCityName: destinationGeoCityName,
    destinationIp: destinationIp,
    destinationPackets: destinationPackets,
    destinationPort: destinationPort,
    eventId: eventId,
    sourceBytes: sourceBytes,
    sourceGeoContinentName: sourceGeoContinentName,
    sourceGeoCountryName: sourceGeoCountryName,
    sourceGeoCountryIsoCode: sourceGeoCountryIsoCode,
    sourceGeoRegionName: sourceGeoRegionName,
    sourceGeoCityName: sourceGeoCityName,
    sourceIp: sourceIp,
    sourcePackets: sourcePackets,
    sourcePort: sourcePort
  })));
});
exports.SourceDestination = SourceDestination;
SourceDestination.displayName = 'SourceDestination';