"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteCardBody = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _with_copy_to_clipboard = require("../../../lib/clipboard/with_copy_to_clipboard");

var _markdown = require("../../markdown");

var _with_hover_actions = require("../../with_hover_actions");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  height: 25px;\n  justify-content: center;\n  left: 5px;\n  position: absolute;\n  top: -5px;\n  width: 30px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  border: none;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var BodyContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject());
BodyContainer.displayName = 'BodyContainer';
var HoverActionsContainer = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject2());
HoverActionsContainer.displayName = 'HoverActionsContainer';
var NoteCardBody = (0, _recompose.pure)(function (_ref) {
  var rawNote = _ref.rawNote;
  return React.createElement(BodyContainer, {
    "data-test-subj": "note-card-body",
    hasShadow: false,
    paddingSize: "s"
  }, React.createElement(_with_hover_actions.WithHoverActions, {
    hoverContent: React.createElement(HoverActionsContainer, {
      "data-test-subj": "hover-actions-container"
    }, React.createElement(_eui.EuiToolTip, {
      content: i18n.COPY_TO_CLIPBOARD
    }, React.createElement(_with_copy_to_clipboard.WithCopyToClipboard, {
      text: rawNote,
      titleSummary: i18n.NOTE.toLowerCase()
    }))),
    render: function render() {
      return React.createElement(_markdown.Markdown, {
        raw: rawNote
      });
    }
  }));
});
exports.NoteCardBody = NoteCardBody;
NoteCardBody.displayName = 'NoteCardBody';