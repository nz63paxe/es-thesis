"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Filter = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _text_filter = require("../text_filter");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Renders a header's filter, based on the `columnHeaderType` */
var Filter = (0, _recompose.pure)(function (_ref) {
  var header = _ref.header,
      _ref$onFilterChange = _ref.onFilterChange,
      onFilterChange = _ref$onFilterChange === void 0 ? _fp.noop : _ref$onFilterChange;

  switch (header.columnHeaderType) {
    case 'text-filter':
      return React.createElement(_text_filter.TextFilter, {
        columnId: header.id,
        minWidth: header.width,
        onFilterChange: onFilterChange,
        placeholder: header.placeholder
      });

    case 'not-filtered': // fall through

    default:
      return null;
  }
});
exports.Filter = Filter;
Filter.displayName = 'Filter';