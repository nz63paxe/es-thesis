"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CategoriesPane = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _category_columns = require("./category_columns");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  padding-left: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", ";\n  overflow: auto;\n  padding: 5px;\n  ", ";\n  thead {\n    display: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CategoryNames = _styledComponents.default.div(_templateObject(), function (_ref) {
  var height = _ref.height;
  return "height: ".concat(height, "px");
}, function (_ref2) {
  var width = _ref2.width;
  return "width: ".concat(width, "px");
});

CategoryNames.displayName = 'CategoryNames';
var Title = (0, _styledComponents.default)(_eui.EuiTitle)(_templateObject2());
Title.displayName = 'Title';
var CategoriesPane = React.memo(function (_ref3) {
  var browserFields = _ref3.browserFields,
      filteredBrowserFields = _ref3.filteredBrowserFields,
      onCategorySelected = _ref3.onCategorySelected,
      onUpdateColumns = _ref3.onUpdateColumns,
      selectedCategoryId = _ref3.selectedCategoryId,
      timelineId = _ref3.timelineId,
      width = _ref3.width;
  return React.createElement(React.Fragment, null, React.createElement(Title, {
    size: "xxs"
  }, React.createElement("h5", {
    "data-test-subj": "categories-pane-title"
  }, i18n.CATEGORIES)), React.createElement(CategoryNames, {
    className: "euiTable--compressed",
    "data-test-subj": "categories-container",
    height: _helpers.TABLE_HEIGHT,
    width: width
  }, React.createElement(_eui.EuiInMemoryTable, {
    columns: (0, _category_columns.getCategoryColumns)({
      browserFields: browserFields,
      filteredBrowserFields: filteredBrowserFields,
      onCategorySelected: onCategorySelected,
      onUpdateColumns: onUpdateColumns,
      selectedCategoryId: selectedCategoryId,
      timelineId: timelineId
    }),
    items: Object.keys(filteredBrowserFields).sort().map(function (categoryId) {
      return {
        categoryId: categoryId
      };
    }),
    message: i18n.NO_FIELDS_MATCH,
    pagination: false,
    sorting: false
  })));
});
exports.CategoriesPane = CategoriesPane;
CategoriesPane.displayName = 'CategoriesPane';