"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimelineHeader = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _data_providers = require("../data_providers");

var _search_or_filter = require("../search_or_filter");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TimelineHeaderContainer = _styledComponents.default.div(_templateObject());

TimelineHeaderContainer.displayName = 'TimelineHeaderContainer';
var TimelineHeader = React.memo(function (_ref) {
  var browserFields = _ref.browserFields,
      id = _ref.id,
      indexPattern = _ref.indexPattern,
      dataProviders = _ref.dataProviders,
      onChangeDataProviderKqlQuery = _ref.onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider = _ref.onChangeDroppableAndProvider,
      onDataProviderEdited = _ref.onDataProviderEdited,
      onDataProviderRemoved = _ref.onDataProviderRemoved,
      onToggleDataProviderEnabled = _ref.onToggleDataProviderEnabled,
      onToggleDataProviderExcluded = _ref.onToggleDataProviderExcluded,
      show = _ref.show,
      showCallOutUnauthorizedMsg = _ref.showCallOutUnauthorizedMsg;
  return React.createElement(TimelineHeaderContainer, {
    "data-test-subj": "timelineHeader"
  }, showCallOutUnauthorizedMsg && React.createElement(_eui.EuiCallOut, {
    "data-test-subj": "timelineCallOutUnauthorized",
    title: i18n.CALL_OUT_UNAUTHORIZED_MSG,
    color: "warning",
    iconType: "alert",
    size: "s"
  }), React.createElement(_data_providers.DataProviders, {
    browserFields: browserFields,
    id: id,
    dataProviders: dataProviders,
    onChangeDroppableAndProvider: onChangeDroppableAndProvider,
    onChangeDataProviderKqlQuery: onChangeDataProviderKqlQuery,
    onDataProviderEdited: onDataProviderEdited,
    onDataProviderRemoved: onDataProviderRemoved,
    onToggleDataProviderEnabled: onToggleDataProviderEnabled,
    onToggleDataProviderExcluded: onToggleDataProviderExcluded,
    show: show
  }), React.createElement(_search_or_filter.StatefulSearchOrFilter, {
    timelineId: id,
    indexPattern: indexPattern
  }));
});
exports.TimelineHeader = TimelineHeader;
TimelineHeader.displayName = 'TimelineHeader';