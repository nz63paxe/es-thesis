"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TitleRow = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var i18n = _interopRequireWildcard(require("../translations"));

var _header_panel = require("../../header_panel");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Renders the row containing the tile (e.g. Open Timelines / All timelines)
 * and action buttons (i.e. Favorite Selected and Delete Selected)
 */
var TitleRow = (0, _recompose.pure)(function (_ref) {
  var onAddTimelinesToFavorites = _ref.onAddTimelinesToFavorites,
      onDeleteSelected = _ref.onDeleteSelected,
      selectedTimelinesCount = _ref.selectedTimelinesCount,
      title = _ref.title;
  return React.createElement(_header_panel.HeaderPanel, {
    title: title
  }, (onAddTimelinesToFavorites || onDeleteSelected) && React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s",
    responsive: false
  }, onAddTimelinesToFavorites && React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiButton, {
    "data-test-subj": "favorite-selected",
    iconSide: "left",
    iconType: "starEmptySpace",
    isDisabled: selectedTimelinesCount === 0,
    onClick: onAddTimelinesToFavorites
  }, i18n.FAVORITE_SELECTED)), onDeleteSelected && React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiButton, {
    "data-test-subj": "delete-selected",
    iconSide: "left",
    iconType: "trash",
    isDisabled: selectedTimelinesCount === 0,
    onClick: onDeleteSelected
  }, i18n.DELETE_SELECTED))));
});
exports.TitleRow = TitleRow;
TitleRow.displayName = 'TitleRow';