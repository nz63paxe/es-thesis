"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PropertiesRight = exports.PropertiesRightStyle = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

var _helpers = require("./helpers");

var _open_timeline_modal = require("../../open_timeline/open_timeline_modal");

var _inspect = require("../../inspect");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  display: none;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 4px;\n  cursor: pointer;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-top: 15px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PropertiesRightStyle = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
exports.PropertiesRightStyle = PropertiesRightStyle;
PropertiesRightStyle.displayName = 'PropertiesRightStyle';

var DescriptionPopoverMenuContainer = _styledComponents.default.div(_templateObject2());

DescriptionPopoverMenuContainer.displayName = 'DescriptionPopoverMenuContainer';
var SettingsIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject3());
SettingsIcon.displayName = 'SettingsIcon';
var HiddenFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject4());
HiddenFlexItem.displayName = 'HiddenFlexItem';
var Avatar = (0, _styledComponents.default)(_eui.EuiAvatar)(_templateObject5());
Avatar.displayName = 'Avatar';

var PropertiesRight = _react.default.memo(function (_ref) {
  var onButtonClick = _ref.onButtonClick,
      showActions = _ref.showActions,
      onClosePopover = _ref.onClosePopover,
      createTimeline = _ref.createTimeline,
      timelineId = _ref.timelineId,
      isDataInTimeline = _ref.isDataInTimeline,
      showNotesFromWidth = _ref.showNotesFromWidth,
      showNotes = _ref.showNotes,
      showDescription = _ref.showDescription,
      showUsersView = _ref.showUsersView,
      usersViewing = _ref.usersViewing,
      description = _ref.description,
      updateDescription = _ref.updateDescription,
      associateNote = _ref.associateNote,
      getNotesByIds = _ref.getNotesByIds,
      noteIds = _ref.noteIds,
      onToggleShowNotes = _ref.onToggleShowNotes,
      updateNote = _ref.updateNote;
  return _react.default.createElement(PropertiesRightStyle, {
    alignItems: "flexStart",
    "data-test-subj": "properties-right",
    gutterSize: "s"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiPopover, {
    anchorPosition: "downRight",
    button: _react.default.createElement(SettingsIcon, {
      "data-test-subj": "settings-gear",
      type: "gear",
      size: "l",
      onClick: onButtonClick
    }),
    id: "timelineSettingsPopover",
    isOpen: showActions,
    closePopover: onClosePopover
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "flexStart",
    direction: "column",
    gutterSize: "none"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_helpers.NewTimeline, {
    createTimeline: createTimeline,
    onClosePopover: onClosePopover,
    timelineId: timelineId
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_open_timeline_modal.OpenTimelineModalButton, null)), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_inspect.InspectButton, {
    queryId: timelineId,
    inputId: "timeline",
    inspectIndex: 0,
    isDisabled: !isDataInTimeline,
    onCloseInspect: onClosePopover,
    show: true,
    title: i18n.INSPECT_TIMELINE_TITLE
  })), showNotesFromWidth ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_helpers.NotesButton, {
    animate: true,
    associateNote: associateNote,
    getNotesByIds: getNotesByIds,
    noteIds: noteIds,
    showNotes: showNotes,
    size: "l",
    text: i18n.NOTES,
    toggleShowNotes: onToggleShowNotes,
    toolTip: i18n.NOTES_TOOL_TIP,
    updateNote: updateNote
  })) : null, showDescription ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(DescriptionPopoverMenuContainer, null, _react.default.createElement(_helpers.Description, {
    description: description,
    timelineId: timelineId,
    updateDescription: updateDescription
  }))) : null))), showUsersView ? usersViewing.map(function (user) {
    return (// Hide the hard-coded elastic user avatar as the 7.2 release does not implement
      // support for multi-user-collaboration as proposed in elastic/ingest-dev#395
      _react.default.createElement(HiddenFlexItem, {
        key: user
      }, _react.default.createElement(_eui.EuiToolTip, {
        "data-test-subj": "timeline-action-pin-tool-tip",
        content: "".concat(user, " ").concat(i18n.IS_VIEWING)
      }, _react.default.createElement(Avatar, {
        "data-test-subj": "avatar",
        size: "s",
        name: user
      })))
    );
  }) : null);
});

exports.PropertiesRight = PropertiesRight;
PropertiesRight.displayName = 'PropertiesRight';