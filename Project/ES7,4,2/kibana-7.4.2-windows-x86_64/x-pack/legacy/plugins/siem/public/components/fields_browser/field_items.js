"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFieldColumns = exports.getFieldItems = exports.Description = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _reactBeautifulDnd = require("react-beautiful-dnd");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _field_badge = require("../draggables/field_badge");

var _draggable_wrapper = require("../drag_and_drop/draggable_wrapper");

var _droppable_wrapper = require("../drag_and_drop/droppable_wrapper");

var _helpers = require("../event_details/helpers");

var _helpers2 = require("../drag_and_drop/helpers");

var _empty_value = require("../empty_value");

var _selectable_text = require("../selectable_text");

var _truncatable_text = require("../truncatable_text");

var _field_name = require("./field_name");

var i18n = _interopRequireWildcard(require("./translations"));

var _helpers3 = require("../timeline/body/helpers");

var _default_headers = require("../timeline/body/column_headers/default_headers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  user-select: text;\n  width: 150px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n  position: relative;\n  top: -1px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TypeIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject());
TypeIcon.displayName = 'TypeIcon';

var Description = _styledComponents.default.span(_templateObject2());

exports.Description = Description;
Description.displayName = 'Description';
/**
 * An item rendered in the table
 */

/**
 * Returns the draggable fields, values, and descriptions shown when a user expands an event
 */
var getFieldItems = function getFieldItems(_ref) {
  var browserFields = _ref.browserFields,
      category = _ref.category,
      categoryId = _ref.categoryId,
      columnHeaders = _ref.columnHeaders,
      _ref$highlight = _ref.highlight,
      highlight = _ref$highlight === void 0 ? '' : _ref$highlight,
      onUpdateColumns = _ref.onUpdateColumns,
      timelineId = _ref.timelineId,
      toggleColumn = _ref.toggleColumn;
  return (0, _fp.uniqBy)('name', _toConsumableArray(Object.values(category != null && category.fields != null ? category.fields : {}))).map(function (field) {
    return {
      description: React.createElement(_selectable_text.SelectableText, {
        "data-test-subj": "field-".concat(field.name, "-description")
      }, "".concat(field.description || (0, _empty_value.getEmptyValue)(), " ").concat((0, _helpers.getExampleText)(field.example))),
      field: React.createElement(_droppable_wrapper.DroppableWrapper, {
        droppableId: (0, _helpers2.getDroppableId)("field-browser-field-items-field-droppable-wrapper-".concat(timelineId, "-").concat(categoryId, "-").concat(field.name)),
        key: "field-browser-field-items-field-droppable-wrapper-".concat(timelineId, "-").concat(categoryId, "-").concat(field.name),
        isDropDisabled: true,
        type: _helpers2.DRAG_TYPE_FIELD
      }, React.createElement(_reactBeautifulDnd.Draggable, {
        draggableId: (0, _helpers2.getDraggableFieldId)({
          contextId: "field-browser-field-items-field-draggable-".concat(timelineId, "-").concat(categoryId, "-").concat(field.name),
          fieldId: field.name || ''
        }),
        index: 0,
        type: _helpers2.DRAG_TYPE_FIELD
      }, function (provided, snapshot) {
        return React.createElement("div", _extends({}, provided.draggableProps, provided.dragHandleProps, {
          ref: provided.innerRef,
          style: _objectSpread({}, provided.draggableProps.style, {
            zIndex: 9999
          })
        }), !snapshot.isDragging ? React.createElement(_eui.EuiFlexGroup, {
          alignItems: "center",
          gutterSize: "none"
        }, React.createElement(_eui.EuiFlexItem, {
          grow: false
        }, React.createElement(_eui.EuiToolTip, {
          content: i18n.TOGGLE_COLUMN_TOOLTIP
        }, React.createElement(_eui.EuiCheckbox, {
          checked: columnHeaders.findIndex(function (c) {
            return c.id === field.name;
          }) !== -1,
          "data-test-subj": "field-".concat(field.name, "-checkbox"),
          id: field.name || '',
          onChange: function onChange() {
            return toggleColumn({
              columnHeaderType: _default_headers.defaultColumnHeaderType,
              id: field.name || '',
              width: _helpers3.DEFAULT_COLUMN_MIN_WIDTH
            });
          }
        }))), React.createElement(_eui.EuiFlexItem, {
          grow: false
        }, React.createElement(_eui.EuiToolTip, {
          content: field.type
        }, React.createElement(TypeIcon, {
          "data-test-subj": "field-".concat(field.name, "-icon"),
          type: (0, _helpers.getIconFromType)(field.type || '')
        }))), React.createElement(_eui.EuiFlexItem, {
          grow: false
        }, React.createElement(_field_name.FieldName, {
          categoryId: field.category || categoryId,
          categoryColumns: (0, _helpers.getColumnsWithTimestamp)({
            browserFields: browserFields,
            category: field.category || categoryId
          }),
          fieldId: field.name || '',
          highlight: highlight,
          onUpdateColumns: onUpdateColumns
        }))) : React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_field_badge.DraggableFieldBadge, {
          fieldId: field.name || ''
        })));
      })),
      fieldId: field.name || ''
    };
  });
};
/**
 * Returns a table column template provided to the `EuiInMemoryTable`'s
 * `columns` prop
 */


exports.getFieldItems = getFieldItems;

var getFieldColumns = function getFieldColumns() {
  return [{
    field: 'field',
    name: i18n.FIELD,
    sortable: true,
    render: function render(field) {
      return React.createElement(React.Fragment, null, field);
    },
    width: '250px'
  }, {
    field: 'description',
    name: i18n.DESCRIPTION,
    render: function render(description) {
      return React.createElement(_eui.EuiToolTip, {
        position: "top",
        content: description
      }, React.createElement(_truncatable_text.TruncatableText, {
        size: "xs",
        width: "390px"
      }, description));
    },
    sortable: true,
    truncateText: true,
    width: '400px'
  }];
};

exports.getFieldColumns = getFieldColumns;