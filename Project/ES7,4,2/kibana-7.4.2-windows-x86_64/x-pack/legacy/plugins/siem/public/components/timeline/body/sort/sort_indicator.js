"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SortIndicator = exports.getDirection = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _types = require("../../../../graphql/types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SortDirectionIndicatorEnum;

(function (SortDirectionIndicatorEnum) {
  SortDirectionIndicatorEnum["SORT_UP"] = "sortUp";
  SortDirectionIndicatorEnum["SORT_DOWN"] = "sortDown";
})(SortDirectionIndicatorEnum || (SortDirectionIndicatorEnum = {}));

/** Returns the symbol that corresponds to the specified `SortDirection` */
var getDirection = function getDirection(sortDirection) {
  switch (sortDirection) {
    case _types.Direction.asc:
      return SortDirectionIndicatorEnum.SORT_UP;

    case _types.Direction.desc:
      return SortDirectionIndicatorEnum.SORT_DOWN;

    case 'none':
      return undefined;

    default:
      throw new Error('Unhandled sort direction');
  }
};

exports.getDirection = getDirection;

/** Renders a sort indicator */
var SortIndicator = (0, _recompose.pure)(function (_ref) {
  var sortDirection = _ref.sortDirection;
  return React.createElement(_eui.EuiIcon, {
    "data-test-subj": "sortIndicator",
    type: getDirection(sortDirection) || 'empty'
  });
});
exports.SortIndicator = SortIndicator;
SortIndicator.displayName = 'SortIndicator';