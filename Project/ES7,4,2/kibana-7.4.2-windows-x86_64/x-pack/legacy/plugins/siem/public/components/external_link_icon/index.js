"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExternalLinkIcon = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: -2px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LinkIcon = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject());
LinkIcon.displayName = 'LinkIcon';
var LinkIconWithMargin = (0, _styledComponents.default)(LinkIcon)(_templateObject2());
LinkIconWithMargin.displayName = 'LinkIconWithMargin';
var color = 'subdued';
var iconSize = 's';
var iconType = 'popout';
/**
 * Renders an icon that indicates following the hyperlink will navigate to
 * content external to the app
 */

var ExternalLinkIcon = (0, _recompose.pure)(function (_ref) {
  var _ref$leftMargin = _ref.leftMargin,
      leftMargin = _ref$leftMargin === void 0 ? true : _ref$leftMargin;
  return leftMargin ? React.createElement(LinkIconWithMargin, {
    color: color,
    "data-test-subj": "external-link-icon",
    size: iconSize,
    type: iconType
  }) : React.createElement(LinkIcon, {
    color: color,
    "data-test-subj": "external-link-icon",
    size: iconSize,
    type: iconType
  });
});
exports.ExternalLinkIcon = ExternalLinkIcon;
ExternalLinkIcon.displayName = 'ExternalLinkIcon';