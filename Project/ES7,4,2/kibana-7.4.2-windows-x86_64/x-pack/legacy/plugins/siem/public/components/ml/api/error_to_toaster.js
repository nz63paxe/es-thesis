"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isToasterError = exports.isAnError = exports.errorToToaster = void 0;

var _fp = require("lodash/fp");

var _uuid = _interopRequireDefault(require("uuid"));

var _throw_if_not_ok = require("./throw_if_not_ok");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var errorToToaster = function errorToToaster(_ref) {
  var _ref$id = _ref.id,
      id = _ref$id === void 0 ? _uuid.default.v4() : _ref$id,
      title = _ref.title,
      error = _ref.error,
      _ref$color = _ref.color,
      color = _ref$color === void 0 ? 'danger' : _ref$color,
      _ref$iconType = _ref.iconType,
      iconType = _ref$iconType === void 0 ? 'alert' : _ref$iconType,
      dispatchToaster = _ref.dispatchToaster;

  if (isToasterError(error)) {
    var toast = {
      id: id,
      title: title,
      color: color,
      iconType: iconType,
      errors: error.messages
    };
    dispatchToaster({
      type: 'addToaster',
      toast: toast
    });
  } else if (isAnError(error)) {
    var _toast = {
      id: id,
      title: title,
      color: color,
      iconType: iconType,
      errors: [error.message]
    };
    dispatchToaster({
      type: 'addToaster',
      toast: _toast
    });
  } else {
    var _toast2 = {
      id: id,
      title: title,
      color: color,
      iconType: iconType,
      errors: ['Network Error']
    };
    dispatchToaster({
      type: 'addToaster',
      toast: _toast2
    });
  }
};

exports.errorToToaster = errorToToaster;

var isAnError = function isAnError(error) {
  return (0, _fp.isError)(error);
};

exports.isAnError = isAnError;

var isToasterError = function isToasterError(error) {
  return error instanceof _throw_if_not_ok.ToasterErrors;
};

exports.isToasterError = isToasterError;