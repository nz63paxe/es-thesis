"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Actions = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _pin = require("../../../pin");

var _helpers = require("../../properties/helpers");

var _helpers2 = require("../helpers");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: -1px;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  padding: 4px 0 0 7px;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: -1px;\n  width: 27px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  top: 3px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  width: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ActionsContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var actionsColumnWidth = _ref.actionsColumnWidth;
  return actionsColumnWidth;
});

ActionsContainer.displayName = 'ActionsContainer';
var ActionLoading = (0, _styledComponents.default)(_eui.EuiLoadingSpinner)(_templateObject2());
ActionLoading.displayName = 'ActionLoading';

var PinContainer = _styledComponents.default.div(_templateObject3());

PinContainer.displayName = 'PinContainer';
var SelectEventContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject4());
SelectEventContainer.displayName = 'SelectEventContainer';
var NotesButtonContainer = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject5());
NotesButtonContainer.displayName = 'NotesButtonContainer';
var emptyNotes = [];
var Actions = React.memo(function (_ref2) {
  var actionsColumnWidth = _ref2.actionsColumnWidth,
      associateNote = _ref2.associateNote,
      checked = _ref2.checked,
      expanded = _ref2.expanded,
      eventId = _ref2.eventId,
      eventIsPinned = _ref2.eventIsPinned,
      getNotesByIds = _ref2.getNotesByIds,
      _ref2$isEventViewer = _ref2.isEventViewer,
      isEventViewer = _ref2$isEventViewer === void 0 ? false : _ref2$isEventViewer,
      _ref2$loading = _ref2.loading,
      loading = _ref2$loading === void 0 ? false : _ref2$loading,
      noteIds = _ref2.noteIds,
      onEventToggled = _ref2.onEventToggled,
      onPinClicked = _ref2.onPinClicked,
      showCheckboxes = _ref2.showCheckboxes,
      showNotes = _ref2.showNotes,
      toggleShowNotes = _ref2.toggleShowNotes,
      updateNote = _ref2.updateNote;
  return React.createElement(ActionsContainer, {
    actionsColumnWidth: actionsColumnWidth,
    "data-test-subj": "event-actions-container"
  }, React.createElement(_eui.EuiFlexGroup, {
    alignItems: "flexStart",
    "data-test-subj": "event-actions",
    direction: "row",
    gutterSize: "none",
    justifyContent: "spaceBetween"
  }, showCheckboxes && React.createElement(SelectEventContainer, {
    "data-test-subj": "select-event-container",
    grow: false
  }, React.createElement(_eui.EuiCheckbox, {
    "data-test-subj": "select-event",
    id: eventId,
    checked: checked,
    onChange: _fp.noop
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement("div", null, loading && React.createElement(ActionLoading, {
    size: "m"
  }), !loading && React.createElement(_eui.EuiButtonIcon, {
    "aria-label": expanded ? i18n.COLLAPSE : i18n.EXPAND,
    color: "subdued",
    iconType: expanded ? 'arrowDown' : 'arrowRight',
    "data-test-subj": "expand-event",
    id: eventId,
    onClick: onEventToggled,
    size: "s"
  }))), !isEventViewer && React.createElement(React.Fragment, null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_eui.EuiToolTip, {
    "data-test-subj": "timeline-action-pin-tool-tip",
    content: (0, _helpers2.getPinTooltip)({
      isPinned: eventIsPinned,
      eventHasNotes: (0, _helpers2.eventHasNotes)(noteIds)
    })
  }, React.createElement(PinContainer, null, React.createElement(_pin.Pin, {
    allowUnpinning: !(0, _helpers2.eventHasNotes)(noteIds),
    pinned: eventIsPinned,
    "data-test-subj": "pin-event",
    onClick: onPinClicked
  })))), React.createElement(NotesButtonContainer, {
    grow: false
  }, React.createElement(_helpers.NotesButton, {
    animate: false,
    associateNote: associateNote,
    "data-test-subj": "add-note",
    getNotesByIds: getNotesByIds,
    noteIds: noteIds || emptyNotes,
    showNotes: showNotes,
    size: "s",
    toggleShowNotes: toggleShowNotes,
    toolTip: i18n.NOTES_TOOLTIP,
    updateNote: updateNote
  })))));
}, function (nextProps, prevProps) {
  return prevProps.actionsColumnWidth === nextProps.actionsColumnWidth && prevProps.checked === nextProps.checked && prevProps.expanded === nextProps.expanded && prevProps.eventId === nextProps.eventId && prevProps.eventIsPinned === nextProps.eventIsPinned && prevProps.loading === nextProps.loading && prevProps.noteIds === nextProps.noteIds && prevProps.showCheckboxes === nextProps.showCheckboxes && prevProps.showNotes === nextProps.showNotes;
});
exports.Actions = Actions;
Actions.displayName = 'Actions';