"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UpgradeContents = void 0;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 384px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PopoverContentsDiv = _styledComponents.default.div(_templateObject());

PopoverContentsDiv.displayName = 'PopoverContentsDiv';

var UpgradeContents = _react.default.memo(function () {
  return _react.default.createElement(PopoverContentsDiv, {
    "data-test-subj": "ml-popover-upgrade-contents"
  }, _react.default.createElement(_eui.EuiPopoverTitle, null, i18n.UPGRADE_TITLE), _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, i18n.UPGRADE_DESCRIPTION), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiButton, {
    href: "https://www.elastic.co/subscriptions",
    iconType: "popout",
    iconSide: "right",
    target: "_blank"
  }, i18n.UPGRADE_BUTTON));
});

exports.UpgradeContents = UpgradeContents;
UpgradeContents.displayName = 'UpgradeContents';