"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EMPTY_ACTION_SECONDARY = exports.EMPTY_ACTION_PRIMARY = exports.EMPTY_TITLE = exports.NAVIGATION_EVENTS_TITLE = exports.NAVIGATION_ANOMALIES_TITLE = exports.NAVIGATION_UNCOMMON_PROCESSES_TITLE = exports.NAVIGATION_AUTHENTICATIONS_TITLE = exports.NAVIGATION_ALL_HOSTS_TITLE = exports.PAGE_TITLE = exports.KQL_PLACEHOLDER = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var KQL_PLACEHOLDER = _i18n.i18n.translate('xpack.siem.hosts.kqlPlaceholder', {
  defaultMessage: 'e.g. host.name: "foo"'
});

exports.KQL_PLACEHOLDER = KQL_PLACEHOLDER;

var PAGE_TITLE = _i18n.i18n.translate('xpack.siem.hosts.pageTitle', {
  defaultMessage: 'Hosts'
});

exports.PAGE_TITLE = PAGE_TITLE;

var NAVIGATION_ALL_HOSTS_TITLE = _i18n.i18n.translate('xpack.siem.hosts.navigation.allHostsTitle', {
  defaultMessage: 'All Hosts'
});

exports.NAVIGATION_ALL_HOSTS_TITLE = NAVIGATION_ALL_HOSTS_TITLE;

var NAVIGATION_AUTHENTICATIONS_TITLE = _i18n.i18n.translate('xpack.siem.hosts.navigation.authenticationsTitle', {
  defaultMessage: 'Authentications'
});

exports.NAVIGATION_AUTHENTICATIONS_TITLE = NAVIGATION_AUTHENTICATIONS_TITLE;

var NAVIGATION_UNCOMMON_PROCESSES_TITLE = _i18n.i18n.translate('xpack.siem.hosts.navigation.uncommonProcessesTitle', {
  defaultMessage: 'Uncommon processes'
});

exports.NAVIGATION_UNCOMMON_PROCESSES_TITLE = NAVIGATION_UNCOMMON_PROCESSES_TITLE;

var NAVIGATION_ANOMALIES_TITLE = _i18n.i18n.translate('xpack.siem.hosts.navigation.anomaliesTitle', {
  defaultMessage: 'Anomalies'
});

exports.NAVIGATION_ANOMALIES_TITLE = NAVIGATION_ANOMALIES_TITLE;

var NAVIGATION_EVENTS_TITLE = _i18n.i18n.translate('xpack.siem.hosts.navigation.eventsTitle', {
  defaultMessage: 'Events'
});

exports.NAVIGATION_EVENTS_TITLE = NAVIGATION_EVENTS_TITLE;

var EMPTY_TITLE = _i18n.i18n.translate('xpack.siem.hosts.emptyTitle', {
  defaultMessage: 'It looks like you don’t have any indices relevant to hosts in the SIEM application'
});

exports.EMPTY_TITLE = EMPTY_TITLE;

var EMPTY_ACTION_PRIMARY = _i18n.i18n.translate('xpack.siem.hosts.emptyActionPrimary', {
  defaultMessage: 'View setup instructions'
});

exports.EMPTY_ACTION_PRIMARY = EMPTY_ACTION_PRIMARY;

var EMPTY_ACTION_SECONDARY = _i18n.i18n.translate('xpack.siem.hosts.emptyActionSecondary', {
  defaultMessage: 'Go to documentation'
});

exports.EMPTY_ACTION_SECONDARY = EMPTY_ACTION_SECONDARY;