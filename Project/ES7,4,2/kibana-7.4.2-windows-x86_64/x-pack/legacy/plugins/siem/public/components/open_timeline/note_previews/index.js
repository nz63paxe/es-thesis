"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NotePreviews = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _note_preview = require("./note_preview");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  padding: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NotePreviewsContainer = _styledComponents.default.section(_templateObject(), function (props) {
  return "".concat(props.theme.eui.euiSizeS, " 0 ").concat(props.theme.eui.euiSizeS, " ").concat(props.theme.eui.euiSizeXXL);
});

NotePreviewsContainer.displayName = 'NotePreviewsContainer';
/**
 * Renders a preview of a note in the All / Open Timelines table
 */

var NotePreviews = (0, _recompose.pure)(function (_ref) {
  var notes = _ref.notes,
      isModal = _ref.isModal;

  if (notes == null || notes.length === 0) {
    return null;
  }

  var uniqueNotes = (0, _fp.uniqBy)('savedObjectId', notes);
  return React.createElement(NotePreviewsContainer, {
    "data-test-subj": "note-previews-container"
  }, uniqueNotes.map(function (_ref2) {
    var note = _ref2.note,
        savedObjectId = _ref2.savedObjectId,
        updated = _ref2.updated,
        updatedBy = _ref2.updatedBy;
    return savedObjectId != null ? React.createElement(_note_preview.NotePreview, {
      "data-test-subj": "note-preview-".concat(savedObjectId),
      key: savedObjectId,
      note: note,
      updated: updated,
      updatedBy: updatedBy
    }) : null;
  }));
});
exports.NotePreviews = NotePreviews;
NotePreviews.displayName = 'NotePreviews';