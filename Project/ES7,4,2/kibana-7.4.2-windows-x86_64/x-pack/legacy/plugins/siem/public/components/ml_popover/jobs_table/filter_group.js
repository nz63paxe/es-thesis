"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FilterGroup = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var FilterGroup = _react.default.memo(function (_ref) {
  var showCustomJobs = _ref.showCustomJobs,
      setShowCustomJobs = _ref.setShowCustomJobs,
      showElasticJobs = _ref.showElasticJobs,
      setShowElasticJobs = _ref.setShowElasticJobs,
      setFilterQuery = _ref.setFilterQuery;
  return _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "m",
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: true
  }, _react.default.createElement(_eui.EuiSearchBar, {
    "data-test-subj": "jobs-filter-bar",
    box: {
      placeholder: i18n.FILTER_PLACEHOLDER,
      incremental: true
    },
    onChange: function onChange(query) {
      return setFilterQuery(query.queryText.trim());
    }
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiFilterGroup, null, _react.default.createElement(_eui.EuiFilterButton, {
    hasActiveFilters: showCustomJobs,
    onClick: function onClick() {
      setShowCustomJobs(!showCustomJobs);
      setShowElasticJobs(false);
    },
    "data-test-subj": "show-custom-jobs-filter-button",
    withNext: true
  }, i18n.SHOW_ELASTIC_JOBS), _react.default.createElement(_eui.EuiFilterButton, {
    hasActiveFilters: showElasticJobs,
    onClick: function onClick() {
      setShowElasticJobs(!showElasticJobs);
      setShowCustomJobs(false);
    },
    "data-test-subj": "show-elastic-jobs-filter-button"
  }, i18n.SHOW_CUSTOM_JOBS))));
});

exports.FilterGroup = FilterGroup;
FilterGroup.displayName = 'FilterGroup';