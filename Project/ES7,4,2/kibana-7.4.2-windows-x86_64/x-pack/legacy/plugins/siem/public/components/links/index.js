"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WhoIsLink = exports.VirusTotalLink = exports.ReputationLink = exports.CertificateFingerprintLink = exports.Ja3FingerprintLink = exports.PortOrServiceNameLink = exports.GoogleLink = exports.IPDetailsLink = exports.HostDetailsLink = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _helpers = require("../../lib/helpers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Internal Links
var HostDetailsLink = (0, _recompose.pure)(function (_ref) {
  var children = _ref.children,
      hostName = _ref.hostName;
  return React.createElement(_eui.EuiLink, {
    href: "#/link-to/hosts/".concat(encodeURIComponent(hostName))
  }, children ? children : hostName);
});
exports.HostDetailsLink = HostDetailsLink;
HostDetailsLink.displayName = 'HostDetailsLink';
var IPDetailsLink = (0, _recompose.pure)(function (_ref2) {
  var children = _ref2.children,
      ip = _ref2.ip;
  return React.createElement(_eui.EuiLink, {
    href: "#/link-to/network/ip/".concat(encodeURIComponent((0, _helpers.encodeIpv6)(ip)))
  }, children ? children : ip);
});
exports.IPDetailsLink = IPDetailsLink;
IPDetailsLink.displayName = 'IPDetailsLink'; // External Links

var GoogleLink = (0, _recompose.pure)(function (_ref3) {
  var children = _ref3.children,
      link = _ref3.link;
  return React.createElement(_eui.EuiLink, {
    href: "https://www.google.com/search?q=".concat(encodeURIComponent(link)),
    target: "_blank"
  }, children ? children : link);
});
exports.GoogleLink = GoogleLink;
GoogleLink.displayName = 'GoogleLink';
var PortOrServiceNameLink = (0, _recompose.pure)(function (_ref4) {
  var children = _ref4.children,
      portOrServiceName = _ref4.portOrServiceName;
  return React.createElement(_eui.EuiLink, {
    "data-test-subj": "port-or-service-name-link",
    href: "https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml?search=".concat(encodeURIComponent(String(portOrServiceName))),
    target: "_blank"
  }, children ? children : portOrServiceName);
});
exports.PortOrServiceNameLink = PortOrServiceNameLink;
PortOrServiceNameLink.displayName = 'PortOrServiceNameLink';
var Ja3FingerprintLink = (0, _recompose.pure)(function (_ref5) {
  var children = _ref5.children,
      ja3Fingerprint = _ref5.ja3Fingerprint;
  return React.createElement(_eui.EuiLink, {
    "data-test-subj": "ja3-fingerprint-link",
    href: "https://sslbl.abuse.ch/ja3-fingerprints/".concat(encodeURIComponent(ja3Fingerprint)),
    target: "_blank"
  }, children ? children : ja3Fingerprint);
});
exports.Ja3FingerprintLink = Ja3FingerprintLink;
Ja3FingerprintLink.displayName = 'Ja3FingerprintLink';
var CertificateFingerprintLink = (0, _recompose.pure)(function (_ref6) {
  var children = _ref6.children,
      certificateFingerprint = _ref6.certificateFingerprint;
  return React.createElement(_eui.EuiLink, {
    "data-test-subj": "certificate-fingerprint-link",
    href: "https://sslbl.abuse.ch/ssl-certificates/sha1/".concat(encodeURIComponent(certificateFingerprint)),
    target: "_blank"
  }, children ? children : certificateFingerprint);
});
exports.CertificateFingerprintLink = CertificateFingerprintLink;
CertificateFingerprintLink.displayName = 'CertificateFingerprintLink';
var ReputationLink = (0, _recompose.pure)(function (_ref7) {
  var children = _ref7.children,
      domain = _ref7.domain;
  return React.createElement(_eui.EuiLink, {
    href: "https://www.talosintelligence.com/reputation_center/lookup?search=".concat(encodeURIComponent(domain)),
    target: "_blank"
  }, children ? children : domain);
});
exports.ReputationLink = ReputationLink;
ReputationLink.displayName = 'ReputationLink';
var VirusTotalLink = (0, _recompose.pure)(function (_ref8) {
  var children = _ref8.children,
      link = _ref8.link;
  return React.createElement(_eui.EuiLink, {
    href: "https://www.virustotal.com/#/search/".concat(encodeURIComponent(link)),
    target: "_blank"
  }, children ? children : link);
});
exports.VirusTotalLink = VirusTotalLink;
VirusTotalLink.displayName = 'VirusTotalLink';
var WhoIsLink = (0, _recompose.pure)(function (_ref9) {
  var children = _ref9.children,
      domain = _ref9.domain;
  return React.createElement(_eui.EuiLink, {
    href: "https://www.iana.org/whois?q=".concat(encodeURIComponent(domain)),
    target: "_blank"
  }, children ? children : domain);
});
exports.WhoIsLink = WhoIsLink;
WhoIsLink.displayName = 'WhoIsLink';