"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Empty = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _and_or_badge = require("../../and_or_badge");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: no-wrap;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  width: ", "\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  justify-content: center;\n  user-select: none;\n  align-content: center;\n  ", "\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  margin: 5px 0 5px 0;\n  padding: 3px;\n  white-space: nowrap;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Text = (0, _styledComponents.default)(_eui.EuiText)(_templateObject());
Text.displayName = 'Text'; // Ref: https://github.com/elastic/eui/issues/1655
// const BadgeHighlighted = styled(EuiBadge)`
//   height: 20px;
//   margin: 0 5px 0 5px;
//   max-width: 70px;
//   min-width: 70px;
// `;

var BadgeHighlighted = function BadgeHighlighted(props) {
  return React.createElement(_eui.EuiBadge, _extends({}, props, {
    style: {
      height: '20px',
      margin: '0 5px 0 5px',
      maxWidth: '85px',
      minWidth: '85px'
    }
  }));
};

BadgeHighlighted.displayName = 'BadgeHighlighted';

var HighlightedBackground = _styledComponents.default.span(_templateObject2(), function (props) {
  return props.theme.eui.euiColorLightShade;
});

HighlightedBackground.displayName = 'HighlightedBackground';

var EmptyContainer = _styledComponents.default.div(_templateObject3(), function (props) {
  return props.showSmallMsg ? '60px' : 'auto';
}, function (props) {
  return props.showSmallMsg ? "\n      border-right: 1px solid ".concat(props.theme.eui.euiColorMediumShade, ";\n      margin-right: 10px;\n    ") : "\n  min-height: 100px;\n  + div {\n    display: none !important;\n   }\n  ";
});

EmptyContainer.displayName = 'EmptyContainer';

var NoWrap = _styledComponents.default.div(_templateObject4());

NoWrap.displayName = 'NoWrap';

/**
 * Prompts the user to drop anything with a facet count into the data providers section.
 */
var Empty = (0, _recompose.pure)(function (_ref) {
  var _ref$showSmallMsg = _ref.showSmallMsg,
      showSmallMsg = _ref$showSmallMsg === void 0 ? false : _ref$showSmallMsg;
  return React.createElement(EmptyContainer, {
    className: "timeline-drop-area-empty",
    "data-test-subj": "empty",
    showSmallMsg: showSmallMsg
  }, !showSmallMsg && React.createElement(React.Fragment, null, React.createElement(NoWrap, null, React.createElement(Text, {
    color: "subdued",
    size: "s"
  }, i18n.DROP_ANYTHING), React.createElement(HighlightedBackground, null, React.createElement(BadgeHighlighted, null, i18n.HIGHLIGHTED))), React.createElement(NoWrap, null, React.createElement(Text, {
    color: "subdued",
    size: "s"
  }, i18n.HERE_TO_BUILD_AN), React.createElement(_and_or_badge.AndOrBadge, {
    type: "or"
  }), React.createElement(Text, {
    color: "subdued",
    size: "s"
  }, i18n.QUERY))), showSmallMsg && React.createElement(_and_or_badge.AndOrBadge, {
    type: "or"
  }));
});
exports.Empty = Empty;
Empty.displayName = 'Empty';