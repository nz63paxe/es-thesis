"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mockFrameworks = exports.getMockKibanaUiSetting = void 0;

var _constants = require("../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getMockKibanaUiSetting = function getMockKibanaUiSetting(config) {
  return function (key) {
    if (key === _constants.DEFAULT_DATE_FORMAT) {
      return [config.dateFormat];
    } else if (key === _constants.DEFAULT_DATE_FORMAT_TZ) {
      return [config.dateFormatTz];
    } else if (key === _constants.DEFAULT_BYTES_FORMAT) {
      return [config.bytesFormat];
    } else if (key === _constants.DEFAULT_KBN_VERSION) {
      return ['8.0.0'];
    } else if (key === _constants.DEFAULT_TIMEZONE_BROWSER) {
      return config && config.timezone ? [config.timezone] : ['America/New_York'];
    }

    return [null];
  };
};

exports.getMockKibanaUiSetting = getMockKibanaUiSetting;
var mockFrameworks = {
  bytes_short: {
    bytesFormat: '0b',
    dateFormat: 'MMM D, YYYY @ HH:mm:ss.SSS',
    dateFormatTz: 'Browser',
    timezone: 'America/Denver'
  },
  default_browser: {
    bytesFormat: '0,0.[0]b',
    dateFormat: 'MMM D, YYYY @ HH:mm:ss.SSS',
    dateFormatTz: 'Browser',
    timezone: 'America/Denver'
  },
  default_ET: {
    bytesFormat: '0,0.[0]b',
    dateFormat: 'MMM D, YYYY @ HH:mm:ss.SSS',
    dateFormatTz: 'America/New_York',
    timezone: 'America/New_York'
  },
  default_MT: {
    bytesFormat: '0,0.[0]b',
    dateFormat: 'MMM D, YYYY @ HH:mm:ss.SSS',
    dateFormatTz: 'America/Denver',
    timezone: 'America/Denver'
  },
  default_UTC: {
    bytesFormat: '0,0.[0]b',
    dateFormat: 'MMM D, YYYY @ HH:mm:ss.SSS',
    dateFormatTz: 'UTC',
    timezone: 'UTC'
  }
};
exports.mockFrameworks = mockFrameworks;