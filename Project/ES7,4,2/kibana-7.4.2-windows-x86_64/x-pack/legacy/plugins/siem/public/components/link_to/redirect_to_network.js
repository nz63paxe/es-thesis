"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNetworkUrl = exports.RedirectToNetworkPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _redirect_wrapper = require("./redirect_wrapper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RedirectToNetworkPage = function RedirectToNetworkPage(_ref) {
  var detailName = _ref.match.params.detailName,
      search = _ref.location.search;
  return _react.default.createElement(_redirect_wrapper.RedirectWrapper, {
    to: detailName ? "/network/ip/".concat(detailName).concat(search) : "/network".concat(search)
  });
};

exports.RedirectToNetworkPage = RedirectToNetworkPage;

var getNetworkUrl = function getNetworkUrl() {
  return '#/link-to/network';
};

exports.getNetworkUrl = getNetworkUrl;