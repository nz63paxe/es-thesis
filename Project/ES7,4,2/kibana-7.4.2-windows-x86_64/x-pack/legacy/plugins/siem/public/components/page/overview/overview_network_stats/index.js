"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OverviewNetworkStats = exports.DescriptionListDescription = void 0;

var _eui = require("@elastic/eui");

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _react = require("@kbn/i18n/react");

var _fp = require("lodash/fp");

var _react2 = _interopRequireDefault(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _empty_value = require("../../../empty_value");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  text-align: right;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var overviewNetworkStats = function overviewNetworkStats(data) {
  return [{
    description: (0, _fp.has)('auditbeatSocket', data) && data.auditbeatSocket !== null ? (0, _numeral.default)(data.auditbeatSocket).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.auditBeatSocketTitle",
      defaultMessage: "Auditbeat Socket"
    }),
    id: 'auditbeatSocket'
  }, {
    description: (0, _fp.has)('filebeatCisco', data) && data.filebeatCisco !== null ? (0, _numeral.default)(data.filebeatCisco).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.filebeatCiscoTitle",
      defaultMessage: "Filebeat Cisco"
    }),
    id: 'filebeatCisco'
  }, {
    description: (0, _fp.has)('filebeatNetflow', data) && data.filebeatNetflow !== null ? (0, _numeral.default)(data.filebeatNetflow).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.filebeatNetflowTitle",
      defaultMessage: "Filebeat Netflow"
    }),
    id: 'filebeatNetflow'
  }, {
    description: (0, _fp.has)('filebeatPanw', data) && data.filebeatPanw !== null ? (0, _numeral.default)(data.filebeatPanw).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.filebeatPanwTitle",
      defaultMessage: "Filebeat Palo Alto Network"
    }),
    id: 'filebeatPanw'
  }, {
    description: (0, _fp.has)('filebeatSuricata', data) && data.filebeatSuricata !== null ? (0, _numeral.default)(data.filebeatSuricata).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.fileBeatSuricataTitle",
      defaultMessage: "Filebeat Suricata"
    }),
    id: 'filebeatSuricata'
  }, {
    description: (0, _fp.has)('filebeatZeek', data) && data.filebeatZeek !== null ? (0, _numeral.default)(data.filebeatZeek).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.fileBeatZeekTitle",
      defaultMessage: "Filebeat Zeek"
    }),
    id: 'filebeatZeek'
  }, {
    description: (0, _fp.has)('packetbeatDNS', data) && data.packetbeatDNS !== null ? (0, _numeral.default)(data.packetbeatDNS).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.packetBeatDnsTitle",
      defaultMessage: "Packetbeat DNS"
    }),
    id: 'packetbeatDNS'
  }, {
    description: (0, _fp.has)('packetbeatFlow', data) && data.packetbeatFlow !== null ? (0, _numeral.default)(data.packetbeatFlow).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.packetBeatFlowTitle",
      defaultMessage: "Packetbeat Flow"
    }),
    id: 'packetbeatFlow'
  }, {
    description: (0, _fp.has)('packetbeatTLS', data) && data.packetbeatTLS !== null ? (0, _numeral.default)(data.packetbeatTLS).format('0,0') : (0, _empty_value.getEmptyTagValue)(),
    title: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.siem.overview.packetbeatTLSTitle",
      defaultMessage: "Packetbeat TLS"
    }),
    id: 'packetbeatTLS'
  }];
};

var DescriptionListDescription = (0, _styledComponents.default)(_eui.EuiDescriptionListDescription)(_templateObject());
exports.DescriptionListDescription = DescriptionListDescription;
DescriptionListDescription.displayName = 'DescriptionListDescription';
var StatValue = (0, _recompose.pure)(function (_ref) {
  var isLoading = _ref.isLoading,
      value = _ref.value;
  return _react2.default.createElement(_react2.default.Fragment, null, isLoading ? _react2.default.createElement(_eui.EuiLoadingSpinner, {
    size: "m"
  }) : value != null ? value : (0, _empty_value.getEmptyTagValue)());
});
StatValue.displayName = 'StatValue';
var OverviewNetworkStats = (0, _recompose.pure)(function (_ref2) {
  var data = _ref2.data,
      loading = _ref2.loading;
  return _react2.default.createElement(_eui.EuiDescriptionList, {
    type: "column"
  }, overviewNetworkStats(data).map(function (item, index) {
    return _react2.default.createElement(_react2.default.Fragment, {
      key: index
    }, _react2.default.createElement(_eui.EuiDescriptionListTitle, null, item.title), _react2.default.createElement(DescriptionListDescription, {
      "data-test-subj": "network-stat-".concat(item.id)
    }, _react2.default.createElement(StatValue, {
      isLoading: loading,
      value: item.description
    })));
  }));
});
exports.OverviewNetworkStats = OverviewNetworkStats;
OverviewNetworkStats.displayName = 'OverviewNetworkStats';