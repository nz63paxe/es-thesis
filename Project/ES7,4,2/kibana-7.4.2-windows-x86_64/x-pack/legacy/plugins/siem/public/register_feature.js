"use strict";

var _feature_catalogue = require("ui/registry/feature_catalogue");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var APP_ID = 'siem';

_feature_catalogue.FeatureCatalogueRegistryProvider.register(function () {
  return {
    id: 'siem',
    title: 'SIEM',
    description: 'Explore security metrics and logs for events and alerts',
    icon: 'securityAnalyticsApp',
    path: "/app/".concat(APP_ID),
    showOnHomePage: true,
    category: _feature_catalogue.FeatureCatalogueCategory.DATA
  };
});