"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsFilter = void 0;

var _fp = require("lodash/fp");

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _keury = require("../../lib/keury");

var _store = require("../../store");

var _actions = require("../../store/actions");

var _use_update_kql = require("../../utils/kql/use_update_kql");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var HostsFilterComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(HostsFilterComponent, _React$PureComponent);

  function HostsFilterComponent(props) {
    var _this;

    _classCallCheck(this, HostsFilterComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HostsFilterComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedApplyFilterQueryFromKueryExpression", void 0);

    _defineProperty(_assertThisInitialized(_this), "memoizedSetFilterQueryDraftFromKueryExpression", void 0);

    _defineProperty(_assertThisInitialized(_this), "applyFilterQueryFromKueryExpression", function (expression) {
      return _this.props.applyHostsFilterQuery({
        filterQuery: {
          kuery: {
            kind: 'kuery',
            expression: expression
          },
          serializedQuery: (0, _keury.convertKueryToElasticSearchQuery)(expression, _this.props.indexPattern)
        },
        hostsType: _this.props.type
      });
    });

    _defineProperty(_assertThisInitialized(_this), "setFilterQueryDraftFromKueryExpression", function (expression) {
      return _this.props.setHostsFilterQueryDraft({
        filterQueryDraft: {
          kind: 'kuery',
          expression: expression
        },
        hostsType: _this.props.type
      });
    });

    _this.memoizedApplyFilterQueryFromKueryExpression = (0, _memoizeOne.default)(_this.applyFilterQueryFromKueryExpression);
    _this.memoizedSetFilterQueryDraftFromKueryExpression = (0, _memoizeOne.default)(_this.setFilterQueryDraftFromKueryExpression);
    return _this;
  }

  _createClass(HostsFilterComponent, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      var _this$props = this.props,
          indexPattern = _this$props.indexPattern,
          hostsFilterQueryDraft = _this$props.hostsFilterQueryDraft,
          kueryFilterQuery = _this$props.kueryFilterQuery,
          setQuery = _this$props.setQuery,
          type = _this$props.type;

      if (setQuery && (!(0, _fp.isEqual)(prevProps.hostsFilterQueryDraft, hostsFilterQueryDraft) || !(0, _fp.isEqual)(prevProps.kueryFilterQuery, kueryFilterQuery) || prevProps.type !== type)) {
        setQuery({
          id: 'kql',
          inspect: null,
          loading: false,
          refetch: (0, _use_update_kql.useUpdateKql)({
            indexPattern: indexPattern,
            kueryFilterQuery: kueryFilterQuery,
            kueryFilterQueryDraft: hostsFilterQueryDraft,
            storeType: 'hostsType',
            type: type
          })
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          children = _this$props2.children,
          hostsFilterQueryDraft = _this$props2.hostsFilterQueryDraft,
          isHostFilterQueryDraftValid = _this$props2.isHostFilterQueryDraftValid;
      return _react.default.createElement(_react.default.Fragment, null, children({
        applyFilterQueryFromKueryExpression: this.memoizedApplyFilterQueryFromKueryExpression,
        filterQueryDraft: hostsFilterQueryDraft,
        isFilterQueryDraftValid: isHostFilterQueryDraftValid,
        setFilterQueryDraftFromKueryExpression: this.memoizedSetFilterQueryDraftFromKueryExpression
      }));
    }
  }]);

  return HostsFilterComponent;
}(_react.default.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getHostsFilterQueryDraft = _store.hostsSelectors.hostsFilterQueryDraft();

  var getIsHostFilterQueryDraftValid = _store.hostsSelectors.isHostFilterQueryDraftValid();

  var getHostsKueryFilterQuery = _store.hostsSelectors.hostsFilterQueryAsKuery();

  var mapStateToProps = function mapStateToProps(state, _ref) {
    var type = _ref.type;
    return {
      hostsFilterQueryDraft: getHostsFilterQueryDraft(state, type),
      isHostFilterQueryDraftValid: getIsHostFilterQueryDraftValid(state, type),
      kueryFilterQuery: getHostsKueryFilterQuery(state, type)
    };
  };

  return mapStateToProps;
};

var HostsFilter = (0, _reactRedux.connect)(makeMapStateToProps, {
  applyHostsFilterQuery: _actions.hostsActions.applyHostsFilterQuery,
  setHostsFilterQueryDraft: _actions.hostsActions.setHostsFilterQueryDraft
})(HostsFilterComponent);
exports.HostsFilter = HostsFilter;