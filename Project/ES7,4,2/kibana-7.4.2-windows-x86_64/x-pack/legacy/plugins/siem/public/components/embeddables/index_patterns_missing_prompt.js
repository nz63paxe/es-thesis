"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IndexPatternsMissingPrompt = void 0;

var _documentation_links = require("ui/documentation_links");

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var IndexPatternsMissingPrompt = React.memo(function () {
  var beatsSetupDocMapping = [{
    beat: 'auditbeat',
    docLink: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "/guide/en/beats/auditbeat/").concat(_documentation_links.DOC_LINK_VERSION, "/load-kibana-dashboards.html")
  }, {
    beat: 'filebeat',
    docLink: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "/guide/en/beats/filebeat/").concat(_documentation_links.DOC_LINK_VERSION, "/load-kibana-dashboards.html")
  }, {
    beat: 'packetbeat',
    docLink: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "/guide/en/beats/packetbeat/").concat(_documentation_links.DOC_LINK_VERSION, "/load-kibana-dashboards.html")
  }, {
    beat: 'winlogbeat',
    docLink: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "/guide/en/beats/winlogbeat/").concat(_documentation_links.DOC_LINK_VERSION, "/load-kibana-dashboards.html")
  }];
  return React.createElement(_eui.EuiEmptyPrompt, {
    iconType: "gisApp",
    title: React.createElement("h2", null, i18n.ERROR_TITLE),
    titleSize: "xs",
    body: React.createElement(React.Fragment, null, React.createElement("p", null, i18n.ERROR_DESCRIPTION), React.createElement("p", null, beatsSetupDocMapping.map(function (v) {
      return React.createElement(_eui.EuiLink, {
        key: v.beat,
        href: v.docLink,
        target: "blank"
      }, "".concat(v.beat, "-*"));
    }).reduce(function (acc, v) {
      return [acc, ', ', v];
    }))),
    actions: React.createElement(_eui.EuiButton, {
      href: "".concat(_chrome.default.getBasePath(), "/app/kibana#/management/kibana/index_patterns"),
      color: "primary",
      target: "_blank",
      fill: true
    }, i18n.ERROR_BUTTON)
  });
});
exports.IndexPatternsMissingPrompt = IndexPatternsMissingPrompt;