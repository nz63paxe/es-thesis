"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserProcess = exports.USER_NAME_FIELD_NAME = exports.PROCESS_NAME_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _draggables = require("../../draggables");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var PROCESS_NAME_FIELD_NAME = 'process.name';
exports.PROCESS_NAME_FIELD_NAME = PROCESS_NAME_FIELD_NAME;
var USER_NAME_FIELD_NAME = 'user.name';
/**
 * Renders a column of draggable badges containing:
 * - `user.name`
 * - `process.name`
 */

exports.USER_NAME_FIELD_NAME = USER_NAME_FIELD_NAME;
var UserProcess = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      processName = _ref.processName,
      userName = _ref.userName;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "flexStart",
    "data-test-subj": "user-process",
    direction: "column",
    justifyContent: "center",
    gutterSize: "none"
  }, userName != null ? (0, _fp.uniq)(userName).map(function (user) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: user
    }, React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      "data-test-subj": "user-name",
      eventId: eventId,
      field: USER_NAME_FIELD_NAME,
      value: user,
      iconType: "user"
    }));
  }) : null, processName != null ? (0, _fp.uniq)(processName).map(function (process) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: process
    }, React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      "data-test-subj": "process-name",
      eventId: eventId,
      field: PROCESS_NAME_FIELD_NAME,
      value: process,
      iconType: "console"
    }));
  }) : null);
});
exports.UserProcess = UserProcess;
UserProcess.displayName = 'UserProcess';