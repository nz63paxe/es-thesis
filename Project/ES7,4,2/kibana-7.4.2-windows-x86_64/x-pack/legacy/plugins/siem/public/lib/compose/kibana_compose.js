"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

var _apolloCacheInmemory = require("apollo-cache-inmemory");

var _apolloClient = _interopRequireDefault(require("apollo-client"));

var _apolloLink = require("apollo-link");

require("ui/autoload/all");

var _modules = require("ui/modules");

var _introspection = _interopRequireDefault(require("../../graphql/introspection.json"));

var _helpers = require("./helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore: path dynamic for kibana
function compose() {
  var cache = new _apolloCacheInmemory.InMemoryCache({
    dataIdFromObject: function dataIdFromObject() {
      return null;
    },
    fragmentMatcher: new _apolloCacheInmemory.IntrospectionFragmentMatcher({
      introspectionQueryResultData: _introspection.default
    })
  });
  var graphQLOptions = {
    connectToDevTools: process.env.NODE_ENV !== 'production',
    cache: cache,
    link: _apolloLink.ApolloLink.from((0, _helpers.getLinks)(cache))
  };
  var apolloClient = new _apolloClient.default(graphQLOptions);

  var appModule = _modules.uiModules.get('app/siem'); // disable angular's location provider


  appModule.config(function ($locationProvider) {
    $locationProvider.html5Mode({
      enabled: false,
      requireBase: false,
      rewriteLinks: false
    });
  }); // const framework = new AppKibanaFrameworkAdapter(appModule, uiRoutes, timezoneProvider);

  var libs = {
    apolloClient: apolloClient
  };
  return libs;
}