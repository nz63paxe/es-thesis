"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventsViewer = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _auto_sizer = require("../auto_sizer");

var _timeline = require("../../containers/timeline");

var _default_headers = require("../timeline/body/column_headers/default_headers");

var _stateful_body = require("../timeline/body/stateful_body");

var _footer = require("../timeline/footer");

var _helpers = require("../timeline/helpers");

var _timeline2 = require("../timeline/timeline");

var _timeline_context = require("../timeline/timeline_context");

var _events_viewer_header = require("./events_viewer_header");

var _refetch_timeline = require("../timeline/refetch_timeline");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  padding: 0 10px 0 12px;\n  user-select: none;\n  width: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DEFAULT_EVENTS_VIEWER_HEIGHT = 500;

var WrappedByAutoSizer = _styledComponents.default.div(_templateObject()); // required by AutoSizer


WrappedByAutoSizer.displayName = 'WrappedByAutoSizer';
var EventsViewerContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2());
EventsViewerContainer.displayName = 'EventsViewerContainer';

var EventsViewer = _react.default.memo(function (_ref) {
  var browserFields = _ref.browserFields,
      columns = _ref.columns,
      dataProviders = _ref.dataProviders,
      end = _ref.end,
      _ref$height = _ref.height,
      height = _ref$height === void 0 ? DEFAULT_EVENTS_VIEWER_HEIGHT : _ref$height,
      id = _ref.id,
      indexPattern = _ref.indexPattern,
      isLive = _ref.isLive,
      itemsPerPage = _ref.itemsPerPage,
      itemsPerPageOptions = _ref.itemsPerPageOptions,
      kqlMode = _ref.kqlMode,
      kqlQueryExpression = _ref.kqlQueryExpression,
      onChangeItemsPerPage = _ref.onChangeItemsPerPage,
      showInspect = _ref.showInspect,
      start = _ref.start,
      sort = _ref.sort,
      toggleColumn = _ref.toggleColumn;
  var columnsHeader = (0, _fp.isEmpty)(columns) ? _default_headers.defaultHeaders : columns;
  var combinedQueries = (0, _helpers.combineQueries)(dataProviders, indexPattern, browserFields, kqlQueryExpression, kqlMode, start, end, true);
  return _react.default.createElement(_eui.EuiPanel, {
    "data-test-subj": "events-viewer-panel",
    grow: false
  }, _react.default.createElement(_auto_sizer.AutoSizer, {
    detectAnyWindowResize: true,
    content: true
  }, function (_ref2) {
    var measureRef = _ref2.measureRef,
        _ref2$content$width = _ref2.content.width,
        width = _ref2$content$width === void 0 ? 0 : _ref2$content$width;
    return _react.default.createElement(EventsViewerContainer, {
      "data-test-subj": "events-viewer-container",
      direction: "column",
      gutterSize: "none",
      justifyContent: "flexStart"
    }, _react.default.createElement(WrappedByAutoSizer, {
      innerRef: measureRef
    }, _react.default.createElement("div", {
      "data-test-subj": "events-viewer-measured",
      style: {
        height: '0px',
        width: '100%'
      }
    })), combinedQueries != null ? _react.default.createElement(_timeline.TimelineQuery, {
      fields: columnsHeader.map(function (c) {
        return c.id;
      }),
      filterQuery: combinedQueries.filterQuery,
      id: id,
      limit: itemsPerPage,
      sortField: {
        sortFieldId: sort.columnId,
        direction: sort.sortDirection
      },
      sourceId: "default"
    }, function (_ref3) {
      var events = _ref3.events,
          getUpdatedAt = _ref3.getUpdatedAt,
          inspect = _ref3.inspect,
          loading = _ref3.loading,
          loadMore = _ref3.loadMore,
          pageInfo = _ref3.pageInfo,
          refetch = _ref3.refetch,
          _ref3$totalCount = _ref3.totalCount,
          totalCount = _ref3$totalCount === void 0 ? 0 : _ref3$totalCount;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_events_viewer_header.EventsViewerHeader, {
        id: id,
        showInspect: showInspect,
        totalCount: totalCount
      }), _react.default.createElement("div", {
        "data-test-subj": "events-container",
        style: {
          width: "".concat(width, "px")
        }
      }, _react.default.createElement(_timeline_context.ManageTimelineContext, {
        loading: loading,
        width: width
      }, _react.default.createElement(_refetch_timeline.TimelineRefetch, {
        id: id,
        inputId: "global",
        inspect: inspect,
        loading: loading,
        refetch: refetch
      }), _react.default.createElement(_stateful_body.StatefulBody, {
        browserFields: browserFields,
        data: events,
        id: id,
        isEventViewer: true,
        height: height,
        sort: sort,
        toggleColumn: toggleColumn
      }), _react.default.createElement(_footer.Footer, {
        compact: (0, _timeline2.isCompactFooter)(width),
        getUpdatedAt: getUpdatedAt,
        hasNextPage: (0, _fp.getOr)(false, 'hasNextPage', pageInfo),
        height: _footer.footerHeight,
        isEventViewer: true,
        isLive: isLive,
        isLoading: loading,
        itemsCount: events.length,
        itemsPerPage: itemsPerPage,
        itemsPerPageOptions: itemsPerPageOptions,
        onChangeItemsPerPage: onChangeItemsPerPage,
        onLoadMore: loadMore,
        nextCursor: (0, _fp.getOr)(null, 'endCursor.value', pageInfo),
        serverSideEventCount: totalCount,
        tieBreaker: (0, _fp.getOr)(null, 'endCursor.tiebreaker', pageInfo)
      }))));
    }) : null);
  }));
}, function (prevProps, nextProps) {
  return prevProps.browserFields === nextProps.browserFields && prevProps.columns === nextProps.columns && prevProps.dataProviders === nextProps.dataProviders && prevProps.end === nextProps.end && prevProps.height === nextProps.height && prevProps.id === nextProps.id && prevProps.indexPattern === nextProps.indexPattern && prevProps.isLive === nextProps.isLive && prevProps.itemsPerPage === nextProps.itemsPerPage && prevProps.itemsPerPageOptions === nextProps.itemsPerPageOptions && prevProps.kqlMode === nextProps.kqlMode && prevProps.kqlQueryExpression === nextProps.kqlQueryExpression && prevProps.showInspect === nextProps.showInspect && prevProps.start === nextProps.start && prevProps.sort === nextProps.sort;
});

exports.EventsViewer = EventsViewer;
EventsViewer.displayName = 'EventsViewer';