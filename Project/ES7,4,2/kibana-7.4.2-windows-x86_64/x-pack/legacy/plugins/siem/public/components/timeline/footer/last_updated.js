"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LastUpdatedAt = exports.Updated = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Updated = (0, _recompose.pure)(function (_ref) {
  var date = _ref.date,
      prefix = _ref.prefix,
      updatedAt = _ref.updatedAt;
  return React.createElement(React.Fragment, null, prefix, React.createElement(_react.FormattedRelative, {
    "data-test-subj": "last-updated-at-date",
    key: "formatedRelative-".concat(date),
    value: new Date(updatedAt)
  }));
});
exports.Updated = Updated;
Updated.displayName = 'Updated';

var LastUpdatedAt =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(LastUpdatedAt, _React$PureComponent);

  function LastUpdatedAt() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LastUpdatedAt);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LastUpdatedAt)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      date: Date.now()
    });

    _defineProperty(_assertThisInitialized(_this), "timerID", void 0);

    return _this;
  }

  _createClass(LastUpdatedAt, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      this.timerID = setInterval(function () {
        return _this2.tick();
      }, 10000);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      clearInterval(this.timerID);
    }
  }, {
    key: "tick",
    value: function tick() {
      this.setState({
        date: Date.now()
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$compact = this.props.compact,
          compact = _this$props$compact === void 0 ? false : _this$props$compact;
      var prefix = " ".concat(i18n.UPDATED, " ");
      return React.createElement(_eui.EuiToolTip, {
        "data-test-subj": "timeline-stream-tool-tip",
        content: React.createElement(React.Fragment, null, React.createElement(Updated, {
          date: this.state.date,
          prefix: prefix,
          updatedAt: this.props.updatedAt
        }))
      }, React.createElement(_eui.EuiText, {
        size: "s"
      }, React.createElement(_eui.EuiIcon, {
        "data-test-subj": "last-updated-at-clock-icon",
        type: "clock"
      }), !compact ? React.createElement(Updated, {
        date: this.state.date,
        prefix: prefix,
        updatedAt: this.props.updatedAt
      }) : null));
    }
  }]);

  return LastUpdatedAt;
}(React.PureComponent);

exports.LastUpdatedAt = LastUpdatedAt;