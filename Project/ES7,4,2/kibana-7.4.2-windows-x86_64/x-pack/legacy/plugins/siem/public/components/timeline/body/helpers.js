"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getActionsColumnWidth = exports.getColumnHeaders = exports.getColumnWidthFromType = exports.getPinOnClick = exports.eventIsPinned = exports.getPinTooltip = exports.eventHasNotes = exports.stringifyEvent = exports.omitTypenameAndEmpty = exports.DEFAULT_TIMELINE_WIDTH = exports.DEFAULT_DATE_COLUMN_MIN_WIDTH = exports.DEFAULT_COLUMN_MIN_WIDTH = exports.EVENTS_VIEWER_ACTIONS_COLUMN_WIDTH = exports.DEFAULT_ACTIONS_COLUMN_WIDTH = void 0;

var _fp = require("lodash/fp");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/** The (fixed) width of the Actions column */
var DEFAULT_ACTIONS_COLUMN_WIDTH = 115; // px;

/**
 * The (fixed) width of the Actions column when the timeline body is used as
 * an events viewer, which has fewer actions than a regular events viewer
 */

exports.DEFAULT_ACTIONS_COLUMN_WIDTH = DEFAULT_ACTIONS_COLUMN_WIDTH;
var EVENTS_VIEWER_ACTIONS_COLUMN_WIDTH = 25; // px;

/** The default minimum width of a column (when a width for the column type is not specified) */

exports.EVENTS_VIEWER_ACTIONS_COLUMN_WIDTH = EVENTS_VIEWER_ACTIONS_COLUMN_WIDTH;
var DEFAULT_COLUMN_MIN_WIDTH = 180; // px

/** The default minimum width of a column of type `date` */

exports.DEFAULT_COLUMN_MIN_WIDTH = DEFAULT_COLUMN_MIN_WIDTH;
var DEFAULT_DATE_COLUMN_MIN_WIDTH = 190; // px

exports.DEFAULT_DATE_COLUMN_MIN_WIDTH = DEFAULT_DATE_COLUMN_MIN_WIDTH;
var DEFAULT_TIMELINE_WIDTH = 1100; // px
// eslint-disable-next-line @typescript-eslint/no-explicit-any

exports.DEFAULT_TIMELINE_WIDTH = DEFAULT_TIMELINE_WIDTH;

var omitTypenameAndEmpty = function omitTypenameAndEmpty(k, v) {
  return k !== '__typename' && v != null ? v : undefined;
};

exports.omitTypenameAndEmpty = omitTypenameAndEmpty;

var stringifyEvent = function stringifyEvent(ecs) {
  return JSON.stringify(ecs, omitTypenameAndEmpty, 2);
};

exports.stringifyEvent = stringifyEvent;

var eventHasNotes = function eventHasNotes(noteIds) {
  return !(0, _fp.isEmpty)(noteIds);
};

exports.eventHasNotes = eventHasNotes;

var getPinTooltip = function getPinTooltip(_ref) {
  var isPinned = _ref.isPinned,
      eventHasNotes = _ref.eventHasNotes;
  return isPinned && eventHasNotes ? i18n.PINNED_WITH_NOTES : isPinned ? i18n.PINNED : i18n.UNPINNED;
};

exports.getPinTooltip = getPinTooltip;

var eventIsPinned = function eventIsPinned(_ref2) {
  var eventId = _ref2.eventId,
      pinnedEventIds = _ref2.pinnedEventIds;
  return pinnedEventIds[eventId] === true;
};

exports.eventIsPinned = eventIsPinned;

var getPinOnClick = function getPinOnClick(_ref3) {
  var allowUnpinning = _ref3.allowUnpinning,
      eventId = _ref3.eventId,
      onPinEvent = _ref3.onPinEvent,
      onUnPinEvent = _ref3.onUnPinEvent,
      pinnedEventIds = _ref3.pinnedEventIds;

  if (!allowUnpinning) {
    return _fp.noop;
  }

  return eventIsPinned({
    eventId: eventId,
    pinnedEventIds: pinnedEventIds
  }) ? function () {
    return onUnPinEvent(eventId);
  } : function () {
    return onPinEvent(eventId);
  };
};

exports.getPinOnClick = getPinOnClick;

var getColumnWidthFromType = function getColumnWidthFromType(type) {
  return type !== 'date' ? DEFAULT_COLUMN_MIN_WIDTH : DEFAULT_DATE_COLUMN_MIN_WIDTH;
};
/** Enriches the column headers with field details from the specified browserFields */


exports.getColumnWidthFromType = getColumnWidthFromType;

var getColumnHeaders = function getColumnHeaders(headers, browserFields) {
  return headers.map(function (header) {
    var splitHeader = header.id.split('.'); // source.geo.city_name -> [source, geo, city_name]

    return _objectSpread({}, header, {}, (0, _fp.get)([splitHeader.length > 1 ? splitHeader[0] : 'base', 'fields', header.id], browserFields));
  });
};
/** Returns the (fixed) width of the Actions column */


exports.getColumnHeaders = getColumnHeaders;

var getActionsColumnWidth = function getActionsColumnWidth(isEventViewer) {
  return isEventViewer ? EVENTS_VIEWER_ACTIONS_COLUMN_WIDTH : DEFAULT_ACTIONS_COLUMN_WIDTH;
};

exports.getActionsColumnWidth = getActionsColumnWidth;