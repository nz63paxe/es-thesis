"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NoteCreated = void 0;

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _localized_date_tooltip = require("../../localized_date_tooltip");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  user-select: none;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NoteCreatedContainer = _styledComponents.default.span(_templateObject());

NoteCreatedContainer.displayName = 'NoteCreatedContainer';
var NoteCreated = (0, _recompose.pure)(function (_ref) {
  var created = _ref.created;
  return React.createElement(NoteCreatedContainer, {
    "data-test-subj": "note-created"
  }, React.createElement(_localized_date_tooltip.LocalizedDateTooltip, {
    date: created
  }, React.createElement(_react.FormattedRelative, {
    value: created
  })));
});
exports.NoteCreated = NoteCreated;
NoteCreated.displayName = 'NoteCreated';