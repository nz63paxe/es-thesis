"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FieldsBrowser = void 0;

var _fp = require("lodash/fp");

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("./helpers");

var _header = require("./header");

var _categories_pane = require("./categories_pane");

var _fields_pane = require("./fields_pane");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  width: ", "px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  border: 1px solid ", ";\n  border-radius: 4px;\n  padding: 8px 8px 16px 8px;\n  position: absolute;\n  top: 25px;\n  ", ";\n  z-index: 9990;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FieldsBrowserContainer = _styledComponents.default.div(_templateObject(), function (props) {
  return props.theme.eui.euiColorLightestShade;
}, function (_ref) {
  var theme = _ref.theme;
  return theme.eui.euiColorMediumShade;
}, function (_ref2) {
  var width = _ref2.width;
  return "width: ".concat(width, "px");
});

FieldsBrowserContainer.displayName = 'FieldsBrowserContainer';
var PanesFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2(), _helpers.PANES_FLEX_GROUP_WIDTH);
PanesFlexGroup.displayName = 'PanesFlexGroup';

/**
 * This component has no internal state, but it uses lifecycle methods to
 * set focus to the search input, scroll to the selected category, etc
 */
var FieldsBrowser =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(FieldsBrowser, _React$PureComponent);

  function FieldsBrowser() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FieldsBrowser);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FieldsBrowser)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "focusInput", function () {
      var elements = document.getElementsByClassName((0, _helpers.getFieldBrowserSearchInputClassName)(_this.props.timelineId));

      if (elements.length > 0) {
        elements[0].focus(); // this cast is required because focus() does not exist on every `Element` returned by `getElementsByClassName`
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onInputChange", function (event) {
      return _this.props.onSearchInputChange(event.target.value);
    });

    _defineProperty(_assertThisInitialized(_this), "selectFieldAndHide", function (fieldId) {
      var _this$props = _this.props,
          onFieldSelected = _this$props.onFieldSelected,
          onHideFieldBrowser = _this$props.onHideFieldBrowser;

      if (onFieldSelected != null) {
        onFieldSelected(fieldId);
      }

      onHideFieldBrowser();
    });

    _defineProperty(_assertThisInitialized(_this), "scrollViews", function () {
      var _this$props2 = _this.props,
          selectedCategoryId = _this$props2.selectedCategoryId,
          timelineId = _this$props2.timelineId;

      if (_this.props.selectedCategoryId !== '') {
        var categoryPaneTitles = document.getElementsByClassName((0, _helpers.getCategoryPaneCategoryClassName)({
          categoryId: selectedCategoryId,
          timelineId: timelineId
        }));

        if (categoryPaneTitles.length > 0) {
          categoryPaneTitles[0].scrollIntoView();
        }

        var fieldPaneTitles = document.getElementsByClassName((0, _helpers.getFieldBrowserCategoryTitleClassName)({
          categoryId: selectedCategoryId,
          timelineId: timelineId
        }));

        if (fieldPaneTitles.length > 0) {
          fieldPaneTitles[0].scrollIntoView();
        }
      }

      _this.focusInput(); // always re-focus the input to enable additional filtering

    });

    return _this;
  }

  _createClass(FieldsBrowser, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.scrollViews();
      this.focusInput();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.scrollViews();
      this.focusInput(); // always re-focus the input to enable additional filtering
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          columnHeaders = _this$props3.columnHeaders,
          browserFields = _this$props3.browserFields,
          filteredBrowserFields = _this$props3.filteredBrowserFields,
          searchInput = _this$props3.searchInput,
          isEventViewer = _this$props3.isEventViewer,
          isSearching = _this$props3.isSearching,
          onCategorySelected = _this$props3.onCategorySelected,
          onFieldSelected = _this$props3.onFieldSelected,
          onOutsideClick = _this$props3.onOutsideClick,
          onUpdateColumns = _this$props3.onUpdateColumns,
          selectedCategoryId = _this$props3.selectedCategoryId,
          timelineId = _this$props3.timelineId,
          toggleColumn = _this$props3.toggleColumn,
          width = _this$props3.width;
      return React.createElement(_eui.EuiOutsideClickDetector, {
        "data-test-subj": "outside-click-detector",
        onOutsideClick: onFieldSelected != null ? _fp.noop : onOutsideClick,
        isDisabled: false
      }, React.createElement(FieldsBrowserContainer, {
        "data-test-subj": "fields-browser-container",
        width: width
      }, React.createElement(_header.Header, {
        "data-test-subj": "header",
        filteredBrowserFields: filteredBrowserFields,
        isEventViewer: isEventViewer,
        isSearching: isSearching,
        onOutsideClick: onOutsideClick,
        onSearchInputChange: this.onInputChange,
        onUpdateColumns: onUpdateColumns,
        searchInput: searchInput,
        timelineId: timelineId
      }), React.createElement(PanesFlexGroup, {
        alignItems: "flexStart",
        gutterSize: "none",
        justifyContent: "spaceBetween"
      }, React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_categories_pane.CategoriesPane, {
        browserFields: browserFields,
        "data-test-subj": "left-categories-pane",
        filteredBrowserFields: filteredBrowserFields,
        width: _helpers.CATEGORY_PANE_WIDTH,
        onCategorySelected: onCategorySelected,
        onUpdateColumns: onUpdateColumns,
        selectedCategoryId: selectedCategoryId,
        timelineId: timelineId
      })), React.createElement(_eui.EuiFlexItem, {
        grow: false
      }, React.createElement(_fields_pane.FieldsPane, {
        columnHeaders: columnHeaders,
        "data-test-subj": "fields-pane",
        filteredBrowserFields: filteredBrowserFields,
        onCategorySelected: onCategorySelected,
        onFieldSelected: this.selectFieldAndHide,
        onUpdateColumns: onUpdateColumns,
        searchInput: searchInput,
        selectedCategoryId: selectedCategoryId,
        timelineId: timelineId,
        toggleColumn: toggleColumn,
        width: _helpers.FIELDS_PANE_WIDTH
      })))));
    }
    /** Focuses the input that filters the field browser */

  }]);

  return FieldsBrowser;
}(React.PureComponent);

exports.FieldsBrowser = FieldsBrowser;