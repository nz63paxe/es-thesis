"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Pin = exports.getPinIcon = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var i18n = _interopRequireWildcard(require("../../components/timeline/body/translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  svg {\n    ", "\n    height: 19px;\n    ", "px;\n    position: relative;\n    width: 19px;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var getPinIcon = function getPinIcon(pinned) {
  return pinned ? 'pinFilled' : 'pin';
};

exports.getPinIcon = getPinIcon;
var PinButtonIcon = (0, _styledComponents.default)(_eui.EuiButtonIcon)(_templateObject(), function (_ref) {
  var pinned = _ref.pinned,
      theme = _ref.theme;
  return pinned === 'true' ? "fill: ".concat(theme.eui.euiColorPrimary, ";") : '';
}, function (_ref2) {
  var pinned = _ref2.pinned;
  return "left: ".concat(pinned === 'true' ? '-2' : '-1');
});
var Pin = (0, _recompose.pure)(function (_ref3) {
  var allowUnpinning = _ref3.allowUnpinning,
      pinned = _ref3.pinned,
      _ref3$onClick = _ref3.onClick,
      onClick = _ref3$onClick === void 0 ? _fp.noop : _ref3$onClick;
  return React.createElement(PinButtonIcon, {
    "aria-label": pinned ? i18n.PINNED : i18n.UNPINNED,
    isDisabled: allowUnpinning ? false : true,
    color: pinned ? 'primary' : 'subdued',
    "data-test-subj": "pin",
    onClick: onClick,
    pinned: pinned.toString(),
    role: "button",
    size: "l",
    iconType: getPinIcon(pinned)
  });
});
exports.Pin = Pin;
Pin.displayName = 'Pin';