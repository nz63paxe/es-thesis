"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Timeline = exports.isCompactFooter = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _timeline = require("../../containers/timeline");

var _auto_sizer = require("../auto_sizer");

var _default_headers = require("./body/column_headers/default_headers");

var _stateful_body = require("./body/stateful_body");

var _footer = require("./footer");

var _header = require("./header");

var _helpers = require("./helpers");

var _refetch_timeline = require("./refetch_timeline");

var _timeline_context = require("./timeline_context");

var _fetch_kql_timeline = require("./fetch_kql_timeline");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  min-height: 500px;\n  overflow: hidden;\n  padding: 0 10px 0 12px;\n  user-select: none;\n  width: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var WrappedByAutoSizer = _styledComponents.default.div(_templateObject()); // required by AutoSizer


WrappedByAutoSizer.displayName = 'WrappedByAutoSizer';
var TimelineContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2());
TimelineContainer.displayName = 'TimelineContainer';

var isCompactFooter = function isCompactFooter(width) {
  return width < 600;
};

exports.isCompactFooter = isCompactFooter;

/** The parent Timeline component */
var Timeline = React.memo(function (_ref) {
  var browserFields = _ref.browserFields,
      columns = _ref.columns,
      dataProviders = _ref.dataProviders,
      end = _ref.end,
      flyoutHeaderHeight = _ref.flyoutHeaderHeight,
      flyoutHeight = _ref.flyoutHeight,
      id = _ref.id,
      indexPattern = _ref.indexPattern,
      isLive = _ref.isLive,
      itemsPerPage = _ref.itemsPerPage,
      itemsPerPageOptions = _ref.itemsPerPageOptions,
      kqlMode = _ref.kqlMode,
      kqlQueryExpression = _ref.kqlQueryExpression,
      onChangeDataProviderKqlQuery = _ref.onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider = _ref.onChangeDroppableAndProvider,
      onChangeItemsPerPage = _ref.onChangeItemsPerPage,
      onDataProviderEdited = _ref.onDataProviderEdited,
      onDataProviderRemoved = _ref.onDataProviderRemoved,
      onToggleDataProviderEnabled = _ref.onToggleDataProviderEnabled,
      onToggleDataProviderExcluded = _ref.onToggleDataProviderExcluded,
      show = _ref.show,
      showCallOutUnauthorizedMsg = _ref.showCallOutUnauthorizedMsg,
      start = _ref.start,
      sort = _ref.sort,
      toggleColumn = _ref.toggleColumn;
  var combinedQueries = (0, _helpers.combineQueries)(dataProviders, indexPattern, browserFields, kqlQueryExpression, kqlMode, start, end);
  var columnsHeader = (0, _fp.isEmpty)(columns) ? _default_headers.defaultHeaders : columns;
  return React.createElement(_auto_sizer.AutoSizer, {
    detectAnyWindowResize: true,
    content: true
  }, function (_ref2) {
    var measureRef = _ref2.measureRef,
        _ref2$content = _ref2.content,
        _ref2$content$height = _ref2$content.height,
        timelineHeaderHeight = _ref2$content$height === void 0 ? 0 : _ref2$content$height,
        _ref2$content$width = _ref2$content.width,
        width = _ref2$content$width === void 0 ? 0 : _ref2$content$width;
    return React.createElement(TimelineContainer, {
      "data-test-subj": "timeline",
      direction: "column",
      gutterSize: "none",
      justifyContent: "flexStart"
    }, React.createElement(WrappedByAutoSizer, {
      innerRef: measureRef
    }, React.createElement(_header.TimelineHeader, {
      browserFields: browserFields,
      id: id,
      indexPattern: indexPattern,
      dataProviders: dataProviders,
      onChangeDataProviderKqlQuery: onChangeDataProviderKqlQuery,
      onChangeDroppableAndProvider: onChangeDroppableAndProvider,
      onDataProviderEdited: onDataProviderEdited,
      onDataProviderRemoved: onDataProviderRemoved,
      onToggleDataProviderEnabled: onToggleDataProviderEnabled,
      onToggleDataProviderExcluded: onToggleDataProviderExcluded,
      show: show,
      showCallOutUnauthorizedMsg: showCallOutUnauthorizedMsg,
      sort: sort
    })), React.createElement(_fetch_kql_timeline.TimelineKqlFetch, {
      id: id,
      indexPattern: indexPattern,
      inputId: "timeline"
    }), combinedQueries != null ? React.createElement(_timeline.TimelineQuery, {
      id: id,
      fields: columnsHeader.map(function (c) {
        return c.id;
      }),
      sourceId: "default",
      limit: itemsPerPage,
      filterQuery: combinedQueries.filterQuery,
      sortField: {
        sortFieldId: sort.columnId,
        direction: sort.sortDirection
      }
    }, function (_ref3) {
      var events = _ref3.events,
          inspect = _ref3.inspect,
          loading = _ref3.loading,
          totalCount = _ref3.totalCount,
          pageInfo = _ref3.pageInfo,
          loadMore = _ref3.loadMore,
          getUpdatedAt = _ref3.getUpdatedAt,
          refetch = _ref3.refetch;
      return React.createElement(_timeline_context.ManageTimelineContext, {
        loading: loading,
        width: width
      }, React.createElement(_refetch_timeline.TimelineRefetch, {
        id: id,
        inputId: "timeline",
        inspect: inspect,
        loading: loading,
        refetch: refetch
      }), React.createElement(_stateful_body.StatefulBody, {
        browserFields: browserFields,
        data: events,
        id: id,
        height: (0, _helpers.calculateBodyHeight)({
          flyoutHeight: flyoutHeight,
          flyoutHeaderHeight: flyoutHeaderHeight,
          timelineHeaderHeight: timelineHeaderHeight,
          timelineFooterHeight: _footer.footerHeight
        }),
        sort: sort,
        toggleColumn: toggleColumn
      }), React.createElement(_footer.Footer, {
        serverSideEventCount: totalCount,
        hasNextPage: (0, _fp.getOr)(false, 'hasNextPage', pageInfo),
        height: _footer.footerHeight,
        isLive: isLive,
        isLoading: loading,
        itemsCount: events.length,
        itemsPerPage: itemsPerPage,
        itemsPerPageOptions: itemsPerPageOptions,
        onChangeItemsPerPage: onChangeItemsPerPage,
        onLoadMore: loadMore,
        nextCursor: (0, _fp.getOr)(null, 'endCursor.value', pageInfo),
        tieBreaker: (0, _fp.getOr)(null, 'endCursor.tiebreaker', pageInfo),
        getUpdatedAt: getUpdatedAt,
        compact: isCompactFooter(width)
      }));
    }) : null);
  });
});
exports.Timeline = Timeline;
Timeline.displayName = 'Timeline';