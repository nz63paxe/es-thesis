"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LinkToPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _recompose = require("recompose");

var _redirect_to_hosts = require("./redirect_to_hosts");

var _redirect_to_network = require("./redirect_to_network");

var _redirect_to_overview = require("./redirect_to_overview");

var _redirect_to_timelines = require("./redirect_to_timelines");

var _model = require("../../store/hosts/model");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LinkToPage = (0, _recompose.pure)(function (_ref) {
  var match = _ref.match;
  return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(overview)"),
    component: _redirect_to_overview.RedirectToOverviewPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "".concat(match.url, "/:pageName(hosts)"),
    component: _redirect_to_hosts.RedirectToHostsPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(hosts)/:tabName(").concat(_model.HostsTableType.hosts, "|").concat(_model.HostsTableType.authentications, "|").concat(_model.HostsTableType.uncommonProcesses, "|").concat(_model.HostsTableType.anomalies, "|").concat(_model.HostsTableType.events, ")"),
    component: _redirect_to_hosts.RedirectToHostsPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(hosts)/:detailName/:tabName(").concat(_model.HostsTableType.authentications, "|").concat(_model.HostsTableType.uncommonProcesses, "|").concat(_model.HostsTableType.anomalies, "|").concat(_model.HostsTableType.events, ")"),
    component: _redirect_to_hosts.RedirectToHostDetailsPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(hosts)/:detailName"),
    component: _redirect_to_hosts.RedirectToHostDetailsPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    exact: true,
    path: "".concat(match.url, "/:pageName(network)"),
    component: _redirect_to_network.RedirectToNetworkPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(network)/ip/:detailName"),
    component: _redirect_to_network.RedirectToNetworkPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.url, "/:pageName(timelines)"),
    component: _redirect_to_timelines.RedirectToTimelinesPage
  }), _react.default.createElement(_reactRouterDom.Redirect, {
    to: "/"
  }));
});
exports.LinkToPage = LinkToPage;
LinkToPage.displayName = 'LinkToPage';