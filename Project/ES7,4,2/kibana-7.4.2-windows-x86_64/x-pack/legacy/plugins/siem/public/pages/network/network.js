"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Network = exports.getFlexDirection = void 0;

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _fp = require("lodash/fp");

var _react = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _reactSticky = require("react-sticky");

var _filters_global = require("../../components/filters_global");

var _header_page = require("../../components/header_page");

var _last_event_time = require("../../components/last_event_time");

var _manage_query = require("../../components/page/manage_query");

var _network = require("../../components/page/network");

var _network_dns_table = require("../../components/page/network/network_dns_table");

var _global_time = require("../../containers/global_time");

var _kpi_network = require("../../containers/kpi_network");

var _network_dns = require("../../containers/network_dns");

var _network_top_n_flow = require("../../containers/network_top_n_flow");

var _source = require("../../containers/source");

var _types = require("../../graphql/types");

var _store = require("../../store");

var _kql = require("./kql");

var _network_empty_page = require("./network_empty_page");

var i18n = _interopRequireWildcard(require("./translations"));

var _anomalies_network_table = require("../../components/ml/tables/anomalies_network_table");

var _score_interval_to_datetime = require("../../components/ml/score/score_interval_to_datetime");

var _actions = require("../../store/inputs/actions");

var _embedded_map = require("../../components/embeddables/embedded_map");

var _network2 = require("../../containers/network");

var _spy_routes = require("../../utils/route/spy_routes");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var NetworkTopNFlowTableManage = (0, _manage_query.manageQuery)(_network.NetworkTopNFlowTable);
var NetworkDnsTableManage = (0, _manage_query.manageQuery)(_network_dns_table.NetworkDnsTable);
var KpiNetworkComponentManage = (0, _manage_query.manageQuery)(_network.KpiNetworkComponent);
var mediaMatch = window.matchMedia('screen and (min-width: ' + _eui_theme_light.default.euiBreakpoints.xl + ')');

var getFlexDirectionByMediaMatch = function getFlexDirectionByMediaMatch() {
  var matches = mediaMatch.matches;
  return matches ? 'row' : 'column';
};

var getFlexDirection = function getFlexDirection() {
  var _useState = (0, _react.useState)(getFlexDirectionByMediaMatch()),
      _useState2 = _slicedToArray(_useState, 2),
      display = _useState2[0],
      setDisplay = _useState2[1];

  (0, _react.useEffect)(function () {
    var setFromEvent = function setFromEvent() {
      return setDisplay(getFlexDirectionByMediaMatch());
    };

    window.addEventListener('resize', setFromEvent);
    return function () {
      window.removeEventListener('resize', setFromEvent);
    };
  }, []);
  return display;
};

exports.getFlexDirection = getFlexDirection;

var NetworkComponent = _react.default.memo(function (_ref) {
  var filterQuery = _ref.filterQuery,
      queryExpression = _ref.queryExpression,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_global_time.GlobalTime, null, function (_ref3) {
      var to = _ref3.to,
          from = _ref3.from,
          setQuery = _ref3.setQuery,
          isInitializing = _ref3.isInitializing;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_filters_global.FiltersGlobal, null, _react.default.createElement(_kql.NetworkKql, {
        indexPattern: indexPattern,
        setQuery: setQuery,
        type: _store.networkModel.NetworkType.page
      })), _react.default.createElement(_header_page.HeaderPage, {
        subtitle: _react.default.createElement(_last_event_time.LastEventTime, {
          indexKey: _types.LastEventIndexKey.network
        }),
        title: i18n.PAGE_TITLE
      }), _react.default.createElement(_network2.NetworkFilter, {
        indexPattern: indexPattern,
        type: _store.networkModel.NetworkType.page
      }, function (_ref4) {
        var applyFilterQueryFromKueryExpression = _ref4.applyFilterQueryFromKueryExpression;
        return _react.default.createElement(_embedded_map.EmbeddedMap, {
          applyFilterQueryFromKueryExpression: applyFilterQueryFromKueryExpression,
          queryExpression: queryExpression,
          startDate: from,
          endDate: to,
          setQuery: setQuery
        });
      }), _react.default.createElement(_kpi_network.KpiNetworkQuery, {
        endDate: to,
        filterQuery: filterQuery,
        skip: isInitializing,
        sourceId: "default",
        startDate: from
      }, function (_ref5) {
        var kpiNetwork = _ref5.kpiNetwork,
            loading = _ref5.loading,
            id = _ref5.id,
            inspect = _ref5.inspect,
            refetch = _ref5.refetch;
        return _react.default.createElement(KpiNetworkComponentManage, {
          id: id,
          inspect: inspect,
          setQuery: setQuery,
          refetch: refetch,
          data: kpiNetwork,
          loading: loading,
          from: from,
          to: to,
          narrowDateRange: function narrowDateRange(min, max) {
            setAbsoluteRangeDatePicker({
              id: 'global',
              from: min,
              to: max
            });
          }
        });
      }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiFlexGroup, {
        direction: getFlexDirection()
      }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_network_top_n_flow.NetworkTopNFlowQuery, {
        endDate: to,
        flowTarget: _types.FlowTargetNew.source,
        filterQuery: filterQuery,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.page
      }, function (_ref6) {
        var id = _ref6.id,
            inspect = _ref6.inspect,
            isInspected = _ref6.isInspected,
            loading = _ref6.loading,
            loadPage = _ref6.loadPage,
            networkTopNFlow = _ref6.networkTopNFlow,
            pageInfo = _ref6.pageInfo,
            refetch = _ref6.refetch,
            totalCount = _ref6.totalCount;
        return _react.default.createElement(NetworkTopNFlowTableManage, {
          data: networkTopNFlow,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          flowTargeted: _types.FlowTargetNew.source,
          id: id,
          indexPattern: indexPattern,
          inspect: inspect,
          isInspect: isInspected,
          loading: loading,
          loadPage: loadPage,
          refetch: refetch,
          setQuery: setQuery,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.page
        });
      })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_network_top_n_flow.NetworkTopNFlowQuery, {
        endDate: to,
        flowTarget: _types.FlowTargetNew.destination,
        filterQuery: filterQuery,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.page
      }, function (_ref7) {
        var id = _ref7.id,
            inspect = _ref7.inspect,
            isInspected = _ref7.isInspected,
            loading = _ref7.loading,
            loadPage = _ref7.loadPage,
            networkTopNFlow = _ref7.networkTopNFlow,
            pageInfo = _ref7.pageInfo,
            refetch = _ref7.refetch,
            totalCount = _ref7.totalCount;
        return _react.default.createElement(NetworkTopNFlowTableManage, {
          data: networkTopNFlow,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          flowTargeted: _types.FlowTargetNew.destination,
          id: id,
          indexPattern: indexPattern,
          inspect: inspect,
          isInspect: isInspected,
          loading: loading,
          loadPage: loadPage,
          refetch: refetch,
          setQuery: setQuery,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.page
        });
      }))), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_network_dns.NetworkDnsQuery, {
        endDate: to,
        filterQuery: filterQuery,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.page
      }, function (_ref8) {
        var totalCount = _ref8.totalCount,
            loading = _ref8.loading,
            networkDns = _ref8.networkDns,
            pageInfo = _ref8.pageInfo,
            loadPage = _ref8.loadPage,
            id = _ref8.id,
            inspect = _ref8.inspect,
            isInspected = _ref8.isInspected,
            refetch = _ref8.refetch;
        return _react.default.createElement(NetworkDnsTableManage, {
          data: networkDns,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          id: id,
          inspect: inspect,
          isInspect: isInspected,
          loading: loading,
          loadPage: loadPage,
          refetch: refetch,
          setQuery: setQuery,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.page
        });
      }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_anomalies_network_table.AnomaliesNetworkTable, {
        startDate: from,
        endDate: to,
        skip: isInitializing,
        type: _store.networkModel.NetworkType.page,
        narrowDateRange: function narrowDateRange(score, interval) {
          var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
          setAbsoluteRangeDatePicker({
            id: 'global',
            from: fromTo.from,
            to: fromTo.to
          });
        }
      }));
    })) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_header_page.HeaderPage, {
      title: i18n.PAGE_TITLE
    }), _react.default.createElement(_network_empty_page.NetworkEmptyPage, null));
  }), _react.default.createElement(_spy_routes.SpyRoute, null));
});

NetworkComponent.displayName = 'NetworkComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getNetworkFilterQueryAsJson = _store.networkSelectors.networkFilterQueryAsJson();

  var getNetworkFilterExpression = _store.networkSelectors.networkFilterExpression();

  var mapStateToProps = function mapStateToProps(state) {
    return {
      filterQuery: getNetworkFilterQueryAsJson(state, _store.networkModel.NetworkType.page) || '',
      queryExpression: getNetworkFilterExpression(state, _store.networkModel.NetworkType.page) || ''
    };
  };

  return mapStateToProps;
};

var Network = (0, _reactRedux.connect)(makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
})(NetworkComponent);
exports.Network = Network;