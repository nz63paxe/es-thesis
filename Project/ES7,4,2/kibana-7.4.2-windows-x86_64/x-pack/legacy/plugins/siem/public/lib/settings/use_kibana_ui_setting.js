"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useKibanaUiSetting = void 0;

var _react = require("react");

var _new_platform = require("ui/new_platform");

var _timezone = require("ui/vis/lib/timezone");

var _constants = require("../../../common/constants");

var _use_observable = require("./use_observable");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore: path dynamic for kibana

/**
 * This hook behaves like a `useState` hook in that it provides a requested
 * setting value (with an optional default) from the Kibana UI settings (also
 * known as "advanced settings") and a setter to change that setting:
 *
 * ```
 * const [darkMode, setDarkMode] = useKibanaUiSetting('theme:darkMode');
 * ```
 *
 * This is not just a static consumption of the value, but will reactively
 * update when the underlying setting subscription of the `UiSettingsClient`
 * notifies of a change.
 *
 * Unlike the `useState`, it doesn't give type guarantees for the value,
 * because the underlying `UiSettingsClient` doesn't support that.
 */
var useKibanaUiSetting = function useKibanaUiSetting(key, defaultValue) {
  var uiSettingsClient = _new_platform.npSetup.core.uiSettings;
  var uiInjectedMetadata = _new_platform.npStart.core.injectedMetadata;

  if (key === _constants.DEFAULT_KBN_VERSION) {
    return [uiInjectedMetadata.getKibanaVersion()];
  }

  if (key === _constants.DEFAULT_TIMEZONE_BROWSER) {
    return [(0, _react.useMemo)(function () {
      return (0, _timezone.timezoneProvider)(uiSettingsClient)();
    }, [uiSettingsClient])];
  }

  var uiSetting$ = (0, _react.useMemo)(function () {
    return uiSettingsClient.get$(key, defaultValue);
  }, [uiSettingsClient]);
  var uiSetting = (0, _use_observable.useObservable)(uiSetting$);
  var setUiSetting = (0, _react.useCallback)(function (value) {
    return uiSettingsClient.set(key, value);
  }, [uiSettingsClient]);
  return [uiSetting, setUiSetting];
};

exports.useKibanaUiSetting = useKibanaUiSetting;