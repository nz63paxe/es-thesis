"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NetworkTopNFlowTable = exports.NetworkTopNFlowTableId = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _actions = require("../../../../store/actions");

var _types = require("../../../../graphql/types");

var _store = require("../../../../store");

var _paginated_table = require("../../../paginated_table");

var _columns = require("./columns");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  min-width: 180px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var rowItems = [{
  text: i18n.ROWS_5,
  numberOfRow: 5
}, {
  text: i18n.ROWS_10,
  numberOfRow: 10
}];
var NetworkTopNFlowTableId = 'networkTopSourceFlow-top-talkers';
exports.NetworkTopNFlowTableId = NetworkTopNFlowTableId;

var NetworkTopNFlowTableComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(NetworkTopNFlowTableComponent, _React$PureComponent);

  function NetworkTopNFlowTableComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, NetworkTopNFlowTableComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(NetworkTopNFlowTableComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onChange", function (criteria, tableType) {
      if (criteria.sort != null) {
        var splitField = criteria.sort.field.split('.');
        var field = (0, _fp.last)(splitField);
        var newSortDirection = field !== _this.props.topNFlowSort.field ? _types.Direction.desc : criteria.sort.direction; // sort by desc on init click

        var newTopNFlowSort = {
          field: field,
          direction: newSortDirection
        };

        if (!(0, _fp.isEqual)(newTopNFlowSort, _this.props.topNFlowSort)) {
          _this.props.updateTopNFlowSort({
            topNFlowSort: newTopNFlowSort,
            networkType: _this.props.type,
            tableType: tableType
          });
        }
      }
    });

    return _this;
  }

  _createClass(NetworkTopNFlowTableComponent, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          activePage = _this$props.activePage,
          data = _this$props.data,
          fakeTotalCount = _this$props.fakeTotalCount,
          flowTargeted = _this$props.flowTargeted,
          id = _this$props.id,
          indexPattern = _this$props.indexPattern,
          isInspect = _this$props.isInspect,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _loadPage = _this$props.loadPage,
          showMorePagesIndicator = _this$props.showMorePagesIndicator,
          topNFlowSort = _this$props.topNFlowSort,
          totalCount = _this$props.totalCount,
          type = _this$props.type,
          updateTopNFlowLimit = _this$props.updateTopNFlowLimit,
          updateTableActivePage = _this$props.updateTableActivePage;
      var tableType;
      var headerTitle;

      if (flowTargeted === _types.FlowTargetNew.source) {
        headerTitle = i18n.SOURCE_IP;
        tableType = _store.networkModel.NetworkTableType.topNFlowSource;
      } else {
        headerTitle = i18n.DESTINATION_IP;
        tableType = _store.networkModel.NetworkTableType.topNFlowDestination;
      }

      var field = topNFlowSort.field === _types.NetworkTopNFlowFields.bytes_out || topNFlowSort.field === _types.NetworkTopNFlowFields.bytes_in ? "node.network.".concat(topNFlowSort.field) : "node.".concat(flowTargeted, ".").concat(topNFlowSort.field);
      return _react.default.createElement(_paginated_table.PaginatedTable, {
        activePage: activePage,
        columns: (0, _columns.getNetworkTopNFlowColumns)(indexPattern, flowTargeted, type, NetworkTopNFlowTableId),
        headerCount: totalCount,
        headerTitle: headerTitle,
        headerUnit: i18n.UNIT(totalCount),
        id: id,
        isInspect: isInspect,
        itemsPerRow: rowItems,
        limit: limit,
        loading: loading,
        loadPage: function loadPage(newActivePage) {
          return _loadPage(newActivePage);
        },
        onChange: function onChange(criteria) {
          return _this2.onChange(criteria, tableType);
        },
        pageOfItems: data,
        showMorePagesIndicator: showMorePagesIndicator,
        sorting: {
          field: field,
          direction: topNFlowSort.direction
        },
        totalCount: fakeTotalCount,
        updateActivePage: function updateActivePage(newPage) {
          return updateTableActivePage({
            activePage: newPage,
            tableType: tableType
          });
        },
        updateLimitPagination: function updateLimitPagination(newLimit) {
          return updateTopNFlowLimit({
            limit: newLimit,
            networkType: type,
            tableType: tableType
          });
        }
      });
    }
  }]);

  return NetworkTopNFlowTableComponent;
}(_react.default.PureComponent);

var mapStateToProps = function mapStateToProps(state, ownProps) {
  return _store.networkSelectors.topNFlowSelector(ownProps.flowTargeted);
};

var NetworkTopNFlowTable = (0, _reactRedux.connect)(mapStateToProps, {
  updateTopNFlowLimit: _actions.networkActions.updateTopNFlowLimit,
  updateTopNFlowSort: _actions.networkActions.updateTopNFlowSort,
  updateTableActivePage: _actions.networkActions.updateNetworkPageTableActivePage
})(NetworkTopNFlowTableComponent);
exports.NetworkTopNFlowTable = NetworkTopNFlowTable;
var SelectTypeItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
SelectTypeItem.displayName = 'SelectTypeItem';