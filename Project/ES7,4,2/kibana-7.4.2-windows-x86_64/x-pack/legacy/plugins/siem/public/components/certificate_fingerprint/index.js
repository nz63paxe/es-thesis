"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CertificateFingerprint = exports.TLS_SERVER_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = exports.TLS_CLIENT_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _draggables = require("../draggables");

var _external_link_icon = require("../external_link_icon");

var _links = require("../links");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TLS_CLIENT_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = 'tls.client_certificate.fingerprint.sha1';
exports.TLS_CLIENT_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = TLS_CLIENT_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME;
var TLS_SERVER_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = 'tls.server_certificate.fingerprint.sha1';
exports.TLS_SERVER_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME = TLS_SERVER_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME;

var FingerprintLabel = _styledComponents.default.span(_templateObject());

FingerprintLabel.displayName = 'FingerprintLabel';
/**
 * Represents a field containing a certificate fingerprint (e.g. a sha1), with
 * a link to an external site, which in-turn compares the fingerprint against a
 * set of known fingerprints
 * Examples:
 * 'tls.client_certificate.fingerprint.sha1'
 * 'tls.server_certificate.fingerprint.sha1'
 */

var CertificateFingerprint = (0, _recompose.pure)(function (_ref) {
  var eventId = _ref.eventId,
      certificateType = _ref.certificateType,
      contextId = _ref.contextId,
      fieldName = _ref.fieldName,
      value = _ref.value;
  return React.createElement(_draggables.DraggableBadge, {
    contextId: contextId,
    "data-test-subj": "".concat(certificateType, "-certificate-fingerprint"),
    eventId: eventId,
    field: fieldName,
    iconType: "snowflake",
    tooltipContent: React.createElement(_eui.EuiText, {
      size: "xs"
    }, React.createElement("span", null, fieldName)),
    value: value
  }, React.createElement(FingerprintLabel, {
    "data-test-subj": "fingerprint-label"
  }, certificateType === 'client' ? i18n.CLIENT_CERT : i18n.SERVER_CERT), React.createElement(_links.CertificateFingerprintLink, {
    certificateFingerprint: value || ''
  }), React.createElement(_external_link_icon.ExternalLinkIcon, null));
});
exports.CertificateFingerprint = CertificateFingerprint;
CertificateFingerprint.displayName = 'CertificateFingerprint';