"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Pane = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _resize_handle = require("../../resize_handle");

var _styled_handles = require("../../resize_handle/styled_handles");

var _header = require("../header");

var i18n = _interopRequireWildcard(require("./translations"));

var _actions = require("../../../store/actions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .timeline-flyout {\n    min-width: 150px;\n    width: ", ";\n  }\n  .timeline-flyout-header {\n    align-items: center;\n    box-shadow: none;\n    display: flex;\n    flex-direction: row;\n    height: ", ";\n    max-height: ", ";\n    overflow: hidden;\n    padding: 5px 0 0 10px;\n  }\n  .timeline-flyout-body {\n    overflow-y: hidden;\n    padding: 0;\n    .euiFlyoutBody__overflow {\n      padding: 0;\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var minWidthPixels = 550; // do not allow the flyout to shrink below this width (pixels)

var maxWidthPercent = 95; // do not allow the flyout to grow past this percentage of the view

var EuiFlyoutContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var width = _ref.width;
  return "".concat(width, "px");
}, function (_ref2) {
  var headerHeight = _ref2.headerHeight;
  return "".concat(headerHeight, "px");
}, function (_ref3) {
  var headerHeight = _ref3.headerHeight;
  return "".concat(headerHeight, "px");
});

EuiFlyoutContainer.displayName = 'EuiFlyoutContainer';

var FlyoutHeaderContainer = _styledComponents.default.div(_templateObject2());

FlyoutHeaderContainer.displayName = 'FlyoutHeaderContainer'; // manually wrap the close button because EuiButtonIcon can't be a wrapped `styled`

var WrappedCloseButton = _styledComponents.default.div(_templateObject3());

WrappedCloseButton.displayName = 'WrappedCloseButton';
var FlyoutHeaderWithCloseButton = React.memo(function (_ref4) {
  var onClose = _ref4.onClose,
      timelineId = _ref4.timelineId,
      usersViewing = _ref4.usersViewing;
  return React.createElement(FlyoutHeaderContainer, null, React.createElement(WrappedCloseButton, null, React.createElement(_eui.EuiToolTip, {
    content: i18n.CLOSE_TIMELINE
  }, React.createElement(_eui.EuiButtonIcon, {
    "aria-label": i18n.CLOSE_TIMELINE,
    "data-test-subj": "close-timeline",
    iconType: "cross",
    onClick: onClose
  }))), React.createElement(_header.FlyoutHeader, {
    timelineId: timelineId,
    usersViewing: usersViewing
  }));
}, function (prevProps, nextProps) {
  return prevProps.timelineId === nextProps.timelineId && prevProps.usersViewing === nextProps.usersViewing;
});
FlyoutHeaderWithCloseButton.displayName = 'FlyoutHeaderWithCloseButton';

var FlyoutPaneComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(FlyoutPaneComponent, _React$PureComponent);

  function FlyoutPaneComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FlyoutPaneComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FlyoutPaneComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "renderFlyout", function () {
      return React.createElement(React.Fragment, null);
    });

    _defineProperty(_assertThisInitialized(_this), "onResize", function (_ref5) {
      var delta = _ref5.delta,
          id = _ref5.id;
      var applyDeltaToWidth = _this.props.applyDeltaToWidth;
      var bodyClientWidthPixels = document.body.clientWidth;
      applyDeltaToWidth({
        bodyClientWidthPixels: bodyClientWidthPixels,
        delta: delta,
        id: id,
        maxWidthPercent: maxWidthPercent,
        minWidthPixels: minWidthPixels
      });
    });

    return _this;
  }

  _createClass(FlyoutPaneComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          children = _this$props.children,
          flyoutHeight = _this$props.flyoutHeight,
          headerHeight = _this$props.headerHeight,
          onClose = _this$props.onClose,
          timelineId = _this$props.timelineId,
          usersViewing = _this$props.usersViewing,
          width = _this$props.width;
      return React.createElement(EuiFlyoutContainer, {
        headerHeight: headerHeight,
        "data-test-subj": "flyout-pane",
        width: width
      }, React.createElement(_eui.EuiFlyout, {
        "aria-label": i18n.TIMELINE_DESCRIPTION,
        className: "timeline-flyout",
        "data-test-subj": "eui-flyout",
        hideCloseButton: true,
        maxWidth: "".concat(maxWidthPercent, "%"),
        onClose: onClose,
        size: "l"
      }, React.createElement(_resize_handle.Resizeable, {
        handle: React.createElement(_styled_handles.TimelineResizeHandle, {
          "data-test-subj": "flyout-resize-handle",
          height: flyoutHeight
        }),
        id: timelineId,
        onResize: this.onResize,
        render: this.renderFlyout
      }), React.createElement(_eui.EuiFlyoutHeader, {
        className: "timeline-flyout-header",
        "data-test-subj": "eui-flyout-header",
        hasBorder: false
      }, React.createElement(FlyoutHeaderWithCloseButton, {
        onClose: onClose,
        timelineId: timelineId,
        usersViewing: usersViewing
      })), React.createElement(_eui.EuiFlyoutBody, {
        "data-test-subj": "eui-flyout-body",
        className: "timeline-flyout-body"
      }, children)));
    }
  }]);

  return FlyoutPaneComponent;
}(React.PureComponent);

var Pane = (0, _reactRedux.connect)(null, {
  applyDeltaToWidth: _actions.timelineActions.applyDeltaToWidth
})(FlyoutPaneComponent);
exports.Pane = Pane;