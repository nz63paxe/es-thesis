"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "HostDetailsBody", {
  enumerable: true,
  get: function get() {
    return _body.HostDetailsBody;
  }
});
exports.HostDetails = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireWildcard(require("react"));

var _redux = require("redux");

var _reactRedux = require("react-redux");

var _reactSticky = require("react-sticky");

var _filters_global = require("../../../components/filters_global");

var _header_page = require("../../../components/header_page");

var _last_event_time = require("../../../components/last_event_time");

var _overview = require("../../../containers/hosts/overview");

var _source = require("../../../containers/source");

var _types = require("../../../graphql/types");

var _hosts_empty_page = require("../hosts_empty_page");

var _kql = require("../kql");

var _actions = require("../../../store/inputs/actions");

var _score_interval_to_datetime = require("../../../components/ml/score/score_interval_to_datetime");

var _kpi_host_details = require("../../../containers/kpi_host_details");

var _host_to_criteria = require("../../../components/ml/criteria/host_to_criteria");

var _hosts_navigations = require("../hosts_navigations");

var _navigation = require("../../../components/navigation");

var _spy_routes = require("../../../utils/route/spy_routes");

var _anomaly_table_provider = require("../../../components/ml/anomaly/anomaly_table_provider");

var _manage_query = require("../../../components/page/manage_query");

var _host_overview = require("../../../components/page/hosts/host_overview");

var _hosts = require("../../../components/page/hosts");

var _utils = require("./utils");

var _ml_capabilities_provider = require("../../../components/ml/permissions/ml_capabilities_provider");

var _has_ml_user_permissions = require("../../../components/ml/permissions/has_ml_user_permissions");

var _body = require("./body");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HostOverviewManage = (0, _manage_query.manageQuery)(_host_overview.HostOverview);
var KpiHostDetailsManage = (0, _manage_query.manageQuery)(_hosts.KpiHostsComponent);

var HostDetailsComponent = _react.default.memo(function (_ref) {
  var isInitializing = _ref.isInitializing,
      filterQueryExpression = _ref.filterQueryExpression,
      from = _ref.from,
      detailName = _ref.detailName,
      setQuery = _ref.setQuery,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker,
      to = _ref.to;
  var capabilities = (0, _react.useContext)(_ml_capabilities_provider.MlCapabilitiesContext);
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_source.WithSource, {
    sourceId: "default"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_filters_global.FiltersGlobal, null, _react.default.createElement(_kql.HostsKql, {
      indexPattern: indexPattern,
      setQuery: setQuery,
      type: _utils.type
    })), _react.default.createElement(_header_page.HeaderPage, {
      subtitle: _react.default.createElement(_last_event_time.LastEventTime, {
        indexKey: _types.LastEventIndexKey.hostDetails,
        hostName: detailName
      }),
      title: detailName
    }), _react.default.createElement(_overview.HostOverviewByNameQuery, {
      sourceId: "default",
      hostName: detailName,
      skip: isInitializing,
      startDate: from,
      endDate: to
    }, function (_ref3) {
      var hostOverview = _ref3.hostOverview,
          loading = _ref3.loading,
          id = _ref3.id,
          inspect = _ref3.inspect,
          refetch = _ref3.refetch;
      return _react.default.createElement(_anomaly_table_provider.AnomalyTableProvider, {
        criteriaFields: (0, _host_to_criteria.hostToCriteria)(hostOverview),
        startDate: from,
        endDate: to,
        skip: isInitializing
      }, function (_ref4) {
        var isLoadingAnomaliesData = _ref4.isLoadingAnomaliesData,
            anomaliesData = _ref4.anomaliesData;
        return _react.default.createElement(HostOverviewManage, {
          id: id,
          inspect: inspect,
          refetch: refetch,
          setQuery: setQuery,
          data: hostOverview,
          anomaliesData: anomaliesData,
          isLoadingAnomaliesData: isLoadingAnomaliesData,
          loading: loading,
          startDate: from,
          endDate: to,
          narrowDateRange: function narrowDateRange(score, interval) {
            var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
            setAbsoluteRangeDatePicker({
              id: 'global',
              from: fromTo.from,
              to: fromTo.to
            });
          }
        });
      });
    }), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_kpi_host_details.KpiHostDetailsQuery, {
      sourceId: "default",
      filterQuery: (0, _utils.getFilterQuery)(detailName, filterQueryExpression, indexPattern),
      skip: isInitializing,
      startDate: from,
      endDate: to
    }, function (_ref5) {
      var kpiHostDetails = _ref5.kpiHostDetails,
          id = _ref5.id,
          inspect = _ref5.inspect,
          loading = _ref5.loading,
          refetch = _ref5.refetch;
      return _react.default.createElement(KpiHostDetailsManage, {
        data: kpiHostDetails,
        from: from,
        id: id,
        inspect: inspect,
        loading: loading,
        refetch: refetch,
        setQuery: setQuery,
        to: to,
        narrowDateRange: function narrowDateRange(min, max) {
          setAbsoluteRangeDatePicker({
            id: 'global',
            from: min,
            to: max
          });
        }
      });
    }), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_navigation.SiemNavigation, {
      navTabs: (0, _hosts_navigations.navTabsHostDetails)(detailName, (0, _has_ml_user_permissions.hasMlUserPermissions)(capabilities)),
      display: "default",
      showBorder: true
    }), _react.default.createElement(_eui.EuiSpacer, null)) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_header_page.HeaderPage, {
      title: detailName
    }), _react.default.createElement(_hosts_empty_page.HostsEmptyPage, null));
  }), _react.default.createElement(_spy_routes.SpyRoute, null));
});

HostDetailsComponent.displayName = 'HostDetailsComponent';
var HostDetails = (0, _redux.compose)((0, _reactRedux.connect)(_utils.makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
}))(HostDetailsComponent);
exports.HostDetails = HostDetails;