"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MlPopover = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _moment = _interopRequireDefault(require("moment"));

var _use_job_summary_data = require("./hooks/use_job_summary_data");

var i18n = _interopRequireWildcard(require("./translations"));

var _has_ml_admin_permissions = require("../ml/permissions/has_ml_admin_permissions");

var _ml_capabilities_provider = require("../ml/permissions/ml_capabilities_provider");

var _jobs_table = require("./jobs_table/jobs_table");

var _api = require("./api");

var _use_index_patterns = require("./hooks/use_index_patterns");

var _upgrade_contents = require("./upgrade_contents");

var _filter_group = require("./jobs_table/filter_group");

var _showing_count = require("./jobs_table/showing_count");

var _popover_description = require("./popover_description");

var _helpers = require("./helpers");

var _config_templates = require("./config_templates");

var _toasters = require("../toasters");

var _error_to_toaster = require("../ml/api/error_to_toaster");

var _use_kibana_ui_setting = require("../../lib/settings/use_kibana_ui_setting");

var _constants = require("../../../common/constants");

var _track_usage = require("../../lib/track_usage");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  max-width: 550px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var PopoverContentsDiv = _styledComponents.default.div(_templateObject());

PopoverContentsDiv.displayName = 'PopoverContentsDiv';

function mlPopoverReducer(state, action) {
  switch (action.type) {
    case 'refresh':
      {
        return _objectSpread({}, state, {
          refreshToggle: !state.refreshToggle
        });
      }

    case 'loading':
      {
        return _objectSpread({}, state, {
          isLoading: true
        });
      }

    case 'success':
      {
        return _objectSpread({}, state, {
          isLoading: false,
          jobs: action.results
        });
      }

    case 'failure':
      {
        return _objectSpread({}, state, {
          isLoading: false,
          jobs: []
        });
      }

    default:
      return state;
  }
}

var initialState = {
  isLoading: false,
  jobs: [],
  refreshToggle: true
};

var MlPopover = _react.default.memo(function () {
  var _useReducer = (0, _react.useReducer)(mlPopoverReducer, initialState),
      _useReducer2 = _slicedToArray(_useReducer, 2),
      refreshToggle = _useReducer2[0].refreshToggle,
      dispatch = _useReducer2[1];

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isPopoverOpen = _useState2[0],
      setIsPopoverOpen = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showCustomJobs = _useState4[0],
      setShowCustomJobs = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      showElasticJobs = _useState6[0],
      setShowElasticJobs = _useState6[1];

  var _useJobSummaryData = (0, _use_job_summary_data.useJobSummaryData)([], refreshToggle),
      _useJobSummaryData2 = _slicedToArray(_useJobSummaryData, 2),
      isLoadingJobSummaryData = _useJobSummaryData2[0],
      jobSummaryData = _useJobSummaryData2[1];

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      isCreatingJobs = _useState8[0],
      setIsCreatingJobs = _useState8[1];

  var _useState9 = (0, _react.useState)(''),
      _useState10 = _slicedToArray(_useState9, 2),
      filterQuery = _useState10[0],
      setFilterQuery = _useState10[1];

  var _useStateToaster = (0, _toasters.useStateToaster)(),
      _useStateToaster2 = _slicedToArray(_useStateToaster, 2),
      dispatchToaster = _useStateToaster2[1];

  var _useIndexPatterns = (0, _use_index_patterns.useIndexPatterns)(refreshToggle),
      _useIndexPatterns2 = _slicedToArray(_useIndexPatterns, 2),
      configuredIndexPatterns = _useIndexPatterns2[1];

  var capabilities = (0, _react.useContext)(_ml_capabilities_provider.MlCapabilitiesContext);

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)(_constants.DEFAULT_KBN_VERSION),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      kbnVersion = _useKibanaUiSetting2[0];

  var headers = {
    'kbn-version': kbnVersion
  };
  var configuredIndexPatternTitles = (0, _helpers.getIndexPatternTitles)(configuredIndexPatterns); // Enable/Disable Job & Datafeed -- passed to JobsTable for use as callback on JobSwitch

  var enableDatafeed =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(jobName, latestTimestampMs, enable) {
      var maxStartTime, startTime;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              submitTelemetry(jobName, enable, embeddedJobIds); // Max start time for job is no more than two weeks ago to ensure job performance

              maxStartTime = _moment.default.utc().subtract(14, 'days').valueOf();

              if (!enable) {
                _context.next = 15;
                break;
              }

              startTime = Math.max(latestTimestampMs, maxStartTime);
              _context.prev = 4;
              _context.next = 7;
              return (0, _api.startDatafeeds)(["datafeed-".concat(jobName)], headers, startTime);

            case 7:
              _context.next = 13;
              break;

            case 9:
              _context.prev = 9;
              _context.t0 = _context["catch"](4);
              (0, _track_usage.trackUiAction)(_track_usage.METRIC_TYPE.COUNT, _track_usage.TELEMETRY_EVENT.JOB_ENABLE_FAILURE);
              (0, _error_to_toaster.errorToToaster)({
                title: i18n.START_JOB_FAILURE,
                error: _context.t0,
                dispatchToaster: dispatchToaster
              });

            case 13:
              _context.next = 24;
              break;

            case 15:
              _context.prev = 15;
              _context.next = 18;
              return (0, _api.stopDatafeeds)(["datafeed-".concat(jobName)], headers);

            case 18:
              _context.next = 24;
              break;

            case 20:
              _context.prev = 20;
              _context.t1 = _context["catch"](15);
              (0, _track_usage.trackUiAction)(_track_usage.METRIC_TYPE.COUNT, _track_usage.TELEMETRY_EVENT.JOB_DISABLE_FAILURE);
              (0, _error_to_toaster.errorToToaster)({
                title: i18n.STOP_JOB_FAILURE,
                error: _context.t1,
                dispatchToaster: dispatchToaster
              });

            case 24:
              dispatch({
                type: 'refresh'
              });

            case 25:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[4, 9], [15, 20]]);
    }));

    return function enableDatafeed(_x, _x2, _x3) {
      return _ref.apply(this, arguments);
    };
  }(); // All jobs from embedded configTemplates that should be installed


  var embeddedJobIds = (0, _helpers.getJobsToInstall)(_config_templates.configTemplates); // Jobs currently installed retrieved via ml jobs_summary api for 'siem' group

  var siemGroupJobIds = jobSummaryData != null ? jobSummaryData.map(function (job) {
    return job.id;
  }) : [];
  var installedJobIds = embeddedJobIds.filter(function (job) {
    return siemGroupJobIds.includes(job);
  }); // Config templates that still need to be installed and have a defaultIndexPattern that is configured

  var configTemplatesToInstall = (0, _helpers.getConfigTemplatesToInstall)(_config_templates.configTemplates, installedJobIds, configuredIndexPatternTitles || []); // Filter installed job to show all 'siem' group jobs or just embedded

  var jobsToDisplay = (0, _helpers.getJobsToDisplay)(jobSummaryData, embeddedJobIds, showCustomJobs, showElasticJobs, filterQuery); // Install Config Templates as effect of opening popover

  (0, _react.useEffect)(function () {
    if (isPopoverOpen && jobSummaryData != null && configuredIndexPatternTitles.length > 0 && configTemplatesToInstall.length > 0) {
      var setupJobs =
      /*#__PURE__*/
      function () {
        var _ref2 = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee2() {
          return regeneratorRuntime.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  setIsCreatingJobs(true);
                  _context2.prev = 1;
                  _context2.next = 4;
                  return Promise.all(configTemplatesToInstall.map(function (configTemplate) {
                    return (0, _api.setupMlJob)({
                      configTemplate: configTemplate.name,
                      indexPatternName: configTemplate.defaultIndexPattern,
                      groups: ['siem'],
                      headers: headers
                    });
                  }));

                case 4:
                  setIsCreatingJobs(false);
                  dispatch({
                    type: 'refresh'
                  });
                  _context2.next = 12;
                  break;

                case 8:
                  _context2.prev = 8;
                  _context2.t0 = _context2["catch"](1);
                  (0, _error_to_toaster.errorToToaster)({
                    title: i18n.CREATE_JOB_FAILURE,
                    error: _context2.t0,
                    dispatchToaster: dispatchToaster
                  });
                  setIsCreatingJobs(false);

                case 12:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, null, [[1, 8]]);
        }));

        return function setupJobs() {
          return _ref2.apply(this, arguments);
        };
      }();

      setupJobs();
    }
  }, [jobSummaryData, (0, _helpers.getStablePatternTitles)(configuredIndexPatternTitles)]);

  if (!capabilities.isPlatinumOrTrialLicense) {
    // If the user does not have platinum show upgrade UI
    return _react.default.createElement(_eui.EuiPopover, {
      anchorPosition: "downRight",
      id: "integrations-popover",
      button: _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": "integrations-button",
        iconType: "arrowDown",
        iconSide: "right",
        onClick: function onClick() {
          return setIsPopoverOpen(!isPopoverOpen);
        }
      }, i18n.ANOMALY_DETECTION),
      isOpen: isPopoverOpen,
      closePopover: function closePopover() {
        return setIsPopoverOpen(!isPopoverOpen);
      }
    }, _react.default.createElement(_upgrade_contents.UpgradeContents, null));
  } else if ((0, _has_ml_admin_permissions.hasMlAdminPermissions)(capabilities)) {
    // If the user has Platinum License & ML Admin Permissions, show Anomaly Detection button & full config UI
    return _react.default.createElement(_eui.EuiPopover, {
      anchorPosition: "downRight",
      id: "integrations-popover",
      button: _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": "integrations-button",
        iconType: "arrowDown",
        iconSide: "right",
        onClick: function onClick() {
          setIsPopoverOpen(!isPopoverOpen);
          dispatch({
            type: 'refresh'
          });
        }
      }, i18n.ANOMALY_DETECTION),
      isOpen: isPopoverOpen,
      closePopover: function closePopover() {
        return setIsPopoverOpen(!isPopoverOpen);
      }
    }, _react.default.createElement(PopoverContentsDiv, {
      "data-test-subj": "ml-popover-contents"
    }, _react.default.createElement(_eui.EuiPopoverTitle, null, i18n.ANOMALY_DETECTION_TITLE), _react.default.createElement(_popover_description.PopoverDescription, null), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_filter_group.FilterGroup, {
      showCustomJobs: showCustomJobs,
      setShowCustomJobs: setShowCustomJobs,
      showElasticJobs: showElasticJobs,
      setShowElasticJobs: setShowElasticJobs,
      setFilterQuery: setFilterQuery
    }), _react.default.createElement(_showing_count.ShowingCount, {
      filterResultsLength: jobsToDisplay.length
    }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_jobs_table.JobsTable, {
      isLoading: isCreatingJobs || isLoadingJobSummaryData,
      jobs: jobsToDisplay,
      onJobStateChange: enableDatafeed
    })));
  } else {
    // If the user has Platinum License & not ML Admin, hide Anomaly Detection button as they don't have permissions to configure
    return null;
  }
});

exports.MlPopover = MlPopover;

var submitTelemetry = function submitTelemetry(jobName, enabled, embeddedJobIds) {
  // Report type of job enabled/disabled
  (0, _track_usage.trackUiAction)(_track_usage.METRIC_TYPE.COUNT, embeddedJobIds.includes(jobName) ? enabled ? _track_usage.TELEMETRY_EVENT.SIEM_JOB_ENABLED : _track_usage.TELEMETRY_EVENT.SIEM_JOB_DISABLED : enabled ? _track_usage.TELEMETRY_EVENT.CUSTOM_JOB_ENABLED : _track_usage.TELEMETRY_EVENT.CUSTOM_JOB_DISABLED);
};

MlPopover.displayName = 'MlPopover';