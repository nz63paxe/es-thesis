"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FlowTargetSelectConnected = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _types = require("../../../../graphql/types");

var _network = require("../../../../store/network");

var i18nIp = _interopRequireWildcard(require("../ip_overview/translations"));

var _flow_target_select = require("../../../flow_controls/flow_target_select");

var _field_renderers = require("../../../field_renderers/field_renderers");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  min-width: 180px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SelectTypeItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
SelectTypeItem.displayName = 'SelectTypeItem';
var FlowTargetSelectComponent = (0, _recompose.pure)(function (_ref) {
  var flowTarget = _ref.flowTarget,
      updateIpDetailsFlowTarget = _ref.updateIpDetailsFlowTarget;
  return _react.default.createElement(SelectTypeItem, {
    grow: false,
    "data-test-subj": "".concat(_field_renderers.IpOverviewId, "-select-flow-target")
  }, _react.default.createElement(_flow_target_select.FlowTargetSelect, {
    id: _field_renderers.IpOverviewId,
    isLoading: !flowTarget,
    selectedDirection: _types.FlowDirection.uniDirectional,
    selectedTarget: flowTarget,
    displayTextOverride: [i18nIp.AS_SOURCE, i18nIp.AS_DESTINATION],
    updateFlowTargetAction: updateIpDetailsFlowTarget
  }));
});
FlowTargetSelectComponent.displayName = 'FlowTargetSelectComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getIpDetailsFlowTargetSelector = _network.networkSelectors.ipDetailsFlowTargetSelector();

  return function (state) {
    return {
      flowTarget: getIpDetailsFlowTargetSelector(state)
    };
  };
};

var FlowTargetSelectConnected = (0, _reactRedux.connect)(makeMapStateToProps, {
  updateIpDetailsFlowTarget: _network.networkActions.updateIpDetailsFlowTarget
})(FlowTargetSelectComponent);
exports.FlowTargetSelectConnected = FlowTargetSelectConnected;