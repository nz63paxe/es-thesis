"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsTable = void 0;

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../../../store/actions");

var _types = require("../../../../graphql/types");

var _helpers = require("../../../../lib/helpers");

var _store = require("../../../../store");

var _paginated_table = require("../../../paginated_table");

var _columns = require("./columns");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var tableType = _store.hostsModel.HostsTableType.hosts;
var rowItems = [{
  text: i18n.ROWS_5,
  numberOfRow: 5
}, {
  text: i18n.ROWS_10,
  numberOfRow: 10
}];

var HostsTableComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(HostsTableComponent, _React$PureComponent);

  function HostsTableComponent(props) {
    var _this;

    _classCallCheck(this, HostsTableComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(HostsTableComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "memoizedColumns", void 0);

    _defineProperty(_assertThisInitialized(_this), "memoizedSorting", void 0);

    _defineProperty(_assertThisInitialized(_this), "getSorting", function (trigger, sortField, direction) {
      return {
        field: getNodeField(sortField),
        direction: direction
      };
    });

    _defineProperty(_assertThisInitialized(_this), "getMemoizeHostsColumns", function (type, indexPattern) {
      return (0, _columns.getHostsColumns)(type, indexPattern);
    });

    _defineProperty(_assertThisInitialized(_this), "onChange", function (criteria) {
      if (criteria.sort != null) {
        var sort = {
          field: getSortField(criteria.sort.field),
          direction: criteria.sort.direction
        };

        if (sort.direction !== _this.props.direction || sort.field !== _this.props.sortField) {
          _this.props.updateHostsSort({
            sort: sort,
            hostsType: _this.props.type
          });
        }
      }
    });

    _this.memoizedColumns = (0, _memoizeOne.default)(_this.getMemoizeHostsColumns);
    _this.memoizedSorting = (0, _memoizeOne.default)(_this.getSorting);
    return _this;
  }

  _createClass(HostsTableComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          activePage = _this$props.activePage,
          data = _this$props.data,
          direction = _this$props.direction,
          fakeTotalCount = _this$props.fakeTotalCount,
          id = _this$props.id,
          isInspect = _this$props.isInspect,
          indexPattern = _this$props.indexPattern,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _loadPage = _this$props.loadPage,
          showMorePagesIndicator = _this$props.showMorePagesIndicator,
          totalCount = _this$props.totalCount,
          sortField = _this$props.sortField,
          type = _this$props.type,
          updateTableActivePage = _this$props.updateTableActivePage,
          updateTableLimit = _this$props.updateTableLimit;
      return _react.default.createElement(_paginated_table.PaginatedTable, {
        activePage: activePage,
        columns: this.memoizedColumns(type, indexPattern),
        dataTestSubj: "all-hosts",
        headerCount: totalCount,
        headerTitle: i18n.HOSTS,
        headerUnit: i18n.UNIT(totalCount),
        id: id,
        isInspect: isInspect,
        itemsPerRow: rowItems,
        limit: limit,
        loading: loading,
        loadPage: function loadPage(newActivePage) {
          return _loadPage(newActivePage);
        },
        onChange: this.onChange,
        pageOfItems: data,
        showMorePagesIndicator: showMorePagesIndicator,
        sorting: this.memoizedSorting("".concat(sortField, "-").concat(direction), sortField, direction),
        totalCount: fakeTotalCount,
        updateLimitPagination: function updateLimitPagination(newLimit) {
          return updateTableLimit({
            hostsType: type,
            limit: newLimit,
            tableType: tableType
          });
        },
        updateActivePage: function updateActivePage(newPage) {
          return updateTableActivePage({
            activePage: newPage,
            hostsType: type,
            tableType: tableType
          });
        }
      });
    }
  }]);

  return HostsTableComponent;
}(_react.default.PureComponent);

var getSortField = function getSortField(field) {
  switch (field) {
    case 'node.host.name':
      return _types.HostsFields.hostName;

    case 'node.lastSeen':
      return _types.HostsFields.lastSeen;

    default:
      return _types.HostsFields.lastSeen;
  }
};

var getNodeField = function getNodeField(field) {
  switch (field) {
    case _types.HostsFields.hostName:
      return 'node.host.name';

    case _types.HostsFields.lastSeen:
      return 'node.lastSeen';
  }

  (0, _helpers.assertUnreachable)(field);
};

var makeMapStateToProps = function makeMapStateToProps() {
  var getHostsSelector = _store.hostsSelectors.hostsSelector();

  var mapStateToProps = function mapStateToProps(state, _ref) {
    var type = _ref.type;
    return getHostsSelector(state, type);
  };

  return mapStateToProps;
};

var HostsTable = (0, _reactRedux.connect)(makeMapStateToProps, {
  updateHostsSort: _actions.hostsActions.updateHostsSort,
  updateTableActivePage: _actions.hostsActions.updateTableActivePage,
  updateTableLimit: _actions.hostsActions.updateTableLimit
})(HostsTableComponent);
exports.HostsTable = HostsTable;