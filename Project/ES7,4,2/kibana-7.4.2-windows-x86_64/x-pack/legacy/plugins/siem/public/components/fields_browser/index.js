"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulFieldsBrowser = exports.StatefulFieldsBrowserComponent = void 0;

var _reactRedux = require("react-redux");

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _default_headers = require("../timeline/body/column_headers/default_headers");

var _field_browser = require("./field_browser");

var _helpers = require("./helpers");

var i18n = _interopRequireWildcard(require("./translations"));

var _actions = require("../../store/actions");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  ", "\n\n  .", " {\n    border-color: ", ";\n    color: ", ";\n    font-size: 14px;\n    margin: 1px 5px 2px 0;\n    ", "\n    ", "\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var fieldsButtonClassName = 'fields-button';
/** wait this many ms after the user completes typing before applying the filter input */

var INPUT_TIMEOUT = 250;

var FieldsBrowserButtonContainer = _styledComponents.default.div(_templateObject(), function (_ref) {
  var show = _ref.show;
  return show ? 'position: absolute;' : '';
}, fieldsButtonClassName, function (_ref2) {
  var theme = _ref2.theme;
  return theme.eui.euiColorLightShade;
}, function (_ref3) {
  var theme = _ref3.theme;
  return theme.eui.euiColorDarkestShade;
}, function (_ref4) {
  var show = _ref4.show;
  return show ? 'position: absolute;' : '';
}, function (_ref5) {
  var show = _ref5.show;
  return show ? 'top: -15px;' : '';
});

FieldsBrowserButtonContainer.displayName = 'FieldsBrowserButtonContainer';

/**
 * Manages the state of the field browser
 */
var StatefulFieldsBrowserComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(StatefulFieldsBrowserComponent, _React$PureComponent);

  /** tracks the latest timeout id from `setTimeout`*/
  function StatefulFieldsBrowserComponent(props) {
    var _this;

    _classCallCheck(this, StatefulFieldsBrowserComponent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(StatefulFieldsBrowserComponent).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "inputTimeoutId", 0);

    _defineProperty(_assertThisInitialized(_this), "toggleShow", function () {
      _this.setState(function (_ref6) {
        var show = _ref6.show;
        return {
          show: !show
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "updateFilter", function (filterInput) {
      _this.setState({
        filterInput: filterInput,
        isSearching: true
      });

      if (_this.inputTimeoutId !== 0) {
        clearTimeout(_this.inputTimeoutId); // ⚠️ mutation: cancel any previous timers
      } // ⚠️ mutation: schedule a new timer that will apply the filter when it fires:


      _this.inputTimeoutId = window.setTimeout(function () {
        var filteredBrowserFields = (0, _helpers.filterBrowserFieldsByFieldName)({
          browserFields: (0, _helpers.mergeBrowserFieldsWithDefaultCategory)(_this.props.browserFields),
          substring: _this.state.filterInput
        });

        _this.setState(function (currentState) {
          return {
            filteredBrowserFields: filteredBrowserFields,
            isSearching: false,
            selectedCategoryId: currentState.filterInput === '' || Object.keys(filteredBrowserFields).length === 0 ? _default_headers.DEFAULT_CATEGORY_NAME : Object.keys(filteredBrowserFields).sort().reduce(function (selected, category) {
              return filteredBrowserFields[category].fields != null && filteredBrowserFields[selected].fields != null && filteredBrowserFields[category].fields.length > filteredBrowserFields[selected].fields.length ? category : selected;
            }, Object.keys(filteredBrowserFields)[0])
          };
        });
      }, INPUT_TIMEOUT);
    });

    _defineProperty(_assertThisInitialized(_this), "updateSelectedCategoryId", function (categoryId) {
      _this.setState({
        selectedCategoryId: categoryId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "updateColumnsAndSelectCategoryId", function (columns) {
      _this.props.onUpdateColumns(columns); // show the category columns in the timeline

    });

    _defineProperty(_assertThisInitialized(_this), "hideFieldBrowser", function () {
      _this.setState({
        filterInput: '',
        filteredBrowserFields: null,
        isSearching: false,
        selectedCategoryId: _default_headers.DEFAULT_CATEGORY_NAME,
        show: false
      });
    });

    _this.state = {
      filterInput: '',
      filteredBrowserFields: null,
      isSearching: false,
      selectedCategoryId: _default_headers.DEFAULT_CATEGORY_NAME,
      show: false
    };
    return _this;
  }

  _createClass(StatefulFieldsBrowserComponent, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.inputTimeoutId !== 0) {
        // ⚠️ mutation: cancel any remaining timers and zero-out the timer id:
        clearTimeout(this.inputTimeoutId);
        this.inputTimeoutId = 0;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          columnHeaders = _this$props.columnHeaders,
          browserFields = _this$props.browserFields,
          height = _this$props.height,
          _this$props$isEventVi = _this$props.isEventViewer,
          isEventViewer = _this$props$isEventVi === void 0 ? false : _this$props$isEventVi,
          onFieldSelected = _this$props.onFieldSelected,
          timelineId = _this$props.timelineId,
          toggleColumn = _this$props.toggleColumn,
          width = _this$props.width;
      var _this$state = this.state,
          filterInput = _this$state.filterInput,
          filteredBrowserFields = _this$state.filteredBrowserFields,
          isSearching = _this$state.isSearching,
          selectedCategoryId = _this$state.selectedCategoryId,
          show = _this$state.show; // only merge in the default category if the field browser is visible

      var browserFieldsWithDefaultCategory = show ? (0, _helpers.mergeBrowserFieldsWithDefaultCategory)(browserFields) : {};
      return React.createElement(React.Fragment, null, React.createElement(FieldsBrowserButtonContainer, {
        "data-test-subj": "fields-browser-button-container",
        show: show
      }, React.createElement(_eui.EuiToolTip, {
        content: i18n.CUSTOMIZE_COLUMNS
      }, isEventViewer ? React.createElement(_eui.EuiButtonIcon, {
        "aria-label": i18n.CUSTOMIZE_COLUMNS,
        className: fieldsButtonClassName,
        "data-test-subj": "show-field-browser-gear",
        iconType: "gear",
        onClick: this.toggleShow
      }) : React.createElement(_eui.EuiButton, {
        className: fieldsButtonClassName,
        color: "primary",
        "data-test-subj": "show-field-browser",
        iconSide: "right",
        iconType: "arrowDown",
        onClick: this.toggleShow,
        size: "s"
      }, i18n.FIELDS)), show && React.createElement(_field_browser.FieldsBrowser, {
        browserFields: browserFieldsWithDefaultCategory,
        columnHeaders: columnHeaders,
        filteredBrowserFields: filteredBrowserFields != null ? filteredBrowserFields : browserFieldsWithDefaultCategory,
        height: height,
        isEventViewer: isEventViewer,
        isSearching: isSearching,
        onCategorySelected: this.updateSelectedCategoryId,
        onFieldSelected: onFieldSelected,
        onHideFieldBrowser: this.hideFieldBrowser,
        onOutsideClick: show ? this.hideFieldBrowser : _fp.noop,
        onSearchInputChange: this.updateFilter,
        onUpdateColumns: this.updateColumnsAndSelectCategoryId,
        searchInput: filterInput,
        selectedCategoryId: selectedCategoryId,
        timelineId: timelineId,
        toggleColumn: toggleColumn,
        width: width
      })));
    }
    /** Shows / hides the field browser */

  }]);

  return StatefulFieldsBrowserComponent;
}(React.PureComponent);

exports.StatefulFieldsBrowserComponent = StatefulFieldsBrowserComponent;
var StatefulFieldsBrowser = (0, _reactRedux.connect)(null, {
  removeColumn: _actions.timelineActions.removeColumn,
  upsertColumn: _actions.timelineActions.upsertColumn
})(StatefulFieldsBrowserComponent);
exports.StatefulFieldsBrowser = StatefulFieldsBrowser;