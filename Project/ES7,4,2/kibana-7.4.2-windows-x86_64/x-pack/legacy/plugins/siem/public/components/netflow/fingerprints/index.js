"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Fingerprints = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _certificate_fingerprint = require("../../certificate_fingerprint");

var _ja3_fingerprint = require("../../ja3_fingerprint");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Renders rows of draggable badges containing ja3 and certificate fingerprints
 * (i.e. sha1 hashes)
 */
var Fingerprints = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      tlsClientCertificateFingerprintSha1 = _ref.tlsClientCertificateFingerprintSha1,
      tlsFingerprintsJa3Hash = _ref.tlsFingerprintsJa3Hash,
      tlsServerCertificateFingerprintSha1 = _ref.tlsServerCertificateFingerprintSha1;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    "data-test-subj": "fingerprints-group",
    direction: "column",
    justifyContent: "center",
    gutterSize: "none"
  }, tlsFingerprintsJa3Hash != null ? (0, _fp.uniq)(tlsFingerprintsJa3Hash).map(function (ja3) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: ja3
    }, React.createElement(_ja3_fingerprint.Ja3Fingerprint, {
      eventId: eventId,
      fieldName: _ja3_fingerprint.JA3_HASH_FIELD_NAME,
      contextId: contextId,
      value: ja3
    }));
  }) : null, tlsClientCertificateFingerprintSha1 != null ? (0, _fp.uniq)(tlsClientCertificateFingerprintSha1).map(function (clientCert) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: clientCert
    }, React.createElement(_certificate_fingerprint.CertificateFingerprint, {
      eventId: eventId,
      certificateType: "client",
      contextId: contextId,
      fieldName: _certificate_fingerprint.TLS_CLIENT_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME,
      value: clientCert
    }));
  }) : null, tlsServerCertificateFingerprintSha1 != null ? (0, _fp.uniq)(tlsServerCertificateFingerprintSha1).map(function (serverCert) {
    return React.createElement(_eui.EuiFlexItem, {
      grow: false,
      key: serverCert
    }, React.createElement(_certificate_fingerprint.CertificateFingerprint, {
      eventId: eventId,
      certificateType: "server",
      contextId: contextId,
      fieldName: _certificate_fingerprint.TLS_SERVER_CERTIFICATE_FINGERPRINT_SHA1_FIELD_NAME,
      value: serverCert
    }));
  }) : null);
});
exports.Fingerprints = Fingerprints;
Fingerprints.displayName = 'Fingerprints';