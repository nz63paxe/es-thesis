"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RowRendererContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _timeline_context = require("../../timeline_context");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RowRendererContainer = _react.default.memo(function (_ref) {
  var children = _ref.children;
  var width = (0, _timeline_context.useTimelineWidthContext)(); // Passing the styles directly to the component because the width is
  // being calculated and is recommended by Styled Components for performance
  // https://github.com/styled-components/styled-components/issues/134#issuecomment-312415291

  return _react.default.createElement("div", {
    style: {
      width: "".concat(width, "px")
    }
  }, children);
});

exports.RowRendererContainer = RowRendererContainer;
RowRendererContainer.displayName = 'RowRendererContainer';