"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TruncatableText = void 0;

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

/**
 * Applies CSS styling to enable text to be truncated with an ellipsis.
 * Example: "Don't leave me hanging..."
 *
 * Width is required, because CSS will not truncate the text unless a width is
 * specified.
 */
var TruncatableText = (0, _styledComponents.default)(_eui.EuiText)(_templateObject(), function (_ref) {
  var width = _ref.width;
  return width;
});
exports.TruncatableText = TruncatableText;
TruncatableText.displayName = 'TruncatableText';