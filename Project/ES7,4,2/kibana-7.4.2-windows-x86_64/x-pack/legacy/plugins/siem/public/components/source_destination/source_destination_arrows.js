"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceDestinationArrows = exports.DESTINATION_PACKETS_FIELD_NAME = exports.DESTINATION_BYTES_FIELD_NAME = exports.SOURCE_PACKETS_FIELD_NAME = exports.SOURCE_BYTES_FIELD_NAME = void 0;

var _eui = require("@elastic/eui");

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _arrows = require("../arrows");

var _helpers = require("../arrows/helpers");

var _draggables = require("../draggables");

var _formatted_bytes = require("../formatted_bytes");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 5px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin: 0 2px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var SOURCE_BYTES_FIELD_NAME = 'source.bytes';
exports.SOURCE_BYTES_FIELD_NAME = SOURCE_BYTES_FIELD_NAME;
var SOURCE_PACKETS_FIELD_NAME = 'source.packets';
exports.SOURCE_PACKETS_FIELD_NAME = SOURCE_PACKETS_FIELD_NAME;
var DESTINATION_BYTES_FIELD_NAME = 'destination.bytes';
exports.DESTINATION_BYTES_FIELD_NAME = DESTINATION_BYTES_FIELD_NAME;
var DESTINATION_PACKETS_FIELD_NAME = 'destination.packets';
exports.DESTINATION_PACKETS_FIELD_NAME = DESTINATION_PACKETS_FIELD_NAME;

var Percent = _styledComponents.default.span(_templateObject());

Percent.displayName = 'Percent';
var SourceDestinationArrowsContainer = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject2());
SourceDestinationArrowsContainer.displayName = 'SourceDestinationArrowsContainer';
var Data = (0, _styledComponents.default)(_eui.EuiText)(_templateObject3());
Data.displayName = 'Data';
/**
 * Visualizes the communication from a source as an arrow with draggable badges
 */

var SourceArrow = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      sourceBytes = _ref.sourceBytes,
      sourceBytesPercent = _ref.sourceBytesPercent,
      sourcePackets = _ref.sourcePackets;
  var sourceArrowHeight = sourceBytesPercent != null ? (0, _helpers.getArrowHeightFromPercent)(sourceBytesPercent) : _helpers.DEFAULT_ARROW_HEIGHT;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none",
    justifyContent: "center"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    height: sourceArrowHeight
  })), sourceBytes != null && !isNaN(Number(sourceBytes)) ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_draggables.DefaultDraggable, {
    field: SOURCE_BYTES_FIELD_NAME,
    id: "source-arrow-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(SOURCE_BYTES_FIELD_NAME, "-").concat(sourceBytes),
    value: sourceBytes
  }, React.createElement(Data, {
    size: "xs"
  }, sourceBytesPercent != null ? React.createElement(Percent, {
    "data-test-subj": "source-bytes-percent"
  }, "(".concat((0, _numeral.default)(sourceBytesPercent).format('0.00'), "%)")) : null, React.createElement("span", {
    "data-test-subj": "source-bytes"
  }, React.createElement(_formatted_bytes.PreferenceFormattedBytes, {
    value: sourceBytes
  }))))) : null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    "data-test-subj": "source-arrow",
    height: sourceArrowHeight
  })), sourcePackets != null && !isNaN(Number(sourcePackets)) ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_draggables.DefaultDraggable, {
    field: SOURCE_PACKETS_FIELD_NAME,
    id: "source-arrow-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(SOURCE_PACKETS_FIELD_NAME, "-").concat(sourcePackets),
    value: sourcePackets
  }, React.createElement(Data, {
    size: "xs"
  }, React.createElement("span", {
    "data-test-subj": "source-packets"
  }, "".concat(sourcePackets, " ").concat(i18n.PACKETS))))) : null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    height: sourceArrowHeight
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowHead, {
    direction: "arrowRight"
  })));
});
SourceArrow.displayName = 'SourceArrow';
/**
 * Visualizes the communication from a destination as an arrow with draggable
 * badges
 */

var DestinationArrow = (0, _recompose.pure)(function (_ref2) {
  var contextId = _ref2.contextId,
      eventId = _ref2.eventId,
      destinationBytes = _ref2.destinationBytes,
      destinationBytesPercent = _ref2.destinationBytesPercent,
      destinationPackets = _ref2.destinationPackets;
  var destinationArrowHeight = destinationBytesPercent != null ? (0, _helpers.getArrowHeightFromPercent)(destinationBytesPercent) : _helpers.DEFAULT_ARROW_HEIGHT;
  return React.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "none",
    justifyContent: "center"
  }, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowHead, {
    direction: "arrowLeft"
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    height: destinationArrowHeight
  })), destinationBytes != null && !isNaN(Number(destinationBytes)) ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_draggables.DefaultDraggable, {
    field: DESTINATION_BYTES_FIELD_NAME,
    id: "destination-arrow-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(DESTINATION_BYTES_FIELD_NAME, "-").concat(destinationBytes),
    value: destinationBytes
  }, React.createElement(Data, {
    size: "xs"
  }, destinationBytesPercent != null ? React.createElement(Percent, {
    "data-test-subj": "destination-bytes-percent"
  }, "(".concat((0, _numeral.default)(destinationBytesPercent).format('0.00'), "%)")) : null, React.createElement("span", {
    "data-test-subj": "destination-bytes"
  }, React.createElement(_formatted_bytes.PreferenceFormattedBytes, {
    value: destinationBytes
  }))))) : null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    height: destinationArrowHeight
  })), destinationPackets != null && !isNaN(Number(destinationPackets)) ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_draggables.DefaultDraggable, {
    field: DESTINATION_PACKETS_FIELD_NAME,
    id: "destination-arrow-default-draggable-".concat(contextId, "-").concat(eventId, "-").concat(DESTINATION_PACKETS_FIELD_NAME, "-").concat(destinationPackets),
    value: destinationPackets
  }, React.createElement(Data, {
    size: "xs"
  }, React.createElement("span", {
    "data-test-subj": "destination-packets"
  }, "".concat((0, _numeral.default)(destinationPackets).format('0,0'), " ").concat(i18n.PACKETS))))) : null, React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_arrows.ArrowBody, {
    height: destinationArrowHeight
  })));
});
DestinationArrow.displayName = 'DestinationArrow';
/**
 * Visualizes the communication between a source and a destination using arrows
 * that grow in thickness based on the percentage of bytes transferred, and stats badges
 */

var SourceDestinationArrows = (0, _recompose.pure)(function (_ref3) {
  var contextId = _ref3.contextId,
      destinationBytes = _ref3.destinationBytes,
      destinationPackets = _ref3.destinationPackets,
      eventId = _ref3.eventId,
      sourceBytes = _ref3.sourceBytes,
      sourcePackets = _ref3.sourcePackets;
  var maybeSourceBytes = sourceBytes != null && (0, _helpers.hasOneValue)(sourceBytes) ? sourceBytes[0] : undefined;
  var maybeSourcePackets = sourcePackets != null && (0, _helpers.hasOneValue)(sourcePackets) ? sourcePackets[0] : undefined;
  var maybeDestinationBytes = destinationBytes != null && (0, _helpers.hasOneValue)(destinationBytes) ? destinationBytes[0] : undefined;
  var maybeDestinationPackets = destinationPackets != null && (0, _helpers.hasOneValue)(destinationPackets) ? destinationPackets[0] : undefined;
  var maybeSourceBytesPercent = maybeSourceBytes != null && maybeDestinationBytes != null ? (0, _helpers.getPercent)({
    numerator: Number(maybeSourceBytes),
    denominator: Number(maybeSourceBytes) + Number(maybeDestinationBytes)
  }) : undefined;
  var maybeDestinationBytesPercent = maybeSourceBytesPercent != null ? 100 - maybeSourceBytesPercent : undefined;
  return React.createElement(SourceDestinationArrowsContainer, {
    alignItems: "center",
    "data-test-subj": "source-destination-arrows-container",
    justifyContent: "center",
    direction: "column",
    gutterSize: "none"
  }, maybeSourceBytes != null ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(SourceArrow, {
    contextId: contextId,
    sourceBytes: maybeSourceBytes,
    sourcePackets: maybeSourcePackets,
    sourceBytesPercent: maybeSourceBytesPercent,
    eventId: eventId
  })) : null, maybeDestinationBytes != null ? React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(DestinationArrow, {
    contextId: contextId,
    destinationBytes: maybeDestinationBytes,
    destinationPackets: maybeDestinationPackets,
    destinationBytesPercent: maybeDestinationBytesPercent,
    eventId: eventId
  })) : null);
});
exports.SourceDestinationArrows = SourceDestinationArrows;
SourceDestinationArrows.displayName = 'SourceDestinationArrows';