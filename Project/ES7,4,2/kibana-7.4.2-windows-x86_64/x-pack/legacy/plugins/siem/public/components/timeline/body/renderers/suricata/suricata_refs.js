"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SuricataRefs = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _external_link_icon = require("../../../../external_link_icon");

var _suricata_links = require("./suricata_links");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: inline;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LinkEuiFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
LinkEuiFlexItem.displayName = 'LinkEuiFlexItem';
var SuricataRefs = (0, _recompose.pure)(function (_ref) {
  var signatureId = _ref.signatureId;
  var links = (0, _suricata_links.getLinksFromSignature)(signatureId);
  return React.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none",
    justifyContent: "center",
    wrap: true
  }, links.map(function (link) {
    return React.createElement(LinkEuiFlexItem, {
      key: link,
      grow: false
    }, React.createElement(_eui.EuiLink, {
      href: link,
      color: "subdued",
      target: "_blank"
    }, link), React.createElement(_external_link_icon.ExternalLinkIcon, null));
  }));
});
exports.SuricataRefs = SuricataRefs;
SuricataRefs.displayName = 'SuricataRefs';