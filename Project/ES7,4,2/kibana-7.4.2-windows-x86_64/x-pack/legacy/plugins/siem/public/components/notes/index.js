"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Notes = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _add_note = require("./add_note");

var _columns = require("./columns");

var _helpers = require("./helpers");

var _notes_size = require("../timeline/properties/notes_size");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  overflow-x: hidden;\n  overflow-y: auto;\n  height: 220px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: ", "px;\n  width: ", "px;\n\n  & thead {\n    display: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NotesPanel = (0, _styledComponents.default)(_eui.EuiPanel)(_templateObject(), _notes_size.NOTES_PANEL_HEIGHT, _notes_size.NOTES_PANEL_WIDTH);
NotesPanel.displayName = 'NotesPanel';
var InMemoryTable = (0, _styledComponents.default)(_eui.EuiInMemoryTable)(_templateObject2());
InMemoryTable.displayName = 'InMemoryTable';
/** A view for entering and reviewing notes */

var Notes =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(Notes, _React$PureComponent);

  function Notes(props) {
    var _this;

    _classCallCheck(this, Notes);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Notes).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "updateNewNote", function (newNote) {
      _this.setState({
        newNote: newNote
      });
    });

    _this.state = {
      newNote: ''
    };
    return _this;
  }

  _createClass(Notes, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          associateNote = _this$props.associateNote,
          getNotesByIds = _this$props.getNotesByIds,
          getNewNoteId = _this$props.getNewNoteId,
          noteIds = _this$props.noteIds,
          updateNote = _this$props.updateNote;
      return React.createElement(NotesPanel, null, React.createElement(_eui.EuiModalHeader, null, React.createElement(_helpers.NotesCount, {
        noteIds: noteIds
      })), React.createElement(_eui.EuiModalBody, null, React.createElement(_add_note.AddNote, {
        associateNote: associateNote,
        getNewNoteId: getNewNoteId,
        newNote: this.state.newNote,
        updateNewNote: this.updateNewNote,
        updateNote: updateNote
      }), React.createElement(_eui.EuiSpacer, {
        size: "s"
      }), React.createElement(InMemoryTable, {
        "data-test-subj": "notes-table",
        items: getNotesByIds(noteIds),
        columns: _columns.columns,
        pagination: false,
        search: _helpers.search,
        sorting: true
      })));
    }
  }]);

  return Notes;
}(React.PureComponent);

exports.Notes = Notes;