"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HostsContainer = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _details = require("./details");

var _hosts_navigations = require("./hosts_navigations");

var _hosts_body = require("./hosts_body");

var _model = require("../../store/hosts/model");

var _global_time = require("../../containers/global_time");

var _hosts = require("./hosts");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var hostsPagePath = "/:pageName(hosts)";

var getHostsTabPath = function getHostsTabPath(pagePath) {
  return "".concat(pagePath, "/:tabName(") + "".concat(_model.HostsTableType.hosts, "|") + "".concat(_model.HostsTableType.authentications, "|") + "".concat(_model.HostsTableType.uncommonProcesses, "|") + "".concat(_model.HostsTableType.anomalies, "|") + "".concat(_model.HostsTableType.events, ")");
};

var getHostDetailsTabPath = function getHostDetailsTabPath(pagePath) {
  return "".concat(pagePath, "/:detailName/:tabName(") + "".concat(_model.HostsTableType.authentications, "|") + "".concat(_model.HostsTableType.uncommonProcesses, "|") + "".concat(_model.HostsTableType.anomalies, "|") + "".concat(_model.HostsTableType.events, ")");
};

var HostsContainer = _react.default.memo(function (_ref) {
  var url = _ref.url;
  return _react.default.createElement(_global_time.GlobalTime, null, function (_ref2) {
    var to = _ref2.to,
        from = _ref2.from,
        setQuery = _ref2.setQuery,
        deleteQuery = _ref2.deleteQuery,
        isInitializing = _ref2.isInitializing;
    return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
      strict: true,
      exact: true,
      path: hostsPagePath,
      render: function render() {
        return _react.default.createElement(_reactRouterDom.Route, {
          path: hostsPagePath,
          render: function render() {
            return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_hosts.Hosts, {
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing
            }), _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.HostsQueryTabBody
            }));
          }
        });
      }
    }), _react.default.createElement(_reactRouterDom.Route, {
      strict: true,
      exact: true,
      path: getHostsTabPath(hostsPagePath),
      render: function render() {
        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_hosts.Hosts, {
          from: from,
          to: to,
          setQuery: setQuery,
          isInitializing: isInitializing
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:tabName(").concat(_model.HostsTableType.hosts, ")"),
          render: function render() {
            return _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.HostsQueryTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:tabName(").concat(_model.HostsTableType.authentications, ")"),
          render: function render() {
            return _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.AuthenticationsQueryTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:tabName(").concat(_model.HostsTableType.uncommonProcesses, ")"),
          render: function render() {
            return _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.UncommonProcessTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:tabName(").concat(_model.HostsTableType.anomalies, ")"),
          render: function render() {
            return _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.AnomaliesTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:tabName(").concat(_model.HostsTableType.events, ")"),
          render: function render() {
            return _react.default.createElement(_hosts_body.HostsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.EventsTabBody
            });
          }
        }));
      }
    }), _react.default.createElement(_reactRouterDom.Route, {
      strict: true,
      exact: true,
      path: getHostDetailsTabPath(hostsPagePath),
      render: function render(props) {
        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_details.HostDetails, {
          from: from,
          to: to,
          setQuery: setQuery,
          isInitializing: isInitializing,
          detailName: props.match.params.detailName
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:detailName/:tabName(").concat(_model.HostsTableType.hosts, ")"),
          render: function render() {
            return _react.default.createElement(_details.HostDetailsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              children: _hosts_navigations.HostsQueryTabBody,
              detailName: props.match.params.detailName
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:detailName/:tabName(").concat(_model.HostsTableType.authentications, ")"),
          render: function render() {
            return _react.default.createElement(_details.HostDetailsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              detailName: props.match.params.detailName,
              children: _hosts_navigations.AuthenticationsQueryTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:detailName/:tabName(").concat(_model.HostsTableType.uncommonProcesses, ")"),
          render: function render() {
            return _react.default.createElement(_details.HostDetailsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              detailName: props.match.params.detailName,
              children: _hosts_navigations.UncommonProcessTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:detailName/:tabName(").concat(_model.HostsTableType.anomalies, ")"),
          render: function render() {
            return _react.default.createElement(_details.HostDetailsBody, {
              deleteQuery: deleteQuery,
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              detailName: props.match.params.detailName,
              children: _hosts_navigations.AnomaliesTabBody
            });
          }
        }), _react.default.createElement(_reactRouterDom.Route, {
          path: "".concat(hostsPagePath, "/:detailName/:tabName(").concat(_model.HostsTableType.events, ")"),
          render: function render() {
            return _react.default.createElement(_details.HostDetailsBody, {
              from: from,
              to: to,
              setQuery: setQuery,
              isInitializing: isInitializing,
              detailName: props.match.params.detailName,
              children: _hosts_navigations.EventsTabBody
            });
          }
        }));
      }
    }), _react.default.createElement(_reactRouterDom.Route, {
      path: "".concat(url, "/:detailName"),
      render: function render(_ref3) {
        var _ref3$location$search = _ref3.location.search,
            search = _ref3$location$search === void 0 ? '' : _ref3$location$search;
        return _react.default.createElement(_reactRouterDom.Redirect, {
          from: "".concat(url, "/:detailName"),
          to: "".concat(url, "/:detailName/").concat(_model.HostsTableType.authentications).concat(search)
        });
      }
    }), _react.default.createElement(_reactRouterDom.Route, {
      path: "/hosts/",
      render: function render(_ref4) {
        var _ref4$location$search = _ref4.location.search,
            search = _ref4$location$search === void 0 ? '' : _ref4$location$search;
        return _react.default.createElement(_reactRouterDom.Redirect, {
          from: "/hosts/\"",
          to: "/hosts/".concat(_model.HostsTableType.hosts).concat(search)
        });
      }
    }));
  });
});

exports.HostsContainer = HostsContainer;
HostsContainer.displayName = 'HostsContainer';