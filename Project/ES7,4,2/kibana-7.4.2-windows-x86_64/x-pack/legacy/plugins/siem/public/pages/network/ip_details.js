"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getBreadcrumbs = exports.IPDetails = exports.IPDetailsComponent = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reactSticky = require("react-sticky");

var _recompose = require("recompose");

var _filters_global = require("../../components/filters_global");

var _header_page = require("../../components/header_page");

var _last_event_time = require("../../components/last_event_time");

var _redirect_to_network = require("../../components/link_to/redirect_to_network");

var _manage_query = require("../../components/page/manage_query");

var _domains_table = require("../../components/page/network/domains_table");

var _flow_target_select_connected = require("../../components/page/network/flow_target_select_connected");

var _ip_overview = require("../../components/page/network/ip_overview");

var _users_table = require("../../components/page/network/users_table");

var _tls_table = require("../../components/page/network/tls_table");

var _domains = require("../../containers/domains");

var _global_time = require("../../containers/global_time");

var _ip_overview2 = require("../../containers/ip_overview");

var _source = require("../../containers/source");

var _tls = require("../../containers/tls");

var _users = require("../../containers/users");

var _types = require("../../graphql/types");

var _helpers = require("../../lib/helpers");

var _store = require("../../store");

var _actions = require("../../store/inputs/actions");

var _kql = require("./kql");

var _network_empty_page = require("./network_empty_page");

var i18n = _interopRequireWildcard(require("./translations"));

var _anomaly_table_provider = require("../../components/ml/anomaly/anomaly_table_provider");

var _score_interval_to_datetime = require("../../components/ml/score/score_interval_to_datetime");

var _anomalies_network_table = require("../../components/ml/tables/anomalies_network_table");

var _network_to_criteria = require("../../components/ml/criteria/network_to_criteria");

var _spy_routes = require("../../utils/route/spy_routes");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DomainsTableManage = (0, _manage_query.manageQuery)(_domains_table.DomainsTable);
var TlsTableManage = (0, _manage_query.manageQuery)(_tls_table.TlsTable);
var UsersTableManage = (0, _manage_query.manageQuery)(_users_table.UsersTable);
var IpOverviewManage = (0, _manage_query.manageQuery)(_ip_overview.IpOverview);
var IPDetailsComponent = (0, _recompose.pure)(function (_ref) {
  var detailName = _ref.detailName,
      filterQuery = _ref.filterQuery,
      flowTarget = _ref.flowTarget,
      setAbsoluteRangeDatePicker = _ref.setAbsoluteRangeDatePicker;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_source.WithSource, {
    sourceId: "default",
    "data-test-subj": "ip-details-page"
  }, function (_ref2) {
    var indicesExist = _ref2.indicesExist,
        indexPattern = _ref2.indexPattern;
    var ip = (0, _helpers.decodeIpv6)(detailName);
    return (0, _source.indicesExistOrDataTemporarilyUnavailable)(indicesExist) ? _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_global_time.GlobalTime, null, function (_ref3) {
      var to = _ref3.to,
          from = _ref3.from,
          setQuery = _ref3.setQuery,
          isInitializing = _ref3.isInitializing;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_filters_global.FiltersGlobal, null, _react.default.createElement(_kql.NetworkKql, {
        indexPattern: indexPattern,
        setQuery: setQuery,
        type: _store.networkModel.NetworkType.details
      })), _react.default.createElement(_header_page.HeaderPage, {
        "data-test-subj": "ip-details-headline",
        subtitle: _react.default.createElement(_last_event_time.LastEventTime, {
          indexKey: _types.LastEventIndexKey.ipDetails,
          ip: ip
        }),
        title: ip,
        draggableArguments: {
          field: "".concat(flowTarget, ".ip"),
          value: ip
        }
      }, _react.default.createElement(_flow_target_select_connected.FlowTargetSelectConnected, null)), _react.default.createElement(_ip_overview2.IpOverviewQuery, {
        skip: isInitializing,
        sourceId: "default",
        filterQuery: filterQuery,
        type: _store.networkModel.NetworkType.details,
        ip: ip
      }, function (_ref4) {
        var id = _ref4.id,
            inspect = _ref4.inspect,
            ipOverviewData = _ref4.ipOverviewData,
            loading = _ref4.loading,
            refetch = _ref4.refetch;
        return _react.default.createElement(_anomaly_table_provider.AnomalyTableProvider, {
          criteriaFields: (0, _network_to_criteria.networkToCriteria)(detailName, flowTarget),
          startDate: from,
          endDate: to,
          skip: isInitializing
        }, function (_ref5) {
          var isLoadingAnomaliesData = _ref5.isLoadingAnomaliesData,
              anomaliesData = _ref5.anomaliesData;
          return _react.default.createElement(IpOverviewManage, {
            id: id,
            inspect: inspect,
            ip: ip,
            data: ipOverviewData,
            anomaliesData: anomaliesData,
            loading: loading,
            isLoadingAnomaliesData: isLoadingAnomaliesData,
            type: _store.networkModel.NetworkType.details,
            flowTarget: flowTarget,
            refetch: refetch,
            setQuery: setQuery,
            startDate: from,
            endDate: to,
            narrowDateRange: function narrowDateRange(score, interval) {
              var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
              setAbsoluteRangeDatePicker({
                id: 'global',
                from: fromTo.from,
                to: fromTo.to
              });
            }
          });
        });
      }), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_domains.DomainsQuery, {
        endDate: to,
        filterQuery: filterQuery,
        flowTarget: flowTarget,
        ip: ip,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.details
      }, function (_ref6) {
        var id = _ref6.id,
            inspect = _ref6.inspect,
            isInspected = _ref6.isInspected,
            domains = _ref6.domains,
            totalCount = _ref6.totalCount,
            pageInfo = _ref6.pageInfo,
            loading = _ref6.loading,
            loadPage = _ref6.loadPage,
            refetch = _ref6.refetch;
        return _react.default.createElement(DomainsTableManage, {
          data: domains,
          indexPattern: indexPattern,
          id: id,
          inspect: inspect,
          flowTarget: flowTarget,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          ip: ip,
          isInspect: isInspected,
          loading: loading,
          loadPage: loadPage,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          refetch: refetch,
          setQuery: setQuery,
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.details
        });
      }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_users.UsersQuery, {
        endDate: to,
        filterQuery: filterQuery,
        flowTarget: flowTarget,
        ip: ip,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.details
      }, function (_ref7) {
        var id = _ref7.id,
            inspect = _ref7.inspect,
            isInspected = _ref7.isInspected,
            users = _ref7.users,
            totalCount = _ref7.totalCount,
            pageInfo = _ref7.pageInfo,
            loading = _ref7.loading,
            loadPage = _ref7.loadPage,
            refetch = _ref7.refetch;
        return _react.default.createElement(UsersTableManage, {
          data: users,
          id: id,
          inspect: inspect,
          isInspect: isInspected,
          flowTarget: flowTarget,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          loading: loading,
          loadPage: loadPage,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          refetch: refetch,
          setQuery: setQuery,
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.details
        });
      }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_tls.TlsQuery, {
        endDate: to,
        filterQuery: filterQuery,
        flowTarget: flowTarget,
        ip: ip,
        skip: isInitializing,
        sourceId: "default",
        startDate: from,
        type: _store.networkModel.NetworkType.details
      }, function (_ref8) {
        var id = _ref8.id,
            inspect = _ref8.inspect,
            isInspected = _ref8.isInspected,
            tls = _ref8.tls,
            totalCount = _ref8.totalCount,
            pageInfo = _ref8.pageInfo,
            loading = _ref8.loading,
            loadPage = _ref8.loadPage,
            refetch = _ref8.refetch;
        return _react.default.createElement(TlsTableManage, {
          data: tls,
          id: id,
          inspect: inspect,
          isInspect: isInspected,
          fakeTotalCount: (0, _fp.getOr)(50, 'fakeTotalCount', pageInfo),
          loading: loading,
          loadPage: loadPage,
          showMorePagesIndicator: (0, _fp.getOr)(false, 'showMorePagesIndicator', pageInfo),
          refetch: refetch,
          setQuery: setQuery,
          totalCount: totalCount,
          type: _store.networkModel.NetworkType.details
        });
      }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_anomalies_network_table.AnomaliesNetworkTable, {
        startDate: from,
        endDate: to,
        skip: isInitializing,
        ip: ip,
        type: _store.networkModel.NetworkType.details,
        flowTarget: flowTarget,
        narrowDateRange: function narrowDateRange(score, interval) {
          var fromTo = (0, _score_interval_to_datetime.scoreIntervalToDateTime)(score, interval);
          setAbsoluteRangeDatePicker({
            id: 'global',
            from: fromTo.from,
            to: fromTo.to
          });
        }
      }));
    })) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_header_page.HeaderPage, {
      title: ip
    }), _react.default.createElement(_network_empty_page.NetworkEmptyPage, null));
  }), _react.default.createElement(_spy_routes.SpyRoute, null));
});
exports.IPDetailsComponent = IPDetailsComponent;
IPDetailsComponent.displayName = 'IPDetailsComponent';

var makeMapStateToProps = function makeMapStateToProps() {
  var getNetworkFilterQuery = _store.networkSelectors.networkFilterQueryAsJson();

  var getIpDetailsFlowTargetSelector = _store.networkSelectors.ipDetailsFlowTargetSelector();

  return function (state) {
    return {
      filterQuery: getNetworkFilterQuery(state, _store.networkModel.NetworkType.details) || '',
      flowTarget: getIpDetailsFlowTargetSelector(state)
    };
  };
};

var IPDetails = (0, _reactRedux.connect)(makeMapStateToProps, {
  setAbsoluteRangeDatePicker: _actions.setAbsoluteRangeDatePicker
})(IPDetailsComponent);
exports.IPDetails = IPDetails;

var getBreadcrumbs = function getBreadcrumbs(ip, search) {
  var breadcrumbs = [{
    text: i18n.PAGE_TITLE,
    href: "".concat((0, _redirect_to_network.getNetworkUrl)()).concat(search && search[0] ? search[0] : '')
  }];

  if (ip) {
    return [].concat(breadcrumbs, [{
      text: (0, _helpers.decodeIpv6)(ip),
      href: ''
    }]);
  } else {
    return breadcrumbs;
  }
};

exports.getBreadcrumbs = getBreadcrumbs;