"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NetflowColumns = exports.EVENT_END = exports.EVENT_START = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _source_destination = require("../../source_destination");

var _duration_event_start_end = require("./duration_event_start_end");

var _user_process = require("./user_process");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 10px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EVENT_START = 'event.start';
exports.EVENT_START = EVENT_START;
var EVENT_END = 'event.end';
exports.EVENT_END = EVENT_END;
var EuiFlexItemMarginRight = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject());
EuiFlexItemMarginRight.displayName = 'EuiFlexItemMarginRight';
/**
 * Renders columns of draggable badges that describe both Netflow data, or more
 * generally, hosts interacting over a network connection. This component is
 * consumed by the `Netflow` visualization / row renderer.
 *
 * This component will allow columns to wrap if constraints on width prevent all
 * the columns from fitting on a single horizontal row
 */

var NetflowColumns = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      destinationBytes = _ref.destinationBytes,
      destinationGeoContinentName = _ref.destinationGeoContinentName,
      destinationGeoCountryName = _ref.destinationGeoCountryName,
      destinationGeoCountryIsoCode = _ref.destinationGeoCountryIsoCode,
      destinationGeoRegionName = _ref.destinationGeoRegionName,
      destinationGeoCityName = _ref.destinationGeoCityName,
      destinationIp = _ref.destinationIp,
      destinationPackets = _ref.destinationPackets,
      destinationPort = _ref.destinationPort,
      eventDuration = _ref.eventDuration,
      eventId = _ref.eventId,
      eventEnd = _ref.eventEnd,
      eventStart = _ref.eventStart,
      networkBytes = _ref.networkBytes,
      networkCommunityId = _ref.networkCommunityId,
      networkDirection = _ref.networkDirection,
      networkPackets = _ref.networkPackets,
      networkProtocol = _ref.networkProtocol,
      processName = _ref.processName,
      sourceBytes = _ref.sourceBytes,
      sourceGeoContinentName = _ref.sourceGeoContinentName,
      sourceGeoCountryName = _ref.sourceGeoCountryName,
      sourceGeoCountryIsoCode = _ref.sourceGeoCountryIsoCode,
      sourceGeoRegionName = _ref.sourceGeoRegionName,
      sourceGeoCityName = _ref.sourceGeoCityName,
      sourceIp = _ref.sourceIp,
      sourcePackets = _ref.sourcePackets,
      sourcePort = _ref.sourcePort,
      transport = _ref.transport,
      userName = _ref.userName;
  return React.createElement(_eui.EuiFlexGroup, {
    "data-test-subj": "netflow-columns",
    gutterSize: "none",
    justifyContent: "center",
    wrap: true
  }, React.createElement(EuiFlexItemMarginRight, {
    grow: false
  }, React.createElement(_user_process.UserProcess, {
    contextId: contextId,
    eventId: eventId,
    processName: processName,
    userName: userName
  })), React.createElement(EuiFlexItemMarginRight, {
    grow: false
  }, React.createElement(_duration_event_start_end.DurationEventStartEnd, {
    contextId: contextId,
    eventDuration: eventDuration,
    eventId: eventId,
    eventEnd: eventEnd,
    eventStart: eventStart
  })), React.createElement(_eui.EuiFlexItem, {
    grow: false
  }, React.createElement(_source_destination.SourceDestination, {
    contextId: contextId,
    destinationBytes: destinationBytes,
    destinationGeoContinentName: destinationGeoContinentName,
    destinationGeoCountryName: destinationGeoCountryName,
    destinationGeoCountryIsoCode: destinationGeoCountryIsoCode,
    destinationGeoRegionName: destinationGeoRegionName,
    destinationGeoCityName: destinationGeoCityName,
    destinationIp: destinationIp,
    destinationPackets: destinationPackets,
    destinationPort: destinationPort,
    eventId: eventId,
    networkBytes: networkBytes,
    networkCommunityId: networkCommunityId,
    networkDirection: networkDirection,
    networkPackets: networkPackets,
    networkProtocol: networkProtocol,
    sourceBytes: sourceBytes,
    sourceGeoContinentName: sourceGeoContinentName,
    sourceGeoCountryName: sourceGeoCountryName,
    sourceGeoCountryIsoCode: sourceGeoCountryIsoCode,
    sourceGeoRegionName: sourceGeoRegionName,
    sourceGeoCityName: sourceGeoCityName,
    sourceIp: sourceIp,
    sourcePackets: sourcePackets,
    sourcePort: sourcePort,
    transport: transport
  })));
});
exports.NetflowColumns = NetflowColumns;
NetflowColumns.displayName = 'NetflowColumns';