"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mockGlobalState = void 0;

var _helpers = require("../components/timeline/body/helpers");

var _types = require("../graphql/types");

var _header = require("./header");

var _constants = require("../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var mockGlobalState = {
  app: {
    notesById: {},
    errors: [{
      id: 'error-id-1',
      title: 'title-1',
      message: ['error-message-1']
    }, {
      id: 'error-id-2',
      title: 'title-2',
      message: ['error-message-2']
    }]
  },
  hosts: {
    page: {
      queries: {
        authentications: {
          activePage: 0,
          limit: 10
        },
        allHosts: {
          activePage: 0,
          limit: 10,
          direction: _types.Direction.desc,
          sortField: _types.HostsFields.lastSeen
        },
        events: {
          activePage: 0,
          limit: 10
        },
        uncommonProcesses: {
          activePage: 0,
          limit: 10
        },
        anomalies: null
      },
      filterQuery: null,
      filterQueryDraft: null
    },
    details: {
      queries: {
        authentications: {
          activePage: 0,
          limit: 10
        },
        allHosts: {
          activePage: 0,
          limit: 10,
          direction: _types.Direction.desc,
          sortField: _types.HostsFields.lastSeen
        },
        events: {
          activePage: 0,
          limit: 10
        },
        uncommonProcesses: {
          activePage: 0,
          limit: 10
        },
        anomalies: null
      },
      filterQuery: null,
      filterQueryDraft: null
    }
  },
  network: {
    page: {
      queries: {
        topNFlowSource: {
          activePage: 0,
          limit: 10,
          topNFlowSort: {
            field: _types.NetworkTopNFlowFields.bytes_out,
            direction: _types.Direction.desc
          }
        },
        topNFlowDestination: {
          activePage: 0,
          limit: 10,
          topNFlowSort: {
            field: _types.NetworkTopNFlowFields.bytes_out,
            direction: _types.Direction.desc
          }
        },
        dns: {
          activePage: 0,
          limit: 10,
          dnsSortField: {
            field: _types.NetworkDnsFields.queryCount,
            direction: _types.Direction.desc
          },
          isPtrIncluded: false
        }
      },
      filterQuery: null,
      filterQueryDraft: null
    },
    details: {
      filterQuery: null,
      filterQueryDraft: null,
      flowTarget: _types.FlowTarget.source,
      queries: {
        domains: {
          activePage: 0,
          limit: 10,
          flowDirection: _types.FlowDirection.uniDirectional,
          domainsSortField: {
            field: _types.DomainsFields.bytes,
            direction: _types.Direction.desc
          }
        },
        tls: {
          activePage: 0,
          limit: 10,
          tlsSortField: {
            field: _types.TlsFields._id,
            direction: _types.Direction.desc
          }
        },
        users: {
          activePage: 0,
          limit: 10,
          usersSortField: {
            field: _types.UsersFields.name,
            direction: _types.Direction.asc
          }
        }
      }
    }
  },
  inputs: {
    global: {
      timerange: {
        kind: 'relative',
        fromStr: _constants.DEFAULT_FROM,
        toStr: _constants.DEFAULT_TO,
        from: 0,
        to: 1
      },
      linkTo: ['timeline'],
      query: [],
      policy: {
        kind: _constants.DEFAULT_INTERVAL_TYPE,
        duration: _constants.DEFAULT_INTERVAL_VALUE
      }
    },
    timeline: {
      timerange: {
        kind: 'relative',
        fromStr: _constants.DEFAULT_FROM,
        toStr: _constants.DEFAULT_TO,
        from: 0,
        to: 1
      },
      linkTo: ['global'],
      query: [],
      policy: {
        kind: _constants.DEFAULT_INTERVAL_TYPE,
        duration: _constants.DEFAULT_INTERVAL_VALUE
      }
    }
  },
  dragAndDrop: {
    dataProviders: {}
  },
  timeline: {
    showCallOutUnauthorizedMsg: false,
    autoSavedWarningMsg: {
      timelineId: null,
      newTimelineModel: null
    },
    timelineById: {
      test: {
        id: 'test',
        savedObjectId: null,
        columns: _header.defaultHeaders,
        itemsPerPage: 5,
        dataProviders: [],
        description: '',
        eventIdToNoteIds: {},
        highlightedDropAndProviderId: '',
        historyIds: [],
        isFavorite: false,
        isLive: false,
        isLoading: false,
        kqlMode: 'filter',
        kqlQuery: {
          filterQuery: null,
          filterQueryDraft: null
        },
        title: '',
        noteIds: [],
        dateRange: {
          start: 0,
          end: 0
        },
        show: false,
        pinnedEventIds: {},
        pinnedEventsSaveObject: {},
        itemsPerPageOptions: [5, 10, 20],
        sort: {
          columnId: '@timestamp',
          sortDirection: _types.Direction.desc
        },
        width: _helpers.DEFAULT_TIMELINE_WIDTH,
        isSaving: false,
        version: null
      }
    }
  }
};
exports.mockGlobalState = mockGlobalState;