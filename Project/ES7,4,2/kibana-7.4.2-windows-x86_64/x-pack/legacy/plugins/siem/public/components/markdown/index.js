"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Markdown = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _reactMarkdown = _interopRequireDefault(require("react-markdown"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TableHeader = _styledComponents.default.thead(_templateObject());

TableHeader.displayName = 'TableHeader';
/** prevents links to the new pages from accessing `window.opener` */

var REL_NOOPENER = 'noopener';
/** prevents search engine manipulation by noting the linked document is not trusted or endorsed by us */

var REL_NOFOLLOW = 'nofollow';
/** prevents the browser from sending the current address as referrer via the Referer HTTP header */

var REL_NOREFERRER = 'noreferrer';
var Markdown = (0, _recompose.pure)(function (_ref) {
  var raw = _ref.raw,
      _ref$size = _ref.size,
      size = _ref$size === void 0 ? 's' : _ref$size;
  var markdownRenderers = {
    root: function root(_ref2) {
      var children = _ref2.children;
      return React.createElement(_eui.EuiText, {
        "data-test-subj": "markdown-root",
        grow: true,
        size: size
      }, children);
    },
    table: function table(_ref3) {
      var children = _ref3.children;
      return React.createElement("table", {
        "data-test-subj": "markdown-table",
        className: "euiTable euiTable--responsive"
      }, children);
    },
    tableHead: function tableHead(_ref4) {
      var children = _ref4.children;
      return React.createElement(TableHeader, {
        "data-test-subj": "markdown-table-header"
      }, children);
    },
    tableRow: function tableRow(_ref5) {
      var children = _ref5.children;
      return React.createElement(_eui.EuiTableRow, {
        "data-test-subj": "markdown-table-row"
      }, children);
    },
    tableCell: function tableCell(_ref6) {
      var children = _ref6.children;
      return React.createElement(_eui.EuiTableRowCell, {
        "data-test-subj": "markdown-table-cell"
      }, children);
    },
    link: function link(_ref7) {
      var children = _ref7.children,
          href = _ref7.href;
      return React.createElement(_eui.EuiToolTip, {
        content: href
      }, React.createElement(_eui.EuiLink, {
        href: href,
        "data-test-subj": "markdown-link",
        rel: "".concat(REL_NOOPENER, " ").concat(REL_NOFOLLOW, " ").concat(REL_NOREFERRER),
        target: "_blank"
      }, children));
    }
  };
  return React.createElement(_reactMarkdown.default, {
    "data-test-subj": "markdown",
    linkTarget: "_blank",
    renderers: markdownRenderers,
    source: raw
  });
});
exports.Markdown = Markdown;
Markdown.displayName = 'Markdown';