"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabNavigation = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _classnames = _interopRequireDefault(require("classnames"));

var _track_usage = require("../../../lib/track_usage");

var _helpers = require("../helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiLink {\n    color: inherit !important;\n  }\n\n  &.showBorder {\n    padding: 8px 8px 0;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TabContainer = _styledComponents.default.div(_templateObject());

TabContainer.displayName = 'TabContainer';

var TabNavigation =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(TabNavigation, _React$PureComponent);

  function TabNavigation(props) {
    var _this;

    _classCallCheck(this, TabNavigation);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(TabNavigation).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "mapLocationToTab", function (pageName, tabName) {
      var navTabs = _this.props.navTabs;
      return (0, _fp.getOr)('', 'id', Object.values(navTabs).find(function (item) {
        return tabName === item.id || pageName === item.id;
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "renderTabs", function () {
      var navTabs = _this.props.navTabs;
      return Object.keys(navTabs).map(function (tabName) {
        var tab = (0, _fp.get)(tabName, navTabs);
        return React.createElement(TabContainer, {
          className: (0, _classnames.default)({
            euiTab: true,
            showBorder: _this.props.showBorder
          }),
          key: "navigation-".concat(tab.id)
        }, React.createElement(_eui.EuiLink, {
          "data-test-subj": "navigation-link-".concat(tab.id),
          href: tab.href + (0, _helpers.getSearch)(tab, _this.props)
        }, React.createElement(_eui.EuiTab, {
          "data-href": tab.href,
          "data-test-subj": "navigation-".concat(tab.id),
          disabled: tab.disabled,
          isSelected: _this.state.selectedTabId === tab.id,
          onClick: function onClick() {
            (0, _track_usage.trackUiAction)(_track_usage.METRIC_TYPE.CLICK, "".concat(_track_usage.TELEMETRY_EVENT.TAB_CLICKED).concat(tab.id));
          }
        }, tab.name)));
      });
    });

    var selectedTabId = _this.mapLocationToTab(props.pageName, props.tabName);

    _this.state = {
      selectedTabId: selectedTabId
    };
    return _this;
  }

  _createClass(TabNavigation, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      var selectedTabId = this.mapLocationToTab(nextProps.pageName, nextProps.tabName);

      if (this.state.selectedTabId !== selectedTabId) {
        this.setState(function (prevState) {
          return _objectSpread({}, prevState, {
            selectedTabId: selectedTabId
          });
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$display = this.props.display,
          display = _this$props$display === void 0 ? 'condensed' : _this$props$display;
      return React.createElement(_eui.EuiTabs, {
        display: display,
        size: "m"
      }, this.renderTabs());
    }
  }]);

  return TabNavigation;
}(React.PureComponent);

exports.TabNavigation = TabNavigation;