"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventDetails = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _event_fields_browser = require("./event_fields_browser");

var _json_view = require("./json_view");

var i18n = _interopRequireWildcard(require("./translations"));

var _timeline_context = require("../timeline/timeline_context");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  user-select: none;\n  width: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Details = _styledComponents.default.div(_templateObject(), function (_ref) {
  var width = _ref.width;
  return "".concat(width, "px");
});

Details.displayName = 'Details';

var EventDetails = _react.default.memo(function (_ref2) {
  var browserFields = _ref2.browserFields,
      columnHeaders = _ref2.columnHeaders,
      data = _ref2.data,
      id = _ref2.id,
      view = _ref2.view,
      onUpdateColumns = _ref2.onUpdateColumns,
      onViewSelected = _ref2.onViewSelected,
      timelineId = _ref2.timelineId,
      toggleColumn = _ref2.toggleColumn;
  var width = (0, _timeline_context.useTimelineWidthContext)();
  var tabs = [{
    id: 'table-view',
    name: i18n.TABLE,
    content: _react.default.createElement(_event_fields_browser.EventFieldsBrowser, {
      browserFields: browserFields,
      columnHeaders: columnHeaders,
      data: data,
      eventId: id,
      onUpdateColumns: onUpdateColumns,
      timelineId: timelineId,
      toggleColumn: toggleColumn
    })
  }, {
    id: 'json-view',
    name: i18n.JSON_VIEW,
    content: _react.default.createElement(_json_view.JsonView, {
      data: data
    })
  }];
  return _react.default.createElement(Details, {
    "data-test-subj": "eventDetails",
    width: width
  }, _react.default.createElement(_eui.EuiTabbedContent, {
    tabs: tabs,
    selectedTab: view === 'table-view' ? tabs[0] : tabs[1],
    onTabClick: function onTabClick(e) {
      return onViewSelected(e.id);
    }
  }));
});

exports.EventDetails = EventDetails;
EventDetails.displayName = 'EventDetails';