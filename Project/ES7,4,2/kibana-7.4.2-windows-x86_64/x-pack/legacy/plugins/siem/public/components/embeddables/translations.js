"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ERROR_BUTTON = exports.ERROR_DESCRIPTION = exports.ERROR_TITLE = exports.ERROR_CREATING_EMBEDDABLE = exports.ERROR_CONFIGURING_EMBEDDABLES_API = exports.MAP_TITLE = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MAP_TITLE = _i18n.i18n.translate('xpack.siem.components.embeddables.embeddedMap.embeddablePanelTitle', {
  defaultMessage: 'Source -> Destination Point-to-Point Map'
});

exports.MAP_TITLE = MAP_TITLE;

var ERROR_CONFIGURING_EMBEDDABLES_API = _i18n.i18n.translate('xpack.siem.components.embeddables.embeddedMap.errorConfiguringEmbeddableApiTitle', {
  defaultMessage: 'Error configuring Embeddables API'
});

exports.ERROR_CONFIGURING_EMBEDDABLES_API = ERROR_CONFIGURING_EMBEDDABLES_API;

var ERROR_CREATING_EMBEDDABLE = _i18n.i18n.translate('xpack.siem.components.embeddables.embeddedMap.errorCreatingMapEmbeddableTitle', {
  defaultMessage: 'Error creating Map Embeddable'
});

exports.ERROR_CREATING_EMBEDDABLE = ERROR_CREATING_EMBEDDABLE;

var ERROR_TITLE = _i18n.i18n.translate('xpack.siem.components.embeddables.indexPatternsMissingPrompt.errorTitle', {
  defaultMessage: 'Required Index Patterns Not Configured'
});

exports.ERROR_TITLE = ERROR_TITLE;

var ERROR_DESCRIPTION = _i18n.i18n.translate('xpack.siem.components.embeddables.indexPatternsMissingPrompt.errorDescription', {
  defaultMessage: 'An ECS compliant Kibana index pattern must be configured to view event data on the map. When using beats, you can run the following setup commands to create the required Kibana index patterns, otherwise you can configure them manually within Kibana settings.'
});

exports.ERROR_DESCRIPTION = ERROR_DESCRIPTION;

var ERROR_BUTTON = _i18n.i18n.translate('xpack.siem.components.embeddables.indexPatternsMissingPrompt.errorButtonLabel', {
  defaultMessage: 'Configure index patterns'
});

exports.ERROR_BUTTON = ERROR_BUTTON;