"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FlyoutButton = exports.READY_TO_DROP_CLASS_NAME = exports.NOT_READY_TO_DROP_CLASS_NAME = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _droppable_wrapper = require("../../drag_and_drop/droppable_wrapper");

var _helpers = require("../../drag_and_drop/helpers");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: flex-start;\n  display: flex;\n  flex-direction: row;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow-x: auto;\n  overflow-y: hidden;\n  padding-top: 8px;\n  position: fixed;\n  top: 40%;\n  right: -51px;\n  z-index: ", ";\n  transform: rotate(-90deg);\n  user-select: none;\n\n  button {\n    border-radius: 4px 4px 0 0;\n    box-shadow: none;\n    height: 46px;\n    margin: 1px 0 1px 1px;\n    width: 136px;\n  }\n\n  .euiButton:hover:not(:disabled) {\n    transform: none;\n  }\n\n  .euiButton--primary:enabled {\n    background: ", ";\n    box-shadow: none;\n  }\n\n  .euiButton--primary:enabled:hover,\n  .euiButton--primary:enabled:focus {\n    animation: none;\n    background: ", ";\n    box-shadow: none;\n  }\n\n  .", " & .", " {\n    color: ", ";\n    background: ", ";\n    border: 1px solid ", ";\n    border-bottom: none;\n    text-decoration: none;\n  }\n\n  .", " {\n    color: ", ";\n    background: ", ";\n    border: 1px solid ", ";\n    border-bottom: none;\n    text-decoration: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var NOT_READY_TO_DROP_CLASS_NAME = 'not-ready-to-drop';
exports.NOT_READY_TO_DROP_CLASS_NAME = NOT_READY_TO_DROP_CLASS_NAME;
var READY_TO_DROP_CLASS_NAME = 'ready-to-drop';
exports.READY_TO_DROP_CLASS_NAME = READY_TO_DROP_CLASS_NAME;

var Container = _styledComponents.default.div(_templateObject(), function (_ref) {
  var theme = _ref.theme;
  return theme.eui.euiZLevel9;
}, function (_ref2) {
  var theme = _ref2.theme;
  return theme.eui.euiColorEmptyShade;
}, function (_ref3) {
  var theme = _ref3.theme;
  return theme.eui.euiColorEmptyShade;
}, _helpers.IS_DRAGGING_CLASS_NAME, NOT_READY_TO_DROP_CLASS_NAME, function (_ref4) {
  var theme = _ref4.theme;
  return theme.eui.euiColorSuccess;
}, function (_ref5) {
  var theme = _ref5.theme;
  return "".concat(theme.eui.euiColorSuccess).concat(_helpers.TEN_PERCENT_ALPHA_HEX_SUFFIX, " !important");
}, function (_ref6) {
  var theme = _ref6.theme;
  return theme.eui.euiColorSuccess;
}, READY_TO_DROP_CLASS_NAME, function (_ref7) {
  var theme = _ref7.theme;
  return theme.eui.euiColorSuccess;
}, function (_ref8) {
  var theme = _ref8.theme;
  return "".concat(theme.eui.euiColorSuccess).concat(_helpers.TWENTY_PERCENT_ALPHA_HEX_SUFFIX, " !important");
}, function (_ref9) {
  var theme = _ref9.theme;
  return theme.eui.euiColorSuccess;
});

Container.displayName = 'Container';

var BadgeButtonContainer = _styledComponents.default.div(_templateObject2());

BadgeButtonContainer.displayName = 'BadgeButtonContainer';
var FlyoutButton = React.memo(function (_ref10) {
  var onOpen = _ref10.onOpen,
      show = _ref10.show,
      dataProviders = _ref10.dataProviders,
      timelineId = _ref10.timelineId;
  return show ? React.createElement(Container, {
    onClick: onOpen
  }, React.createElement(_droppable_wrapper.DroppableWrapper, {
    "data-test-subj": "flyout-droppable-wrapper",
    droppableId: "".concat(_helpers.droppableTimelineFlyoutButtonPrefix).concat(timelineId),
    render: function render(_ref11) {
      var isDraggingOver = _ref11.isDraggingOver;
      return React.createElement(BadgeButtonContainer, {
        className: "flyout-overlay",
        "data-test-subj": "flyoutOverlay",
        onClick: onOpen
      }, !isDraggingOver ? React.createElement(_eui.EuiButton, {
        className: NOT_READY_TO_DROP_CLASS_NAME,
        "data-test-subj": "flyout-button-not-ready-to-drop",
        fill: false,
        iconSide: "right",
        iconType: "arrowUp"
      }, i18n.FLYOUT_BUTTON) : React.createElement(_eui.EuiButton, {
        className: READY_TO_DROP_CLASS_NAME,
        "data-test-subj": "flyout-button-ready-to-drop",
        fill: false
      }, React.createElement(_eui.EuiIcon, {
        "data-test-subj": "flyout-button-plus-icon",
        type: "plusInCircleFilled"
      })), React.createElement(_eui.EuiNotificationBadge, {
        color: "accent",
        "data-test-subj": "badge",
        style: {
          left: '-9px',
          position: 'relative',
          top: '-6px',
          transform: 'rotate(90deg)',
          visibility: dataProviders.length !== 0 ? 'inherit' : 'hidden',
          zIndex: 10
        }
      }, dataProviders.length));
    }
  })) : null;
}, function (prevProps, nextProps) {
  return prevProps.show === nextProps.show && prevProps.dataProviders === nextProps.dataProviders && prevProps.timelineId === nextProps.timelineId;
});
exports.FlyoutButton = FlyoutButton;
FlyoutButton.displayName = 'FlyoutButton';