"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FullHeightFlexItem = exports.FullHeightFlexGroup = void 0;

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FullHeightFlexGroup = (0, _styledComponents.default)(_eui.EuiFlexGroup)(_templateObject());
exports.FullHeightFlexGroup = FullHeightFlexGroup;
FullHeightFlexGroup.displayName = 'FullHeightFlexGroup';
var FullHeightFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
exports.FullHeightFlexItem = FullHeightFlexItem;
FullHeightFlexItem.displayName = 'FullHeightFlexItem';