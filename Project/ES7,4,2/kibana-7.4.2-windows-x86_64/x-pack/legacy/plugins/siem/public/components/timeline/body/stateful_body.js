"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StatefulBody = exports.emptyColumnHeaders = void 0;

var _fp = require("lodash/fp");

var _memoizeOne = _interopRequireDefault(require("memoize-one"));

var React = _interopRequireWildcard(require("react"));

var _reactRedux = require("react-redux");

var _store = require("../../../store");

var _helpers = require("./helpers");

var _index = require("./index");

var _renderers = require("./renderers");

var _actions = require("../../../store/actions");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var emptyColumnHeaders = [];
exports.emptyColumnHeaders = emptyColumnHeaders;

var StatefulBodyComponent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(StatefulBodyComponent, _React$Component);

  function StatefulBodyComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, StatefulBodyComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(StatefulBodyComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onAddNoteToEvent", function (_ref) {
      var eventId = _ref.eventId,
          noteId = _ref.noteId;
      return _this.props.addNoteToEvent({
        id: _this.props.id,
        eventId: eventId,
        noteId: noteId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onColumnSorted", function (sorted) {
      _this.props.updateSort({
        id: _this.props.id,
        sort: sorted
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onColumnRemoved", function (columnId) {
      return _this.props.removeColumn({
        id: _this.props.id,
        columnId: columnId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onColumnResized", function (_ref2) {
      var columnId = _ref2.columnId,
          delta = _ref2.delta;
      return _this.props.applyDeltaToColumnWidth({
        id: _this.props.id,
        columnId: columnId,
        delta: delta
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onPinEvent", function (eventId) {
      return _this.props.pinEvent({
        id: _this.props.id,
        eventId: eventId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onUnPinEvent", function (eventId) {
      return _this.props.unPinEvent({
        id: _this.props.id,
        eventId: eventId
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onUpdateNote", function (note) {
      return _this.props.updateNote({
        note: note
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onUpdateColumns", function (columns) {
      return _this.props.updateColumns({
        id: _this.props.id,
        columns: columns
      });
    });

    return _this;
  }

  _createClass(StatefulBodyComponent, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(_ref3) {
      var browserFields = _ref3.browserFields,
          columnHeaders = _ref3.columnHeaders,
          data = _ref3.data,
          eventIdToNoteIds = _ref3.eventIdToNoteIds,
          getNotesByIds = _ref3.getNotesByIds,
          height = _ref3.height,
          id = _ref3.id,
          isEventViewer = _ref3.isEventViewer,
          pinnedEventIds = _ref3.pinnedEventIds,
          range = _ref3.range,
          sort = _ref3.sort;
      return browserFields !== this.props.browserFields || columnHeaders !== this.props.columnHeaders || data !== this.props.data || eventIdToNoteIds !== this.props.eventIdToNoteIds || getNotesByIds !== this.props.getNotesByIds || height !== this.props.height || id !== this.props.id || isEventViewer !== this.props.isEventViewer || pinnedEventIds !== this.props.pinnedEventIds || range !== this.props.range || sort !== this.props.sort;
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          browserFields = _this$props.browserFields,
          columnHeaders = _this$props.columnHeaders,
          data = _this$props.data,
          eventIdToNoteIds = _this$props.eventIdToNoteIds,
          getNotesByIds = _this$props.getNotesByIds,
          height = _this$props.height,
          id = _this$props.id,
          _this$props$isEventVi = _this$props.isEventViewer,
          isEventViewer = _this$props$isEventVi === void 0 ? false : _this$props$isEventVi,
          pinnedEventIds = _this$props.pinnedEventIds,
          range = _this$props.range,
          sort = _this$props.sort,
          toggleColumn = _this$props.toggleColumn;
      return React.createElement(_index.Body, {
        addNoteToEvent: this.onAddNoteToEvent,
        browserFields: browserFields,
        columnHeaders: columnHeaders || emptyColumnHeaders,
        columnRenderers: _renderers.columnRenderers,
        data: data,
        eventIdToNoteIds: eventIdToNoteIds,
        getNotesByIds: getNotesByIds,
        height: height,
        id: id,
        isEventViewer: isEventViewer,
        onColumnResized: this.onColumnResized,
        onColumnRemoved: this.onColumnRemoved,
        onColumnSorted: this.onColumnSorted,
        onFilterChange: _fp.noop // TODO: this is the callback for column filters, which is out scope for this phase of delivery
        ,
        onPinEvent: this.onPinEvent,
        onUpdateColumns: this.onUpdateColumns,
        onUnPinEvent: this.onUnPinEvent,
        pinnedEventIds: pinnedEventIds,
        range: range,
        rowRenderers: _renderers.rowRenderers,
        sort: sort,
        toggleColumn: toggleColumn,
        updateNote: this.onUpdateNote
      });
    }
  }]);

  return StatefulBodyComponent;
}(React.Component);

var makeMapStateToProps = function makeMapStateToProps() {
  var memoizedColumnHeaders = (0, _memoizeOne.default)(_helpers.getColumnHeaders);

  var getTimeline = _store.timelineSelectors.getTimelineByIdSelector();

  var getNotesByIds = _store.appSelectors.notesByIdsSelector();

  var mapStateToProps = function mapStateToProps(state, _ref4) {
    var browserFields = _ref4.browserFields,
        id = _ref4.id;
    var timeline = getTimeline(state, id);
    var columns = timeline.columns,
        eventIdToNoteIds = timeline.eventIdToNoteIds,
        pinnedEventIds = timeline.pinnedEventIds;
    return {
      columnHeaders: memoizedColumnHeaders(columns, browserFields),
      id: id,
      eventIdToNoteIds: eventIdToNoteIds,
      getNotesByIds: getNotesByIds(state),
      pinnedEventIds: pinnedEventIds
    };
  };

  return mapStateToProps;
};

var StatefulBody = (0, _reactRedux.connect)(makeMapStateToProps, {
  addNoteToEvent: _actions.timelineActions.addNoteToEvent,
  applyDeltaToColumnWidth: _actions.timelineActions.applyDeltaToColumnWidth,
  unPinEvent: _actions.timelineActions.unPinEvent,
  updateColumns: _actions.timelineActions.updateColumns,
  updateSort: _actions.timelineActions.updateSort,
  pinEvent: _actions.timelineActions.pinEvent,
  removeColumn: _actions.timelineActions.removeColumn,
  removeProvider: _actions.timelineActions.removeProvider,
  updateNote: _actions.appActions.updateNote
})(StatefulBodyComponent);
exports.StatefulBody = StatefulBody;