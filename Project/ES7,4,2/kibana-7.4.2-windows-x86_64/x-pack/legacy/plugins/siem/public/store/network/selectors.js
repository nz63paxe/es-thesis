"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usersSelector = exports.tlsSelector = exports.domainsSelector = exports.ipDetailsFlowTargetSelector = exports.isNetworkFilterQueryDraftValid = exports.networkFilterQueryDraft = exports.networkFilterQueryAsKuery = exports.networkFilterExpression = exports.networkFilterQueryAsJson = exports.topNFlowSelector = exports.NetworkTableType = exports.dnsSelector = void 0;

var _fp = require("lodash/fp");

var _reselect = require("reselect");

var _keury = require("../../lib/keury");

var _types = require("../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var selectNetworkPage = function selectNetworkPage(state) {
  return state.network.page;
};

var selectNetworkDetails = function selectNetworkDetails(state) {
  return state.network.details;
};

var selectNetworkByType = function selectNetworkByType(state, networkType) {
  return (0, _fp.get)(networkType, state.network);
}; // Network Page Selectors


var dnsSelector = function dnsSelector() {
  return (0, _reselect.createSelector)(selectNetworkPage, function (network) {
    return network.queries.dns;
  });
};

exports.dnsSelector = dnsSelector;
var NetworkTableType;
exports.NetworkTableType = NetworkTableType;

(function (NetworkTableType) {
  NetworkTableType["dns"] = "dns";
  NetworkTableType["topNFlowSource"] = "topNFlowSource";
  NetworkTableType["topNFlowDestination"] = "topNFlowDestination";
})(NetworkTableType || (exports.NetworkTableType = NetworkTableType = {}));

var topNFlowSelector = function topNFlowSelector(flowTarget) {
  return (0, _reselect.createSelector)(selectNetworkPage, function (network) {
    return flowTarget === _types.FlowTargetNew.source ? network.queries[NetworkTableType.topNFlowSource] : network.queries[NetworkTableType.topNFlowDestination];
  });
}; // Filter Query Selectors


exports.topNFlowSelector = topNFlowSelector;

var networkFilterQueryAsJson = function networkFilterQueryAsJson() {
  return (0, _reselect.createSelector)(selectNetworkByType, function (network) {
    return network.filterQuery ? network.filterQuery.serializedQuery : null;
  });
};

exports.networkFilterQueryAsJson = networkFilterQueryAsJson;

var networkFilterExpression = function networkFilterExpression() {
  return (0, _reselect.createSelector)(selectNetworkByType, function (network) {
    return network.filterQuery && network.filterQuery.kuery ? network.filterQuery.kuery.expression : null;
  });
};

exports.networkFilterExpression = networkFilterExpression;

var networkFilterQueryAsKuery = function networkFilterQueryAsKuery() {
  return (0, _reselect.createSelector)(selectNetworkByType, function (network) {
    return network.filterQuery && network.filterQuery.kuery ? network.filterQuery.kuery : null;
  });
};

exports.networkFilterQueryAsKuery = networkFilterQueryAsKuery;

var networkFilterQueryDraft = function networkFilterQueryDraft() {
  return (0, _reselect.createSelector)(selectNetworkByType, function (network) {
    return network.filterQueryDraft;
  });
};

exports.networkFilterQueryDraft = networkFilterQueryDraft;

var isNetworkFilterQueryDraftValid = function isNetworkFilterQueryDraftValid() {
  return (0, _reselect.createSelector)(selectNetworkByType, function (network) {
    return (0, _keury.isFromKueryExpressionValid)(network.filterQueryDraft);
  });
}; // IP Details Selectors


exports.isNetworkFilterQueryDraftValid = isNetworkFilterQueryDraftValid;

var ipDetailsFlowTargetSelector = function ipDetailsFlowTargetSelector() {
  return (0, _reselect.createSelector)(selectNetworkDetails, function (network) {
    return network.flowTarget;
  });
};

exports.ipDetailsFlowTargetSelector = ipDetailsFlowTargetSelector;

var domainsSelector = function domainsSelector() {
  return (0, _reselect.createSelector)(selectNetworkDetails, function (network) {
    return network.queries.domains;
  });
};

exports.domainsSelector = domainsSelector;

var tlsSelector = function tlsSelector() {
  return (0, _reselect.createSelector)(selectNetworkDetails, function (network) {
    return network.queries.tls;
  });
};

exports.tlsSelector = tlsSelector;

var usersSelector = function usersSelector() {
  return (0, _reselect.createSelector)(selectNetworkDetails, function (network) {
    return network.queries.users;
  });
};

exports.usersSelector = usersSelector;