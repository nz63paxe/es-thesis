"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Row = exports.TokensFlexItem = exports.Details = exports.getValues = exports.findItem = exports.deleteItemIdx = void 0;

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n  overflow: hidden;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-left: 3px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: 5px 0 5px 10px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var deleteItemIdx = function deleteItemIdx(data, idx) {
  return [].concat(_toConsumableArray(data.slice(0, idx)), _toConsumableArray(data.slice(idx + 1)));
};

exports.deleteItemIdx = deleteItemIdx;

var findItem = function findItem(data, field) {
  return data.findIndex(function (d) {
    return d.field === field;
  });
};

exports.findItem = findItem;

var getValues = function getValues(field, data) {
  var obj = data.find(function (d) {
    return d.field === field;
  });

  if (obj != null && obj.value != null) {
    return obj.value;
  }

  return undefined;
};

exports.getValues = getValues;

var Details = _styledComponents.default.div(_templateObject());

exports.Details = Details;
Details.displayName = 'Details';
var TokensFlexItem = (0, _styledComponents.default)(_eui.EuiFlexItem)(_templateObject2());
exports.TokensFlexItem = TokensFlexItem;
TokensFlexItem.displayName = 'TokensFlexItem';

var Row = _styledComponents.default.div(_templateObject3());

exports.Row = Row;
Row.displayName = 'Row';