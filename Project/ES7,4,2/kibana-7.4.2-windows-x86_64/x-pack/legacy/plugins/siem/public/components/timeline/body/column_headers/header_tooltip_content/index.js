"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HeaderToolTipContent = void 0;

var _eui = require("@elastic/eui");

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _helpers = require("../../../../event_details/helpers");

var i18n = _interopRequireWildcard(require("../translations"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  word-wrap: break-word;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 5px;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: 5px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-right: 3px;\n  position: relative;\n  top: -2px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var IconType = (0, _styledComponents.default)(_eui.EuiIcon)(_templateObject());
IconType.displayName = 'IconType';

var P = _styledComponents.default.p(_templateObject2());

P.displayName = 'P';

var ToolTipTableMetadata = _styledComponents.default.span(_templateObject3());

ToolTipTableMetadata.displayName = 'ToolTipTableMetadata';

var ToolTipTableValue = _styledComponents.default.span(_templateObject4());

ToolTipTableValue.displayName = 'ToolTipTableValue';
var HeaderToolTipContent = (0, _recompose.pure)(function (_ref) {
  var header = _ref.header;
  return React.createElement(React.Fragment, null, !(0, _fp.isEmpty)(header.category) ? React.createElement(P, null, React.createElement(ToolTipTableMetadata, {
    "data-test-subj": "category"
  }, i18n.CATEGORY, ':'), React.createElement(ToolTipTableValue, {
    "data-test-subj": "category-value"
  }, header.category)) : null, React.createElement(P, null, React.createElement(ToolTipTableMetadata, {
    "data-test-subj": "field"
  }, i18n.FIELD, ':'), React.createElement(ToolTipTableValue, {
    "data-test-subj": "field-value"
  }, header.id)), React.createElement(P, null, React.createElement(ToolTipTableMetadata, {
    "data-test-subj": "type"
  }, i18n.TYPE, ':'), React.createElement(ToolTipTableValue, null, React.createElement(IconType, {
    "data-test-subj": "type-icon",
    type: (0, _helpers.getIconFromType)(header.type)
  }), React.createElement("span", {
    "data-test-subj": "type-value"
  }, header.type))), !(0, _fp.isEmpty)(header.description) ? React.createElement(P, null, React.createElement(ToolTipTableMetadata, {
    "data-test-subj": "description"
  }, i18n.DESCRIPTION, ':'), React.createElement(ToolTipTableValue, {
    "data-test-subj": "description-value"
  }, header.description)) : null);
});
exports.HeaderToolTipContent = HeaderToolTipContent;
HeaderToolTipContent.displayName = 'HeaderToolTipContent';