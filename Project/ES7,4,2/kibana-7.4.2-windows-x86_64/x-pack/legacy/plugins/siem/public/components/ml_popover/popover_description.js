"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PopoverDescription = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _chrome = _interopRequireDefault(require("ui/chrome"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var PopoverDescription = _react2.default.memo(function () {
  return _react2.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.siem.components.mlPopup.anomalyDetectionDescription",
    defaultMessage: "Run any of the Machine Learning jobs below to view anomalous events throughout the SIEM application. We\u2019ve provided a few common detection jobs to get you started. If you wish to add your own custom jobs, simply create and tag them with \u201CSIEM\u201D from the {machineLearning} application for inclusion here.",
    values: {
      machineLearning: _react2.default.createElement(_eui.EuiLink, {
        href: "".concat(_chrome.default.getBasePath(), "/app/ml"),
        target: "_blank"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.siem.components.mlPopup.machineLearningLink",
        defaultMessage: "Machine Learning"
      }))
    }
  }));
});

exports.PopoverDescription = PopoverDescription;
PopoverDescription.displayName = 'PopoverDescription';