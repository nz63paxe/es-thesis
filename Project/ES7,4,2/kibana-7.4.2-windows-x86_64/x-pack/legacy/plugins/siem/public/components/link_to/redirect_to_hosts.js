"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTabsOnHostDetailsUrl = exports.getHostDetailsUrl = exports.getTabsOnHostsUrl = exports.getHostsUrl = exports.RedirectToHostDetailsPage = exports.RedirectToHostsPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _redirect_wrapper = require("./redirect_wrapper");

var _model = require("../../store/hosts/model");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RedirectToHostsPage = function RedirectToHostsPage(_ref) {
  var tabName = _ref.match.params.tabName,
      search = _ref.location.search;
  var defaultSelectedTab = _model.HostsTableType.hosts;
  var selectedTab = tabName ? tabName : defaultSelectedTab;
  var to = "/hosts/".concat(selectedTab).concat(search);
  return _react.default.createElement(_redirect_wrapper.RedirectWrapper, {
    to: to
  });
};

exports.RedirectToHostsPage = RedirectToHostsPage;

var RedirectToHostDetailsPage = function RedirectToHostDetailsPage(_ref2) {
  var _ref2$match$params = _ref2.match.params,
      detailName = _ref2$match$params.detailName,
      tabName = _ref2$match$params.tabName,
      search = _ref2.location.search;
  var defaultSelectedTab = _model.HostsTableType.authentications;
  var selectedTab = tabName ? tabName : defaultSelectedTab;
  var to = "/hosts/".concat(detailName, "/").concat(selectedTab).concat(search);
  return _react.default.createElement(_redirect_wrapper.RedirectWrapper, {
    to: to
  });
};

exports.RedirectToHostDetailsPage = RedirectToHostDetailsPage;

var getHostsUrl = function getHostsUrl() {
  return '#/link-to/hosts';
};

exports.getHostsUrl = getHostsUrl;

var getTabsOnHostsUrl = function getTabsOnHostsUrl(tabName) {
  return "#/link-to/hosts/".concat(tabName);
};

exports.getTabsOnHostsUrl = getTabsOnHostsUrl;

var getHostDetailsUrl = function getHostDetailsUrl(detailName) {
  return "#/link-to/hosts/".concat(detailName);
};

exports.getHostDetailsUrl = getHostDetailsUrl;

var getTabsOnHostDetailsUrl = function getTabsOnHostDetailsUrl(detailName, tabName) {
  return "#/link-to/hosts/".concat(detailName, "/").concat(tabName);
};

exports.getTabsOnHostDetailsUrl = getTabsOnHostDetailsUrl;