"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ProcessDraggableWithNonExistentProcess = exports.ProcessDraggable = exports.isNillOrEmptyString = void 0;

var _fp = require("lodash/fp");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _draggables = require("../../../draggables");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var isNillOrEmptyString = function isNillOrEmptyString(value) {
  if (value == null) {
    return true;
  } else if ((0, _fp.isString)(value)) {
    return value === '';
  } else if ((0, _fp.isNumber)(value)) {
    return !isFinite(value);
  }
};

exports.isNillOrEmptyString = isNillOrEmptyString;
var ProcessDraggable = (0, _recompose.pure)(function (_ref) {
  var contextId = _ref.contextId,
      eventId = _ref.eventId,
      processExecutable = _ref.processExecutable,
      processName = _ref.processName,
      processPid = _ref.processPid;

  if (!isNillOrEmptyString(processName) || processName === '' && isNillOrEmptyString(processExecutable) && isNillOrEmptyString(processPid)) {
    return React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      eventId: eventId,
      field: "process.name",
      value: processName,
      iconType: "console"
    });
  } else if (!isNillOrEmptyString(processExecutable) || processExecutable === '' && isNillOrEmptyString(processPid)) {
    return React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      eventId: eventId,
      field: "process.executable",
      value: processExecutable,
      iconType: "console"
    });
  } else if (processPid != null) {
    return React.createElement(_draggables.DraggableBadge, {
      contextId: contextId,
      eventId: eventId,
      field: "process.pid",
      value: String(processPid),
      iconType: "number"
    });
  } else {
    return null;
  }
});
exports.ProcessDraggable = ProcessDraggable;
ProcessDraggable.displayName = 'ProcessDraggable';
var ProcessDraggableWithNonExistentProcess = (0, _recompose.pure)(function (_ref2) {
  var contextId = _ref2.contextId,
      eventId = _ref2.eventId,
      processExecutable = _ref2.processExecutable,
      processName = _ref2.processName,
      processPid = _ref2.processPid;

  if (processExecutable == null && processName == null && processPid == null) {
    return React.createElement(React.Fragment, null, i18n.NON_EXISTENT);
  } else {
    return React.createElement(ProcessDraggable, {
      contextId: contextId,
      eventId: eventId,
      processExecutable: processExecutable,
      processName: processName,
      processPid: processPid
    });
  }
});
exports.ProcessDraggableWithNonExistentProcess = ProcessDraggableWithNonExistentProcess;
ProcessDraggableWithNonExistentProcess.displayName = 'ProcessDraggableWithNonExistentProcess';