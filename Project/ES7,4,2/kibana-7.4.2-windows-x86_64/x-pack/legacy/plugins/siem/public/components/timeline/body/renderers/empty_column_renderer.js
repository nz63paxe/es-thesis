"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.emptyColumnRenderer = exports.dataNotExistsAtColumn = void 0;

var React = _interopRequireWildcard(require("react"));

var _draggable_wrapper = require("../../../drag_and_drop/draggable_wrapper");

var _helpers = require("../../../drag_and_drop/helpers");

var _parse_query_value = require("./parse_query_value");

var _data_provider = require("../../data_providers/data_provider");

var _provider = require("../../data_providers/provider");

var _empty_value = require("../../../empty_value");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var dataNotExistsAtColumn = function dataNotExistsAtColumn(columnName, data) {
  return data.findIndex(function (item) {
    return item.field === columnName;
  }) === -1;
};

exports.dataNotExistsAtColumn = dataNotExistsAtColumn;
var emptyColumnRenderer = {
  isInstance: function isInstance(columnName, data) {
    return dataNotExistsAtColumn(columnName, data);
  },
  renderColumn: function renderColumn(_ref) {
    var columnName = _ref.columnName,
        eventId = _ref.eventId,
        field = _ref.field,
        width = _ref.width,
        timelineId = _ref.timelineId;
    return React.createElement(_draggable_wrapper.DraggableWrapper, {
      key: "empty-column-renderer-draggable-wrapper-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id),
      dataProvider: {
        enabled: true,
        id: (0, _helpers.escapeDataProviderId)("empty-column-renderer-draggable-wrapper-".concat(timelineId, "-").concat(columnName, "-").concat(eventId, "-").concat(field.id)),
        name: "".concat(columnName, ": ").concat((0, _parse_query_value.parseQueryValue)(null)),
        queryMatch: {
          field: field.id,
          value: (0, _parse_query_value.parseQueryValue)(null),
          displayValue: (0, _empty_value.getEmptyValue)(),
          operator: _data_provider.EXISTS_OPERATOR
        },
        excluded: true,
        kqlQuery: '',
        and: []
      },
      render: function render(dataProvider, _, snapshot) {
        return snapshot.isDragging ? React.createElement(_draggable_wrapper.DragEffects, null, React.createElement(_provider.Provider, {
          dataProvider: dataProvider
        })) : React.createElement("span", null, (0, _empty_value.getEmptyValue)());
      },
      width: width
    });
  }
};
exports.emptyColumnRenderer = emptyColumnRenderer;