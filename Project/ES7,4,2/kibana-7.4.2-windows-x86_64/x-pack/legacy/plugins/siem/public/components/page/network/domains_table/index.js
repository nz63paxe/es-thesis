"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DomainsTable = exports.DomainsTableId = void 0;

var _fp = require("lodash/fp");

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _actions = require("../../../../store/actions");

var _types = require("../../../../graphql/types");

var _store = require("../../../../store");

var _flow_direction_select = require("../../../flow_controls/flow_direction_select");

var _paginated_table = require("../../../paginated_table");

var _columns = require("./columns");

var i18n = _interopRequireWildcard(require("./translations"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var tableType = _store.networkModel.IpDetailsTableType.domains;
var rowItems = [{
  text: i18n.ROWS_5,
  numberOfRow: 5
}, {
  text: i18n.ROWS_10,
  numberOfRow: 10
}];
var DomainsTableId = 'domains-table';
exports.DomainsTableId = DomainsTableId;

var DomainsTableComponent =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(DomainsTableComponent, _React$PureComponent);

  function DomainsTableComponent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, DomainsTableComponent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(DomainsTableComponent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onChange", function (criteria) {
      if (criteria.sort != null) {
        var splitField = criteria.sort.field.split('.');
        var newDomainsSort = {
          field: getSortFromString(splitField[splitField.length - 1]),
          direction: criteria.sort.direction
        };

        if (!(0, _fp.isEqual)(newDomainsSort, _this.props.domainsSortField)) {
          _this.props.updateDomainsSort({
            domainsSortField: newDomainsSort,
            networkType: _this.props.type
          });
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeDomainsDirection", function (flowDirection) {
      return _this.props.updateDomainsDirection({
        flowDirection: flowDirection,
        networkType: _this.props.type
      });
    });

    return _this;
  }

  _createClass(DomainsTableComponent, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          activePage = _this$props.activePage,
          data = _this$props.data,
          domainsSortField = _this$props.domainsSortField,
          fakeTotalCount = _this$props.fakeTotalCount,
          flowDirection = _this$props.flowDirection,
          flowTarget = _this$props.flowTarget,
          id = _this$props.id,
          indexPattern = _this$props.indexPattern,
          ip = _this$props.ip,
          isInspect = _this$props.isInspect,
          limit = _this$props.limit,
          loading = _this$props.loading,
          _loadPage = _this$props.loadPage,
          showMorePagesIndicator = _this$props.showMorePagesIndicator,
          totalCount = _this$props.totalCount,
          type = _this$props.type,
          updateDomainsLimit = _this$props.updateDomainsLimit,
          updateTableActivePage = _this$props.updateTableActivePage;
      return _react.default.createElement(_paginated_table.PaginatedTable, {
        activePage: activePage,
        columns: (0, _columns.getDomainsColumns)(indexPattern, ip, flowDirection, flowTarget, type, DomainsTableId),
        showMorePagesIndicator: showMorePagesIndicator,
        headerCount: totalCount,
        headerSupplement: _react.default.createElement(_flow_direction_select.FlowDirectionSelect, {
          selectedDirection: flowDirection,
          onChangeDirection: this.onChangeDomainsDirection
        }),
        headerTitle: i18n.DOMAINS,
        headerUnit: i18n.UNIT(totalCount),
        id: id,
        isInspect: isInspect,
        itemsPerRow: rowItems,
        limit: limit,
        loading: loading,
        loadPage: function loadPage(newActivePage) {
          return _loadPage(newActivePage);
        },
        onChange: this.onChange,
        pageOfItems: data,
        sorting: getSortField(domainsSortField, flowTarget),
        totalCount: fakeTotalCount,
        updateActivePage: function updateActivePage(newPage) {
          return updateTableActivePage({
            activePage: newPage,
            tableType: tableType
          });
        },
        updateLimitPagination: function updateLimitPagination(newLimit) {
          return updateDomainsLimit({
            limit: newLimit,
            networkType: type
          });
        }
      });
    }
  }]);

  return DomainsTableComponent;
}(_react.default.PureComponent);

var makeMapStateToProps = function makeMapStateToProps() {
  var getDomainsSelector = _store.networkSelectors.domainsSelector();

  var mapStateToProps = function mapStateToProps(state) {
    return _objectSpread({}, getDomainsSelector(state));
  };

  return mapStateToProps;
};

var DomainsTable = (0, _reactRedux.connect)(makeMapStateToProps, {
  updateDomainsLimit: _actions.networkActions.updateDomainsLimit,
  updateDomainsDirection: _actions.networkActions.updateDomainsFlowDirection,
  updateDomainsSort: _actions.networkActions.updateDomainsSort,
  updateTableActivePage: _actions.networkActions.updateIpDetailsTableActivePage
})(DomainsTableComponent);
exports.DomainsTable = DomainsTable;

var getSortField = function getSortField(sortField, flowTarget) {
  switch (sortField.field) {
    case _types.DomainsFields.domainName:
      return {
        field: "node.".concat(flowTarget, ".").concat(sortField.field),
        direction: sortField.direction
      };

    case _types.DomainsFields.bytes:
      return {
        field: "node.network.".concat(sortField.field),
        direction: sortField.direction
      };

    case _types.DomainsFields.packets:
      return {
        field: "node.network.".concat(sortField.field),
        direction: sortField.direction
      };

    case _types.DomainsFields.uniqueIpCount:
      return {
        field: "node.".concat(flowTarget, ".").concat(sortField.field),
        direction: sortField.direction
      };

    default:
      return {
        field: 'node.network.bytes',
        direction: _types.Direction.desc
      };
  }
};

var getSortFromString = function getSortFromString(sortField) {
  switch (sortField) {
    case _types.DomainsFields.domainName.valueOf():
      return _types.DomainsFields.domainName;

    case _types.DomainsFields.bytes.valueOf():
      return _types.DomainsFields.bytes;

    case _types.DomainsFields.packets.valueOf():
      return _types.DomainsFields.packets;

    case _types.DomainsFields.uniqueIpCount.valueOf():
      return _types.DomainsFields.uniqueIpCount;

    default:
      return _types.DomainsFields.bytes;
  }
};