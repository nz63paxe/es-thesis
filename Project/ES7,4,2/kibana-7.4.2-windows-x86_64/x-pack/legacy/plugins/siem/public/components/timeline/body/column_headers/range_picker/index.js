"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RangePicker = exports.rangePickerWidth = void 0;

var _eui = require("@elastic/eui");

var React = _interopRequireWildcard(require("react"));

var _recompose = require("recompose");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _ranges = require("./ranges");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  cursor: pointer;\n  width: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var rangePickerWidth = 120; // TODO: Upgrade Eui library and use EuiSuperSelect

exports.rangePickerWidth = rangePickerWidth;

var SelectContainer = _styledComponents.default.div(_templateObject(), rangePickerWidth);

SelectContainer.displayName = 'SelectContainer';
/** Renders a time range picker for the MiniMap (e.g. 1 Day, 1 Week...) */

var RangePicker = (0, _recompose.pure)(function (_ref) {
  var selected = _ref.selected,
      onRangeSelected = _ref.onRangeSelected;

  var onChange = function onChange(event) {
    onRangeSelected(event.target.value);
  };

  return React.createElement(SelectContainer, null, React.createElement(_eui.EuiSelect, {
    "data-test-subj": "rangePicker",
    value: selected,
    options: _ranges.Ranges.map(function (range) {
      return {
        text: range
      };
    }),
    onChange: onChange
  }));
});
exports.RangePicker = RangePicker;
RangePicker.displayName = 'RangePicker';