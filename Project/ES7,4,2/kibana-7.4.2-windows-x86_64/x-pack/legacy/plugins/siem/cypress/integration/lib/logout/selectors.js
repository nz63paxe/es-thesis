"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOGOUT_LINK = exports.USER_MENU = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The avatar / button that represents the logged-in Kibana user */
const USER_MENU = '[data-test-subj="userMenuButton"]';
/** Clicking this link logs out the currently logged-in Kibana user */

exports.USER_MENU = USER_MENU;
const LOGOUT_LINK = '[data-test-subj="logoutLink"]';
exports.LOGOUT_LINK = LOGOUT_LINK;