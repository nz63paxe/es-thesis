"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stubApi = exports.clearFetch = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Cypress workaround to hijack XHR
// https://github.com/cypress-io/cypress/issues/687
const clearFetch = () => cy.on('window:before:load', win => {
  // @ts-ignore no null, this is a temp hack see issue above
  win.fetch = null;
});

exports.clearFetch = clearFetch;

const stubApi = dataFileName => {
  cy.server();
  cy.fixture(dataFileName).as(`${dataFileName}JSON`);
  cy.route('POST', 'api/siem/graphql', `@${dataFileName}JSON`);
};

exports.stubApi = stubApi;