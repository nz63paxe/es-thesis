"use strict";

var _logout = require("../../lib/logout");

var _url_state = require("../../lib/url_state");

var _helpers = require("../../lib/util/helpers");

var _helpers2 = require("../../lib/timeline/helpers");

var _selectors = require("../../lib/navigation/selectors");

var _urls = require("../../lib/urls");

var _helpers3 = require("../../lib/hosts/helpers");

var _selectors2 = require("../../lib/hosts/selectors");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('url state', () => {
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('sets the global start and end dates from the url', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.url);
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.startTimeFormat);
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.endTimeFormat);
  });
  it('sets the url state when start and end date are set', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.url);
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_TAB).first().click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_INPUT, {
      timeout: 5000
    }).type(`{selectall}{backspace}${_url_state.ABSOLUTE_DATE_RANGE.newStartTimeTyped}`);
    cy.get(_url_state.DATE_PICKER_APPLY_BUTTON).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_TAB).first().click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_INPUT, {
      timeout: 5000
    }).type(`{selectall}{backspace}${_url_state.ABSOLUTE_DATE_RANGE.newEndTimeTyped}`);
    cy.get(_url_state.DATE_PICKER_APPLY_BUTTON).click({
      force: true
    });
    cy.url().should('include', `(global:(linkTo:!(timeline),timerange:(from:${new Date(_url_state.ABSOLUTE_DATE_RANGE.newStartTimeTyped).valueOf()},kind:absolute,to:${new Date(_url_state.ABSOLUTE_DATE_RANGE.newEndTimeTyped).valueOf()}))`);
  });
  it('sets the timeline start and end dates from the url when locked to global time', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.url);
    (0, _helpers2.toggleTimelineVisibility)();
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON_TIMELINE).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.startTimeFormat);
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON_TIMELINE).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.endTimeFormat);
  });
  it('sets the timeline start and end dates independently of the global start and end dates when times are unlocked', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlUnlinked);
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.startTimeFormat);
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.endTimeFormat);
    (0, _helpers2.toggleTimelineVisibility)();
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON_TIMELINE).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.startTimeTimelineFormat);
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON_TIMELINE).should('have.attr', 'title', _url_state.ABSOLUTE_DATE_RANGE.endTimeTimelineFormat);
  });
  it('sets the url state when timeline/global date pickers are unlinked and timeline start and end date are set', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlUnlinked);
    (0, _helpers2.toggleTimelineVisibility)();
    cy.get(_url_state.DATE_PICKER_START_DATE_POPOVER_BUTTON_TIMELINE, {
      timeout: _helpers.DEFAULT_TIMEOUT
    }).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_TAB).first().click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_INPUT, {
      timeout: 5000
    }).type(`{selectall}{backspace}${_url_state.ABSOLUTE_DATE_RANGE.newStartTimeTyped}`);
    cy.get(_url_state.DATE_PICKER_APPLY_BUTTON_TIMELINE).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_END_DATE_POPOVER_BUTTON_TIMELINE).click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_TAB).first().click({
      force: true
    });
    cy.get(_url_state.DATE_PICKER_ABSOLUTE_INPUT, {
      timeout: 5000
    }).type(`{selectall}{backspace}${_url_state.ABSOLUTE_DATE_RANGE.newEndTimeTyped}{enter}`);
    cy.get(_url_state.DATE_PICKER_APPLY_BUTTON_TIMELINE).click({
      force: true
    });
    cy.url().should('include', `timeline:(linkTo:!(),timerange:(from:${new Date(_url_state.ABSOLUTE_DATE_RANGE.newStartTimeTyped).valueOf()},kind:absolute,to:${new Date(_url_state.ABSOLUTE_DATE_RANGE.newEndTimeTyped).valueOf()}))`);
  });
  it('sets kql on network page when queryLocation == network.page', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlKqlNetworkNetwork);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', 'source.ip: "10.142.0.9"');
  });
  it('does not set kql on network page when queryLocation != network.page', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlKqlNetworkHosts);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '');
  });
  it('sets kql on hosts page when queryLocation == hosts.page', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlKqlHostsHosts);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', 'source.ip: "10.142.0.9"');
  });
  it('does not set kql on hosts page when queryLocation != hosts.page', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlKqlHostsNetwork);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '');
  });
  it('sets the url state when kql is set', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.url);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).type('source.ip: "10.142.0.9" {enter}');
    cy.url().should('include', `kqlQuery=(filterQuery:(expression:'source.ip:%20%2210.142.0.9%22%20',kind:kuery),queryLocation:network.page)`);
  });
  it('sets the url state when kql is set and check if href reflect this change', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.url);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).type('source.ip: "10.142.0.9" {enter}');
    cy.get(_selectors.NAVIGATION_HOSTS).first().click({
      force: true
    });
    cy.get(_selectors.NAVIGATION_NETWORK).should('have.attr', 'href', "#/link-to/network?kqlQuery=(filterQuery:(expression:'source.ip:%20%2210.142.0.9%22%20',kind:kuery),queryLocation:network.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))");
  });
  it('sets KQL in host page and detail page and check if href match on breadcrumb, tabs and subTabs', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlHost);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).type('host.name: "siem-kibana" {enter}');
    cy.get(_selectors2.NAVIGATION_HOSTS_ALL_HOSTS, {
      timeout: 5000
    }).first().click({
      force: true
    });
    (0, _helpers3.waitForAllHostsWidget)();
    cy.get(_url_state.HOST_DETAIL_SIEM_KIBANA, {
      timeout: 5000
    }).first().invoke('text').should('eq', 'siem-kibana');
    cy.get(_url_state.HOST_DETAIL_SIEM_KIBANA).first().click({
      force: true
    });
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).type('agent.type: "auditbeat" {enter}');
    cy.get(_selectors.NAVIGATION_HOSTS).should('have.attr', 'href', "#/link-to/hosts?kqlQuery=(filterQuery:(expression:'host.name:%20%22siem-kibana%22%20',kind:kuery),queryLocation:hosts.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))");
    cy.get(_selectors.NAVIGATION_NETWORK).should('have.attr', 'href', '#/link-to/network?timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))');
    cy.get(_selectors2.NAVIGATION_HOSTS_ANOMALIES).should('have.attr', 'href', "#/hosts/siem-kibana/anomalies?kqlQuery=(filterQuery:(expression:'agent.type:%20%22auditbeat%22%20',kind:kuery),queryLocation:hosts.details)&timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))");
    cy.get(_url_state.BREADCRUMBS).eq(1).should('have.attr', 'href', "#/link-to/hosts?kqlQuery=(filterQuery:(expression:'host.name:%20%22siem-kibana%22%20',kind:kuery),queryLocation:hosts.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))");
    cy.get(_url_state.BREADCRUMBS).eq(2).should('have.attr', 'href', "#/link-to/hosts/siem-kibana?kqlQuery=(filterQuery:(expression:'agent.type:%20%22auditbeat%22%20',kind:kuery),queryLocation:hosts.details)&timerange=(global:(linkTo:!(timeline),timerange:(from:1564689809186,kind:absolute,to:1564691609186)),timeline:(linkTo:!(global),timerange:(from:1564689809186,kind:absolute,to:1564691609186)))");
  });
  it('clears kql when navigating to a new page', () => {
    (0, _helpers.loginAndWaitForPage)(_url_state.ABSOLUTE_DATE_RANGE.urlKqlHostsHosts);
    cy.get(_selectors.NAVIGATION_NETWORK).click({
      force: true
    });
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '');
  });
  it('sets and reads the url state for timeline by id', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.HOSTS_PAGE);
    (0, _helpers2.toggleTimelineVisibility)();
    (0, _helpers2.executeKQL)(_helpers2.hostExistsQuery);
    (0, _helpers2.assertAtLeastOneEventMatchesSearch)();
    const bestTimelineName = 'The Best Timeline';
    cy.get(_url_state.TIMELINE_TITLE, {
      timeout: 5000
    }).type(bestTimelineName);
    cy.url().should('include', 'timelineId=');
    cy.visit(`/app/siem#/timelines?timerange=(global:(linkTo:!(),timerange:(from:1565274377369,kind:absolute,to:1565360777369)),timeline:(linkTo:!(),timerange:(from:1565274377369,kind:absolute,to:1565360777369)))`).then(() => cy.get(_url_state.TIMELINE_TITLE).should('have.attr', 'value', bestTimelineName));
  });
});