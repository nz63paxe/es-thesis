"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterFieldsBrowser = exports.clickOutsideFieldsBrowser = exports.populateTimeline = exports.openTimelineFieldsBrowser = void 0;

var _selectors = require("./selectors");

var _helpers = require("../timeline/helpers");

var _selectors2 = require("../timeline/selectors");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Opens the timeline's Field Browser */
const openTimelineFieldsBrowser = () => {
  cy.get(_selectors.TIMELINE_FIELDS_BUTTON).click();
  cy.get(_selectors.FIELDS_BROWSER_CONTAINER).should('exist');
};
/** Populates the timeline with a host from the hosts page */


exports.openTimelineFieldsBrowser = openTimelineFieldsBrowser;

const populateTimeline = () => {
  (0, _helpers.toggleTimelineVisibility)();
  (0, _helpers.executeKQL)(_helpers.hostExistsQuery);
  (0, _helpers.assertAtLeastOneEventMatchesSearch)();
};
/** Clicks an arbitrary UI element that's not part of the fields browser (to dismiss it) */


exports.populateTimeline = populateTimeline;

const clickOutsideFieldsBrowser = () => {
  cy.get(_selectors2.TIMELINE_DATA_PROVIDERS).click();
};
/** Filters the Field Browser by typing `fieldName` in the input */


exports.clickOutsideFieldsBrowser = clickOutsideFieldsBrowser;

const filterFieldsBrowser = fieldName => {
  cy.get(_selectors.FIELDS_BROWSER_FILTER_INPUT).type(fieldName);
};

exports.filterFieldsBrowser = filterFieldsBrowser;