"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NAVIGATION_HOSTS_ANOMALIES = exports.NAVIGATION_HOSTS_ALL_HOSTS = exports.EVENTS_TAB_BUTTON = exports.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS = exports.ALL_HOSTS_WIDGET_HOST = exports.ALL_HOSTS_WIDGET = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The `All Hosts` widget on the `Hosts` page */
const ALL_HOSTS_WIDGET = '[data-test-subj="all-hosts-false"]';
/** A single draggable host in the `All Hosts` widget on the `Hosts` page */

exports.ALL_HOSTS_WIDGET = ALL_HOSTS_WIDGET;
const ALL_HOSTS_WIDGET_HOST = '[data-react-beautiful-dnd-drag-handle]';
/** All the draggable hosts in the `All Hosts` widget on the `Hosts` page */

exports.ALL_HOSTS_WIDGET_HOST = ALL_HOSTS_WIDGET_HOST;
const ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS = `${ALL_HOSTS_WIDGET} ${ALL_HOSTS_WIDGET_HOST}`;
/** Clicking this button displays the `Events` tab */

exports.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS = ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS;
const EVENTS_TAB_BUTTON = '[data-test-subj="navigation-events"]';
exports.EVENTS_TAB_BUTTON = EVENTS_TAB_BUTTON;
const NAVIGATION_HOSTS_ALL_HOSTS = '[data-test-subj="navigation-link-allHosts"]';
exports.NAVIGATION_HOSTS_ALL_HOSTS = NAVIGATION_HOSTS_ALL_HOSTS;
const NAVIGATION_HOSTS_ANOMALIES = '[data-test-subj="navigation-link-anomalies"]';
exports.NAVIGATION_HOSTS_ANOMALIES = NAVIGATION_HOSTS_ANOMALIES;