"use strict";

var _logout = require("../../lib/logout");

var _helpers = require("../../lib/timeline/helpers");

var _urls = require("../../lib/urls");

var _helpers2 = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('timeline search or filter KQL bar', () => {
  beforeEach(() => {
    (0, _helpers2.loginAndWaitForPage)(_urls.HOSTS_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('executes a KQL query', () => {
    (0, _helpers.toggleTimelineVisibility)();
    (0, _helpers.executeKQL)(_helpers.hostExistsQuery);
    (0, _helpers.assertAtLeastOneEventMatchesSearch)();
  });
});