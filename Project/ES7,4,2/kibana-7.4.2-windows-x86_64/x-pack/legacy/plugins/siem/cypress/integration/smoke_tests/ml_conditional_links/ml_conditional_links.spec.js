"use strict";

var _logout = require("../../lib/logout");

var _ml_conditional_links = require("../../lib/ml_conditional_links");

var _helpers = require("../../lib/util/helpers");

var _url_state = require("../../lib/url_state");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('ml conditional links', () => {
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('sets the KQL from a single IP with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkSingleIpFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(process.name: "conhost.exe" or process.name: "sc.exe")');
  });
  it('sets the KQL from a multiple IPs with a null for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkMultipleIpNullFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '((source.ip: "127.0.0.1" or destination.ip: "127.0.0.1") or (source.ip: "127.0.0.2" or destination.ip: "127.0.0.2"))');
  });
  it('sets the KQL from a multiple IPs with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkMultipleIpFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '((source.ip: "127.0.0.1" or destination.ip: "127.0.0.1") or (source.ip: "127.0.0.2" or destination.ip: "127.0.0.2")) and ((process.name: "conhost.exe" or process.name: "sc.exe"))');
  });
  it('sets the KQL from a $ip$ with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(process.name: "conhost.exe" or process.name: "sc.exe")');
  });
  it('sets the KQL from a single host name with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostSingleHostFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(process.name: "conhost.exe" or process.name: "sc.exe")');
  });
  it('sets the KQL from a multiple host names with null for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostMultiHostNullFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(host.name: "siem-windows" or host.name: "siem-suricata")');
  });
  it('sets the KQL from a multiple host names with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostMultiHostFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(host.name: "siem-windows" or host.name: "siem-suricata") and ((process.name: "conhost.exe" or process.name: "sc.exe"))');
  });
  it('sets the KQL from a undefined/null host name but with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostVariableHostFilterQuery);
    cy.get(_url_state.KQL_INPUT, {
      timeout: 5000
    }).should('have.attr', 'value', '(process.name: "conhost.exe" or process.name: "sc.exe")');
  });
  it('redirects from a single IP with a null for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkSingleIpNullFilterQuery);
    cy.url().should('include', '/app/siem#/network/ip/127.0.0.1?timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))');
  });
  it('redirects from a single IP with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkSingleIpFilterQuery);
    cy.url().should('include', "/app/siem#/network/ip/127.0.0.1?kqlQuery=(filterQuery:(expression:'(process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22)',kind:kuery),queryLocation:network.details)&timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))");
  });
  it('redirects from a multiple IPs with a null for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkMultipleIpNullFilterQuery);
    cy.url().should('include', "/app/siem#/network?kqlQuery=(filterQuery:(expression:'((source.ip:%20%22127.0.0.1%22%20or%20destination.ip:%20%22127.0.0.1%22)%20or%20(source.ip:%20%22127.0.0.2%22%20or%20destination.ip:%20%22127.0.0.2%22))'),queryLocation:network.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))");
  });
  it('redirects from a multiple IPs with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkMultipleIpFilterQuery);
    cy.url().should('include', "/app/siem#/network?kqlQuery=(filterQuery:(expression:'((source.ip:%20%22127.0.0.1%22%20or%20destination.ip:%20%22127.0.0.1%22)%20or%20(source.ip:%20%22127.0.0.2%22%20or%20destination.ip:%20%22127.0.0.2%22))%20and%20((process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22))',kind:kuery),queryLocation:network.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))");
  });
  it('redirects from a $ip$ with a null filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkNullFilterQuery);
    cy.url().should('include', '/app/siem#/network?timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))');
  });
  it('redirects from a $ip$ with a value for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlNetworkFilterQuery);
    cy.url().should('include', "/app/siem#/network?kqlQuery=(filterQuery:(expression:'(process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22)',kind:kuery),queryLocation:network.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1566990000000,kind:absolute,to:1567000799999)),timeline:(linkTo:!(global),timerange:(from:1566990000000,kind:absolute,to:1567000799999)))");
  });
  it('redirects from a single host name with a null for the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostSingleHostNullFilterQuery);
    cy.url().should('include', '/app/siem#/hosts/siem-windows/anomalies?_g=()&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))');
  });
  it('redirects from a host name with a variable in the filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostSingleHostFilterQueryVariable);
    cy.url().should('include', '/app/siem#/hosts/siem-windows/anomalies?_g=()&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))');
  });
  it('redirects from a single host name with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostSingleHostFilterQuery);
    cy.url().should('include', "/app/siem#/hosts/siem-windows/anomalies?_g=()&kqlQuery=(filterQuery:(expression:'(process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22)',kind:kuery),queryLocation:hosts.details)&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))");
  });
  it('redirects from a multiple host names with null for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostMultiHostNullFilterQuery);
    cy.url().should('include', "/app/siem#/hosts/anomalies?_g=()&kqlQuery=(filterQuery:(expression:'(host.name:%20%22siem-windows%22%20or%20host.name:%20%22siem-suricata%22)'),queryLocation:hosts.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))");
  });
  it('redirects from a multiple host names with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostMultiHostFilterQuery);
    cy.url().should('include', "/app/siem#/hosts/anomalies?_g=()&kqlQuery=(filterQuery:(expression:'(host.name:%20%22siem-windows%22%20or%20host.name:%20%22siem-suricata%22)%20and%20((process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22))',kind:kuery),queryLocation:hosts.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))");
  });
  it('redirects from a undefined/null host name with a null for the KQL', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostVariableHostNullFilterQuery);
    cy.url().should('include', '/app/siem#/hosts/anomalies?_g=()&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))');
  });
  it('redirects from a undefined/null host name but with a value for filterQuery', () => {
    (0, _helpers.loginAndWaitForPage)(_ml_conditional_links.mlHostVariableHostFilterQuery);
    cy.url().should('include', "/app/siem#/hosts/anomalies?_g=()&kqlQuery=(filterQuery:(expression:'(process.name:%20%22conhost.exe%22%20or%20process.name:%20%22sc.exe%22)',kind:kuery),queryLocation:hosts.page)&timerange=(global:(linkTo:!(timeline),timerange:(from:1559800800000,kind:absolute,to:1559887199999)),timeline:(linkTo:!(global),timerange:(from:1559800800000,kind:absolute,to:1559887199999)))");
  });
});