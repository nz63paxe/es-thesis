"use strict";

var _logout = require("../../lib/logout");

var _urls = require("../../lib/urls");

var _selectors = require("../../lib/navigation/selectors");

var _helpers = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('top-level navigation common to all pages in the SIEM app', () => {
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('navigates to the Overview page', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.TIMELINES_PAGE);
    cy.get(_selectors.NAVIGATION_OVERVIEW).click({
      force: true
    });
    cy.url().should('include', '/siem#/overview');
  });
  it('navigates to the Hosts page', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.TIMELINES_PAGE);
    cy.get(_selectors.NAVIGATION_HOSTS).click({
      force: true
    });
    cy.url().should('include', '/siem#/hosts');
  });
  it('navigates to the Network page', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.TIMELINES_PAGE);
    cy.get(_selectors.NAVIGATION_NETWORK).click({
      force: true
    });
    cy.url().should('include', '/siem#/network');
  });
  it('navigates to the Timelines page', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.OVERVIEW_PAGE);
    cy.get(_selectors.NAVIGATION_TIMELINES).click({
      force: true
    });
    cy.url().should('include', '/siem#/timelines');
  });
});