"use strict";

var _logout = require("../../lib/logout");

var _urls = require("../../lib/urls");

var _selectors = require("../../lib/pagination/selectors");

var _helpers = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('Pagination', () => {
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('pagination updates results and page number', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.HOSTS_PAGE_TABS.authentications);
    (0, _helpers.waitForTableLoad)();
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('have.class', 'euiPaginationButton-isActive');
    cy.get((0, _selectors.getDraggableField)('user.name')).first().invoke('text').then(text1 => {
      cy.get((0, _selectors.getPageButtonSelector)(2)).click({
        force: true
      }); // wait for table to be done loading

      (0, _helpers.waitForTableLoad)();
      cy.get((0, _selectors.getDraggableField)('user.name')).first().invoke('text').should(text2 => {
        expect(text1).not.to.eq(text2);
      });
    });
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('not.have.class', 'euiPaginationButton-isActive');
    cy.get((0, _selectors.getPageButtonSelector)(2)).should('have.class', 'euiPaginationButton-isActive');
  });
  it('pagination keeps track of page results when tabs change', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.HOSTS_PAGE_TABS.authentications);
    (0, _helpers.waitForTableLoad)();
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('have.class', 'euiPaginationButton-isActive');
    let thirdPageResult;
    cy.get((0, _selectors.getPageButtonSelector)(2)).click({
      force: true
    }); // wait for table to be done loading

    (0, _helpers.waitForTableLoad)();
    cy.get((0, _selectors.getDraggableField)('user.name')).first().invoke('text').then(text2 => {
      thirdPageResult = `${text2}`;
    });
    cy.get(_selectors.NAVIGATION_UNCOMMON_PROCESSES).click({
      force: true
    });
    (0, _helpers.waitForTableLoad)(); // check uncommon processes table starts at 1

    cy.get((0, _selectors.getPageButtonSelector)(0)).should('have.class', 'euiPaginationButton-isActive');
    cy.get(_selectors.NAVIGATION_AUTHENTICATIONS).click({
      force: true
    });
    (0, _helpers.waitForTableLoad)(); // check authentications table picks up at 3

    cy.get((0, _selectors.getPageButtonSelector)(2)).should('have.class', 'euiPaginationButton-isActive');
    cy.get((0, _selectors.getDraggableField)('user.name')).first().invoke('text').should(text1 => {
      expect(text1).to.eq(thirdPageResult);
    });
  });
  /*
   * We only want to comment this code/test for now because it can be nondeterministic
   * when we figure out a way to really mock the data, we should come back to it
   */

  it('pagination resets results and page number to first page when refresh is clicked', () => {
    (0, _helpers.loginAndWaitForPage)(_urls.HOSTS_PAGE_TABS.authentications);
    cy.get(_selectors.NUMBERED_PAGINATION, {
      timeout: _helpers.DEFAULT_TIMEOUT
    });
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('have.class', 'euiPaginationButton-isActive'); // let firstResult: string;
    // cy.get(getDraggableField('user.name'))
    //   .first()
    //   .invoke('text')
    //   .then(text1 => {
    //     firstResult = `${text1}`;
    //   });

    cy.get((0, _selectors.getPageButtonSelector)(2)).click({
      force: true
    });
    (0, _helpers.waitForTableLoad)();
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('not.have.class', 'euiPaginationButton-isActive');
    cy.get(_selectors.SUPER_DATE_PICKER_APPLY_BUTTON).last().click({
      force: true
    });
    (0, _helpers.waitForTableLoad)();
    cy.get((0, _selectors.getPageButtonSelector)(0)).should('have.class', 'euiPaginationButton-isActive'); // cy.get(getDraggableField('user.name'))
    //   .first()
    //   .invoke('text')
    //   .should(text1 => {
    //     expect(text1).to.eq(firstResult);
    //   });
  });
});