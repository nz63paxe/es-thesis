"use strict";

var _helpers = require("../../lib/fields_browser/helpers");

var _selectors = require("../../lib/fields_browser/selectors");

var _logout = require("../../lib/logout");

var _urls = require("../../lib/urls");

var _helpers2 = require("../../lib/util/helpers");

var _helpers3 = require("../../lib/events_viewer/helpers");

var _selectors2 = require("../../lib/events_viewer/selectors");

var _helpers4 = require("../../lib/hosts/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const defaultHeadersInDefaultEcsCategory = [{
  id: '@timestamp'
}, {
  id: 'message'
}, {
  id: 'host.name'
}, {
  id: 'event.action'
}, {
  id: 'user.name'
}, {
  id: 'source.ip'
}, {
  id: 'destination.ip'
}];
describe('Events Viewer', () => {
  beforeEach(() => {
    (0, _helpers2.loginAndWaitForPage)(_urls.HOSTS_PAGE);
    (0, _helpers4.clickEventsTab)();
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('renders the fields browser with the expected title when the Events Viewer Fields Browser button is clicked', () => {
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_TITLE).invoke('text').should('eq', 'Customize Columns');
  });
  it('closes the fields browser when the user clicks outside of it', () => {
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    (0, _helpers3.clickOutsideFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_CONTAINER).should('not.exist');
  });
  it('displays the `default ECS` category (by default)', () => {
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_SELECTED_CATEGORY_TITLE).invoke('text').should('eq', 'default ECS');
  });
  it('displays a checked checkbox for all of the default events viewer columns that are also in the default ECS category', () => {
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    defaultHeadersInDefaultEcsCategory.forEach(header => cy.get(`[data-test-subj="field-${header.id}-checkbox"]`).should('be.checked'));
  });
  it('removes the message field from the timeline when the user un-checks the field', () => {
    const toggleField = 'message';
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('exist');
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="field-${toggleField}-checkbox"]`).uncheck({
      force: true
    });
    (0, _helpers3.clickOutsideFieldsBrowser)();
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
  });
  it('filters the events by applying filter criteria from the search bar at the top of the page', () => {
    const filterInput = '4bf34c1c-eaa9-46de-8921-67a4ccc49829'; // this will never match real data

    cy.get(_selectors2.HEADER_SUBTITLE).invoke('text').then(text1 => {
      cy.get(_selectors2.HEADER_SUBTITLE).invoke('text').should('not.equal', 'Showing: 0 events');
      (0, _helpers3.filterSearchBar)(filterInput);
      cy.get(_selectors2.HEADER_SUBTITLE).invoke('text').should(text2 => {
        expect(text1).not.to.eq(text2);
      });
    });
  });
  it('adds a field to the events viewer when the user clicks the checkbox', () => {
    const filterInput = 'host.geo.c';
    const toggleField = 'host.geo.city_name';
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    (0, _helpers.filterFieldsBrowser)(filterInput);
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="field-${toggleField}-checkbox"]`).check({
      force: true
    });
    (0, _helpers3.clickOutsideFieldsBrowser)();
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('exist');
  });
  it('loads more events when the load more button is clicked', () => {
    cy.get(_selectors2.LOCAL_EVENTS_COUNT).invoke('text').then(text1 => {
      cy.get(_selectors2.LOCAL_EVENTS_COUNT).invoke('text').should('equal', '25');
      cy.get(_selectors2.LOAD_MORE).click({
        force: true
      });
      cy.get(_selectors2.LOCAL_EVENTS_COUNT).invoke('text').should(text2 => {
        expect(text1).not.to.eq(text2);
      });
    });
  });
  it('launches the inspect query modal when the inspect button is clicked', () => {
    // wait for data to load
    cy.get(_selectors2.HEADER_SUBTITLE).invoke('text').should('not.equal', 'Showing: 0 events');
    cy.get(_selectors2.INSPECT_QUERY).trigger('mousemove', {
      force: true
    }).click({
      force: true
    });
    cy.get(_selectors2.INSPECT_MODAL, {
      timeout: _helpers2.DEFAULT_TIMEOUT
    }).should('exist');
  });
  it('resets all fields in the events viewer when `Reset Fields` is clicked', () => {
    const filterInput = 'host.geo.c';
    const toggleField = 'host.geo.city_name';
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    (0, _helpers.filterFieldsBrowser)(filterInput);
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="field-${toggleField}-checkbox"]`).check({
      force: true
    });
    (0, _helpers3.clickOutsideFieldsBrowser)();
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('exist');
    (0, _helpers3.openEventsViewerFieldsBrowser)();
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="reset-fields"]`).click({
      force: true
    });
    cy.get(`${_selectors2.EVENTS_VIEWER_PANEL} [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
  });
});