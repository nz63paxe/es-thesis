"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NAVIGATION_TIMELINES = exports.NAVIGATION_OVERVIEW = exports.NAVIGATION_NETWORK = exports.NAVIGATION_HOSTS = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Top-level (global) navigation link to the `Hosts` page */
const NAVIGATION_HOSTS = '[data-test-subj="navigation-link-hosts"]';
/** Top-level (global) navigation link to the `Network` page */

exports.NAVIGATION_HOSTS = NAVIGATION_HOSTS;
const NAVIGATION_NETWORK = '[data-test-subj="navigation-link-network"]';
/** Top-level (global) navigation link to the `Overview` page */

exports.NAVIGATION_NETWORK = NAVIGATION_NETWORK;
const NAVIGATION_OVERVIEW = '[data-test-subj="navigation-link-overview"]';
/** Top-level (global) navigation link to the `Timelines` page */

exports.NAVIGATION_OVERVIEW = NAVIGATION_OVERVIEW;
const NAVIGATION_TIMELINES = '[data-test-subj="navigation-link-timelines"]';
exports.NAVIGATION_TIMELINES = NAVIGATION_TIMELINES;