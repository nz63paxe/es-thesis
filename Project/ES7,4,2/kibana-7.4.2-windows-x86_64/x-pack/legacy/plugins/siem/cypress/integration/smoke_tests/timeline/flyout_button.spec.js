"use strict";

var _logout = require("../../lib/logout");

var _selectors = require("../../lib/timeline/selectors");

var _selectors2 = require("../../lib/hosts/selectors");

var _urls = require("../../lib/urls");

var _helpers = require("../../lib/hosts/helpers");

var _helpers2 = require("../../lib/util/helpers");

var _helpers3 = require("../../lib/drag_n_drop/helpers");

var _helpers4 = require("../../lib/timeline/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('timeline flyout button', () => {
  beforeEach(() => {
    (0, _helpers2.loginAndWaitForPage)(_urls.HOSTS_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('toggles open the timeline', () => {
    (0, _helpers4.toggleTimelineVisibility)();
    cy.get(_selectors.TIMELINE_FLYOUT_BODY).should('have.css', 'visibility', 'visible');
  });
  it('sets the flyout button background to euiColorSuccess with a 10% alpha channel when the user starts dragging a host, but is not hovering over the flyout button', () => {
    (0, _helpers.waitForAllHostsWidget)();
    cy.get(_selectors2.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().then(host => (0, _helpers3.drag)(host));
    cy.get(_selectors.TIMELINE_NOT_READY_TO_DROP_BUTTON).should('have.css', 'background', 'rgba(125, 226, 209, 0.1) none repeat scroll 0% 0% / auto padding-box border-box');
  });
});