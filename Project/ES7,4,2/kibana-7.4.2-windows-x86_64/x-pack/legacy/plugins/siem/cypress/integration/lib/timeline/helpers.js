"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toggleFirstTimelineEventDetails = exports.assertAtLeastOneEventMatchesSearch = exports.hostExistsQuery = exports.executeKQL = exports.dragFromAllHostsToTimeline = exports.toggleTimelineVisibility = void 0;

var _helpers = require("../drag_n_drop/helpers");

var _selectors = require("../hosts/selectors");

var _selectors2 = require("./selectors");

var _helpers2 = require("../util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Toggles the timeline's open / closed state by clicking the `T I M E L I N E` button */
const toggleTimelineVisibility = () => cy.get(_selectors2.TIMELINE_TOGGLE_BUTTON, {
  timeout: _helpers2.DEFAULT_TIMEOUT
}).click();
/** Drags and drops a host from the `All Hosts` widget on the `Hosts` page to the timeline */


exports.toggleTimelineVisibility = toggleTimelineVisibility;

const dragFromAllHostsToTimeline = () => {
  cy.get(_selectors.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().then(host => (0, _helpers.drag)(host));
  cy.get(_selectors2.TIMELINE_DATA_PROVIDERS).then(dataProvidersDropArea => (0, _helpers.drop)(dataProvidersDropArea));
};
/** Executes the specified KQL query in the timeline */


exports.dragFromAllHostsToTimeline = dragFromAllHostsToTimeline;

const executeKQL = query => {
  cy.get(`${_selectors2.SEARCH_OR_FILTER_CONTAINER} input`).type(`${query} {enter}`);
};
/** A sample KQL query that finds any documents where the `host.name` field exists */


exports.executeKQL = executeKQL;
const hostExistsQuery = 'host.name: *';
/** Asserts that at least one event matches the timeline's search criteria */

exports.hostExistsQuery = hostExistsQuery;

const assertAtLeastOneEventMatchesSearch = () => cy.get(_selectors2.SERVER_SIDE_EVENT_COUNT, {
  timeout: _helpers2.DEFAULT_TIMEOUT
}).invoke('text').should('be.above', 0);
/** Toggles open or closed the first event in the timeline */


exports.assertAtLeastOneEventMatchesSearch = assertAtLeastOneEventMatchesSearch;

const toggleFirstTimelineEventDetails = () => {
  cy.get(_selectors2.TOGGLE_TIMELINE_EXPAND_EVENT, {
    timeout: _helpers2.DEFAULT_TIMEOUT
  }).first().click();
};

exports.toggleFirstTimelineEventDetails = toggleFirstTimelineEventDetails;