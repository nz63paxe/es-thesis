"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FIELDS_BROWSER_FIELDS_COUNT = exports.FIELDS_BROWSER_CATEGORIES_COUNT = exports.FIELDS_BROWSER_FILTER_INPUT = exports.FIELDS_BROWSER_SELECTED_CATEGORY_COUNT = exports.FIELDS_BROWSER_SELECTED_CATEGORY_TITLE = exports.FIELDS_BROWSER_CONTAINER = exports.FIELDS_BROWSER_TITLE = exports.TIMELINE_FIELDS_BUTTON = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Clicking this button in the timeline opens the Fields browser */
const TIMELINE_FIELDS_BUTTON = '[data-test-subj="timeline"] [data-test-subj="show-field-browser"]';
/** The title displayed in the fields browser (i.e. Customize Columns) */

exports.TIMELINE_FIELDS_BUTTON = TIMELINE_FIELDS_BUTTON;
const FIELDS_BROWSER_TITLE = '[data-test-subj="field-browser-title"]';
/** Contains the body of the fields browser */

exports.FIELDS_BROWSER_TITLE = FIELDS_BROWSER_TITLE;
const FIELDS_BROWSER_CONTAINER = '[data-test-subj="fields-browser-container"]';
/** The title of the selected category in the right-hand side of the fields browser */

exports.FIELDS_BROWSER_CONTAINER = FIELDS_BROWSER_CONTAINER;
const FIELDS_BROWSER_SELECTED_CATEGORY_TITLE = '[data-test-subj="selected-category-title"]';
/** A count of the fields in the selected category in the right-hand side of the fields browser */

exports.FIELDS_BROWSER_SELECTED_CATEGORY_TITLE = FIELDS_BROWSER_SELECTED_CATEGORY_TITLE;
const FIELDS_BROWSER_SELECTED_CATEGORY_COUNT = '[data-test-subj="selected-category-count-badge"]';
/** Typing in this input filters the Field Browser */

exports.FIELDS_BROWSER_SELECTED_CATEGORY_COUNT = FIELDS_BROWSER_SELECTED_CATEGORY_COUNT;
const FIELDS_BROWSER_FILTER_INPUT = '[data-test-subj="field-search"]';
/**
 * This label displays a count of the categories containing (one or more)
 * fields that match the filter criteria
 */

exports.FIELDS_BROWSER_FILTER_INPUT = FIELDS_BROWSER_FILTER_INPUT;
const FIELDS_BROWSER_CATEGORIES_COUNT = '[data-test-subj="categories-count"]';
/**
 * This label displays a count of the fields that match the filter criteria
 */

exports.FIELDS_BROWSER_CATEGORIES_COUNT = FIELDS_BROWSER_CATEGORIES_COUNT;
const FIELDS_BROWSER_FIELDS_COUNT = '[data-test-subj="fields-count"]';
exports.FIELDS_BROWSER_FIELDS_COUNT = FIELDS_BROWSER_FIELDS_COUNT;