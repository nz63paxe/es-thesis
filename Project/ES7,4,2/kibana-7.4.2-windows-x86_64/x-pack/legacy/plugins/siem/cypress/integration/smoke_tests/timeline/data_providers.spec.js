"use strict";

var _logout = require("../../lib/logout");

var _selectors = require("../../lib/timeline/selectors");

var _helpers = require("../../lib/timeline/helpers");

var _selectors2 = require("../../lib/hosts/selectors");

var _urls = require("../../lib/urls");

var _helpers2 = require("../../lib/hosts/helpers");

var _helpers3 = require("../../lib/util/helpers");

var _helpers4 = require("../../lib/drag_n_drop/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('timeline data providers', () => {
  beforeEach(() => {
    (0, _helpers3.loginAndWaitForPage)(_urls.HOSTS_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('renders the data provider of a host dragged from the All Hosts widget on the hosts page', () => {
    (0, _helpers2.waitForAllHostsWidget)();
    (0, _helpers.toggleTimelineVisibility)();
    (0, _helpers.dragFromAllHostsToTimeline)();
    cy.get(_selectors.TIMELINE_DROPPED_DATA_PROVIDERS, {
      timeout: _helpers3.DEFAULT_TIMEOUT + 10 * 1000
    }).first().invoke('text').then(dataProviderText => {
      // verify the data provider displays the same `host.name` as the host dragged from the `All Hosts` widget
      cy.get(_selectors2.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().invoke('text').should(hostname => {
        expect(dataProviderText).to.eq(`host.name: "${hostname}"`);
      });
    });
  });
  it('sets the background to euiColorSuccess with a 10% alpha channel when the user starts dragging a host, but is not hovering over the data providers', () => {
    (0, _helpers2.waitForAllHostsWidget)();
    (0, _helpers.toggleTimelineVisibility)();
    cy.get(_selectors2.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().then(host => (0, _helpers4.drag)(host));
    cy.get(_selectors.TIMELINE_DATA_PROVIDERS).should('have.css', 'background', 'rgba(125, 226, 209, 0.1) none repeat scroll 0% 0% / auto padding-box border-box');
  });
  it('sets the background to euiColorSuccess with a 20% alpha channel when the user starts dragging a host AND is hovering over the data providers', () => {
    (0, _helpers2.waitForAllHostsWidget)();
    (0, _helpers.toggleTimelineVisibility)();
    cy.get(_selectors2.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().then(host => (0, _helpers4.drag)(host));
    cy.get(_selectors.TIMELINE_DATA_PROVIDERS_EMPTY).then(dataProvidersDropArea => (0, _helpers4.dragWithoutDrop)(dataProvidersDropArea));
    cy.get(_selectors.TIMELINE_DATA_PROVIDERS_EMPTY).should('have.css', 'background', 'rgba(125, 226, 209, 0.2) none repeat scroll 0% 0% / auto padding-box border-box');
  });
  it('renders the dashed border color as euiColorSuccess when hovering over the data providers', () => {
    (0, _helpers2.waitForAllHostsWidget)();
    (0, _helpers.toggleTimelineVisibility)();
    cy.get(_selectors2.ALL_HOSTS_WIDGET_DRAGGABLE_HOSTS).first().then(host => (0, _helpers4.drag)(host));
    cy.get(_selectors.TIMELINE_DATA_PROVIDERS_EMPTY).then(dataProvidersDropArea => (0, _helpers4.dragWithoutDrop)(dataProvidersDropArea));
    cy.get(_selectors.TIMELINE_DATA_PROVIDERS).should('have.css', 'border', '3.1875px dashed rgb(125, 226, 209)');
  });
});