"use strict";

var _helpers = require("../../lib/drag_n_drop/helpers");

var _helpers2 = require("../../lib/fields_browser/helpers");

var _logout = require("../../lib/logout");

var _helpers3 = require("../../lib/timeline/helpers");

var _urls = require("../../lib/urls");

var _helpers4 = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('toggle column in timeline', () => {
  beforeEach(() => {
    (0, _helpers4.loginAndWaitForPage)(_urls.HOSTS_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  const timestampField = '@timestamp';
  const idField = '_id';
  it('displays a checked Toggle field checkbox for `@timestamp`, a default timeline column', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers3.toggleFirstTimelineEventDetails)();
    cy.get(`[data-test-subj="toggle-field-${timestampField}"]`).should('be.checked');
  });
  it('displays an Unchecked Toggle field checkbox for `_id`, because it is NOT a default timeline column', () => {
    (0, _helpers2.populateTimeline)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="toggle-field-${idField}"]`).should('not.be.checked');
  });
  it('removes the @timestamp field from the timeline when the user un-checks the toggle', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers3.toggleFirstTimelineEventDetails)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${timestampField}"]`).should('exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="toggle-field-${timestampField}"]`).uncheck();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${timestampField}"]`).should('not.exist');
  });
  it('adds the _id field to the timeline when the user checks the field', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers3.toggleFirstTimelineEventDetails)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${idField}"]`).should('not.exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="toggle-field-${idField}"]`).check();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${idField}"]`).should('exist');
  });
  it('adds the _id field to the timeline via drag and drop', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers3.toggleFirstTimelineEventDetails)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${idField}"]`).should('not.exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="field-name-${idField}"]`).then(field => (0, _helpers.drag)(field));
    cy.get(`[data-test-subj="timeline"] [data-test-subj="headers-group"]`).then(headersDropArea => (0, _helpers.drop)(headersDropArea));
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${idField}"]`, {
      timeout: _helpers4.DEFAULT_TIMEOUT
    }).should('exist');
  });
});