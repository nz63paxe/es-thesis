"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clickEventsTab = exports.waitForAllHostsWidget = void 0;

var _helpers = require("../util/helpers");

var _selectors = require("./selectors");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Wait for the for the `All Hosts` widget on the `Hosts` page to load */
const waitForAllHostsWidget = () => cy.get(_selectors.ALL_HOSTS_WIDGET, {
  timeout: _helpers.DEFAULT_TIMEOUT
});
/** Clicks the Events tab on the hosts page */


exports.waitForAllHostsWidget = waitForAllHostsWidget;

const clickEventsTab = () => cy.get(_selectors.EVENTS_TAB_BUTTON, {
  timeout: _helpers.DEFAULT_TIMEOUT
}).click({
  force: true
});

exports.clickEventsTab = clickEventsTab;