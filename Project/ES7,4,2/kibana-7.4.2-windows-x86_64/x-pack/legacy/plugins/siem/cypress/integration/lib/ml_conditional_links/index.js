"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mlHostVariableHostFilterQuery = exports.mlHostVariableHostNullFilterQuery = exports.mlHostMultiHostFilterQuery = exports.mlHostMultiHostNullFilterQuery = exports.mlHostSingleHostFilterQuery = exports.mlHostSingleHostFilterQueryVariable = exports.mlHostSingleHostNullFilterQuery = exports.mlNetworkFilterQuery = exports.mlNetworkNullFilterQuery = exports.mlNetworkMultipleIpFilterQuery = exports.mlNetworkMultipleIpNullFilterQuery = exports.mlNetworkSingleIpFilterQuery = exports.mlNetworkSingleIpNullFilterQuery = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * These links are for different test scenarios that try and capture different drill downs into
 * ml-network and ml-hosts and are of the flavor of testing:
 * A filter being null: (filterQuery:!n)
 * A filter being set with single values: filterQuery:(expression:%27process.name%20:%20%22conhost.exe%22%27,kind:kuery)
 * A filter being set with multiple values: filterQuery:(expression:%27process.name%20:%20%22conhost.exe,sc.exe%22%27,kind:kuery)
 * A filter containing variables not replaced: filterQuery:(expression:%27process.name%20:%20%$process.name$%22%27,kind:kuery)
 *
 * In different combination with:
 * network not being set: $ip$
 * host not being set: $host.name$
 * ...or...
 * network being set normally: 127.0.0.1
 * host being set normally: suricata-iowa
 * ...or...
 * network having multiple values: 127.0.0.1,127.0.0.2
 * host having multiple values: suricata-iowa,siem-windows
 */
// Single IP with a null for the filterQuery:
const mlNetworkSingleIpNullFilterQuery = "/app/siem#/ml-network/ip/127.0.0.1?kqlQuery=(filterQuery:!n,queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // Single IP with a value for the filterQuery:

exports.mlNetworkSingleIpNullFilterQuery = mlNetworkSingleIpNullFilterQuery;
const mlNetworkSingleIpFilterQuery = "/app/siem#/ml-network/ip/127.0.0.1?kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // Multiple IPs with a null for the filterQuery:

exports.mlNetworkSingleIpFilterQuery = mlNetworkSingleIpFilterQuery;
const mlNetworkMultipleIpNullFilterQuery = "/app/siem#/ml-network/ip/127.0.0.1,127.0.0.2?kqlQuery=(filterQuery:!n,queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // Multiple IPs with a value for the filterQuery:

exports.mlNetworkMultipleIpNullFilterQuery = mlNetworkMultipleIpNullFilterQuery;
const mlNetworkMultipleIpFilterQuery = "/app/siem#/ml-network/ip/127.0.0.1,127.0.0.2?kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // $ip$ with a null filterQuery:

exports.mlNetworkMultipleIpFilterQuery = mlNetworkMultipleIpFilterQuery;
const mlNetworkNullFilterQuery = "/app/siem#/ml-network/ip/$ip$?kqlQuery=(filterQuery:!n,queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // $ip$ with a value for the filterQuery:

exports.mlNetworkNullFilterQuery = mlNetworkNullFilterQuery;
const mlNetworkFilterQuery = "/app/siem#/ml-network/ip/$ip$?kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:network.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-08-28T11:00:00.000Z',kind:absolute,to:'2019-08-28T13:59:59.999Z')))"; // Single host name with a null for the filterQuery:

exports.mlNetworkFilterQuery = mlNetworkFilterQuery;
const mlHostSingleHostNullFilterQuery = "/app/siem#/ml-hosts/siem-windows?_g=()&kqlQuery=(filterQuery:!n,queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Single host name with a variable in the filterQuery

exports.mlHostSingleHostNullFilterQuery = mlHostSingleHostNullFilterQuery;
const mlHostSingleHostFilterQueryVariable = "/app/siem#/ml-hosts/siem-windows?_g=()&kqlQuery=(filterQuery:(expression:'process.name%20:%20%22$process.name$%22',kind:kuery),queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Single host name with a value for filterQuery:

exports.mlHostSingleHostFilterQueryVariable = mlHostSingleHostFilterQueryVariable;
const mlHostSingleHostFilterQuery = "/app/siem#/ml-hosts/siem-windows?_g=()&kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Multiple host names with null for filterQuery

exports.mlHostSingleHostFilterQuery = mlHostSingleHostFilterQuery;
const mlHostMultiHostNullFilterQuery = "/app/siem#/ml-hosts/siem-windows,siem-suricata?_g=()&kqlQuery=(filterQuery:!n,queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Multiple host names with a value for filterQuery

exports.mlHostMultiHostNullFilterQuery = mlHostMultiHostNullFilterQuery;
const mlHostMultiHostFilterQuery = "/app/siem#/ml-hosts/siem-windows,siem-suricata?_g=()&kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Undefined/null host name with a null for the KQL:

exports.mlHostMultiHostFilterQuery = mlHostMultiHostFilterQuery;
const mlHostVariableHostNullFilterQuery = "/app/siem#/ml-hosts/$host.name$?_g=()&kqlQuery=(filterQuery:!n,queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))"; // Undefined/null host name but with a value for filterQuery

exports.mlHostVariableHostNullFilterQuery = mlHostVariableHostNullFilterQuery;
const mlHostVariableHostFilterQuery = "/app/siem#/ml-hosts/$host.name$?_g=()&kqlQuery=(filterQuery:(expression:'process.name%20:%20%22conhost.exe,sc.exe%22',kind:kuery),queryLocation:hosts.details,type:details)&timerange=(global:(linkTo:!(timeline),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')),timeline:(linkTo:!(global),timerange:(from:'2019-06-06T06:00:00.000Z',kind:absolute,to:'2019-06-07T05:59:59.999Z')))";
exports.mlHostVariableHostFilterQuery = mlHostVariableHostFilterQuery;