"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterSearchBar = exports.clickOutsideFieldsBrowser = exports.openEventsViewerFieldsBrowser = void 0;

var _selectors = require("./selectors");

var _selectors2 = require("../fields_browser/selectors");

var _helpers = require("../util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** Opens the eventsViewer Field Browser */
const openEventsViewerFieldsBrowser = () => {
  cy.get(_selectors.EVENTS_VIEWER_FIELDS_BUTTON, {
    timeout: _helpers.DEFAULT_TIMEOUT
  }).click({
    force: true
  });
  cy.get(_selectors2.FIELDS_BROWSER_CONTAINER).should('exist');
};
/** Clicks an arbitrary UI element that's not part of the fields browser (to dismiss it) */


exports.openEventsViewerFieldsBrowser = openEventsViewerFieldsBrowser;

const clickOutsideFieldsBrowser = () => {
  cy.get(_selectors.KQL_SEARCH_BAR, {
    timeout: _helpers.DEFAULT_TIMEOUT
  }).click();
};
/** Filters the search bar at the top of most pages with the specified KQL */


exports.clickOutsideFieldsBrowser = clickOutsideFieldsBrowser;

const filterSearchBar = kql => {
  cy.get(_selectors.KQL_SEARCH_BAR).type(`${kql} {enter}`);
};

exports.filterSearchBar = filterSearchBar;