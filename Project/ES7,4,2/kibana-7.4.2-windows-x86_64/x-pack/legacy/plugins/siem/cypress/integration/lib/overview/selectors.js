"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NETWORK_STATS = exports.STAT_TLS = exports.STAT_FLOW = exports.STAT_DNS = exports.STAT_ZEEK = exports.STAT_SURICATA = exports.STAT_PANW = exports.STAT_NETFLOW = exports.STAT_CISCO = exports.STAT_SOCKET = exports.HOST_STATS = exports.STAT_WINLOGBEAT = exports.STAT_USER = exports.STAT_PROCESS = exports.STAT_PACKAGE = exports.STAT_LOGIN = exports.STAT_FIM = exports.STAT_FILEBEAT = exports.STAT_AUDITD = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Host Stats
const STAT_AUDITD = {
  value: '123',
  domId: '[data-test-subj="host-stat-auditbeatAuditd"]'
};
exports.STAT_AUDITD = STAT_AUDITD;
const STAT_FILEBEAT = {
  value: '890',
  domId: '[data-test-subj="host-stat-filebeatSystemModule"]'
};
exports.STAT_FILEBEAT = STAT_FILEBEAT;
const STAT_FIM = {
  value: '345',
  domId: '[data-test-subj="host-stat-auditbeatFIM"]'
};
exports.STAT_FIM = STAT_FIM;
const STAT_LOGIN = {
  value: '456',
  domId: '[data-test-subj="host-stat-auditbeatLogin"]'
};
exports.STAT_LOGIN = STAT_LOGIN;
const STAT_PACKAGE = {
  value: '567',
  domId: '[data-test-subj="host-stat-auditbeatPackage"]'
};
exports.STAT_PACKAGE = STAT_PACKAGE;
const STAT_PROCESS = {
  value: '678',
  domId: '[data-test-subj="host-stat-auditbeatProcess"]'
};
exports.STAT_PROCESS = STAT_PROCESS;
const STAT_USER = {
  value: '789',
  domId: '[data-test-subj="host-stat-auditbeatUser"]'
};
exports.STAT_USER = STAT_USER;
const STAT_WINLOGBEAT = {
  value: '100',
  domId: '[data-test-subj="host-stat-winlogbeat"]'
};
exports.STAT_WINLOGBEAT = STAT_WINLOGBEAT;
const HOST_STATS = [STAT_AUDITD, STAT_FILEBEAT, STAT_FIM, STAT_LOGIN, STAT_PACKAGE, STAT_PROCESS, STAT_USER, STAT_WINLOGBEAT]; // Network Stats

exports.HOST_STATS = HOST_STATS;
const STAT_SOCKET = {
  value: '578,502',
  domId: '[data-test-subj="network-stat-auditbeatSocket"]'
};
exports.STAT_SOCKET = STAT_SOCKET;
const STAT_CISCO = {
  value: '999',
  domId: '[data-test-subj="network-stat-filebeatCisco"]'
};
exports.STAT_CISCO = STAT_CISCO;
const STAT_NETFLOW = {
  value: '2,544',
  domId: '[data-test-subj="network-stat-filebeatNetflow"]'
};
exports.STAT_NETFLOW = STAT_NETFLOW;
const STAT_PANW = {
  value: '678',
  domId: '[data-test-subj="network-stat-filebeatPanw"]'
};
exports.STAT_PANW = STAT_PANW;
const STAT_SURICATA = {
  value: '303,699',
  domId: '[data-test-subj="network-stat-filebeatSuricata"]'
};
exports.STAT_SURICATA = STAT_SURICATA;
const STAT_ZEEK = {
  value: '71,129',
  domId: '[data-test-subj="network-stat-filebeatZeek"]'
};
exports.STAT_ZEEK = STAT_ZEEK;
const STAT_DNS = {
  value: '1,090',
  domId: '[data-test-subj="network-stat-packetbeatDNS"]'
};
exports.STAT_DNS = STAT_DNS;
const STAT_FLOW = {
  value: '722,153',
  domId: '[data-test-subj="network-stat-packetbeatFlow"]'
};
exports.STAT_FLOW = STAT_FLOW;
const STAT_TLS = {
  value: '340',
  domId: '[data-test-subj="network-stat-packetbeatTLS"]'
};
exports.STAT_TLS = STAT_TLS;
const NETWORK_STATS = [STAT_SOCKET, STAT_CISCO, STAT_NETFLOW, STAT_PANW, STAT_SURICATA, STAT_ZEEK, STAT_DNS, STAT_FLOW, STAT_TLS];
exports.NETWORK_STATS = NETWORK_STATS;