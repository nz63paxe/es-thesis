"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_SPACE_BUTTON = exports.PASSWORD = exports.USERNAME = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The Username field in the Kibana login page */
const USERNAME = '[data-test-subj="loginUsername"]';
/** The Password field in the Kibana login page */

exports.USERNAME = USERNAME;
const PASSWORD = '[data-test-subj="loginPassword"]';
/** The `Default` space button on the `Select your space` page */

exports.PASSWORD = PASSWORD;
const DEFAULT_SPACE_BUTTON = '[data-test-subj="space-card-default"]';
exports.DEFAULT_SPACE_BUTTON = DEFAULT_SPACE_BUTTON;