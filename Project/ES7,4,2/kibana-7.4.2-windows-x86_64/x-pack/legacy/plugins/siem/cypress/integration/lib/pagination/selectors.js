"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SUPER_DATE_PICKER_APPLY_BUTTON = exports.NUMBERED_PAGINATION = exports.NAVIGATION_UNCOMMON_PROCESSES = exports.NAVIGATION_AUTHENTICATIONS = exports.getPageButtonSelector = exports.getDraggableField = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getDraggableField = field => `[data-test-subj="draggable-content-${field}"]`;

exports.getDraggableField = getDraggableField;

const getPageButtonSelector = num => `[data-test-subj="pagination-button-${num}"]`;

exports.getPageButtonSelector = getPageButtonSelector;
const NAVIGATION_AUTHENTICATIONS = '[data-test-subj="navigation-authentications"]';
exports.NAVIGATION_AUTHENTICATIONS = NAVIGATION_AUTHENTICATIONS;
const NAVIGATION_UNCOMMON_PROCESSES = '[data-test-subj="navigation-uncommonProcesses"]';
exports.NAVIGATION_UNCOMMON_PROCESSES = NAVIGATION_UNCOMMON_PROCESSES;
const NUMBERED_PAGINATION = '[data-test-subj="numberedPagination"]';
exports.NUMBERED_PAGINATION = NUMBERED_PAGINATION;
const SUPER_DATE_PICKER_APPLY_BUTTON = '[data-test-subj="superDatePickerApplyTimeButton"]';
exports.SUPER_DATE_PICKER_APPLY_BUTTON = SUPER_DATE_PICKER_APPLY_BUTTON;