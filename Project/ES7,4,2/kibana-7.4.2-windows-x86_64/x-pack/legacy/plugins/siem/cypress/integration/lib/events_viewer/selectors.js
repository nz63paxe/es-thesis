"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOAD_MORE = exports.LOCAL_EVENTS_COUNT = exports.INSPECT_QUERY = exports.INSPECT_MODAL = exports.HEADER_SUBTITLE = exports.KQL_SEARCH_BAR = exports.EVENTS_VIEWER_FIELDS_BUTTON = exports.EVENTS_VIEWER_PANEL = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The panel containing the events viewer */
const EVENTS_VIEWER_PANEL = '[data-test-subj="events-viewer-panel"]';
/** Clicking this button in the timeline opens the Fields browser in the Events Viewer */

exports.EVENTS_VIEWER_PANEL = EVENTS_VIEWER_PANEL;
const EVENTS_VIEWER_FIELDS_BUTTON = `${EVENTS_VIEWER_PANEL} [data-test-subj="show-field-browser-gear"]`;
/** The KQL search bar that exists at the top of most pages */

exports.EVENTS_VIEWER_FIELDS_BUTTON = EVENTS_VIEWER_FIELDS_BUTTON;
const KQL_SEARCH_BAR = '[data-test-subj="kqlInput"]';
/** The Events Viewer Showing N events header subtitle */

exports.KQL_SEARCH_BAR = KQL_SEARCH_BAR;
const HEADER_SUBTITLE = `${EVENTS_VIEWER_PANEL} [data-test-subj="subtitle"]`;
/** The inspect query modal */

exports.HEADER_SUBTITLE = HEADER_SUBTITLE;
const INSPECT_MODAL = '[data-test-subj="modal-inspect-euiModal"]';
/** The inspect query button that launches the inspect query modal */

exports.INSPECT_MODAL = INSPECT_MODAL;
const INSPECT_QUERY = `${EVENTS_VIEWER_PANEL} [data-test-subj="inspect-icon-button"]`;
/** A count of the events loaded in the table */

exports.INSPECT_QUERY = INSPECT_QUERY;
const LOCAL_EVENTS_COUNT = `${EVENTS_VIEWER_PANEL} [data-test-subj="local-events-count"]`;
/** The events viewer Load More button */

exports.LOCAL_EVENTS_COUNT = LOCAL_EVENTS_COUNT;
const LOAD_MORE = `${EVENTS_VIEWER_PANEL} [data-test-subj="TimelineMoreButton"]`;
exports.LOAD_MORE = LOAD_MORE;