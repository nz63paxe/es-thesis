"use strict";

var _helpers = require("../../lib/drag_n_drop/helpers");

var _helpers2 = require("../../lib/fields_browser/helpers");

var _selectors = require("../../lib/fields_browser/selectors");

var _logout = require("../../lib/logout");

var _urls = require("../../lib/urls");

var _helpers3 = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const defaultHeaders = [{
  id: '@timestamp'
}, {
  id: 'message'
}, {
  id: 'event.category'
}, {
  id: 'event.action'
}, {
  id: 'host.name'
}, {
  id: 'source.ip'
}, {
  id: 'destination.ip'
}, {
  id: 'user.name'
}];
describe('Fields Browser', () => {
  beforeEach(() => {
    (0, _helpers3.loginAndWaitForPage)(_urls.HOSTS_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('renders the fields browser with the expected title when the Fields button is clicked', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_TITLE).invoke('text').should('eq', 'Customize Columns');
  });
  it('closes the fields browser when the user clicks outside of it', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.clickOutsideFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_CONTAINER).should('not.exist');
  });
  it('displays the `default ECS` category (by default)', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_SELECTED_CATEGORY_TITLE).invoke('text').should('eq', 'default ECS');
  });
  it('the `defaultECS` (selected) category count matches the default timeline header count', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    cy.get(_selectors.FIELDS_BROWSER_SELECTED_CATEGORY_COUNT).invoke('text').should('eq', `${defaultHeaders.length}`);
  });
  it('displays a checked checkbox for all of the default timeline columns', () => {
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    defaultHeaders.forEach(header => cy.get(`[data-test-subj="field-${header.id}-checkbox"]`).should('be.checked'));
  });
  it('removes the message field from the timeline when the user un-checks the field', () => {
    const toggleField = 'message';
    (0, _helpers2.populateTimeline)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('exist');
    (0, _helpers2.openTimelineFieldsBrowser)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="field-${toggleField}-checkbox"]`).uncheck();
    (0, _helpers2.clickOutsideFieldsBrowser)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
  });
  it('displays the expected count of categories that match the filter input', () => {
    const filterInput = 'host.mac';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(_selectors.FIELDS_BROWSER_CATEGORIES_COUNT).invoke('text').should('eq', '2 categories');
  });
  it('displays a search results label with the expected count of fields matching the filter input', () => {
    const filterInput = 'host.mac';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(_selectors.FIELDS_BROWSER_FIELDS_COUNT).invoke('text').should('eq', '2 fields');
  });
  it('selects a search results label with the expected count of categories matching the filter input', () => {
    const category = 'host';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(`${category}.`);
    cy.get(_selectors.FIELDS_BROWSER_SELECTED_CATEGORY_TITLE).invoke('text').should('eq', category);
  });
  it('displays a count of only the fields in the selected category that match the filter input', () => {
    const filterInput = 'host.geo.c';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(_selectors.FIELDS_BROWSER_SELECTED_CATEGORY_COUNT).invoke('text').should('eq', '4');
  });
  it('adds a field to the timeline when the user clicks the checkbox', () => {
    const filterInput = 'host.geo.c';
    const toggleField = 'host.geo.city_name';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="field-${toggleField}-checkbox"]`).check();
    (0, _helpers2.clickOutsideFieldsBrowser)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('exist');
  });
  it('adds a field to the timeline when the user drags and drops a field', () => {
    const filterInput = 'host.geo.c';
    const toggleField = 'host.geo.city_name';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="field-name-${toggleField}"]`).then(field => (0, _helpers.drag)(field));
    cy.get(`[data-test-subj="timeline"] [data-test-subj="headers-group"]`).then(headersDropArea => (0, _helpers.drop)(headersDropArea));
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`, {
      timeout: _helpers3.DEFAULT_TIMEOUT
    }).should('exist');
  });
  it('resets all fields in the timeline when `Reset Fields` is clicked', () => {
    const filterInput = 'host.geo.c';
    const toggleField = 'host.geo.city_name';
    (0, _helpers2.populateTimeline)();
    (0, _helpers2.openTimelineFieldsBrowser)();
    (0, _helpers2.filterFieldsBrowser)(filterInput);
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
    cy.get(`[data-test-subj="timeline"] [data-test-subj="field-${toggleField}-checkbox"]`).check();
    (0, _helpers2.clickOutsideFieldsBrowser)();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('exist');
    (0, _helpers2.openTimelineFieldsBrowser)();
    cy.get('[data-test-subj="timeline"] [data-test-subj="reset-fields"]').click();
    cy.get(`[data-test-subj="timeline"] [data-test-subj="header-text-${toggleField}"]`).should('not.exist');
  });
});