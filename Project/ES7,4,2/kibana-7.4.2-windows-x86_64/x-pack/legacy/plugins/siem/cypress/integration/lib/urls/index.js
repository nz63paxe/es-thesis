"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LOGOUT = exports.TIMELINES_PAGE = exports.OVERVIEW_PAGE = exports.NETWORK_PAGE = exports.LOGIN_PAGE = exports.HOSTS_PAGE_TABS = exports.HOSTS_PAGE = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The SIEM app's Hosts page */
const HOSTS_PAGE = '/app/siem#/hosts/allHosts';
exports.HOSTS_PAGE = HOSTS_PAGE;
const HOSTS_PAGE_TABS = {
  allHosts: '/app/siem#/hosts/allHosts',
  anomalies: '/app/siem#/hosts/anomalies',
  authentications: '/app/siem#/hosts/authentications',
  events: '/app/siem#/hosts/events',
  uncommonProcesses: '/app/siem#/hosts/uncommonProcesses'
};
/** Kibana's login page */

exports.HOSTS_PAGE_TABS = HOSTS_PAGE_TABS;
const LOGIN_PAGE = '/login';
/** The SIEM app's Network page */

exports.LOGIN_PAGE = LOGIN_PAGE;
const NETWORK_PAGE = '/app/siem#/network';
/** The SIEM app's Overview page */

exports.NETWORK_PAGE = NETWORK_PAGE;
const OVERVIEW_PAGE = '/app/siem#/overview';
/** The SIEM app's Timelines page */

exports.OVERVIEW_PAGE = OVERVIEW_PAGE;
const TIMELINES_PAGE = '/app/siem#/timelines';
/** Visit this URL to logout of Kibana */

exports.TIMELINES_PAGE = TIMELINES_PAGE;
const LOGOUT = '/logout';
exports.LOGOUT = LOGOUT;