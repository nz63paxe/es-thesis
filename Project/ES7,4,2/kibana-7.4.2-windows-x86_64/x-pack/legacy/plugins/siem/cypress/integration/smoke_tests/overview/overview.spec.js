"use strict";

var _logout = require("../../lib/logout");

var _urls = require("../../lib/urls");

var _helpers = require("../../lib/fixtures/helpers");

var _selectors = require("../../lib/overview/selectors");

var _helpers2 = require("../../lib/util/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
describe('Overview Page', () => {
  beforeEach(() => {
    (0, _helpers.clearFetch)();
    (0, _helpers.stubApi)('overview');
    (0, _helpers2.loginAndWaitForPage)(_urls.OVERVIEW_PAGE);
  });
  afterEach(() => {
    return (0, _logout.logout)();
  });
  it('Host and Network stats render with correct values', () => {
    cy.get(_selectors.STAT_AUDITD.domId);

    _selectors.HOST_STATS.forEach(stat => {
      cy.get(stat.domId).invoke('text').should('eq', stat.value);
    });

    _selectors.NETWORK_STATS.forEach(stat => {
      cy.get(stat.domId).invoke('text').should('eq', stat.value);
    });
  });
});