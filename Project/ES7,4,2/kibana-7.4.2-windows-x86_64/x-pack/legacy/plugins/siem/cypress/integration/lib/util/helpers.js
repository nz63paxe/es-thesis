"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.waitForTableLoad = exports.loginAndWaitForPage = exports.DEFAULT_TIMEOUT = void 0;

var _helpers = require("../login/helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/** The default time in ms to wait for a Cypress command to complete */
const DEFAULT_TIMEOUT = 30 * 1000;
/**
 * Authenticates with Kibana, visits the specified `url`, and waits for the
 * Kibana logo to be displayed before continuing
 */

exports.DEFAULT_TIMEOUT = DEFAULT_TIMEOUT;

const loginAndWaitForPage = url => {
  (0, _helpers.login)();
  cy.visit(`${Cypress.config().baseUrl}${url}`);
  cy.viewport('macbook-15');
  cy.contains('a', 'SIEM', {
    timeout: DEFAULT_TIMEOUT
  });
};

exports.loginAndWaitForPage = loginAndWaitForPage;

const waitForTableLoad = () => cy.get('[data-test-subj="paginated-table-false"]', {
  timeout: DEFAULT_TIMEOUT
});

exports.waitForTableLoad = waitForTableLoad;