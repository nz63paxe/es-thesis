"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEFAULT_INTERVAL_VALUE = exports.DEFAULT_INTERVAL_TYPE = exports.DEFAULT_INTERVAL_PAUSE = exports.DEFAULT_TO = exports.DEFAULT_FROM = exports.DEFAULT_TIMEZONE_BROWSER = exports.DEFAULT_KBN_VERSION = exports.DEFAULT_SCALE_DATE_FORMAT = exports.DEFAULT_MAX_TABLE_QUERY_SIZE = exports.DEFAULT_ANOMALY_SCORE = exports.DEFAULT_SIEM_REFRESH_INTERVAL = exports.DEFAULT_SIEM_TIME_RANGE = exports.DEFAULT_REFRESH_RATE_INTERVAL = exports.DEFAULT_TIME_RANGE = exports.DEFAULT_INDEX_KEY = exports.DEFAULT_DARK_MODE = exports.DEFAULT_DATE_FORMAT_TZ = exports.DEFAULT_DATE_FORMAT = exports.DEFAULT_BYTES_FORMAT = exports.APP_NAME = exports.APP_ID = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const APP_ID = 'siem';
exports.APP_ID = APP_ID;
const APP_NAME = 'SIEM';
exports.APP_NAME = APP_NAME;
const DEFAULT_BYTES_FORMAT = 'format:bytes:defaultPattern';
exports.DEFAULT_BYTES_FORMAT = DEFAULT_BYTES_FORMAT;
const DEFAULT_DATE_FORMAT = 'dateFormat';
exports.DEFAULT_DATE_FORMAT = DEFAULT_DATE_FORMAT;
const DEFAULT_DATE_FORMAT_TZ = 'dateFormat:tz';
exports.DEFAULT_DATE_FORMAT_TZ = DEFAULT_DATE_FORMAT_TZ;
const DEFAULT_DARK_MODE = 'theme:darkMode';
exports.DEFAULT_DARK_MODE = DEFAULT_DARK_MODE;
const DEFAULT_INDEX_KEY = 'siem:defaultIndex';
exports.DEFAULT_INDEX_KEY = DEFAULT_INDEX_KEY;
const DEFAULT_TIME_RANGE = 'timepicker:timeDefaults';
exports.DEFAULT_TIME_RANGE = DEFAULT_TIME_RANGE;
const DEFAULT_REFRESH_RATE_INTERVAL = 'timepicker:refreshIntervalDefaults';
exports.DEFAULT_REFRESH_RATE_INTERVAL = DEFAULT_REFRESH_RATE_INTERVAL;
const DEFAULT_SIEM_TIME_RANGE = 'siem:timeDefaults';
exports.DEFAULT_SIEM_TIME_RANGE = DEFAULT_SIEM_TIME_RANGE;
const DEFAULT_SIEM_REFRESH_INTERVAL = 'siem:refreshIntervalDefaults';
exports.DEFAULT_SIEM_REFRESH_INTERVAL = DEFAULT_SIEM_REFRESH_INTERVAL;
const DEFAULT_ANOMALY_SCORE = 'siem:defaultAnomalyScore';
exports.DEFAULT_ANOMALY_SCORE = DEFAULT_ANOMALY_SCORE;
const DEFAULT_MAX_TABLE_QUERY_SIZE = 10000;
exports.DEFAULT_MAX_TABLE_QUERY_SIZE = DEFAULT_MAX_TABLE_QUERY_SIZE;
const DEFAULT_SCALE_DATE_FORMAT = 'dateFormat:scaled';
exports.DEFAULT_SCALE_DATE_FORMAT = DEFAULT_SCALE_DATE_FORMAT;
const DEFAULT_KBN_VERSION = 'kbnVersion';
exports.DEFAULT_KBN_VERSION = DEFAULT_KBN_VERSION;
const DEFAULT_TIMEZONE_BROWSER = 'timezoneBrowser';
exports.DEFAULT_TIMEZONE_BROWSER = DEFAULT_TIMEZONE_BROWSER;
const DEFAULT_FROM = 'now-24h';
exports.DEFAULT_FROM = DEFAULT_FROM;
const DEFAULT_TO = 'now';
exports.DEFAULT_TO = DEFAULT_TO;
const DEFAULT_INTERVAL_PAUSE = true;
exports.DEFAULT_INTERVAL_PAUSE = DEFAULT_INTERVAL_PAUSE;
const DEFAULT_INTERVAL_TYPE = 'manual';
exports.DEFAULT_INTERVAL_TYPE = DEFAULT_INTERVAL_TYPE;
const DEFAULT_INTERVAL_VALUE = 300000; // ms

exports.DEFAULT_INTERVAL_VALUE = DEFAULT_INTERVAL_VALUE;