"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfluencersSelect = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _fields = require("../../../../../../../../common/types/fields");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var InfluencersSelect = function InfluencersSelect(_ref) {
  var fields = _ref.fields,
      changeHandler = _ref.changeHandler,
      selectedInfluencers = _ref.selectedInfluencers;
  var options = fields.filter(function (f) {
    return f.id !== _fields.EVENT_RATE_FIELD_ID;
  }).map(function (f) {
    return {
      label: f.name
    };
  }).sort(function (a, b) {
    return a.label.localeCompare(b.label);
  });
  var selection = selectedInfluencers.map(function (i) {
    return {
      label: i
    };
  });

  function onChange(selectedOptions) {
    changeHandler(selectedOptions.map(function (o) {
      return o.label;
    }));
  }

  return _react.default.createElement(_eui.EuiComboBox, {
    options: options,
    selectedOptions: selection,
    onChange: onChange,
    isClearable: false,
    "data-test-subj": "influencerSelect"
  });
};

exports.InfluencersSelect = InfluencersSelect;