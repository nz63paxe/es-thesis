"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteAnalytics = void 0;

var _i18n = require("@kbn/i18n");

var _notify = require("ui/notify");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _common = require("../../../../common");

var _common2 = require("../../components/analytics_list/common");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var deleteAnalytics =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(d) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;

            if (!(d.stats.state === _common2.DATA_FRAME_TASK_STATE.FAILED)) {
              _context.next = 4;
              break;
            }

            _context.next = 4;
            return _ml_api_service.ml.dataFrameAnalytics.stopDataFrameAnalytics(d.config.id, d.stats.state === _common2.DATA_FRAME_TASK_STATE.FAILED, true);

          case 4:
            _context.next = 6;
            return _ml_api_service.ml.dataFrameAnalytics.deleteDataFrameAnalytics(d.config.id);

          case 6:
            _notify.toastNotifications.addSuccess(_i18n.i18n.translate('xpack.ml.dataframe.analyticsList.deleteAnalyticsSuccessMessage', {
              defaultMessage: 'Request to delete data frame analytics {analyticsId} acknowledged.',
              values: {
                analyticsId: d.config.id
              }
            }));

            _context.next = 12;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](0);

            _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.analyticsList.deleteAnalyticsErrorMessage', {
              defaultMessage: 'An error occurred deleting the data frame analytics {analyticsId}: {error}',
              values: {
                analyticsId: d.config.id,
                error: JSON.stringify(_context.t0)
              }
            }));

          case 12:
            _common.refreshAnalyticsList$.next(_common.REFRESH_ANALYTICS_LIST_STATE.REFRESH);

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 9]]);
  }));

  return function deleteAnalytics(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.deleteAnalytics = deleteAnalytics;