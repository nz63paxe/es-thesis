"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRefreshAnalyticsList = exports.refreshAnalyticsList$ = exports.REFRESH_ANALYTICS_LIST_STATE = exports.isOutlierAnalysis = exports.getAnalysisType = exports.isAnalyticsIdValid = void 0;

var _react = require("react");

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _job_utils = require("../../../common/util/job_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var isAnalyticsIdValid = _job_utils.isJobIdValid;
exports.isAnalyticsIdValid = isAnalyticsIdValid;
var ANALYSIS_CONFIG_TYPE;

(function (ANALYSIS_CONFIG_TYPE) {
  ANALYSIS_CONFIG_TYPE["OUTLIER_DETECTION"] = "outlier_detection";
  ANALYSIS_CONFIG_TYPE["REGRESSION"] = "regression";
  ANALYSIS_CONFIG_TYPE["UNKNOWN"] = "unknown";
})(ANALYSIS_CONFIG_TYPE || (ANALYSIS_CONFIG_TYPE = {}));

var getAnalysisType = function getAnalysisType(analysis) {
  var keys = Object.keys(analysis);

  if (keys.length === 1) {
    return keys[0];
  }

  return ANALYSIS_CONFIG_TYPE.UNKNOWN;
};

exports.getAnalysisType = getAnalysisType;

var isOutlierAnalysis = function isOutlierAnalysis(arg) {
  var keys = Object.keys(arg);
  return keys.length === 1 && keys[0] === ANALYSIS_CONFIG_TYPE.OUTLIER_DETECTION;
};

exports.isOutlierAnalysis = isOutlierAnalysis;
var REFRESH_ANALYTICS_LIST_STATE;
exports.REFRESH_ANALYTICS_LIST_STATE = REFRESH_ANALYTICS_LIST_STATE;

(function (REFRESH_ANALYTICS_LIST_STATE) {
  REFRESH_ANALYTICS_LIST_STATE["ERROR"] = "error";
  REFRESH_ANALYTICS_LIST_STATE["IDLE"] = "idle";
  REFRESH_ANALYTICS_LIST_STATE["LOADING"] = "loading";
  REFRESH_ANALYTICS_LIST_STATE["REFRESH"] = "refresh";
})(REFRESH_ANALYTICS_LIST_STATE || (exports.REFRESH_ANALYTICS_LIST_STATE = REFRESH_ANALYTICS_LIST_STATE = {}));

var refreshAnalyticsList$ = new _rxjs.BehaviorSubject(REFRESH_ANALYTICS_LIST_STATE.IDLE);
exports.refreshAnalyticsList$ = refreshAnalyticsList$;

var useRefreshAnalyticsList = function useRefreshAnalyticsList() {
  var callback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  (0, _react.useEffect)(function () {
    var distinct$ = refreshAnalyticsList$.pipe((0, _operators.distinctUntilChanged)());
    var subscriptions = [];

    if (typeof callback.onRefresh === 'function') {
      // initial call to refresh
      callback.onRefresh();
      subscriptions.push(distinct$.pipe((0, _operators.filter)(function (state) {
        return state === REFRESH_ANALYTICS_LIST_STATE.REFRESH;
      })).subscribe(function () {
        return typeof callback.onRefresh === 'function' && callback.onRefresh();
      }));
    }

    if (typeof callback.isLoading === 'function') {
      subscriptions.push(distinct$.subscribe(function (state) {
        return typeof callback.isLoading === 'function' && callback.isLoading(state === REFRESH_ANALYTICS_LIST_STATE.LOADING);
      }));
    }

    return function () {
      subscriptions.map(function (sub) {
        return sub.unsubscribe();
      });
    };
  }, []);
  return {
    refresh: function refresh() {
      // A refresh is followed immediately by setting the state to loading
      // to trigger data fetching and loading indicators in one go.
      refreshAnalyticsList$.next(REFRESH_ANALYTICS_LIST_STATE.REFRESH);
      refreshAnalyticsList$.next(REFRESH_ANALYTICS_LIST_STATE.LOADING);
    }
  };
};

exports.useRefreshAnalyticsList = useRefreshAnalyticsList;