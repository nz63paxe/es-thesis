"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getJobConfigFromFormState = exports.getInitialState = void 0;

var _check_privilege = require("../../../../../privilege/check_privilege");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ANALYTICS_DETAULT_MODEL_MEMORY_LIMIT = '50mb';

var getInitialState = function getInitialState() {
  return {
    advancedEditorMessages: [],
    advancedEditorRawString: '',
    form: {
      createIndexPattern: false,
      destinationIndex: '',
      destinationIndexNameExists: false,
      destinationIndexNameEmpty: true,
      destinationIndexNameValid: false,
      destinationIndexPatternTitleExists: false,
      jobId: '',
      jobIdExists: false,
      jobIdEmpty: true,
      jobIdValid: false,
      sourceIndex: '',
      sourceIndexNameEmpty: true,
      sourceIndexNameValid: false
    },
    jobConfig: {},
    disabled: !(0, _check_privilege.checkPermission)('canCreateDataFrameAnalytics') || !(0, _check_privilege.checkPermission)('canStartStopDataFrameAnalytics'),
    indexNames: [],
    indexPatternTitles: [],
    indexPatternsWithNumericFields: [],
    isAdvancedEditorEnabled: false,
    isJobCreated: false,
    isJobStarted: false,
    isModalVisible: false,
    isModalButtonDisabled: false,
    isValid: false,
    jobIds: [],
    requestMessages: []
  };
};

exports.getInitialState = getInitialState;

var getJobConfigFromFormState = function getJobConfigFromFormState(formState) {
  return {
    source: {
      // If a Kibana index patterns includes commas, we need to split
      // the into an array of indices to be in the correct format for
      // the data frame analytics API.
      index: formState.sourceIndex.includes(',') ? formState.sourceIndex.split(',').map(function (d) {
        return d.trim();
      }) : formState.sourceIndex
    },
    dest: {
      index: formState.destinationIndex
    },
    analyzed_fields: {
      excludes: []
    },
    analysis: {
      outlier_detection: {}
    },
    model_memory_limit: ANALYTICS_DETAULT_MODEL_MEMORY_LIMIT
  };
};

exports.getJobConfigFromFormState = getJobConfigFromFormState;