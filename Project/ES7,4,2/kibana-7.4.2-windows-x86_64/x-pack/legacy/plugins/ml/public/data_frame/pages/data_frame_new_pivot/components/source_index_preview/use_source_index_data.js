"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSourceIndexData = exports.SOURCE_INDEX_STATUS = void 0;

var _react = require("react");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _object_utils = require("../../../../../util/object_utils");

var _common = require("../../../../common");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var SEARCH_SIZE = 1000;
var SOURCE_INDEX_STATUS;
exports.SOURCE_INDEX_STATUS = SOURCE_INDEX_STATUS;

(function (SOURCE_INDEX_STATUS) {
  SOURCE_INDEX_STATUS[SOURCE_INDEX_STATUS["UNUSED"] = 0] = "UNUSED";
  SOURCE_INDEX_STATUS[SOURCE_INDEX_STATUS["LOADING"] = 1] = "LOADING";
  SOURCE_INDEX_STATUS[SOURCE_INDEX_STATUS["LOADED"] = 2] = "LOADED";
  SOURCE_INDEX_STATUS[SOURCE_INDEX_STATUS["ERROR"] = 3] = "ERROR";
})(SOURCE_INDEX_STATUS || (exports.SOURCE_INDEX_STATUS = SOURCE_INDEX_STATUS = {}));

var useSourceIndexData = function useSourceIndexData(indexPattern, query, selectedFields, setSelectedFields) {
  var _useState = (0, _react.useState)(''),
      _useState2 = _slicedToArray(_useState, 2),
      errorMessage = _useState2[0],
      setErrorMessage = _useState2[1];

  var _useState3 = (0, _react.useState)(SOURCE_INDEX_STATUS.UNUSED),
      _useState4 = _slicedToArray(_useState3, 2),
      status = _useState4[0],
      setStatus = _useState4[1];

  var _useState5 = (0, _react.useState)([]),
      _useState6 = _slicedToArray(_useState5, 2),
      tableItems = _useState6[0],
      setTableItems = _useState6[1];

  var getSourceIndexData =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var resp, docs, newSelectedFields, flattenedFields, transformedTableItems;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setErrorMessage('');
              setStatus(SOURCE_INDEX_STATUS.LOADING);
              _context.prev = 2;
              _context.next = 5;
              return _ml_api_service.ml.esSearch({
                index: indexPattern.title,
                size: SEARCH_SIZE,
                // Instead of using the default query (`*`), fall back to a more efficient `match_all` query.
                body: {
                  query: (0, _common.isDefaultQuery)(query) ? {
                    match_all: {}
                  } : query
                }
              });

            case 5:
              resp = _context.sent;
              docs = resp.hits.hits;

              if (!(docs.length === 0)) {
                _context.next = 11;
                break;
              }

              setTableItems([]);
              setStatus(SOURCE_INDEX_STATUS.LOADED);
              return _context.abrupt("return");

            case 11:
              if (selectedFields.length === 0) {
                newSelectedFields = (0, _common.getDefaultSelectableFields)(docs);
                setSelectedFields(newSelectedFields);
              } // Create a version of the doc's source with flattened field names.
              // This avoids confusion later on if a field name has dots in its name
              // or is a nested fields when displaying it via EuiInMemoryTable.


              flattenedFields = (0, _common.getFlattenedFields)(docs[0]._source);
              transformedTableItems = docs.map(function (doc) {
                var item = {};
                flattenedFields.forEach(function (ff) {
                  item[ff] = (0, _object_utils.getNestedProperty)(doc._source, ff);

                  if (item[ff] === undefined) {
                    // If the attribute is undefined, it means it was not a nested property
                    // but had dots in its actual name. This selects the property by its
                    // full name and assigns it to `item[ff]`.
                    item[ff] = doc._source["\"".concat(ff, "\"")];
                  }
                });
                return _objectSpread({}, doc, {
                  _source: item
                });
              });
              setTableItems(transformedTableItems);
              setStatus(SOURCE_INDEX_STATUS.LOADED);
              _context.next = 23;
              break;

            case 18:
              _context.prev = 18;
              _context.t0 = _context["catch"](2);

              if (_context.t0.message !== undefined) {
                setErrorMessage(_context.t0.message);
              } else {
                setErrorMessage(JSON.stringify(_context.t0));
              }

              setTableItems([]);
              setStatus(SOURCE_INDEX_STATUS.ERROR);

            case 23:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[2, 18]]);
    }));

    return function getSourceIndexData() {
      return _ref.apply(this, arguments);
    };
  }();

  (0, _react.useEffect)(function () {
    getSourceIndexData();
  }, [indexPattern.title, JSON.stringify(query)]);
  return {
    errorMessage: errorMessage,
    status: status,
    tableItems: tableItems
  };
};

exports.useSourceIndexData = useSourceIndexData;