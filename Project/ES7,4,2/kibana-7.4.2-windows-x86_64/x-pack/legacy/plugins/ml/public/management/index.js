"use strict";

var _management = require("ui/management");

var _xpack_info = require("plugins/xpack_main/services/xpack_info");

var _i18n = require("@kbn/i18n");

var _management_urls = require("./management_urls");

var _license = require("../../common/constants/license");

require("plugins/ml/management/jobs_list");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore No declaration file for module
if (_xpack_info.xpackInfo.get('features.ml.showLinks', false) === true && _xpack_info.xpackInfo.get('features.ml.licenseType') === _license.LICENSE_TYPE.FULL) {
  _management.management.register('ml', {
    display: _i18n.i18n.translate('xpack.ml.management.mlTitle', {
      defaultMessage: 'Machine Learning'
    }),
    order: 100,
    icon: 'machineLearningApp'
  });

  _management.management.getSection('ml').register('jobsList', {
    name: 'jobsListLink',
    order: 10,
    display: _i18n.i18n.translate('xpack.ml.management.jobsListTitle', {
      defaultMessage: 'Jobs list'
    }),
    url: "#".concat(_management_urls.JOBS_LIST_PATH)
  });
}