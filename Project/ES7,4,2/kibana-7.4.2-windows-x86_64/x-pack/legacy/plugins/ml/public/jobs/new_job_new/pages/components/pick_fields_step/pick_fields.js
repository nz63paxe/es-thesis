"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PickFieldsStep = void 0;

var _react = _interopRequireWildcard(require("react"));

var _job_creator_context = require("../job_creator_context");

var _wizard_nav = require("../wizard_nav");

var _step_types = require("../step_types");

var _constants = require("../../../common/job_creator/util/constants");

var _single_metric_view = require("./components/single_metric_view");

var _multi_metric_view = require("./components/multi_metric_view");

var _population_view = require("./components/population_view");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var PickFieldsStep = function PickFieldsStep(_ref) {
  var setCurrentStep = _ref.setCurrentStep,
      isCurrentStep = _ref.isCurrentStep;

  var _useContext = (0, _react.useContext)(_job_creator_context.JobCreatorContext),
      jobCreator = _useContext.jobCreator,
      jobCreatorUpdated = _useContext.jobCreatorUpdated,
      jobValidator = _useContext.jobValidator,
      jobValidatorUpdated = _useContext.jobValidatorUpdated;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      nextActive = _useState2[0],
      setNextActive = _useState2[1];

  var _useState3 = (0, _react.useState)(jobCreator.type),
      _useState4 = _slicedToArray(_useState3, 2),
      jobType = _useState4[0],
      setJobType = _useState4[1];

  (0, _react.useEffect)(function () {
    // this shouldn't really change, but just in case we need to...
    setJobType(jobCreator.type);
  }, [jobCreatorUpdated]);
  (0, _react.useEffect)(function () {
    var active = jobCreator.detectors.length > 0 && jobValidator.bucketSpan.valid && jobValidator.duplicateDetectors.valid && jobValidator.validating === false;
    setNextActive(active);
  }, [jobValidatorUpdated]);
  return _react.default.createElement(_react.Fragment, null, isCurrentStep && _react.default.createElement(_react.Fragment, null, jobType === _constants.JOB_TYPE.SINGLE_METRIC && _react.default.createElement(_single_metric_view.SingleMetricView, {
    isActive: isCurrentStep,
    setCanProceed: setNextActive
  }), jobType === _constants.JOB_TYPE.MULTI_METRIC && _react.default.createElement(_multi_metric_view.MultiMetricView, {
    isActive: isCurrentStep,
    setCanProceed: setNextActive
  }), jobType === _constants.JOB_TYPE.POPULATION && _react.default.createElement(_population_view.PopulationView, {
    isActive: isCurrentStep,
    setCanProceed: setNextActive
  }), _react.default.createElement(_wizard_nav.WizardNav, {
    previous: function previous() {
      return setCurrentStep(_step_types.WIZARD_STEPS.TIME_RANGE);
    },
    next: function next() {
      return setCurrentStep(_step_types.WIZARD_STEPS.JOB_DETAILS);
    },
    nextActive: nextActive
  })));
};

exports.PickFieldsStep = PickFieldsStep;