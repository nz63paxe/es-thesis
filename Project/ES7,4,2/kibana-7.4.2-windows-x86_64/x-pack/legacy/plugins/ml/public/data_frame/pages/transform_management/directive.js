"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _modules = require("ui/modules");

var _i18n = require("ui/i18n");

var _page = require("./page");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var _module = _modules.uiModules.get('apps/ml', ['react']);

_module.directive('mlDataFramePage', function () {
  return {
    scope: {},
    restrict: 'E',
    link: function link(scope, element) {
      _reactDom.default.render(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_page.Page, null)), element[0]);

      element.on('$destroy', function () {
        _reactDom.default.unmountComponentAtNode(element[0]);

        scope.$destroy();
      });
    }
  };
});