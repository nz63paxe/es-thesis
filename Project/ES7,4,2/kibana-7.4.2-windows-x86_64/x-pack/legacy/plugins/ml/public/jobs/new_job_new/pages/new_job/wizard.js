"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Wizard = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _step_types = require("../components/step_types");

var _time_range_step = require("../components/time_range_step");

var _pick_fields_step = require("../components/pick_fields_step");

var _job_details_step = require("../components/job_details_step");

var _validation_step = require("../components/validation_step");

var _summary_step = require("../components/summary_step");

var _kibana = require("../../../../contexts/kibana");

var _job_creator_context = require("../components/job_creator_context");

var _new_job_capabilities_service = require("../../../../services/new_job_capabilities_service");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Wizard = function Wizard(_ref) {
  var jobCreator = _ref.jobCreator,
      chartLoader = _ref.chartLoader,
      resultsLoader = _ref.resultsLoader,
      chartInterval = _ref.chartInterval,
      jobValidator = _ref.jobValidator,
      existingJobsAndGroups = _ref.existingJobsAndGroups,
      _ref$skipTimeRangeSte = _ref.skipTimeRangeStep,
      skipTimeRangeStep = _ref$skipTimeRangeSte === void 0 ? false : _ref$skipTimeRangeSte;
  var kibanaContext = (0, _kibana.useKibanaContext)();

  var _useReducer = (0, _react.useReducer)(function (s) {
    return s + 1;
  }, 0),
      _useReducer2 = _slicedToArray(_useReducer, 2),
      jobCreatorUpdated = _useReducer2[0],
      setJobCreatorUpdate = _useReducer2[1];

  var jobCreatorUpdate = function jobCreatorUpdate() {
    return setJobCreatorUpdate(jobCreatorUpdated);
  };

  var _useReducer3 = (0, _react.useReducer)(function (s) {
    return s + 1;
  }, 0),
      _useReducer4 = _slicedToArray(_useReducer3, 2),
      jobValidatorUpdated = _useReducer4[0],
      setJobValidatorUpdate = _useReducer4[1];

  var jobCreatorContext = {
    jobCreatorUpdated: jobCreatorUpdated,
    jobCreatorUpdate: jobCreatorUpdate,
    jobCreator: jobCreator,
    chartLoader: chartLoader,
    resultsLoader: resultsLoader,
    chartInterval: chartInterval,
    jobValidator: jobValidator,
    jobValidatorUpdated: jobValidatorUpdated,
    fields: _new_job_capabilities_service.newJobCapsService.fields,
    aggs: _new_job_capabilities_service.newJobCapsService.aggs,
    existingJobsAndGroups: existingJobsAndGroups
  }; // store whether the advanced and additional sections have been expanded.
  // has to be stored at this level to ensure it's remembered on wizard step change

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      advancedExpanded = _useState2[0],
      setAdvancedExpanded = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      additionalExpanded = _useState4[0],
      setAdditionalExpanded = _useState4[1];

  var _useState5 = (0, _react.useState)(_step_types.WIZARD_STEPS.TIME_RANGE),
      _useState6 = _slicedToArray(_useState5, 2),
      currentStep = _useState6[0],
      setCurrentStep = _useState6[1];

  var _useState7 = (0, _react.useState)(_step_types.WIZARD_STEPS.TIME_RANGE),
      _useState8 = _slicedToArray(_useState7, 2),
      highestStep = _useState8[0],
      setHighestStep = _useState8[1];

  var _useState9 = (0, _react.useState)(false),
      _useState10 = _slicedToArray(_useState9, 2),
      disableSteps = _useState10[0],
      setDisableSteps = _useState10[1];

  var _useState11 = (0, _react.useState)(resultsLoader.progress),
      _useState12 = _slicedToArray(_useState11, 2),
      progress = _useState12[0],
      setProgress = _useState12[1];

  var _useState13 = (0, _react.useState)(stringifyConfigs(jobCreator.jobConfig, jobCreator.datafeedConfig)),
      _useState14 = _slicedToArray(_useState13, 2),
      stringifiedConfigs = _useState14[0],
      setStringifiedConfigs = _useState14[1];

  (0, _react.useEffect)(function () {
    jobValidator.validate(function () {
      setJobValidatorUpdate(jobValidatorUpdated);
    }); // if the job config has changed, reset the highestStep
    // compare a stringified config to ensure the configs have actually changed

    var tempConfigs = stringifyConfigs(jobCreator.jobConfig, jobCreator.datafeedConfig);

    if (tempConfigs !== stringifiedConfigs) {
      setHighestStep(currentStep);
      setStringifiedConfigs(tempConfigs);
    }
  }, [jobCreatorUpdated]);
  (0, _react.useEffect)(function () {
    jobCreator.subscribeToProgress(setProgress);

    if (skipTimeRangeStep) {
      setCurrentStep(_step_types.WIZARD_STEPS.PICK_FIELDS);
    }
  }, []); // disable the step links if the job is running

  (0, _react.useEffect)(function () {
    setDisableSteps(progress > 0);
  }, [progress]); // keep a record of the highest step reached in the wizard

  (0, _react.useEffect)(function () {
    if (currentStep >= highestStep) {
      setHighestStep(currentStep);
    }
  }, [currentStep]);

  function jumpToStep(step) {
    if (step <= highestStep) {
      setCurrentStep(step);
    }
  }

  var stepsConfig = [{
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.step.timeRangeTitle', {
      defaultMessage: 'Time range'
    }),
    onClick: function onClick() {
      return jumpToStep(_step_types.WIZARD_STEPS.TIME_RANGE);
    },
    isSelected: currentStep === _step_types.WIZARD_STEPS.TIME_RANGE,
    isComplete: currentStep > _step_types.WIZARD_STEPS.TIME_RANGE,
    disabled: disableSteps
  }, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.step.pickFieldsTitle', {
      defaultMessage: 'Pick fields'
    }),
    onClick: function onClick() {
      return jumpToStep(_step_types.WIZARD_STEPS.PICK_FIELDS);
    },
    isSelected: currentStep === _step_types.WIZARD_STEPS.PICK_FIELDS,
    isComplete: currentStep > _step_types.WIZARD_STEPS.PICK_FIELDS,
    disabled: disableSteps
  }, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.step.jobDetailsTitle', {
      defaultMessage: 'Job details'
    }),
    onClick: function onClick() {
      return jumpToStep(_step_types.WIZARD_STEPS.JOB_DETAILS);
    },
    isSelected: currentStep === _step_types.WIZARD_STEPS.JOB_DETAILS,
    isComplete: currentStep > _step_types.WIZARD_STEPS.JOB_DETAILS,
    disabled: disableSteps
  }, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.step.validationTitle', {
      defaultMessage: 'Validation'
    }),
    onClick: function onClick() {
      return jumpToStep(_step_types.WIZARD_STEPS.VALIDATION);
    },
    isSelected: currentStep === _step_types.WIZARD_STEPS.VALIDATION,
    isComplete: currentStep > _step_types.WIZARD_STEPS.VALIDATION,
    disabled: disableSteps
  }, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.step.summaryTitle', {
      defaultMessage: 'Summary'
    }),
    onClick: function onClick() {
      return jumpToStep(_step_types.WIZARD_STEPS.SUMMARY);
    },
    isSelected: currentStep === _step_types.WIZARD_STEPS.SUMMARY,
    isComplete: currentStep > _step_types.WIZARD_STEPS.SUMMARY,
    disabled: disableSteps
  }];

  function getSummaryStepTitle() {
    if (kibanaContext.currentSavedSearch.id !== undefined) {
      return _i18n.i18n.translate('xpack.ml.newJob.wizard.stepComponentWrapper.summaryTitleSavedSearch', {
        defaultMessage: 'New job from saved search {title}',
        values: {
          title: kibanaContext.currentSavedSearch.title
        }
      });
    } else if (kibanaContext.currentIndexPattern.id !== undefined) {
      return _i18n.i18n.translate('xpack.ml.newJob.wizard.stepComponentWrapper.summaryTitleIndexPattern', {
        defaultMessage: 'New job from index pattern {title}',
        values: {
          title: kibanaContext.currentIndexPattern.title
        }
      });
    }

    return '';
  }

  return _react.default.createElement(_job_creator_context.JobCreatorContext.Provider, {
    value: jobCreatorContext
  }, _react.default.createElement(_eui.EuiStepsHorizontal, {
    steps: stepsConfig,
    style: {
      backgroundColor: 'inherit'
    }
  }), currentStep === _step_types.WIZARD_STEPS.TIME_RANGE && _react.default.createElement(_react.Fragment, null, _react.default.createElement(Title, {
    "data-test-subj": "mlJobWizardStepTitleTimeRange"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.stepComponentWrapper.timeRangeTitle",
    defaultMessage: "Time range"
  })), _react.default.createElement(_time_range_step.TimeRangeStep, {
    isCurrentStep: currentStep === _step_types.WIZARD_STEPS.TIME_RANGE,
    setCurrentStep: setCurrentStep
  })), currentStep === _step_types.WIZARD_STEPS.PICK_FIELDS && _react.default.createElement(_react.Fragment, null, _react.default.createElement(Title, {
    "data-test-subj": "mlJobWizardStepTitlePickFields"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.stepComponentWrapper.pickFieldsTitle",
    defaultMessage: "Pick fields"
  })), _react.default.createElement(_pick_fields_step.PickFieldsStep, {
    isCurrentStep: currentStep === _step_types.WIZARD_STEPS.PICK_FIELDS,
    setCurrentStep: setCurrentStep
  })), currentStep === _step_types.WIZARD_STEPS.JOB_DETAILS && _react.default.createElement(_react.Fragment, null, _react.default.createElement(Title, {
    "data-test-subj": "mlJobWizardStepTitleJobDetails"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.stepComponentWrapper.jobDetailsTitle",
    defaultMessage: "Job details"
  })), _react.default.createElement(_job_details_step.JobDetailsStep, {
    isCurrentStep: currentStep === _step_types.WIZARD_STEPS.JOB_DETAILS,
    setCurrentStep: setCurrentStep,
    advancedExpanded: advancedExpanded,
    setAdvancedExpanded: setAdvancedExpanded,
    additionalExpanded: additionalExpanded,
    setAdditionalExpanded: setAdditionalExpanded
  })), currentStep === _step_types.WIZARD_STEPS.VALIDATION && _react.default.createElement(_react.Fragment, null, _react.default.createElement(Title, {
    "data-test-subj": "mlJobWizardStepTitleValidation"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.stepComponentWrapper.validationTitle",
    defaultMessage: "Validation"
  })), _react.default.createElement(_validation_step.ValidationStep, {
    isCurrentStep: currentStep === _step_types.WIZARD_STEPS.VALIDATION,
    setCurrentStep: setCurrentStep
  })), currentStep === _step_types.WIZARD_STEPS.SUMMARY && _react.default.createElement(_react.Fragment, null, _react.default.createElement(Title, {
    "data-test-subj": "mlJobWizardStepTitleSummary"
  }, getSummaryStepTitle()), _react.default.createElement(_summary_step.SummaryStep, {
    isCurrentStep: currentStep === _step_types.WIZARD_STEPS.SUMMARY,
    setCurrentStep: setCurrentStep
  })));
};

exports.Wizard = Wizard;

var Title = function Title(_ref2) {
  var dataTestSubj = _ref2['data-test-subj'],
      children = _ref2.children;
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h2", {
    "data-test-subj": dataTestSubj
  }, children)), _react.default.createElement(_eui.EuiSpacer, null));
};

function stringifyConfigs(jobConfig, datafeedConfig) {
  return JSON.stringify(jobConfig) + JSON.stringify(datafeedConfig);
}