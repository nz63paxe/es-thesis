"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getErrorMessage = getErrorMessage;
exports.useCreateAnalyticsForm = void 0;

var _react = require("react");

var _i18n = require("@kbn/i18n");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _kibana = require("../../../../../contexts/kibana");

var _common = require("../../../../common");

var _actions = require("./actions");

var _reducer = require("./reducer");

var _state = require("./state");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// List of system fields we want to ignore for the numeric field check.
var OMIT_FIELDS = ['_source', '_type', '_index', '_id', '_version', '_score'];

function getErrorMessage(error) {
  if (_typeof(error) === 'object' && typeof error.message === 'string') {
    return error.message;
  }

  return JSON.stringify(error);
}

var useCreateAnalyticsForm = function useCreateAnalyticsForm() {
  var kibanaContext = (0, _kibana.useKibanaContext)();

  var _useReducer = (0, _react.useReducer)(_reducer.reducer, (0, _state.getInitialState)()),
      _useReducer2 = _slicedToArray(_useReducer, 2),
      state = _useReducer2[0],
      dispatch = _useReducer2[1];

  var _useRefreshAnalyticsL = (0, _common.useRefreshAnalyticsList)(),
      refresh = _useRefreshAnalyticsL.refresh;

  var form = state.form,
      jobConfig = state.jobConfig,
      isAdvancedEditorEnabled = state.isAdvancedEditorEnabled;
  var createIndexPattern = form.createIndexPattern,
      destinationIndex = form.destinationIndex,
      jobId = form.jobId;

  var addRequestMessage = function addRequestMessage(requestMessage) {
    return dispatch({
      type: _actions.ACTION.ADD_REQUEST_MESSAGE,
      requestMessage: requestMessage
    });
  };

  var closeModal = function closeModal() {
    return dispatch({
      type: _actions.ACTION.CLOSE_MODAL
    });
  };

  var resetAdvancedEditorMessages = function resetAdvancedEditorMessages() {
    return dispatch({
      type: _actions.ACTION.RESET_ADVANCED_EDITOR_MESSAGES
    });
  };

  var setIndexNames = function setIndexNames(indexNames) {
    return dispatch({
      type: _actions.ACTION.SET_INDEX_NAMES,
      indexNames: indexNames
    });
  };

  var setAdvancedEditorRawString = function setAdvancedEditorRawString(advancedEditorRawString) {
    return dispatch({
      type: _actions.ACTION.SET_ADVANCED_EDITOR_RAW_STRING,
      advancedEditorRawString: advancedEditorRawString
    });
  };

  var setIndexPatternTitles = function setIndexPatternTitles(payload) {
    return dispatch({
      type: _actions.ACTION.SET_INDEX_PATTERN_TITLES,
      payload: payload
    });
  };

  var setIsJobCreated = function setIsJobCreated(isJobCreated) {
    return dispatch({
      type: _actions.ACTION.SET_IS_JOB_CREATED,
      isJobCreated: isJobCreated
    });
  };

  var setIsJobStarted = function setIsJobStarted(isJobStarted) {
    dispatch({
      type: _actions.ACTION.SET_IS_JOB_STARTED,
      isJobStarted: isJobStarted
    });
  };

  var setIsModalButtonDisabled = function setIsModalButtonDisabled(isModalButtonDisabled) {
    return dispatch({
      type: _actions.ACTION.SET_IS_MODAL_BUTTON_DISABLED,
      isModalButtonDisabled: isModalButtonDisabled
    });
  };

  var setIsModalVisible = function setIsModalVisible(isModalVisible) {
    return dispatch({
      type: _actions.ACTION.SET_IS_MODAL_VISIBLE,
      isModalVisible: isModalVisible
    });
  };

  var setJobIds = function setJobIds(jobIds) {
    return dispatch({
      type: _actions.ACTION.SET_JOB_IDS,
      jobIds: jobIds
    });
  };

  var resetRequestMessages = function resetRequestMessages() {
    return dispatch({
      type: _actions.ACTION.RESET_REQUEST_MESSAGES
    });
  };

  var resetForm = function resetForm() {
    return dispatch({
      type: _actions.ACTION.RESET_FORM
    });
  };

  var createAnalyticsJob =
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var analyticsJobConfig;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              resetRequestMessages();
              setIsModalButtonDisabled(true);
              analyticsJobConfig = isAdvancedEditorEnabled ? jobConfig : (0, _state.getJobConfigFromFormState)(form);
              _context.prev = 3;
              _context.next = 6;
              return _ml_api_service.ml.dataFrameAnalytics.createDataFrameAnalytics(jobId, analyticsJobConfig);

            case 6:
              addRequestMessage({
                message: _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createDataFrameAnalyticsSuccessMessage', {
                  defaultMessage: 'Request to create data frame analytics {jobId} acknowledged.',
                  values: {
                    jobId: jobId
                  }
                })
              });
              setIsModalButtonDisabled(false);
              setIsJobCreated(true);

              if (createIndexPattern) {
                createKibanaIndexPattern();
              }

              refresh();
              _context.next = 17;
              break;

            case 13:
              _context.prev = 13;
              _context.t0 = _context["catch"](3);
              addRequestMessage({
                error: getErrorMessage(_context.t0),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.errorCreatingDataFrameAnalyticsJob', {
                  defaultMessage: 'An error occurred creating the data frame analytics job:'
                })
              });
              setIsModalButtonDisabled(false);

            case 17:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[3, 13]]);
    }));

    return function createAnalyticsJob() {
      return _ref.apply(this, arguments);
    };
  }();

  var createKibanaIndexPattern =
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var indexPatternName, newIndexPattern, id;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              indexPatternName = destinationIndex;
              _context2.prev = 1;
              _context2.next = 4;
              return kibanaContext.indexPatterns.get();

            case 4:
              newIndexPattern = _context2.sent;
              Object.assign(newIndexPattern, {
                id: '',
                title: indexPatternName
              });
              _context2.next = 8;
              return newIndexPattern.create();

            case 8:
              id = _context2.sent;

              if (!(id === false)) {
                _context2.next = 12;
                break;
              }

              addRequestMessage({
                error: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.duplicateIndexPatternErrorMessageError', {
                  defaultMessage: 'The index pattern {indexPatternName} already exists.',
                  values: {
                    indexPatternName: indexPatternName
                  }
                }),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.duplicateIndexPatternErrorMessage', {
                  defaultMessage: 'An error occurred creating the Kibana index pattern:'
                })
              });
              return _context2.abrupt("return");

            case 12:
              if (kibanaContext.kibanaConfig.get('defaultIndex')) {
                _context2.next = 15;
                break;
              }

              _context2.next = 15;
              return kibanaContext.kibanaConfig.set('defaultIndex', id);

            case 15:
              addRequestMessage({
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.createIndexPatternSuccessMessage', {
                  defaultMessage: 'Kibana index pattern {indexPatternName} created.',
                  values: {
                    indexPatternName: indexPatternName
                  }
                })
              });
              _context2.next = 21;
              break;

            case 18:
              _context2.prev = 18;
              _context2.t0 = _context2["catch"](1);
              addRequestMessage({
                error: getErrorMessage(_context2.t0),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.createIndexPatternErrorMessage', {
                  defaultMessage: 'An error occurred creating the Kibana index pattern:'
                })
              });

            case 21:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[1, 18]]);
    }));

    return function createKibanaIndexPattern() {
      return _ref2.apply(this, arguments);
    };
  }();

  var openModal =
  /*#__PURE__*/
  function () {
    var _ref3 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4() {
      var indexPatternTitles, ids, indexPatternsWithNumericFields;
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              resetForm(); // re-fetch existing analytics job IDs and indices for form validation

              _context4.prev = 1;
              _context4.t0 = setJobIds;
              _context4.next = 5;
              return _ml_api_service.ml.dataFrameAnalytics.getDataFrameAnalytics();

            case 5:
              _context4.t1 = function (job) {
                return job.id;
              };

              _context4.t2 = _context4.sent.data_frame_analytics.map(_context4.t1);
              (0, _context4.t0)(_context4.t2);
              _context4.next = 13;
              break;

            case 10:
              _context4.prev = 10;
              _context4.t3 = _context4["catch"](1);
              addRequestMessage({
                error: getErrorMessage(_context4.t3),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.errorGettingDataFrameAnalyticsList', {
                  defaultMessage: 'An error occurred getting the existing data frame analytics job IDs:'
                })
              });

            case 13:
              _context4.prev = 13;
              _context4.t4 = setIndexNames;
              _context4.next = 17;
              return _ml_api_service.ml.getIndices();

            case 17:
              _context4.t5 = function (index) {
                return index.name;
              };

              _context4.t6 = _context4.sent.map(_context4.t5);
              (0, _context4.t4)(_context4.t6);
              _context4.next = 25;
              break;

            case 22:
              _context4.prev = 22;
              _context4.t7 = _context4["catch"](13);
              addRequestMessage({
                error: getErrorMessage(_context4.t7),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.errorGettingDataFrameIndexNames', {
                  defaultMessage: 'An error occurred getting the existing index names:'
                })
              });

            case 25:
              _context4.prev = 25;
              _context4.next = 28;
              return kibanaContext.indexPatterns.getTitles(true);

            case 28:
              indexPatternTitles = _context4.sent;
              _context4.next = 31;
              return kibanaContext.indexPatterns.getIds(true);

            case 31:
              ids = _context4.sent;
              indexPatternsWithNumericFields = [];
              ids.forEach(
              /*#__PURE__*/
              function () {
                var _ref4 = _asyncToGenerator(
                /*#__PURE__*/
                regeneratorRuntime.mark(function _callee3(id) {
                  var indexPattern;
                  return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                      switch (_context3.prev = _context3.next) {
                        case 0:
                          _context3.next = 2;
                          return kibanaContext.indexPatterns.get(id);

                        case 2:
                          indexPattern = _context3.sent;

                          if (indexPattern.fields.filter(function (f) {
                            return !OMIT_FIELDS.includes(f.name);
                          }).map(function (f) {
                            return f.type;
                          }).includes('number')) {
                            indexPatternsWithNumericFields.push(indexPattern.title);
                          }

                        case 4:
                        case "end":
                          return _context3.stop();
                      }
                    }
                  }, _callee3);
                }));

                return function (_x) {
                  return _ref4.apply(this, arguments);
                };
              }());
              setIndexPatternTitles({
                indexPatternTitles: indexPatternTitles,
                indexPatternsWithNumericFields: indexPatternsWithNumericFields
              });
              _context4.next = 40;
              break;

            case 37:
              _context4.prev = 37;
              _context4.t8 = _context4["catch"](25);
              addRequestMessage({
                error: getErrorMessage(_context4.t8),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.errorGettingIndexPatternTitles', {
                  defaultMessage: 'An error occurred getting the existing index pattern titles:'
                })
              });

            case 40:
              dispatch({
                type: _actions.ACTION.OPEN_MODAL
              });

            case 41:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[1, 10], [13, 22], [25, 37]]);
    }));

    return function openModal() {
      return _ref3.apply(this, arguments);
    };
  }();

  var startAnalyticsJob =
  /*#__PURE__*/
  function () {
    var _ref5 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5() {
      var response;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              setIsModalButtonDisabled(true);
              _context5.prev = 1;
              _context5.next = 4;
              return _ml_api_service.ml.dataFrameAnalytics.startDataFrameAnalytics(jobId);

            case 4:
              response = _context5.sent;

              if (!(response.acknowledged !== true)) {
                _context5.next = 7;
                break;
              }

              throw new Error(response);

            case 7:
              addRequestMessage({
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.startDataFrameAnalyticsSuccessMessage', {
                  defaultMessage: 'Request to start data frame analytics {jobId} acknowledged.',
                  values: {
                    jobId: jobId
                  }
                })
              });
              setIsJobStarted(true);
              setIsModalButtonDisabled(false);
              refresh();
              _context5.next = 17;
              break;

            case 13:
              _context5.prev = 13;
              _context5.t0 = _context5["catch"](1);
              addRequestMessage({
                error: getErrorMessage(_context5.t0),
                message: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.errorStartingDataFrameAnalyticsJob', {
                  defaultMessage: 'An error occurred starting the data frame analytics job:'
                })
              });
              setIsModalButtonDisabled(false);

            case 17:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[1, 13]]);
    }));

    return function startAnalyticsJob() {
      return _ref5.apply(this, arguments);
    };
  }();

  var setJobConfig = function setJobConfig(payload) {
    dispatch({
      type: _actions.ACTION.SET_JOB_CONFIG,
      payload: payload
    });
  };

  var setFormState = function setFormState(payload) {
    dispatch({
      type: _actions.ACTION.SET_FORM_STATE,
      payload: payload
    });
  };

  var switchToAdvancedEditor = function switchToAdvancedEditor() {
    dispatch({
      type: _actions.ACTION.SWITCH_TO_ADVANCED_EDITOR
    });
  };

  var actions = {
    closeModal: closeModal,
    createAnalyticsJob: createAnalyticsJob,
    openModal: openModal,
    resetAdvancedEditorMessages: resetAdvancedEditorMessages,
    setAdvancedEditorRawString: setAdvancedEditorRawString,
    setFormState: setFormState,
    setIsModalVisible: setIsModalVisible,
    setJobConfig: setJobConfig,
    startAnalyticsJob: startAnalyticsJob,
    switchToAdvancedEditor: switchToAdvancedEditor
  };
  return {
    state: state,
    actions: actions
  };
};

exports.useCreateAnalyticsForm = useCreateAnalyticsForm;