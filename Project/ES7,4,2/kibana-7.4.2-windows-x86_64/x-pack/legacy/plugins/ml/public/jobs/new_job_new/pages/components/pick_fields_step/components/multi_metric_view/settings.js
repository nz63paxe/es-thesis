"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MultiMetricSettings = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _job_creator_context = require("../../../job_creator_context");

var _bucket_span = require("../bucket_span");

var _split_field = require("../split_field");

var _influencers = require("../influencers");

var _sparse_data = require("../sparse_data");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var MultiMetricSettings = function MultiMetricSettings(_ref) {
  var setIsValid = _ref.setIsValid;

  var _useContext = (0, _react.useContext)(_job_creator_context.JobCreatorContext),
      jobCreator = _useContext.jobCreator,
      jobCreatorUpdate = _useContext.jobCreatorUpdate,
      jobCreatorUpdated = _useContext.jobCreatorUpdated;

  var _useState = (0, _react.useState)(jobCreator.bucketSpan),
      _useState2 = _slicedToArray(_useState, 2),
      bucketSpan = _useState2[0],
      setBucketSpan = _useState2[1];

  (0, _react.useEffect)(function () {
    jobCreator.bucketSpan = bucketSpan;
    jobCreatorUpdate();
    setIsValid(bucketSpan !== '');
  }, [bucketSpan]);
  (0, _react.useEffect)(function () {
    setBucketSpan(jobCreator.bucketSpan);
  }, [jobCreatorUpdated]);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xl"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_split_field.SplitFieldSelector, null)), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_influencers.Influencers, null))), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xl"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_bucket_span.BucketSpan, {
    setIsValid: setIsValid
  })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_sparse_data.SparseDataSwitch, null))));
};

exports.MultiMetricSettings = MultiMetricSettings;