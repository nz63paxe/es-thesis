"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDefaultStepCreateState = getDefaultStepCreateState;
exports.StepCreateForm = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _notify = require("ui/notify");

var _eui = require("@elastic/eui");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _use_kibana_context = require("../../../../../contexts/kibana/use_kibana_context");

var _use_ui_chrome_context = require("../../../../../contexts/ui/use_ui_chrome_context");

var _jobs_list = require("../../../../../../common/constants/jobs_list");

var _common = require("../../../../common");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function getDefaultStepCreateState() {
  return {
    created: false,
    started: false,
    indexPatternId: undefined
  };
}

var StepCreateForm = _react.default.memo(function (_ref) {
  var createIndexPattern = _ref.createIndexPattern,
      transformConfig = _ref.transformConfig,
      transformId = _ref.transformId,
      onChange = _ref.onChange,
      overrides = _ref.overrides;

  var defaults = _objectSpread({}, getDefaultStepCreateState(), {}, overrides);

  var _useState = (0, _react.useState)(defaults.created),
      _useState2 = _slicedToArray(_useState, 2),
      created = _useState2[0],
      setCreated = _useState2[1];

  var _useState3 = (0, _react.useState)(defaults.started),
      _useState4 = _slicedToArray(_useState3, 2),
      started = _useState4[0],
      setStarted = _useState4[1];

  var _useState5 = (0, _react.useState)(defaults.indexPatternId),
      _useState6 = _slicedToArray(_useState5, 2),
      indexPatternId = _useState6[0],
      setIndexPatternId = _useState6[1];

  var _useState7 = (0, _react.useState)(undefined),
      _useState8 = _slicedToArray(_useState7, 2),
      progressPercentComplete = _useState8[0],
      setProgressPercentComplete = _useState8[1];

  var kibanaContext = (0, _use_kibana_context.useKibanaContext)();
  var baseUrl = (0, _use_ui_chrome_context.useUiChromeContext)().addBasePath(kibanaContext.kbnBaseUrl);
  (0, _react.useEffect)(function () {
    onChange({
      created: created,
      started: started,
      indexPatternId: indexPatternId
    });
  }, [created, started, indexPatternId]);

  function createDataFrame() {
    return _createDataFrame.apply(this, arguments);
  }

  function _createDataFrame() {
    _createDataFrame = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3() {
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              setCreated(true);
              _context3.prev = 1;
              _context3.next = 4;
              return _ml_api_service.ml.dataFrame.createDataFrameTransform(transformId, transformConfig);

            case 4:
              _notify.toastNotifications.addSuccess(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createTransformSuccessMessage', {
                defaultMessage: 'Request to create data frame transform {transformId} acknowledged.',
                values: {
                  transformId: transformId
                }
              }));

              _context3.next = 12;
              break;

            case 7:
              _context3.prev = 7;
              _context3.t0 = _context3["catch"](1);
              setCreated(false);

              _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createTransformErrorMessage', {
                defaultMessage: 'An error occurred creating the data frame transform {transformId}: {error}',
                values: {
                  transformId: transformId,
                  error: JSON.stringify(_context3.t0)
                }
              }));

              return _context3.abrupt("return", false);

            case 12:
              if (createIndexPattern) {
                createKibanaIndexPattern();
              }

              return _context3.abrupt("return", true);

            case 14:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[1, 7]]);
    }));
    return _createDataFrame.apply(this, arguments);
  }

  function startDataFrame() {
    return _startDataFrame.apply(this, arguments);
  }

  function _startDataFrame() {
    _startDataFrame = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee4() {
      return regeneratorRuntime.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              setStarted(true);
              _context4.prev = 1;
              _context4.next = 4;
              return _ml_api_service.ml.dataFrame.startDataFrameTransforms([{
                id: transformId
              }]);

            case 4:
              _notify.toastNotifications.addSuccess(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.startTransformSuccessMessage', {
                defaultMessage: 'Request to start data frame transform {transformId} acknowledged.',
                values: {
                  transformId: transformId
                }
              }));

              _context4.next = 11;
              break;

            case 7:
              _context4.prev = 7;
              _context4.t0 = _context4["catch"](1);
              setStarted(false);

              _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.startTransformErrorMessage', {
                defaultMessage: 'An error occurred starting the data frame transform {transformId}: {error}',
                values: {
                  transformId: transformId,
                  error: JSON.stringify(_context4.t0)
                }
              }));

            case 11:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[1, 7]]);
    }));
    return _startDataFrame.apply(this, arguments);
  }

  function createAndStartDataFrame() {
    return _createAndStartDataFrame.apply(this, arguments);
  }

  function _createAndStartDataFrame() {
    _createAndStartDataFrame = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee5() {
      var success;
      return regeneratorRuntime.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _context5.next = 2;
              return createDataFrame();

            case 2:
              success = _context5.sent;

              if (!success) {
                _context5.next = 6;
                break;
              }

              _context5.next = 6;
              return startDataFrame();

            case 6:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5);
    }));
    return _createAndStartDataFrame.apply(this, arguments);
  }

  var createKibanaIndexPattern =
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var indexPatternName, newIndexPattern, id;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              indexPatternName = transformConfig.dest.index;
              _context.prev = 1;
              _context.next = 4;
              return kibanaContext.indexPatterns.get();

            case 4:
              newIndexPattern = _context.sent;
              Object.assign(newIndexPattern, {
                id: '',
                title: indexPatternName
              });
              _context.next = 8;
              return newIndexPattern.create();

            case 8:
              id = _context.sent;

              if (!(id === false)) {
                _context.next = 12;
                break;
              }

              _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.duplicateIndexPatternErrorMessage', {
                defaultMessage: 'An error occurred creating the Kibana index pattern {indexPatternName}: The index pattern already exists.',
                values: {
                  indexPatternName: indexPatternName
                }
              }));

              return _context.abrupt("return");

            case 12:
              if (kibanaContext.kibanaConfig.get('defaultIndex')) {
                _context.next = 15;
                break;
              }

              _context.next = 15;
              return kibanaContext.kibanaConfig.set('defaultIndex', id);

            case 15:
              _notify.toastNotifications.addSuccess(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createIndexPatternSuccessMessage', {
                defaultMessage: 'Kibana index pattern {indexPatternName} created successfully.',
                values: {
                  indexPatternName: indexPatternName
                }
              }));

              setIndexPatternId(id);
              return _context.abrupt("return", true);

            case 20:
              _context.prev = 20;
              _context.t0 = _context["catch"](1);

              _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createIndexPatternErrorMessage', {
                defaultMessage: 'An error occurred creating the Kibana index pattern {indexPatternName}: {error}',
                values: {
                  indexPatternName: indexPatternName,
                  error: JSON.stringify(_context.t0)
                }
              }));

              return _context.abrupt("return", false);

            case 24:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[1, 20]]);
    }));

    return function createKibanaIndexPattern() {
      return _ref2.apply(this, arguments);
    };
  }();

  var isBatchTransform = typeof transformConfig.sync === 'undefined';

  if (started === true && progressPercentComplete === undefined && isBatchTransform) {
    // wrapping in function so we can keep the interval id in local scope
    var startProgressBar = function startProgressBar() {
      var interval = setInterval(
      /*#__PURE__*/
      _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var stats, percent;
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _ml_api_service.ml.dataFrame.getDataFrameTransformsStats(transformId);

              case 3:
                stats = _context2.sent;

                if (stats && Array.isArray(stats.transforms) && stats.transforms.length > 0) {
                  percent = (0, _common.getTransformProgress)({
                    id: transformConfig.id,
                    config: transformConfig,
                    stats: stats.transforms[0]
                  }) || 0;
                  setProgressPercentComplete(percent);

                  if (percent >= 100) {
                    clearInterval(interval);
                  }
                }

                _context2.next = 11;
                break;

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);

                _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.progressErrorMessage', {
                  defaultMessage: 'An error occurred getting the progress percentage: {error}',
                  values: {
                    error: JSON.stringify(_context2.t0)
                  }
                }));

                clearInterval(interval);

              case 11:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 7]]);
      })), _jobs_list.PROGRESS_JOBS_REFRESH_INTERVAL_MS);
      setProgressPercentComplete(0);
    };

    startProgressBar();
  }

  function getTransformConfigDevConsoleStatement() {
    return "PUT _data_frame/transforms/".concat(transformId, "\n").concat(JSON.stringify(transformConfig, null, 2), "\n\n");
  } // TODO move this to SASS


  var FLEX_GROUP_STYLE = {
    height: '90px',
    maxWidth: '800px'
  };
  var FLEX_ITEM_STYLE = {
    width: '200px'
  };
  var PANEL_ITEM_STYLE = {
    width: '300px'
  };
  return _react.default.createElement(_eui.EuiForm, null, !created && _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    style: FLEX_GROUP_STYLE
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: FLEX_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    isDisabled: created && started,
    onClick: createAndStartDataFrame
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createAndStartDataFrameButton', {
    defaultMessage: 'Create and start'
  }))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createAndStartDataFrameDescription', {
    defaultMessage: 'Creates and starts the data frame transform. A data frame transform will increase search and indexing load in your cluster. Please stop the transform if excessive load is experienced. After the transform is started, you will be offered options to continue exploring the data frame transform.'
  })))), created && _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    style: FLEX_GROUP_STYLE
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: FLEX_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    isDisabled: created && started,
    onClick: startDataFrame
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.startDataFrameButton', {
    defaultMessage: 'Start'
  }))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.startDataFrameDescription', {
    defaultMessage: 'Starts the data frame transform. A data frame transform will increase search and indexing load in your cluster. Please stop the transform if excessive load is experienced. After the transform is started, you will be offered options to continue exploring the data frame transform.'
  })))), _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    style: FLEX_GROUP_STYLE
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: FLEX_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiButton, {
    isDisabled: created,
    onClick: createDataFrame
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createDataFrameButton', {
    defaultMessage: 'Create'
  }))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.createDataFrameDescription', {
    defaultMessage: 'Create the data frame transform without starting it. You will be able to start the transform later by returning to the data frame transforms list.'
  })))), _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    style: FLEX_GROUP_STYLE
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: FLEX_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiCopy, {
    textToCopy: getTransformConfigDevConsoleStatement()
  }, function (copy) {
    return _react.default.createElement(_eui.EuiButton, {
      onClick: copy,
      style: {
        width: '100%'
      }
    }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.copyTransformConfigToClipboardButton', {
      defaultMessage: 'Copy to clipboard'
    }));
  })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.copyTransformConfigToClipboardDescription', {
    defaultMessage: 'Copies to the clipboard the Kibana Dev Console command for creating the transform.'
  })))), progressPercentComplete !== undefined && isBatchTransform && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiText, {
    size: "xs"
  }, _react.default.createElement("strong", null, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.progressTitle', {
    defaultMessage: 'Progress'
  }))), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xs"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    style: {
      width: '400px'
    },
    grow: false
  }, _react.default.createElement(_eui.EuiProgress, {
    size: "l",
    color: "primary",
    value: progressPercentComplete,
    max: 100
  })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
    size: "xs"
  }, progressPercentComplete, "%")))), created && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_eui.EuiFlexGrid, {
    gutterSize: "l"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    style: PANEL_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiCard, {
    icon: _react.default.createElement(_eui.EuiIcon, {
      size: "xxl",
      type: "list"
    }),
    title: _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.transformListCardTitle', {
      defaultMessage: 'Data frame transforms'
    }),
    description: _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.transformListCardDescription', {
      defaultMessage: 'Return to the data frame transform management page.'
    }),
    href: "#/data_frames"
  })), started === true && createIndexPattern === true && indexPatternId === undefined && _react.default.createElement(_eui.EuiFlexItem, {
    style: PANEL_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiPanel, {
    style: {
      position: 'relative'
    }
  }, _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "primary",
    position: "absolute"
  }), _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "s"
  }, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.creatingIndexPatternMessage', {
    defaultMessage: 'Creating Kibana index pattern ...'
  }))))), started === true && indexPatternId !== undefined && _react.default.createElement(_eui.EuiFlexItem, {
    style: PANEL_ITEM_STYLE
  }, _react.default.createElement(_eui.EuiCard, {
    icon: _react.default.createElement(_eui.EuiIcon, {
      size: "xxl",
      type: "discoverApp"
    }),
    title: _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.discoverCardTitle', {
      defaultMessage: 'Discover'
    }),
    description: _i18n.i18n.translate('xpack.ml.dataframe.stepCreateForm.discoverCardDescription', {
      defaultMessage: 'Use Discover to explore the data frame pivot.'
    }),
    href: (0, _common.getDiscoverUrl)(indexPatternId, baseUrl)
  })))));
});

exports.StepCreateForm = StepCreateForm;