"use strict";

var _reactDom = _interopRequireWildcard(require("react-dom"));

var _react = _interopRequireDefault(require("react"));

var _routes = _interopRequireDefault(require("ui/routes"));

var _check_privilege = require("../../privilege/check_privilege");

var _management_urls = require("../management_urls");

var _components = require("./components");

var _breadcrumbs = require("../breadcrumbs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var template = "<kbn-management-app section=\"ml/jobs-list\">\n<div id=\"kibanaManagementMLSection\" />\n</kbn-management-app>";

_routes.default.when(_management_urls.JOBS_LIST_PATH, {
  template: template,
  k7Breadcrumbs: _breadcrumbs.getJobsListBreadcrumbs,
  resolve: {
    checkPrivilege: _check_privilege.canGetManagementMlJobs
  },
  controller: function controller($scope, checkPrivilege) {
    var mlFeatureEnabledInSpace = checkPrivilege.mlFeatureEnabledInSpace;
    $scope.$on('$destroy', function () {
      var elem = document.getElementById('kibanaManagementMLSection');
      if (elem) (0, _reactDom.unmountComponentAtNode)(elem);
    });
    $scope.$$postDigest(function () {
      var element = document.getElementById('kibanaManagementMLSection');

      _reactDom.default.render(_react.default.createElement(_components.JobsListPage, {
        isMlEnabledInSpace: mlFeatureEnabledInSpace
      }), element);
    });
  }
});

_routes.default.when(_management_urls.ACCESS_DENIED_PATH, {
  template: template,
  k7Breadcrumbs: _breadcrumbs.getJobsListBreadcrumbs,
  controller: function controller($scope) {
    $scope.$on('$destroy', function () {
      var elem = document.getElementById('kibanaManagementMLSection');
      if (elem) (0, _reactDom.unmountComponentAtNode)(elem);
    });
    $scope.$$postDigest(function () {
      var element = document.getElementById('kibanaManagementMLSection');
      (0, _reactDom.render)((0, _components.AccessDeniedPage)(), element);
    });
  }
});