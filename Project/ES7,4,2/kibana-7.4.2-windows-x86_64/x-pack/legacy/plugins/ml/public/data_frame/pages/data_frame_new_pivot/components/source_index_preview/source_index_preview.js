"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceIndexPreview = void 0;

var _react = _interopRequireWildcard(require("react"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _in_memory_table = require("../../../../../../common/types/eui/in_memory_table");

var _field_types = require("../../../../../../common/constants/field_types");

var _date_utils = require("../../../../../util/date_utils");

var _kibana = require("../../../../../contexts/kibana");

var _common = require("../../../../common");

var _common2 = require("./common");

var _expanded_row = require("./expanded_row");

var _use_source_index_data = require("./use_source_index_data");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var CELL_CLICK_ENABLED = false;

var SourceIndexPreviewTitle = function SourceIndexPreviewTitle(_ref) {
  var indexPatternTitle = _ref.indexPatternTitle;
  return _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("span", null, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.sourceIndexPatternTitle', {
    defaultMessage: 'Source index {indexPatternTitle}',
    values: {
      indexPatternTitle: indexPatternTitle
    }
  })));
};

var SourceIndexPreview = _react.default.memo(function (_ref2) {
  var cellClick = _ref2.cellClick,
      query = _ref2.query;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      clearTable = _useState2[0],
      setClearTable = _useState2[1];

  var indexPattern = (0, _kibana.useCurrentIndexPattern)();

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      selectedFields = _useState4[0],
      setSelectedFields = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isColumnsPopoverVisible = _useState6[0],
      setColumnsPopoverVisible = _useState6[1]; // EuiInMemoryTable has an issue with dynamic sortable columns
  // and will trigger a full page Kibana error in such a case.
  // The following is a workaround until this is solved upstream:
  // - If the sortable/columns config changes,
  //   the table will be unmounted/not rendered.
  //   This is what setClearTable(true) in toggleColumn() does.
  // - After that on next render it gets re-enabled. To make sure React
  //   doesn't consolidate the state updates, setTimeout is used.


  if (clearTable) {
    setTimeout(function () {
      return setClearTable(false);
    }, 0);
  }

  function toggleColumnsPopover() {
    setColumnsPopoverVisible(!isColumnsPopoverVisible);
  }

  function closeColumnsPopover() {
    setColumnsPopoverVisible(false);
  }

  function toggleColumn(column) {
    // spread to a new array otherwise the component wouldn't re-render
    setClearTable(true);
    setSelectedFields(_toConsumableArray((0, _common.toggleSelectedField)(selectedFields, column)));
  }

  var _useState7 = (0, _react.useState)({}),
      _useState8 = _slicedToArray(_useState7, 2),
      itemIdToExpandedRowMap = _useState8[0],
      setItemIdToExpandedRowMap = _useState8[1];

  function toggleDetails(item) {
    if (itemIdToExpandedRowMap[item._id]) {
      delete itemIdToExpandedRowMap[item._id];
    } else {
      itemIdToExpandedRowMap[item._id] = _react.default.createElement(_expanded_row.ExpandedRow, {
        item: item
      });
    } // spread to a new object otherwise the component wouldn't re-render


    setItemIdToExpandedRowMap(_objectSpread({}, itemIdToExpandedRowMap));
  }

  var _useSourceIndexData = (0, _use_source_index_data.useSourceIndexData)(indexPattern, query, selectedFields, setSelectedFields),
      errorMessage = _useSourceIndexData.errorMessage,
      status = _useSourceIndexData.status,
      tableItems = _useSourceIndexData.tableItems;

  if (status === _use_source_index_data.SOURCE_INDEX_STATUS.ERROR) {
    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(SourceIndexPreviewTitle, {
      indexPatternTitle: indexPattern.title
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.sourceIndexPatternError', {
        defaultMessage: 'An error occurred loading the source index data.'
      }),
      color: "danger",
      iconType: "cross"
    }, _react.default.createElement("p", null, errorMessage)));
  }

  if (status === _use_source_index_data.SOURCE_INDEX_STATUS.LOADED && tableItems.length === 0) {
    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(SourceIndexPreviewTitle, {
      indexPatternTitle: indexPattern.title
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexNoDataCalloutTitle', {
        defaultMessage: 'Empty source index query result.'
      }),
      color: "primary"
    }, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexNoDataCalloutBody', {
      defaultMessage: 'The query for the source index returned no results. Please make sure the index contains documents and your query is not too restrictive.'
    }))));
  }

  var docFields = [];
  var docFieldsCount = 0;

  if (tableItems.length > 0) {
    docFields = Object.keys(tableItems[0]._source);
    docFields.sort();
    docFieldsCount = docFields.length;
  }

  var columns = selectedFields.map(function (k) {
    var column = {
      field: "_source[\"".concat(k, "\"]"),
      name: k,
      sortable: true,
      truncateText: true
    };
    var field = indexPattern.fields.find(function (f) {
      return f.name === k;
    });

    var formatField = function formatField(d) {
      return field !== undefined && field.type === _field_types.KBN_FIELD_TYPES.DATE ? (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(d).unix() * 1000) : d;
    };

    var render = function render(d) {
      if (Array.isArray(d) && d.every(function (item) {
        return typeof item === 'string';
      })) {
        // If the cells data is an array of strings, return as a comma separated list.
        // The list will get limited to 5 items with `…` at the end if there's more in the original array.
        return "".concat(d.map(function (item) {
          return formatField(item);
        }).slice(0, 5).join(', ')).concat(d.length > 5 ? ', …' : '');
      } else if (Array.isArray(d)) {
        // If the cells data is an array of e.g. objects, display a 'array' badge with a
        // tooltip that explains that this type of field is not supported in this table.
        return _react.default.createElement(_eui.EuiToolTip, {
          content: _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexArrayToolTipContent', {
            defaultMessage: 'The full content of this array based column is available in the expanded row.'
          })
        }, _react.default.createElement(_eui.EuiBadge, null, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexArrayBadgeContent', {
          defaultMessage: 'array'
        })));
      } else if (_typeof(d) === 'object' && d !== null) {
        // If the cells data is an object, display a 'object' badge with a
        // tooltip that explains that this type of field is not supported in this table.
        return _react.default.createElement(_eui.EuiToolTip, {
          content: _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexObjectToolTipContent', {
            defaultMessage: 'The full content of this object based column is available in the expanded row.'
          })
        }, _react.default.createElement(_eui.EuiBadge, null, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.dataFrameSourceIndexObjectBadgeContent', {
          defaultMessage: 'object'
        })));
      }

      return formatField(d);
    };

    if (typeof field !== 'undefined') {
      switch (field.type) {
        case _field_types.KBN_FIELD_TYPES.BOOLEAN:
          column.dataType = 'boolean';
          break;

        case _field_types.KBN_FIELD_TYPES.DATE:
          column.align = 'right';

          column.render = function (d) {
            return (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(d).unix() * 1000);
          };

          break;

        case _field_types.KBN_FIELD_TYPES.NUMBER:
          column.dataType = 'number';
          break;

        default:
          column.render = render;
          break;
      }
    } else {
      column.render = render;
    }

    if (CELL_CLICK_ENABLED && cellClick) {
      column.render = function (d) {
        return _react.default.createElement(_eui.EuiButtonEmpty, {
          size: "xs",
          onClick: function onClick() {
            return cellClick("".concat(k, ":(").concat(d, ")"));
          }
        }, render(d));
      };
    }

    return column;
  });
  var sorting = false;

  if (columns.length > 0) {
    sorting = {
      sort: {
        field: "_source[\"".concat(selectedFields[0], "\"]"),
        direction: _in_memory_table.SORT_DIRECTION.ASC
      }
    };
  }

  columns.unshift({
    align: _eui.RIGHT_ALIGNMENT,
    width: '40px',
    isExpander: true,
    render: function render(item) {
      return _react.default.createElement(_eui.EuiButtonIcon, {
        onClick: function onClick() {
          return toggleDetails(item);
        },
        "aria-label": itemIdToExpandedRowMap[item._id] ? _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.rowCollapse', {
          defaultMessage: 'Collapse'
        }) : _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.rowExpand', {
          defaultMessage: 'Expand'
        }),
        iconType: itemIdToExpandedRowMap[item._id] ? 'arrowUp' : 'arrowDown'
      });
    }
  });

  var euiCopyText = _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.copyClipboardTooltip', {
    defaultMessage: 'Copy Dev Console statement of the source index preview to the clipboard.'
  });

  return _react.default.createElement(_eui.EuiPanel, {
    grow: false
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(SourceIndexPreviewTitle, {
    indexPatternTitle: indexPattern.title
  })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "xs"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    style: {
      textAlign: 'right'
    }
  }, docFieldsCount > _common.MAX_COLUMNS && _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.fieldSelection', {
    defaultMessage: '{selectedFieldsLength, number} of {docFieldsCount, number} {docFieldsCount, plural, one {field} other {fields}} selected',
    values: {
      selectedFieldsLength: selectedFields.length,
      docFieldsCount: docFieldsCount
    }
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement(_eui.EuiPopover, {
    id: "popover",
    button: _react.default.createElement(_eui.EuiButtonIcon, {
      iconType: "gear",
      onClick: toggleColumnsPopover,
      "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.selectColumnsAriaLabel', {
        defaultMessage: 'Select columns'
      })
    }),
    isOpen: isColumnsPopoverVisible,
    closePopover: closeColumnsPopover,
    ownFocus: true
  }, _react.default.createElement(_eui.EuiPopoverTitle, null, _i18n.i18n.translate('xpack.ml.dataframe.sourceIndexPreview.selectFieldsPopoverTitle', {
    defaultMessage: 'Select fields'
  })), _react.default.createElement("div", {
    style: {
      maxHeight: '400px',
      overflowY: 'scroll'
    }
  }, docFields.map(function (d) {
    return _react.default.createElement(_eui.EuiCheckbox, {
      key: d,
      id: d,
      label: d,
      checked: selectedFields.includes(d),
      onChange: function onChange() {
        return toggleColumn(d);
      },
      disabled: selectedFields.includes(d) && selectedFields.length === 1
    });
  }))))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiCopy, {
    beforeMessage: euiCopyText,
    textToCopy: (0, _common2.getSourceIndexDevConsoleStatement)(query, indexPattern.title)
  }, function (copy) {
    return _react.default.createElement(_eui.EuiButtonIcon, {
      onClick: copy,
      iconType: "copyClipboard",
      "aria-label": euiCopyText
    });
  }))))), status === _use_source_index_data.SOURCE_INDEX_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent"
  }), status !== _use_source_index_data.SOURCE_INDEX_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent",
    max: 1,
    value: 0
  }), clearTable === false && columns.length > 0 && sorting !== false && _react.default.createElement(_in_memory_table.MlInMemoryTable, {
    allowNeutralSort: false,
    compressed: true,
    items: tableItems,
    columns: columns,
    pagination: {
      initialPageSize: 5,
      pageSizeOptions: [5, 10, 25]
    },
    hasActions: false,
    isSelectable: false,
    itemId: "_id",
    itemIdToExpandedRowMap: itemIdToExpandedRowMap,
    isExpandable: true,
    sorting: sorting
  }));
});

exports.SourceIndexPreview = SourceIndexPreview;