"use strict";

var _routes = _interopRequireDefault(require("ui/routes"));

var _check_license = require("../../../../license/check_license");

var _check_privilege = require("../../../../privilege/check_privilege");

var _index_utils = require("../../../../util/index_utils");

var _breadcrumbs = require("../../../breadcrumbs");

var _new_job_capabilities_service = require("../../../../services/new_job_capabilities_service");

var _new_job_defaults = require("../../../new_job/utils/new_job_defaults");

var _job_service = require("../../../../services/job_service");

var _constants = require("../../common/job_creator/util/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
var template = "<ml-new-job-page />";
var routes = [{
  id: _constants.JOB_TYPE.SINGLE_METRIC,
  k7Breadcrumbs: _breadcrumbs.getCreateSingleMetricJobBreadcrumbs
}, {
  id: _constants.JOB_TYPE.MULTI_METRIC,
  k7Breadcrumbs: _breadcrumbs.getCreateMultiMetricJobBreadcrumbs
}, {
  id: _constants.JOB_TYPE.POPULATION,
  k7Breadcrumbs: _breadcrumbs.getCreatePopulationJobBreadcrumbs
}];
routes.forEach(function (route) {
  _routes.default.when("/jobs/new_job/".concat(route.id), {
    template: template,
    k7Breadcrumbs: route.k7Breadcrumbs,
    resolve: {
      CheckLicense: _check_license.checkFullLicense,
      privileges: _check_privilege.checkGetJobsPrivilege,
      indexPattern: _index_utils.loadCurrentIndexPattern,
      savedSearch: _index_utils.loadCurrentSavedSearch,
      loadNewJobCapabilities: _new_job_capabilities_service.loadNewJobCapabilities,
      loadNewJobDefaults: _new_job_defaults.loadNewJobDefaults,
      existingJobsAndGroups: _job_service.mlJobService.getJobAndGroupIds,
      jobType: function jobType() {
        return route.id;
      }
    }
  });
});