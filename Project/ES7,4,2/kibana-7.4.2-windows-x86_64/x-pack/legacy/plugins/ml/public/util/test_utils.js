"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryByTestSubj = void 0;

var _reactTestingLibrary = require("react-testing-library");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * 'react-testing-library provides 'queryByTestId()' to get
 * elements with a 'data-testid' attribut. This custom method
 * supports querying for the Kibana style 'data-test-subj' attribute.
 * @param {HTMLElement} container The wrapping HTML element.
 * @param {Matcher} id The 'data-test-subj' id.
 * @returns {HTMLElement | null}
 */
var queryByTestSubj = function queryByTestSubj(container, id) {
  return _reactTestingLibrary.queryHelpers.queryByAttribute('data-test-subj', container, id);
};

exports.queryByTestSubj = queryByTestSubj;