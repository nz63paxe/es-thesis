"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isSingleMetricJobCreator = isSingleMetricJobCreator;
exports.isMultiMetricJobCreator = isMultiMetricJobCreator;
exports.isPopulationJobCreator = isPopulationJobCreator;

var _constants = require("./util/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function isSingleMetricJobCreator(jobCreator) {
  return jobCreator.type === _constants.JOB_TYPE.SINGLE_METRIC;
}

function isMultiMetricJobCreator(jobCreator) {
  return jobCreator.type === _constants.JOB_TYPE.MULTI_METRIC;
}

function isPopulationJobCreator(jobCreator) {
  return jobCreator.type === _constants.JOB_TYPE.POPULATION;
}