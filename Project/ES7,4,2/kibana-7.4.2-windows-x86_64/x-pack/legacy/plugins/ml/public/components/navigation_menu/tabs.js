"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Tabs = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _chrome = _interopRequireDefault(require("ui/chrome"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function getTabs(disableLinks) {
  return [{
    id: 'jobs',
    name: _i18n.i18n.translate('xpack.ml.navMenu.jobManagementTabLinkText', {
      defaultMessage: 'Job Management'
    }),
    disabled: disableLinks
  }, {
    id: 'explorer',
    name: _i18n.i18n.translate('xpack.ml.navMenu.anomalyExplorerTabLinkText', {
      defaultMessage: 'Anomaly Explorer'
    }),
    disabled: disableLinks
  }, {
    id: 'timeseriesexplorer',
    name: _i18n.i18n.translate('xpack.ml.navMenu.singleMetricViewerTabLinkText', {
      defaultMessage: 'Single Metric Viewer'
    }),
    disabled: disableLinks
  }, {
    id: 'data_frames',
    name: _i18n.i18n.translate('xpack.ml.navMenu.dataFrameTabLinkText', {
      defaultMessage: 'Transforms'
    }),
    disabled: false
  }, {
    id: 'data_frame_analytics',
    name: _i18n.i18n.translate('xpack.ml.navMenu.dataFrameAnalyticsTabLinkText', {
      defaultMessage: 'Analytics'
    }),
    disabled: disableLinks
  }, {
    id: 'datavisualizer',
    name: _i18n.i18n.translate('xpack.ml.navMenu.dataVisualizerTabLinkText', {
      defaultMessage: 'Data Visualizer'
    }),
    disabled: false
  }, {
    id: 'settings',
    name: _i18n.i18n.translate('xpack.ml.navMenu.settingsTabLinkText', {
      defaultMessage: 'Settings'
    }),
    disabled: disableLinks
  }];
}

var TAB_TEST_SUBJECT;

(function (TAB_TEST_SUBJECT) {
  TAB_TEST_SUBJECT["jobs"] = "mlTabJobManagement";
  TAB_TEST_SUBJECT["explorer"] = "mlTabAnomalyExplorer";
  TAB_TEST_SUBJECT["timeseriesexplorer"] = "mlTabSingleMetricViewer";
  TAB_TEST_SUBJECT["data_frames"] = "mlTabDataFrames";
  TAB_TEST_SUBJECT["data_frame_analytics"] = "mlTabDataFrameAnalytics";
  TAB_TEST_SUBJECT["datavisualizer"] = "mlTabDataVisualizer";
  TAB_TEST_SUBJECT["settings"] = "mlTabSettings";
})(TAB_TEST_SUBJECT || (TAB_TEST_SUBJECT = {}));

var Tabs = function Tabs(_ref) {
  var tabId = _ref.tabId,
      disableLinks = _ref.disableLinks;

  var _useState = (0, _react.useState)(tabId),
      _useState2 = _slicedToArray(_useState, 2),
      selectedTabId = _useState2[0],
      setSelectedTabId = _useState2[1];

  function onSelectedTabChanged(id) {
    setSelectedTabId(id);
  }

  var tabs = getTabs(disableLinks);
  return _react.default.createElement(_eui.EuiTabs, null, tabs.map(function (tab) {
    var id = tab.id;
    return _react.default.createElement(_eui.EuiLink, {
      "data-test-subj": TAB_TEST_SUBJECT[id],
      href: "".concat(_chrome.default.getBasePath(), "/app/ml#/").concat(id),
      key: "".concat(id, "-key"),
      color: "text"
    }, _react.default.createElement(_eui.EuiTab, {
      className: "mlNavigationMenu__tab",
      onClick: function onClick() {
        return onSelectedTabChanged(id);
      },
      isSelected: id === selectedTabId,
      disabled: tab.disabled
    }, tab.name));
  }));
};

exports.Tabs = Tabs;