"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDataFrameAnalyticsBreadcrumbs = getDataFrameAnalyticsBreadcrumbs;

var _i18n = require("@kbn/i18n");

var _breadcrumbs = require("../breadcrumbs");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function getDataFrameAnalyticsBreadcrumbs() {
  return [_breadcrumbs.ML_BREADCRUMB, {
    text: _i18n.i18n.translate('xpack.ml.dataFrameAnalyticsBreadcrumbs.dataFrameLabel', {
      defaultMessage: 'Analytics'
    }),
    href: ''
  }];
}