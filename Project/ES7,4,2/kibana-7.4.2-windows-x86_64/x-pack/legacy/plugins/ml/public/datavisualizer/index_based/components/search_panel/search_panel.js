"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SearchPanel = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _search = require("../../../../../common/constants/search");

var _kql_filter_bar = require("../../../../components/kql_filter_bar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var SearchPanel = function SearchPanel(_ref) {
  var indexPattern = _ref.indexPattern,
      searchString = _ref.searchString,
      setSearchString = _ref.setSearchString,
      searchQuery = _ref.searchQuery,
      setSearchQuery = _ref.setSearchQuery,
      searchQueryLanguage = _ref.searchQueryLanguage,
      samplerShardSize = _ref.samplerShardSize,
      setSamplerShardSize = _ref.setSamplerShardSize,
      totalCount = _ref.totalCount;

  var searchAllOptionText = _i18n.i18n.translate('xpack.ml.datavisualizer.searchPanel.allOptionLabel', {
    defaultMessage: 'all'
  });

  var searchSizeOptions = [{
    value: '1000',
    text: '1000'
  }, {
    value: '5000',
    text: '5000'
  }, {
    value: '10000',
    text: '10000'
  }, {
    value: '100000',
    text: '100000'
  }, {
    value: '-1',
    text: searchAllOptionText
  }];

  var searchHandler = function searchHandler(d) {
    setSearchQuery(d.filterQuery);
  };

  return _react.default.createElement(_eui.EuiPanel, {
    grow: false,
    "data-test-subj": "mlDataVisualizerSearchPanel"
  }, searchQueryLanguage === _search.SEARCH_QUERY_LANGUAGE.KUERY ? _react.default.createElement(_kql_filter_bar.KqlFilterBar, {
    indexPattern: indexPattern,
    onSubmit: searchHandler,
    initialValue: searchString,
    placeholder: _i18n.i18n.translate('xpack.ml.datavisualizer.searchPanel.queryBarPlaceholderText', {
      defaultMessage: 'Search… (e.g. status:200 AND extension:"PHP")'
    })
  }) : _react.default.createElement(_eui.EuiForm, null, _react.default.createElement(_eui.EuiFormRow, {
    helpText: _i18n.i18n.translate('xpack.ml.datavisualizer.searchPanel.kqlEditOnlyLabel', {
      defaultMessage: 'Currently only KQL saved searches can be edited'
    })
  }, _react.default.createElement(_eui.EuiFieldSearch, {
    value: "".concat(searchString),
    readOnly: true,
    "data-test-subj": "mlDataVisualizerLuceneSearchBarl"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xs",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.datavisualizer.searchPanel.sampleLabel",
    defaultMessage: "Sample"
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiSelect, {
    options: searchSizeOptions,
    value: samplerShardSize,
    onChange: function onChange(e) {
      return setSamplerShardSize(+e.target.value);
    },
    "aria-label": _i18n.i18n.translate('xpack.ml.datavisualizer.searchPanel.sampleSizeAriaLabel', {
      defaultMessage: 'Select number of documents to sample'
    }),
    "data-test-subj": "mlDataVisualizerShardSizeSelect"
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement("span", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.datavisualizer.searchPanel.documentsPerShardLabel",
    defaultMessage: "documents per shard from a total of {wrappedTotalCount} {totalCount, plural, one {document} other {documents}}",
    values: {
      wrappedTotalCount: _react.default.createElement("b", null, totalCount),
      totalCount: totalCount
    }
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiIconTip, {
    content: _i18n.i18n.translate('xpack.ml.datavisualizer.searchPanel.queryBarPlaceholder', {
      defaultMessage: 'Selecting a smaller sample size will reduce query run times and the load on the cluster.'
    }),
    position: "right"
  }))));
};

exports.SearchPanel = SearchPanel;