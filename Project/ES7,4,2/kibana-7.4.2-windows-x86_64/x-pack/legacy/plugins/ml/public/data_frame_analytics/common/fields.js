"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFlattenedFields = getFlattenedFields;
exports.toggleSelectedField = exports.getDefaultSelectableFields = exports.sortColumns = exports.MAX_COLUMNS = void 0;

var _object_utils = require("../../util/object_utils");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var MAX_COLUMNS = 20;
exports.MAX_COLUMNS = MAX_COLUMNS;
var ML__ID_COPY = 'ml__id_copy'; // Used to sort columns:
// - string based columns are moved to the left
// - followed by the outlier_score column
// - feature_influence fields get moved next to the corresponding field column
// - overall fields get sorted alphabetically

var sortColumns = function sortColumns(obj, resultsField) {
  return function (a, b) {
    var typeofA = _typeof(obj[a]);

    var typeofB = _typeof(obj[b]);

    if (typeofA !== 'string' && typeofB === 'string') {
      return 1;
    }

    if (typeofA === 'string' && typeofB !== 'string') {
      return -1;
    }

    if (typeofA === 'string' && typeofB === 'string') {
      return a.localeCompare(b);
    }

    if (a === "".concat(resultsField, ".outlier_score")) {
      return -1;
    }

    if (b === "".concat(resultsField, ".outlier_score")) {
      return 1;
    }

    var tokensA = a.split('.');
    var prefixA = tokensA[0];
    var tokensB = b.split('.');
    var prefixB = tokensB[0];

    if (prefixA === resultsField && tokensA.length > 1 && prefixB !== resultsField) {
      tokensA.shift();
      tokensA.shift();
      if (tokensA.join('.') === b) return 1;
      return tokensA.join('.').localeCompare(b);
    }

    if (prefixB === resultsField && tokensB.length > 1 && prefixA !== resultsField) {
      tokensB.shift();
      tokensB.shift();
      if (tokensB.join('.') === a) return -1;
      return a.localeCompare(tokensB.join('.'));
    }

    return a.localeCompare(b);
  };
};

exports.sortColumns = sortColumns;

function getFlattenedFields(obj, resultsField) {
  var flatDocFields = [];
  var newDocFields = Object.keys(obj);
  newDocFields.forEach(function (f) {
    var fieldValue = (0, _object_utils.getNestedProperty)(obj, f);

    if (_typeof(fieldValue) !== 'object' || fieldValue === null || Array.isArray(fieldValue)) {
      flatDocFields.push(f);
    } else {
      var innerFields = getFlattenedFields(fieldValue, resultsField);
      var flattenedFields = innerFields.map(function (d) {
        return "".concat(f, ".").concat(d);
      });
      flatDocFields.push.apply(flatDocFields, _toConsumableArray(flattenedFields));
    }
  });
  return flatDocFields.filter(function (f) {
    return f !== ML__ID_COPY;
  });
}

var getDefaultSelectableFields = function getDefaultSelectableFields(docs, resultsField) {
  if (docs.length === 0) {
    return [];
  }

  var newDocFields = getFlattenedFields(docs[0]._source, resultsField);
  return newDocFields.filter(function (k) {
    if (k === "".concat(resultsField, ".outlier_score")) {
      return true;
    }

    if (k.split('.')[0] === resultsField) {
      return false;
    }

    var value = false;
    docs.forEach(function (row) {
      var source = row._source;

      if (source[k] !== null) {
        value = true;
      }
    });
    return value;
  }).slice(0, MAX_COLUMNS);
};

exports.getDefaultSelectableFields = getDefaultSelectableFields;

var toggleSelectedField = function toggleSelectedField(selectedFields, column) {
  var index = selectedFields.indexOf(column);

  if (index === -1) {
    selectedFields.push(column);
  } else {
    selectedFields.splice(index, 1);
  }

  return selectedFields;
};

exports.toggleSelectedField = toggleSelectedField;