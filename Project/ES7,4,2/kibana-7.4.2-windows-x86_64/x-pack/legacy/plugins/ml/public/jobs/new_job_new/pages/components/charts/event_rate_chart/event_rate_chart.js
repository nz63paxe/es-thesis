"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EventRateChart = void 0;

var _react = _interopRequireDefault(require("react"));

var _charts = require("@elastic/charts");

var _axes = require("../common/axes");

var _utils = require("../common/utils");

var _settings = require("../common/settings");

var _loading_wrapper = require("../loading_wrapper");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SPEC_ID = 'event_rate';

var EventRateChart = function EventRateChart(_ref) {
  var eventRateChartData = _ref.eventRateChartData,
      height = _ref.height,
      width = _ref.width,
      showAxis = _ref.showAxis,
      _ref$loading = _ref.loading,
      loading = _ref$loading === void 0 ? false : _ref$loading;
  return _react.default.createElement("div", {
    style: {
      width: width,
      height: height
    },
    "data-test-subj": "mlEventRateChart"
  }, _react.default.createElement(_loading_wrapper.LoadingWrapper, {
    height: height,
    hasData: eventRateChartData.length > 0,
    loading: loading
  }, _react.default.createElement(_charts.Chart, null, showAxis === true && _react.default.createElement(_axes.Axes, null), _react.default.createElement(_charts.Settings, {
    tooltip: _charts.TooltipType.None
  }), _react.default.createElement(_charts.BarSeries, {
    id: (0, _charts.getSpecId)('event_rate'),
    xScaleType: _charts.ScaleType.Time,
    yScaleType: _charts.ScaleType.Linear,
    xAccessor: 'time',
    yAccessors: ['value'],
    data: eventRateChartData,
    customSeriesColors: (0, _utils.getCustomColor)(SPEC_ID, _settings.EVENT_RATE_COLOR)
  }))));
};

exports.EventRateChart = EventRateChart;