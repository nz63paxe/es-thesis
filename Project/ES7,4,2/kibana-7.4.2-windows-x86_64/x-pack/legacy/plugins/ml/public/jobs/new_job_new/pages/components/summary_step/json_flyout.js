"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JsonFlyout = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _ml_job_editor = require("../../../../jobs_list/components/ml_job_editor");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var JsonFlyout = function JsonFlyout(_ref) {
  var jobCreator = _ref.jobCreator,
      closeFlyout = _ref.closeFlyout;
  return _react.default.createElement(_eui.EuiFlyout, {
    onClose: closeFlyout,
    hideCloseButton: true,
    size: "l"
  }, _react.default.createElement(_eui.EuiFlyoutBody, null, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(Contents, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.summaryStep.jsonFlyout.job.title', {
      defaultMessage: 'Job configuration JSON'
    }),
    value: jobCreator.formattedJobJson
  }), _react.default.createElement(Contents, {
    title: _i18n.i18n.translate('xpack.ml.newJob.wizard.summaryStep.jsonFlyout.datafeed.title', {
      defaultMessage: 'Datafeed configuration JSON'
    }),
    value: jobCreator.formattedDatafeedJson
  }))), _react.default.createElement(_eui.EuiFlyoutFooter, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    iconType: "cross",
    onClick: closeFlyout,
    flush: "left"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.jsonFlyout.closeLink",
    defaultMessage: "Close"
  }))))));
};

exports.JsonFlyout = JsonFlyout;

var Contents = function Contents(_ref2) {
  var title = _ref2.title,
      value = _ref2.value;
  var EDITOR_HEIGHT = '800px';
  return _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiTitle, {
    size: "s"
  }, _react.default.createElement("h5", null, title)), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_ml_job_editor.MLJobEditor, {
    value: value,
    height: EDITOR_HEIGHT,
    readOnly: true
  }));
};