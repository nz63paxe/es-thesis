"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reducer = reducer;
exports.validateAdvancedEditor = void 0;

var _i18n = require("@kbn/i18n");

var _index_patterns = require("ui/index_patterns");

var _es_utils = require("../../../../../../common/util/es_utils");

var _common = require("../../../../common");

var _actions = require("./actions");

var _state = require("./state");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getSourceIndexString = function getSourceIndexString(state) {
  var jobConfig = state.jobConfig;
  var sourceIndex = jobConfig != null && jobConfig.source != null ? jobConfig.source.index : undefined;

  if (typeof sourceIndex === 'string') {
    return sourceIndex;
  }

  if (Array.isArray(sourceIndex)) {
    return sourceIndex.join(',');
  }

  return '';
};

var validateAdvancedEditor = function validateAdvancedEditor(state) {
  var _state$form = state.form,
      jobIdEmpty = _state$form.jobIdEmpty,
      jobIdValid = _state$form.jobIdValid,
      jobIdExists = _state$form.jobIdExists,
      createIndexPattern = _state$form.createIndexPattern;
  var jobConfig = state.jobConfig;
  state.advancedEditorMessages = [];
  var sourceIndexName = getSourceIndexString(state);
  var sourceIndexNameEmpty = sourceIndexName === ''; // general check against Kibana index pattern names, but since this is about the advanced editor
  // with support for arrays in the job config, we also need to check that each individual name
  // doesn't include a comma if index names are supplied as an array.
  // `validateIndexPattern()` returns a map of messages, we're only interested here if it's valid or not.
  // If there are no messages, it means the index pattern is valid.

  var sourceIndexNameValid = Object.keys((0, _index_patterns.validateIndexPattern)(sourceIndexName)).length === 0;
  var sourceIndex = jobConfig != null && jobConfig.source != null ? jobConfig.source.index : undefined;

  if (sourceIndexNameValid) {
    if (typeof sourceIndex === 'string') {
      sourceIndexNameValid = !sourceIndex.includes(',');
    }

    if (Array.isArray(sourceIndex)) {
      sourceIndexNameValid = !sourceIndex.some(function (d) {
        return d.includes(',');
      });
    }
  }

  var destinationIndexName = (jobConfig != null && jobConfig.dest != null ? jobConfig.dest.index : undefined) || '';
  var destinationIndexNameEmpty = destinationIndexName === '';
  var destinationIndexNameValid = (0, _es_utils.isValidIndexName)(destinationIndexName);
  var destinationIndexPatternTitleExists = state.indexPatternTitles.some(function (name) {
    return destinationIndexName === name;
  });

  if (sourceIndexNameEmpty) {
    state.advancedEditorMessages.push({
      error: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.advancedEditorMessage.sourceIndexNameEmpty', {
        defaultMessage: 'The source index name must not be empty.'
      }),
      message: ''
    });
  } else if (!sourceIndexNameValid) {
    state.advancedEditorMessages.push({
      error: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.advancedEditorMessage.sourceIndexNameValid', {
        defaultMessage: 'Invalid source index name.'
      }),
      message: ''
    });
  }

  if (destinationIndexNameEmpty) {
    state.advancedEditorMessages.push({
      error: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.advancedEditorMessage.destinationIndexNameEmpty', {
        defaultMessage: 'The destination index name must not be empty.'
      }),
      message: ''
    });
  } else if (!destinationIndexNameValid) {
    state.advancedEditorMessages.push({
      error: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.advancedEditorMessage.destinationIndexNameValid', {
        defaultMessage: 'Invalid destination index name.'
      }),
      message: ''
    });
  }

  state.isValid = !jobIdEmpty && jobIdValid && !jobIdExists && !sourceIndexNameEmpty && sourceIndexNameValid && !destinationIndexNameEmpty && destinationIndexNameValid && (!destinationIndexPatternTitleExists || !createIndexPattern);
  return state;
};

exports.validateAdvancedEditor = validateAdvancedEditor;

var validateForm = function validateForm(state) {
  var _state$form2 = state.form,
      jobIdEmpty = _state$form2.jobIdEmpty,
      jobIdValid = _state$form2.jobIdValid,
      jobIdExists = _state$form2.jobIdExists,
      sourceIndexNameEmpty = _state$form2.sourceIndexNameEmpty,
      sourceIndexNameValid = _state$form2.sourceIndexNameValid,
      destinationIndexNameEmpty = _state$form2.destinationIndexNameEmpty,
      destinationIndexNameValid = _state$form2.destinationIndexNameValid,
      destinationIndexPatternTitleExists = _state$form2.destinationIndexPatternTitleExists,
      createIndexPattern = _state$form2.createIndexPattern;
  state.isValid = !jobIdEmpty && jobIdValid && !jobIdExists && !sourceIndexNameEmpty && sourceIndexNameValid && !destinationIndexNameEmpty && destinationIndexNameValid && (!destinationIndexPatternTitleExists || !createIndexPattern);
  return state;
};

function reducer(state, action) {
  switch (action.type) {
    case _actions.ACTION.ADD_REQUEST_MESSAGE:
      var requestMessages = state.requestMessages;
      requestMessages.push(action.requestMessage);
      return _objectSpread({}, state, {
        requestMessages: requestMessages
      });

    case _actions.ACTION.RESET_REQUEST_MESSAGES:
      return _objectSpread({}, state, {
        requestMessages: []
      });

    case _actions.ACTION.CLOSE_MODAL:
      return _objectSpread({}, state, {
        isModalVisible: false
      });

    case _actions.ACTION.OPEN_MODAL:
      return _objectSpread({}, state, {
        isModalVisible: true
      });

    case _actions.ACTION.RESET_ADVANCED_EDITOR_MESSAGES:
      return _objectSpread({}, state, {
        advancedEditorMessages: []
      });

    case _actions.ACTION.RESET_FORM:
      return (0, _state.getInitialState)();

    case _actions.ACTION.SET_ADVANCED_EDITOR_RAW_STRING:
      return _objectSpread({}, state, {
        advancedEditorRawString: action.advancedEditorRawString
      });

    case _actions.ACTION.SET_FORM_STATE:
      var newFormState = _objectSpread({}, state.form, {}, action.payload); // update state attributes which are derived from other state attributes.


      if (action.payload.destinationIndex !== undefined) {
        newFormState.destinationIndexNameExists = state.indexNames.some(function (name) {
          return newFormState.destinationIndex === name;
        });
        newFormState.destinationIndexNameEmpty = newFormState.destinationIndex === '';
        newFormState.destinationIndexNameValid = (0, _es_utils.isValidIndexName)(newFormState.destinationIndex);
        newFormState.destinationIndexPatternTitleExists = state.indexPatternTitles.some(function (name) {
          return newFormState.destinationIndex === name;
        });
      }

      if (action.payload.jobId !== undefined) {
        newFormState.jobIdExists = state.jobIds.some(function (id) {
          return newFormState.jobId === id;
        });
        newFormState.jobIdEmpty = newFormState.jobId === '';
        newFormState.jobIdValid = (0, _common.isAnalyticsIdValid)(newFormState.jobId);
      }

      if (action.payload.sourceIndex !== undefined) {
        newFormState.sourceIndexNameEmpty = newFormState.sourceIndex === '';
        var validationMessages = (0, _index_patterns.validateIndexPattern)(newFormState.sourceIndex);
        newFormState.sourceIndexNameValid = Object.keys(validationMessages).length === 0;
      }

      return state.isAdvancedEditorEnabled ? validateAdvancedEditor(_objectSpread({}, state, {
        form: newFormState
      })) : validateForm(_objectSpread({}, state, {
        form: newFormState
      }));

    case _actions.ACTION.SET_INDEX_NAMES:
      {
        var newState = _objectSpread({}, state, {
          indexNames: action.indexNames
        });

        newState.form.destinationIndexNameExists = newState.indexNames.some(function (name) {
          return newState.form.destinationIndex === name;
        });
        return newState;
      }

    case _actions.ACTION.SET_INDEX_PATTERN_TITLES:
      {
        var _newState = _objectSpread({}, state, {}, action.payload);

        _newState.form.destinationIndexPatternTitleExists = _newState.indexPatternTitles.some(function (name) {
          return _newState.form.destinationIndex === name;
        });
        return _newState;
      }

    case _actions.ACTION.SET_IS_JOB_CREATED:
      return _objectSpread({}, state, {
        isJobCreated: action.isJobCreated
      });

    case _actions.ACTION.SET_IS_JOB_STARTED:
      return _objectSpread({}, state, {
        isJobStarted: action.isJobStarted
      });

    case _actions.ACTION.SET_IS_MODAL_BUTTON_DISABLED:
      return _objectSpread({}, state, {
        isModalButtonDisabled: action.isModalButtonDisabled
      });

    case _actions.ACTION.SET_IS_MODAL_VISIBLE:
      return _objectSpread({}, state, {
        isModalVisible: action.isModalVisible
      });

    case _actions.ACTION.SET_JOB_CONFIG:
      return validateAdvancedEditor(_objectSpread({}, state, {
        jobConfig: action.payload
      }));

    case _actions.ACTION.SET_JOB_IDS:
      {
        var _newState2 = _objectSpread({}, state, {
          jobIds: action.jobIds
        });

        _newState2.form.jobIdExists = _newState2.jobIds.some(function (id) {
          return _newState2.form.jobId === id;
        });
        return _newState2;
      }

    case _actions.ACTION.SWITCH_TO_ADVANCED_EDITOR:
      var jobConfig = (0, _state.getJobConfigFromFormState)(state.form);
      return validateAdvancedEditor(_objectSpread({}, state, {
        advancedEditorRawString: JSON.stringify(jobConfig, null, 2),
        isAdvancedEditorEnabled: true,
        jobConfig: jobConfig
      }));
  }

  return state;
}