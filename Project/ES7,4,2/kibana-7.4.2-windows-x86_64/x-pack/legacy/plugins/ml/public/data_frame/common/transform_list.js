"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DATA_FRAME_TRANSFORM_LIST_COLUMN = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Used to pass on attribute names to table columns
var DATA_FRAME_TRANSFORM_LIST_COLUMN;
exports.DATA_FRAME_TRANSFORM_LIST_COLUMN = DATA_FRAME_TRANSFORM_LIST_COLUMN;

(function (DATA_FRAME_TRANSFORM_LIST_COLUMN) {
  DATA_FRAME_TRANSFORM_LIST_COLUMN["CONFIG_DEST_INDEX"] = "config.dest.index";
  DATA_FRAME_TRANSFORM_LIST_COLUMN["CONFIG_SOURCE_INDEX"] = "config.source.index";
  DATA_FRAME_TRANSFORM_LIST_COLUMN["DESCRIPTION"] = "config.description";
  DATA_FRAME_TRANSFORM_LIST_COLUMN["ID"] = "id";
})(DATA_FRAME_TRANSFORM_LIST_COLUMN || (exports.DATA_FRAME_TRANSFORM_LIST_COLUMN = DATA_FRAME_TRANSFORM_LIST_COLUMN = {}));