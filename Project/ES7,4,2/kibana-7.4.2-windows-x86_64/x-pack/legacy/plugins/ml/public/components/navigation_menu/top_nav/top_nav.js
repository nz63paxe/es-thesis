"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TopNav = exports.MlSuperDatePickerWithUpdate = void 0;

var _react = _interopRequireWildcard(require("react"));

var _rxjs = require("rxjs");

var _eui = require("@elastic/eui");

var _timefilter_refresh_service = require("../../../services/timefilter_refresh_service");

var _use_ui_context = require("../../../contexts/ui/use_ui_context");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MlSuperDatePicker = _eui.EuiSuperDatePicker; // This part fixes a problem with EuiSuperDater picker where it would not reflect
// a prop change of isPaused on the internal interval. This fix will be released
// with EUI 13.7.0 but only 13.6.1 without the fix made it into Kibana 7.4 so
// it's copied here.

var MlSuperDatePickerWithUpdate =
/*#__PURE__*/
function (_MlSuperDatePicker) {
  _inherits(MlSuperDatePickerWithUpdate, _MlSuperDatePicker);

  function MlSuperDatePickerWithUpdate() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, MlSuperDatePickerWithUpdate);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(MlSuperDatePickerWithUpdate)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "componentDidUpdate", function () {
      // @ts-ignore
      _this.stopInterval();

      if (!_this.props.isPaused) {
        // @ts-ignore
        _this.startInterval(_this.props.refreshInterval);
      }
    });

    return _this;
  }

  return MlSuperDatePickerWithUpdate;
}(MlSuperDatePicker);

exports.MlSuperDatePickerWithUpdate = MlSuperDatePickerWithUpdate;

function getRecentlyUsedRangesFactory(timeHistory) {
  return function () {
    return timeHistory.get().map(function (_ref) {
      var from = _ref.from,
          to = _ref.to;
      return {
        start: from,
        end: to
      };
    });
  };
}

var TopNav = function TopNav() {
  var _useUiContext = (0, _use_ui_context.useUiContext)(),
      chrome = _useUiContext.chrome,
      timefilter = _useUiContext.timefilter,
      timeHistory = _useUiContext.timeHistory;

  var getRecentlyUsedRanges = getRecentlyUsedRangesFactory(timeHistory);

  var _useState = (0, _react.useState)(timefilter.getRefreshInterval()),
      _useState2 = _slicedToArray(_useState, 2),
      refreshInterval = _useState2[0],
      setRefreshInterval = _useState2[1];

  var _useState3 = (0, _react.useState)(timefilter.getTime()),
      _useState4 = _slicedToArray(_useState3, 2),
      time = _useState4[0],
      setTime = _useState4[1];

  var _useState5 = (0, _react.useState)(getRecentlyUsedRanges()),
      _useState6 = _slicedToArray(_useState5, 2),
      recentlyUsedRanges = _useState6[0],
      setRecentlyUsedRanges = _useState6[1];

  var _useState7 = (0, _react.useState)(timefilter.isAutoRefreshSelectorEnabled),
      _useState8 = _slicedToArray(_useState7, 2),
      isAutoRefreshSelectorEnabled = _useState8[0],
      setIsAutoRefreshSelectorEnabled = _useState8[1];

  var _useState9 = (0, _react.useState)(timefilter.isTimeRangeSelectorEnabled),
      _useState10 = _slicedToArray(_useState9, 2),
      isTimeRangeSelectorEnabled = _useState10[0],
      setIsTimeRangeSelectorEnabled = _useState10[1];

  var dateFormat = chrome.getUiSettingsClient().get('dateFormat');
  (0, _react.useEffect)(function () {
    var subscriptions = new _rxjs.Subscription();
    subscriptions.add(timefilter.getRefreshIntervalUpdate$().subscribe(timefilterUpdateListener));
    subscriptions.add(timefilter.getTimeUpdate$().subscribe(timefilterUpdateListener));
    subscriptions.add(timefilter.getEnabledUpdated$().subscribe(timefilterUpdateListener));
    return function cleanup() {
      subscriptions.unsubscribe();
    };
  }, []);
  (0, _react.useEffect)(function () {
    // Force re-render with up-to-date values when isTimeRangeSelectorEnabled/isAutoRefreshSelectorEnabled are changed.
    timefilterUpdateListener();
  }, [isTimeRangeSelectorEnabled, isAutoRefreshSelectorEnabled]);

  function timefilterUpdateListener() {
    setTime(timefilter.getTime());
    setRefreshInterval(timefilter.getRefreshInterval());
    setIsAutoRefreshSelectorEnabled(timefilter.isAutoRefreshSelectorEnabled);
    setIsTimeRangeSelectorEnabled(timefilter.isTimeRangeSelectorEnabled);
  }

  function updateFilter(_ref2) {
    var start = _ref2.start,
        end = _ref2.end;
    var newTime = {
      from: start,
      to: end
    }; // Update timefilter for controllers listening for changes

    timefilter.setTime(newTime);
    setTime(newTime);
    setRecentlyUsedRanges(getRecentlyUsedRanges());
  }

  function updateInterval(_ref3) {
    var isPaused = _ref3.isPaused,
        interval = _ref3.refreshInterval;
    var newInterval = {
      pause: isPaused,
      value: interval
    }; // Update timefilter for controllers listening for changes

    timefilter.setRefreshInterval(newInterval); // Update state

    setRefreshInterval(newInterval);
  }

  return _react.default.createElement(_react.Fragment, null, (isAutoRefreshSelectorEnabled || isTimeRangeSelectorEnabled) && _react.default.createElement("div", {
    className: "mlNavigationMenu__topNav"
  }, _react.default.createElement(MlSuperDatePickerWithUpdate, {
    start: time.from,
    end: time.to,
    isPaused: refreshInterval.pause,
    isAutoRefreshOnly: !isTimeRangeSelectorEnabled,
    refreshInterval: refreshInterval.value,
    onTimeChange: updateFilter,
    onRefresh: function onRefresh() {
      return _timefilter_refresh_service.mlTimefilterRefresh$.next();
    },
    onRefreshChange: updateInterval,
    recentlyUsedRanges: recentlyUsedRanges,
    dateFormat: dateFormat
  })));
};

exports.TopNav = TopNav;