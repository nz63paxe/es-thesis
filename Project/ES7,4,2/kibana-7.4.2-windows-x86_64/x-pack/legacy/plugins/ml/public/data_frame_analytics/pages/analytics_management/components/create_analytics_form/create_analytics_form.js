"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateAnalyticsForm = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _metadata = require("ui/metadata");

var _index_patterns = require("ui/index_patterns");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

// based on code used by `ui/index_patterns` internally
// remove the space character from the list of illegal characters
_index_patterns.INDEX_PATTERN_ILLEGAL_CHARACTERS.pop();

var characterList = _index_patterns.INDEX_PATTERN_ILLEGAL_CHARACTERS.join(', ');

var CreateAnalyticsForm = function CreateAnalyticsForm(_ref) {
  var actions = _ref.actions,
      state = _ref.state;
  var setFormState = actions.setFormState;
  var form = state.form,
      indexPatternsWithNumericFields = state.indexPatternsWithNumericFields,
      indexPatternTitles = state.indexPatternTitles,
      isJobCreated = state.isJobCreated,
      requestMessages = state.requestMessages;
  var createIndexPattern = form.createIndexPattern,
      destinationIndex = form.destinationIndex,
      destinationIndexNameEmpty = form.destinationIndexNameEmpty,
      destinationIndexNameExists = form.destinationIndexNameExists,
      destinationIndexNameValid = form.destinationIndexNameValid,
      destinationIndexPatternTitleExists = form.destinationIndexPatternTitleExists,
      jobId = form.jobId,
      jobIdEmpty = form.jobIdEmpty,
      jobIdExists = form.jobIdExists,
      jobIdValid = form.jobIdValid,
      sourceIndex = form.sourceIndex,
      sourceIndexNameEmpty = form.sourceIndexNameEmpty,
      sourceIndexNameValid = form.sourceIndexNameValid;
  return _react.default.createElement(_eui.EuiForm, {
    className: "mlDataFrameAnalyticsCreateForm"
  }, requestMessages.map(function (requestMessage, i) {
    return _react.default.createElement(_react.Fragment, {
      key: i
    }, _react.default.createElement(_eui.EuiCallOut, {
      title: requestMessage.message,
      color: requestMessage.error !== undefined ? 'danger' : 'primary',
      iconType: requestMessage.error !== undefined ? 'alert' : 'checkInCircleFilled',
      size: "s"
    }, requestMessage.error !== undefined ? _react.default.createElement("p", null, requestMessage.error) : null), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }));
  }), !isJobCreated && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobTypeLabel', {
      defaultMessage: 'Job type'
    }),
    helpText: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.ml.dataframe.analytics.create.jobTypeHelpText",
      defaultMessage: "Outlier detection jobs require a source index that is mapped as a table-like data structure and will only analyze numeric and boolean fields. Please use the {advancedEditorButton} to apply custom options such as the model memory limit and analysis type. You cannot switch back to this form from the advanced editor.",
      values: {
        advancedEditorButton: _react.default.createElement(_eui.EuiLink, {
          onClick: actions.switchToAdvancedEditor
        }, _react.default.createElement(_react2.FormattedMessage, {
          id: "xpack.ml.dataframe.analytics.create.switchToAdvancedEditorButton",
          defaultMessage: "advanced editor"
        }))
      }
    })
  }, _react.default.createElement(_eui.EuiText, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.outlierDetectionText', {
    defaultMessage: 'Outlier detection'
  }))), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobIdLabel', {
      defaultMessage: 'Job ID'
    }),
    isInvalid: !jobIdEmpty && !jobIdValid || jobIdExists,
    error: [].concat(_toConsumableArray(!jobIdEmpty && !jobIdValid ? [_i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobIdInvalidError', {
      defaultMessage: 'Must contain lowercase alphanumeric characters (a-z and 0-9), hyphens, and underscores only and must start and end with alphanumeric characters.'
    })] : []), _toConsumableArray(jobIdExists ? [_i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobIdExistsError', {
      defaultMessage: 'An analytics job with this ID already exists.'
    })] : []))
  }, _react.default.createElement(_eui.EuiFieldText, {
    disabled: isJobCreated,
    placeholder: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobIdPlaceholder', {
      defaultMessage: 'Job ID'
    }),
    value: jobId,
    onChange: function onChange(e) {
      return setFormState({
        jobId: e.target.value
      });
    },
    "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.jobIdInputAriaLabel', {
      defaultMessage: 'Choose a unique analytics job ID.'
    }),
    isInvalid: !jobIdEmpty && !jobIdValid || jobIdExists
  })), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.sourceIndexLabel', {
      defaultMessage: 'Source index'
    }),
    helpText: !sourceIndexNameEmpty && !indexPatternsWithNumericFields.includes(sourceIndex) && _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.sourceIndexHelpText', {
      defaultMessage: 'This index pattern does not contain any numeric type fields. The analytics job may not be able to come up with any outliers.'
    }),
    isInvalid: !sourceIndexNameEmpty && !sourceIndexNameValid,
    error: !sourceIndexNameEmpty && !sourceIndexNameValid && [_react.default.createElement(_react.Fragment, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.sourceIndexInvalidError', {
      defaultMessage: 'Invalid source index name, it cannot contain spaces or the characters: {characterList}',
      values: {
        characterList: characterList
      }
    }))]
  }, _react.default.createElement(_react.Fragment, null, !isJobCreated && _react.default.createElement(_eui.EuiComboBox, {
    placeholder: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.sourceIndexPlaceholder', {
      defaultMessage: 'Choose a source index pattern or saved search.'
    }),
    singleSelection: {
      asPlainText: true
    },
    options: indexPatternTitles.sort().map(function (d) {
      return {
        label: d
      };
    }),
    selectedOptions: [{
      label: sourceIndex
    }],
    onChange: function onChange(selectedOptions) {
      return setFormState({
        sourceIndex: selectedOptions[0].label || ''
      });
    },
    isClearable: false
  }), isJobCreated && _react.default.createElement(_eui.EuiFieldText, {
    disabled: true,
    value: sourceIndex,
    "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.sourceIndexInputAriaLabel', {
      defaultMessage: 'Source index pattern or search.'
    })
  }))), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.destinationIndexLabel', {
      defaultMessage: 'Destination index'
    }),
    isInvalid: !destinationIndexNameEmpty && !destinationIndexNameValid,
    helpText: destinationIndexNameExists && _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.destinationIndexHelpText', {
      defaultMessage: 'An index with this name already exists. Be aware that running this analytics job will modify this destination index.'
    }),
    error: !destinationIndexNameEmpty && !destinationIndexNameValid && [_react.default.createElement(_react.Fragment, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.destinationIndexInvalidError', {
      defaultMessage: 'Invalid destination index name.'
    }), _react.default.createElement("br", null), _react.default.createElement(_eui.EuiLink, {
      href: "https://www.elastic.co/guide/en/elasticsearch/reference/".concat(_metadata.metadata.branch, "/indices-create-index.html#indices-create-index"),
      target: "_blank"
    }, _i18n.i18n.translate('xpack.ml.dataframe.stepDetailsForm.destinationIndexInvalidErrorLink', {
      defaultMessage: 'Learn more about index name limitations.'
    })))]
  }, _react.default.createElement(_eui.EuiFieldText, {
    disabled: isJobCreated,
    placeholder: "destination index",
    value: destinationIndex,
    onChange: function onChange(e) {
      return setFormState({
        destinationIndex: e.target.value
      });
    },
    "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.destinationIndexInputAriaLabel', {
      defaultMessage: 'Choose a unique destination index name.'
    }),
    isInvalid: !destinationIndexNameEmpty && !destinationIndexNameValid
  })), _react.default.createElement(_eui.EuiFormRow, {
    isInvalid: createIndexPattern && destinationIndexPatternTitleExists,
    error: createIndexPattern && destinationIndexPatternTitleExists && [_i18n.i18n.translate('xpack.ml.dataframe.analytics.create.indexPatternTitleError', {
      defaultMessage: 'An index pattern with this title already exists.'
    })]
  }, _react.default.createElement(_eui.EuiSwitch, {
    disabled: isJobCreated,
    name: "mlDataFrameAnalyticsCreateIndexPattern",
    label: _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.createIndexPatternLabel', {
      defaultMessage: 'Create index pattern'
    }),
    checked: createIndexPattern === true,
    onChange: function onChange() {
      return setFormState({
        createIndexPattern: !createIndexPattern
      });
    }
  }))));
};

exports.CreateAnalyticsForm = CreateAnalyticsForm;