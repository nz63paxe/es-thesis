"use strict";

require("./pages/access_denied/directive");

require("./pages/access_denied/route");

require("./pages/transform_management/directive");

require("./pages/transform_management/route");

require("./pages/data_frame_new_pivot/directive");

require("./pages/data_frame_new_pivot/route");