"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExpandedRow = void 0;

var _react = _interopRequireDefault(require("react"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _date_utils = require("../../../../../util/date_utils");

var _expanded_row_details_pane = require("./expanded_row_details_pane");

var _expanded_row_json_pane = require("./expanded_row_json_pane");

var _progress_bar = require("./progress_bar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

// import { ExpandedRowMessagesPane } from './expanded_row_messages_pane';
function getItemDescription(value) {
  if (_typeof(value) === 'object') {
    return JSON.stringify(value);
  }

  return value.toString();
}

var ExpandedRow = function ExpandedRow(_ref) {
  var item = _ref.item;

  var stateValues = _objectSpread({}, item.stats);

  delete stateValues.progress;
  var state = {
    title: _i18n.i18n.translate('xpack.ml.dataframe.analyticsList.expandedRow.tabs.jobSettings.state', {
      defaultMessage: 'State'
    }),
    items: Object.entries(stateValues).map(function (s) {
      return {
        title: s[0].toString(),
        description: getItemDescription(s[1])
      };
    }),
    position: 'left'
  };
  var progress = {
    title: _i18n.i18n.translate('xpack.ml.dataframe.analyticsList.expandedRow.tabs.jobSettings.progress', {
      defaultMessage: 'Progress'
    }),
    items: item.stats.progress.map(function (s) {
      return {
        title: s.phase,
        description: _react.default.createElement(_progress_bar.ProgressBar, {
          progress: s.progress_percent
        })
      };
    }),
    position: 'left'
  };
  var stats = {
    title: _i18n.i18n.translate('xpack.ml.dataframe.analyticsList.expandedRow.tabs.jobSettings.stats', {
      defaultMessage: 'Stats'
    }),
    items: [{
      title: 'create_time',
      description: (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(item.config.create_time).unix() * 1000)
    }, {
      title: 'model_memory_limit',
      description: item.config.model_memory_limit
    }, {
      title: 'version',
      description: item.config.version
    }],
    position: 'right'
  };
  var tabs = [{
    id: 'ml-analytics-job-details',
    name: _i18n.i18n.translate('xpack.ml.dataframe.analyticsList.expandedRow.tabs.jobSettingsLabel', {
      defaultMessage: 'Job details'
    }),
    content: _react.default.createElement(_expanded_row_details_pane.ExpandedRowDetailsPane, {
      sections: [state, progress, stats]
    })
  }, {
    id: 'ml-analytics-job-json',
    name: 'JSON',
    content: _react.default.createElement(_expanded_row_json_pane.ExpandedRowJsonPane, {
      json: item.config
    })
  }];
  return _react.default.createElement(_eui.EuiTabbedContent, {
    size: "s",
    tabs: tabs,
    initialSelectedTab: tabs[0],
    onTabClick: function onTabClick() {},
    expand: false
  });
};

exports.ExpandedRow = ExpandedRow;