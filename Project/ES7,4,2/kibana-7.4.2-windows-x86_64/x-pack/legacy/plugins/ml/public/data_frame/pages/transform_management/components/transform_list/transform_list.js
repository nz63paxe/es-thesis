"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataFrameTransformList = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _in_memory_table = require("../../../../../../common/types/eui/in_memory_table");

var _common = require("../../../../common");

var _check_privilege = require("../../../../../privilege/check_privilege");

var _columns = require("./columns");

var _action_delete = require("./action_delete");

var _action_start = require("./action_start");

var _action_stop = require("./action_stop");

var _expanded_row = require("./expanded_row");

var _transform_table = require("./transform_table");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function getItemIdToExpandedRowMap(itemIds, dataFrameTransforms) {
  return itemIds.reduce(function (m, transformId) {
    var item = dataFrameTransforms.find(function (transform) {
      return transform.config.id === transformId;
    });

    if (item !== undefined) {
      m[transformId] = _react.default.createElement(_expanded_row.ExpandedRow, {
        item: item
      });
    }

    return m;
  }, {});
}

function stringMatch(str, substr) {
  return typeof str === 'string' && typeof substr === 'string' && str.toLowerCase().match(substr.toLowerCase()) === null === false;
}

var DataFrameTransformList = function DataFrameTransformList(_ref) {
  var isInitialized = _ref.isInitialized,
      transforms = _ref.transforms,
      errorMessage = _ref.errorMessage,
      transformsLoading = _ref.transformsLoading;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isLoading = _useState2[0],
      setIsLoading = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      filterActive = _useState4[0],
      setFilterActive = _useState4[1];

  var _useState5 = (0, _react.useState)([]),
      _useState6 = _slicedToArray(_useState5, 2),
      filteredTransforms = _useState6[0],
      setFilteredTransforms = _useState6[1];

  var _useState7 = (0, _react.useState)([]),
      _useState8 = _slicedToArray(_useState7, 2),
      expandedRowItemIds = _useState8[0],
      setExpandedRowItemIds = _useState8[1];

  var _useState9 = (0, _react.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      transformSelection = _useState10[0],
      setTransformSelection = _useState10[1];

  var _useState11 = (0, _react.useState)(false),
      _useState12 = _slicedToArray(_useState11, 2),
      isActionsMenuOpen = _useState12[0],
      setIsActionsMenuOpen = _useState12[1];

  var _useState13 = (0, _react.useState)(undefined),
      _useState14 = _slicedToArray(_useState13, 2),
      searchError = _useState14[0],
      setSearchError = _useState14[1];

  var _useState15 = (0, _react.useState)(0),
      _useState16 = _slicedToArray(_useState15, 2),
      pageIndex = _useState16[0],
      setPageIndex = _useState16[1];

  var _useState17 = (0, _react.useState)(10),
      _useState18 = _slicedToArray(_useState17, 2),
      pageSize = _useState18[0],
      setPageSize = _useState18[1];

  var _useState19 = (0, _react.useState)(_common.DATA_FRAME_TRANSFORM_LIST_COLUMN.ID),
      _useState20 = _slicedToArray(_useState19, 2),
      sortField = _useState20[0],
      setSortField = _useState20[1];

  var _useState21 = (0, _react.useState)(_in_memory_table.SORT_DIRECTION.ASC),
      _useState22 = _slicedToArray(_useState21, 2),
      sortDirection = _useState22[0],
      setSortDirection = _useState22[1];

  var disabled = !(0, _check_privilege.checkPermission)('canCreateDataFrame') || !(0, _check_privilege.checkPermission)('canPreviewDataFrame') || !(0, _check_privilege.checkPermission)('canStartStopDataFrame');

  var onQueryChange = function onQueryChange(_ref2) {
    var query = _ref2.query,
        error = _ref2.error;

    if (error) {
      setSearchError(error.message);
    } else {
      var clauses = [];

      if (query && query.ast !== undefined && query.ast.clauses !== undefined) {
        clauses = query.ast.clauses;
      }

      if (clauses.length > 0) {
        setFilterActive(true);
        filterTransforms(clauses);
      } else {
        setFilterActive(false);
      }

      setSearchError(undefined);
    }
  };

  var filterTransforms = function filterTransforms(clauses) {
    setIsLoading(true); // keep count of the number of matches we make as we're looping over the clauses
    // we only want to return transforms which match all clauses, i.e. each search term is ANDed
    // { transform-one:  { transform: { id: transform-one, config: {}, state: {}, ... }, count: 0 }, transform-two: {...} }

    var matches = transforms.reduce(function (p, c) {
      p[c.id] = {
        transform: c,
        count: 0
      };
      return p;
    }, {});
    clauses.forEach(function (c) {
      // the search term could be negated with a minus, e.g. -bananas
      var bool = c.match === 'must';
      var ts = [];

      if (c.type === 'term') {
        // filter term based clauses, e.g. bananas
        // match on id and description
        // if the term has been negated, AND the matches
        if (bool === true) {
          ts = transforms.filter(function (transform) {
            return stringMatch(transform.id, c.value) === bool || stringMatch(transform.config.description, c.value) === bool;
          });
        } else {
          ts = transforms.filter(function (transform) {
            return stringMatch(transform.id, c.value) === bool && stringMatch(transform.config.description, c.value) === bool;
          });
        }
      } else {
        // filter other clauses, i.e. the mode and status filters
        if (Array.isArray(c.value)) {
          // the status value is an array of string(s) e.g. ['failed', 'stopped']
          ts = transforms.filter(function (transform) {
            return c.value.includes(transform.stats.state);
          });
        } else {
          ts = transforms.filter(function (transform) {
            return transform.mode === c.value;
          });
        }
      }

      ts.forEach(function (t) {
        return matches[t.id].count++;
      });
    }); // loop through the matches and return only transforms which have match all the clauses

    var filtered = Object.values(matches).filter(function (m) {
      return (m && m.count) >= clauses.length;
    }).map(function (m) {
      return m.transform;
    });
    setFilteredTransforms(filtered);
    setIsLoading(false);
  }; // Before the transforms have been loaded for the first time, display the loading indicator only.
  // Otherwise a user would see 'No data frame transforms found' during the initial loading.


  if (!isInitialized) {
    return _react.default.createElement(_transform_table.ProgressBar, {
      isLoading: isLoading || transformsLoading
    });
  }

  if (typeof errorMessage !== 'undefined') {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_transform_table.ProgressBar, {
      isLoading: isLoading || transformsLoading
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataFrame.list.errorPromptTitle', {
        defaultMessage: 'An error occurred getting the data frame transform list.'
      }),
      color: "danger",
      iconType: "alert"
    }, _react.default.createElement("pre", null, JSON.stringify(errorMessage))));
  }

  if (transforms.length === 0) {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_transform_table.ProgressBar, {
      isLoading: isLoading || transformsLoading
    }), _react.default.createElement(_eui.EuiEmptyPrompt, {
      title: _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.ml.dataFrame.list.emptyPromptTitle', {
        defaultMessage: 'No data frame transforms found'
      })),
      actions: [_react.default.createElement(_eui.EuiButtonEmpty, {
        onClick: _common.moveToDataFrameWizard,
        isDisabled: disabled
      }, _i18n.i18n.translate('xpack.ml.dataFrame.list.emptyPromptButtonText', {
        defaultMessage: 'Create your first data frame transform'
      }))],
      "data-test-subj": "mlNoDataFrameTransformsFound"
    }));
  }

  var columns = (0, _columns.getColumns)(expandedRowItemIds, setExpandedRowItemIds, transformSelection);
  var sorting = {
    sort: {
      field: sortField,
      direction: sortDirection
    }
  };
  var itemIdToExpandedRowMap = getItemIdToExpandedRowMap(expandedRowItemIds, transforms);
  var pagination = {
    initialPageIndex: pageIndex,
    initialPageSize: pageSize,
    totalItemCount: transforms.length,
    pageSizeOptions: [10, 20, 50],
    hidePerPageOptions: false
  };
  var bulkActionMenuItems = [_react.default.createElement("div", {
    key: "startAction",
    className: "mlTransformBulkActionItem"
  }, _react.default.createElement(_action_start.StartAction, {
    items: transformSelection
  })), _react.default.createElement("div", {
    key: "stopAction",
    className: "mlTransformBulkActionItem"
  }, _react.default.createElement(_action_stop.StopAction, {
    items: transformSelection
  })), _react.default.createElement("div", {
    key: "deleteAction",
    className: "mlTransformBulkActionItem"
  }, _react.default.createElement(_action_delete.DeleteAction, {
    items: transformSelection
  }))];

  var renderToolsLeft = function renderToolsLeft() {
    var buttonIcon = _react.default.createElement(_eui.EuiButtonIcon, {
      size: "s",
      iconType: "gear",
      color: "text",
      onClick: function onClick() {
        setIsActionsMenuOpen(true);
      },
      "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.multiTransformActionsMenu.managementActionsAriaLabel', {
        defaultMessage: 'Management actions'
      })
    });

    var bulkActionIcon = _react.default.createElement(_eui.EuiPopover, {
      key: "bulkActionIcon",
      id: "transformBulkActionsMenu",
      button: buttonIcon,
      isOpen: isActionsMenuOpen,
      closePopover: function closePopover() {
        return setIsActionsMenuOpen(false);
      },
      panelPaddingSize: "none",
      anchorPosition: "rightUp"
    }, bulkActionMenuItems);

    return [_react.default.createElement(_eui.EuiTitle, {
      key: "selectedText",
      size: "s"
    }, _react.default.createElement("h3", null, _i18n.i18n.translate('xpack.ml.dataframe.multiTransformActionsMenu.transformsCount', {
      defaultMessage: '{count} {count, plural, one {transform} other {transforms}} selected',
      values: {
        count: transformSelection.length
      }
    }))), _react.default.createElement("div", {
      key: "bulkActionsBorder",
      className: "mlTransformBulkActionsBorder"
    }), bulkActionIcon];
  };

  var search = {
    toolsLeft: transformSelection.length > 0 ? renderToolsLeft() : undefined,
    onChange: onQueryChange,
    box: {
      incremental: true
    },
    filters: [{
      type: 'field_value_selection',
      field: 'state.state',
      name: _i18n.i18n.translate('xpack.ml.dataframe.statusFilter', {
        defaultMessage: 'Status'
      }),
      multiSelect: 'or',
      options: Object.values(_common.DATA_FRAME_TRANSFORM_STATE).map(function (val) {
        return {
          value: val,
          name: val,
          view: (0, _columns.getTaskStateBadge)(val)
        };
      })
    }, {
      type: 'field_value_selection',
      field: 'mode',
      name: _i18n.i18n.translate('xpack.ml.dataframe.modeFilter', {
        defaultMessage: 'Mode'
      }),
      multiSelect: false,
      options: Object.values(_common.DATA_FRAME_MODE).map(function (val) {
        return {
          value: val,
          name: val,
          view: _react.default.createElement(_eui.EuiBadge, {
            className: "mlTaskModeBadge",
            color: "hollow"
          }, val)
        };
      })
    }]
  };

  var onTableChange = function onTableChange(_ref3) {
    var _ref3$page = _ref3.page,
        page = _ref3$page === void 0 ? {
      index: 0,
      size: 10
    } : _ref3$page,
        _ref3$sort = _ref3.sort,
        sort = _ref3$sort === void 0 ? {
      field: _common.DATA_FRAME_TRANSFORM_LIST_COLUMN.ID,
      direction: _in_memory_table.SORT_DIRECTION.ASC
    } : _ref3$sort;
    var index = page.index,
        size = page.size;
    setPageIndex(index);
    setPageSize(size);
    var field = sort.field,
        direction = sort.direction;
    setSortField(field);
    setSortDirection(direction);
  };

  var selection = {
    onSelectionChange: function onSelectionChange(selected) {
      return setTransformSelection(selected);
    }
  };
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_transform_table.ProgressBar, {
    isLoading: isLoading || transformsLoading
  }), _react.default.createElement(_transform_table.TransformTable, {
    allowNeutralSort: false,
    className: "mlTransformTable",
    columns: columns,
    error: searchError,
    hasActions: false,
    isExpandable: true,
    isSelectable: false,
    items: filterActive ? filteredTransforms : transforms,
    itemId: _common.DATA_FRAME_TRANSFORM_LIST_COLUMN.ID,
    itemIdToExpandedRowMap: itemIdToExpandedRowMap,
    onTableChange: onTableChange,
    pagination: pagination,
    selection: selection,
    sorting: sorting,
    search: search,
    "data-test-subj": "mlDataFramesTableTransforms"
  }));
};

exports.DataFrameTransformList = DataFrameTransformList;