"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDefaultStepDefineState = getDefaultStepDefineState;
exports.isAggNameConflict = isAggNameConflict;
exports.StepDefineForm = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _metadata = require("ui/metadata");

var _notify = require("ui/notify");

var _eui = require("@elastic/eui");

var _common = require("../../../../../../common/types/common");

var _aggregation_dropdown = require("../aggregation_dropdown");

var _aggregation_list = require("../aggregation_list");

var _group_by_list = require("../group_by_list");

var _source_index_preview = require("../source_index_preview");

var _pivot_preview = require("./pivot_preview");

var _kql_filter_bar = require("../../../../../components/kql_filter_bar");

var _switch_modal = require("./switch_modal");

var _kibana = require("../../../../../contexts/kibana");

var _common2 = require("../../../../common");

var _common3 = require("./common");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var defaultSearch = '*';
var emptySearch = '';

function getDefaultStepDefineState(kibanaContext) {
  return {
    aggList: {},
    groupByList: {},
    isAdvancedPivotEditorEnabled: false,
    isAdvancedSourceEditorEnabled: false,
    searchString: kibanaContext.currentSavedSearch.id !== undefined ? kibanaContext.combinedQuery : defaultSearch,
    searchQuery: kibanaContext.currentSavedSearch.id !== undefined ? kibanaContext.combinedQuery : defaultSearch,
    sourceConfigUpdated: false,
    valid: false
  };
}

function isAggNameConflict(aggName, aggList, groupByList) {
  if (aggList[aggName] !== undefined) {
    _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.aggExistsErrorMessage', {
      defaultMessage: "An aggregation configuration with the name '{aggName}' already exists.",
      values: {
        aggName: aggName
      }
    }));

    return true;
  }

  if (groupByList[aggName] !== undefined) {
    _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.groupByExistsErrorMessage', {
      defaultMessage: "A group by configuration with the name '{aggName}' already exists.",
      values: {
        aggName: aggName
      }
    }));

    return true;
  }

  var conflict = false; // check the new aggName against existing aggs and groupbys

  var aggNameSplit = aggName.split('.');
  var aggNameCheck;
  aggNameSplit.forEach(function (aggNamePart) {
    aggNameCheck = aggNameCheck === undefined ? aggNamePart : "".concat(aggNameCheck, ".").concat(aggNamePart);

    if (aggList[aggNameCheck] !== undefined || groupByList[aggNameCheck] !== undefined) {
      _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.nestedConflictErrorMessage', {
        defaultMessage: "Couldn't add configuration '{aggName}' because of a nesting conflict with '{aggNameCheck}'.",
        values: {
          aggName: aggName,
          aggNameCheck: aggNameCheck
        }
      }));

      conflict = true;
    }
  });

  if (conflict) {
    return true;
  } // check all aggs against new aggName


  conflict = Object.keys(aggList).some(function (aggListName) {
    var aggListNameSplit = aggListName.split('.');
    var aggListNameCheck;
    return aggListNameSplit.some(function (aggListNamePart) {
      aggListNameCheck = aggListNameCheck === undefined ? aggListNamePart : "".concat(aggListNameCheck, ".").concat(aggListNamePart);

      if (aggListNameCheck === aggName) {
        _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.nestedAggListConflictErrorMessage', {
          defaultMessage: "Couldn't add configuration '{aggName}' because of a nesting conflict with '{aggListName}'.",
          values: {
            aggName: aggName,
            aggListName: aggListName
          }
        }));

        return true;
      }

      return false;
    });
  });

  if (conflict) {
    return true;
  } // check all group-bys against new aggName


  conflict = Object.keys(groupByList).some(function (groupByListName) {
    var groupByListNameSplit = groupByListName.split('.');
    var groupByListNameCheck;
    return groupByListNameSplit.some(function (groupByListNamePart) {
      groupByListNameCheck = groupByListNameCheck === undefined ? groupByListNamePart : "".concat(groupByListNameCheck, ".").concat(groupByListNamePart);

      if (groupByListNameCheck === aggName) {
        _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.nestedGroupByListConflictErrorMessage', {
          defaultMessage: "Couldn't add configuration '{aggName}' because of a nesting conflict with '{groupByListName}'.",
          values: {
            aggName: aggName,
            groupByListName: groupByListName
          }
        }));

        return true;
      }

      return false;
    });
  });
  return conflict;
}

var StepDefineForm = _react.default.memo(function (_ref) {
  var _ref$overrides = _ref.overrides,
      overrides = _ref$overrides === void 0 ? {} : _ref$overrides,
      onChange = _ref.onChange;
  var kibanaContext = (0, _kibana.useKibanaContext)();
  var indexPattern = kibanaContext.currentIndexPattern;

  var defaults = _objectSpread({}, getDefaultStepDefineState(kibanaContext), {}, overrides); // The search filter


  var _useState = (0, _react.useState)(defaults.searchString),
      _useState2 = _slicedToArray(_useState, 2),
      searchString = _useState2[0],
      setSearchString = _useState2[1];

  var _useState3 = (0, _react.useState)(defaults.searchQuery),
      _useState4 = _slicedToArray(_useState3, 2),
      searchQuery = _useState4[0],
      setSearchQuery = _useState4[1];

  var _useState5 = (0, _react.useState)(true),
      _useState6 = _slicedToArray(_useState5, 1),
      useKQL = _useState6[0];

  var addToSearch = function addToSearch(newSearch) {
    var currentDisplaySearch = searchString === defaultSearch ? emptySearch : searchString;
    setSearchString("".concat(currentDisplaySearch, " ").concat(newSearch).trim());
  };

  var searchHandler = function searchHandler(d) {
    var filterQuery = d.filterQuery,
        queryString = d.queryString;
    var newSearch = queryString === emptySearch ? defaultSearch : queryString;
    var newSearchQuery = filterQuery.match_all && Object.keys(filterQuery.match_all).length === 0 ? defaultSearch : filterQuery;
    setSearchString(newSearch);
    setSearchQuery(newSearchQuery);
  }; // The list of selected group by fields


  var _useState7 = (0, _react.useState)(defaults.groupByList),
      _useState8 = _slicedToArray(_useState7, 2),
      groupByList = _useState8[0],
      setGroupByList = _useState8[1];

  var _getPivotDropdownOpti = (0, _common3.getPivotDropdownOptions)(indexPattern),
      groupByOptions = _getPivotDropdownOpti.groupByOptions,
      groupByOptionsData = _getPivotDropdownOpti.groupByOptionsData,
      aggOptions = _getPivotDropdownOpti.aggOptions,
      aggOptionsData = _getPivotDropdownOpti.aggOptionsData;

  var addGroupBy = function addGroupBy(d) {
    var label = d[0].label;
    var config = groupByOptionsData[label];
    var aggName = config.aggName;

    if (isAggNameConflict(aggName, aggList, groupByList)) {
      return;
    }

    groupByList[aggName] = config;
    setGroupByList(_objectSpread({}, groupByList));
  };

  var updateGroupBy = function updateGroupBy(previousAggName, item) {
    var groupByListWithoutPrevious = _objectSpread({}, groupByList);

    delete groupByListWithoutPrevious[previousAggName];

    if (isAggNameConflict(item.aggName, aggList, groupByListWithoutPrevious)) {
      return;
    }

    groupByListWithoutPrevious[item.aggName] = item;
    setGroupByList(_objectSpread({}, groupByListWithoutPrevious));
  };

  var deleteGroupBy = function deleteGroupBy(aggName) {
    delete groupByList[aggName];
    setGroupByList(_objectSpread({}, groupByList));
  }; // The list of selected aggregations


  var _useState9 = (0, _react.useState)(defaults.aggList),
      _useState10 = _slicedToArray(_useState9, 2),
      aggList = _useState10[0],
      setAggList = _useState10[1];

  var addAggregation = function addAggregation(d) {
    var label = d[0].label;
    var config = aggOptionsData[label];
    var aggName = config.aggName;

    if (isAggNameConflict(aggName, aggList, groupByList)) {
      return;
    }

    aggList[aggName] = config;
    setAggList(_objectSpread({}, aggList));
  };

  var updateAggregation = function updateAggregation(previousAggName, item) {
    var aggListWithoutPrevious = _objectSpread({}, aggList);

    delete aggListWithoutPrevious[previousAggName];

    if (isAggNameConflict(item.aggName, aggListWithoutPrevious, groupByList)) {
      return;
    }

    aggListWithoutPrevious[item.aggName] = item;
    setAggList(_objectSpread({}, aggListWithoutPrevious));
  };

  var deleteAggregation = function deleteAggregation(aggName) {
    delete aggList[aggName];
    setAggList(_objectSpread({}, aggList));
  };

  var pivotAggsArr = (0, _common.dictionaryToArray)(aggList);
  var pivotGroupByArr = (0, _common.dictionaryToArray)(groupByList);
  var pivotQuery = useKQL ? (0, _common2.getPivotQuery)(searchQuery) : (0, _common2.getPivotQuery)(searchString); // Advanced editor for pivot config state

  var _useState11 = (0, _react.useState)(false),
      _useState12 = _slicedToArray(_useState11, 2),
      isAdvancedEditorSwitchModalVisible = _useState12[0],
      setAdvancedEditorSwitchModalVisible = _useState12[1];

  var _useState13 = (0, _react.useState)(false),
      _useState14 = _slicedToArray(_useState13, 2),
      isAdvancedPivotEditorApplyButtonEnabled = _useState14[0],
      setAdvancedPivotEditorApplyButtonEnabled = _useState14[1];

  var _useState15 = (0, _react.useState)(defaults.isAdvancedPivotEditorEnabled),
      _useState16 = _slicedToArray(_useState15, 2),
      isAdvancedPivotEditorEnabled = _useState16[0],
      setAdvancedPivotEditorEnabled = _useState16[1]; // Advanced editor for source config state


  var _useState17 = (0, _react.useState)(defaults.sourceConfigUpdated),
      _useState18 = _slicedToArray(_useState17, 2),
      sourceConfigUpdated = _useState18[0],
      setSourceConfigUpdated = _useState18[1];

  var _useState19 = (0, _react.useState)(false),
      _useState20 = _slicedToArray(_useState19, 2),
      isAdvancedSourceEditorSwitchModalVisible = _useState20[0],
      setAdvancedSourceEditorSwitchModalVisible = _useState20[1];

  var _useState21 = (0, _react.useState)(defaults.isAdvancedSourceEditorEnabled),
      _useState22 = _slicedToArray(_useState21, 2),
      isAdvancedSourceEditorEnabled = _useState22[0],
      setAdvancedSourceEditorEnabled = _useState22[1];

  var _useState23 = (0, _react.useState)(false),
      _useState24 = _slicedToArray(_useState23, 2),
      isAdvancedSourceEditorApplyButtonEnabled = _useState24[0],
      setAdvancedSourceEditorApplyButtonEnabled = _useState24[1];

  var previewRequest = (0, _common2.getPreviewRequestBody)(indexPattern.title, pivotQuery, pivotGroupByArr, pivotAggsArr); // pivot config

  var stringifiedPivotConfig = JSON.stringify(previewRequest.pivot, null, 2);

  var _useState25 = (0, _react.useState)(stringifiedPivotConfig),
      _useState26 = _slicedToArray(_useState25, 2),
      advancedEditorConfigLastApplied = _useState26[0],
      setAdvancedEditorConfigLastApplied = _useState26[1];

  var _useState27 = (0, _react.useState)(stringifiedPivotConfig),
      _useState28 = _slicedToArray(_useState27, 2),
      advancedEditorConfig = _useState28[0],
      setAdvancedEditorConfig = _useState28[1]; // source config


  var stringifiedSourceConfig = JSON.stringify(previewRequest.source.query, null, 2);

  var _useState29 = (0, _react.useState)(stringifiedSourceConfig),
      _useState30 = _slicedToArray(_useState29, 2),
      advancedEditorSourceConfigLastApplied = _useState30[0],
      setAdvancedEditorSourceConfigLastApplied = _useState30[1];

  var _useState31 = (0, _react.useState)(stringifiedSourceConfig),
      _useState32 = _slicedToArray(_useState31, 2),
      advancedEditorSourceConfig = _useState32[0],
      setAdvancedEditorSourceConfig = _useState32[1];

  var applyAdvancedSourceEditorChanges = function applyAdvancedSourceEditorChanges() {
    var sourceConfig = JSON.parse(advancedEditorSourceConfig);
    var prettySourceConfig = JSON.stringify(sourceConfig, null, 2); // Switched to editor so we clear out the search string as the bar won't be visible

    setSearchString(emptySearch);
    setSearchQuery(sourceConfig);
    setSourceConfigUpdated(true);
    setAdvancedEditorSourceConfig(prettySourceConfig);
    setAdvancedEditorSourceConfigLastApplied(prettySourceConfig);
    setAdvancedSourceEditorApplyButtonEnabled(false);
  };

  var applyAdvancedPivotEditorChanges = function applyAdvancedPivotEditorChanges() {
    var pivotConfig = JSON.parse(advancedEditorConfig);
    var newGroupByList = {};

    if (pivotConfig !== undefined && pivotConfig.group_by !== undefined) {
      Object.entries(pivotConfig.group_by).forEach(function (d) {
        var aggName = d[0];
        var aggConfig = d[1];
        var aggConfigKeys = Object.keys(aggConfig);
        var agg = aggConfigKeys[0];
        newGroupByList[aggName] = _objectSpread({
          agg: agg,
          aggName: aggName,
          dropDownName: ''
        }, aggConfig[agg]);
      });
    }

    setGroupByList(newGroupByList);
    var newAggList = {};

    if (pivotConfig !== undefined && pivotConfig.aggregations !== undefined) {
      Object.entries(pivotConfig.aggregations).forEach(function (d) {
        var aggName = d[0];
        var aggConfig = d[1];
        var aggConfigKeys = Object.keys(aggConfig);
        var agg = aggConfigKeys[0];
        newAggList[aggName] = _objectSpread({
          agg: agg,
          aggName: aggName,
          dropDownName: ''
        }, aggConfig[agg]);
      });
    }

    setAggList(newAggList);
    var prettyPivotConfig = JSON.stringify(pivotConfig, null, 2);
    setAdvancedEditorConfig(prettyPivotConfig);
    setAdvancedEditorConfigLastApplied(prettyPivotConfig);
    setAdvancedPivotEditorApplyButtonEnabled(false);
  };

  var toggleAdvancedEditor = function toggleAdvancedEditor() {
    setAdvancedEditorConfig(advancedEditorConfig);
    setAdvancedPivotEditorEnabled(!isAdvancedPivotEditorEnabled);
    setAdvancedPivotEditorApplyButtonEnabled(false);

    if (isAdvancedPivotEditorEnabled === false) {
      setAdvancedEditorConfigLastApplied(advancedEditorConfig);
    }
  }; // If switching to KQL after updating via editor - reset search


  var toggleAdvancedSourceEditor = function toggleAdvancedSourceEditor() {
    var reset = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

    if (reset === true) {
      setSearchQuery(defaultSearch);
      setSearchString(defaultSearch);
      setSourceConfigUpdated(false);
    }

    if (isAdvancedSourceEditorEnabled === false) {
      setAdvancedEditorSourceConfigLastApplied(advancedEditorSourceConfig);
    }

    setAdvancedSourceEditorEnabled(!isAdvancedSourceEditorEnabled);
    setAdvancedSourceEditorApplyButtonEnabled(false);
  }; // metadata.branch corresponds to the version used in documentation links.


  var docsUrl = "https://www.elastic.co/guide/en/elasticsearch/reference/".concat(_metadata.metadata.branch, "/transform-pivot.html");

  var advancedEditorHelpText = _react.default.createElement(_react.Fragment, null, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorHelpText', {
    defaultMessage: 'The advanced editor allows you to edit the pivot configuration of the data frame transform.'
  }), ' ', _react.default.createElement(_eui.EuiLink, {
    href: docsUrl,
    target: "_blank"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorHelpTextLink', {
    defaultMessage: 'Learn more about available options.'
  })));

  var sourceDocsUrl = "https://www.elastic.co/guide/en/elasticsearch/reference/".concat(_metadata.metadata.branch, "/query-dsl.html");

  var advancedSourceEditorHelpText = _react.default.createElement(_react.Fragment, null, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedSourceEditorHelpText', {
    defaultMessage: 'The advanced editor allows you to edit the source query clause of the data frame transform.'
  }), ' ', _react.default.createElement(_eui.EuiLink, {
    href: sourceDocsUrl,
    target: "_blank"
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorHelpTextLink', {
    defaultMessage: 'Learn more about available options.'
  })));

  var valid = pivotGroupByArr.length > 0 && pivotAggsArr.length > 0;
  (0, _react.useEffect)(function () {
    var previewRequestUpdate = (0, _common2.getPreviewRequestBody)(indexPattern.title, pivotQuery, pivotGroupByArr, pivotAggsArr);
    var stringifiedPivotConfigUpdate = JSON.stringify(previewRequestUpdate.pivot, null, 2);
    var stringifiedSourceConfigUpdate = JSON.stringify(previewRequestUpdate.source.query, null, 2);
    setAdvancedEditorConfig(stringifiedPivotConfigUpdate);
    setAdvancedEditorSourceConfig(stringifiedSourceConfigUpdate);
    onChange({
      aggList: aggList,
      groupByList: groupByList,
      isAdvancedPivotEditorEnabled: isAdvancedPivotEditorEnabled,
      isAdvancedSourceEditorEnabled: isAdvancedSourceEditorEnabled,
      searchString: searchString,
      searchQuery: searchQuery,
      sourceConfigUpdated: sourceConfigUpdated,
      valid: valid
    });
  }, [JSON.stringify(pivotAggsArr), JSON.stringify(pivotGroupByArr), isAdvancedPivotEditorEnabled, isAdvancedSourceEditorEnabled, searchString, searchQuery, valid]); // TODO This should use the actual value of `indices.query.bool.max_clause_count`

  var maxIndexFields = 1024;
  var numIndexFields = indexPattern.fields.length;
  var disabledQuery = numIndexFields > maxIndexFields;
  return _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      minWidth: '420px'
    }
  }, _react.default.createElement(_eui.EuiForm, null, kibanaContext.currentSavedSearch.id === undefined && typeof searchString === 'string' && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.indexPatternLabel', {
      defaultMessage: 'Index pattern'
    }),
    helpText: disabledQuery ? _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.indexPatternHelpText', {
      defaultMessage: 'An optional query for this index pattern is not supported. The number of supported index fields is {maxIndexFields} whereas this index has {numIndexFields} fields.',
      values: {
        maxIndexFields: maxIndexFields,
        numIndexFields: numIndexFields
      }
    }) : ''
  }, _react.default.createElement("span", null, kibanaContext.currentIndexPattern.title)), !disabledQuery && _react.default.createElement(_react.Fragment, null, !isAdvancedSourceEditorEnabled && _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.queryLabel', {
      defaultMessage: 'Query'
    }),
    helpText: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.queryHelpText', {
      defaultMessage: 'Use a query to filter the source data (optional).'
    })
  }, _react.default.createElement(_kql_filter_bar.KqlFilterBar, {
    indexPattern: indexPattern,
    onSubmit: searchHandler,
    initialValue: searchString === defaultSearch ? emptySearch : searchString,
    placeholder: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.queryPlaceholder', {
      defaultMessage: 'e.g. {example}',
      values: {
        example: 'method : "GET" or status : "404"'
      }
    })
  })))), isAdvancedSourceEditorEnabled && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedSourceEditorLabel', {
      defaultMessage: 'Source query clause'
    }),
    helpText: advancedSourceEditorHelpText
  }, _react.default.createElement(_eui.EuiPanel, {
    grow: false,
    paddingSize: "none"
  }, _react.default.createElement(_eui.EuiCodeEditor, {
    mode: "json",
    width: "100%",
    value: advancedEditorSourceConfig,
    onChange: function onChange(d) {
      setAdvancedEditorSourceConfig(d); // Disable the "Apply"-Button if the config hasn't changed.

      if (advancedEditorSourceConfigLastApplied === d) {
        setAdvancedSourceEditorApplyButtonEnabled(false);
        return;
      } // Try to parse the string passed on from the editor.
      // If parsing fails, the "Apply"-Button will be disabled


      try {
        JSON.parse(d);
        setAdvancedSourceEditorApplyButtonEnabled(true);
      } catch (e) {
        setAdvancedSourceEditorApplyButtonEnabled(false);
      }
    },
    setOptions: {
      fontSize: '12px'
    },
    "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedSourceEditorAriaLabel', {
      defaultMessage: 'Advanced query editor'
    })
  })))), kibanaContext.currentSavedSearch.id === undefined && _react.default.createElement(_eui.EuiFormRow, null, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiSwitch, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorSourceConfigSwitchLabel', {
      defaultMessage: 'Advanced query editor'
    }),
    checked: isAdvancedSourceEditorEnabled,
    onChange: function onChange() {
      if (isAdvancedSourceEditorEnabled && sourceConfigUpdated) {
        setAdvancedSourceEditorSwitchModalVisible(true);
        return;
      }

      toggleAdvancedSourceEditor();
    }
  }), isAdvancedSourceEditorSwitchModalVisible && _react.default.createElement(_switch_modal.SwitchModal, {
    onCancel: function onCancel() {
      return setAdvancedSourceEditorSwitchModalVisible(false);
    },
    onConfirm: function onConfirm() {
      setAdvancedSourceEditorSwitchModalVisible(false);
      toggleAdvancedSourceEditor(true);
    },
    type: 'source'
  })), isAdvancedSourceEditorEnabled && _react.default.createElement(_eui.EuiButton, {
    size: "s",
    fill: true,
    onClick: applyAdvancedSourceEditorChanges,
    disabled: !isAdvancedSourceEditorApplyButtonEnabled
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedSourceEditorApplyButtonText', {
    defaultMessage: 'Apply changes'
  })))), kibanaContext.currentSavedSearch.id !== undefined && _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.savedSearchLabel', {
      defaultMessage: 'Saved search'
    })
  }, _react.default.createElement("span", null, kibanaContext.currentSavedSearch.title)), !isAdvancedPivotEditorEnabled && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.groupByLabel', {
      defaultMessage: 'Group by'
    })
  }, _react.default.createElement(_react.Fragment, null, _react.default.createElement(_group_by_list.GroupByListForm, {
    list: groupByList,
    options: groupByOptionsData,
    onChange: updateGroupBy,
    deleteHandler: deleteGroupBy
  }), _react.default.createElement(_aggregation_dropdown.DropDown, {
    changeHandler: addGroupBy,
    options: groupByOptions,
    placeholder: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.groupByPlaceholder', {
      defaultMessage: 'Add a group by field ...'
    })
  }))), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.aggregationsLabel', {
      defaultMessage: 'Aggregations'
    })
  }, _react.default.createElement(_react.Fragment, null, _react.default.createElement(_aggregation_list.AggListForm, {
    list: aggList,
    options: aggOptionsData,
    onChange: updateAggregation,
    deleteHandler: deleteAggregation
  }), _react.default.createElement(_aggregation_dropdown.DropDown, {
    changeHandler: addAggregation,
    options: aggOptions,
    placeholder: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.aggregationsPlaceholder', {
      defaultMessage: 'Add an aggregation ...'
    })
  })))), isAdvancedPivotEditorEnabled && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorLabel', {
      defaultMessage: 'Pivot configuration object'
    }),
    helpText: advancedEditorHelpText
  }, _react.default.createElement(_eui.EuiPanel, {
    grow: false,
    paddingSize: "none"
  }, _react.default.createElement(_eui.EuiCodeEditor, {
    mode: "json",
    width: "100%",
    value: advancedEditorConfig,
    onChange: function onChange(d) {
      setAdvancedEditorConfig(d); // Disable the "Apply"-Button if the config hasn't changed.

      if (advancedEditorConfigLastApplied === d) {
        setAdvancedPivotEditorApplyButtonEnabled(false);
        return;
      } // Try to parse the string passed on from the editor.
      // If parsing fails, the "Apply"-Button will be disabled


      try {
        JSON.parse(d);
        setAdvancedPivotEditorApplyButtonEnabled(true);
      } catch (e) {
        setAdvancedPivotEditorApplyButtonEnabled(false);
      }
    },
    setOptions: {
      fontSize: '12px'
    },
    "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorAriaLabel', {
      defaultMessage: 'Advanced pivot editor'
    })
  })))), _react.default.createElement(_eui.EuiFormRow, null, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "none"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiSwitch, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorSwitchLabel', {
      defaultMessage: 'Advanced pivot editor'
    }),
    checked: isAdvancedPivotEditorEnabled,
    onChange: function onChange() {
      if (isAdvancedPivotEditorEnabled && (isAdvancedPivotEditorApplyButtonEnabled || advancedEditorConfig !== advancedEditorConfigLastApplied)) {
        setAdvancedEditorSwitchModalVisible(true);
        return;
      }

      toggleAdvancedEditor();
    }
  }), isAdvancedEditorSwitchModalVisible && _react.default.createElement(_switch_modal.SwitchModal, {
    onCancel: function onCancel() {
      return setAdvancedEditorSwitchModalVisible(false);
    },
    onConfirm: function onConfirm() {
      setAdvancedEditorSwitchModalVisible(false);
      toggleAdvancedEditor();
    },
    type: 'pivot'
  })), isAdvancedPivotEditorEnabled && _react.default.createElement(_eui.EuiButton, {
    size: "s",
    fill: true,
    onClick: applyAdvancedPivotEditorChanges,
    disabled: !isAdvancedPivotEditorApplyButtonEnabled
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.advancedEditorApplyButtonText', {
    defaultMessage: 'Apply changes'
  })))), !valid && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormHelpText, {
    style: {
      maxWidth: '320px'
    }
  }, _i18n.i18n.translate('xpack.ml.dataframe.stepDefineForm.formHelp', {
    defaultMessage: 'Data frame transforms are scalable and automated processes for pivoting. Choose at least one group-by and aggregation to get started.'
  }))))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_source_index_preview.SourceIndexPreview, {
    cellClick: addToSearch,
    query: pivotQuery
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_pivot_preview.PivotPreview, {
    aggs: aggList,
    groupBy: groupByList,
    query: pivotQuery
  })));
});

exports.StepDefineForm = StepDefineForm;