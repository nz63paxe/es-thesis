"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JobValidator = void 0;

var _job_utils = require("../../../../../common/util/job_utils");

var _new_job_defaults = require("../../../new_job/utils/new_job_defaults");

var _util = require("./util");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// delay start of validation to allow the user to make changes
// e.g. if they are typing in a new value, try not to validate
// after every keystroke
var VALIDATION_DELAY_MS = 500;

var JobValidator =
/*#__PURE__*/
function () {
  function JobValidator(jobCreator, existingJobsAndGroups) {
    _classCallCheck(this, JobValidator);

    _defineProperty(this, "_jobCreator", void 0);

    _defineProperty(this, "_validationSummary", void 0);

    _defineProperty(this, "_lastJobConfig", void 0);

    _defineProperty(this, "_validateTimeout", null);

    _defineProperty(this, "_existingJobsAndGroups", void 0);

    _defineProperty(this, "_basicValidations", {
      jobId: {
        valid: true
      },
      groupIds: {
        valid: true
      },
      modelMemoryLimit: {
        valid: true
      },
      bucketSpan: {
        valid: true
      },
      duplicateDetectors: {
        valid: true
      }
    });

    _defineProperty(this, "_validating", false);

    this._jobCreator = jobCreator;
    this._lastJobConfig = this._jobCreator.formattedJobJson;
    this._validationSummary = {
      basic: false,
      advanced: false
    };
    this._existingJobsAndGroups = existingJobsAndGroups;
  }

  _createClass(JobValidator, [{
    key: "validate",
    value: function validate(callback) {
      var _this = this;

      this._validating = true;
      var formattedJobConfig = this._jobCreator.formattedJobJson; // only validate if the config has changed

      if (formattedJobConfig !== this._lastJobConfig) {
        if (this._validateTimeout !== null) {
          // clear any previous on going validation timeouts
          clearTimeout(this._validateTimeout);
        }

        this._lastJobConfig = formattedJobConfig;
        this._validateTimeout = setTimeout(function () {
          _this._runBasicValidation();

          _this._validating = false;
          _this._validateTimeout = null;
          callback();
        }, VALIDATION_DELAY_MS);
      } else {
        // _validating is still true if there is a previous validation timeout on going.
        this._validating = this._validateTimeout !== null;
      }

      callback();
    }
  }, {
    key: "_resetBasicValidations",
    value: function _resetBasicValidations() {
      this._validationSummary.basic = true;
      Object.values(this._basicValidations).forEach(function (v) {
        v.valid = true;
        delete v.message;
      });
    }
  }, {
    key: "_runBasicValidation",
    value: function _runBasicValidation() {
      this._resetBasicValidations();

      var jobConfig = this._jobCreator.jobConfig;
      var limits = (0, _new_job_defaults.newJobLimits)(); // run standard basic validation

      var basicResults = (0, _job_utils.basicJobValidation)(jobConfig, undefined, limits);
      (0, _util.populateValidationMessages)(basicResults, this._basicValidations, jobConfig); // run addition job and group id validation

      var idResults = (0, _util.checkForExistingJobAndGroupIds)(this._jobCreator.jobId, this._jobCreator.groups, this._existingJobsAndGroups);
      (0, _util.populateValidationMessages)(idResults, this._basicValidations, jobConfig);
      this._validationSummary.basic = this._isOverallBasicValid();
    }
  }, {
    key: "_isOverallBasicValid",
    value: function _isOverallBasicValid() {
      return Object.values(this._basicValidations).some(function (v) {
        return v.valid === false;
      }) === false;
    }
  }, {
    key: "validationSummary",
    get: function get() {
      return this._validationSummary;
    }
  }, {
    key: "bucketSpan",
    get: function get() {
      return this._basicValidations.bucketSpan;
    }
  }, {
    key: "duplicateDetectors",
    get: function get() {
      return this._basicValidations.duplicateDetectors;
    }
  }, {
    key: "jobId",
    get: function get() {
      return this._basicValidations.jobId;
    }
  }, {
    key: "groupIds",
    get: function get() {
      return this._basicValidations.groupIds;
    }
  }, {
    key: "modelMemoryLimit",
    get: function get() {
      return this._basicValidations.modelMemoryLimit;
    }
  }, {
    key: "advancedValid",
    set: function set(valid) {
      this._validationSummary.advanced = valid;
    }
  }, {
    key: "validating",
    get: function get() {
      return this._validating;
    }
  }]);

  return JobValidator;
}();

exports.JobValidator = JobValidator;