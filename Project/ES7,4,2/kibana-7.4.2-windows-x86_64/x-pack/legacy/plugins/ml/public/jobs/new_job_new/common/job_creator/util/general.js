"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRichDetectors = getRichDetectors;
exports.isSparseDataJob = isSparseDataJob;
exports.convertToMultiMetricJob = convertToMultiMetricJob;
exports.convertToAdvancedJob = convertToAdvancedJob;
exports.resetJob = resetJob;

var _new_job_capabilities_service = require("../../../../../services/new_job_capabilities_service");

var _aggregation_types = require("../../../../../../common/constants/aggregation_types");

var _fields = require("../../../../../../common/types/fields");

var _job_service = require("../../../../../services/job_service");

var _2 = require("../");

var _constants = require("./constants");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// populate the detectors with Field and Agg objects loaded from the job capabilities service
function getRichDetectors(job, datafeed) {
  var detectors = getDetectors(job, datafeed);
  return detectors.map(function (d) {
    return {
      agg: _new_job_capabilities_service.newJobCapsService.getAggById(d.function),
      field: d.field_name !== undefined ? _new_job_capabilities_service.newJobCapsService.getFieldById(d.field_name) : null,
      byField: d.by_field_name !== undefined ? _new_job_capabilities_service.newJobCapsService.getFieldById(d.by_field_name) : null,
      overField: d.over_field_name !== undefined ? _new_job_capabilities_service.newJobCapsService.getFieldById(d.over_field_name) : null,
      partitionField: d.partition_field_name !== undefined ? _new_job_capabilities_service.newJobCapsService.getFieldById(d.partition_field_name) : null
    };
  });
}

function getDetectors(job, datafeed) {
  var detectors = job.analysis_config.detectors;
  var sparseData = isSparseDataJob(job, datafeed); // if aggregations have been used in a single metric job and a distinct count detector
  // was used, we need to rebuild the detector.

  if (datafeed.aggregations !== undefined && job.analysis_config.detectors[0].function === _aggregation_types.ML_JOB_AGGREGATION.NON_ZERO_COUNT && sparseData === false) {
    // distinct count detector, field has been removed.
    // determine field from datafeed aggregations
    var field = datafeed != null && datafeed.aggregations != null && datafeed.aggregations.buckets != null && datafeed.aggregations.buckets.aggregations != null && datafeed.aggregations.buckets.aggregations.dc_region != null && datafeed.aggregations.buckets.aggregations.dc_region.cardinality != null ? datafeed.aggregations.buckets.aggregations.dc_region.cardinality.field : undefined;

    if (field !== undefined) {
      detectors = [{
        function: _aggregation_types.ML_JOB_AGGREGATION.DISTINCT_COUNT,
        field_name: field
      }];
    }
  } else {
    // all other detectors.
    detectors = detectors.map(function (d) {
      switch (d.function) {
        // if a count function is used, add EVENT_RATE_FIELD_ID as its field
        case _aggregation_types.ML_JOB_AGGREGATION.COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID
          });

        case _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID
          });

        case _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID
          });
        // if sparse data functions were used, replace them with their non-sparse versions
        // the sparse data flag has already been determined and set, so this information is not being lost.

        case _aggregation_types.ML_JOB_AGGREGATION.NON_ZERO_COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID,
            function: _aggregation_types.ML_JOB_AGGREGATION.COUNT
          });

        case _aggregation_types.ML_JOB_AGGREGATION.HIGH_NON_ZERO_COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID,
            function: _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT
          });

        case _aggregation_types.ML_JOB_AGGREGATION.LOW_NON_ZERO_COUNT:
          return _objectSpread({}, d, {
            field_name: _fields.EVENT_RATE_FIELD_ID,
            function: _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT
          });

        case _aggregation_types.ML_JOB_AGGREGATION.NON_NULL_SUM:
          return _objectSpread({}, d, {
            function: _aggregation_types.ML_JOB_AGGREGATION.SUM
          });

        case _aggregation_types.ML_JOB_AGGREGATION.HIGH_NON_NULL_SUM:
          return _objectSpread({}, d, {
            function: _aggregation_types.ML_JOB_AGGREGATION.HIGH_SUM
          });

        case _aggregation_types.ML_JOB_AGGREGATION.LOW_NON_NULL_SUM:
          return _objectSpread({}, d, {
            function: _aggregation_types.ML_JOB_AGGREGATION.LOW_SUM
          });

        default:
          return d;
      }
    });
  }

  return detectors;
} // determine whether the job has been configured to run on sparse data
// by looking to see whether the datafeed contains a dc_region field in an aggregation
// if it does, it is a distinct count single metric job and no a sparse data job.
// this check is needed because distinct count jobs also use NON_ZERO_COUNT


function isSparseDataJob(job, datafeed) {
  var detectors = job.analysis_config.detectors;
  var distinctCountField = datafeed != null && datafeed.aggregations != null && datafeed.aggregations.buckets != null && datafeed.aggregations.buckets.aggregations != null && datafeed.aggregations.buckets.aggregations.dc_region != null && datafeed.aggregations.buckets.aggregations.dc_region.cardinality != null ? datafeed.aggregations.buckets.aggregations.dc_region.cardinality.field : undefined; // if distinctCountField is undefined, and any detectors contain a sparse data function
  // return true

  if (distinctCountField === undefined) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = detectors[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var detector = _step.value;

        if (_aggregation_types.SPARSE_DATA_AGGREGATIONS.includes(detector.function)) {
          return true;
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator.return != null) {
          _iterator.return();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }

  return false;
}

function stashCombinedJob(jobCreator) {
  var skipTimeRangeStep = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var advanced = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var includeTimeRange = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : false;

  var combinedJob = _objectSpread({}, jobCreator.jobConfig, {
    datafeed_config: jobCreator.datafeedConfig
  });

  if (advanced === true) {
    _job_service.mlJobService.currentJob = combinedJob;
  } else {
    _job_service.mlJobService.tempJobCloningObjects.job = combinedJob; // skip over the time picker step of the wizard

    _job_service.mlJobService.tempJobCloningObjects.skipTimeRangeStep = skipTimeRangeStep;

    if (includeTimeRange === true) {
      // auto select the start and end dates of the time picker
      _job_service.mlJobService.tempJobCloningObjects.start = jobCreator.start;
      _job_service.mlJobService.tempJobCloningObjects.end = jobCreator.end;
    }
  }
}

function convertToMultiMetricJob(jobCreator) {
  jobCreator.createdBy = _constants.CREATED_BY_LABEL.MULTI_METRIC;
  jobCreator.modelPlot = false;
  stashCombinedJob(jobCreator, true, false, true);
  window.location.href = window.location.href.replace(_constants.JOB_TYPE.SINGLE_METRIC, _constants.JOB_TYPE.MULTI_METRIC);
}

function convertToAdvancedJob(jobCreator) {
  jobCreator.createdBy = null;
  stashCombinedJob(jobCreator, false, true, false);
  var jobType = _constants.JOB_TYPE.SINGLE_METRIC;

  if ((0, _2.isMultiMetricJobCreator)(jobCreator)) {
    jobType = _constants.JOB_TYPE.MULTI_METRIC;
  } else if ((0, _2.isPopulationJobCreator)(jobCreator)) {
    jobType = _constants.JOB_TYPE.POPULATION;
  }

  window.location.href = window.location.href.replace(jobType, _constants.JOB_TYPE.ADVANCED);
}

function resetJob(jobCreator) {
  jobCreator.jobId = '';
  stashCombinedJob(jobCreator, true, false, true);
  window.location.href = '#/jobs/new_job';
}