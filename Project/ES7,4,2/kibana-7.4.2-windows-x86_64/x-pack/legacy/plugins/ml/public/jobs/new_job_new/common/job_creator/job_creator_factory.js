"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.jobCreatorFactory = void 0;

var _single_metric_job_creator = require("./single_metric_job_creator");

var _multi_metric_job_creator = require("./multi_metric_job_creator");

var _population_job_creator = require("./population_job_creator");

var _constants = require("./util/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var jobCreatorFactory = function jobCreatorFactory(jobType) {
  return function (indexPattern, savedSearch, query) {
    var jc;

    switch (jobType) {
      case _constants.JOB_TYPE.SINGLE_METRIC:
        jc = _single_metric_job_creator.SingleMetricJobCreator;
        break;

      case _constants.JOB_TYPE.MULTI_METRIC:
        jc = _multi_metric_job_creator.MultiMetricJobCreator;
        break;

      case _constants.JOB_TYPE.POPULATION:
        jc = _population_job_creator.PopulationJobCreator;
        break;

      default:
        jc = _single_metric_job_creator.SingleMetricJobCreator;
        break;
    }

    return new jc(indexPattern, savedSearch, query);
  };
};

exports.jobCreatorFactory = jobCreatorFactory;