"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Example = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var Example = function Example(_ref) {
  var example = _ref.example;
  var exampleStr = typeof example === 'string' ? example : JSON.stringify(example); // Use 95% width for each example so that the truncation ellipses show up when
  // wrapped inside a tooltip.

  return _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      width: '95%'
    },
    className: "eui-textTruncate"
  }, _react.default.createElement(_eui.EuiToolTip, {
    content: exampleStr
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    className: "eui-textTruncate"
  }, exampleStr))));
};

exports.Example = Example;