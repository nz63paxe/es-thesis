"use strict";

require("./pages/analytics_exploration/directive");

require("./pages/analytics_exploration/route");

require("./pages/analytics_management/directive");

require("./pages/analytics_management/route");