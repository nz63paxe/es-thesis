"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Page = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _navigation_menu = require("../../../components/navigation_menu/navigation_menu");

var _common = require("../../common");

var _create_transform_button = require("./components/create_transform_button");

var _transform_list = require("./components/transform_list");

var _refresh_transform_list_button = require("./components/refresh_transform_list_button");

var _transforms_stats_bar = require("../transform_management/components/transform_list/transforms_stats_bar");

var _transform_service = require("./services/transform_service");

var _use_refresh_interval = require("./components/transform_list/use_refresh_interval");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Page = function Page() {
  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isLoading = _useState2[0],
      setIsLoading = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      transformsLoading = _useState4[0],
      setTransformsLoading = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isInitialized = _useState6[0],
      setIsInitialized = _useState6[1];

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      blockRefresh = _useState8[0],
      setBlockRefresh = _useState8[1];

  var _useState9 = (0, _react.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      transforms = _useState10[0],
      setTransforms = _useState10[1];

  var _useState11 = (0, _react.useState)(undefined),
      _useState12 = _slicedToArray(_useState11, 2),
      errorMessage = _useState12[0],
      setErrorMessage = _useState12[1];

  var _useRefreshTransformL = (0, _common.useRefreshTransformList)({
    isLoading: setIsLoading
  }),
      refresh = _useRefreshTransformL.refresh;

  var getTransforms = (0, _transform_service.getTransformsFactory)(setTransforms, setErrorMessage, setIsInitialized, blockRefresh); // Subscribe to the refresh observable to trigger reloading the transform list.

  (0, _common.useRefreshTransformList)({
    isLoading: setTransformsLoading,
    onRefresh: function onRefresh() {
      return getTransforms(true);
    }
  }); // Call useRefreshInterval() after the subscription above is set up.

  (0, _use_refresh_interval.useRefreshInterval)(setBlockRefresh);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_navigation_menu.NavigationMenu, {
    tabId: "data_frames"
  }), _react.default.createElement(_transforms_stats_bar.TransformStatsBar, {
    transformsList: transforms
  }), _react.default.createElement(_eui.EuiPage, {
    "data-test-subj": "mlPageDataFrame"
  }, _react.default.createElement(_eui.EuiPageBody, null, _react.default.createElement(_eui.EuiPageContentHeader, null, _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h1", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.transformList.dataFrameTitle",
    defaultMessage: "Data frame transforms"
  }), _react.default.createElement("span", null, "\xA0"), _react.default.createElement(_eui.EuiBetaBadge, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.transformList.betaBadgeLabel', {
      defaultMessage: "Beta"
    }),
    tooltipContent: _i18n.i18n.translate('xpack.ml.dataframe.transformList.betaBadgeTooltipContent', {
      defaultMessage: "Data frames are a beta feature. We'd love to hear your feedback."
    })
  })))), _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_refresh_transform_list_button.RefreshTransformListButton, {
    onClick: refresh,
    isLoading: isLoading
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_create_transform_button.CreateTransformButton, null))))), _react.default.createElement(_eui.EuiPageContentBody, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(_transform_list.DataFrameTransformList, {
    transforms: transforms,
    isInitialized: isInitialized,
    errorMessage: errorMessage,
    transformsLoading: transformsLoading
  }))))));
};

exports.Page = Page;