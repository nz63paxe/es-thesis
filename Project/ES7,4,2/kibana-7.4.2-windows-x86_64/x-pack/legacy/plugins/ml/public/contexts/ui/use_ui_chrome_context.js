"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUiChromeContext = void 0;

var _react = require("react");

var _ui_context = require("./ui_context");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useUiChromeContext = function useUiChromeContext() {
  return (0, _react.useContext)(_ui_context.UiContext).chrome;
};

exports.useUiChromeContext = useUiChromeContext;