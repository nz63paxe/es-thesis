"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _modules = require("ui/modules");

var _i18n = require("ui/i18n");

var _new_job_utils = require("../../../jobs/new_job/utils/new_job_utils");

var _kibana = require("../../../contexts/kibana");

var _page = require("./page");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var _module = _modules.uiModules.get('apps/ml', ['react']);

_module.directive('mlDataFrameAnalyticsExploration', function ($injector) {
  return {
    scope: {},
    restrict: 'E',
    link: function link(scope, element) {
      var globalState = $injector.get('globalState');
      globalState.fetch();
      var indexPatterns = $injector.get('indexPatterns');
      var kbnBaseUrl = $injector.get('kbnBaseUrl');
      var kibanaConfig = $injector.get('config');
      var Private = $injector.get('Private');
      var createSearchItems = Private(_new_job_utils.SearchItemsProvider);

      var _createSearchItems = createSearchItems(),
          indexPattern = _createSearchItems.indexPattern,
          savedSearch = _createSearchItems.savedSearch,
          combinedQuery = _createSearchItems.combinedQuery;

      var kibanaContext = {
        combinedQuery: combinedQuery,
        currentIndexPattern: indexPattern,
        currentSavedSearch: savedSearch,
        indexPatterns: indexPatterns,
        kbnBaseUrl: kbnBaseUrl,
        kibanaConfig: kibanaConfig
      };

      _reactDom.default.render(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_kibana.KibanaContext.Provider, {
        value: kibanaContext
      }, _react.default.createElement(_page.Page, {
        jobId: globalState.ml.jobId
      }))), element[0]);

      element.on('$destroy', function () {
        _reactDom.default.unmountComponentAtNode(element[0]);

        scope.$destroy();
      });
    }
  };
});