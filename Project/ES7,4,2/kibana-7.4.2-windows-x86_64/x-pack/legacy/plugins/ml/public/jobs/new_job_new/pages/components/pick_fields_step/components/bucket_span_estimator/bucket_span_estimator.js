"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BucketSpanEstimator = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _estimate_bucket_span = require("./estimate_bucket_span");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var BucketSpanEstimator = function BucketSpanEstimator(_ref) {
  var setEstimating = _ref.setEstimating;

  var _useEstimateBucketSpa = (0, _estimate_bucket_span.useEstimateBucketSpan)(),
      status = _useEstimateBucketSpa.status,
      estimateBucketSpan = _useEstimateBucketSpa.estimateBucketSpan;

  (0, _react.useEffect)(function () {
    setEstimating(status === _estimate_bucket_span.ESTIMATE_STATUS.RUNNING);
  }, [status]);
  return _react.default.createElement(_eui.EuiButton, {
    disabled: status === _estimate_bucket_span.ESTIMATE_STATUS.RUNNING,
    onClick: estimateBucketSpan
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.pickFieldsStep.bucketSpanEstimatorButton",
    defaultMessage: "Estimate bucket span"
  }));
};

exports.BucketSpanEstimator = BucketSpanEstimator;