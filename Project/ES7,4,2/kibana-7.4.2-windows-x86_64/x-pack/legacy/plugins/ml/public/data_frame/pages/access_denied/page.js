"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Page = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _navigation_menu = require("../../../components/navigation_menu/navigation_menu");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var Page = function Page(_ref) {
  var goToKibana = _ref.goToKibana,
      retry = _ref.retry;
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_navigation_menu.NavigationMenu, {
    tabId: "access-denied"
  }), _react.default.createElement(_eui.EuiPage, null, _react.default.createElement(_eui.EuiPageBody, null, _react.default.createElement(_eui.EuiPageContentHeader, null, _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h1", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.accessDeniedTitle",
    defaultMessage: "Access denied"
  }))))), _react.default.createElement(_eui.EuiPageContentBody, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiCallOut, {
    title: _i18n.i18n.translate('xpack.ml.dataframe.noPermissionToAccessMLLabel', {
      defaultMessage: 'You need permission to access Data Frames'
    }),
    color: "danger",
    iconType: "cross"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.noGrantedPrivilegesDescription",
    defaultMessage: "You must have the privileges granted in the {kibanaUserParam} and {dataFrameUserParam} roles.{br}Your system admin can set these roles on the Management User page.",
    values: {
      kibanaUserParam: _react.default.createElement("span", {
        className: "text-monospace"
      }, "kibana_user"),
      dataFrameUserParam: _react.default.createElement("span", {
        className: "text-monospace"
      }, "data_frame_transforms_user"),
      br: _react.default.createElement("br", null)
    }
  })))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    onClick: goToKibana,
    size: "s"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.accessDenied.backToKibanaHomeButtonLabel",
    defaultMessage: "Back to Kibana home"
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    onClick: retry,
    size: "s"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.accessDenied.retryButtonLabel",
    defaultMessage: "Retry"
  }))))))));
};

exports.Page = Page;