"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isDataFrameTransformStats = isDataFrameTransformStats;
exports.getTransformProgress = getTransformProgress;
exports.isCompletedBatchTransform = isCompletedBatchTransform;
exports.DATA_FRAME_MODE = exports.DATA_FRAME_TRANSFORM_STATE = void 0;

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// reflects https://github.com/elastic/elasticsearch/blob/master/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/dataframe/transforms/DataFrameTransformStats.java#L243
var DATA_FRAME_TRANSFORM_STATE;
exports.DATA_FRAME_TRANSFORM_STATE = DATA_FRAME_TRANSFORM_STATE;

(function (DATA_FRAME_TRANSFORM_STATE) {
  DATA_FRAME_TRANSFORM_STATE["ABORTING"] = "aborting";
  DATA_FRAME_TRANSFORM_STATE["FAILED"] = "failed";
  DATA_FRAME_TRANSFORM_STATE["INDEXING"] = "indexing";
  DATA_FRAME_TRANSFORM_STATE["STARTED"] = "started";
  DATA_FRAME_TRANSFORM_STATE["STOPPED"] = "stopped";
  DATA_FRAME_TRANSFORM_STATE["STOPPING"] = "stopping";
})(DATA_FRAME_TRANSFORM_STATE || (exports.DATA_FRAME_TRANSFORM_STATE = DATA_FRAME_TRANSFORM_STATE = {}));

var DATA_FRAME_MODE;
exports.DATA_FRAME_MODE = DATA_FRAME_MODE;

(function (DATA_FRAME_MODE) {
  DATA_FRAME_MODE["BATCH"] = "batch";
  DATA_FRAME_MODE["CONTINUOUS"] = "continuous";
})(DATA_FRAME_MODE || (exports.DATA_FRAME_MODE = DATA_FRAME_MODE = {}));

function isDataFrameTransformStats(arg) {
  return _typeof(arg) === 'object' && arg !== null && {}.hasOwnProperty.call(arg, 'state') && Object.values(DATA_FRAME_TRANSFORM_STATE).includes(arg.state);
}

function getTransformProgress(item) {
  if (isCompletedBatchTransform(item)) {
    return 100;
  }

  var progress = item != null && item.stats != null && item.stats.checkpointing != null && item.stats.checkpointing.next != null && item.stats.checkpointing.next.checkpoint_progress != null ? item.stats.checkpointing.next.checkpoint_progress.percent_complete : undefined;
  return progress !== undefined ? Math.round(progress) : undefined;
}

function isCompletedBatchTransform(item) {
  // If `checkpoint=1`, `sync` is missing from the config and state is stopped,
  // then this is a completed batch data frame transform.
  return item.stats.checkpointing.last.checkpoint === 1 && item.config.sync === undefined && item.stats.state === DATA_FRAME_TRANSFORM_STATE.STOPPED;
}