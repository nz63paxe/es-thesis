"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NavigationMenu = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _check_license = require("../../license/check_license");

var _top_nav = require("./top_nav");

var _tabs = require("./tabs");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var tabSupport = ['jobs', 'settings', 'data_frames', 'data_frame_analytics', 'datavisualizer', 'filedatavisualizer', 'timeseriesexplorer', 'access-denied', 'explorer'];

var NavigationMenu = function NavigationMenu(_ref) {
  var tabId = _ref.tabId;
  var disableLinks = (0, _check_license.isFullLicense)() === false;
  var showTabs = tabSupport.includes(tabId);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexEnd",
    gutterSize: "xs"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_top_nav.TopNav, null))), showTabs && _react.default.createElement(_tabs.Tabs, {
    tabId: tabId,
    disableLinks: disableLinks
  }));
};

exports.NavigationMenu = NavigationMenu;