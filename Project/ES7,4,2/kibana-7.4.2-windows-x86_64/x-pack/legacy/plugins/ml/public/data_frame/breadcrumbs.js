"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getJobManagementBreadcrumbs = getJobManagementBreadcrumbs;
exports.getDataFrameBreadcrumbs = getDataFrameBreadcrumbs;
exports.getDataFrameCreateBreadcrumbs = getDataFrameCreateBreadcrumbs;
exports.getDataFrameIndexOrSearchBreadcrumbs = getDataFrameIndexOrSearchBreadcrumbs;

var _i18n = require("@kbn/i18n");

var _breadcrumbs = require("../breadcrumbs");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function getJobManagementBreadcrumbs() {
  // Whilst top level nav menu with tabs remains,
  // use root ML breadcrumb.
  return [_breadcrumbs.ML_BREADCRUMB];
}

function getDataFrameBreadcrumbs() {
  return [_breadcrumbs.ML_BREADCRUMB, {
    text: _i18n.i18n.translate('xpack.ml.dataFrameBreadcrumbs.dataFrameLabel', {
      defaultMessage: 'Transforms'
    }),
    href: ''
  }];
}

var DATA_FRAME_HOME = {
  text: _i18n.i18n.translate('xpack.ml.dataFrameBreadcrumbs.dataFrameLabel', {
    defaultMessage: 'Transforms'
  }),
  href: '#/data_frames'
};

function getDataFrameCreateBreadcrumbs() {
  return [_breadcrumbs.ML_BREADCRUMB, DATA_FRAME_HOME, {
    text: _i18n.i18n.translate('xpack.ml.dataFrameBreadcrumbs.dataFrameCreateLabel', {
      defaultMessage: 'Create transform'
    }),
    href: ''
  }];
}

function getDataFrameIndexOrSearchBreadcrumbs() {
  return [_breadcrumbs.ML_BREADCRUMB, DATA_FRAME_HOME, {
    text: _i18n.i18n.translate('xpack.ml.dataFrameBreadcrumbs.selectIndexOrSearchLabel', {
      defaultMessage: 'Select index or search'
    }),
    href: ''
  }];
}