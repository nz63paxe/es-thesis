"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JobCreator = void 0;

var _aggregation_types = require("../../../../../common/constants/aggregation_types");

var _default_configs = require("./util/default_configs");

var _job_service = require("../../../../services/job_service");

var _job_runner = require("../job_runner");

var _constants = require("./util/constants");

var _general = require("./util/general");

var _parse_interval = require("../../../../../common/util/parse_interval");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var JobCreator =
/*#__PURE__*/
function () {
  function JobCreator(indexPattern, savedSearch, query) {
    _classCallCheck(this, JobCreator);

    _defineProperty(this, "_type", _constants.JOB_TYPE.SINGLE_METRIC);

    _defineProperty(this, "_indexPattern", void 0);

    _defineProperty(this, "_savedSearch", void 0);

    _defineProperty(this, "_indexPatternTitle", '');

    _defineProperty(this, "_job_config", void 0);

    _defineProperty(this, "_datafeed_config", void 0);

    _defineProperty(this, "_detectors", void 0);

    _defineProperty(this, "_influencers", void 0);

    _defineProperty(this, "_bucketSpanMs", 0);

    _defineProperty(this, "_useDedicatedIndex", false);

    _defineProperty(this, "_start", 0);

    _defineProperty(this, "_end", 0);

    _defineProperty(this, "_subscribers", []);

    _defineProperty(this, "_aggs", []);

    _defineProperty(this, "_fields", []);

    _defineProperty(this, "_sparseData", false);

    _defineProperty(this, "_stopAllRefreshPolls", {
      stop: false
    });

    this._indexPattern = indexPattern;
    this._savedSearch = savedSearch;
    this._indexPatternTitle = indexPattern.title;
    this._job_config = (0, _default_configs.createEmptyJob)();
    this._datafeed_config = (0, _default_configs.createEmptyDatafeed)(this._indexPatternTitle);
    this._detectors = this._job_config.analysis_config.detectors;
    this._influencers = this._job_config.analysis_config.influencers;

    if (typeof indexPattern.timeFieldName === 'string') {
      this._job_config.data_description.time_field = indexPattern.timeFieldName;
    }

    this._datafeed_config.query = query;
  }

  _createClass(JobCreator, [{
    key: "_addDetector",
    value: function _addDetector(detector, agg, field) {
      this._detectors.push(detector);

      this._aggs.push(agg);

      this._fields.push(field);

      this._updateSparseDataDetectors();
    }
  }, {
    key: "_editDetector",
    value: function _editDetector(detector, agg, field, index) {
      if (this._detectors[index] !== undefined) {
        this._detectors[index] = detector;
        this._aggs[index] = agg;
        this._fields[index] = field;

        this._updateSparseDataDetectors();
      }
    }
  }, {
    key: "_removeDetector",
    value: function _removeDetector(index) {
      this._detectors.splice(index, 1);

      this._aggs.splice(index, 1);

      this._fields.splice(index, 1);
    }
  }, {
    key: "removeAllDetectors",
    value: function removeAllDetectors() {
      this._detectors.length = 0;
      this._aggs.length = 0;
      this._fields.length = 0;
    }
  }, {
    key: "getAggregation",
    value: function getAggregation(index) {
      var agg = this._aggs[index];
      return agg !== undefined ? agg : null;
    }
  }, {
    key: "getField",
    value: function getField(index) {
      var field = this._fields[index];
      return field !== undefined ? field : null;
    }
  }, {
    key: "_setBucketSpanMs",
    value: function _setBucketSpanMs(bucketSpan) {
      var bs = (0, _parse_interval.parseInterval)(bucketSpan);
      this._bucketSpanMs = bs === null ? 0 : bs.asMilliseconds();
    }
  }, {
    key: "addInfluencer",
    value: function addInfluencer(influencer) {
      if (this._influencers.includes(influencer) === false) {
        this._influencers.push(influencer);
      }
    }
  }, {
    key: "removeInfluencer",
    value: function removeInfluencer(influencer) {
      var idx = this._influencers.indexOf(influencer);

      if (idx !== -1) {
        this._influencers.splice(idx, 1);
      }
    }
  }, {
    key: "removeAllInfluencers",
    value: function removeAllInfluencers() {
      this._influencers.length = 0;
    }
  }, {
    key: "_updateSparseDataDetectors",
    value: function _updateSparseDataDetectors() {
      var _this = this;

      // loop through each detector, if the aggregation in the corresponding detector index is a count or sum
      // change the detector to be a non-zer or non-null count or sum.
      // note, the aggregations will always be a standard count or sum and not a non-null or non-zero version
      this._detectors.forEach(function (d, i) {
        switch (_this._aggs[i].id) {
          case _aggregation_types.ML_JOB_AGGREGATION.COUNT:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.NON_ZERO_COUNT : _aggregation_types.ML_JOB_AGGREGATION.COUNT;
            break;

          case _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.HIGH_NON_ZERO_COUNT : _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT;
            break;

          case _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.LOW_NON_ZERO_COUNT : _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT;
            break;

          case _aggregation_types.ML_JOB_AGGREGATION.SUM:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.NON_NULL_SUM : _aggregation_types.ML_JOB_AGGREGATION.SUM;
            break;

          case _aggregation_types.ML_JOB_AGGREGATION.HIGH_SUM:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.HIGH_NON_NULL_SUM : _aggregation_types.ML_JOB_AGGREGATION.HIGH_SUM;
            break;

          case _aggregation_types.ML_JOB_AGGREGATION.LOW_SUM:
            d.function = _this._sparseData ? _aggregation_types.ML_JOB_AGGREGATION.LOW_NON_NULL_SUM : _aggregation_types.ML_JOB_AGGREGATION.LOW_SUM;
            break;
        }
      });
    }
  }, {
    key: "setTimeRange",
    value: function setTimeRange(start, end) {
      this._start = start;
      this._end = end;
    }
  }, {
    key: "createAndStartJob",
    value: function () {
      var _createAndStartJob = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var jobRunner;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return this.createJob();

              case 3:
                _context.next = 5;
                return this.createDatafeed();

              case 5:
                _context.next = 7;
                return this.startDatafeed();

              case 7:
                jobRunner = _context.sent;
                return _context.abrupt("return", jobRunner);

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](0);
                throw _context.t0;

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 11]]);
      }));

      function createAndStartJob() {
        return _createAndStartJob.apply(this, arguments);
      }

      return createAndStartJob;
    }()
  }, {
    key: "createJob",
    value: function () {
      var _createJob = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        var _ref, success, resp;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _job_service.mlJobService.saveNewJob(this._job_config);

              case 3:
                _ref = _context2.sent;
                success = _ref.success;
                resp = _ref.resp;

                if (!(success === true)) {
                  _context2.next = 10;
                  break;
                }

                return _context2.abrupt("return", resp);

              case 10:
                throw resp;

              case 11:
                _context2.next = 16;
                break;

              case 13:
                _context2.prev = 13;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 13]]);
      }));

      function createJob() {
        return _createJob.apply(this, arguments);
      }

      return createJob;
    }()
  }, {
    key: "createDatafeed",
    value: function () {
      var _createDatafeed = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3() {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _job_service.mlJobService.saveNewDatafeed(this._datafeed_config, this._job_config.job_id);

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[0, 6]]);
      }));

      function createDatafeed() {
        return _createDatafeed.apply(this, arguments);
      }

      return createDatafeed;
    }() // create a jobRunner instance, start it and return it

  }, {
    key: "startDatafeed",
    value: function () {
      var _startDatafeed = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4() {
        var jobRunner;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                jobRunner = new _job_runner.JobRunner(this);
                _context4.next = 3;
                return jobRunner.startDatafeed();

              case 3:
                return _context4.abrupt("return", jobRunner);

              case 4:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function startDatafeed() {
        return _startDatafeed.apply(this, arguments);
      }

      return startDatafeed;
    }()
  }, {
    key: "subscribeToProgress",
    value: function subscribeToProgress(func) {
      this._subscribers.push(func);
    }
  }, {
    key: "forceStopRefreshPolls",
    value: function forceStopRefreshPolls() {
      this._stopAllRefreshPolls.stop = true;
    }
  }, {
    key: "_setCustomSetting",
    value: function _setCustomSetting(setting, value) {
      if (value === null) {
        // if null is passed in, delete the custom setting
        if (this._job_config.custom_settings !== undefined && this._job_config.custom_settings[setting] !== undefined) {
          delete this._job_config.custom_settings[setting];

          if (Object.keys(this._job_config.custom_settings).length === 0) {
            // clean up custom_settings if there's nothing else in there
            delete this._job_config.custom_settings;
          }
        }
      } else {
        if (this._job_config.custom_settings === undefined) {
          // if custom_settings doesn't exist, create it.
          this._job_config.custom_settings = _defineProperty({}, setting, value);
        } else {
          this._job_config.custom_settings[setting] = value;
        }
      }
    }
  }, {
    key: "_getCustomSetting",
    value: function _getCustomSetting(setting) {
      if (this._job_config.custom_settings !== undefined && this._job_config.custom_settings[setting] !== undefined) {
        return this._job_config.custom_settings[setting];
      }

      return null;
    }
  }, {
    key: "_overrideConfigs",
    value: function _overrideConfigs(job, datafeed) {
      var _this2 = this;

      this._job_config = job;
      this._datafeed_config = datafeed;
      this._detectors = this._job_config.analysis_config.detectors;
      this._influencers = this._job_config.analysis_config.influencers;

      if (this._job_config.groups === undefined) {
        this._job_config.groups = [];
      }

      if (this._job_config.analysis_config.influencers !== undefined) {
        this._job_config.analysis_config.influencers.forEach(function (i) {
          return _this2.addInfluencer(i);
        });
      }

      if (this._job_config.results_index_name !== undefined && this._job_config.results_index_name !== _constants.SHARED_RESULTS_INDEX_NAME) {
        this.useDedicatedIndex = true;
      }

      this._sparseData = (0, _general.isSparseDataJob)(job, datafeed);
    }
  }, {
    key: "type",
    get: function get() {
      return this._type;
    }
  }, {
    key: "detectors",
    get: function get() {
      return this._detectors;
    }
  }, {
    key: "aggregationsInDetectors",
    get: function get() {
      return this._aggs;
    }
  }, {
    key: "aggregations",
    get: function get() {
      return this._aggs;
    }
  }, {
    key: "fields",
    get: function get() {
      return this._fields;
    }
  }, {
    key: "bucketSpan",
    set: function set(bucketSpan) {
      this._job_config.analysis_config.bucket_span = bucketSpan;

      this._setBucketSpanMs(bucketSpan);
    },
    get: function get() {
      return this._job_config.analysis_config.bucket_span;
    }
  }, {
    key: "bucketSpanMs",
    get: function get() {
      return this._bucketSpanMs;
    }
  }, {
    key: "influencers",
    get: function get() {
      return this._influencers;
    }
  }, {
    key: "jobId",
    set: function set(jobId) {
      this._job_config.job_id = jobId;
      this._datafeed_config.job_id = jobId;
      this._datafeed_config.datafeed_id = "datafeed-".concat(jobId);

      if (this._useDedicatedIndex) {
        this._job_config.results_index_name = jobId;
      }
    },
    get: function get() {
      return this._job_config.job_id;
    }
  }, {
    key: "datafeedId",
    get: function get() {
      return this._datafeed_config.datafeed_id;
    }
  }, {
    key: "description",
    set: function set(description) {
      this._job_config.description = description;
    },
    get: function get() {
      return this._job_config.description;
    }
  }, {
    key: "groups",
    get: function get() {
      return this._job_config.groups;
    },
    set: function set(groups) {
      this._job_config.groups = groups;
    }
  }, {
    key: "calendars",
    get: function get() {
      return this._job_config.calendars || [];
    },
    set: function set(calendars) {
      this._job_config.calendars = calendars;
    }
  }, {
    key: "modelPlot",
    set: function set(enable) {
      if (enable) {
        this._job_config.model_plot_config = {
          enabled: true
        };
      } else {
        delete this._job_config.model_plot_config;
      }
    },
    get: function get() {
      return this._job_config.model_plot_config !== undefined && this._job_config.model_plot_config.enabled === true;
    }
  }, {
    key: "useDedicatedIndex",
    set: function set(enable) {
      this._useDedicatedIndex = enable;

      if (enable) {
        this._job_config.results_index_name = this._job_config.job_id;
      } else {
        delete this._job_config.results_index_name;
      }
    },
    get: function get() {
      return this._useDedicatedIndex;
    }
  }, {
    key: "modelMemoryLimit",
    set: function set(mml) {
      if (mml !== null) {
        this._job_config.analysis_limits = {
          model_memory_limit: mml
        };
      } else {
        delete this._job_config.analysis_limits;
      }
    },
    get: function get() {
      if (this._job_config.analysis_limits && this._job_config.analysis_limits.model_memory_limit !== undefined) {
        return this._job_config.analysis_limits.model_memory_limit;
      } else {
        return null;
      }
    }
  }, {
    key: "sparseData",
    get: function get() {
      return this._sparseData;
    },
    set: function set(sparseData) {
      this._sparseData = sparseData;

      this._updateSparseDataDetectors();
    }
  }, {
    key: "start",
    get: function get() {
      return this._start;
    }
  }, {
    key: "end",
    get: function get() {
      return this._end;
    }
  }, {
    key: "query",
    get: function get() {
      return this._datafeed_config.query;
    },
    set: function set(query) {
      this._datafeed_config.query = query;
    }
  }, {
    key: "subscribers",
    get: function get() {
      return this._subscribers;
    }
  }, {
    key: "jobConfig",
    get: function get() {
      return this._job_config;
    }
  }, {
    key: "datafeedConfig",
    get: function get() {
      return this._datafeed_config;
    }
  }, {
    key: "stopAllRefreshPolls",
    get: function get() {
      return this._stopAllRefreshPolls;
    }
  }, {
    key: "createdBy",
    set: function set(createdBy) {
      this._setCustomSetting('created_by', createdBy);
    },
    get: function get() {
      return this._getCustomSetting('created_by');
    }
  }, {
    key: "formattedJobJson",
    get: function get() {
      return JSON.stringify(this._job_config, null, 2);
    }
  }, {
    key: "formattedDatafeedJson",
    get: function get() {
      return JSON.stringify(this._datafeed_config, null, 2);
    }
  }]);

  return JobCreator;
}();

exports.JobCreator = JobCreator;