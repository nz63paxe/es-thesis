"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SummaryStep = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _notify = require("ui/notify");

var _wizard_nav = require("../wizard_nav");

var _step_types = require("../step_types");

var _job_creator_context = require("../job_creator_context");

var _job_service = require("../../../../../services/job_service");

var _json_flyout = require("./json_flyout");

var _job_creator = require("../../../common/job_creator");

var _job_details = require("./job_details");

var _detector_chart = require("./detector_chart");

var _job_progress = require("./components/job_progress");

var _post_save_options = require("./components/post_save_options");

var _general = require("../../../common/job_creator/util/general");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var SummaryStep = function SummaryStep(_ref) {
  var setCurrentStep = _ref.setCurrentStep,
      isCurrentStep = _ref.isCurrentStep;

  var _useContext = (0, _react.useContext)(_job_creator_context.JobCreatorContext),
      jobCreator = _useContext.jobCreator,
      jobValidator = _useContext.jobValidator,
      jobValidatorUpdated = _useContext.jobValidatorUpdated,
      resultsLoader = _useContext.resultsLoader;

  var _useState = (0, _react.useState)(resultsLoader.progress),
      _useState2 = _slicedToArray(_useState, 2),
      progress = _useState2[0],
      setProgress = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      showJsonFlyout = _useState4[0],
      setShowJsonFlyout = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      creatingJob = _useState6[0],
      setCreatingJob = _useState6[1];

  var _useState7 = (0, _react.useState)(jobValidator.validationSummary.basic),
      _useState8 = _slicedToArray(_useState7, 2),
      isValid = _useState8[0],
      setIsValid = _useState8[1];

  var _useState9 = (0, _react.useState)(null),
      _useState10 = _slicedToArray(_useState9, 2),
      jobRunner = _useState10[0],
      setJobRunner = _useState10[1];

  (0, _react.useEffect)(function () {
    jobCreator.subscribeToProgress(setProgress);
  }, []);

  function start() {
    return _start.apply(this, arguments);
  }

  function _start() {
    _start = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var jr;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              setShowJsonFlyout(false);
              setCreatingJob(true);
              _context.prev = 2;
              _context.next = 5;
              return jobCreator.createAndStartJob();

            case 5:
              jr = _context.sent;
              setJobRunner(jr);
              _context.next = 13;
              break;

            case 9:
              _context.prev = 9;
              _context.t0 = _context["catch"](2);

              // catch and display all job creation errors
              _notify.toastNotifications.addDanger({
                title: _i18n.i18n.translate('xpack.ml.newJob.wizard.summaryStep.createJobError', {
                  defaultMessage: "Job creation error"
                }),
                text: _context.t0.message
              });

              setCreatingJob(false);

            case 13:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, null, [[2, 9]]);
    }));
    return _start.apply(this, arguments);
  }

  function viewResults() {
    var url = _job_service.mlJobService.createResultsUrl([jobCreator.jobId], jobCreator.start, jobCreator.end, (0, _job_creator.isSingleMetricJobCreator)(jobCreator) === true ? 'timeseriesexplorer' : 'explorer');

    window.open(url, '_blank');
  }

  function toggleJsonFlyout() {
    setShowJsonFlyout(!showJsonFlyout);
  }

  function clickResetJob() {
    (0, _general.resetJob)(jobCreator);
  }

  var convertToAdvanced = function convertToAdvanced() {
    (0, _general.convertToAdvancedJob)(jobCreator);
  };

  (0, _react.useEffect)(function () {
    setIsValid(jobValidator.validationSummary.basic);
  }, [jobValidatorUpdated]);
  return _react.default.createElement(_react.Fragment, null, isCurrentStep && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_detector_chart.DetectorChart, null), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_job_progress.JobProgress, {
    progress: progress
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_job_details.JobDetails, null), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_eui.EuiFlexGroup, null, progress < 100 && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_wizard_nav.PreviousButton, {
    previous: function previous() {
      return setCurrentStep(_step_types.WIZARD_STEPS.VALIDATION);
    },
    previousActive: creatingJob === false && isValid === true
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    onClick: start,
    isDisabled: creatingJob === true || isValid === false,
    "data-test-subj": "mlJobWizardButtonCreateJob",
    fill: true
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.createJobButton",
    defaultMessage: "Create job"
  })))), creatingJob === false && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    onClick: toggleJsonFlyout,
    isDisabled: progress > 0,
    "data-test-subj": "mlJobWizardButtonPreviewJobJson"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.previewJsonButton",
    defaultMessage: "Preview job JSON"
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    onClick: convertToAdvanced
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.convertToAdvancedButton",
    defaultMessage: "Convert to advanced job"
  })))), progress > 0 && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    onClick: viewResults,
    "data-test-subj": "mlJobWizardButtonViewResults"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.viewResultsButton",
    defaultMessage: "View results"
  })))), progress === 100 && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    onClick: clickResetJob,
    "data-test-subj": "mlJobWizardButtonResetJob"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.newJob.wizard.summaryStep.resetJobButton",
    defaultMessage: "Reset job"
  }))), _react.default.createElement(_post_save_options.PostSaveOptions, {
    jobRunner: jobRunner
  }))), showJsonFlyout && _react.default.createElement(_json_flyout.JsonFlyout, {
    closeFlyout: function closeFlyout() {
      return setShowJsonFlyout(false);
    },
    jobCreator: jobCreator
  })));
};

exports.SummaryStep = SummaryStep;