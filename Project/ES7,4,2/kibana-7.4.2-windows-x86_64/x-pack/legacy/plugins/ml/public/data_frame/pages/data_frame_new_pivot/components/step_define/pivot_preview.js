"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PivotPreview = void 0;

var _react = _interopRequireWildcard(require("react"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _in_memory_table = require("../../../../../../common/types/eui/in_memory_table");

var _common = require("../../../../../../common/types/common");

var _field_types = require("../../../../../../common/constants/field_types");

var _date_utils = require("../../../../../util/date_utils");

var _kibana = require("../../../../../contexts/kibana");

var _common2 = require("../../../../common");

var _common3 = require("./common");

var _use_pivot_preview_data = require("./use_pivot_preview_data");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function sortColumns(groupByArr) {
  return function (a, b) {
    // make sure groupBy fields are always most left columns
    if (groupByArr.some(function (d) {
      return d.aggName === a;
    }) && groupByArr.some(function (d) {
      return d.aggName === b;
    })) {
      return a.localeCompare(b);
    }

    if (groupByArr.some(function (d) {
      return d.aggName === a;
    })) {
      return -1;
    }

    if (groupByArr.some(function (d) {
      return d.aggName === b;
    })) {
      return 1;
    }

    return a.localeCompare(b);
  };
}

function usePrevious(value) {
  var ref = (0, _react.useRef)(null);
  (0, _react.useEffect)(function () {
    ref.current = value;
  });
  return ref.current;
}

var PreviewTitle = function PreviewTitle(_ref) {
  var previewRequest = _ref.previewRequest;

  var euiCopyText = _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.copyClipboardTooltip', {
    defaultMessage: 'Copy Dev Console statement of the pivot preview to the clipboard.'
  });

  return _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("span", null, _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.dataFramePivotPreviewTitle', {
    defaultMessage: 'Data frame pivot preview'
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiCopy, {
    beforeMessage: euiCopyText,
    textToCopy: (0, _common3.getPivotPreviewDevConsoleStatement)(previewRequest)
  }, function (copy) {
    return _react.default.createElement(_eui.EuiButtonIcon, {
      onClick: copy,
      iconType: "copyClipboard",
      "aria-label": euiCopyText
    });
  })));
};

var ErrorMessage = function ErrorMessage(_ref2) {
  var message = _ref2.message;
  var error = JSON.parse(message);

  var statusCodeLabel = _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.statusCodeLabel', {
    defaultMessage: 'Status code'
  });

  return _react.default.createElement(_eui.EuiText, {
    size: "xs"
  }, _react.default.createElement("pre", null, error.message && error.statusCode && "".concat(statusCodeLabel, ": ").concat(error.statusCode, "\n").concat(error.message) || message));
};

var PivotPreview = _react.default.memo(function (_ref3) {
  var aggs = _ref3.aggs,
      groupBy = _ref3.groupBy,
      query = _ref3.query;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      clearTable = _useState2[0],
      setClearTable = _useState2[1];

  var indexPattern = (0, _kibana.useCurrentIndexPattern)();

  var _usePivotPreviewData = (0, _use_pivot_preview_data.usePivotPreviewData)(indexPattern, query, aggs, groupBy),
      dataFramePreviewData = _usePivotPreviewData.dataFramePreviewData,
      dataFramePreviewMappings = _usePivotPreviewData.dataFramePreviewMappings,
      errorMessage = _usePivotPreviewData.errorMessage,
      previewRequest = _usePivotPreviewData.previewRequest,
      status = _usePivotPreviewData.status;

  var groupByArr = (0, _common.dictionaryToArray)(groupBy); // EuiInMemoryTable has an issue with dynamic sortable columns
  // and will trigger a full page Kibana error in such a case.
  // The following is a workaround until this is solved upstream:
  // - If the sortable/columns config changes,
  //   the table will be unmounted/not rendered.
  //   This is what the useEffect() part does.
  // - After that the table gets re-enabled. To make sure React
  //   doesn't consolidate the state updates, setTimeout is used.

  var firstColumnName = dataFramePreviewData.length > 0 ? Object.keys(dataFramePreviewData[0]).sort(sortColumns(groupByArr))[0] : undefined;
  var firstColumnNameChanged = usePrevious(firstColumnName) !== firstColumnName;
  (0, _react.useEffect)(function () {
    if (firstColumnNameChanged) {
      setClearTable(true);
    }

    if (clearTable) {
      setTimeout(function () {
        return setClearTable(false);
      }, 0);
    }
  });

  if (firstColumnNameChanged) {
    return null;
  }

  if (status === _use_pivot_preview_data.PIVOT_PREVIEW_STATUS.ERROR) {
    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(PreviewTitle, {
      previewRequest: previewRequest
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.dataFramePivotPreviewError', {
        defaultMessage: 'An error occurred loading the pivot preview.'
      }),
      color: "danger",
      iconType: "cross"
    }, _react.default.createElement(ErrorMessage, {
      message: errorMessage
    })));
  }

  if (dataFramePreviewData.length === 0) {
    var noDataMessage = _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.dataFramePivotPreviewNoDataCalloutBody', {
      defaultMessage: 'The preview request did not return any data. Please ensure the optional query returns data and that values exist for the field used by group-by and aggregation fields.'
    });

    var aggsArr = (0, _common.dictionaryToArray)(aggs);

    if (aggsArr.length === 0 || groupByArr.length === 0) {
      noDataMessage = _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.dataFramePivotPreviewIncompleteConfigCalloutBody', {
        defaultMessage: 'Please choose at least one group-by field and aggregation.'
      });
    }

    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(PreviewTitle, {
      previewRequest: previewRequest
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.pivotPreview.dataFramePivotPreviewNoDataCalloutTitle', {
        defaultMessage: 'Pivot preview not available'
      }),
      color: "primary"
    }, _react.default.createElement("p", null, noDataMessage)));
  }

  var columnKeys = (0, _common2.getFlattenedFields)(dataFramePreviewData[0]);
  columnKeys.sort(sortColumns(groupByArr));
  var columns = columnKeys.map(function (k) {
    var column = {
      field: k,
      name: k,
      sortable: true,
      truncateText: true
    };

    if (typeof dataFramePreviewMappings.properties[k] !== 'undefined') {
      var esFieldType = dataFramePreviewMappings.properties[k].type;

      switch (esFieldType) {
        case _field_types.ES_FIELD_TYPES.BOOLEAN:
          column.dataType = 'boolean';
          break;

        case _field_types.ES_FIELD_TYPES.DATE:
          column.align = 'right';

          column.render = function (d) {
            return (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(d).unix() * 1000);
          };

          break;

        case _field_types.ES_FIELD_TYPES.BYTE:
        case _field_types.ES_FIELD_TYPES.DOUBLE:
        case _field_types.ES_FIELD_TYPES.FLOAT:
        case _field_types.ES_FIELD_TYPES.HALF_FLOAT:
        case _field_types.ES_FIELD_TYPES.INTEGER:
        case _field_types.ES_FIELD_TYPES.LONG:
        case _field_types.ES_FIELD_TYPES.SCALED_FLOAT:
        case _field_types.ES_FIELD_TYPES.SHORT:
          column.dataType = 'number';
          break;

        case _field_types.ES_FIELD_TYPES.KEYWORD:
        case _field_types.ES_FIELD_TYPES.TEXT:
          column.dataType = 'string';
          break;
      }
    }

    return column;
  });

  if (columns.length === 0) {
    return null;
  }

  var sorting = {
    sort: {
      field: columns[0].field,
      direction: _in_memory_table.SORT_DIRECTION.ASC
    }
  };
  return _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(PreviewTitle, {
    previewRequest: previewRequest
  }), status === _use_pivot_preview_data.PIVOT_PREVIEW_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent"
  }), status !== _use_pivot_preview_data.PIVOT_PREVIEW_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent",
    max: 1,
    value: 0
  }), dataFramePreviewData.length > 0 && clearTable === false && columns.length > 0 && _react.default.createElement(_in_memory_table.MlInMemoryTable, {
    allowNeutralSort: false,
    compressed: true,
    items: dataFramePreviewData,
    columns: columns,
    pagination: {
      initialPageSize: 5,
      pageSizeOptions: [5, 10, 25]
    },
    sorting: sorting
  }));
});

exports.PivotPreview = PivotPreview;