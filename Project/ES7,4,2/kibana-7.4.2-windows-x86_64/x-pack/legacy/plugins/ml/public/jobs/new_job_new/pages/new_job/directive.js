"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _modules = require("ui/modules");

var _timefilter = require("ui/timefilter");

var _i18n = require("ui/i18n");

var _new_job_utils = require("../../../new_job/utils/new_job_utils");

var _page = require("./page");

var _kibana = require("../../../../contexts/kibana");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _module = _modules.uiModules.get('apps/ml', ['react']);

_module.directive('mlNewJobPage', function ($injector) {
  return {
    scope: {},
    restrict: 'E',
    link: function () {
      var _link = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(scope, element) {
        var indexPatterns, kbnBaseUrl, kibanaConfig, Private, $route, existingJobsAndGroups, jobType, createSearchItems, _createSearchItems, indexPattern, savedSearch, combinedQuery, kibanaContext, props;

        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _timefilter.timefilter.disableTimeRangeSelector();

                _timefilter.timefilter.disableAutoRefreshSelector();

                indexPatterns = $injector.get('indexPatterns');
                kbnBaseUrl = $injector.get('kbnBaseUrl');
                kibanaConfig = $injector.get('config');
                Private = $injector.get('Private');
                $route = $injector.get('$route');
                existingJobsAndGroups = $route.current.locals.existingJobsAndGroups;

                if (!($route.current.locals.jobType === undefined)) {
                  _context.next = 10;
                  break;
                }

                return _context.abrupt("return");

              case 10:
                jobType = $route.current.locals.jobType;
                createSearchItems = Private(_new_job_utils.SearchItemsProvider);
                _createSearchItems = createSearchItems(), indexPattern = _createSearchItems.indexPattern, savedSearch = _createSearchItems.savedSearch, combinedQuery = _createSearchItems.combinedQuery;
                kibanaContext = {
                  combinedQuery: combinedQuery,
                  currentIndexPattern: indexPattern,
                  currentSavedSearch: savedSearch,
                  indexPatterns: indexPatterns,
                  kbnBaseUrl: kbnBaseUrl,
                  kibanaConfig: kibanaConfig
                };
                props = {
                  existingJobsAndGroups: existingJobsAndGroups,
                  jobType: jobType
                };

                _reactDom.default.render(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_kibana.KibanaContext.Provider, {
                  value: kibanaContext
                }, _react.default.createElement(_page.Page, props))), element[0]);

                element.on('$destroy', function () {
                  _reactDom.default.unmountComponentAtNode(element[0]);

                  scope.$destroy();
                });

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function link(_x, _x2) {
        return _link.apply(this, arguments);
      }

      return link;
    }()
  };
});