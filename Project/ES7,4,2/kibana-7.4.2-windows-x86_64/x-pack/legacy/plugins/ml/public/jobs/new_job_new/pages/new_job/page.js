"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Page = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _wizard = require("./wizard");

var _job_creator = require("../../common/job_creator");

var _constants = require("../../common/job_creator/util/constants");

var _chart_loader = require("../../common/chart_loader");

var _results_loader = require("../../common/results_loader");

var _job_validator = require("../../common/job_validator");

var _kibana = require("../../../../contexts/kibana");

var _full_time_range_selector = require("../../../../components/full_time_range_selector");

var _ml_time_buckets = require("../../../../util/ml_time_buckets");

var _new_job_defaults = require("../../../new_job/utils/new_job_defaults");

var _job_service = require("../../../../services/job_service");

var _configs = require("../../common/job_creator/configs");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var PAGE_WIDTH = 1200; // document.querySelector('.single-metric-job-container').width();

var BAR_TARGET = PAGE_WIDTH > 2000 ? 1000 : PAGE_WIDTH / 2;
var MAX_BARS = BAR_TARGET + BAR_TARGET / 100 * 100; // 100% larger than bar target

var Page = function Page(_ref) {
  var existingJobsAndGroups = _ref.existingJobsAndGroups,
      jobType = _ref.jobType;
  var kibanaContext = (0, _kibana.useKibanaContext)();
  var jobDefaults = (0, _new_job_defaults.newJobDefaults)();
  var jobCreator = (0, _job_creator.jobCreatorFactory)(jobType)(kibanaContext.currentIndexPattern, kibanaContext.currentSavedSearch, kibanaContext.combinedQuery);

  var _getTimeFilterRange = (0, _full_time_range_selector.getTimeFilterRange)(),
      from = _getTimeFilterRange.from,
      to = _getTimeFilterRange.to;

  jobCreator.setTimeRange(from, to);
  var skipTimeRangeStep = false;

  if (_job_service.mlJobService.tempJobCloningObjects.job !== undefined) {
    var clonedJob = _job_service.mlJobService.cloneJob(_job_service.mlJobService.tempJobCloningObjects.job);

    var _expandCombinedJobCon = (0, _configs.expandCombinedJobConfig)(clonedJob),
        job = _expandCombinedJobCon.job,
        datafeed = _expandCombinedJobCon.datafeed;

    jobCreator.cloneFromExistingJob(job, datafeed);
    skipTimeRangeStep = _job_service.mlJobService.tempJobCloningObjects.skipTimeRangeStep; // if we're not skipping the time range, this is a standard job clone, so wipe the jobId

    if (skipTimeRangeStep === false) {
      jobCreator.jobId = '';
    }

    _job_service.mlJobService.tempJobCloningObjects.skipTimeRangeStep = false;
    _job_service.mlJobService.tempJobCloningObjects.job = undefined;

    if (_job_service.mlJobService.tempJobCloningObjects.start !== undefined && _job_service.mlJobService.tempJobCloningObjects.end !== undefined) {
      // auto select the start and end dates for the time range picker
      jobCreator.setTimeRange(_job_service.mlJobService.tempJobCloningObjects.start, _job_service.mlJobService.tempJobCloningObjects.end);
      _job_service.mlJobService.tempJobCloningObjects.start = undefined;
      _job_service.mlJobService.tempJobCloningObjects.end = undefined;
    }
  } else {
    jobCreator.bucketSpan = _constants.DEFAULT_BUCKET_SPAN;

    if ((0, _job_creator.isPopulationJobCreator)(jobCreator) === true) {
      // for population jobs use the default mml (1GB)
      jobCreator.modelMemoryLimit = jobDefaults.anomaly_detectors.model_memory_limit;
    } else {
      // for all other jobs, use 10MB
      jobCreator.modelMemoryLimit = _constants.DEFAULT_MODEL_MEMORY_LIMIT;
    }

    if ((0, _job_creator.isSingleMetricJobCreator)(jobCreator) === true) {
      jobCreator.modelPlot = true;
    }

    if (kibanaContext.currentSavedSearch.id !== undefined) {
      // Jobs created from saved searches cannot be cloned in the wizard as the
      // ML job config holds no reference to the saved search ID.
      jobCreator.createdBy = null;
    }
  }

  var chartInterval = new _ml_time_buckets.MlTimeBuckets();
  chartInterval.setBarTarget(BAR_TARGET);
  chartInterval.setMaxBars(MAX_BARS);
  chartInterval.setInterval('auto');
  var chartLoader = new _chart_loader.ChartLoader(kibanaContext.currentIndexPattern, kibanaContext.combinedQuery);
  var jobValidator = new _job_validator.JobValidator(jobCreator, existingJobsAndGroups);
  var resultsLoader = new _results_loader.ResultsLoader(jobCreator, chartInterval, chartLoader);
  (0, _react.useEffect)(function () {
    return function () {
      jobCreator.forceStopRefreshPolls();
    };
  });
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiPage, {
    style: {
      backgroundColor: 'inherit'
    },
    "data-test-subj": "mlPageJobWizard"
  }, _react.default.createElement(_eui.EuiPageBody, null, _react.default.createElement(_eui.EuiPageContentBody, null, _react.default.createElement(_wizard.Wizard, {
    jobCreator: jobCreator,
    chartLoader: chartLoader,
    resultsLoader: resultsLoader,
    chartInterval: chartInterval,
    jobValidator: jobValidator,
    existingJobsAndGroups: existingJobsAndGroups,
    skipTimeRangeStep: skipTimeRangeStep
  })))));
};

exports.Page = Page;