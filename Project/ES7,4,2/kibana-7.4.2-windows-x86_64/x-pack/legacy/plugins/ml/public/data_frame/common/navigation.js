"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.moveToDataFrameWizard = moveToDataFrameWizard;
exports.getDiscoverUrl = getDiscoverUrl;

var _risonNode = _interopRequireDefault(require("rison-node"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function moveToDataFrameWizard() {
  window.location.href = '#/data_frames/new_transform';
}
/**
 * Gets a url for navigating to Discover page.
 * @param indexPatternId Index pattern id.
 * @param baseUrl Base url.
 */


function getDiscoverUrl(indexPatternId, baseUrl) {
  var _g = _risonNode.default.encode({}); // Add the index pattern ID to the appState part of the URL.


  var _a = _risonNode.default.encode({
    index: indexPatternId
  });

  var hash = "#/discover?_g=".concat(_g, "&_a=").concat(_a);
  return "".concat(baseUrl).concat(hash);
}