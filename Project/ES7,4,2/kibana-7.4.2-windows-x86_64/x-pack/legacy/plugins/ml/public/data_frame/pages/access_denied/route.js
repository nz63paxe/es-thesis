"use strict";

var _routes = _interopRequireDefault(require("ui/routes"));

var _breadcrumbs = require("../../breadcrumbs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var template = "<ml-data-frame-access-denied />";

_routes.default.when('/data_frames/access-denied', {
  template: template,
  k7Breadcrumbs: _breadcrumbs.getDataFrameBreadcrumbs
});