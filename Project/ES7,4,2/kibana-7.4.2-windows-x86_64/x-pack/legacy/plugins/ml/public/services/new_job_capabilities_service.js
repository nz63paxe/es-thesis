"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadNewJobCapabilities = loadNewJobCapabilities;
exports.newJobCapsService = void 0;

var _fields = require("../../common/types/fields");

var _field_types = require("../../common/constants/field_types");

var _aggregation_types = require("../../common/constants/aggregation_types");

var _ml_api_service = require("./ml_api_service");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// called in the angular routing resolve block to initialize the
// newJobCapsService with the currently selected index pattern
function loadNewJobCapabilities(indexPatterns, savedSearches, $route) {
  return new Promise(
  /*#__PURE__*/
  function () {
    var _ref = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee(resolve, reject) {
      var _$route$current$param, indexPatternId, savedSearchId, indexPattern, savedSearch, _indexPattern;

      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // get the index pattern id or saved search id from the url params
              _$route$current$param = $route.current.params, indexPatternId = _$route$current$param.index, savedSearchId = _$route$current$param.savedSearchId;

              if (!(indexPatternId !== undefined)) {
                _context.next = 10;
                break;
              }

              _context.next = 4;
              return indexPatterns.get(indexPatternId);

            case 4:
              indexPattern = _context.sent;
              _context.next = 7;
              return newJobCapsService.initializeFromIndexPattern(indexPattern);

            case 7:
              resolve(newJobCapsService.newJobCaps);
              _context.next = 21;
              break;

            case 10:
              if (!(savedSearchId !== undefined)) {
                _context.next = 20;
                break;
              }

              _context.next = 13;
              return savedSearches.get(savedSearchId);

            case 13:
              savedSearch = _context.sent;
              _indexPattern = savedSearch.searchSource.getField('index');
              _context.next = 17;
              return newJobCapsService.initializeFromIndexPattern(_indexPattern);

            case 17:
              resolve(newJobCapsService.newJobCaps);
              _context.next = 21;
              break;

            case 20:
              reject();

            case 21:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x, _x2) {
      return _ref.apply(this, arguments);
    };
  }());
}

var categoryFieldTypes = [_field_types.ES_FIELD_TYPES.TEXT, _field_types.ES_FIELD_TYPES.KEYWORD, _field_types.ES_FIELD_TYPES.IP];

var NewJobCapsService =
/*#__PURE__*/
function () {
  function NewJobCapsService() {
    var includeCountAgg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;

    _classCallCheck(this, NewJobCapsService);

    _defineProperty(this, "_fields", void 0);

    _defineProperty(this, "_aggs", void 0);

    _defineProperty(this, "_includeCountAgg", void 0);

    this._fields = [];
    this._aggs = [];
    this._includeCountAgg = includeCountAgg;
  }

  _createClass(NewJobCapsService, [{
    key: "initializeFromIndexPattern",
    value: function () {
      var _initializeFromIndexPattern = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(indexPattern) {
        var resp, _createObjects, fields, aggs, _createCountFieldAndA, countField, countAggs;

        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _ml_api_service.ml.jobs.newJobCaps(indexPattern.title, indexPattern.type === 'rollup');

              case 3:
                resp = _context2.sent;
                _createObjects = createObjects(resp, indexPattern.title), fields = _createObjects.fields, aggs = _createObjects.aggs;

                if (this._includeCountAgg === true) {
                  _createCountFieldAndA = createCountFieldAndAggs(), countField = _createCountFieldAndA.countField, countAggs = _createCountFieldAndA.countAggs;
                  fields.splice(0, 0, countField);
                  aggs.push.apply(aggs, _toConsumableArray(countAggs));
                }

                this._fields = fields;
                this._aggs = aggs;
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.error('Unable to load new job capabilities', _context2.t0); // eslint-disable-line no-console

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 10]]);
      }));

      function initializeFromIndexPattern(_x3) {
        return _initializeFromIndexPattern.apply(this, arguments);
      }

      return initializeFromIndexPattern;
    }()
  }, {
    key: "getFieldById",
    value: function getFieldById(id) {
      var field = this._fields.find(function (f) {
        return f.id === id;
      });

      return field === undefined ? null : field;
    }
  }, {
    key: "getAggById",
    value: function getAggById(id) {
      var agg = this._aggs.find(function (f) {
        return f.id === id;
      });

      return agg === undefined ? null : agg;
    }
  }, {
    key: "fields",
    get: function get() {
      return this._fields;
    }
  }, {
    key: "aggs",
    get: function get() {
      return this._aggs;
    }
  }, {
    key: "newJobCaps",
    get: function get() {
      return {
        fields: this._fields,
        aggs: this._aggs
      };
    }
  }, {
    key: "categoryFields",
    get: function get() {
      return this._fields.filter(function (f) {
        return categoryFieldTypes.includes(f.type);
      });
    }
  }]);

  return NewJobCapsService;
}(); // using the response from the endpoint, create the field and aggs objects
// when transported over the endpoint, the fields and aggs contain lists of ids of the
// fields and aggs they are related to.
// this function creates lists of real Fields and Aggregations and cross references them.
// the list if ids are then deleted.


function createObjects(resp, indexPatternTitle) {
  var results = resp[indexPatternTitle];
  var fields = [];
  var aggs = []; // for speed, a map of aggregations, keyed on their id
  // create a AggMap type to allow an enum (AggId) to be used as a Record key and then initialized with {}

  var aggMap = {}; // for speed, a map of aggregation id lists from a field, keyed on the field id

  var aggIdMap = {};

  if (results !== undefined) {
    results.aggs.forEach(function (a) {
      // copy the agg and add a Fields list
      var agg = _objectSpread({}, a, {
        fields: []
      });

      aggMap[agg.id] = agg;
      aggs.push(agg);
    });
    results.fields.forEach(function (f) {
      // copy the field and add an Aggregations list
      var field = _objectSpread({}, f, {
        aggs: []
      });

      if (field.aggIds !== undefined) {
        aggIdMap[field.id] = field.aggIds;
      }

      fields.push(field);
    }); // loop through the fields and populate their aggs lists.
    // for each agg added to a field, also add that field to the agg's field list

    fields.forEach(function (field) {
      aggIdMap[field.id].forEach(function (aggId) {
        mix(field, aggMap[aggId]);
      });
    });
  } // the aggIds and fieldIds lists are no longer needed as we've created
  // lists of real fields and aggs


  fields.forEach(function (f) {
    return delete f.aggIds;
  });
  aggs.forEach(function (a) {
    return delete a.fieldIds;
  });
  return {
    fields: fields,
    aggs: aggs
  };
}

function mix(field, agg) {
  if (agg.fields === undefined) {
    agg.fields = [];
  }

  if (field.aggs === undefined) {
    field.aggs = [];
  }

  agg.fields.push(field);
  field.aggs.push(agg);
}

function createCountFieldAndAggs() {
  var countField = {
    id: _fields.EVENT_RATE_FIELD_ID,
    name: 'Event rate',
    type: _field_types.ES_FIELD_TYPES.INTEGER,
    aggregatable: true,
    aggs: []
  };
  var countAggs = [{
    id: _aggregation_types.ML_JOB_AGGREGATION.COUNT,
    title: 'Count',
    kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
    dslName: _aggregation_types.ES_AGGREGATION.COUNT,
    type: 'metrics',
    mlModelPlotAgg: {
      min: 'min',
      max: 'max'
    },
    fields: [countField]
  }, {
    id: _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT,
    title: 'High count',
    kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
    dslName: _aggregation_types.ES_AGGREGATION.COUNT,
    type: 'metrics',
    mlModelPlotAgg: {
      min: 'min',
      max: 'max'
    },
    fields: [countField]
  }, {
    id: _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT,
    title: 'Low count',
    kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
    dslName: _aggregation_types.ES_AGGREGATION.COUNT,
    type: 'metrics',
    mlModelPlotAgg: {
      min: 'min',
      max: 'max'
    },
    fields: [countField]
  }];

  if (countField.aggs !== undefined) {
    var _countField$aggs;

    (_countField$aggs = countField.aggs).push.apply(_countField$aggs, countAggs);
  }

  return {
    countField: countField,
    countAggs: countAggs
  };
}

var newJobCapsService = new NewJobCapsService();
exports.newJobCapsService = newJobCapsService;