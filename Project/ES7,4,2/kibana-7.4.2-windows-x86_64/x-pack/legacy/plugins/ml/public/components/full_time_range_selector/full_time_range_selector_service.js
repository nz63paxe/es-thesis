"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setFullTimeRange = setFullTimeRange;
exports.getTimeFilterRange = getTimeFilterRange;

var _moment = _interopRequireDefault(require("moment"));

var _i18n = require("@kbn/i18n");

var _notify = require("ui/notify");

var _timefilter = require("ui/timefilter");

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _ml_api_service = require("../../services/ml_api_service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function setFullTimeRange(_x, _x2) {
  return _setFullTimeRange.apply(this, arguments);
}

function _setFullTimeRange() {
  _setFullTimeRange = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(indexPattern, query) {
    var resp;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _ml_api_service.ml.getTimeFieldRange({
              index: indexPattern.title,
              timeFieldName: indexPattern.timeFieldName,
              query: query
            });

          case 3:
            resp = _context.sent;

            _timefilter.timefilter.setTime({
              from: (0, _moment.default)(resp.start.epoch).toISOString(),
              to: (0, _moment.default)(resp.end.epoch).toISOString()
            });

            return _context.abrupt("return", resp);

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](0);

            _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.fullTimeRangeSelector.errorSettingTimeRangeNotification', {
              defaultMessage: 'An error occurred setting the time range.'
            }));

            return _context.abrupt("return", _context.t0);

          case 12:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[0, 8]]);
  }));
  return _setFullTimeRange.apply(this, arguments);
}

function getTimeFilterRange() {
  var from = 0;
  var to = 0;

  var fromString = _timefilter.timefilter.getTime().from;

  var toString = _timefilter.timefilter.getTime().to;

  if (typeof fromString === 'string' && typeof toString === 'string') {
    var fromMoment = _datemath.default.parse(fromString);

    var toMoment = _datemath.default.parse(toString);

    if (typeof fromMoment !== 'undefined' && typeof toMoment !== 'undefined') {
      var fromMs = fromMoment.valueOf();
      var toMs = toMoment.valueOf();
      from = fromMs;
      to = toMs;
    }
  }

  return {
    to: to,
    from: from
  };
}