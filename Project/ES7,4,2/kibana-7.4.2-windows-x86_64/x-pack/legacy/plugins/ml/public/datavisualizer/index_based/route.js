"use strict";

var _routes = _interopRequireDefault(require("ui/routes"));

var _check_license = require("../../license/check_license");

var _check_privilege = require("../../privilege/check_privilege");

var _index_utils = require("../../util/index_utils");

var _check_ml_nodes = require("../../ml_nodes_check/check_ml_nodes");

var _breadcrumbs = require("./breadcrumbs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore
var template = "<ml-nav-menu name=\"datavisualizer\" /><ml-data-visualizer />";

_routes.default.when('/jobs/new_job/datavisualizer', {
  template: template,
  k7Breadcrumbs: _breadcrumbs.getDataVisualizerBreadcrumbs,
  resolve: {
    CheckLicense: _check_license.checkBasicLicense,
    privileges: _check_privilege.checkGetJobsPrivilege,
    indexPattern: _index_utils.loadCurrentIndexPattern,
    savedSearch: _index_utils.loadCurrentSavedSearch,
    checkMlNodesAvailable: _check_ml_nodes.checkMlNodesAvailable
  }
});