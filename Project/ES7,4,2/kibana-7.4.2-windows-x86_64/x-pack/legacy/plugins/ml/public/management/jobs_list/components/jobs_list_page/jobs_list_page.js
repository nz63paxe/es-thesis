"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.JobsListPage = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _i18n2 = require("ui/i18n");

var _eui = require("@elastic/eui");

var _metadata = require("ui/metadata");

var _jobs_list_view = require("../../../../jobs/jobs_list/components/jobs_list_view");

var _analytics_list = require("../../../../data_frame_analytics/pages/analytics_management/components/analytics_list");

var _refresh_analytics_list_button = require("../../../../data_frame_analytics/pages/analytics_management/components/refresh_analytics_list_button");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function getTabs(isMlEnabledInSpace) {
  return [{
    id: 'anomaly_detection_jobs',
    name: _i18n.i18n.translate('xpack.ml.management.jobsList.anomalyDetectionTab', {
      defaultMessage: 'Anomaly detection'
    }),
    content: _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_jobs_list_view.JobsListView, {
      isManagementTable: true,
      isMlEnabledInSpace: isMlEnabledInSpace
    }))
  }, {
    id: 'analytics_jobs',
    name: _i18n.i18n.translate('xpack.ml.management.jobsList.analyticsTab', {
      defaultMessage: 'Analytics'
    }),
    content: _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement("span", {
      className: "mlKibanaManagement__analyticsRefreshButton"
    }, _react.default.createElement(_refresh_analytics_list_button.RefreshAnalyticsListButton, null)), _react.default.createElement(_eui.EuiSpacer, {
      size: "s",
      className: "mlKibanaManagement__analyticsSpacer"
    }), _react.default.createElement(_analytics_list.DataFrameAnalyticsList, {
      isManagementTable: true
    }))
  }];
}

var JobsListPage = function JobsListPage(_ref) {
  var isMlEnabledInSpace = _ref.isMlEnabledInSpace;
  var tabs = getTabs(isMlEnabledInSpace);

  var _useState = (0, _react.useState)(tabs[0].id),
      _useState2 = _slicedToArray(_useState, 2),
      currentTabId = _useState2[0],
      setCurrentTabId = _useState2[1]; // metadata.branch corresponds to the version used in documentation links.


  var anomalyDetectionJobsUrl = "https://www.elastic.co/guide/en/elastic-stack-overview/".concat(_metadata.metadata.branch, "/ml-jobs.html");
  var anomalyJobsUrl = "https://www.elastic.co/guide/en/elastic-stack-overview/".concat(_metadata.metadata.branch, "/ml-dfanalytics.html");

  var anomalyDetectionDocsLabel = _i18n.i18n.translate('xpack.ml.management.jobsList.anomalyDetectionDocsLabel', {
    defaultMessage: 'Anomaly detection jobs docs'
  });

  var analyticsDocsLabel = _i18n.i18n.translate('xpack.ml.management.jobsList.analyticsDocsLabel', {
    defaultMessage: 'Analytics jobs docs'
  });

  function renderTabs() {
    return _react.default.createElement(_eui.EuiTabbedContent, {
      onTabClick: function onTabClick(_ref2) {
        var id = _ref2.id;
        setCurrentTabId(id);
      },
      size: "s",
      tabs: getTabs(isMlEnabledInSpace),
      initialSelectedTab: tabs[0]
    });
  }

  return _react.default.createElement(_i18n2.I18nContext, null, _react.default.createElement(_eui.EuiPageContent, null, _react.default.createElement(_eui.EuiTitle, {
    size: "l"
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement("h1", null, _i18n.i18n.translate('xpack.ml.management.jobsList.jobsListTitle', {
    defaultMessage: 'Machine Learning Jobs'
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    target: "_blank",
    iconType: "help",
    iconSide: "left",
    color: "primary",
    href: currentTabId === 'anomaly_detection_jobs' ? anomalyDetectionJobsUrl : anomalyJobsUrl
  }, currentTabId === 'anomaly_detection_jobs' ? anomalyDetectionDocsLabel : analyticsDocsLabel)))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiTitle, {
    size: "s"
  }, _react.default.createElement(_eui.EuiText, {
    color: "subdued"
  }, _i18n.i18n.translate('xpack.ml.management.jobsList.jobsListTagline', {
    defaultMessage: 'View machine learning analytics and anomaly detection jobs.'
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiPageContentBody, null, renderTabs())));
};

exports.JobsListPage = JobsListPage;