"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateTransformButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _check_privilege = require("../../../../../privilege/check_privilege");

var _common = require("../../../../common");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CreateTransformButton = function CreateTransformButton() {
  var disabled = !(0, _check_privilege.checkPermission)('canCreateDataFrame') || !(0, _check_privilege.checkPermission)('canPreviewDataFrame') || !(0, _check_privilege.checkPermission)('canStartStopDataFrame');

  var button = _react.default.createElement(_eui.EuiButton, {
    disabled: disabled,
    fill: true,
    onClick: _common.moveToDataFrameWizard,
    iconType: "plusInCircle",
    size: "s",
    "data-test-subj": "mlDataFramesButtonCreate"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.dataframe.transformList.createDataFrameButton",
    defaultMessage: "Create transform"
  }));

  if (disabled) {
    return _react.default.createElement(_eui.EuiToolTip, {
      position: "top",
      content: (0, _check_privilege.createPermissionFailureMessage)('canCreateDataFrame')
    }, button);
  }

  return button;
};

exports.CreateTransformButton = CreateTransformButton;