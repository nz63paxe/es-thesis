"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AdvancedSection = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _model_plot = require("./components/model_plot");

var _dedicated_index = require("./components/dedicated_index");

var _model_memory_limit = require("./components/model_memory_limit");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ButtonContent = _i18n.i18n.translate('xpack.ml.newJob.wizard.jobDetailsStep.advancedSectionButton', {
  defaultMessage: 'Advanced'
});

var AdvancedSection = function AdvancedSection(_ref) {
  var advancedExpanded = _ref.advancedExpanded,
      setAdvancedExpanded = _ref.setAdvancedExpanded;
  return _react.default.createElement(_eui.EuiAccordion, {
    id: "advanced-section",
    buttonContent: ButtonContent,
    onToggle: setAdvancedExpanded,
    initialIsOpen: advancedExpanded,
    "data-test-subj": "mlJobWizardToggleAdvancedSection"
  }, _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xl",
    style: {
      marginLeft: '0px',
      marginRight: '0px'
    },
    "data-test-subj": "mlJobWizardAdvancedSection"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_model_plot.ModelPlotSwitch, null), _react.default.createElement(_model_memory_limit.ModelMemoryLimitInput, null)), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_dedicated_index.DedicatedIndexSwitch, null))));
};

exports.AdvancedSection = AdvancedSection;