"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SHARED_RESULTS_INDEX_NAME = exports.DEFAULT_BUCKET_SPAN = exports.DEFAULT_MODEL_MEMORY_LIMIT = exports.CREATED_BY_LABEL = exports.JOB_TYPE = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var JOB_TYPE;
exports.JOB_TYPE = JOB_TYPE;

(function (JOB_TYPE) {
  JOB_TYPE["SINGLE_METRIC"] = "single_metric";
  JOB_TYPE["MULTI_METRIC"] = "multi_metric";
  JOB_TYPE["POPULATION"] = "population";
  JOB_TYPE["ADVANCED"] = "advanced";
})(JOB_TYPE || (exports.JOB_TYPE = JOB_TYPE = {}));

var CREATED_BY_LABEL;
exports.CREATED_BY_LABEL = CREATED_BY_LABEL;

(function (CREATED_BY_LABEL) {
  CREATED_BY_LABEL["SINGLE_METRIC"] = "single-metric-wizard";
  CREATED_BY_LABEL["MULTI_METRIC"] = "multi-metric-wizard";
  CREATED_BY_LABEL["POPULATION"] = "population-wizard";
})(CREATED_BY_LABEL || (exports.CREATED_BY_LABEL = CREATED_BY_LABEL = {}));

var DEFAULT_MODEL_MEMORY_LIMIT = '10MB';
exports.DEFAULT_MODEL_MEMORY_LIMIT = DEFAULT_MODEL_MEMORY_LIMIT;
var DEFAULT_BUCKET_SPAN = '15m';
exports.DEFAULT_BUCKET_SPAN = DEFAULT_BUCKET_SPAN;
var SHARED_RESULTS_INDEX_NAME = 'shared';
exports.SHARED_RESULTS_INDEX_NAME = SHARED_RESULTS_INDEX_NAME;