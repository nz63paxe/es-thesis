"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "UiContext", {
  enumerable: true,
  get: function get() {
    return _ui_context.UiContext;
  }
});

var _ui_context = require("./ui_context");