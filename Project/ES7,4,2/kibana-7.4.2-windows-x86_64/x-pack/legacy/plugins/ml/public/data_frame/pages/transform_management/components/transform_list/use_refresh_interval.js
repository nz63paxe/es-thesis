"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRefreshInterval = void 0;

var _react = require("react");

var _timefilter = require("ui/timefilter");

var _jobs_list = require("../../../../../../common/constants/jobs_list");

var _common = require("../../../../common");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useRefreshInterval = function useRefreshInterval(setBlockRefresh) {
  var _useRefreshTransformL = (0, _common.useRefreshTransformList)(),
      refresh = _useRefreshTransformL.refresh;

  (0, _react.useEffect)(function () {
    var transformRefreshInterval = null;

    var refreshIntervalSubscription = _timefilter.timefilter.getRefreshIntervalUpdate$().subscribe(setAutoRefresh);

    _timefilter.timefilter.disableTimeRangeSelector();

    _timefilter.timefilter.enableAutoRefreshSelector();

    initAutoRefresh();

    function initAutoRefresh() {
      var _timefilter$getRefres = _timefilter.timefilter.getRefreshInterval(),
          value = _timefilter$getRefres.value;

      if (value === 0) {
        // the auto refresher starts in an off state
        // so switch it on and set the interval to 30s
        _timefilter.timefilter.setRefreshInterval({
          pause: false,
          value: _jobs_list.DEFAULT_REFRESH_INTERVAL_MS
        });
      }

      setAutoRefresh();
    }

    function setAutoRefresh() {
      var _timefilter$getRefres2 = _timefilter.timefilter.getRefreshInterval(),
          value = _timefilter$getRefres2.value,
          pause = _timefilter$getRefres2.pause;

      if (pause) {
        clearRefreshInterval();
      } else {
        setRefreshInterval(value);
      }

      refresh();
    }

    function setRefreshInterval(interval) {
      clearRefreshInterval();

      if (interval >= _jobs_list.MINIMUM_REFRESH_INTERVAL_MS) {
        setBlockRefresh(false);
        var intervalId = window.setInterval(function () {
          refresh();
        }, interval);
        transformRefreshInterval = intervalId;
      }
    }

    function clearRefreshInterval() {
      setBlockRefresh(true);

      if (transformRefreshInterval !== null) {
        window.clearInterval(transformRefreshInterval);
      }
    } // useEffect cleanup


    return function () {
      refreshIntervalSubscription.unsubscribe();
      clearRefreshInterval();
    };
  }, []); // [] as comparator makes sure this only runs once
};

exports.useRefreshInterval = useRefreshInterval;