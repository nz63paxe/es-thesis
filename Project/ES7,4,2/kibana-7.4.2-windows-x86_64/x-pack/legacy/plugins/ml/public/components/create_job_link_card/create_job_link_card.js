"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateJobLinkCard = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Component for rendering a card which links to the Create Job page, displaying an
// icon, card title, description and link.
var CreateJobLinkCard = function CreateJobLinkCard(_ref) {
  var iconType = _ref.iconType,
      title = _ref.title,
      description = _ref.description,
      onClick = _ref.onClick;
  return _react.default.createElement(_eui.EuiCard, {
    layout: "horizontal",
    icon: _react.default.createElement(_eui.EuiIcon, {
      size: "xl",
      type: iconType
    }),
    title: title,
    description: description,
    onClick: onClick
  });
};

exports.CreateJobLinkCard = CreateJobLinkCard;