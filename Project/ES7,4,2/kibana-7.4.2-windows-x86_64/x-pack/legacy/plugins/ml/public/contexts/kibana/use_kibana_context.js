"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useKibanaContext = void 0;

var _react = require("react");

var _kibana_context = require("./kibana_context");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useKibanaContext = function useKibanaContext() {
  var context = (0, _react.useContext)(_kibana_context.KibanaContext);

  if (context.combinedQuery === undefined || context.currentIndexPattern === undefined || context.currentSavedSearch === undefined || context.indexPatterns === undefined || context.kbnBaseUrl === undefined || context.kibanaConfig === undefined) {
    throw new Error('required attribute is undefined');
  }

  return context;
};

exports.useKibanaContext = useKibanaContext;