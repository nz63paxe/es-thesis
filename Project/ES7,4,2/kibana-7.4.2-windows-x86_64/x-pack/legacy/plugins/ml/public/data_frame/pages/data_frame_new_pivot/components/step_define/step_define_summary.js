"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StepDefineSummary = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _kibana = require("../../../../../contexts/kibana");

var _aggregation_list = require("../aggregation_list");

var _group_by_list = require("../group_by_list");

var _pivot_preview = require("./pivot_preview");

var _common = require("../../../../common");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var defaultSearch = '*';
var emptySearch = '';

var StepDefineSummary = function StepDefineSummary(_ref) {
  var searchString = _ref.searchString,
      searchQuery = _ref.searchQuery,
      groupByList = _ref.groupByList,
      aggList = _ref.aggList;
  var kibanaContext = (0, _kibana.useKibanaContext)();
  var pivotQuery = (0, _common.getPivotQuery)(searchQuery);
  var useCodeBlock = false;
  var displaySearch; // searchString set to empty once source config editor used - display query instead

  if (searchString === emptySearch) {
    displaySearch = JSON.stringify(searchQuery, null, 2);
    useCodeBlock = true;
  } else if (searchString === defaultSearch) {
    displaySearch = emptySearch;
  } else {
    displaySearch = searchString;
  }

  return _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      minWidth: '420px'
    }
  }, _react.default.createElement(_eui.EuiForm, null, kibanaContext.currentSavedSearch.id === undefined && typeof searchString === 'string' && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.indexPatternLabel', {
      defaultMessage: 'Index pattern'
    })
  }, _react.default.createElement("span", null, kibanaContext.currentIndexPattern.title)), useCodeBlock === false && displaySearch !== emptySearch && _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.queryLabel', {
      defaultMessage: 'Query'
    })
  }, _react.default.createElement("span", null, displaySearch)), useCodeBlock === true && displaySearch !== emptySearch && _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.queryCodeBlockLabel', {
      defaultMessage: 'Query'
    })
  }, _react.default.createElement(_eui.EuiCodeBlock, {
    language: "js",
    fontSize: "s",
    paddingSize: "s",
    color: "light",
    overflowHeight: 300,
    isCopyable: true
  }, displaySearch))), kibanaContext.currentSavedSearch.id !== undefined && _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.savedSearchLabel', {
      defaultMessage: 'Saved search'
    })
  }, _react.default.createElement("span", null, kibanaContext.currentSavedSearch.title)), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.groupByLabel', {
      defaultMessage: 'Group by'
    })
  }, _react.default.createElement(_group_by_list.GroupByListSummary, {
    list: groupByList
  })), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.ml.dataframe.stepDefineSummary.aggregationsLabel', {
      defaultMessage: 'Aggregations'
    })
  }, _react.default.createElement(_aggregation_list.AggListSummary, {
    list: aggList
  })))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, null, _react.default.createElement(_pivot_preview.PivotPreview, {
    aggs: aggList,
    groupBy: groupByList,
    query: pivotQuery
  }))));
};

exports.StepDefineSummary = StepDefineSummary;