"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StopAction = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _common = require("../../../../common");

var _check_privilege = require("../../../../../privilege/check_privilege");

var _transform_service = require("../../services/transform_service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var StopAction = function StopAction(_ref) {
  var items = _ref.items,
      forceDisable = _ref.forceDisable;
  var isBulkAction = items.length > 1;
  var canStartStopDataFrame = (0, _check_privilege.checkPermission)('canStartStopDataFrame');

  var buttonStopText = _i18n.i18n.translate('xpack.ml.dataframe.transformList.stopActionName', {
    defaultMessage: 'Stop'
  }); // Disable stop action if one of the transforms is stopped already


  var stoppedTransform = items.some(function (i) {
    return i.stats.state === _common.DATA_FRAME_TRANSFORM_STATE.STOPPED;
  });
  var stoppedTransformMessage;

  if (isBulkAction === true) {
    stoppedTransformMessage = _i18n.i18n.translate('xpack.ml.dataframe.transformList.stoppedTransformBulkToolTip', {
      defaultMessage: 'One or more selected data frame transforms is already stopped.'
    });
  } else {
    stoppedTransformMessage = _i18n.i18n.translate('xpack.ml.dataframe.transformList.stoppedTransformToolTip', {
      defaultMessage: '{transformId} is already stopped.',
      values: {
        transformId: items[0] && items[0].config.id
      }
    });
  }

  var handleStop = function handleStop() {
    (0, _transform_service.stopTransforms)(items);
  };

  var stopButton = _react.default.createElement(_eui.EuiButtonEmpty, {
    size: "xs",
    color: "text",
    disabled: forceDisable === true || !canStartStopDataFrame || stoppedTransform === true,
    iconType: "stop",
    onClick: handleStop,
    "aria-label": buttonStopText
  }, buttonStopText);

  if (!canStartStopDataFrame || stoppedTransform) {
    return _react.default.createElement(_eui.EuiToolTip, {
      position: "top",
      content: !canStartStopDataFrame ? (0, _check_privilege.createPermissionFailureMessage)('canStartStopDataFrame') : stoppedTransformMessage
    }, stopButton);
  }

  return stopButton;
};

exports.StopAction = StopAction;