"use strict";

var _routes = _interopRequireDefault(require("ui/routes"));

var _check_license = require("../../../license/check_license");

var _check_privilege = require("../../../privilege/check_privilege");

var _index_utils = require("../../../util/index_utils");

var _index_or_search = _interopRequireDefault(require("../../../jobs/new_job/wizard/steps/index_or_search/index_or_search.html"));

var _breadcrumbs = require("../../breadcrumbs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var wizardTemplate = "<ml-new-data-frame />";

_routes.default.when('/data_frames/new_transform/step/pivot?', {
  template: wizardTemplate,
  k7Breadcrumbs: _breadcrumbs.getDataFrameCreateBreadcrumbs,
  resolve: {
    CheckLicense: _check_license.checkBasicLicense,
    privileges: _check_privilege.checkCreateDataFrameTransformPrivilege,
    indexPattern: _index_utils.loadCurrentIndexPattern,
    savedSearch: _index_utils.loadCurrentSavedSearch
  }
});

_routes.default.when('/data_frames/new_transform', {
  redirectTo: '/data_frames/new_transform/step/index_or_search'
});

_routes.default.when('/data_frames/new_transform/step/index_or_search', {
  template: _index_or_search.default,
  k7Breadcrumbs: _breadcrumbs.getDataFrameIndexOrSearchBreadcrumbs,
  resolve: {
    CheckLicense: _check_license.checkBasicLicense,
    privileges: _check_privilege.checkCreateDataFrameTransformPrivilege,
    indexPatterns: _index_utils.loadIndexPatterns,
    nextStepPath: function nextStepPath() {
      return '#data_frames/new_transform/step/pivot';
    }
  }
});