"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Exploration = void 0;

var _react = _interopRequireWildcard(require("react"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _i18n = require("@kbn/i18n");

var _d2 = _interopRequireDefault(require("d3"));

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _eui_theme_dark = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_dark.json"));

var _in_memory_table = require("../../../../../../common/types/eui/in_memory_table");

var _use_ui_chrome_context = require("../../../../../contexts/ui/use_ui_chrome_context");

var _date_utils = require("../../../../../util/date_utils");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _common = require("../../../../common");

var _common2 = require("./common");

var _use_explore_data = require("./use_explore_data");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var customColorScaleFactory = function customColorScaleFactory(n) {
  return function (t) {
    if (t < 1 / n) {
      return 0;
    }

    if (t < 3 / n) {
      return n / 4 * (t - 1 / n);
    }

    return 0.5 + (t - 3 / n);
  };
};

var FEATURE_INFLUENCE = 'feature_influence';
var PAGE_SIZE_OPTIONS = [5, 10, 25, 50];

var ExplorationTitle = function ExplorationTitle(_ref) {
  var jobId = _ref.jobId;
  return _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("span", null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.jobIdTitle', {
    defaultMessage: 'Job ID {jobId}',
    values: {
      jobId: jobId
    }
  })));
};

var Exploration = _react.default.memo(function (_ref2) {
  var jobId = _ref2.jobId;

  var _useState = (0, _react.useState)(undefined),
      _useState2 = _slicedToArray(_useState, 2),
      jobConfig = _useState2[0],
      setJobConfig = _useState2[1];

  var _useState3 = (0, _react.useState)(0),
      _useState4 = _slicedToArray(_useState3, 2),
      pageIndex = _useState4[0],
      setPageIndex = _useState4[1];

  var _useState5 = (0, _react.useState)(25),
      _useState6 = _slicedToArray(_useState5, 2),
      pageSize = _useState6[0],
      setPageSize = _useState6[1];

  (0, _react.useEffect)(function () {
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var analyticsConfigs;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _ml_api_service.ml.dataFrameAnalytics.getDataFrameAnalytics(jobId);

            case 2:
              analyticsConfigs = _context.sent;

              if (Array.isArray(analyticsConfigs.data_frame_analytics) && analyticsConfigs.data_frame_analytics.length > 0) {
                setJobConfig(analyticsConfigs.data_frame_analytics[0]);
              }

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  }, []);
  var euiTheme = (0, _use_ui_chrome_context.useUiChromeContext)().getUiSettingsClient().get('theme:darkMode') ? _eui_theme_dark.default : _eui_theme_light.default;

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      clearTable = _useState8[0],
      setClearTable = _useState8[1];

  var _useState9 = (0, _react.useState)([]),
      _useState10 = _slicedToArray(_useState9, 2),
      selectedFields = _useState10[0],
      setSelectedFields = _useState10[1];

  var _useState11 = (0, _react.useState)(false),
      _useState12 = _slicedToArray(_useState11, 2),
      isColumnsPopoverVisible = _useState12[0],
      setColumnsPopoverVisible = _useState12[1]; // EuiInMemoryTable has an issue with dynamic sortable columns
  // and will trigger a full page Kibana error in such a case.
  // The following is a workaround until this is solved upstream:
  // - If the sortable/columns config changes,
  //   the table will be unmounted/not rendered.
  //   This is what setClearTable(true) in toggleColumn() does.
  // - After that on next render it gets re-enabled. To make sure React
  //   doesn't consolidate the state updates, setTimeout is used.


  if (clearTable) {
    setTimeout(function () {
      return setClearTable(false);
    }, 0);
  }

  function toggleColumnsPopover() {
    setColumnsPopoverVisible(!isColumnsPopoverVisible);
  }

  function closeColumnsPopover() {
    setColumnsPopoverVisible(false);
  }

  function toggleColumn(column) {
    if (tableItems.length > 0 && jobConfig !== undefined) {
      setClearTable(true); // spread to a new array otherwise the component wouldn't re-render

      setSelectedFields(_toConsumableArray((0, _common.toggleSelectedField)(selectedFields, column)));
    }
  }

  var _useExploreData = (0, _use_explore_data.useExploreData)(jobConfig, selectedFields, setSelectedFields),
      errorMessage = _useExploreData.errorMessage,
      loadExploreData = _useExploreData.loadExploreData,
      sortField = _useExploreData.sortField,
      sortDirection = _useExploreData.sortDirection,
      status = _useExploreData.status,
      tableItems = _useExploreData.tableItems;

  var docFields = [];
  var docFieldsCount = 0;

  if (tableItems.length > 0) {
    docFields = Object.keys(tableItems[0]);
    docFields.sort();
    docFieldsCount = docFields.length;
  }

  var columns = [];

  if (jobConfig !== undefined && selectedFields.length > 0 && tableItems.length > 0) {
    // table cell color coding takes into account:
    // - whether the theme is dark/light
    // - the number of analysis features
    // based on that
    var cellBgColorScale = _d2.default.scale.linear().domain([0, 1]) // typings for .range() incorrectly don't allow passing in a color extent.
    // @ts-ignore
    .range([_d2.default.rgb(euiTheme.euiColorEmptyShade), _d2.default.rgb(euiTheme.euiColorVis1)]);

    var featureCount = Object.keys(tableItems[0]).filter(function (key) {
      return key.includes("".concat(jobConfig.dest.results_field, ".").concat(FEATURE_INFLUENCE, "."));
    }).length;
    var customScale = customColorScaleFactory(featureCount);

    var cellBgColor = function cellBgColor(n) {
      return cellBgColorScale(customScale(n));
    };

    columns.push.apply(columns, _toConsumableArray(selectedFields.sort((0, _common.sortColumns)(tableItems[0], jobConfig.dest.results_field)).map(function (k) {
      var column = {
        field: k,
        name: k,
        sortable: true,
        truncateText: true
      };

      var render = function render(d, fullItem) {
        if (Array.isArray(d) && d.every(function (item) {
          return typeof item === 'string';
        })) {
          // If the cells data is an array of strings, return as a comma separated list.
          // The list will get limited to 5 items with `…` at the end if there's more in the original array.
          return "".concat(d.slice(0, 5).join(', ')).concat(d.length > 5 ? ', …' : '');
        } else if (Array.isArray(d)) {
          // If the cells data is an array of e.g. objects, display a 'array' badge with a
          // tooltip that explains that this type of field is not supported in this table.
          return _react.default.createElement(_eui.EuiToolTip, {
            content: _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.indexArrayToolTipContent', {
              defaultMessage: 'The full content of this array based column cannot be displayed.'
            })
          }, _react.default.createElement(_eui.EuiBadge, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.indexArrayBadgeContent', {
            defaultMessage: 'array'
          })));
        } else if (_typeof(d) === 'object' && d !== null) {
          // If the cells data is an object, display a 'object' badge with a
          // tooltip that explains that this type of field is not supported in this table.
          return _react.default.createElement(_eui.EuiToolTip, {
            content: _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.indexObjectToolTipContent', {
              defaultMessage: 'The full content of this object based column cannot be displayed.'
            })
          }, _react.default.createElement(_eui.EuiBadge, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.indexObjectBadgeContent', {
            defaultMessage: 'object'
          })));
        }

        var split = k.split('.');
        var backgroundColor;
        var color = undefined;
        var resultsField = jobConfig.dest.results_field;

        if (fullItem["".concat(resultsField, ".").concat(FEATURE_INFLUENCE, ".").concat(k)] !== undefined) {
          backgroundColor = cellBgColor(fullItem["".concat(resultsField, ".").concat(FEATURE_INFLUENCE, ".").concat(k)]);
        }

        if (split.length > 2 && split[0] === resultsField && split[1] === FEATURE_INFLUENCE) {
          backgroundColor = cellBgColor(d);
        }

        return _react.default.createElement("div", {
          className: "mlColoredTableCell",
          style: {
            backgroundColor: backgroundColor,
            color: color
          }
        }, d);
      };

      var columnType;

      if (tableItems.length > 0) {
        columnType = _typeof(tableItems[0][k]);
      }

      if (typeof columnType !== 'undefined') {
        switch (columnType) {
          case 'boolean':
            column.dataType = 'boolean';
            break;

          case 'Date':
            column.align = 'right';

            column.render = function (d) {
              return (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(d).unix() * 1000);
            };

            break;

          case 'number':
            column.dataType = 'number';
            column.render = render;
            break;

          default:
            column.render = render;
            break;
        }
      } else {
        column.render = render;
      }

      return column;
    })));
  }

  (0, _react.useEffect)(function () {
    // by default set the sorting to descending on the `outlier_score` field.
    // if that's not available sort ascending on the first column.
    // also check if the current sorting field is still available.
    if (jobConfig !== undefined && columns.length > 0 && !selectedFields.includes(sortField)) {
      var outlierScoreFieldName = (0, _common2.getOutlierScoreFieldName)(jobConfig);
      var outlierScoreFieldSelected = selectedFields.includes(outlierScoreFieldName);
      var field = outlierScoreFieldSelected ? outlierScoreFieldName : selectedFields[0];
      var direction = outlierScoreFieldSelected ? _in_memory_table.SORT_DIRECTION.DESC : _in_memory_table.SORT_DIRECTION.ASC;
      loadExploreData({
        field: field,
        direction: direction
      });
      return;
    }
  }, [jobConfig, columns.length, sortField, sortDirection, tableItems.length]);
  var sorting = false;
  var onTableChange;

  if (columns.length > 0 && sortField !== '') {
    sorting = {
      sort: {
        field: sortField,
        direction: sortDirection
      }
    };

    onTableChange = function onTableChange(_ref4) {
      var _ref4$page = _ref4.page,
          page = _ref4$page === void 0 ? {
        index: 0,
        size: 10
      } : _ref4$page,
          _ref4$sort = _ref4.sort,
          sort = _ref4$sort === void 0 ? {
        field: sortField,
        direction: sortDirection
      } : _ref4$sort;
      var index = page.index,
          size = page.size;
      setPageIndex(index);
      setPageSize(size);

      if (sort.field !== sortField || sort.direction !== sortDirection) {
        setClearTable(true);
        loadExploreData(sort);
      }
    };
  }

  var pagination = {
    initialPageIndex: pageIndex,
    initialPageSize: pageSize,
    totalItemCount: tableItems.length,
    pageSizeOptions: PAGE_SIZE_OPTIONS,
    hidePerPageOptions: false
  };

  if (jobConfig === undefined) {
    return null;
  }

  if (status === _use_explore_data.INDEX_STATUS.ERROR) {
    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(ExplorationTitle, {
      jobId: jobConfig.id
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.indexError', {
        defaultMessage: 'An error occurred loading the index data.'
      }),
      color: "danger",
      iconType: "cross"
    }, _react.default.createElement("p", null, errorMessage)));
  }

  if (status === _use_explore_data.INDEX_STATUS.LOADED && tableItems.length === 0) {
    return _react.default.createElement(_eui.EuiPanel, {
      grow: false
    }, _react.default.createElement(ExplorationTitle, {
      jobId: jobConfig.id
    }), _react.default.createElement(_eui.EuiCallOut, {
      title: _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.noDataCalloutTitle', {
        defaultMessage: 'Empty index query result.'
      }),
      color: "primary"
    }, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.noDataCalloutBody', {
      defaultMessage: 'The query for the index returned no results. Please make sure the index contains documents and your query is not too restrictive.'
    }))));
  }

  return _react.default.createElement(_eui.EuiPanel, {
    grow: false
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "spaceBetween",
    responsive: false
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(ExplorationTitle, {
    jobId: jobConfig.id
  })), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    gutterSize: "xs",
    responsive: false
  }, _react.default.createElement(_eui.EuiFlexItem, {
    style: {
      textAlign: 'right'
    }
  }, docFieldsCount > _common.MAX_COLUMNS && _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.fieldSelection', {
    defaultMessage: '{selectedFieldsLength, number} of {docFieldsCount, number} {docFieldsCount, plural, one {field} other {fields}} selected',
    values: {
      selectedFieldsLength: selectedFields.length,
      docFieldsCount: docFieldsCount
    }
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement(_eui.EuiPopover, {
    id: "popover",
    button: _react.default.createElement(_eui.EuiButtonIcon, {
      iconType: "gear",
      onClick: toggleColumnsPopover,
      "aria-label": _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.selectColumnsAriaLabel', {
        defaultMessage: 'Select columns'
      })
    }),
    isOpen: isColumnsPopoverVisible,
    closePopover: closeColumnsPopover,
    ownFocus: true
  }, _react.default.createElement(_eui.EuiPopoverTitle, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.exploration.selectFieldsPopoverTitle', {
    defaultMessage: 'Select fields'
  })), _react.default.createElement("div", {
    style: {
      maxHeight: '400px',
      overflowY: 'scroll'
    }
  }, docFields.map(function (d) {
    return _react.default.createElement(_eui.EuiCheckbox, {
      key: d,
      id: d,
      label: d,
      checked: selectedFields.includes(d),
      onChange: function onChange() {
        return toggleColumn(d);
      },
      disabled: selectedFields.includes(d) && selectedFields.length === 1
    });
  })))))))), status === _use_explore_data.INDEX_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent"
  }), status !== _use_explore_data.INDEX_STATUS.LOADING && _react.default.createElement(_eui.EuiProgress, {
    size: "xs",
    color: "accent",
    max: 1,
    value: 0
  }), clearTable === false && columns.length > 0 && sortField !== '' && _react.default.createElement(_in_memory_table.MlInMemoryTable, {
    allowNeutralSort: false,
    className: "mlDataFrameAnalyticsExploration",
    columns: columns,
    compressed: true,
    hasActions: false,
    isSelectable: false,
    items: tableItems,
    onTableChange: onTableChange,
    pagination: pagination,
    responsive: false,
    sorting: sorting
  }));
});

exports.Exploration = Exploration;