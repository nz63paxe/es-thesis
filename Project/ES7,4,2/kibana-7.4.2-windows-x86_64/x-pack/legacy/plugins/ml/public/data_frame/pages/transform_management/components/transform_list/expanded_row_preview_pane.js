"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sortColumns = sortColumns;
exports.ExpandedRowPreviewPane = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

var _in_memory_table = require("../../../../../../common/types/eui/in_memory_table");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _common = require("../../../../common");

var _field_types = require("../../../../../../common/constants/field_types");

var _date_utils = require("../../../../../util/date_utils");

var _transform_table = require("./transform_table");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function sortColumns(groupByArr) {
  return function (a, b) {
    // make sure groupBy fields are always most left columns
    if (groupByArr.some(function (aggName) {
      return aggName === a;
    }) && groupByArr.some(function (aggName) {
      return aggName === b;
    })) {
      return a.localeCompare(b);
    }

    if (groupByArr.some(function (aggName) {
      return aggName === a;
    })) {
      return -1;
    }

    if (groupByArr.some(function (aggName) {
      return aggName === b;
    })) {
      return 1;
    }

    return a.localeCompare(b);
  };
}

function getDataFromTransform(transformConfig) {
  var index = transformConfig.source.index;
  var pivot = transformConfig.pivot;
  var groupByArr = [];
  var previewRequest = {
    source: {
      index: index
    },
    pivot: pivot
  }; // hasOwnProperty check to ensure only properties on object itself, and not its prototypes

  if (pivot.group_by !== undefined) {
    for (var key in pivot.group_by) {
      if (pivot.group_by.hasOwnProperty(key)) {
        groupByArr.push(key);
      }
    }
  }

  return {
    groupByArr: groupByArr,
    previewRequest: previewRequest
  };
}

var ExpandedRowPreviewPane = function ExpandedRowPreviewPane(_ref) {
  var transformConfig = _ref.transformConfig;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      dataFramePreviewData = _useState2[0],
      setDataFramePreviewData = _useState2[1];

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      columns = _useState4[0],
      setColumns = _useState4[1];

  var _useState5 = (0, _react.useState)(0),
      _useState6 = _slicedToArray(_useState5, 2),
      pageIndex = _useState6[0],
      setPageIndex = _useState6[1];

  var _useState7 = (0, _react.useState)(10),
      _useState8 = _slicedToArray(_useState7, 2),
      pageSize = _useState8[0],
      setPageSize = _useState8[1];

  var _useState9 = (0, _react.useState)(''),
      _useState10 = _slicedToArray(_useState9, 2),
      sortField = _useState10[0],
      setSortField = _useState10[1];

  var _useState11 = (0, _react.useState)(_in_memory_table.SORT_DIRECTION.ASC),
      _useState12 = _slicedToArray(_useState11, 2),
      sortDirection = _useState12[0],
      setSortDirection = _useState12[1];

  var _useState13 = (0, _react.useState)(false),
      _useState14 = _slicedToArray(_useState13, 2),
      isLoading = _useState14[0],
      setIsLoading = _useState14[1];

  var _useState15 = (0, _react.useState)(''),
      _useState16 = _slicedToArray(_useState15, 2),
      errorMessage = _useState16[0],
      setErrorMessage = _useState16[1];

  var getPreviewFactory = function getPreviewFactory() {
    var concurrentLoads = 0;
    return (
      /*#__PURE__*/
      function () {
        var _getPreview = _asyncToGenerator(
        /*#__PURE__*/
        regeneratorRuntime.mark(function _callee() {
          var _getDataFromTransform, previewRequest, groupByArr, resp, columnKeys, tableColumns;

          return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.prev = 0;
                  concurrentLoads++;

                  if (!(concurrentLoads > 1)) {
                    _context.next = 4;
                    break;
                  }

                  return _context.abrupt("return");

                case 4:
                  _getDataFromTransform = getDataFromTransform(transformConfig), previewRequest = _getDataFromTransform.previewRequest, groupByArr = _getDataFromTransform.groupByArr;
                  setIsLoading(true);
                  _context.next = 8;
                  return _ml_api_service.ml.dataFrame.getDataFrameTransformsPreview(previewRequest);

                case 8:
                  resp = _context.sent;
                  setIsLoading(false);

                  if (resp.preview.length > 0) {
                    columnKeys = (0, _common.getFlattenedFields)(resp.preview[0]);
                    columnKeys.sort(sortColumns(groupByArr));
                    tableColumns = columnKeys.map(function (k) {
                      var column = {
                        field: k,
                        name: k,
                        sortable: true,
                        truncateText: true
                      };

                      if (typeof resp.mappings.properties[k] !== 'undefined') {
                        var esFieldType = resp.mappings.properties[k].type;

                        switch (esFieldType) {
                          case _field_types.ES_FIELD_TYPES.BOOLEAN:
                            column.dataType = 'boolean';
                            break;

                          case _field_types.ES_FIELD_TYPES.DATE:
                            column.align = 'right';

                            column.render = function (d) {
                              return (0, _date_utils.formatHumanReadableDateTimeSeconds)((0, _momentTimezone.default)(d).unix() * 1000);
                            };

                            break;

                          case _field_types.ES_FIELD_TYPES.BYTE:
                          case _field_types.ES_FIELD_TYPES.DOUBLE:
                          case _field_types.ES_FIELD_TYPES.FLOAT:
                          case _field_types.ES_FIELD_TYPES.HALF_FLOAT:
                          case _field_types.ES_FIELD_TYPES.INTEGER:
                          case _field_types.ES_FIELD_TYPES.LONG:
                          case _field_types.ES_FIELD_TYPES.SCALED_FLOAT:
                          case _field_types.ES_FIELD_TYPES.SHORT:
                            column.dataType = 'number';
                            break;

                          case _field_types.ES_FIELD_TYPES.KEYWORD:
                          case _field_types.ES_FIELD_TYPES.TEXT:
                            column.dataType = 'string';
                            break;
                        }
                      }

                      return column;
                    });
                    setDataFramePreviewData(resp.preview);
                    setColumns(tableColumns);
                    setSortField(sortField);
                    setSortDirection(sortDirection);
                  }

                  concurrentLoads--;

                  if (concurrentLoads > 0) {
                    concurrentLoads = 0;
                    getPreview();
                  }

                  _context.next = 19;
                  break;

                case 15:
                  _context.prev = 15;
                  _context.t0 = _context["catch"](0);
                  setIsLoading(false);
                  setErrorMessage(_i18n.i18n.translate('xpack.ml.dfTransformList.stepDetails.previewPane.errorMessage', {
                    defaultMessage: 'Preview could not be loaded'
                  }));

                case 19:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[0, 15]]);
        }));

        function getPreview() {
          return _getPreview.apply(this, arguments);
        }

        return getPreview;
      }()
    );
  };

  (0, _common.useRefreshTransformList)({
    onRefresh: getPreviewFactory()
  });
  var pagination = {
    initialPageIndex: pageIndex,
    initialPageSize: pageSize,
    totalItemCount: dataFramePreviewData.length,
    pageSizeOptions: [10, 20],
    hidePerPageOptions: false
  };
  var sorting = {
    sort: {
      field: sortField,
      direction: sortDirection
    }
  };

  var onTableChange = function onTableChange(_ref2) {
    var _ref2$page = _ref2.page,
        page = _ref2$page === void 0 ? {
      index: 0,
      size: 10
    } : _ref2$page,
        _ref2$sort = _ref2.sort,
        sort = _ref2$sort === void 0 ? {
      field: columns[0].field,
      direction: _in_memory_table.SORT_DIRECTION.ASC
    } : _ref2$sort;
    var index = page.index,
        size = page.size;
    setPageIndex(index);
    setPageSize(size);
    var field = sort.field,
        direction = sort.direction;
    setSortField(field);
    setSortDirection(direction);
  };

  return _react.default.createElement(_transform_table.TransformTable, {
    allowNeutralSort: false,
    loading: dataFramePreviewData.length === 0 && isLoading === true,
    compressed: true,
    items: dataFramePreviewData,
    columns: columns,
    onTableChange: onTableChange,
    pagination: pagination,
    sorting: sorting,
    error: errorMessage
  });
};

exports.ExpandedRowPreviewPane = ExpandedRowPreviewPane;