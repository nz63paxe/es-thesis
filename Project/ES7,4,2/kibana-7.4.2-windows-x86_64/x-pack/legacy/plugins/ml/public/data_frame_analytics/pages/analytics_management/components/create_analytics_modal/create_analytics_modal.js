"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateAnalyticsModal = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CreateAnalyticsModal = function CreateAnalyticsModal(_ref) {
  var actions = _ref.actions,
      children = _ref.children,
      state = _ref.state;
  var closeModal = actions.closeModal,
      createAnalyticsJob = actions.createAnalyticsJob,
      startAnalyticsJob = actions.startAnalyticsJob;
  var isAdvancedEditorEnabled = state.isAdvancedEditorEnabled,
      isJobCreated = state.isJobCreated,
      isJobStarted = state.isJobStarted,
      isModalButtonDisabled = state.isModalButtonDisabled,
      isValid = state.isValid;
  var width = isAdvancedEditorEnabled ? '640px' : '450px';
  return _react.default.createElement(_eui.EuiOverlayMask, null, _react.default.createElement(_eui.EuiModal, {
    onClose: closeModal,
    style: {
      width: width
    }
  }, _react.default.createElement(_eui.EuiModalHeader, null, _react.default.createElement(_eui.EuiModalHeaderTitle, null, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.modalHeaderTitle', {
    defaultMessage: 'Create analytics job'
  }))), _react.default.createElement(_eui.EuiModalBody, null, children), _react.default.createElement(_eui.EuiModalFooter, null, (!isJobCreated || !isJobStarted) && _react.default.createElement(_eui.EuiButtonEmpty, {
    onClick: closeModal
  }, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.modalCancelButton', {
    defaultMessage: 'Cancel'
  })), !isJobCreated && !isJobStarted && _react.default.createElement(_eui.EuiButton, {
    disabled: !isValid || isModalButtonDisabled,
    onClick: createAnalyticsJob,
    fill: true
  }, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.modalCreateButton', {
    defaultMessage: 'Create'
  })), isJobCreated && !isJobStarted && _react.default.createElement(_eui.EuiButton, {
    disabled: isModalButtonDisabled,
    onClick: startAnalyticsJob,
    fill: true
  }, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.modalStartButton', {
    defaultMessage: 'Start'
  })), isJobCreated && isJobStarted && _react.default.createElement(_eui.EuiButton, {
    onClick: closeModal,
    fill: true
  }, _i18n.i18n.translate('xpack.ml.dataframe.analytics.create.modalCloseButton', {
    defaultMessage: 'Close'
  })))));
};

exports.CreateAnalyticsModal = CreateAnalyticsModal;