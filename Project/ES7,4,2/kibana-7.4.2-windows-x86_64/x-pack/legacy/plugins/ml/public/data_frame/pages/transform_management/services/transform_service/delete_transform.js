"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteTransforms = void 0;

var _i18n = require("@kbn/i18n");

var _notify = require("ui/notify");

var _ml_api_service = require("../../../../../services/ml_api_service");

var _common = require("../../../../common");

var _messagebar_service = require("../../../../../../public/components/messagebar/messagebar_service");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var deleteTransforms =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(dataFrames) {
    var dataFramesInfo, results, transformId;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            dataFramesInfo = dataFrames.map(function (df) {
              return {
                id: df.config.id,
                state: df.stats.state
              };
            });
            _context.next = 3;
            return _ml_api_service.ml.dataFrame.deleteDataFrameTransforms(dataFramesInfo);

          case 3:
            results = _context.sent;

            for (transformId in results) {
              // hasOwnProperty check to ensure only properties on object itself, and not its prototypes
              if (results.hasOwnProperty(transformId)) {
                if (results[transformId].success === true) {
                  _notify.toastNotifications.addSuccess(_i18n.i18n.translate('xpack.ml.dataframe.transformList.deleteTransformSuccessMessage', {
                    defaultMessage: 'Request to delete data frame transform {transformId} acknowledged.',
                    values: {
                      transformId: transformId
                    }
                  }));
                } else {
                  _notify.toastNotifications.addDanger(_i18n.i18n.translate('xpack.ml.dataframe.transformList.deleteTransformErrorMessage', {
                    defaultMessage: 'An error occurred deleting the data frame transform {transformId}',
                    values: {
                      transformId: transformId
                    }
                  }));

                  _messagebar_service.mlMessageBarService.notify.error(results[transformId].error);
                }
              }
            }

            _common.refreshTransformList$.next(_common.REFRESH_TRANSFORM_LIST_STATE.REFRESH);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function deleteTransforms(_x) {
    return _ref.apply(this, arguments);
  };
}();

exports.deleteTransforms = deleteTransforms;