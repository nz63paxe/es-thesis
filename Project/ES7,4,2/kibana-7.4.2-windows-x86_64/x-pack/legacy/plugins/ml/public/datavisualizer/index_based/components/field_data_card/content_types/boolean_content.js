"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BooleanContent = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _round_to_decimal_place = require("../../../../../formatters/round_to_decimal_place");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function getPercentLabel(valueCount, totalCount) {
  if (valueCount === 0) {
    return '0%';
  }

  var percent = 100 * valueCount / totalCount;

  if (percent >= 0.1) {
    return "".concat((0, _round_to_decimal_place.roundToDecimalPlace)(percent, 1), "%");
  } else {
    return '< 0.1%';
  }
}

var BooleanContent = function BooleanContent(_ref) {
  var config = _ref.config;
  var stats = config.stats;

  if (stats === undefined) {
    return null;
  }

  var count = stats.count,
      sampleCount = stats.sampleCount,
      trueCount = stats.trueCount,
      falseCount = stats.falseCount;
  var docsPercent = (0, _round_to_decimal_place.roundToDecimalPlace)(count / sampleCount * 100); // TODO - display counts of true / false in an Elastic Charts bar chart (or Pie chart if available).

  return _react.default.createElement("div", {
    className: "mlFieldDataCard__stats"
  }, _react.default.createElement("div", null, _react.default.createElement(_eui.EuiIcon, {
    type: "document"
  }), "\xA0", _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.fieldDataCard.cardBoolean.documentsCountDescription",
    defaultMessage: "{count, plural, zero {# document} one {# document} other {# documents}} ({docsPercent}%)",
    values: {
      count: count,
      docsPercent: docsPercent
    }
  })), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement("div", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.ml.fieldDataCard.cardBoolean.valuesLabel",
    defaultMessage: "values"
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "xs"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xs",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      width: 100
    },
    className: "eui-textTruncate"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    textAlign: "right"
  }, "true")), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiProgress, {
    value: trueCount,
    max: count,
    color: "primary",
    size: "l"
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      width: 70
    },
    className: "eui-textTruncate"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    textAlign: "left"
  }, getPercentLabel(trueCount, count)))), _react.default.createElement(_eui.EuiSpacer, {
    size: "xs"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "xs",
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      width: 100
    },
    className: "eui-textTruncate"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    textAlign: "right"
  }, "false")), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiProgress, {
    value: falseCount,
    max: count,
    color: "subdued",
    size: "l"
  })), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      width: 70
    },
    className: "eui-textTruncate"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s",
    textAlign: "left"
  }, getPercentLabel(falseCount, count))))));
};

exports.BooleanContent = BooleanContent;