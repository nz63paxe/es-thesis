"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateAnalyticsButton = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _check_privilege = require("../../../../../privilege/check_privilege");

var _create_analytics_advanced_editor = require("../create_analytics_advanced_editor");

var _create_analytics_form = require("../create_analytics_form");

var _create_analytics_modal = require("../create_analytics_modal");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CreateAnalyticsButton = function CreateAnalyticsButton(props) {
  var _props$state = props.state,
      disabled = _props$state.disabled,
      isAdvancedEditorEnabled = _props$state.isAdvancedEditorEnabled,
      isModalVisible = _props$state.isModalVisible;
  var openModal = props.actions.openModal;

  var button = _react.default.createElement(_eui.EuiButton, {
    disabled: disabled,
    fill: true,
    onClick: openModal,
    iconType: "plusInCircle",
    size: "s",
    "data-test-subj": "mlDataFrameAnalyticsButtonCreate"
  }, _i18n.i18n.translate('xpack.ml.dataframe.analyticsList.createDataFrameAnalyticsButton', {
    defaultMessage: 'Create analytics job'
  }));

  if (disabled) {
    return _react.default.createElement(_eui.EuiToolTip, {
      position: "top",
      content: (0, _check_privilege.createPermissionFailureMessage)('canCreateDataFrameAnalytics')
    }, button);
  }

  return _react.default.createElement(_react.Fragment, null, button, isModalVisible && _react.default.createElement(_create_analytics_modal.CreateAnalyticsModal, props, isAdvancedEditorEnabled === false && _react.default.createElement(_create_analytics_form.CreateAnalyticsForm, props), isAdvancedEditorEnabled === true && _react.default.createElement(_create_analytics_advanced_editor.CreateAnalyticsAdvancedEditor, props)));
};

exports.CreateAnalyticsButton = CreateAnalyticsButton;