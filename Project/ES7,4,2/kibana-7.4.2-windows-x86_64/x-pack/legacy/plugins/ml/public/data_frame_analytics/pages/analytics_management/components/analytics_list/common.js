"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isDataFrameAnalyticsRunning = isDataFrameAnalyticsRunning;
exports.isDataFrameAnalyticsStats = isDataFrameAnalyticsStats;
exports.getDataFrameAnalyticsProgress = getDataFrameAnalyticsProgress;
exports.isCompletedAnalyticsJob = isCompletedAnalyticsJob;
exports.getResultsUrl = getResultsUrl;
exports.DataFrameAnalyticsListColumn = exports.DATA_FRAME_MODE = exports.DATA_FRAME_TASK_STATE = void 0;

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DATA_FRAME_TASK_STATE;
exports.DATA_FRAME_TASK_STATE = DATA_FRAME_TASK_STATE;

(function (DATA_FRAME_TASK_STATE) {
  DATA_FRAME_TASK_STATE["ANALYZING"] = "analyzing";
  DATA_FRAME_TASK_STATE["FAILED"] = "failed";
  DATA_FRAME_TASK_STATE["REINDEXING"] = "reindexing";
  DATA_FRAME_TASK_STATE["STARTED"] = "started";
  DATA_FRAME_TASK_STATE["STOPPED"] = "stopped";
})(DATA_FRAME_TASK_STATE || (exports.DATA_FRAME_TASK_STATE = DATA_FRAME_TASK_STATE = {}));

var DATA_FRAME_MODE;
exports.DATA_FRAME_MODE = DATA_FRAME_MODE;

(function (DATA_FRAME_MODE) {
  DATA_FRAME_MODE["BATCH"] = "batch";
  DATA_FRAME_MODE["CONTINUOUS"] = "continuous";
})(DATA_FRAME_MODE || (exports.DATA_FRAME_MODE = DATA_FRAME_MODE = {}));

function isDataFrameAnalyticsRunning(stats) {
  return stats.state === DATA_FRAME_TASK_STATE.ANALYZING || stats.state === DATA_FRAME_TASK_STATE.STARTED || stats.state === DATA_FRAME_TASK_STATE.REINDEXING;
}

function isDataFrameAnalyticsStats(arg) {
  return _typeof(arg) === 'object' && arg !== null && {}.hasOwnProperty.call(arg, 'state') && Object.values(DATA_FRAME_TASK_STATE).includes(arg.state) && {}.hasOwnProperty.call(arg, 'progress') && Array.isArray(arg.progress);
}

function getDataFrameAnalyticsProgress(stats) {
  if (isDataFrameAnalyticsStats(stats)) {
    return Math.round(stats.progress.reduce(function (p, c) {
      return p + c.progress_percent;
    }, 0) / stats.progress.length);
  }

  return undefined;
}

// Used to pass on attribute names to table columns
var DataFrameAnalyticsListColumn;
exports.DataFrameAnalyticsListColumn = DataFrameAnalyticsListColumn;

(function (DataFrameAnalyticsListColumn) {
  DataFrameAnalyticsListColumn["configDestIndex"] = "config.dest.index";
  DataFrameAnalyticsListColumn["configSourceIndex"] = "config.source.index";
  DataFrameAnalyticsListColumn["id"] = "id";
})(DataFrameAnalyticsListColumn || (exports.DataFrameAnalyticsListColumn = DataFrameAnalyticsListColumn = {}));

function isCompletedAnalyticsJob(stats) {
  var progress = getDataFrameAnalyticsProgress(stats);
  return stats.state === DATA_FRAME_TASK_STATE.STOPPED && progress === 100;
}

function getResultsUrl(jobId) {
  return "ml#/data_frame_analytics/exploration?_g=(ml:(jobId:".concat(jobId, "))");
}