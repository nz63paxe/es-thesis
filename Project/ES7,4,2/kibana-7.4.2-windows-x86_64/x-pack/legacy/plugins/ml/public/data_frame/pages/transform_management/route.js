"use strict";

var _routes = _interopRequireDefault(require("ui/routes"));

var _check_license = require("../../../license/check_license");

var _check_privilege = require("../../../privilege/check_privilege");

var _index_utils = require("../../../util/index_utils");

var _breadcrumbs = require("../../breadcrumbs");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore
var template = "<ml-data-frame-page />";

_routes.default.when('/data_frames/?', {
  template: template,
  k7Breadcrumbs: _breadcrumbs.getDataFrameBreadcrumbs,
  resolve: {
    CheckLicense: _check_license.checkBasicLicense,
    privileges: _check_privilege.checkGetDataFrameTransformsPrivilege,
    indexPatterns: _index_utils.loadIndexPatterns
  }
});