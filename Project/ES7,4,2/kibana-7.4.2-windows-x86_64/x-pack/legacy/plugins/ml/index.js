"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ml = void 0;

var _path = require("path");

var _i18n = require("@kbn/i18n");

var _new_platform = require("./server/new_platform");

var _mappings = _interopRequireDefault(require("./mappings"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore: could not find declaration file for module
const ml = kibana => {
  return new kibana.Plugin({
    require: ['kibana', 'elasticsearch', 'xpack_main'],
    id: 'ml',
    configPrefix: 'xpack.ml',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    uiExports: {
      managementSections: ['plugins/ml/management'],
      app: {
        title: _i18n.i18n.translate('xpack.ml.mlNavTitle', {
          defaultMessage: 'Machine Learning'
        }),
        description: _i18n.i18n.translate('xpack.ml.mlNavDescription', {
          defaultMessage: 'Machine Learning for the Elastic Stack'
        }),
        icon: 'plugins/ml/ml.svg',
        euiIconType: 'machineLearningApp',
        main: 'plugins/ml/app'
      },
      styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),
      hacks: ['plugins/ml/hacks/toggle_app_link_in_nav'],
      savedObjectSchemas: {
        'ml-telemetry': {
          isNamespaceAgnostic: true
        }
      },
      mappings: _mappings.default,
      home: ['plugins/ml/register_feature'],

      injectDefaultVars(server) {
        const config = server.config();
        return {
          mlEnabled: config.get('xpack.ml.enabled')
        };
      }

    },

    async init(server) {
      const kbnServer = server;
      const initializerContext = {
        legacyConfig: server.config(),
        logger: {
          get(...contextParts) {
            return kbnServer.newPlatform.coreContext.logger.get('plugins', 'ml', ...contextParts);
          }

        }
      };
      const mlHttpService = { ...kbnServer.newPlatform.setup.core.http,
        route: server.route.bind(server)
      };
      const core = {
        addAppLinksToSampleDataset: server.addAppLinksToSampleDataset,
        injectUiAppVars: server.injectUiAppVars,
        http: mlHttpService,
        savedObjects: server.savedObjects,
        usage: server.usage
      };
      const plugins = {
        elasticsearch: server.plugins.elasticsearch,
        security: server.plugins.security,
        xpackMain: server.plugins.xpack_main,
        spaces: server.plugins.spaces,
        ml: this
      };
      (0, _new_platform.plugin)(initializerContext).setup(core, plugins);
    }

  });
};

exports.ml = ml;