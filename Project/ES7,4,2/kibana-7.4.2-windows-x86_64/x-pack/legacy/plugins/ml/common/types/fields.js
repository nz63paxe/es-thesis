"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.METRIC_AGG_TYPE = exports.EVENT_RATE_FIELD_ID = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const EVENT_RATE_FIELD_ID = '__ml_event_rate_count__';
exports.EVENT_RATE_FIELD_ID = EVENT_RATE_FIELD_ID;
const METRIC_AGG_TYPE = 'metrics';
exports.METRIC_AGG_TYPE = METRIC_AGG_TYPE;