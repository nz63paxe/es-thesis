"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isMlJob = isMlJob;
exports.isMlJobs = isMlJobs;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// TS TODO: This is not yet a fully fledged representation of the job data structure,
// but it fulfills some basic TypeScript related needs.
function isMlJob(arg) {
  return typeof arg.job_id === 'string';
} // eslint-disable-next-line @typescript-eslint/no-empty-interface


function isMlJobs(arg) {
  if (Array.isArray(arg) === false) {
    return false;
  }

  return arg.every(d => isMlJob(d));
}