"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fillResultsWithTimeouts = fillResultsWithTimeouts;
Object.defineProperty(exports, "isRequestTimeout", {
  enumerable: true,
  get: function () {
    return _error_utils.isRequestTimeout;
  }
});

var _i18n = require("@kbn/i18n");

var _error_utils = require("../job_service/error_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore no declaration file for module
// populate a results object with timeout errors for the ids which haven't already been set
function fillResultsWithTimeouts({
  results,
  id,
  items,
  action
}) {
  const extra = items.length - Object.keys(results).length > 1 ? _i18n.i18n.translate('xpack.ml.models.transformService.allOtherRequestsCancelledDescription', {
    defaultMessage: 'All other requests cancelled.'
  }) : '';
  const error = {
    response: {
      error: {
        root_cause: [{
          reason: _i18n.i18n.translate('xpack.ml.models.transformService.requestToActionTimedOutErrorMessage', {
            defaultMessage: `Request to {action} '{id}' timed out. {extra}`,
            values: {
              id,
              action,
              extra
            }
          })
        }]
      }
    }
  };
  const newResults = {};
  return items.reduce((accumResults, currentVal) => {
    if (results[currentVal.id] === undefined) {
      accumResults[currentVal.id] = {
        success: false,
        error
      };
    } else {
      accumResults[currentVal.id] = results[currentVal.id];
    }

    return accumResults;
  }, newResults);
}