"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.aggregations = void 0;

var _aggregation_types = require("../../../../common/constants/aggregation_types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const aggregations = [{
  id: _aggregation_types.ML_JOB_AGGREGATION.COUNT,
  title: 'Count',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
  dslName: _aggregation_types.ES_AGGREGATION.COUNT,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.HIGH_COUNT,
  title: 'High count',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
  dslName: _aggregation_types.ES_AGGREGATION.COUNT,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.LOW_COUNT,
  title: 'Low count',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.COUNT,
  dslName: _aggregation_types.ES_AGGREGATION.COUNT,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.MEAN,
  title: 'Mean',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.AVG,
  dslName: _aggregation_types.ES_AGGREGATION.AVG,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.AVG,
    min: _aggregation_types.KIBANA_AGGREGATION.AVG
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.HIGH_MEAN,
  title: 'High mean',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.AVG,
  dslName: _aggregation_types.ES_AGGREGATION.AVG,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.AVG,
    min: _aggregation_types.KIBANA_AGGREGATION.AVG
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.LOW_MEAN,
  title: 'Low mean',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.AVG,
  dslName: _aggregation_types.ES_AGGREGATION.AVG,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.AVG,
    min: _aggregation_types.KIBANA_AGGREGATION.AVG
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.SUM,
  title: 'Sum',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.SUM,
  dslName: _aggregation_types.ES_AGGREGATION.SUM,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.SUM,
    min: _aggregation_types.KIBANA_AGGREGATION.SUM
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.HIGH_SUM,
  title: 'High sum',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.SUM,
  dslName: _aggregation_types.ES_AGGREGATION.SUM,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.SUM,
    min: _aggregation_types.KIBANA_AGGREGATION.SUM
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.LOW_SUM,
  title: 'Low sum',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.SUM,
  dslName: _aggregation_types.ES_AGGREGATION.SUM,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.SUM,
    min: _aggregation_types.KIBANA_AGGREGATION.SUM
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.MEDIAN,
  title: 'Median',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.MEDIAN,
  dslName: _aggregation_types.ES_AGGREGATION.PERCENTILES,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.HIGH_MEDIAN,
  title: 'High median',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.MEDIAN,
  dslName: _aggregation_types.ES_AGGREGATION.PERCENTILES,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.LOW_MEDIAN,
  title: 'Low median',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.MEDIAN,
  dslName: _aggregation_types.ES_AGGREGATION.PERCENTILES,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.MIN,
  title: 'Min',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.MIN,
  dslName: _aggregation_types.ES_AGGREGATION.MIN,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MIN,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.MAX,
  title: 'Max',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.MAX,
  dslName: _aggregation_types.ES_AGGREGATION.MAX,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MAX
  },
  fields: []
}, {
  id: _aggregation_types.ML_JOB_AGGREGATION.DISTINCT_COUNT,
  title: 'Distinct count',
  kibanaName: _aggregation_types.KIBANA_AGGREGATION.CARDINALITY,
  dslName: _aggregation_types.ES_AGGREGATION.CARDINALITY,
  type: 'metrics',
  mlModelPlotAgg: {
    max: _aggregation_types.KIBANA_AGGREGATION.MAX,
    min: _aggregation_types.KIBANA_AGGREGATION.MIN
  },
  fields: []
}];
exports.aggregations = aggregations;