"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _i18n = require("@kbn/i18n");

var _sample_data_sets = require("../lib/sample_data_sets");

var _check_license = require("../lib/check_license");

var _mirror_plugin_status = require("../../../../server/lib/mirror_plugin_status");

var _feature_flags = require("../../common/constants/feature_flags");

var _license = require("../../common/constants/license");

var _annotations = require("../routes/annotations");

var _anomaly_detectors = require("../routes/anomaly_detectors");

var _datafeeds = require("../routes/datafeeds");

var _indices = require("../routes/indices");

var _job_validation = require("../routes/job_validation");

var _ml_telemetry = require("../lib/ml_telemetry");

var _notification_settings = require("../routes/notification_settings");

var _system = require("../routes/system");

var _data_frame = require("../routes/data_frame");

var _data_frame_analytics = require("../routes/data_frame_analytics");

var _modules = require("../routes/modules");

var _data_visualizer = require("../routes/data_visualizer");

var _calendars = require("../routes/calendars");

var _fields_service = require("../routes/fields_service");

var _filters = require("../routes/filters");

var _results_service = require("../routes/results_service");

var _job_service = require("../routes/job_service");

var _job_audit_messages = require("../routes/job_audit_messages");

var _file_data_visualizer = require("../routes/file_data_visualizer");

var _log = require("../client/log");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Plugin {
  constructor(initializerContext) {
    _defineProperty(this, "pluginId", 'ml');

    _defineProperty(this, "config", void 0);

    _defineProperty(this, "log", void 0);

    this.config = initializerContext.legacyConfig;
    this.log = initializerContext.logger.get();
  }

  setup(core, plugins) {
    const xpackMainPlugin = plugins.xpackMain;
    const {
      addAppLinksToSampleDataset,
      http,
      injectUiAppVars
    } = core;
    const pluginId = this.pluginId;
    (0, _mirror_plugin_status.mirrorPluginStatus)(xpackMainPlugin, plugins.ml);
    xpackMainPlugin.status.once('green', () => {
      // Register a function that is called whenever the xpack info changes,
      // to re-compute the license check results for this plugin
      const mlFeature = xpackMainPlugin.info.feature(pluginId);
      mlFeature.registerLicenseCheckResultsGenerator(_check_license.checkLicense); // Add links to the Kibana sample data sets if ml is enabled
      // and there is a full license (trial or platinum).

      if (mlFeature.isEnabled() === true) {
        const licenseCheckResults = mlFeature.getLicenseCheckResults();

        if (licenseCheckResults.licenseType === _license.LICENSE_TYPE.FULL) {
          (0, _sample_data_sets.addLinksToSampleDatasets)({
            addAppLinksToSampleDataset
          });
        }
      }
    });
    xpackMainPlugin.registerFeature({
      id: 'ml',
      name: _i18n.i18n.translate('xpack.ml.featureRegistry.mlFeatureName', {
        defaultMessage: 'Machine Learning'
      }),
      icon: 'machineLearningApp',
      navLinkId: 'ml',
      app: ['ml', 'kibana'],
      catalogue: ['ml'],
      privileges: {},
      reserved: {
        privilege: {
          savedObject: {
            all: [],
            read: []
          },
          ui: []
        },
        description: _i18n.i18n.translate('xpack.ml.feature.reserved.description', {
          defaultMessage: 'To grant users access, you should also assign either the machine_learning_user or machine_learning_admin role.'
        })
      }
    }); // Add server routes and initialize the plugin here

    const commonRouteConfig = {
      pre: [function forbidApiAccess() {
        const licenseCheckResults = xpackMainPlugin.info.feature(pluginId).getLicenseCheckResults();

        if (licenseCheckResults.isAvailable) {
          return null;
        } else {
          throw _boom.default.forbidden(licenseCheckResults.message);
        }
      }]
    };
    injectUiAppVars('ml', () => {
      return {
        kbnIndex: this.config.get('kibana.index'),
        mlAnnotationsEnabled: _feature_flags.FEATURE_ANNOTATIONS_ENABLED
      };
    });
    const routeInitializationDeps = {
      commonRouteConfig,
      route: http.route,
      elasticsearchPlugin: plugins.elasticsearch,
      spacesPlugin: plugins.spaces
    };
    const extendedRouteInitializationDeps = { ...routeInitializationDeps,
      config: this.config,
      xpackMainPlugin: plugins.xpackMain,
      savedObjects: core.savedObjects,
      spacesPlugin: plugins.spaces
    };
    const usageInitializationDeps = {
      elasticsearchPlugin: plugins.elasticsearch,
      usage: core.usage,
      savedObjects: core.savedObjects
    };
    const logInitializationDeps = {
      log: this.log
    };
    (0, _annotations.annotationRoutes)(routeInitializationDeps);
    (0, _anomaly_detectors.jobRoutes)(routeInitializationDeps);
    (0, _datafeeds.dataFeedRoutes)(routeInitializationDeps);
    (0, _data_frame.dataFrameRoutes)(routeInitializationDeps);
    (0, _data_frame_analytics.dataFrameAnalyticsRoutes)(routeInitializationDeps);
    (0, _indices.indicesRoutes)(routeInitializationDeps);
    (0, _job_validation.jobValidationRoutes)(extendedRouteInitializationDeps);
    (0, _notification_settings.notificationRoutes)(routeInitializationDeps);
    (0, _system.systemRoutes)(extendedRouteInitializationDeps);
    (0, _modules.dataRecognizer)(routeInitializationDeps);
    (0, _data_visualizer.dataVisualizerRoutes)(routeInitializationDeps);
    (0, _calendars.calendars)(routeInitializationDeps);
    (0, _fields_service.fieldsService)(routeInitializationDeps);
    (0, _filters.filtersRoutes)(routeInitializationDeps);
    (0, _results_service.resultsServiceRoutes)(routeInitializationDeps);
    (0, _job_service.jobServiceRoutes)(routeInitializationDeps);
    (0, _job_audit_messages.jobAuditMessagesRoutes)(routeInitializationDeps);
    (0, _file_data_visualizer.fileDataVisualizerRoutes)(extendedRouteInitializationDeps);
    (0, _log.initMlServerLog)(logInitializationDeps);
    (0, _ml_telemetry.makeMlUsageCollector)(usageInitializationDeps);
  }

  stop() {}

}

exports.Plugin = Plugin;