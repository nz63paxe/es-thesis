"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "newJobChartsProvider", {
  enumerable: true,
  get: function () {
    return _charts.newJobChartsProvider;
  }
});

var _charts = require("./charts");