"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeMlUsageCollector = makeMlUsageCollector;

var _ml_telemetry = require("./ml_telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function makeMlUsageCollector({
  elasticsearchPlugin,
  usage,
  savedObjects
}) {
  const mlUsageCollector = usage.collectorSet.makeUsageCollector({
    type: 'ml',
    isReady: () => true,
    fetch: async () => {
      try {
        const savedObjectsClient = (0, _ml_telemetry.getSavedObjectsClient)(elasticsearchPlugin, savedObjects);
        const mlTelemetrySavedObject = await savedObjectsClient.get('ml-telemetry', _ml_telemetry.ML_TELEMETRY_DOC_ID);
        return mlTelemetrySavedObject.attributes;
      } catch (err) {
        return (0, _ml_telemetry.createMlTelemetry)();
      }
    }
  });
  usage.collectorSet.register(mlUsageCollector);
}