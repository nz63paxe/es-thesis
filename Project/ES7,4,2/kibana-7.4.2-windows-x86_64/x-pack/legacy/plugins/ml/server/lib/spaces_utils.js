"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.spacesUtilsProvider = spacesUtilsProvider;

var _get_active_space = require("../../../spaces/server/lib/get_active_space");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function spacesUtilsProvider(spacesPlugin, request, config) {
  async function activeSpace() {
    const spacesClient = await spacesPlugin.getScopedSpacesClient(request);

    try {
      return {
        valid: true,
        space: await (0, _get_active_space.getActiveSpace)(spacesClient, request.getBasePath(), config.get('server.basePath'))
      };
    } catch (e) {
      return {
        valid: false
      };
    }
  }

  async function isMlEnabledInSpace() {
    const {
      valid,
      space
    } = await activeSpace();

    if (valid === true && space !== undefined) {
      return space.disabledFeatures.includes('ml') === false;
    }

    return true;
  }

  return {
    isMlEnabledInSpace
  };
}