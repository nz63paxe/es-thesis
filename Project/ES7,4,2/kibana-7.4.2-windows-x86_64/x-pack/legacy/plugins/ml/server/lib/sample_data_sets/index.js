"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "addLinksToSampleDatasets", {
  enumerable: true,
  get: function () {
    return _sample_data_sets.addLinksToSampleDatasets;
  }
});

var _sample_data_sets = require("./sample_data_sets");