"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformServiceProvider = transformServiceProvider;

var _common = require("../../../public/data_frame/common");

var _error_utils = require("./error_utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var TRANSFORM_ACTIONS;

(function (TRANSFORM_ACTIONS) {
  TRANSFORM_ACTIONS["STOP"] = "stop";
  TRANSFORM_ACTIONS["START"] = "start";
  TRANSFORM_ACTIONS["DELETE"] = "delete";
})(TRANSFORM_ACTIONS || (TRANSFORM_ACTIONS = {}));

function transformServiceProvider(callWithRequest) {
  async function deleteTransform(transformId) {
    return callWithRequest('ml.deleteDataFrameTransform', {
      transformId
    });
  }

  async function stopTransform(options) {
    return callWithRequest('ml.stopDataFrameTransform', options);
  }

  async function startTransform(options) {
    return callWithRequest('ml.startDataFrameTransform', options);
  }

  async function deleteTransforms(transformsInfo) {
    const results = {};

    for (const transformInfo of transformsInfo) {
      const transformId = transformInfo.id;

      try {
        if (transformInfo.state === _common.DATA_FRAME_TRANSFORM_STATE.FAILED) {
          try {
            await stopTransform({
              transformId,
              force: true,
              waitForCompletion: true
            });
          } catch (e) {
            if ((0, _error_utils.isRequestTimeout)(e)) {
              return (0, _error_utils.fillResultsWithTimeouts)({
                results,
                id: transformId,
                items: transformsInfo,
                action: TRANSFORM_ACTIONS.DELETE
              });
            }
          }
        }

        await deleteTransform(transformId);
        results[transformId] = {
          success: true
        };
      } catch (e) {
        if ((0, _error_utils.isRequestTimeout)(e)) {
          return (0, _error_utils.fillResultsWithTimeouts)({
            results,
            id: transformInfo.id,
            items: transformsInfo,
            action: TRANSFORM_ACTIONS.DELETE
          });
        }

        results[transformId] = {
          success: false,
          error: JSON.stringify(e)
        };
      }
    }

    return results;
  }

  async function startTransforms(transformsInfo) {
    const results = {};

    for (const transformInfo of transformsInfo) {
      const transformId = transformInfo.id;

      try {
        await startTransform({
          transformId,
          force: transformInfo.state !== undefined ? transformInfo.state === _common.DATA_FRAME_TRANSFORM_STATE.FAILED : false
        });
        results[transformId] = {
          success: true
        };
      } catch (e) {
        if ((0, _error_utils.isRequestTimeout)(e)) {
          return (0, _error_utils.fillResultsWithTimeouts)({
            results,
            id: transformId,
            items: transformsInfo,
            action: TRANSFORM_ACTIONS.START
          });
        }

        results[transformId] = {
          success: false,
          error: JSON.stringify(e)
        };
      }
    }

    return results;
  }

  async function stopTransforms(transformsInfo) {
    const results = {};

    for (const transformInfo of transformsInfo) {
      const transformId = transformInfo.id;

      try {
        await stopTransform({
          transformId,
          force: transformInfo.state !== undefined ? transformInfo.state === _common.DATA_FRAME_TRANSFORM_STATE.FAILED : false,
          waitForCompletion: true
        });
        results[transformId] = {
          success: true
        };
      } catch (e) {
        if ((0, _error_utils.isRequestTimeout)(e)) {
          return (0, _error_utils.fillResultsWithTimeouts)({
            results,
            id: transformId,
            items: transformsInfo,
            action: TRANSFORM_ACTIONS.STOP
          });
        }

        results[transformId] = {
          success: false,
          error: JSON.stringify(e)
        };
      }
    }

    return results;
  }

  return {
    deleteTransforms,
    startTransforms,
    stopTransforms
  };
}