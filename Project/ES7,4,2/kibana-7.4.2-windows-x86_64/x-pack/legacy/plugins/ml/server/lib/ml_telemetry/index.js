"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createMlTelemetry", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.createMlTelemetry;
  }
});
Object.defineProperty(exports, "getSavedObjectsClient", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.getSavedObjectsClient;
  }
});
Object.defineProperty(exports, "incrementFileDataVisualizerIndexCreationCount", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.incrementFileDataVisualizerIndexCreationCount;
  }
});
Object.defineProperty(exports, "storeMlTelemetry", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.storeMlTelemetry;
  }
});
Object.defineProperty(exports, "MlTelemetry", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.MlTelemetry;
  }
});
Object.defineProperty(exports, "MlTelemetrySavedObject", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.MlTelemetrySavedObject;
  }
});
Object.defineProperty(exports, "ML_TELEMETRY_DOC_ID", {
  enumerable: true,
  get: function () {
    return _ml_telemetry.ML_TELEMETRY_DOC_ID;
  }
});
Object.defineProperty(exports, "makeMlUsageCollector", {
  enumerable: true,
  get: function () {
    return _make_ml_usage_collector.makeMlUsageCollector;
  }
});

var _ml_telemetry = require("./ml_telemetry");

var _make_ml_usage_collector = require("./make_ml_usage_collector");