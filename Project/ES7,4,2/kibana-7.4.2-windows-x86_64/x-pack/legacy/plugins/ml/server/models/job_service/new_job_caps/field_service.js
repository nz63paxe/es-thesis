"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fieldServiceProvider = fieldServiceProvider;

var _lodash = require("lodash");

var _fields = require("../../../../common/types/fields");

var _field_types = require("../../../../common/constants/field_types");

var _aggregation_types = require("../../../../common/constants/aggregation_types");

var _rollup = require("./rollup");

var _aggregations = require("./aggregations");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const supportedTypes = [_field_types.ES_FIELD_TYPES.DATE, _field_types.ES_FIELD_TYPES.KEYWORD, _field_types.ES_FIELD_TYPES.TEXT, _field_types.ES_FIELD_TYPES.DOUBLE, _field_types.ES_FIELD_TYPES.INTEGER, _field_types.ES_FIELD_TYPES.FLOAT, _field_types.ES_FIELD_TYPES.LONG, _field_types.ES_FIELD_TYPES.BYTE, _field_types.ES_FIELD_TYPES.HALF_FLOAT, _field_types.ES_FIELD_TYPES.SCALED_FLOAT, _field_types.ES_FIELD_TYPES.SHORT, _field_types.ES_FIELD_TYPES.IP];

function fieldServiceProvider(indexPattern, isRollup, callWithRequest, request) {
  return new FieldsService(indexPattern, isRollup, callWithRequest, request);
}

class FieldsService {
  constructor(indexPattern, isRollup, callWithRequest, request) {
    _defineProperty(this, "_indexPattern", void 0);

    _defineProperty(this, "_isRollup", void 0);

    _defineProperty(this, "_callWithRequest", void 0);

    _defineProperty(this, "_request", void 0);

    this._indexPattern = indexPattern;
    this._isRollup = isRollup;
    this._callWithRequest = callWithRequest;
    this._request = request;
  }

  async loadFieldCaps() {
    return this._callWithRequest('fieldCaps', {
      index: this._indexPattern,
      fields: '*'
    });
  } // create field object from the results from _field_caps


  async createFields() {
    const fieldCaps = await this.loadFieldCaps();
    const fields = [];

    if (fieldCaps && fieldCaps.fields) {
      Object.keys(fieldCaps.fields).forEach(k => {
        const fc = fieldCaps.fields[k];
        const firstKey = Object.keys(fc)[0];

        if (firstKey !== undefined) {
          const field = fc[firstKey]; // add to the list of fields if the field type can be used by ML

          if (supportedTypes.includes(field.type) === true) {
            fields.push({
              id: k,
              name: k,
              type: field.type,
              aggregatable: field.aggregatable,
              aggs: []
            });
          }
        }
      });
    }

    return fields.sort((a, b) => a.id.localeCompare(b.id));
  } // public function to load fields from _field_caps and create a list
  // of aggregations and fields that can be used for an ML job
  // if the index is a rollup, the fields and aggs will be filtered
  // based on what is available in the rollup job
  // the _indexPattern will be replaced with a comma separated list
  // of index patterns from all of the rollup jobs


  async getData() {
    let rollupFields = {};

    if (this._isRollup) {
      const rollupService = await (0, _rollup.rollupServiceProvider)(this._indexPattern, this._callWithRequest, this._request);
      const rollupConfigs = await rollupService.getRollupJobs(); // if a rollup index has been specified, yet there are no
      // rollup configs, return with no results

      if (rollupConfigs === null) {
        return {
          aggs: [],
          fields: []
        };
      } else {
        rollupFields = combineAllRollupFields(rollupConfigs);
        this._indexPattern = rollupService.getIndexPattern();
      }
    }

    const aggs = (0, _lodash.cloneDeep)(_aggregations.aggregations);
    const fields = await this.createFields();
    return await combineFieldsAndAggs(fields, aggs, rollupFields);
  }

} // cross reference fields and aggs.
// fields contain a list of aggs that are compatible, and vice versa.


async function combineFieldsAndAggs(fields, aggs, rollupFields) {
  const keywordFields = getKeywordFields(fields);
  const numericalFields = getNumericalFields(fields);
  const ipFields = getIpFields(fields);
  const mix = mixFactory(rollupFields);
  aggs.forEach(a => {
    if (a.type === _fields.METRIC_AGG_TYPE) {
      switch (a.dslName) {
        case _aggregation_types.ES_AGGREGATION.COUNT:
          // count doesn't take any fields, so break here
          break;

        case _aggregation_types.ES_AGGREGATION.CARDINALITY:
          // distinct count (i.e. cardinality) takes keywords, ips
          // as well as numerical fields
          keywordFields.forEach(f => {
            mix(f, a);
          });
          ipFields.forEach(f => {
            mix(f, a);
          });
        // note, no break to fall through to add numerical fields.

        default:
          // all other aggs take numerical fields
          numericalFields.forEach(f => {
            mix(f, a);
          });
          break;
      }
    }
  });
  return {
    aggs: filterAggs(aggs),
    fields: filterFields(fields)
  };
} // remove fields that have no aggs associated to them


function filterFields(fields) {
  return fields.filter(f => f.aggs && f.aggs.length);
} // remove aggs that have no fields associated to them


function filterAggs(aggs) {
  return aggs.filter(a => a.fields && a.fields.length);
} // returns a mix function that is used to cross-reference aggs and fields.
// wrapped in a provider to allow filtering based on rollup job capabilities


function mixFactory(rollupFields) {
  const isRollup = Object.keys(rollupFields).length > 0;
  return function mix(field, agg) {
    if (isRollup === false || rollupFields[field.id] && rollupFields[field.id].find(f => f.agg === agg.dslName)) {
      if (field.aggs !== undefined) {
        field.aggs.push(agg);
      }

      if (agg.fields !== undefined) {
        agg.fields.push(field);
      }
    }
  };
}

function combineAllRollupFields(rollupConfigs) {
  const rollupFields = {};
  rollupConfigs.forEach(conf => {
    Object.keys(conf.fields).forEach(fieldName => {
      if (rollupFields[fieldName] === undefined) {
        rollupFields[fieldName] = conf.fields[fieldName];
      } else {
        const aggs = conf.fields[fieldName];
        aggs.forEach(agg => {
          if (rollupFields[fieldName].find(f => f.agg === agg.agg) === null) {
            rollupFields[fieldName].push(agg);
          }
        });
      }
    });
  });
  return rollupFields;
}

function getKeywordFields(fields) {
  return fields.filter(f => f.type === _field_types.ES_FIELD_TYPES.KEYWORD);
}

function getIpFields(fields) {
  return fields.filter(f => f.type === _field_types.ES_FIELD_TYPES.IP);
}

function getNumericalFields(fields) {
  return fields.filter(f => f.type === _field_types.ES_FIELD_TYPES.LONG || f.type === _field_types.ES_FIELD_TYPES.INTEGER || f.type === _field_types.ES_FIELD_TYPES.SHORT || f.type === _field_types.ES_FIELD_TYPES.BYTE || f.type === _field_types.ES_FIELD_TYPES.DOUBLE || f.type === _field_types.ES_FIELD_TYPES.FLOAT || f.type === _field_types.ES_FIELD_TYPES.HALF_FLOAT || f.type === _field_types.ES_FIELD_TYPES.SCALED_FLOAT);
}