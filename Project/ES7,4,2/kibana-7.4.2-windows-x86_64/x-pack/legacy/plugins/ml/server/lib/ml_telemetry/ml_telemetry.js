"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMlTelemetry = createMlTelemetry;
exports.storeMlTelemetry = storeMlTelemetry;
exports.getSavedObjectsClient = getSavedObjectsClient;
exports.incrementFileDataVisualizerIndexCreationCount = incrementFileDataVisualizerIndexCreationCount;
exports.ML_TELEMETRY_DOC_ID = void 0;

var _call_with_internal_user_factory = require("../../client/call_with_internal_user_factory");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ML_TELEMETRY_DOC_ID = 'ml-telemetry';
exports.ML_TELEMETRY_DOC_ID = ML_TELEMETRY_DOC_ID;

function createMlTelemetry(count = 0) {
  return {
    file_data_visualizer: {
      index_creation_count: count
    }
  };
} // savedObjects


function storeMlTelemetry(elasticsearchPlugin, savedObjects, mlTelemetry) {
  const savedObjectsClient = getSavedObjectsClient(elasticsearchPlugin, savedObjects);
  savedObjectsClient.create('ml-telemetry', mlTelemetry, {
    id: ML_TELEMETRY_DOC_ID,
    overwrite: true
  });
} // needs savedObjects and elasticsearchPlugin


function getSavedObjectsClient(elasticsearchPlugin, savedObjects) {
  const {
    SavedObjectsClient,
    getSavedObjectsRepository
  } = savedObjects;
  const callWithInternalUser = (0, _call_with_internal_user_factory.callWithInternalUserFactory)(elasticsearchPlugin);
  const internalRepository = getSavedObjectsRepository(callWithInternalUser);
  return new SavedObjectsClient(internalRepository);
}

async function incrementFileDataVisualizerIndexCreationCount(elasticsearchPlugin, savedObjects) {
  const savedObjectsClient = getSavedObjectsClient(elasticsearchPlugin, savedObjects);

  try {
    const {
      attributes
    } = await savedObjectsClient.get('telemetry', 'telemetry');

    if (attributes.enabled === false) {
      return;
    }
  } catch (error) {
    // if we aren't allowed to get the telemetry document,
    // we assume we couldn't opt in to telemetry and won't increment the index count.
    return;
  }

  let indicesCount = 1;

  try {
    const {
      attributes
    } = await savedObjectsClient.get('ml-telemetry', ML_TELEMETRY_DOC_ID);
    indicesCount = attributes.file_data_visualizer.index_creation_count + 1;
  } catch (e) {
    /* silently fail, this will happen if the saved object doesn't exist yet. */
  }

  const mlTelemetry = createMlTelemetry(indicesCount);
  storeMlTelemetry(elasticsearchPlugin, savedObjects, mlTelemetry);
}