"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "deserializeCluster", {
  enumerable: true,
  get: function () {
    return _cluster_serialization.deserializeCluster;
  }
});
Object.defineProperty(exports, "serializeCluster", {
  enumerable: true,
  get: function () {
    return _cluster_serialization.serializeCluster;
  }
});
exports.API_BASE_PATH = exports.PLUGIN = void 0;

var _i18n = require("@kbn/i18n");

var _constants = require("../../../common/constants");

var _cluster_serialization = require("./cluster_serialization");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const PLUGIN = {
  ID: 'remote_clusters',
  // Remote Clusters are used in both CCS and CCR, and CCS is available for all licenses.
  MINIMUM_LICENSE_REQUIRED: _constants.LICENSE_TYPE_BASIC,
  getI18nName: () => {
    return _i18n.i18n.translate('xpack.remoteClusters.appName', {
      defaultMessage: 'Remote Clusters'
    });
  }
};
exports.PLUGIN = PLUGIN;
const API_BASE_PATH = '/api/remote_clusters';
exports.API_BASE_PATH = API_BASE_PATH;