"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _common = require("./common");

var _api = require("./server/routes/api");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class Plugin {
  setup(core) {
    const {
      http: {
        createRouter,
        isEsError
      }
    } = core;
    const router = createRouter(_common.API_BASE_PATH); // Register routes.

    (0, _api.registerGetRoute)(router);
    (0, _api.registerAddRoute)(router);
    (0, _api.registerUpdateRoute)(router);
    (0, _api.registerDeleteRoute)(router, isEsError);
  }

}

exports.Plugin = Plugin;