"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.remoteClusters = remoteClusters;

var _path = require("path");

var _common = require("./common");

var _plugin = require("./plugin");

var _shim = require("./shim");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function remoteClusters(kibana) {
  return new kibana.Plugin({
    id: _common.PLUGIN.ID,
    configPrefix: 'xpack.remote_clusters',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    // xpack_main is required for license checking.
    require: ['kibana', 'elasticsearch', 'xpack_main', 'index_management'],
    uiExports: {
      styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),
      managementSections: ['plugins/remote_clusters'],

      injectDefaultVars(server) {
        const config = server.config();
        return {
          remoteClustersUiEnabled: config.get('xpack.remote_clusters.ui.enabled')
        };
      }

    },

    config(Joi) {
      return Joi.object({
        // display menu item
        ui: Joi.object({
          enabled: Joi.boolean().default(true)
        }).default(),
        // enable plugin
        enabled: Joi.boolean().default(true)
      }).default();
    },

    isEnabled(config) {
      return config.get('xpack.remote_clusters.enabled') && config.get('xpack.index_management.enabled');
    },

    init(server) {
      const {
        coreSetup,
        pluginsSetup: {
          license: {
            registerLicenseChecker
          }
        }
      } = (0, _shim.createShim)(server, _common.PLUGIN.ID);
      const remoteClustersPlugin = new _plugin.Plugin(); // Set up plugin.

      remoteClustersPlugin.setup(coreSetup);
      registerLicenseChecker(server, _common.PLUGIN.ID, _common.PLUGIN.getI18nName(), _common.PLUGIN.MINIMUM_LICENSE_REQUIRED);
    }

  });
}