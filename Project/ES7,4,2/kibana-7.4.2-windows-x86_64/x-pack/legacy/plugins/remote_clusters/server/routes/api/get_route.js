"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllHandler = exports.register = void 0;

var _lodash = require("lodash");

var _cluster_serialization = require("../../../common/cluster_serialization");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const register = router => {
  router.get('', getAllHandler);
}; // GET '/api/remote_clusters'


exports.register = register;

const getAllHandler = async (req, callWithRequest) => {
  const clusterSettings = await callWithRequest('cluster.getSettings');
  const transientClusterNames = Object.keys((0, _lodash.get)(clusterSettings, `transient.cluster.remote`) || {});
  const persistentClusterNames = Object.keys((0, _lodash.get)(clusterSettings, `persistent.cluster.remote`) || {});
  const clustersByName = await callWithRequest('cluster.remoteInfo');
  const clusterNames = clustersByName && Object.keys(clustersByName) || [];
  return clusterNames.map(clusterName => {
    const cluster = clustersByName[clusterName];
    const isTransient = transientClusterNames.includes(clusterName);
    const isPersistent = persistentClusterNames.includes(clusterName); // If the cluster hasn't been stored in the cluster state, then it's defined by the
    // node's config file.

    const isConfiguredByNode = !isTransient && !isPersistent;
    return { ...(0, _cluster_serialization.deserializeCluster)(clusterName, cluster),
      isConfiguredByNode
    };
  });
};

exports.getAllHandler = getAllHandler;