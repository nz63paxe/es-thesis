"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createDeleteHandler = exports.register = void 0;

var _lodash = require("lodash");

var _create_router = require("../../../../../server/lib/create_router");

var _cluster_serialization = require("../../../common/cluster_serialization");

var _does_cluster_exist = require("../../lib/does_cluster_exist");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const register = (router, isEsError) => {
  router.delete('/{nameOrNames}', createDeleteHandler(isEsError));
};

exports.register = register;

const createDeleteHandler = isEsError => {
  const deleteHandler = async (req, callWithRequest) => {
    const {
      nameOrNames
    } = req.params;
    const names = nameOrNames.split(',');
    const itemsDeleted = [];
    const errors = []; // Validator that returns an error if the remote cluster does not exist.

    const validateClusterDoesExist = async name => {
      try {
        const existingCluster = await (0, _does_cluster_exist.doesClusterExist)(callWithRequest, name);

        if (!existingCluster) {
          return (0, _create_router.wrapCustomError)(new Error('There is no remote cluster with that name.'), 404);
        }
      } catch (error) {
        return (0, _create_router.wrapCustomError)(error, 400);
      }
    }; // Send the request to delete the cluster and return an error if it could not be deleted.


    const sendRequestToDeleteCluster = async name => {
      try {
        const body = (0, _cluster_serialization.serializeCluster)({
          name
        });
        const response = await callWithRequest('cluster.putSettings', {
          body
        });
        const acknowledged = (0, _lodash.get)(response, 'acknowledged');
        const cluster = (0, _lodash.get)(response, `persistent.cluster.remote.${name}`);

        if (acknowledged && !cluster) {
          return null;
        } // If for some reason the ES response still returns the cluster information,
        // return an error. This shouldn't happen.


        return (0, _create_router.wrapCustomError)(new Error('Unable to delete cluster, information still returned from ES.'), 400);
      } catch (error) {
        if (isEsError(error)) {
          return (0, _create_router.wrapEsError)(error);
        }

        return (0, _create_router.wrapUnknownError)(error);
      }
    };

    const deleteCluster = async clusterName => {
      // Validate that the cluster exists.
      let error = await validateClusterDoesExist(clusterName);

      if (!error) {
        // Delete the cluster.
        error = await sendRequestToDeleteCluster(clusterName);
      }

      if (error) {
        errors.push({
          name: clusterName,
          error
        });
      } else {
        itemsDeleted.push(clusterName);
      }
    }; // Delete all our cluster in parallel.


    await Promise.all(names.map(deleteCluster));
    return {
      itemsDeleted,
      errors
    };
  };

  return deleteHandler;
};

exports.createDeleteHandler = createDeleteHandler;