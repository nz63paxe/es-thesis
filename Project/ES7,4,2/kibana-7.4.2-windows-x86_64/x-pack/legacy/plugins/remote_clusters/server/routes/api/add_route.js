"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addHandler = exports.register = void 0;

var _lodash = require("lodash");

var _create_router = require("../../../../../server/lib/create_router");

var _cluster_serialization = require("../../../common/cluster_serialization");

var _does_cluster_exist = require("../../lib/does_cluster_exist");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const register = router => {
  router.post('', addHandler);
};

exports.register = register;

const addHandler = async (req, callWithRequest) => {
  const {
    name,
    seeds,
    skipUnavailable
  } = req.payload; // Check if cluster already exists.

  const existingCluster = await (0, _does_cluster_exist.doesClusterExist)(callWithRequest, name);

  if (existingCluster) {
    const conflictError = (0, _create_router.wrapCustomError)(new Error('There is already a remote cluster with that name.'), 409);
    throw conflictError;
  }

  const addClusterPayload = (0, _cluster_serialization.serializeCluster)({
    name,
    seeds,
    skipUnavailable
  });
  const response = await callWithRequest('cluster.putSettings', {
    body: addClusterPayload
  });
  const acknowledged = (0, _lodash.get)(response, 'acknowledged');
  const cluster = (0, _lodash.get)(response, `persistent.cluster.remote.${name}`);

  if (acknowledged && cluster) {
    return {
      acknowledged: true
    };
  } // If for some reason the ES response did not acknowledge,
  // return an error. This shouldn't happen.


  throw (0, _create_router.wrapCustomError)(new Error('Unable to add cluster, no response returned from ES.'), 400);
};

exports.addHandler = addHandler;