"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createShim = createShim;

var _create_router = require("../../server/lib/create_router");

var _register_license_checker = require("../../server/lib/register_license_checker");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createShim(server, pluginId) {
  return {
    coreSetup: {
      http: {
        createRouter: basePath => (0, _create_router.createRouter)(server, pluginId, basePath),
        isEsError: (0, _create_router.isEsErrorFactory)(server)
      }
    },
    pluginsSetup: {
      license: {
        registerLicenseChecker: _register_license_checker.registerLicenseChecker
      }
    }
  };
}