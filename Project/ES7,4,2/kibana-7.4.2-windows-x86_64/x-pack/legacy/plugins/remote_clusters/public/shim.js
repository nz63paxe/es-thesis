"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createShim = createShim;

var _new_platform = require("ui/new_platform");

var _management = require("ui/management");

var _notify = require("ui/notify");

var _documentation_links = require("ui/documentation_links");

var _public = require("../../../../../src/legacy/core_plugins/ui_metric/public");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createShim() {
  var _npStart$core = _new_platform.npStart.core,
      chrome = _npStart$core.chrome,
      i18n = _npStart$core.i18n,
      notifications = _npStart$core.notifications,
      http = _npStart$core.http,
      injectedMetadata = _npStart$core.injectedMetadata;
  return {
    coreStart: {
      chrome: chrome,
      i18n: i18n,
      notifications: notifications,
      fatalError: _notify.fatalError,
      injectedMetadata: injectedMetadata,
      http: http,
      documentation: {
        elasticWebsiteUrl: _documentation_links.ELASTIC_WEBSITE_URL,
        docLinkVersion: _documentation_links.DOC_LINK_VERSION
      }
    },
    pluginsStart: {
      management: {
        getSection: _management.management.getSection.bind(_management.management),
        breadcrumb: _management.MANAGEMENT_BREADCRUMB
      },
      uiMetric: {
        createUiStatsReporter: _public.createUiStatsReporter
      }
    }
  };
}