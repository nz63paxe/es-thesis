"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = init;
exports.getFullPath = getFullPath;
exports.sendPost = sendPost;
exports.sendGet = sendGet;
exports.sendPut = sendPut;
exports.sendDelete = sendDelete;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var _httpClient;

var _prependBasePath;

function init(httpClient, prependBasePath) {
  _httpClient = httpClient;
  _prependBasePath = prependBasePath;
}

function getFullPath(path) {
  var apiPrefix = _prependBasePath('/api/remote_clusters');

  if (path) {
    return "".concat(apiPrefix, "/").concat(path);
  }

  return apiPrefix;
}

function sendPost(path, payload) {
  return _httpClient.post(getFullPath(path), payload);
}

function sendGet(path) {
  return _httpClient.get(getFullPath(path));
}

function sendPut(path, payload) {
  return _httpClient.put(getFullPath(path), payload);
}

function sendDelete(path) {
  return _httpClient.delete(getFullPath(path));
}