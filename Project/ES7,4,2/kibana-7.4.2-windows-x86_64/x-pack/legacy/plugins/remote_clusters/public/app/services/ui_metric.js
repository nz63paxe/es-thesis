"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = init;
exports.trackUserRequest = trackUserRequest;
Object.defineProperty(exports, "METRIC_TYPE", {
  enumerable: true,
  get: function get() {
    return _public.METRIC_TYPE;
  }
});
exports.trackUiMetric = void 0;

var _constants = require("../constants");

var _public = require("../../../../../../../src/legacy/core_plugins/ui_metric/public");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var trackUiMetric;
exports.trackUiMetric = trackUiMetric;

function init(getReporter) {
  exports.trackUiMetric = trackUiMetric = getReporter(_constants.UIM_APP_NAME);
}
/**
 * Transparently return provided request Promise, while allowing us to track
 * a successful completion of the request.
 */


function trackUserRequest(request, eventName) {
  // Only track successful actions.
  return request.then(function (response) {
    trackUiMetric(_public.METRIC_TYPE.COUNT, eventName); // We return the response immediately without waiting for the tracking request to resolve,
    // to avoid adding additional latency.

    return response;
  });
}