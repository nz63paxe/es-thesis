"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FeatureRegistry = exports.uiCapabilitiesRegex = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Each feature gets its own property on the UICapabilities object,
// but that object has a few built-in properties which should not be overwritten.
const prohibitedFeatureIds = ['catalogue', 'management', 'navLinks'];
const featurePrivilegePartRegex = /^[a-zA-Z0-9_-]+$/;
const managementSectionIdRegex = /^[a-zA-Z0-9_-]+$/;
const uiCapabilitiesRegex = /^[a-zA-Z0-9:_-]+$/;
exports.uiCapabilitiesRegex = uiCapabilitiesRegex;

const managementSchema = _joi.default.object().pattern(managementSectionIdRegex, _joi.default.array().items(_joi.default.string().regex(uiCapabilitiesRegex)));

const catalogueSchema = _joi.default.array().items(_joi.default.string().regex(uiCapabilitiesRegex));

const privilegeSchema = _joi.default.object({
  excludeFromBasePrivileges: _joi.default.boolean(),
  management: managementSchema,
  catalogue: catalogueSchema,
  api: _joi.default.array().items(_joi.default.string()),
  app: _joi.default.array().items(_joi.default.string()),
  savedObject: _joi.default.object({
    all: _joi.default.array().items(_joi.default.string()).required(),
    read: _joi.default.array().items(_joi.default.string()).required()
  }).required(),
  ui: _joi.default.array().items(_joi.default.string().regex(uiCapabilitiesRegex)).required()
});

const schema = _joi.default.object({
  id: _joi.default.string().regex(featurePrivilegePartRegex).invalid(...prohibitedFeatureIds).required(),
  name: _joi.default.string().required(),
  excludeFromBasePrivileges: _joi.default.boolean(),
  validLicenses: _joi.default.array().items(_joi.default.string().valid('basic', 'standard', 'gold', 'platinum')),
  icon: _joi.default.string(),
  description: _joi.default.string(),
  navLinkId: _joi.default.string().regex(uiCapabilitiesRegex),
  app: _joi.default.array().items(_joi.default.string()).required(),
  management: managementSchema,
  catalogue: catalogueSchema,
  privileges: _joi.default.object({
    all: privilegeSchema,
    read: privilegeSchema
  }).required(),
  privilegesTooltip: _joi.default.string(),
  reserved: _joi.default.object({
    privilege: privilegeSchema.required(),
    description: _joi.default.string().required()
  })
});

class FeatureRegistry {
  constructor() {
    _defineProperty(this, "locked", false);

    _defineProperty(this, "features", {});
  }

  register(feature) {
    if (this.locked) {
      throw new Error(`Features are locked, can't register new features`);
    }

    validateFeature(feature);

    if (feature.id in this.features) {
      throw new Error(`Feature with id ${feature.id} is already registered.`);
    }

    const featureCopy = (0, _lodash.cloneDeep)(feature);
    this.features[feature.id] = applyAutomaticPrivilegeGrants(featureCopy);
  }

  getAll() {
    this.locked = true;
    return (0, _lodash.cloneDeep)(Object.values(this.features));
  }

}

exports.FeatureRegistry = FeatureRegistry;

function validateFeature(feature) {
  const validateResult = _joi.default.validate(feature, schema);

  if (validateResult.error) {
    throw validateResult.error;
  } // the following validation can't be enforced by the Joi schema, since it'd require us looking "up" the object graph for the list of valid value, which they explicitly forbid.


  const {
    app = [],
    management = {},
    catalogue = []
  } = feature;
  const privilegeEntries = [...Object.entries(feature.privileges)];

  if (feature.reserved) {
    privilegeEntries.push(['reserved', feature.reserved.privilege]);
  }

  privilegeEntries.forEach(([privilegeId, privilegeDefinition]) => {
    if (!privilegeDefinition) {
      throw new Error('Privilege definition may not be null or undefined');
    }

    const unknownAppEntries = (0, _lodash.difference)(privilegeDefinition.app || [], app);

    if (unknownAppEntries.length > 0) {
      throw new Error(`Feature privilege ${feature.id}.${privilegeId} has unknown app entries: ${unknownAppEntries.join(', ')}`);
    }

    const unknownCatalogueEntries = (0, _lodash.difference)(privilegeDefinition.catalogue || [], catalogue);

    if (unknownCatalogueEntries.length > 0) {
      throw new Error(`Feature privilege ${feature.id}.${privilegeId} has unknown catalogue entries: ${unknownCatalogueEntries.join(', ')}`);
    }

    Object.entries(privilegeDefinition.management || {}).forEach(([managementSectionId, managementEntry]) => {
      if (!management[managementSectionId]) {
        throw new Error(`Feature privilege ${feature.id}.${privilegeId} has unknown management section: ${managementSectionId}`);
      }

      const unknownSectionEntries = (0, _lodash.difference)(managementEntry, management[managementSectionId]);

      if (unknownSectionEntries.length > 0) {
        throw new Error(`Feature privilege ${feature.id}.${privilegeId} has unknown management entries for section ${managementSectionId}: ${unknownSectionEntries.join(', ')}`);
      }
    });
  });
}

function applyAutomaticPrivilegeGrants(feature) {
  const {
    all: allPrivilege,
    read: readPrivilege
  } = feature.privileges;
  const reservedPrivilege = feature.reserved ? feature.reserved.privilege : null;
  applyAutomaticAllPrivilegeGrants(allPrivilege, reservedPrivilege);
  applyAutomaticReadPrivilegeGrants(readPrivilege);
  return feature;
}

function applyAutomaticAllPrivilegeGrants(...allPrivileges) {
  allPrivileges.forEach(allPrivilege => {
    if (allPrivilege) {
      allPrivilege.savedObject.all = (0, _lodash.uniq)([...allPrivilege.savedObject.all, 'telemetry']);
      allPrivilege.savedObject.read = (0, _lodash.uniq)([...allPrivilege.savedObject.read, 'config', 'url']);
    }
  });
}

function applyAutomaticReadPrivilegeGrants(...readPrivileges) {
  readPrivileges.forEach(readPrivilege => {
    if (readPrivilege) {
      readPrivilege.savedObject.read = (0, _lodash.uniq)([...readPrivilege.savedObject.read, 'config', 'url']);
    }
  });
}