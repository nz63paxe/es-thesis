"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Feature", {
  enumerable: true,
  get: function () {
    return _feature_registry.Feature;
  }
});
Object.defineProperty(exports, "FeatureKibanaPrivileges", {
  enumerable: true,
  get: function () {
    return _feature_registry.FeatureKibanaPrivileges;
  }
});
Object.defineProperty(exports, "FeatureRegistry", {
  enumerable: true,
  get: function () {
    return _feature_registry.FeatureRegistry;
  }
});
Object.defineProperty(exports, "FeatureWithAllOrReadPrivileges", {
  enumerable: true,
  get: function () {
    return _feature_registry.FeatureWithAllOrReadPrivileges;
  }
});
Object.defineProperty(exports, "uiCapabilitiesRegex", {
  enumerable: true,
  get: function () {
    return _feature_registry.uiCapabilitiesRegex;
  }
});

var _feature_registry = require("./feature_registry");