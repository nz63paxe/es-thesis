"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "featuresRoute", {
  enumerable: true,
  get: function () {
    return _features.featuresRoute;
  }
});

var _features = require("./features");