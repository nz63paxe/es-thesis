"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "TelemetryOptInProvider", {
  enumerable: true,
  get: function get() {
    return _telemetry_opt_in.TelemetryOptInProvider;
  }
});

var _telemetry_opt_in = require("../../../../../../src/legacy/core_plugins/kibana/public/home/telemetry_opt_in");