"use strict";

var _modules = require("ui/modules");

var _path = require("plugins/xpack_main/services/path");

var _new_platform = require("ui/new_platform");

var _telemetry = require("./telemetry");

var _fetch_telemetry = require("./fetch_telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore
// @ts-ignore
function telemetryInit($injector) {
  var $http = $injector.get('$http');

  var telemetryEnabled = _new_platform.npStart.core.injectedMetadata.getInjectedVar('telemetryEnabled');

  if (telemetryEnabled) {
    // no telemetry for non-logged in users
    if (_path.Path.isUnauthenticated()) {
      return;
    }

    var sender = new _telemetry.Telemetry($injector, function () {
      return (0, _fetch_telemetry.fetchTelemetry)($http);
    });
    sender.start();
  }
}

_modules.uiModules.get('telemetry/hacks').run(telemetryInit);