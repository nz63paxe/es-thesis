"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isFileReadable = isFileReadable;
exports.readTelemetryFile = readTelemetryFile;
exports.createTelemetryUsageCollector = createTelemetryUsageCollector;
exports.MAX_FILE_SIZE = void 0;

var _fs = require("fs");

var _jsYaml = require("js-yaml");

var _path = require("path");

var _ensure_deep_object = require("./ensure_deep_object");

var _get_xpack_config_with_deprecated = require("../../../common/get_xpack_config_with_deprecated");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// look for telemetry.yml in the same places we expect kibana.yml

/**
 * The maximum file size before we ignore it (note: this limit is arbitrary).
 */
const MAX_FILE_SIZE = 10 * 1024; // 10 KB

exports.MAX_FILE_SIZE = MAX_FILE_SIZE;

/**
 * Determine if the supplied `path` is readable.
 *
 * @param path The possible path where a config file may exist.
 * @returns `true` if the file should be used.
 */
function isFileReadable(path) {
  try {
    (0, _fs.accessSync)(path, _fs.constants.R_OK); // ignore files above the limit

    const stats = (0, _fs.statSync)(path);
    return stats.size <= MAX_FILE_SIZE;
  } catch (e) {
    return false;
  }
}
/**
 * Load the `telemetry.yml` file, if it exists, and return its contents as
 * a JSON object.
 *
 * @param configPath The config file path.
 * @returns The unmodified JSON object if the file exists and is a valid YAML file.
 */


async function readTelemetryFile(path) {
  try {
    if (isFileReadable(path)) {
      const yaml = (0, _fs.readFileSync)(path);
      const data = (0, _jsYaml.safeLoad)(yaml.toString()); // don't bother returning empty objects

      if (Object.keys(data).length) {
        // ensure { "a.b": "value" } becomes { "a": { "b": "value" } }
        return (0, _ensure_deep_object.ensureDeepObject)(data);
      }
    }
  } catch (e) {// ignored
  }

  return undefined;
}
/**
 * Create a usage collector that provides the `telemetry.yml` data as a
 * `static_telemetry` object.
 *
 * Loading of the file is done lazily and on-demand. This avoids hanging
 * onto the data in memory unnecessarily, as well as allows it to be
 * updated out-of-process without having to restart Kibana.
 *
 * @param server The Kibana server instance.
 * @return `UsageCollector` that provides the `static_telemetry` described.
 */


function createTelemetryUsageCollector(server) {
  return server.usage.collectorSet.makeUsageCollector({
    type: 'static_telemetry',
    isReady: () => true,
    fetch: async () => {
      const config = server.config();
      const configPath = (0, _get_xpack_config_with_deprecated.getXpackConfigWithDeprecated)(config, 'telemetry.config');
      const telemetryPath = (0, _path.join)((0, _path.dirname)(configPath), 'telemetry.yml');
      return await readTelemetryFile(telemetryPath);
    }
  });
}