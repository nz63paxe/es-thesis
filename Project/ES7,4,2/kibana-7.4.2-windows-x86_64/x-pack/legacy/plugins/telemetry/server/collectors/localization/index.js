"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createLocalizationUsageCollector", {
  enumerable: true,
  get: function () {
    return _telemetry_localization_collector.createLocalizationUsageCollector;
  }
});

var _telemetry_localization_collector = require("./telemetry_localization_collector");