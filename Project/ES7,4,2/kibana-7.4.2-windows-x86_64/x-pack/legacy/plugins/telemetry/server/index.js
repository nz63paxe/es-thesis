"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "getTelemetryOptIn", {
  enumerable: true,
  get: function () {
    return _get_telemetry_opt_in.getTelemetryOptIn;
  }
});
exports.constants = exports.telemetryPlugin = void 0;

var _plugin = require("./plugin");

var constants = _interopRequireWildcard(require("../common/constants"));

exports.constants = constants;

var _get_telemetry_opt_in = require("./get_telemetry_opt_in");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const telemetryPlugin = initializerContext => new _plugin.TelemetryPlugin();

exports.telemetryPlugin = telemetryPlugin;