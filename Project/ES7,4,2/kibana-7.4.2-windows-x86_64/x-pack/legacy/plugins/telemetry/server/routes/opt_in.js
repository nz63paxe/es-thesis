"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerOptInRoutes = registerOptInRoutes;

var _joi = _interopRequireDefault(require("joi"));

var _boom = require("boom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerOptInRoutes(core) {
  const {
    server
  } = core.http;
  server.route({
    method: 'POST',
    path: '/api/telemetry/v2/optIn',
    options: {
      validate: {
        payload: _joi.default.object({
          enabled: _joi.default.bool().required()
        })
      }
    },
    handler: async (req, h) => {
      const savedObjectsClient = req.getSavedObjectsClient();

      try {
        await savedObjectsClient.create('telemetry', {
          enabled: req.payload.enabled
        }, {
          id: 'telemetry',
          overwrite: true
        });
      } catch (err) {
        return (0, _boom.boomify)(err);
      }

      return h.response({}).code(200);
    }
  });
}