"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerTelemetryDataRoutes = registerTelemetryDataRoutes;

var _joi = _interopRequireDefault(require("joi"));

var _boom = require("boom");

var _collectors = require("../collectors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerTelemetryDataRoutes(core) {
  const {
    server
  } = core.http;
  server.route({
    method: 'POST',
    path: '/api/telemetry/v2/clusters/_stats',
    config: {
      validate: {
        payload: _joi.default.object({
          unencrypted: _joi.default.bool(),
          timeRange: _joi.default.object({
            min: _joi.default.date().required(),
            max: _joi.default.date().required()
          }).required()
        })
      }
    },
    handler: async (req, h) => {
      const config = req.server.config();
      const start = req.payload.timeRange.min;
      const end = req.payload.timeRange.max;
      const unencrypted = req.payload.unencrypted;
      const isDev = config.get('env.dev');

      try {
        const usageData = await (0, _collectors.getStats)(req, config, start, end, unencrypted);
        if (unencrypted) return usageData;
        return (0, _collectors.encryptTelemetry)(usageData, isDev);
      } catch (err) {
        if (isDev) {
          // don't ignore errors when running in dev mode
          return (0, _boom.boomify)(err, {
            statusCode: err.status
          });
        } else {
          const statusCode = unencrypted && err.status === 403 ? 403 : 200; // ignore errors and return empty set

          return h.response([]).code(statusCode);
        }
      }
    }
  });
}