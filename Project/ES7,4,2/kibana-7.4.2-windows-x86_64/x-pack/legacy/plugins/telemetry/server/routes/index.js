"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerRoutes = registerRoutes;

var _opt_in = require("./opt_in");

var _telemetry_stats = require("./telemetry_stats");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerRoutes(core) {
  (0, _opt_in.registerOptInRoutes)(core);
  (0, _telemetry_stats.registerTelemetryDataRoutes)(core);
}