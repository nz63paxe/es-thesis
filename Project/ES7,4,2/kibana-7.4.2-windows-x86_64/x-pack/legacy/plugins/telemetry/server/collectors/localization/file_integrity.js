"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getIntegrityHashes = getIntegrityHashes;
exports.getIntegrityHash = getIntegrityHash;

var _crypto = require("crypto");

var fs = _interopRequireWildcard(require("fs"));

var _lodash = require("lodash");

var stream = _interopRequireWildcard(require("stream"));

var util = _interopRequireWildcard(require("util"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const pipeline = util.promisify(stream.pipeline);

async function getIntegrityHashes(filepaths) {
  const hashes = await Promise.all(filepaths.map(getIntegrityHash));
  return (0, _lodash.zipObject)(filepaths, hashes);
}

async function getIntegrityHash(filepath) {
  try {
    const output = (0, _crypto.createHash)('md5');
    await pipeline(fs.createReadStream(filepath), output);
    const data = output.read();

    if (data instanceof Buffer) {
      return data.toString('hex');
    }

    return data;
  } catch (err) {
    return null;
  }
}