"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getKID = getKID;
exports.encryptTelemetry = encryptTelemetry;

var _requestCrypto = require("@elastic/request-crypto");

var _telemetry_jwks = require("./telemetry_jwks");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getKID(isProd = false) {
  return isProd ? 'kibana' : 'kibana_dev';
}

async function encryptTelemetry(payload, isProd = false) {
  const kid = getKID(isProd);
  const encryptor = await (0, _requestCrypto.createRequestEncryptor)(_telemetry_jwks.telemetryJWKS);
  const clusters = [].concat(payload);
  return Promise.all(clusters.map(cluster => encryptor.encrypt(kid, cluster)));
}