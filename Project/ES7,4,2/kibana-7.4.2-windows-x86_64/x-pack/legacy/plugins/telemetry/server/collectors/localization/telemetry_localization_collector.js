"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTranslationCount = getTranslationCount;
exports.createCollectorFetch = createCollectorFetch;
exports.createLocalizationUsageCollector = createLocalizationUsageCollector;

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _file_integrity = require("./file_integrity");

var _constants = require("../../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getTranslationCount(loader, locale) {
  const translations = await loader.getTranslationsByLocale(locale);
  return (0, _lodash.size)(translations.messages);
}

function createCollectorFetch(server) {
  return async function fetchUsageStats() {
    const config = server.config();
    const locale = config.get('i18n.locale');
    const translationFilePaths = server.getTranslationsFilePaths();
    const [labelsCount, integrities] = await Promise.all([getTranslationCount(_i18n.i18nLoader, locale), (0, _file_integrity.getIntegrityHashes)(translationFilePaths)]);
    return {
      locale,
      integrities,
      labelsCount
    };
  };
}
/*
 * @param {Object} server
 * @return {Object} kibana usage stats type collection object
 */


function createLocalizationUsageCollector(server) {
  const {
    collectorSet
  } = server.usage;
  return collectorSet.makeUsageCollector({
    type: _constants.KIBANA_LOCALIZATION_STATS_TYPE,
    isReady: () => true,
    fetch: createCollectorFetch(server)
  });
}