"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createUiMetricUsageCollector", {
  enumerable: true,
  get: function () {
    return _telemetry_ui_metric_collector.createUiMetricUsageCollector;
  }
});

var _telemetry_ui_metric_collector = require("./telemetry_ui_metric_collector");