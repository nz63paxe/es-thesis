"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createTelemetryUsageCollector", {
  enumerable: true,
  get: function () {
    return _telemetry_usage_collector.createTelemetryUsageCollector;
  }
});

var _telemetry_usage_collector = require("./telemetry_usage_collector");