"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.telemetry = void 0;

var _path = require("path");

var _i18n = require("@kbn/i18n");

var _mappings = _interopRequireDefault(require("./mappings.json"));

var _constants = require("./common/constants");

var _get_xpack_config_with_deprecated = require("./common/get_xpack_config_with_deprecated");

var _server = require("./server");

var _collectors = require("./server/collectors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ENDPOINT_VERSION = 'v2';

const telemetry = kibana => {
  return new kibana.Plugin({
    id: 'telemetry',
    configPrefix: 'xpack.telemetry',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    require: ['elasticsearch'],

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
        // `config` is used internally and not intended to be set
        config: Joi.string().default(Joi.ref('$defaultConfigPath')),
        banner: Joi.boolean().default(true),
        url: Joi.when('$dev', {
          is: true,
          then: Joi.string().default(`https://telemetry-staging.elastic.co/xpack/${ENDPOINT_VERSION}/send`),
          otherwise: Joi.string().default(`https://telemetry.elastic.co/xpack/${ENDPOINT_VERSION}/send`)
        })
      }).default();
    },

    uiExports: {
      managementSections: ['plugins/telemetry/views/management'],
      uiSettingDefaults: {
        [_constants.CONFIG_TELEMETRY]: {
          name: _i18n.i18n.translate('xpack.telemetry.telemetryConfigTitle', {
            defaultMessage: 'Telemetry opt-in'
          }),
          description: (0, _constants.getConfigTelemetryDesc)(),
          value: false,
          readonly: true
        }
      },
      savedObjectSchemas: {
        telemetry: {
          isNamespaceAgnostic: true
        }
      },

      injectDefaultVars(server) {
        const config = server.config();
        return {
          telemetryUrl: (0, _get_xpack_config_with_deprecated.getXpackConfigWithDeprecated)(config, 'telemetry.url'),
          telemetryBanner: config.get('xpack.telemetry.banner'),
          telemetryOptedIn: null
        };
      },

      hacks: ['plugins/telemetry/hacks/telemetry_init', 'plugins/telemetry/hacks/telemetry_opt_in'],
      mappings: _mappings.default
    },

    init(server) {
      const initializerContext = {};
      const coreSetup = {
        http: {
          server
        }
      };
      (0, _server.telemetryPlugin)(initializerContext).setup(coreSetup); // register collectors

      server.usage.collectorSet.register((0, _collectors.createLocalizationUsageCollector)(server));
      server.usage.collectorSet.register((0, _collectors.createTelemetryUsageCollector)(server));
      server.usage.collectorSet.register((0, _collectors.createUiMetricUsageCollector)(server)); // expose

      server.expose('telemetryCollectionInterval', _constants.REPORT_INTERVAL_MS);
    }

  });
};

exports.telemetry = telemetry;