"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getXpackConfigWithDeprecated = getXpackConfigWithDeprecated;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getXpackConfigWithDeprecated(config, configPath) {
  const deprecatedConfig = config.get(`xpack.xpack_main.${configPath}`);

  if (typeof deprecatedConfig !== 'undefined') {
    return deprecatedConfig;
  }

  return config.get(`xpack.${configPath}`);
}