"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UI_METRIC_USAGE_TYPE = exports.TELEMETRY_QUERY_SOURCE = exports.KIBANA_LOCALIZATION_STATS_TYPE = exports.PRIVACY_STATEMENT_URL = exports.LOCALSTORAGE_KEY = exports.REPORT_INTERVAL_MS = exports.REPORTING_SYSTEM_ID = exports.LOGSTASH_SYSTEM_ID = exports.APM_SYSTEM_ID = exports.BEATS_SYSTEM_ID = exports.KIBANA_SYSTEM_ID = exports.getConfigTelemetryDesc = exports.CONFIG_TELEMETRY = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/*
 * config options opt into telemetry
 * @type {string}
 */
const CONFIG_TELEMETRY = 'telemetry:optIn';
/*
 * config description for opting into telemetry
 * @type {string}
 */

exports.CONFIG_TELEMETRY = CONFIG_TELEMETRY;

const getConfigTelemetryDesc = () => {
  return _i18n.i18n.translate('xpack.telemetry.telemetryConfigDescription', {
    defaultMessage: 'Help us improve the Elastic Stack by providing usage statistics for basic features. We will not share this data outside of Elastic.'
  });
};
/**
 * The name of the Kibana System ID used to publish and look up Kibana stats through the Monitoring system.
 * @type {string}
 */


exports.getConfigTelemetryDesc = getConfigTelemetryDesc;
const KIBANA_SYSTEM_ID = 'kibana';
/**
 * The name of the Beats System ID used to publish and look up Beats stats through the Monitoring system.
 * @type {string}
 */

exports.KIBANA_SYSTEM_ID = KIBANA_SYSTEM_ID;
const BEATS_SYSTEM_ID = 'beats';
/**
 * The name of the Apm System ID used to publish and look up Apm stats through the Monitoring system.
 * @type {string}
 */

exports.BEATS_SYSTEM_ID = BEATS_SYSTEM_ID;
const APM_SYSTEM_ID = 'beats';
/**
 * The name of the Kibana System ID used to look up Logstash stats through the Monitoring system.
 * @type {string}
 */

exports.APM_SYSTEM_ID = APM_SYSTEM_ID;
const LOGSTASH_SYSTEM_ID = 'logstash';
/**
 * The name of the Kibana System ID used to look up Reporting stats through the Monitoring system.
 * @type {string}
 */

exports.LOGSTASH_SYSTEM_ID = LOGSTASH_SYSTEM_ID;
const REPORTING_SYSTEM_ID = 'reporting';
/**
 * The amount of time, in milliseconds, to wait between reports when enabled.
 *
 * Currently 24 hours.
 * @type {Number}
 */

exports.REPORTING_SYSTEM_ID = REPORTING_SYSTEM_ID;
const REPORT_INTERVAL_MS = 86400000;
/*
 * Key for the localStorage service
 */

exports.REPORT_INTERVAL_MS = REPORT_INTERVAL_MS;
const LOCALSTORAGE_KEY = 'xpack.data';
/**
 * Link to the Elastic Telemetry privacy statement.
 */

exports.LOCALSTORAGE_KEY = LOCALSTORAGE_KEY;
const PRIVACY_STATEMENT_URL = `https://www.elastic.co/legal/telemetry-privacy-statement`;
/**
 * The type name used within the Monitoring index to publish localization stats.
 * @type {string}
 */

exports.PRIVACY_STATEMENT_URL = PRIVACY_STATEMENT_URL;
const KIBANA_LOCALIZATION_STATS_TYPE = 'localization';
/**
 * The header sent by telemetry service when hitting Elasticsearch to identify query source
 * @type {string}
 */

exports.KIBANA_LOCALIZATION_STATS_TYPE = KIBANA_LOCALIZATION_STATS_TYPE;
const TELEMETRY_QUERY_SOURCE = 'TELEMETRY';
/**
 * UI metric usage type
 * @type {string}
 */

exports.TELEMETRY_QUERY_SOURCE = TELEMETRY_QUERY_SOURCE;
const UI_METRIC_USAGE_TYPE = 'ui_metric';
exports.UI_METRIC_USAGE_TYPE = UI_METRIC_USAGE_TYPE;