"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StickyErrorProperties = StickyErrorProperties;

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _react = _interopRequireWildcard(require("react"));

var _elasticsearch_fieldnames = require("../../../../../common/elasticsearch_fieldnames");

var _i18n2 = require("../../../../../common/i18n");

var _StickyProperties = require("../../../shared/StickyProperties");

var _TransactionDetailLink = require("../../../shared/Links/apm/TransactionDetailLink");

var _agent_name = require("../../../../../common/agent_name");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function TransactionLinkWrapper(_ref) {
  var transaction = _ref.transaction;

  if (!transaction) {
    return _react.default.createElement(_react.Fragment, null, _i18n2.NOT_AVAILABLE_LABEL);
  }

  var isSampled = transaction.transaction.sampled;

  if (!isSampled) {
    return _react.default.createElement(_react.Fragment, null, transaction.transaction.id);
  }

  return _react.default.createElement(_TransactionDetailLink.TransactionDetailLink, {
    serviceName: transaction.service.name,
    transactionId: transaction.transaction.id,
    traceId: transaction.trace.id,
    transactionName: transaction.transaction.name,
    transactionType: transaction.transaction.type
  }, transaction.transaction.id);
}

function StickyErrorProperties(_ref2) {
  var error = _ref2.error,
      transaction = _ref2.transaction;
  var isHandled = error != null && error.error != null && error.error.exception != null && error.error.exception[0] != null ? error.error.exception[0].handled : undefined;
  var isRumAgent = (0, _agent_name.isRumAgentName)(error.agent.name);

  var _ref3 = isRumAgent ? {
    urlFieldName: _elasticsearch_fieldnames.ERROR_PAGE_URL,
    urlValue: error != null && error.error != null && error.error.page != null ? error.error.page.url : undefined
  } : {
    urlFieldName: _elasticsearch_fieldnames.URL_FULL,
    urlValue: error != null && error.url != null ? error.url.full : undefined
  },
      urlFieldName = _ref3.urlFieldName,
      urlValue = _ref3.urlValue;

  var stickyProperties = [{
    fieldName: '@timestamp',
    label: _i18n.i18n.translate('xpack.apm.errorGroupDetails.timestampLabel', {
      defaultMessage: 'Timestamp'
    }),
    val: error['@timestamp'],
    width: '50%'
  }, {
    fieldName: urlFieldName,
    label: 'URL',
    val: urlValue || _i18n2.NOT_AVAILABLE_LABEL,
    truncated: true,
    width: '50%'
  }, {
    fieldName: _elasticsearch_fieldnames.HTTP_REQUEST_METHOD,
    label: _i18n.i18n.translate('xpack.apm.errorGroupDetails.requestMethodLabel', {
      defaultMessage: 'Request method'
    }),
    val: (error != null && error.http != null && error.http.request != null ? error.http.request.method : undefined) || _i18n2.NOT_AVAILABLE_LABEL,
    width: '25%'
  }, {
    fieldName: _elasticsearch_fieldnames.ERROR_EXC_HANDLED,
    label: _i18n.i18n.translate('xpack.apm.errorGroupDetails.handledLabel', {
      defaultMessage: 'Handled'
    }),
    val: (0, _lodash.isBoolean)(isHandled) ? String(isHandled) : _i18n2.NOT_AVAILABLE_LABEL,
    width: '25%'
  }, {
    fieldName: _elasticsearch_fieldnames.TRANSACTION_ID,
    label: _i18n.i18n.translate('xpack.apm.errorGroupDetails.transactionSampleIdLabel', {
      defaultMessage: 'Transaction sample ID'
    }),
    val: _react.default.createElement(TransactionLinkWrapper, {
      transaction: transaction
    }),
    width: '25%'
  }, {
    fieldName: _elasticsearch_fieldnames.USER_ID,
    label: _i18n.i18n.translate('xpack.apm.errorGroupDetails.userIdLabel', {
      defaultMessage: 'User ID'
    }),
    val: (error != null && error.user != null ? error.user.id : undefined) || _i18n2.NOT_AVAILABLE_LABEL,
    width: '25%'
  }];
  return _react.default.createElement(_StickyProperties.StickyProperties, {
    stickyProperties: stickyProperties
  });
}