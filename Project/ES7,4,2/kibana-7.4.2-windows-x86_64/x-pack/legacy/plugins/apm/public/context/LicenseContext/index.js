"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LicenseProvider = exports.LicenseContext = void 0;

var _react = _interopRequireDefault(require("react"));

var _useFetcher2 = require("../../hooks/useFetcher");

var _xpack = require("../../services/rest/xpack");

var _InvalidLicenseNotification = require("./InvalidLicenseNotification");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialLicense = {
  features: {},
  license: {
    is_active: false
  }
};

var LicenseContext = _react.default.createContext(initialLicense);

exports.LicenseContext = LicenseContext;

var LicenseProvider = function LicenseProvider(_ref) {
  var children = _ref.children;

  var _useFetcher = (0, _useFetcher2.useFetcher)(function () {
    return (0, _xpack.loadLicense)();
  }, []),
      _useFetcher$data = _useFetcher.data,
      data = _useFetcher$data === void 0 ? initialLicense : _useFetcher$data,
      status = _useFetcher.status;

  var hasValidLicense = data.license.is_active; // if license is invalid show an error message

  if (status === _useFetcher2.FETCH_STATUS.SUCCESS && !hasValidLicense) {
    return _react.default.createElement(_InvalidLicenseNotification.InvalidLicenseNotification, null);
  } // render rest of application and pass down license via context


  return _react.default.createElement(LicenseContext.Provider, {
    value: data,
    children: children
  });
};

exports.LicenseProvider = LicenseProvider;