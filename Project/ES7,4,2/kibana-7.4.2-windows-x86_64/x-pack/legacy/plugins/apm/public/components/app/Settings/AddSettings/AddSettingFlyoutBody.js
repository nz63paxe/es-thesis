"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddSettingFlyoutBody = AddSettingFlyoutBody;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _environment_filter_values = require("../../../../../common/environment_filter_values");

var _SelectWithPlaceholder = require("../../../shared/SelectWithPlaceholder");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var selectPlaceholderLabel = "- ".concat(_i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.selectPlaceholder', {
  defaultMessage: 'Select'
}), " -");

function AddSettingFlyoutBody(_ref) {
  var selectedConfig = _ref.selectedConfig,
      onDelete = _ref.onDelete,
      environment = _ref.environment,
      setEnvironment = _ref.setEnvironment,
      serviceName = _ref.serviceName,
      setServiceName = _ref.setServiceName,
      sampleRate = _ref.sampleRate,
      setSampleRate = _ref.setSampleRate,
      serviceNames = _ref.serviceNames,
      serviceNamesStatus = _ref.serviceNamesStatus,
      environments = _ref.environments,
      environmentStatus = _ref.environmentStatus,
      isSampleRateValid = _ref.isSampleRateValid,
      isSelectedEnvironmentValid = _ref.isSelectedEnvironmentValid;
  var environmentOptions = environments.map(function (_ref2) {
    var name = _ref2.name,
        available = _ref2.available;
    return {
      disabled: !available,
      text: name === _environment_filter_values.ENVIRONMENT_NOT_DEFINED ? _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceEnvironmentNotSetOptionLabel', {
        defaultMessage: 'Not set'
      }) : name,
      value: name
    };
  });
  return _react.default.createElement(_eui.EuiForm, null, _react.default.createElement("form", null, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h3", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceSectionTitle', {
    defaultMessage: 'Service'
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceNameSelectLabel', {
      defaultMessage: 'Name'
    }),
    helpText: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceNameSelectHelpText', {
      defaultMessage: 'Choose the service you want to configure.'
    })
  }, _react.default.createElement(_SelectWithPlaceholder.SelectWithPlaceholder, {
    placeholder: selectPlaceholderLabel,
    isLoading: serviceNamesStatus === 'loading',
    options: serviceNames.map(function (text) {
      return {
        text: text
      };
    }),
    value: serviceName,
    disabled: Boolean(selectedConfig),
    onChange: function onChange(e) {
      e.preventDefault();
      setServiceName(e.target.value);
      setEnvironment(undefined);
    }
  })), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceEnvironmentSelectLabel', {
      defaultMessage: 'Environment'
    }),
    helpText: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceEnvironmentSelectHelpText', {
      defaultMessage: 'Only a single environment per configuration is supported.'
    }),
    error: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.serviceEnvironmentSelectErrorText', {
      defaultMessage: 'Must select a valid environment to save a configuration.'
    }),
    isInvalid: !(selectedConfig || !selectedConfig && environment && isSelectedEnvironmentValid && environmentStatus === 'success' || (0, _lodash.isEmpty)(sampleRate))
  }, _react.default.createElement(_SelectWithPlaceholder.SelectWithPlaceholder, {
    placeholder: selectPlaceholderLabel,
    isLoading: environmentStatus === 'loading',
    options: environmentOptions,
    value: selectedConfig ? environment || _environment_filter_values.ENVIRONMENT_NOT_DEFINED : environment,
    disabled: !serviceName || Boolean(selectedConfig),
    onChange: function onChange(e) {
      e.preventDefault();
      setEnvironment(e.target.value);
    }
  })), _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h3", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.configurationSectionTitle', {
    defaultMessage: 'Configuration'
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiFormRow, {
    label: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.sampleRateConfigurationInputLabel', {
      defaultMessage: 'Transaction sample rate'
    }),
    helpText: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.sampleRateConfigurationInputHelpText', {
      defaultMessage: 'Choose a rate between 0.000 and 1.0. Default configuration is 1.0 (100% of traces).'
    }),
    error: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.sampleRateConfigurationInputErrorText', {
      defaultMessage: 'Sample rate must be between 0.000 and 1'
    }),
    isInvalid: !(Boolean(selectedConfig) && ((0, _lodash.isEmpty)(sampleRate) || isSampleRateValid) || !selectedConfig && (!(serviceName || environment) || (0, _lodash.isEmpty)(sampleRate) || isSampleRateValid))
  }, _react.default.createElement(_eui.EuiFieldText, {
    placeholder: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.sampleRateConfigurationInputPlaceholderText', {
      defaultMessage: 'Set sample rate'
    }),
    value: sampleRate,
    onChange: function onChange(e) {
      e.preventDefault();
      setSampleRate(e.target.value);
    },
    disabled: !(serviceName && environment) && !selectedConfig
  })), selectedConfig ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiHorizontalRule, {
    margin: "m"
  }), _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h3", null, _react.default.createElement(_eui.EuiText, {
    color: "danger"
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.deleteConfigurationSectionTitle', {
    defaultMessage: 'Delete configuration'
  })))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiText, null, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.deleteConfigurationSectionText', {
    defaultMessage: 'If you wish to delete this configuration, please be aware that the agents will continue to use the existing configuration until they sync with the APM Server.'
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiButton, {
    fill: false,
    color: "danger",
    onClick: onDelete
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.deleteConfigurationButtonLabel', {
    defaultMessage: 'Delete'
  })), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  })) : null));
}