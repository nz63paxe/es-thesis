"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorMetadata = ErrorMetadata;

var _react = _interopRequireDefault(require("react"));

var _sections = require("./sections");

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function ErrorMetadata(_ref) {
  var error = _ref.error;
  return _react.default.createElement(_.MetadataTable, {
    item: error,
    sections: _sections.ERROR_METADATA_SECTIONS
  });
}