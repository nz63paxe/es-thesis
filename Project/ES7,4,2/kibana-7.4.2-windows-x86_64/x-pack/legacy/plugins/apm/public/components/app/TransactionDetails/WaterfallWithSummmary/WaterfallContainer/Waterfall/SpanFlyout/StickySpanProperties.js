"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StickySpanProperties = StickySpanProperties;

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _elasticsearch_fieldnames = require("../../../../../../../../common/elasticsearch_fieldnames");

var _i18n2 = require("../../../../../../../../common/i18n");

var _formatters = require("../../../../../../../utils/formatters");

var _StickyProperties = require("../../../../../../shared/StickyProperties");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function formatType(type) {
  switch (type) {
    case 'db':
      return 'DB';

    case 'hard-navigation':
      return _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.spanType.navigationTimingLabel', {
        defaultMessage: 'Navigation timing'
      });

    default:
      return type;
  }
}

function formatSubtype(subtype) {
  switch (subtype) {
    case 'mysql':
      return 'MySQL';

    default:
      return subtype;
  }
}

function getSpanTypes(span) {
  var _span$span = span.span,
      type = _span$span.type,
      subtype = _span$span.subtype,
      action = _span$span.action;

  var _type$split = type.split('.'),
      _type$split2 = _slicedToArray(_type$split, 3),
      primaryType = _type$split2[0],
      subtypeFromType = _type$split2[1],
      actionFromType = _type$split2[2]; // This is to support 6.x data


  return {
    spanType: formatType(primaryType),
    spanSubtype: formatSubtype(subtype || subtypeFromType),
    spanAction: action || actionFromType
  };
}

function StickySpanProperties(_ref) {
  var span = _ref.span,
      totalDuration = _ref.totalDuration;

  if (!totalDuration) {
    return null;
  }

  var spanName = span.span.name;
  var spanDuration = span.span.duration.us;

  var _getSpanTypes = getSpanTypes(span),
      spanType = _getSpanTypes.spanType,
      spanSubtype = _getSpanTypes.spanSubtype,
      spanAction = _getSpanTypes.spanAction;

  var stickyProperties = [{
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.nameLabel', {
      defaultMessage: 'Name'
    }),
    fieldName: _elasticsearch_fieldnames.SPAN_NAME,
    val: spanName || _i18n2.NOT_AVAILABLE_LABEL,
    truncated: true,
    width: '50%'
  }, {
    fieldName: _elasticsearch_fieldnames.SPAN_DURATION,
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.durationLabel', {
      defaultMessage: 'Duration'
    }),
    val: (0, _formatters.asMillis)(spanDuration),
    width: '50%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.percentOfTransactionLabel', {
      defaultMessage: '% of transaction'
    }),
    val: (0, _formatters.asPercent)(spanDuration, totalDuration),
    width: '50%'
  }, {
    fieldName: _elasticsearch_fieldnames.SPAN_TYPE,
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.typeLabel', {
      defaultMessage: 'Type'
    }),
    val: spanType,
    truncated: true,
    width: '15%'
  }];

  if (spanSubtype) {
    stickyProperties.push({
      fieldName: _elasticsearch_fieldnames.SPAN_SUBTYPE,
      label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.subtypeLabel', {
        defaultMessage: 'Subtype'
      }),
      val: spanSubtype,
      truncated: true,
      width: '15%'
    });
  }

  if (spanAction) {
    stickyProperties.push({
      fieldName: _elasticsearch_fieldnames.SPAN_ACTION,
      label: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.actionLabel', {
        defaultMessage: 'Action'
      }),
      val: spanAction,
      truncated: true,
      width: '15%'
    });
  }

  return _react.default.createElement(_StickyProperties.StickyProperties, {
    stickyProperties: stickyProperties
  });
}