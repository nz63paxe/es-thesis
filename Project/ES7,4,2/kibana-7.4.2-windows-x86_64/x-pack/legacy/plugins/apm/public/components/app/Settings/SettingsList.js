"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SettingsList = SettingsList;

var _react = _interopRequireDefault(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _lodash = require("lodash");

var _ManagedTable = require("../../shared/ManagedTable");

var _LoadingStatePrompt = require("../../shared/LoadingStatePrompt");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function SettingsList(_ref) {
  var status = _ref.status,
      data = _ref.data,
      setIsFlyoutOpen = _ref.setIsFlyoutOpen,
      setSelectedConfig = _ref.setSelectedConfig;
  var columns = [{
    field: 'service.name',
    name: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.serviceNameColumnLabel', {
      defaultMessage: 'Service name'
    }),
    sortable: true,
    render: function render(_, config) {
      return _react.default.createElement(_eui.EuiButtonEmpty, {
        size: "s",
        color: "primary",
        onClick: function onClick() {
          setSelectedConfig(config);
          setIsFlyoutOpen(true);
        }
      }, config.service.name);
    }
  }, {
    field: 'service.environment',
    name: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.environmentColumnLabel', {
      defaultMessage: 'Service environment'
    }),
    sortable: true,
    render: function render(value) {
      return value;
    }
  }, {
    field: 'settings.transaction_sample_rate',
    name: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.sampleRateColumnLabel', {
      defaultMessage: 'Sample rate'
    }),
    sortable: true,
    render: function render(value) {
      return value;
    }
  }, {
    field: '@timestamp',
    name: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.lastUpdatedColumnLabel', {
      defaultMessage: 'Last updated'
    }),
    sortable: true,
    render: function render(value) {
      return value ? (0, _moment.default)(value).fromNow() : null;
    }
  }, {
    name: '',
    actions: [{
      name: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.editButtonLabel', {
        defaultMessage: 'Edit'
      }),
      description: _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.editButtonDescription', {
        defaultMessage: 'Edit this config'
      }),
      icon: 'pencil',
      color: 'primary',
      type: 'icon',
      onClick: function onClick(config) {
        setSelectedConfig(config);
        setIsFlyoutOpen(true);
      }
    }]
  }];

  var emptyStatePrompt = _react.default.createElement(_eui.EuiEmptyPrompt, {
    iconType: "controlsHorizontal",
    title: _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.emptyPromptTitle', {
      defaultMessage: 'No configurations found.'
    })),
    body: _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.emptyPromptText', {
      defaultMessage: "Let's change that! You can fine-tune agent configuration directly from Kibana without having to redeploy. Get started by creating your first configuration."
    }))),
    actions: _react.default.createElement(_eui.EuiButton, {
      color: "primary",
      fill: true,
      onClick: function onClick() {
        return setIsFlyoutOpen(true);
      }
    }, _i18n.i18n.translate('xpack.apm.settings.agentConf.createConfigButtonLabel', {
      defaultMessage: 'Create configuration'
    }))
  });

  var failurePrompt = _react.default.createElement(_eui.EuiEmptyPrompt, {
    iconType: "alert",
    body: _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("p", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.configTable.failurePromptText', {
      defaultMessage: 'The list of agent configurations could not be fetched. Your user may not have the sufficient permissions.'
    })))
  });

  var hasConfigurations = !(0, _lodash.isEmpty)(data);

  if (status === 'failure') {
    return failurePrompt;
  }

  if (status === 'success') {
    if (hasConfigurations) {
      return _react.default.createElement(_ManagedTable.ManagedTable, {
        noItemsMessage: _react.default.createElement(_LoadingStatePrompt.LoadingStatePrompt, null),
        columns: columns,
        items: data,
        initialSortField: "service.name",
        initialSortDirection: "asc",
        initialPageSize: 50
      });
    } else {
      return emptyStatePrompt;
    }
  }

  return null;
}