"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Context = Context;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _lodash = require("lodash");

var _polished = require("polished");

var _react = _interopRequireDefault(require("react"));

var _javascript = _interopRequireDefault(require("react-syntax-highlighter/dist/languages/javascript"));

var _python = _interopRequireDefault(require("react-syntax-highlighter/dist/languages/python"));

var _ruby = _interopRequireDefault(require("react-syntax-highlighter/dist/languages/ruby"));

var _light = _interopRequireWildcard(require("react-syntax-highlighter/dist/light"));

var _styles = require("react-syntax-highlighter/dist/styles");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _variables = require("../../../style/variables");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  padding: 0;\n  margin: 0;\n  white-space: pre;\n  z-index: 2;\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n  // Override all styles\n  margin: 0;\n  color: inherit;\n  background: inherit;\n  border: 0;\n  border-radius: 0;\n  overflow: initial;\n  padding: 0 ", ";\n  line-height: ", ";\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  overflow: auto;\n  margin: 0 0 0 ", ";\n  padding: 0;\n  background-color: ", ";\n\n  &:last-of-type {\n    border-radius: 0 0 ", " 0;\n  }\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  min-width: ", ";\n  padding-left: ", ";\n  padding-right: ", ";\n  color: ", ";\n  line-height: ", ";\n  text-align: right;\n  border-right: 1px solid ", ";\n  background-color: ", ";\n\n  &:last-of-type {\n    border-radius: 0 0 0 ", ";\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  position: absolute;\n  top: 0;\n  left: 0;\n  border-radius: 0 0 0 ", ";\n  background: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: absolute;\n  width: 100%;\n  height: ", ";\n  top: ", ";\n  pointer-events: none;\n  background-color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  border-radius: 0 0 ", " ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

(0, _light.registerLanguage)('javascript', _javascript.default);
(0, _light.registerLanguage)('python', _python.default);
(0, _light.registerLanguage)('ruby', _ruby.default);

var ContextContainer = _styledComponents.default.div(_templateObject(), _variables.borderRadius, _variables.borderRadius);

var LINE_HEIGHT = _variables.units.eighth * 9;

var LineHighlight = _styledComponents.default.div(_templateObject2(), (0, _variables.px)(_variables.units.eighth * 9), function (props) {
  return (0, _variables.px)(props.lineNumber * LINE_HEIGHT);
}, (0, _polished.tint)(0.1, _eui_theme_light.default.euiColorWarning));

var LineNumberContainer = _styledComponents.default.div(_templateObject3(), _variables.borderRadius, function (props) {
  return props.isLibraryFrame ? _eui_theme_light.default.euiColorEmptyShade : _eui_theme_light.default.euiColorLightestShade;
});

var LineNumber = _styledComponents.default.div(_templateObject4(), (0, _variables.px)(_variables.units.eighth * 21), (0, _variables.px)(_variables.units.half), (0, _variables.px)(_variables.units.quarter), _eui_theme_light.default.euiColorMediumShade, (0, _variables.px)(_variables.unit + _variables.units.eighth), _eui_theme_light.default.euiColorLightShade, function (props) {
  return props.highlight ? (0, _polished.tint)(0.1, _eui_theme_light.default.euiColorWarning) : null;
}, _variables.borderRadius);

var LineContainer = _styledComponents.default.div(_templateObject5(), (0, _variables.px)(_variables.units.eighth * 21), _eui_theme_light.default.euiColorEmptyShade, _variables.borderRadius);

var Line = _styledComponents.default.pre(_templateObject6(), (0, _variables.px)(LINE_HEIGHT), (0, _variables.px)(LINE_HEIGHT));

var Code = _styledComponents.default.code(_templateObject7());

function getStackframeLines(stackframe) {
  var line = stackframe.line.context;
  var preLines = (stackframe != null && stackframe.context != null ? stackframe.context.pre : undefined) || [];
  var postLines = (stackframe != null && stackframe.context != null ? stackframe.context.post : undefined) || [];
  return [].concat(_toConsumableArray(preLines), [line], _toConsumableArray(postLines));
}

function getStartLineNumber(stackframe) {
  var preLines = (0, _lodash.size)((stackframe != null && stackframe.context != null ? stackframe.context.pre : undefined) || []);
  return stackframe.line.number - preLines;
}

function Context(_ref) {
  var stackframe = _ref.stackframe,
      codeLanguage = _ref.codeLanguage,
      isLibraryFrame = _ref.isLibraryFrame;
  var lines = getStackframeLines(stackframe);
  var startLineNumber = getStartLineNumber(stackframe);
  var highlightedLineIndex = (0, _lodash.size)((stackframe != null && stackframe.context != null ? stackframe.context.pre : undefined) || []);
  var language = codeLanguage || 'javascript'; // TODO: Add support for more languages

  return _react.default.createElement(ContextContainer, null, _react.default.createElement(LineHighlight, {
    lineNumber: highlightedLineIndex
  }), _react.default.createElement(LineNumberContainer, {
    isLibraryFrame: isLibraryFrame
  }, lines.map(function (line, i) {
    return _react.default.createElement(LineNumber, {
      key: line + i,
      highlight: highlightedLineIndex === i
    }, i + startLineNumber, ".");
  })), _react.default.createElement(LineContainer, null, lines.map(function (line, i) {
    return _react.default.createElement(_light.default, {
      key: line + i,
      language: language,
      style: _styles.xcode,
      PreTag: Line,
      CodeTag: Code,
      customStyle: {
        padding: null,
        overflowX: null
      }
    }, line || '\n');
  })));
}