"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pathify = pathify;
exports.DottedKeyValueTable = DottedKeyValueTable;

var _react = _interopRequireDefault(require("react"));

var _lodash = require("lodash");

var _eui = require("@elastic/eui");

var _FormattedValue = require("./FormattedValue");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Converts a deeply-nested object into a one-level object
 * with dot-notation paths as keys.
 */
function pathify(item, _ref) {
  var maxDepth = _ref.maxDepth,
      _ref$parentKey = _ref.parentKey,
      parentKey = _ref$parentKey === void 0 ? '' : _ref$parentKey,
      _ref$depth = _ref.depth,
      depth = _ref$depth === void 0 ? 0 : _ref$depth;
  return Object.keys(item).sort().reduce(function (pathified, key) {
    var currentKey = (0, _lodash.compact)([parentKey, key]).join('.');

    if ((!maxDepth || depth + 1 <= maxDepth) && (0, _lodash.isObject)(item[key])) {
      return _objectSpread({}, pathified, {}, pathify(item[key], {
        maxDepth: maxDepth,
        parentKey: currentKey,
        depth: depth + 1
      }));
    } else {
      return _objectSpread({}, pathified, _defineProperty({}, currentKey, item[key]));
    }
  }, {});
}

function DottedKeyValueTable(_ref2) {
  var data = _ref2.data,
      parentKey = _ref2.parentKey,
      maxDepth = _ref2.maxDepth,
      _ref2$tableProps = _ref2.tableProps,
      tableProps = _ref2$tableProps === void 0 ? {} : _ref2$tableProps;
  var pathified = pathify(data, {
    maxDepth: maxDepth,
    parentKey: parentKey
  });
  var rows = Object.keys(pathified).sort().map(function (k) {
    return [k, pathified[k]];
  });
  return _react.default.createElement(_eui.EuiTable, _extends({
    compressed: true
  }, tableProps), _react.default.createElement(_eui.EuiTableBody, null, rows.map(function (_ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
        key = _ref4[0],
        value = _ref4[1];

    return _react.default.createElement(_eui.EuiTableRow, {
      key: key
    }, _react.default.createElement(_eui.EuiTableRowCell, null, _react.default.createElement("strong", {
      "data-testid": "dot-key"
    }, key)), _react.default.createElement(_eui.EuiTableRowCell, {
      "data-testid": "value"
    }, _react.default.createElement(_FormattedValue.FormattedValue, {
      value: value
    })));
  })));
}