"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports._clearCache = _clearCache;
exports.callApi = callApi;

var _lodash = require("lodash");

var _lruCache = _interopRequireDefault(require("lru-cache"));

var _objectHash = _interopRequireDefault(require("object-hash"));

var _kfetch = require("ui/kfetch");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function fetchOptionsWithDebug(fetchOptions) {
  var debugEnabled = sessionStorage.getItem('apm_debug') === 'true' && (0, _lodash.startsWith)(fetchOptions.pathname, '/api/apm');

  if (!debugEnabled) {
    return fetchOptions;
  }

  return _objectSpread({}, fetchOptions, {
    query: _objectSpread({}, fetchOptions.query, {
      _debug: true
    })
  });
}

var cache = new _lruCache.default({
  max: 100,
  maxAge: 1000 * 60 * 60
});

function _clearCache() {
  cache.reset();
}

function callApi(_x, _x2) {
  return _callApi.apply(this, arguments);
} // only cache items that has a time range with `start` and `end` params,
// and where `end` is not a timestamp in the future


function _callApi() {
  _callApi = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(fetchOptions, options) {
    var cacheKey, cacheResponse, combinedFetchOptions, res;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            cacheKey = getCacheKey(fetchOptions);
            cacheResponse = cache.get(cacheKey);

            if (!cacheResponse) {
              _context.next = 4;
              break;
            }

            return _context.abrupt("return", cacheResponse);

          case 4:
            combinedFetchOptions = fetchOptionsWithDebug(fetchOptions);
            _context.next = 7;
            return (0, _kfetch.kfetch)(combinedFetchOptions, options);

          case 7:
            res = _context.sent;

            if (isCachable(fetchOptions)) {
              cache.set(cacheKey, res);
            }

            return _context.abrupt("return", res);

          case 10:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _callApi.apply(this, arguments);
}

function isCachable(fetchOptions) {
  if (!(fetchOptions.query && fetchOptions.query.start && fetchOptions.query.end)) {
    return false;
  }

  return (0, _lodash.isString)(fetchOptions.query.end) && new Date(fetchOptions.query.end).getTime() < Date.now();
} // order the options object to make sure that two objects with the same arguments, produce produce the
// same cache key regardless of the order of properties


function getCacheKey(options) {
  return (0, _objectHash.default)(options);
}