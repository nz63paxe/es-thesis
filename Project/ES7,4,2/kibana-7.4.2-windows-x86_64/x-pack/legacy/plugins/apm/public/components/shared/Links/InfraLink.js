"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraLink = InfraLink;

var _eui = require("@elastic/eui");

var _lodash = require("lodash");

var _react = _interopRequireDefault(require("react"));

var _url = _interopRequireDefault(require("url"));

var _url_helpers = require("./url_helpers");

var _useCore = require("../../../hooks/useCore");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function InfraLink(_ref) {
  var path = _ref.path,
      _ref$query = _ref.query,
      query = _ref$query === void 0 ? {} : _ref$query,
      rest = _objectWithoutProperties(_ref, ["path", "query"]);

  var core = (0, _useCore.useCore)();
  var nextSearch = (0, _url_helpers.fromQuery)(query);

  var href = _url.default.format({
    pathname: core.http.basePath.prepend('/app/infra'),
    hash: (0, _lodash.compact)([path, nextSearch]).join('?')
  });

  return _react.default.createElement(_eui.EuiLink, _extends({}, rest, {
    href: href
  }));
}