"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = exports.REACT_APP_ROOT_ID = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactRouterDom = require("react-router-dom");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _history = require("../utils/history");

var _CoreContext = require("../context/CoreContext");

var _LocationContext = require("../context/LocationContext");

var _UrlParamsContext = require("../context/UrlParamsContext");

var _variables = require("../style/variables");

var _LoadingIndicatorContext = require("../context/LoadingIndicatorContext");

var _LicenseContext = require("../context/LicenseContext");

var _UpdateBreadcrumbs = require("../components/app/Main/UpdateBreadcrumbs");

var _route_config = require("../components/app/Main/route_config");

var _ScrollToTopOnPathChange = require("../components/app/Main/ScrollToTopOnPathChange");

var _useUpdateBadgeEffect = require("../components/app/Main/useUpdateBadgeEffect");

var _MatchedRouteContext = require("../context/MatchedRouteContext");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  min-width: ", ";\n  padding: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var REACT_APP_ROOT_ID = 'react-apm-root';
exports.REACT_APP_ROOT_ID = REACT_APP_ROOT_ID;

var MainContainer = _styledComponents.default.div(_templateObject(), (0, _variables.px)(_variables.unit * 50), (0, _variables.px)(_variables.units.plus));

var App = function App() {
  (0, _useUpdateBadgeEffect.useUpdateBadgeEffect)();
  return _react.default.createElement(_MatchedRouteContext.MatchedRouteProvider, null, _react.default.createElement(_UrlParamsContext.UrlParamsProvider, null, _react.default.createElement(_LoadingIndicatorContext.LoadingIndicatorProvider, null, _react.default.createElement(MainContainer, {
    "data-test-subj": "apmMainContainer"
  }, _react.default.createElement(_UpdateBreadcrumbs.UpdateBreadcrumbs, null), _react.default.createElement(_reactRouterDom.Route, {
    component: _ScrollToTopOnPathChange.ScrollToTopOnPathChange
  }), _react.default.createElement(_LicenseContext.LicenseProvider, null, _react.default.createElement(_reactRouterDom.Switch, null, _route_config.routes.map(function (route, i) {
    return _react.default.createElement(_reactRouterDom.Route, _extends({
      key: i
    }, route));
  })))))));
};

var Plugin =
/*#__PURE__*/
function () {
  function Plugin() {
    _classCallCheck(this, Plugin);
  }

  _createClass(Plugin, [{
    key: "start",
    value: function start(core) {
      var i18n = core.i18n;

      _reactDom.default.render(_react.default.createElement(_CoreContext.CoreProvider, {
        core: core
      }, _react.default.createElement(i18n.Context, null, _react.default.createElement(_reactRouterDom.Router, {
        history: _history.history
      }, _react.default.createElement(_LocationContext.LocationProvider, null, _react.default.createElement(App, null))))), document.getElementById(REACT_APP_ROOT_ID));
    }
  }]);

  return Plugin;
}();

exports.Plugin = Plugin;