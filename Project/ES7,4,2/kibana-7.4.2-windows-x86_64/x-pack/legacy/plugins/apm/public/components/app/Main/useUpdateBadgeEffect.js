"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useUpdateBadgeEffect = void 0;

var _i18n = require("@kbn/i18n");

var _react = require("react");

var _capabilities = require("ui/capabilities");

var _useCore2 = require("../../../hooks/useCore");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useUpdateBadgeEffect = function useUpdateBadgeEffect() {
  var _useCore = (0, _useCore2.useCore)(),
      chrome = _useCore.chrome;

  (0, _react.useEffect)(function () {
    var uiCapabilities = _capabilities.capabilities.get();

    chrome.setBadge(!uiCapabilities.apm.save ? {
      text: _i18n.i18n.translate('xpack.apm.header.badge.readOnly.text', {
        defaultMessage: 'Read only'
      }),
      tooltip: _i18n.i18n.translate('xpack.apm.header.badge.readOnly.tooltip', {
        defaultMessage: 'Unable to save'
      }),
      iconType: 'glasses'
    } : undefined);
  }, [chrome]);
};

exports.useUpdateBadgeEffect = useUpdateBadgeEffect;