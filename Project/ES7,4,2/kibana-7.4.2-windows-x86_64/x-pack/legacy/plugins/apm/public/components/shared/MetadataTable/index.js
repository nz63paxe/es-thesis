"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetadataTable = MetadataTable;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _lodash = require("lodash");

var _i18n = require("@kbn/i18n");

var _DottedKeyValueTable = require("../DottedKeyValueTable");

var _ElasticDocsLink = require("../../shared/Links/ElasticDocsLink");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function MetadataTable(_ref) {
  var item = _ref.item,
      sections = _ref.sections;
  var filteredSections = sections.filter(function (_ref2) {
    var key = _ref2.key,
        required = _ref2.required;
    return required || (0, _lodash.has)(item, key);
  });
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_ElasticDocsLink.ElasticDocsLink, {
    section: "/apm/get-started",
    path: "/metadata.html"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement(_eui.EuiIcon, {
    type: "help"
  }), " How to add labels and other data")))), filteredSections.map(function (section) {
    return _react.default.createElement("div", {
      key: section.key
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("h6", null, section.label)), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(Section, {
      propData: (0, _lodash.get)(item, section.key),
      propKey: section.key
    }), _react.default.createElement(_eui.EuiSpacer, {
      size: "xl"
    }));
  }));
}

function Section(_ref3) {
  var propData = _ref3.propData,
      propKey = _ref3.propKey;
  return _react.default.createElement(_react.default.Fragment, null, propData ? _react.default.createElement(_DottedKeyValueTable.DottedKeyValueTable, {
    data: propData,
    parentKey: propKey,
    maxDepth: 5
  }) : _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _i18n.i18n.translate('xpack.apm.propertiesTable.agentFeature.noDataAvailableLabel', {
    defaultMessage: 'No data available'
  })));
}