"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StickyTransactionProperties = StickyTransactionProperties;

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _elasticsearch_fieldnames = require("../../../../../common/elasticsearch_fieldnames");

var _i18n2 = require("../../../../../common/i18n");

var _formatters = require("../../../../utils/formatters");

var _StickyProperties = require("../../../shared/StickyProperties");

var _ErrorCountBadge = require("./ErrorCountBadge");

var _agent_name = require("../../../../../common/agent_name");

var _variables = require("../../../../style/variables");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-size: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ErrorTitle = _styledComponents.default.span(_templateObject(), _variables.fontSize);

function StickyTransactionProperties(_ref) {
  var transaction = _ref.transaction,
      totalDuration = _ref.totalDuration,
      errorCount = _ref.errorCount;
  var timestamp = transaction['@timestamp'];
  var isRumAgent = (0, _agent_name.isRumAgentName)(transaction.agent.name);

  var _ref2 = isRumAgent ? {
    urlFieldName: _elasticsearch_fieldnames.TRANSACTION_PAGE_URL,
    urlValue: transaction != null && transaction.transaction != null && transaction.transaction.page != null ? transaction.transaction.page.url : undefined
  } : {
    urlFieldName: _elasticsearch_fieldnames.URL_FULL,
    urlValue: transaction != null && transaction.url != null ? transaction.url.full : undefined
  },
      urlFieldName = _ref2.urlFieldName,
      urlValue = _ref2.urlValue;

  var duration = transaction.transaction.duration.us;

  var noErrorsText = _i18n.i18n.translate('xpack.apm.transactionDetails.errorsNone', {
    defaultMessage: 'None'
  });

  var stickyProperties = [{
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.timestampLabel', {
      defaultMessage: 'Timestamp'
    }),
    fieldName: '@timestamp',
    val: timestamp,
    truncated: true,
    width: '50%'
  }, {
    fieldName: urlFieldName,
    label: 'URL',
    val: urlValue || _i18n2.NOT_AVAILABLE_LABEL,
    truncated: true,
    width: '50%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.durationLabel', {
      defaultMessage: 'Duration'
    }),
    fieldName: _elasticsearch_fieldnames.TRANSACTION_DURATION,
    val: (0, _formatters.asTime)(duration),
    width: '25%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.percentOfTraceLabel', {
      defaultMessage: '% of trace'
    }),
    val: totalDuration !== undefined && duration > totalDuration ? _react.default.createElement(_eui.EuiToolTip, {
      content: _i18n.i18n.translate('xpack.apm.transactionDetails.percentOfTraceLabelExplanation', {
        defaultMessage: 'The % of trace exceeds 100% because this transaction takes longer than the root transaction.'
      })
    }, _react.default.createElement("span", null, ">100%")) : (0, _formatters.asPercent)(duration, totalDuration, _i18n2.NOT_AVAILABLE_LABEL),
    width: '25%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.resultLabel', {
      defaultMessage: 'Result'
    }),
    fieldName: _elasticsearch_fieldnames.TRANSACTION_RESULT,
    val: (transaction != null && transaction.transaction != null ? transaction.transaction.result : undefined) || _i18n2.NOT_AVAILABLE_LABEL,
    width: '14%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.errorsOverviewLabel', {
      defaultMessage: 'Errors'
    }),
    val: errorCount ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_ErrorCountBadge.ErrorCountBadge, {
      errorCount: errorCount
    }), _react.default.createElement(ErrorTitle, null, "\xA0", _i18n.i18n.translate('xpack.apm.transactionDetails.errorsOverviewLink', {
      values: {
        errorCount: errorCount
      },
      defaultMessage: '{errorCount, plural, one {Related error} other {Related errors}}'
    }))) : noErrorsText,
    width: '18%'
  }, {
    label: _i18n.i18n.translate('xpack.apm.transactionDetails.userIdLabel', {
      defaultMessage: 'User ID'
    }),
    fieldName: _elasticsearch_fieldnames.USER_ID,
    val: (transaction != null && transaction.user != null ? transaction.user.id : undefined) || _i18n2.NOT_AVAILABLE_LABEL,
    truncated: true,
    width: '18%'
  }];
  var userAgent = transaction.user_agent;

  if (userAgent) {
    var os = userAgent.os,
        device = userAgent.device;
    var width = '25%';
    stickyProperties.push({
      label: _i18n.i18n.translate('xpack.apm.transactionDetails.userAgentLabel', {
        defaultMessage: 'User agent'
      }),
      val: [userAgent.name, userAgent.version].filter(Boolean).join(' '),
      truncated: true,
      width: width
    });

    if (os) {
      stickyProperties.push({
        label: _i18n.i18n.translate('xpack.apm.transactionDetails.userAgentOsLabel', {
          defaultMessage: 'User agent OS'
        }),
        val: os.full || os.name,
        truncated: true,
        width: width
      });
    }

    if (device) {
      stickyProperties.push({
        label: _i18n.i18n.translate('xpack.apm.transactionDetails.userAgentDeviceLabel', {
          defaultMessage: 'User agent device'
        }),
        val: device.name,
        width: width
      });
    }
  }

  return _react.default.createElement(_StickyProperties.StickyProperties, {
    stickyProperties: stickyProperties
  });
}