"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RouteName = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RouteName;
exports.RouteName = RouteName;

(function (RouteName) {
  RouteName["HOME"] = "home";
  RouteName["SERVICES"] = "services";
  RouteName["TRACES"] = "traces";
  RouteName["SERVICE"] = "service";
  RouteName["TRANSACTIONS"] = "transactions";
  RouteName["ERRORS"] = "errors";
  RouteName["ERROR"] = "error";
  RouteName["METRICS"] = "metrics";
  RouteName["TRANSACTION_TYPE"] = "transaction_type";
  RouteName["TRANSACTION_NAME"] = "transaction_name";
  RouteName["SETTINGS"] = "settings";
})(RouteName || (exports.RouteName = RouteName = {}));