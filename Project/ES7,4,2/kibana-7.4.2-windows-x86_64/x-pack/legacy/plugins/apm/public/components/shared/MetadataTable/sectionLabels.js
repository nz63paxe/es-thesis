"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CUSTOM = exports.USER = exports.URL = exports.AGENT = exports.PROCESS = exports.SERVICE = exports.CONTAINER = exports.HOST = exports.HTTP = exports.LABELS = void 0;

var _i18n = require("@kbn/i18n");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LABELS = _i18n.i18n.translate('xpack.apm.metadataTable.section.labelsLabel', {
  defaultMessage: 'Labels'
});

exports.LABELS = LABELS;

var HTTP = _i18n.i18n.translate('xpack.apm.metadataTable.section.httpLabel', {
  defaultMessage: 'HTTP'
});

exports.HTTP = HTTP;

var HOST = _i18n.i18n.translate('xpack.apm.metadataTable.section.hostLabel', {
  defaultMessage: 'Host'
});

exports.HOST = HOST;

var CONTAINER = _i18n.i18n.translate('xpack.apm.metadataTable.section.containerLabel', {
  defaultMessage: 'Container'
});

exports.CONTAINER = CONTAINER;

var SERVICE = _i18n.i18n.translate('xpack.apm.metadataTable.section.serviceLabel', {
  defaultMessage: 'Service'
});

exports.SERVICE = SERVICE;

var PROCESS = _i18n.i18n.translate('xpack.apm.metadataTable.section.processLabel', {
  defaultMessage: 'Process'
});

exports.PROCESS = PROCESS;

var AGENT = _i18n.i18n.translate('xpack.apm.metadataTable.section.agentLabel', {
  defaultMessage: 'Agent'
});

exports.AGENT = AGENT;

var URL = _i18n.i18n.translate('xpack.apm.metadataTable.section.urlLabel', {
  defaultMessage: 'URL'
});

exports.URL = URL;

var USER = _i18n.i18n.translate('xpack.apm.metadataTable.section.userLabel', {
  defaultMessage: 'User'
});

exports.USER = USER;

var CUSTOM = _i18n.i18n.translate('xpack.apm.metadataTable.section.customLabel', {
  defaultMessage: 'Custom'
});

exports.CUSTOM = CUSTOM;