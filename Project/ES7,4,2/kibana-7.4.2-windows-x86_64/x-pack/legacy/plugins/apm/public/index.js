"use strict";

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _new_platform = require("ui/new_platform");

require("react-vis/dist/style.css");

require("ui/autoload/all");

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

require("uiExports/autocompleteProviders");

var _GlobalHelpExtension = require("./components/app/GlobalHelpExtension");

var _newPlatform = require("./new-platform");

var _plugin = require("./new-platform/plugin");

require("./style/global_overrides.css");

var _index = _interopRequireDefault(require("./templates/index.html"));

var _CoreContext = require("./context/CoreContext");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var core = _new_platform.npStart.core; // render APM feedback link in global help menu

core.chrome.setHelpExtension(function (domElement) {
  _reactDom.default.render(_react.default.createElement(_CoreContext.CoreProvider, {
    core: core
  }, _react.default.createElement(_GlobalHelpExtension.GlobalHelpExtension, null)), domElement);

  return function () {
    _reactDom.default.unmountComponentAtNode(domElement);
  };
}); // @ts-ignore

_chrome.default.setRootTemplate(_index.default);

var checkForRoot = function checkForRoot() {
  return new Promise(function (resolve) {
    var ready = !!document.getElementById(_plugin.REACT_APP_ROOT_ID);

    if (ready) {
      resolve();
    } else {
      setTimeout(function () {
        return resolve(checkForRoot());
      }, 10);
    }
  });
};

checkForRoot().then(function () {
  (0, _newPlatform.plugin)().start(core);
});