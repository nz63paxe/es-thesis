"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Settings = Settings;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _lodash = require("lodash");

var _useFetcher2 = require("../../../hooks/useFetcher");

var _AddSettingFlyout = require("./AddSettings/AddSettingFlyout");

var _callApmApi = require("../../../services/rest/callApmApi");

var _HomeLink = require("../../shared/Links/apm/HomeLink");

var _SettingsList = require("./SettingsList");

var _public = require("../../../../../infra/public");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function Settings() {
  var _useFetcher = (0, _useFetcher2.useFetcher)(function () {
    return (0, _callApmApi.callApmApi)({
      pathname: "/api/apm/settings/agent-configuration"
    });
  }, []),
      _useFetcher$data = _useFetcher.data,
      data = _useFetcher$data === void 0 ? [] : _useFetcher$data,
      status = _useFetcher.status,
      refresh = _useFetcher.refresh;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      selectedConfig = _useState2[0],
      setSelectedConfig = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isFlyoutOpen = _useState4[0],
      setIsFlyoutOpen = _useState4[1];

  (0, _public.useTrackPageview)({
    app: 'apm',
    path: 'agent_configuration'
  });
  (0, _public.useTrackPageview)({
    app: 'apm',
    path: 'agent_configuration',
    delay: 15000
  });

  var RETURN_TO_OVERVIEW_LINK_LABEL = _i18n.i18n.translate('xpack.apm.settings.agentConf.returnToOverviewLinkLabel', {
    defaultMessage: 'Return to overview'
  });

  var hasConfigurations = !(0, _lodash.isEmpty)(data);
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_AddSettingFlyout.AddSettingsFlyout, {
    isOpen: isFlyoutOpen,
    selectedConfig: selectedConfig,
    onClose: function onClose() {
      setSelectedConfig(null);
      setIsFlyoutOpen(false);
    },
    onSubmit: function onSubmit() {
      setSelectedConfig(null);
      setIsFlyoutOpen(false);
      refresh();
    }
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "l"
  }, _react.default.createElement("h1", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.pageTitle', {
    defaultMessage: 'Settings'
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_HomeLink.HomeLink, null, _react.default.createElement(_eui.EuiButtonEmpty, {
    size: "s",
    color: "primary",
    iconType: "arrowLeft"
  }, RETURN_TO_OVERVIEW_LINK_LABEL)))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.configurationsPanelTitle', {
    defaultMessage: 'Configurations'
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiBetaBadge, {
    label: _i18n.i18n.translate('xpack.apm.settings.agentConf.betaBadgeLabel', {
      defaultMessage: 'Beta'
    }),
    tooltipContent: _i18n.i18n.translate('xpack.apm.settings.agentConf.betaBadgeText', {
      defaultMessage: 'This feature is still in development. If you have feedback, please reach out in our Discuss forum.'
    })
  })), hasConfigurations ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    color: "primary",
    fill: true,
    iconType: "plusInCircle",
    onClick: function onClick() {
      return setIsFlyoutOpen(true);
    }
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.createConfigButtonLabel', {
    defaultMessage: 'Create configuration'
  }))))) : null), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiCallOut, {
    title: _i18n.i18n.translate('xpack.apm.settings.agentConf.betaCallOutTitle', {
      defaultMessage: 'APM Agent Configuration (BETA)'
    }),
    iconType: "iInCircle"
  }, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.apm.settings.agentConf.betaCallOutText",
    defaultMessage: "We're excited to bring you a first look at APM Agent configuration. {agentConfigDocsLink}",
    values: {
      agentConfigDocsLink: _react.default.createElement(_eui.EuiLink, {
        href: "https://www.elastic.co/guide/en/kibana/current/agent-configuration.html"
      }, _i18n.i18n.translate('xpack.apm.settings.agentConf.agentConfigDocsLinkLabel', {
        defaultMessage: 'Learn more in our docs.'
      }))
    }
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_SettingsList.SettingsList, {
    status: status,
    data: data,
    setIsFlyoutOpen: setIsFlyoutOpen,
    setSelectedConfig: setSelectedConfig
  })));
}