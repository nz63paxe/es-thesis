"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ResponsiveFlyout = void 0;

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n\n  @media (min-width: 800px) {\n    width: 90%;\n  }\n\n  @media (min-width: 1000px) {\n    width: 80%;\n  }\n\n  @media (min-width: 1400px) {\n    width: 70%;\n  }\n\n  @media (min-width: 2000px) {\n    width: 60%;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ResponsiveFlyout = (0, _styledComponents.default)(_eui.EuiFlyout)(_templateObject());
exports.ResponsiveFlyout = ResponsiveFlyout;