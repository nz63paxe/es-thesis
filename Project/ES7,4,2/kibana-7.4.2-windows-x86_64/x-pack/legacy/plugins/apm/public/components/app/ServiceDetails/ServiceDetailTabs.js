"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ServiceDetailTabs = ServiceDetailTabs;

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _ErrorGroupOverview = require("../ErrorGroupOverview");

var _TransactionOverview = require("../TransactionOverview");

var _ServiceMetrics = require("../ServiceMetrics");

var _useFetcher2 = require("../../../hooks/useFetcher");

var _agent_name = require("../../../../common/agent_name");

var _callApmApi = require("../../../services/rest/callApmApi");

var _EuiTabLink = require("../../shared/EuiTabLink");

var _useUrlParams2 = require("../../../hooks/useUrlParams");

var _TransactionOverviewLink = require("../../shared/Links/apm/TransactionOverviewLink");

var _ErrorOverviewLink = require("../../shared/Links/apm/ErrorOverviewLink");

var _MetricOverviewLink = require("../../shared/Links/apm/MetricOverviewLink");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function ServiceDetailTabs(_ref) {
  var tab = _ref.tab;

  var _useUrlParams = (0, _useUrlParams2.useUrlParams)(),
      urlParams = _useUrlParams.urlParams;

  var serviceName = urlParams.serviceName,
      start = urlParams.start,
      end = urlParams.end;

  var _useFetcher = (0, _useFetcher2.useFetcher)(function () {
    if (serviceName && start && end) {
      return (0, _callApmApi.callApmApi)({
        pathname: '/api/apm/services/{serviceName}/agent_name',
        params: {
          path: {
            serviceName: serviceName
          },
          query: {
            start: start,
            end: end
          }
        }
      }).then(function (res) {
        return res.agentName;
      });
    }
  }, [serviceName, start, end]),
      agentName = _useFetcher.data;

  if (!serviceName) {
    // this never happens, urlParams type is not accurate enough
    throw new Error('Service name was not defined');
  }

  var transactionsTab = {
    link: _react.default.createElement(_TransactionOverviewLink.TransactionOverviewLink, {
      serviceName: serviceName
    }, _i18n.i18n.translate('xpack.apm.serviceDetails.transactionsTabLabel', {
      defaultMessage: 'Transactions'
    })),
    render: function render() {
      return _react.default.createElement(_TransactionOverview.TransactionOverview, null);
    },
    name: 'transactions'
  };
  var errorsTab = {
    link: _react.default.createElement(_ErrorOverviewLink.ErrorOverviewLink, {
      serviceName: serviceName
    }, _i18n.i18n.translate('xpack.apm.serviceDetails.errorsTabLabel', {
      defaultMessage: 'Errors'
    })),
    render: function render() {
      return _react.default.createElement(_ErrorGroupOverview.ErrorGroupOverview, null);
    },
    name: 'errors'
  };
  var tabs = [transactionsTab, errorsTab];

  if (agentName && !(0, _agent_name.isRumAgentName)(agentName)) {
    var metricsTab = {
      link: _react.default.createElement(_MetricOverviewLink.MetricOverviewLink, {
        serviceName: serviceName
      }, _i18n.i18n.translate('xpack.apm.serviceDetails.metricsTabLabel', {
        defaultMessage: 'Metrics'
      })),
      render: function render() {
        return _react.default.createElement(_ServiceMetrics.ServiceMetrics, {
          agentName: agentName
        });
      },
      name: 'metrics'
    };
    tabs.push(metricsTab);
  }

  var selectedTab = tabs.find(function (serviceTab) {
    return serviceTab.name === tab;
  });
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiTabs, null, tabs.map(function (serviceTab) {
    return _react.default.createElement(_EuiTabLink.EuiTabLink, {
      isSelected: serviceTab.name === tab,
      key: serviceTab.name
    }, serviceTab.link);
  })), _react.default.createElement(_eui.EuiSpacer, null), selectedTab ? selectedTab.render() : null);
}