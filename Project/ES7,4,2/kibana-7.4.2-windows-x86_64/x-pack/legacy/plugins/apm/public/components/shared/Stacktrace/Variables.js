"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Variables = void 0;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _variables = require("../../../style/variables");

var _Icons = require("../Icons");

var _DottedKeyValueTable = require("../DottedKeyValueTable");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  padding: ", " ", " 0;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  display: block;\n  cursor: pointer;\n  user-select: none;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background: ", ";\n  border-top: 1px solid ", ";\n  border-radius: 0 0 ", " ", ";\n  padding: ", " ", ";\n  font-family: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var VariablesContainer = _styledComponents.default.div(_templateObject(), _eui_theme_light.default.euiColorEmptyShade, _eui_theme_light.default.euiColorLightShade, _variables.borderRadius, _variables.borderRadius, (0, _variables.px)(_variables.units.half), (0, _variables.px)(_variables.unit), _variables.fontFamily);

var VariablesToggle = _styledComponents.default.a(_templateObject2());

var VariablesTableContainer = _styledComponents.default.div(_templateObject3(), (0, _variables.px)(_variables.units.plus), (0, _variables.px)(_variables.unit));

var Variables =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Variables, _React$Component);

  function Variables() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Variables);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Variables)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isVisible: false
    });

    _defineProperty(_assertThisInitialized(_this), "onClick", function () {
      _this.setState(function () {
        return {
          isVisible: !_this.state.isVisible
        };
      });
    });

    return _this;
  }

  _createClass(Variables, [{
    key: "render",
    value: function render() {
      if (!this.props.vars) {
        return null;
      }

      return _react.default.createElement(VariablesContainer, null, _react.default.createElement(VariablesToggle, {
        onClick: this.onClick
      }, _react.default.createElement(_Icons.Ellipsis, {
        horizontal: this.state.isVisible
      }), ' ', _i18n.i18n.translate('xpack.apm.stacktraceTab.localVariablesToogleButtonLabel', {
        defaultMessage: 'Local variables'
      })), this.state.isVisible && _react.default.createElement(VariablesTableContainer, null, _react.default.createElement(_DottedKeyValueTable.DottedKeyValueTable, {
        data: this.props.vars,
        maxDepth: 5
      })));
    }
  }]);

  return Variables;
}(_react.default.Component);

exports.Variables = Variables;