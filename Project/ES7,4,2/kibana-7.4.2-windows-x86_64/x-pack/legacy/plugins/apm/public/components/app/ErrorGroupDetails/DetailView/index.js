"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DetailView = DetailView;
exports.TabContent = TabContent;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _lodash = require("lodash");

var _variables = require("../../../../style/variables");

var _DiscoverErrorLink = require("../../../shared/Links/DiscoverLinks/DiscoverErrorLink");

var _url_helpers = require("../../../shared/Links/url_helpers");

var _history = require("../../../../utils/history");

var _ErrorMetadata = require("../../../shared/MetadataTable/ErrorMetadata");

var _Stacktrace = require("../../../shared/Stacktrace");

var _ErrorTabs = require("./ErrorTabs");

var _StickyErrorProperties = require("./StickyErrorProperties");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-start;\n  margin-bottom: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var HeaderContainer = _styledComponents.default.div(_templateObject(), (0, _variables.px)(_variables.unit));

// TODO: Move query-string-based tabs into a re-usable component?
function getCurrentTab() {
  var tabs = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var currentTabKey = arguments.length > 1 ? arguments[1] : undefined;
  var selectedTab = tabs.find(function (_ref) {
    var key = _ref.key;
    return key === currentTabKey;
  });
  return selectedTab ? selectedTab : (0, _lodash.first)(tabs) || {};
}

function DetailView(_ref2) {
  var errorGroup = _ref2.errorGroup,
      urlParams = _ref2.urlParams,
      location = _ref2.location;
  var transaction = errorGroup.transaction,
      error = errorGroup.error,
      occurrencesCount = errorGroup.occurrencesCount;

  if (!error) {
    return null;
  }

  var tabs = (0, _ErrorTabs.getTabs)(error);
  var currentTab = getCurrentTab(tabs, urlParams.detailTab);
  return _react.default.createElement(_eui.EuiPanel, null, _react.default.createElement(HeaderContainer, null, _react.default.createElement(_eui.EuiTitle, {
    size: "s"
  }, _react.default.createElement("h3", null, _i18n.i18n.translate('xpack.apm.errorGroupDetails.errorOccurrenceTitle', {
    defaultMessage: 'Error occurrence'
  }))), _react.default.createElement(_DiscoverErrorLink.DiscoverErrorLink, {
    error: error,
    kuery: urlParams.kuery
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    iconType: "discoverApp"
  }, _i18n.i18n.translate('xpack.apm.errorGroupDetails.viewOccurrencesInDiscoverButtonLabel', {
    defaultMessage: 'View {occurrencesCount} occurrences in Discover',
    values: {
      occurrencesCount: occurrencesCount
    }
  })))), _react.default.createElement(_StickyErrorProperties.StickyErrorProperties, {
    error: error,
    transaction: transaction
  }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiTabs, null, tabs.map(function (_ref3) {
    var key = _ref3.key,
        label = _ref3.label;
    return _react.default.createElement(_eui.EuiTab, {
      onClick: function onClick() {
        _history.history.replace(_objectSpread({}, location, {
          search: (0, _url_helpers.fromQuery)(_objectSpread({}, (0, _url_helpers.toQuery)(location.search), {
            detailTab: key
          }))
        }));
      },
      isSelected: currentTab.key === key,
      key: key
    }, label);
  })), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(TabContent, {
    error: error,
    currentTab: currentTab
  }));
}

function TabContent(_ref4) {
  var error = _ref4.error,
      currentTab = _ref4.currentTab;
  var codeLanguage = error != null && error.service != null && error.service.language != null ? error.service.language.name : undefined;
  var excStackframes = error != null && error.error != null && error.error.exception != null && error.error.exception[0] != null ? error.error.exception[0].stacktrace : undefined;
  var logStackframes = error != null && error.error != null && error.error.exception != null && error.error.exception[0] != null ? error.error.exception[0].stacktrace : undefined;

  switch (currentTab.key) {
    case _ErrorTabs.logStacktraceTab.key:
      return _react.default.createElement(_Stacktrace.Stacktrace, {
        stackframes: logStackframes,
        codeLanguage: codeLanguage
      });

    case _ErrorTabs.exceptionStacktraceTab.key:
      return _react.default.createElement(_Stacktrace.Stacktrace, {
        stackframes: excStackframes,
        codeLanguage: codeLanguage
      });

    default:
      return _react.default.createElement(_ErrorMetadata.ErrorMetadata, {
        error: error
      });
  }
}