"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElasticDocsLink = ElasticDocsLink;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _metadata = require("ui/metadata");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// TODO: metadata should be read from a useContext hook in new platform
var STACK_VERSION = _metadata.metadata.branch; // union type constisting of valid guide sections that we link to

function ElasticDocsLink(_ref) {
  var section = _ref.section,
      path = _ref.path,
      rest = _objectWithoutProperties(_ref, ["section", "path"]);

  var href = "https://www.elastic.co/guide/en".concat(section, "/").concat(STACK_VERSION).concat(path);
  return _react.default.createElement(_eui.EuiLink, _extends({
    href: href
  }, rest));
}