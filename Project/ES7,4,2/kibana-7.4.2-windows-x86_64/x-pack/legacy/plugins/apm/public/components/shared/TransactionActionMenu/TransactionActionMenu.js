"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TransactionActionMenu = void 0;

var _eui = require("@elastic/eui");

var _url = _interopRequireDefault(require("url"));

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _lodash = require("lodash");

var _DiscoverTransactionLink = require("../Links/DiscoverLinks/DiscoverTransactionLink");

var _InfraLink = require("../Links/InfraLink");

var _useUrlParams2 = require("../../../hooks/useUrlParams");

var _url_helpers = require("../Links/url_helpers");

var _useCore = require("../../../hooks/useCore");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function getInfraMetricsQuery(transaction) {
  var plus5 = new Date(transaction['@timestamp']);
  var minus5 = new Date(plus5.getTime());
  plus5.setMinutes(plus5.getMinutes() + 5);
  minus5.setMinutes(minus5.getMinutes() - 5);
  return {
    from: minus5.getTime(),
    to: plus5.getTime()
  };
}

function ActionMenuButton(_ref) {
  var onClick = _ref.onClick;
  return _react.default.createElement(_eui.EuiButtonEmpty, {
    iconType: "arrowDown",
    iconSide: "right",
    onClick: onClick
  }, _i18n.i18n.translate('xpack.apm.transactionActionMenu.actionsButtonLabel', {
    defaultMessage: 'Actions'
  }));
}

var TransactionActionMenu = function TransactionActionMenu(props) {
  var transaction = props.transaction;
  var core = (0, _useCore.useCore)();

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isOpen = _useState2[0],
      setIsOpen = _useState2[1];

  var _useUrlParams = (0, _useUrlParams2.useUrlParams)(),
      urlParams = _useUrlParams.urlParams;

  var hostName = transaction != null && transaction.host != null ? transaction.host.hostname : undefined;
  var podId = transaction != null && transaction.kubernetes != null && transaction.kubernetes.pod != null ? transaction.kubernetes.pod.uid : undefined;
  var containerId = transaction != null && transaction.container != null ? transaction.container.id : undefined;
  var time = Math.round(transaction.timestamp.us / 1000);
  var infraMetricsQuery = getInfraMetricsQuery(transaction);
  var infraConfigItems = [{
    icon: 'loggingApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showPodLogsLinkLabel', {
      defaultMessage: 'Show pod logs'
    }),
    condition: !!podId,
    path: "/link-to/pod-logs/".concat(podId),
    query: {
      time: time
    }
  }, {
    icon: 'loggingApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showContainerLogsLinkLabel', {
      defaultMessage: 'Show container logs'
    }),
    condition: !!containerId,
    path: "/link-to/container-logs/".concat(containerId),
    query: {
      time: time
    }
  }, {
    icon: 'loggingApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showHostLogsLinkLabel', {
      defaultMessage: 'Show host logs'
    }),
    condition: !!hostName,
    path: "/link-to/host-logs/".concat(hostName),
    query: {
      time: time
    }
  }, {
    icon: 'loggingApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showTraceLogsLinkLabel', {
      defaultMessage: 'Show trace logs'
    }),
    condition: true,
    path: "/link-to/logs",
    query: {
      time: time,
      filter: "trace.id:".concat(transaction.trace.id)
    }
  }, {
    icon: 'infraApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showPodMetricsLinkLabel', {
      defaultMessage: 'Show pod metrics'
    }),
    condition: !!podId,
    path: "/link-to/pod-detail/".concat(podId),
    query: infraMetricsQuery
  }, {
    icon: 'infraApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showContainerMetricsLinkLabel', {
      defaultMessage: 'Show container metrics'
    }),
    condition: !!containerId,
    path: "/link-to/container-detail/".concat(containerId),
    query: infraMetricsQuery
  }, {
    icon: 'infraApp',
    label: _i18n.i18n.translate('xpack.apm.transactionActionMenu.showHostMetricsLinkLabel', {
      defaultMessage: 'Show host metrics'
    }),
    condition: !!hostName,
    path: "/link-to/host-detail/".concat(hostName),
    query: infraMetricsQuery
  }];
  var infraItems = infraConfigItems.map(function (_ref2, index) {
    var icon = _ref2.icon,
        label = _ref2.label,
        condition = _ref2.condition,
        path = _ref2.path,
        query = _ref2.query;
    return {
      icon: icon,
      key: "infra-link-".concat(index),
      child: _react.default.createElement(_InfraLink.InfraLink, {
        path: path,
        query: query
      }, label),
      condition: condition
    };
  });

  var uptimeLink = _url.default.format({
    pathname: core.http.basePath.prepend('/app/uptime'),
    hash: "/?".concat((0, _url_helpers.fromQuery)((0, _lodash.pick)({
      dateRangeStart: urlParams.rangeFrom,
      dateRangeEnd: urlParams.rangeTo,
      search: "url.domain:\"".concat(transaction != null && transaction.url != null ? transaction.url.domain : undefined, "\"")
    }, function (val) {
      return !!val;
    })))
  });

  var menuItems = [].concat(_toConsumableArray(infraItems), [{
    icon: 'discoverApp',
    key: 'discover-transaction',
    condition: true,
    child: _react.default.createElement(_DiscoverTransactionLink.DiscoverTransactionLink, {
      transaction: transaction
    }, _i18n.i18n.translate('xpack.apm.transactionActionMenu.viewSampleDocumentLinkLabel', {
      defaultMessage: 'View sample document'
    }))
  }, {
    icon: 'uptimeApp',
    key: 'uptime',
    child: _react.default.createElement(_eui.EuiLink, {
      href: uptimeLink
    }, _i18n.i18n.translate('xpack.apm.transactionActionMenu.viewInUptime', {
      defaultMessage: 'View monitor status'
    })),
    condition: transaction != null && transaction.url != null ? transaction.url.domain : undefined
  }]).filter(function (_ref3) {
    var condition = _ref3.condition;
    return condition;
  }).map(function (_ref4) {
    var icon = _ref4.icon,
        key = _ref4.key,
        child = _ref4.child,
        condition = _ref4.condition;
    return condition ? _react.default.createElement(_eui.EuiContextMenuItem, {
      icon: icon,
      key: key
    }, _react.default.createElement(_eui.EuiFlexGroup, {
      gutterSize: "s"
    }, _react.default.createElement(_eui.EuiFlexItem, null, child), _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_eui.EuiIcon, {
      type: "popout"
    })))) : null;
  });
  return _react.default.createElement(_eui.EuiPopover, {
    id: "transactionActionMenu",
    button: _react.default.createElement(ActionMenuButton, {
      onClick: function onClick() {
        return setIsOpen(!isOpen);
      }
    }),
    isOpen: isOpen,
    closePopover: function closePopover() {
      return setIsOpen(false);
    },
    anchorPosition: "downRight",
    panelPaddingSize: "none"
  }, _react.default.createElement(_eui.EuiContextMenuPanel, {
    items: menuItems,
    title: _i18n.i18n.translate('xpack.apm.transactionActionMenu.actionsLabel', {
      defaultMessage: 'Actions'
    })
  }));
};

exports.TransactionActionMenu = TransactionActionMenu;