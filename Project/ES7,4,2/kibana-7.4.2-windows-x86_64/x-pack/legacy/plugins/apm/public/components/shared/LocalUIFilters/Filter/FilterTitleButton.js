"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FilterTitleButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  width: 100%;\n\n  .alignLeft {\n    justify-content: flex-start;\n    padding-left: 0;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Button = (0, _styledComponents.default)(_eui.EuiButtonEmpty).attrs({
  contentProps: {
    className: 'alignLeft'
  },
  color: 'text'
})(_templateObject());

var FilterTitleButton = function FilterTitleButton(props) {
  return _react.default.createElement(Button, props, _react.default.createElement(_eui.EuiTitle, {
    size: "xxxs",
    textTransform: "uppercase"
  }, _react.default.createElement("h4", null, props.children)));
};

exports.FilterTitleButton = FilterTitleButton;