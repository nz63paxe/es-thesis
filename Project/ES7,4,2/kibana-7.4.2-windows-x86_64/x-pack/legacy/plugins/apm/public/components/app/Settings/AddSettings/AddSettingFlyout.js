"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AddSettingsFlyout = AddSettingsFlyout;

var _eui = require("@elastic/eui");

var _react = _interopRequireWildcard(require("react"));

var _notify = require("ui/notify");

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _transaction_sample_rate_rt = require("../../../../../common/runtime_types/transaction_sample_rate_rt");

var _AddSettingFlyoutBody = require("./AddSettingFlyoutBody");

var _useFetcher3 = require("../../../../hooks/useFetcher");

var _environment_filter_values = require("../../../../../common/environment_filter_values");

var _callApmApi = require("../../../../services/rest/callApmApi");

var _use_track_metric = require("../../../../../../infra/public/hooks/use_track_metric");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function AddSettingsFlyout(_ref) {
  var onClose = _ref.onClose,
      isOpen = _ref.isOpen,
      onSubmit = _ref.onSubmit,
      selectedConfig = _ref.selectedConfig;

  var _useState = (0, _react.useState)(selectedConfig ? selectedConfig.service.environment || _environment_filter_values.ENVIRONMENT_NOT_DEFINED : undefined),
      _useState2 = _slicedToArray(_useState, 2),
      environment = _useState2[0],
      setEnvironment = _useState2[1];

  var _useState3 = (0, _react.useState)(selectedConfig ? selectedConfig.service.name : undefined),
      _useState4 = _slicedToArray(_useState3, 2),
      serviceName = _useState4[0],
      setServiceName = _useState4[1];

  var _useState5 = (0, _react.useState)(selectedConfig ? selectedConfig.settings.transaction_sample_rate.toString() : ''),
      _useState6 = _slicedToArray(_useState5, 2),
      sampleRate = _useState6[0],
      setSampleRate = _useState6[1];

  var _useFetcher = (0, _useFetcher3.useFetcher)(function () {
    return (0, _callApmApi.callApmApi)({
      pathname: '/api/apm/settings/agent-configuration/services'
    });
  }, [], {
    preservePreviousData: false
  }),
      _useFetcher$data = _useFetcher.data,
      serviceNames = _useFetcher$data === void 0 ? [] : _useFetcher$data,
      serviceNamesStatus = _useFetcher.status;

  var _useFetcher2 = (0, _useFetcher3.useFetcher)(function () {
    if (serviceName) {
      return (0, _callApmApi.callApmApi)({
        pathname: '/api/apm/settings/agent-configuration/services/{serviceName}/environments',
        params: {
          path: {
            serviceName: serviceName
          }
        }
      });
    }
  }, [serviceName], {
    preservePreviousData: false
  }),
      _useFetcher2$data = _useFetcher2.data,
      environments = _useFetcher2$data === void 0 ? [] : _useFetcher2$data,
      environmentStatus = _useFetcher2.status;

  var isSampleRateValid = _transaction_sample_rate_rt.transactionSampleRateRt.decode(sampleRate).isRight();

  var isSelectedEnvironmentValid = environments.some(function (env) {
    return env.name === environment && (Boolean(selectedConfig) || env.available);
  });
  (0, _react.useEffect)(function () {
    if (selectedConfig) {
      setEnvironment(selectedConfig.service.environment);
      setServiceName(selectedConfig.service.name);
      setSampleRate(selectedConfig.settings.transaction_sample_rate.toString());
    } else {
      setEnvironment(_environment_filter_values.ENVIRONMENT_NOT_DEFINED);
      setServiceName(undefined);
      setSampleRate('');
    }
  }, [selectedConfig]);

  if (!isOpen) {
    return null;
  }

  return _react.default.createElement(_eui.EuiPortal, null, _react.default.createElement(_eui.EuiFlyout, {
    size: "s",
    onClose: onClose,
    ownFocus: true
  }, _react.default.createElement(_eui.EuiFlyoutHeader, {
    hasBorder: true
  }, _react.default.createElement(_eui.EuiTitle, null, selectedConfig ? _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.editConfigTitle', {
    defaultMessage: 'Edit configuration'
  })) : _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.createConfigTitle', {
    defaultMessage: 'Create configuration'
  })))), _react.default.createElement(_eui.EuiFlyoutBody, null, _react.default.createElement(_eui.EuiCallOut, {
    title: _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.betaCallOutTitle', {
      defaultMessage: 'APM Agent Configuration (BETA)'
    }),
    iconType: "iInCircle",
    color: "warning"
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.betaCallOutText', {
    defaultMessage: 'Please note only sample rate configuration is supported in this first version. We will extend support for agent configuration in future releases. Please be aware of bugs.'
  })), _react.default.createElement(_eui.EuiHorizontalRule, {
    margin: "m"
  }), _react.default.createElement(_AddSettingFlyoutBody.AddSettingFlyoutBody, {
    selectedConfig: selectedConfig,
    onDelete:
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!selectedConfig) {
                _context.next = 3;
                break;
              }

              _context.next = 3;
              return deleteConfig(selectedConfig);

            case 3:
              onSubmit();

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })),
    environment: environment,
    setEnvironment: setEnvironment,
    serviceName: serviceName,
    setServiceName: setServiceName,
    sampleRate: sampleRate,
    setSampleRate: setSampleRate,
    serviceNames: serviceNames,
    serviceNamesStatus: serviceNamesStatus,
    environments: environments,
    environmentStatus: environmentStatus,
    isSampleRateValid: isSampleRateValid,
    isSelectedEnvironmentValid: isSelectedEnvironmentValid
  })), _react.default.createElement(_eui.EuiFlyoutFooter, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    onClick: onClose
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.cancelButtonLabel', {
    defaultMessage: 'Cancel'
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    isDisabled: !(selectedConfig && isSampleRateValid || !selectedConfig && serviceName && environment && isSelectedEnvironmentValid && isSampleRateValid),
    onClick:
    /*#__PURE__*/
    function () {
      var _ref3 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(event) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                event.preventDefault();
                _context2.next = 3;
                return saveConfig({
                  environment: environment,
                  serviceName: serviceName,
                  sampleRate: parseFloat(sampleRate),
                  configurationId: selectedConfig ? selectedConfig.id : undefined
                });

              case 3:
                onSubmit();

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      return function (_x) {
        return _ref3.apply(this, arguments);
      };
    }()
  }, _i18n.i18n.translate('xpack.apm.settings.agentConf.flyOut.saveConfigurationButtonLabel', {
    defaultMessage: 'Save configuration'
  })))))));
}

function deleteConfig(_x2) {
  return _deleteConfig.apply(this, arguments);
}

function _deleteConfig() {
  _deleteConfig = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(selectedConfig) {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.prev = 0;
            _context3.next = 3;
            return (0, _callApmApi.callApmApi)({
              pathname: '/api/apm/settings/agent-configuration/{configurationId}',
              method: 'DELETE',
              params: {
                path: {
                  configurationId: selectedConfig.id
                }
              }
            });

          case 3:
            _notify.toastNotifications.addSuccess({
              title: _i18n.i18n.translate('xpack.apm.settings.agentConf.deleteConfigSucceededTitle', {
                defaultMessage: 'Configuration was deleted'
              }),
              text: _react.default.createElement(_react2.FormattedMessage, {
                id: "xpack.apm.settings.agentConf.deleteConfigSucceededText",
                defaultMessage: "You have successfully deleted a configuration for {serviceName}. It will take some time to propagate to the agents.",
                values: {
                  serviceName: "\"".concat(selectedConfig.service.name, "\"")
                }
              })
            });

            _context3.next = 9;
            break;

          case 6:
            _context3.prev = 6;
            _context3.t0 = _context3["catch"](0);

            _notify.toastNotifications.addDanger({
              title: _i18n.i18n.translate('xpack.apm.settings.agentConf.deleteConfigFailedTitle', {
                defaultMessage: 'Configuration could not be deleted'
              }),
              text: _react.default.createElement(_react2.FormattedMessage, {
                id: "xpack.apm.settings.agentConf.deleteConfigFailedText",
                defaultMessage: "Something went wrong when deleting a configuration for {serviceName}. Error: {errorMessage}",
                values: {
                  serviceName: "\"".concat(selectedConfig.service.name, "\""),
                  errorMessage: "\"".concat(_context3.t0.message, "\"")
                }
              })
            });

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3, null, [[0, 6]]);
  }));
  return _deleteConfig.apply(this, arguments);
}

function saveConfig(_x3) {
  return _saveConfig.apply(this, arguments);
}

function _saveConfig() {
  _saveConfig = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(_ref4) {
    var sampleRate, serviceName, environment, configurationId, configuration;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            sampleRate = _ref4.sampleRate, serviceName = _ref4.serviceName, environment = _ref4.environment, configurationId = _ref4.configurationId;
            (0, _use_track_metric.trackEvent)({
              app: 'apm',
              name: 'save_agent_configuration'
            });
            _context4.prev = 2;

            if (!(isNaN(sampleRate) || !serviceName)) {
              _context4.next = 5;
              break;
            }

            throw new Error('Missing arguments');

          case 5:
            configuration = {
              settings: {
                transaction_sample_rate: sampleRate
              },
              service: {
                name: serviceName,
                environment: environment === _environment_filter_values.ENVIRONMENT_NOT_DEFINED ? undefined : environment
              }
            };

            if (!configurationId) {
              _context4.next = 12;
              break;
            }

            _context4.next = 9;
            return (0, _callApmApi.callApmApi)({
              pathname: '/api/apm/settings/agent-configuration/{configurationId}',
              method: 'PUT',
              params: {
                path: {
                  configurationId: configurationId
                },
                body: configuration
              }
            });

          case 9:
            _notify.toastNotifications.addSuccess({
              title: _i18n.i18n.translate('xpack.apm.settings.agentConf.editConfigSucceededTitle', {
                defaultMessage: 'Configuration edited'
              }),
              text: _react.default.createElement(_react2.FormattedMessage, {
                id: "xpack.apm.settings.agentConf.editConfigSucceededText",
                defaultMessage: "You have successfully edited the configuration for {serviceName}. It will take some time to propagate to the agents.",
                values: {
                  serviceName: "\"".concat(serviceName, "\"")
                }
              })
            });

            _context4.next = 15;
            break;

          case 12:
            _context4.next = 14;
            return (0, _callApmApi.callApmApi)({
              pathname: '/api/apm/settings/agent-configuration/new',
              method: 'POST',
              params: {
                body: configuration
              }
            });

          case 14:
            _notify.toastNotifications.addSuccess({
              title: _i18n.i18n.translate('xpack.apm.settings.agentConf.createConfigSucceededTitle', {
                defaultMessage: 'Configuration created!'
              }),
              text: _react.default.createElement(_react2.FormattedMessage, {
                id: "xpack.apm.settings.agentConf.createConfigSucceededText",
                defaultMessage: "You have successfully created a configuration for {serviceName}. It will take some time to propagate to the agents.",
                values: {
                  serviceName: "\"".concat(serviceName, "\"")
                }
              })
            });

          case 15:
            _context4.next = 20;
            break;

          case 17:
            _context4.prev = 17;
            _context4.t0 = _context4["catch"](2);

            if (configurationId) {
              _notify.toastNotifications.addDanger({
                title: _i18n.i18n.translate('xpack.apm.settings.agentConf.editConfigFailedTitle', {
                  defaultMessage: 'Configuration could not be edited'
                }),
                text: _react.default.createElement(_react2.FormattedMessage, {
                  id: "xpack.apm.settings.agentConf.editConfigFailedText",
                  defaultMessage: "Something went wrong when editing the configuration for {serviceName}. Error: {errorMessage}",
                  values: {
                    serviceName: "\"".concat(serviceName, "\""),
                    errorMessage: "\"".concat(_context4.t0.message, "\"")
                  }
                })
              });
            } else {
              _notify.toastNotifications.addDanger({
                title: _i18n.i18n.translate('xpack.apm.settings.agentConf.createConfigFailedTitle', {
                  defaultMessage: 'Configuration could not be created'
                }),
                text: _react.default.createElement(_react2.FormattedMessage, {
                  id: "xpack.apm.settings.agentConf.createConfigFailedText",
                  defaultMessage: "Something went wrong when creating a configuration for {serviceName}. Error: {errorMessage}",
                  values: {
                    serviceName: "\"".concat(serviceName, "\""),
                    errorMessage: "\"".concat(_context4.t0.message, "\"")
                  }
                })
              });
            }

          case 20:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4, null, [[2, 17]]);
  }));
  return _saveConfig.apply(this, arguments);
}