"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asHours = asHours;
exports.asMinutes = asMinutes;
exports.asSeconds = asSeconds;
exports.asMillis = asMillis;
exports.asMicros = asMicros;
exports.timeUnit = timeUnit;
exports.asTime = asTime;
exports.asDecimal = asDecimal;
exports.asInteger = asInteger;
exports.tpmUnit = tpmUnit;
exports.asPercent = asPercent;
exports.getFixedByteFormatter = exports.asDynamicBytes = exports.getTimeFormatter = void 0;

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _i18n2 = require("../../common/i18n");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var HOURS_CUT_OFF = 3600000000; // 1 hour (in microseconds)

var MINUTES_CUT_OFF = 60000000; // 1 minute (in microseconds)

var SECONDS_CUT_OFF = 10 * 1000000; // 10 seconds (in microseconds)

var MILLISECONDS_CUT_OFF = 10 * 1000; // 10 milliseconds (in microseconds)

var SPACE = ' ';
/*
 * value: time in microseconds
 * withUnit: add unit suffix
 * defaultValue: value to use if the specified is null/undefined
 */

function asHours(value) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref$withUnit = _ref.withUnit,
      withUnit = _ref$withUnit === void 0 ? true : _ref$withUnit,
      _ref$defaultValue = _ref.defaultValue,
      defaultValue = _ref$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var hoursLabel = SPACE + _i18n.i18n.translate('xpack.apm.formatters.hoursTimeUnitLabel', {
    defaultMessage: 'h'
  });

  var formatted = asDecimal(value / 3600000000);
  return "".concat(formatted).concat(withUnit ? hoursLabel : '');
}

function asMinutes(value) {
  var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref2$withUnit = _ref2.withUnit,
      withUnit = _ref2$withUnit === void 0 ? true : _ref2$withUnit,
      _ref2$defaultValue = _ref2.defaultValue,
      defaultValue = _ref2$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref2$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var minutesLabel = SPACE + _i18n.i18n.translate('xpack.apm.formatters.minutesTimeUnitLabel', {
    defaultMessage: 'min'
  });

  var formatted = asDecimal(value / 60000000);
  return "".concat(formatted).concat(withUnit ? minutesLabel : '');
}

function asSeconds(value) {
  var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref3$withUnit = _ref3.withUnit,
      withUnit = _ref3$withUnit === void 0 ? true : _ref3$withUnit,
      _ref3$defaultValue = _ref3.defaultValue,
      defaultValue = _ref3$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref3$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var secondsLabel = SPACE + _i18n.i18n.translate('xpack.apm.formatters.secondsTimeUnitLabel', {
    defaultMessage: 's'
  });

  var formatted = asDecimal(value / 1000000);
  return "".concat(formatted).concat(withUnit ? secondsLabel : '');
}

function asMillis(value) {
  var _ref4 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref4$withUnit = _ref4.withUnit,
      withUnit = _ref4$withUnit === void 0 ? true : _ref4$withUnit,
      _ref4$defaultValue = _ref4.defaultValue,
      defaultValue = _ref4$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref4$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var millisLabel = SPACE + _i18n.i18n.translate('xpack.apm.formatters.millisTimeUnitLabel', {
    defaultMessage: 'ms'
  });

  var formatted = asInteger(value / 1000);
  return "".concat(formatted).concat(withUnit ? millisLabel : '');
}

function asMicros(value) {
  var _ref5 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref5$withUnit = _ref5.withUnit,
      withUnit = _ref5$withUnit === void 0 ? true : _ref5$withUnit,
      _ref5$defaultValue = _ref5.defaultValue,
      defaultValue = _ref5$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref5$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var microsLabel = SPACE + _i18n.i18n.translate('xpack.apm.formatters.microsTimeUnitLabel', {
    defaultMessage: 'μs'
  });

  var formatted = asInteger(value);
  return "".concat(formatted).concat(withUnit ? microsLabel : '');
}

var getTimeFormatter = (0, _lodash.memoize)(function (max) {
  var unit = timeUnit(max);

  switch (unit) {
    case 'h':
      return asHours;

    case 'm':
      return asMinutes;

    case 's':
      return asSeconds;

    case 'ms':
      return asMillis;

    case 'us':
      return asMicros;
  }
});
exports.getTimeFormatter = getTimeFormatter;

function timeUnit(max) {
  if (max > HOURS_CUT_OFF) {
    return 'h';
  } else if (max > MINUTES_CUT_OFF) {
    return 'm';
  } else if (max > SECONDS_CUT_OFF) {
    return 's';
  } else if (max > MILLISECONDS_CUT_OFF) {
    return 'ms';
  } else {
    return 'us';
  }
}

function asTime(value) {
  var _ref6 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      _ref6$withUnit = _ref6.withUnit,
      withUnit = _ref6$withUnit === void 0 ? true : _ref6$withUnit,
      _ref6$defaultValue = _ref6.defaultValue,
      defaultValue = _ref6$defaultValue === void 0 ? _i18n2.NOT_AVAILABLE_LABEL : _ref6$defaultValue;

  if (value == null) {
    return defaultValue;
  }

  var formatter = getTimeFormatter(value);
  return formatter(value, {
    withUnit: withUnit,
    defaultValue: defaultValue
  });
}

function asDecimal(value) {
  return (0, _numeral.default)(value).format('0,0.0');
}

function asInteger(value) {
  return (0, _numeral.default)(value).format('0,0');
}

function tpmUnit(type) {
  return type === 'request' ? _i18n.i18n.translate('xpack.apm.formatters.requestsPerMinLabel', {
    defaultMessage: 'rpm'
  }) : _i18n.i18n.translate('xpack.apm.formatters.transactionsPerMinLabel', {
    defaultMessage: 'tpm'
  });
}

function asPercent(numerator, denominator) {
  var fallbackResult = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

  if (!denominator || isNaN(numerator)) {
    return fallbackResult;
  }

  var decimal = numerator / denominator;
  return (0, _numeral.default)(decimal).format('0.0%');
}

function asKilobytes(value) {
  return "".concat(asDecimal(value / 1000), " KB");
}

function asMegabytes(value) {
  return "".concat(asDecimal(value / 1e6), " MB");
}

function asGigabytes(value) {
  return "".concat(asDecimal(value / 1e9), " GB");
}

function asTerabytes(value) {
  return "".concat(asDecimal(value / 1e12), " TB");
}

function asBytes(value) {
  return "".concat(asDecimal(value), " B");
}

var bailIfNumberInvalid = function bailIfNumberInvalid(cb) {
  return function (val) {
    if (val === null || val === undefined || isNaN(val)) {
      return '';
    }

    return cb(val);
  };
};

var asDynamicBytes = bailIfNumberInvalid(function (value) {
  return unmemoizedFixedByteFormatter(value)(value);
});
exports.asDynamicBytes = asDynamicBytes;

var unmemoizedFixedByteFormatter = function unmemoizedFixedByteFormatter(max) {
  if (max > 1e12) {
    return asTerabytes;
  }

  if (max > 1e9) {
    return asGigabytes;
  }

  if (max > 1e6) {
    return asMegabytes;
  }

  if (max > 1000) {
    return asKilobytes;
  }

  return asBytes;
};

var getFixedByteFormatter = (0, _lodash.memoize)(function (max) {
  var formatter = unmemoizedFixedByteFormatter(max);
  return bailIfNumberInvalid(formatter);
});
exports.getFixedByteFormatter = getFixedByteFormatter;