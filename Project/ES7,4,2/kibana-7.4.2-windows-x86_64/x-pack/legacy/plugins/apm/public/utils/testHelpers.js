"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toJson = toJson;
exports.mockMoment = mockMoment;
exports.getRenderedHref = getRenderedHref;
exports.mockNow = mockNow;
exports.delay = delay;
exports.expectTextsNotInDocument = expectTextsNotInDocument;
exports.expectTextsInDocument = expectTextsInDocument;
exports.inspectSearchParams = inspectSearchParams;
exports.tick = void 0;

var _enzymeToJson = _interopRequireDefault(require("enzyme-to-json"));

require("jest-styled-components");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _reactTestingLibrary = require("react-testing-library");

var _reactRouterDom = require("react-router-dom");

var _LocationContext = require("../context/LocationContext");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function toJson(wrapper) {
  return (0, _enzymeToJson.default)(wrapper, {
    noKey: true,
    mode: 'deep'
  });
}

function mockMoment() {
  // avoid timezone issues
  jest.spyOn(_moment.default.prototype, 'format').mockImplementation(function () {
    return "1st of January (mocking ".concat(this.unix(), ")");
  }); // convert relative time to absolute time to avoid timing issues

  jest.spyOn(_moment.default.prototype, 'fromNow').mockImplementation(function () {
    return "1337 minutes ago (mocking ".concat(this.unix(), ")");
  });
} // Useful for getting the rendered href from any kind of link component


function getRenderedHref(_x, _x2) {
  return _getRenderedHref.apply(this, arguments);
}

function _getRenderedHref() {
  _getRenderedHref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(Component, location) {
    var el, a;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            el = (0, _reactTestingLibrary.render)(_react.default.createElement(_reactRouterDom.MemoryRouter, {
              initialEntries: [location]
            }, _react.default.createElement(_LocationContext.LocationProvider, null, _react.default.createElement(Component, null))));
            _context.next = 3;
            return tick();

          case 3:
            _context.next = 5;
            return (0, _reactTestingLibrary.waitForElement)(function () {
              return el.container.querySelector('a');
            });

          case 5:
            a = el.container.querySelector('a');
            return _context.abrupt("return", a ? a.getAttribute('href') : '');

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _getRenderedHref.apply(this, arguments);
}

function mockNow(date) {
  var fakeNow = new Date(date).getTime();
  return jest.spyOn(Date, 'now').mockReturnValue(fakeNow);
}

function delay(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
} // Await this when you need to "flush" promises to immediately resolve or throw in tests


var tick = function tick() {
  return new Promise(function (resolve) {
    return setImmediate(resolve, 0);
  });
};

exports.tick = tick;

function expectTextsNotInDocument(output, texts) {
  texts.forEach(function (text) {
    try {
      output.getByText(text);
    } catch (err) {
      if (err.message.startsWith('Unable to find an element with the text:')) {
        return;
      } else {
        throw err;
      }
    }

    throw new Error("Unexpected text found: ".concat(text));
  });
}

function expectTextsInDocument(output, texts) {
  texts.forEach(function (text) {
    expect(output.getByText(text)).toBeInTheDocument();
  });
}

function inspectSearchParams(_x3) {
  return _inspectSearchParams.apply(this, arguments);
}

function _inspectSearchParams() {
  _inspectSearchParams = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(fn) {
    var clientSpy, mockSetup;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            clientSpy = jest.fn().mockReturnValueOnce({
              hits: {
                total: 0
              }
            });
            mockSetup = {
              start: 1528113600000,
              end: 1528977600000,
              client: {
                search: clientSpy
              },
              config: {
                get: function get() {
                  return 'myIndex';
                },
                has: function has() {
                  return true;
                }
              },
              uiFiltersES: [{
                term: {
                  'service.environment': 'prod'
                }
              }]
            };
            _context2.prev = 2;
            _context2.next = 5;
            return fn(mockSetup);

          case 5:
            _context2.next = 9;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](2);

          case 9:
            return _context2.abrupt("return", {
              params: clientSpy.mock.calls[0][0],
              teardown: function teardown() {
                return clientSpy.mockClear();
              }
            });

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[2, 7]]);
  }));
  return _inspectSearchParams.apply(this, arguments);
}