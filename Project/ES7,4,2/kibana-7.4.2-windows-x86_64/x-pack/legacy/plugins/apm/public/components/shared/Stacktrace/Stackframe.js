"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Stackframe = Stackframe;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _variables = require("../../../style/variables");

var _FrameHeading = require("../Stacktrace/FrameHeading");

var _Context = require("./Context");

var _Variables = require("./Variables");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  font-family: ", ";\n  font-size: ", ";\n  border: 1px solid ", ";\n  border-radius: ", ";\n  background: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  border-bottom: 1px solid ", ";\n  border-radius: ", " ", " 0 0;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var CodeHeader = _styledComponents.default.div(_templateObject(), _eui_theme_light.default.euiColorLightShade, _variables.borderRadius, _variables.borderRadius);

var Container = _styledComponents.default.div(_templateObject2(), _variables.fontFamilyCode, _variables.fontSize, _eui_theme_light.default.euiColorLightShade, _variables.borderRadius, function (props) {
  return props.isLibraryFrame ? _eui_theme_light.default.euiColorEmptyShade : _eui_theme_light.default.euiColorLightestShade;
});

function Stackframe(_ref) {
  var stackframe = _ref.stackframe,
      codeLanguage = _ref.codeLanguage,
      _ref$isLibraryFrame = _ref.isLibraryFrame,
      isLibraryFrame = _ref$isLibraryFrame === void 0 ? false : _ref$isLibraryFrame;

  if (!hasLineContext(stackframe)) {
    return _react.default.createElement(_FrameHeading.FrameHeading, {
      stackframe: stackframe,
      isLibraryFrame: isLibraryFrame
    });
  }

  return _react.default.createElement(Container, {
    isLibraryFrame: isLibraryFrame
  }, _react.default.createElement(CodeHeader, null, _react.default.createElement(_FrameHeading.FrameHeading, {
    stackframe: stackframe,
    isLibraryFrame: isLibraryFrame
  })), _react.default.createElement(_Context.Context, {
    stackframe: stackframe,
    codeLanguage: codeLanguage,
    isLibraryFrame: isLibraryFrame
  }), _react.default.createElement(_Variables.Variables, {
    vars: stackframe.vars
  }));
}

function hasLineContext(stackframe) {
  return stackframe.line.hasOwnProperty('context');
}