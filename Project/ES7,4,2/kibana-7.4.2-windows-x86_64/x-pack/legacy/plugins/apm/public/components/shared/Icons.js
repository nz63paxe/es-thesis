"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Ellipsis = Ellipsis;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function Ellipsis(_ref) {
  var horizontal = _ref.horizontal;
  return _react.default.createElement(_eui.EuiIcon, {
    style: {
      transition: 'transform 0.1s',
      transform: "rotate(".concat(horizontal ? 90 : 0, "deg)")
    },
    type: "arrowRight"
  });
}