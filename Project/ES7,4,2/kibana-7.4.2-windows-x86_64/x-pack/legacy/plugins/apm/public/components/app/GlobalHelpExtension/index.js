"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GlobalHelpExtension = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _url = _interopRequireDefault(require("url"));

var _variables = require("../../../style/variables");

var _useCore = require("../../../hooks/useCore");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin: ", " 0;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject(), (0, _variables.px)(_variables.units.minus));

var GlobalHelpExtension = function GlobalHelpExtension() {
  var core = (0, _useCore.useCore)();
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(Container, null, _react.default.createElement(_eui.EuiLink, {
    href: "https://discuss.elastic.co/c/apm",
    target: "_blank",
    rel: "noopener"
  }, _i18n.i18n.translate('xpack.apm.feedbackMenu.provideFeedbackTitle', {
    defaultMessage: 'Provide feedback for APM'
  }))), _react.default.createElement(Container, null, _react.default.createElement(_eui.EuiLink, {
    href: _url.default.format({
      pathname: core.http.basePath.prepend('/app/kibana'),
      hash: '/management/elasticsearch/upgrade_assistant'
    })
  }, _i18n.i18n.translate('xpack.apm.helpMenu.upgradeAssistantLink', {
    defaultMessage: 'Upgrade assistant'
  }))));
};

exports.GlobalHelpExtension = GlobalHelpExtension;