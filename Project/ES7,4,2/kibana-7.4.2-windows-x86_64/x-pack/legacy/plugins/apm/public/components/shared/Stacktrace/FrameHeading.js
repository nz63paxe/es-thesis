"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FrameHeading = void 0;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _variables = require("../../../style/variables");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n  color: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n  padding: ", ";\n  font-family: ", ";\n  font-size: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var FileDetails = _styledComponents.default.div(_templateObject(), _eui_theme_light.default.euiColorMediumShade, (0, _variables.px)(_variables.units.half), _variables.fontFamilyCode, _variables.fontSize);

var LibraryFrameFileDetail = _styledComponents.default.span(_templateObject2(), _eui_theme_light.default.euiColorDarkShade);

var AppFrameFileDetail = _styledComponents.default.span(_templateObject3(), _eui_theme_light.default.euiColorFullShade);

var FrameHeading = function FrameHeading(_ref) {
  var stackframe = _ref.stackframe,
      isLibraryFrame = _ref.isLibraryFrame;
  var FileDetail = isLibraryFrame ? LibraryFrameFileDetail : AppFrameFileDetail;
  var lineNumber = (stackframe != null && stackframe.line != null ? stackframe.line.number : undefined) || 0;
  return _react.default.createElement(FileDetails, null, _react.default.createElement(FileDetail, null, stackframe.filename), " in", ' ', _react.default.createElement(FileDetail, null, stackframe.function), lineNumber > 0 && _react.default.createElement(_react.Fragment, null, ' at ', _react.default.createElement(FileDetail, null, "line ", stackframe.line.number)));
};

exports.FrameHeading = FrameHeading;