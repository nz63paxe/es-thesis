"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LibraryStackFrames = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _Icons = require("../../shared/Icons");

var _Stackframe = require("./Stackframe");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  user-select: none;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LibraryFrameToggle = _styledComponents.default.div(_templateObject());

var LibraryStackFrames =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LibraryStackFrames, _React$Component);

  function LibraryStackFrames() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LibraryStackFrames);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LibraryStackFrames)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isVisible: _this.props.initialVisiblity
    });

    _defineProperty(_assertThisInitialized(_this), "onClick", function () {
      _this.setState(function (_ref) {
        var isVisible = _ref.isVisible;
        return {
          isVisible: !isVisible
        };
      });
    });

    return _this;
  }

  _createClass(LibraryStackFrames, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          stackframes = _this$props.stackframes,
          codeLanguage = _this$props.codeLanguage;
      var isVisible = this.state.isVisible;

      if (stackframes.length === 0) {
        return null;
      }

      if (stackframes.length === 1) {
        return _react.default.createElement(_Stackframe.Stackframe, {
          isLibraryFrame: true,
          codeLanguage: codeLanguage,
          stackframe: stackframes[0]
        });
      }

      return _react.default.createElement("div", null, _react.default.createElement(LibraryFrameToggle, null, _react.default.createElement(_eui.EuiLink, {
        onClick: this.onClick
      }, _react.default.createElement(_Icons.Ellipsis, {
        horizontal: isVisible
      }), ' ', _i18n.i18n.translate('xpack.apm.stacktraceTab.libraryFramesToogleButtonLabel', {
        defaultMessage: '{stackframesLength} library frames',
        values: {
          stackframesLength: stackframes.length
        }
      }))), _react.default.createElement("div", null, isVisible && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }), stackframes.map(function (stackframe, i) {
        return _react.default.createElement(_Stackframe.Stackframe, {
          key: i,
          isLibraryFrame: true,
          codeLanguage: codeLanguage,
          stackframe: stackframe
        });
      }))));
    }
  }]);

  return LibraryStackFrames;
}(_react.default.Component);

exports.LibraryStackFrames = LibraryStackFrames;