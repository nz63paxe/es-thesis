"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TransactionMetadata = TransactionMetadata;

var _react = _interopRequireDefault(require("react"));

var _sections = require("./sections");

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function TransactionMetadata(_ref) {
  var transaction = _ref.transaction;
  return _react.default.createElement(_.MetadataTable, {
    item: transaction,
    sections: _sections.TRANSACTION_METADATA_SECTIONS
  });
}