"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TransactionBreakdownGraph = void 0;

var _react = _interopRequireDefault(require("react"));

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _lodash = require("lodash");

var _i18n = require("../../../../../common/i18n");

var _TransactionLineChart = require("../../charts/TransactionCharts/TransactionLineChart");

var _formatters = require("../../../../utils/formatters");

var _variables = require("../../../../style/variables");

var _isValidCoordinateValue = require("../../../../utils/isValidCoordinateValue");

var _use_track_metric = require("../../../../../../infra/public/hooks/use_track_metric");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var tickFormatY = function tickFormatY(y) {
  return (0, _numeral.default)(y || 0).format('0 %');
};

var formatTooltipValue = function formatTooltipValue(coordinate) {
  return (0, _isValidCoordinateValue.isValidCoordinateValue)(coordinate.y) ? (0, _formatters.asPercent)(coordinate.y, 1) : _i18n.NOT_AVAILABLE_LABEL;
};

var trackHoverBreakdownChart = (0, _lodash.throttle)(function () {
  return (0, _use_track_metric.trackEvent)({
    app: 'apm',
    name: 'hover_breakdown_chart'
  });
}, 60000);

var TransactionBreakdownGraph = function TransactionBreakdownGraph(props) {
  var timeseries = props.timeseries;
  return _react.default.createElement(_TransactionLineChart.TransactionLineChart, {
    series: timeseries,
    tickFormatY: tickFormatY,
    formatTooltipValue: formatTooltipValue,
    yMax: 1,
    height: _variables.unit * 12,
    stacked: true,
    onHover: trackHoverBreakdownChart
  });
};

exports.TransactionBreakdownGraph = TransactionBreakdownGraph;