"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Waterfall = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactSticky = require("react-sticky");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _Timeline = _interopRequireDefault(require("../../../../../shared/charts/Timeline"));

var _url_helpers = require("../../../../../shared/Links/url_helpers");

var _history = require("../../../../../../utils/history");

var _SpanFlyout = require("./SpanFlyout");

var _TransactionFlyout = require("./TransactionFlyout");

var _WaterfallItem = require("./WaterfallItem");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  transition: 0.1s padding ease;\n  position: relative;\n  overflow: hidden;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject());

var TIMELINE_MARGINS = {
  top: 40,
  left: 50,
  right: 50,
  bottom: 0
};

var Waterfall =
/*#__PURE__*/
function (_Component) {
  _inherits(Waterfall, _Component);

  function Waterfall() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, Waterfall);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(Waterfall)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onOpenFlyout", function (item) {
      _this.setQueryParams({
        flyoutDetailTab: undefined,
        waterfallItemId: String(item.id)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onCloseFlyout", function () {
      _this.setQueryParams({
        flyoutDetailTab: undefined,
        waterfallItemId: undefined
      });
    });

    _defineProperty(_assertThisInitialized(_this), "renderWaterfallItem", function (item) {
      var _this$props = _this.props,
          serviceColors = _this$props.serviceColors,
          waterfall = _this$props.waterfall,
          urlParams = _this$props.urlParams;
      var errorCount = item.docType === 'transaction' ? waterfall.errorCountByTransactionId[item.transaction.transaction.id] : 0;
      return _react.default.createElement(_WaterfallItem.WaterfallItem, {
        key: item.id,
        timelineMargins: TIMELINE_MARGINS,
        color: serviceColors[item.serviceName],
        item: item,
        totalDuration: waterfall.duration,
        isSelected: item.id === urlParams.waterfallItemId,
        errorCount: errorCount,
        onClick: function onClick() {
          return _this.onOpenFlyout(item);
        }
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getFlyOut", function () {
      var _this$props2 = _this.props,
          waterfall = _this$props2.waterfall,
          urlParams = _this$props2.urlParams;
      var currentItem = urlParams.waterfallItemId && waterfall.itemsById[urlParams.waterfallItemId];

      if (!currentItem) {
        return null;
      }

      switch (currentItem.docType) {
        case 'span':
          var parentTransaction = waterfall.getTransactionById(currentItem.parentId);
          return _react.default.createElement(_SpanFlyout.SpanFlyout, {
            totalDuration: waterfall.duration,
            span: currentItem.span,
            parentTransaction: parentTransaction,
            onClose: _this.onCloseFlyout
          });

        case 'transaction':
          return _react.default.createElement(_TransactionFlyout.TransactionFlyout, {
            transaction: currentItem.transaction,
            onClose: _this.onCloseFlyout,
            traceRootDuration: waterfall.traceRootDuration,
            errorCount: currentItem.errorCount
          });

        default:
          return null;
      }
    });

    return _this;
  }

  _createClass(Waterfall, [{
    key: "render",
    value: function render() {
      var _this$props3 = this.props,
          waterfall = _this$props3.waterfall,
          exceedsMax = _this$props3.exceedsMax;
      var itemContainerHeight = 58; // TODO: This is a nasty way to calculate the height of the svg element. A better approach should be found

      var waterfallHeight = itemContainerHeight * waterfall.orderedItems.length;
      return _react.default.createElement(Container, null, exceedsMax ? _react.default.createElement(_eui.EuiCallOut, {
        color: "warning",
        size: "s",
        iconType: "alert",
        title: _i18n.i18n.translate('xpack.apm.waterfall.exceedsMax', {
          defaultMessage: 'Number of items in this trace exceed what is displayed'
        })
      }) : null, _react.default.createElement(_reactSticky.StickyContainer, null, _react.default.createElement(_Timeline.default, {
        agentMarks: this.props.agentMarks,
        duration: waterfall.duration,
        height: waterfallHeight,
        margins: TIMELINE_MARGINS
      }), _react.default.createElement("div", {
        style: {
          paddingTop: TIMELINE_MARGINS.top
        }
      }, waterfall.orderedItems.map(this.renderWaterfallItem))), this.getFlyOut());
    }
  }, {
    key: "setQueryParams",
    value: function setQueryParams(params) {
      var location = this.props.location;

      _history.history.replace(_objectSpread({}, location, {
        search: (0, _url_helpers.fromQuery)(_objectSpread({}, (0, _url_helpers.toQuery)(location.search), {}, params))
      }));
    }
  }]);

  return Waterfall;
}(_react.Component);

exports.Waterfall = Waterfall;