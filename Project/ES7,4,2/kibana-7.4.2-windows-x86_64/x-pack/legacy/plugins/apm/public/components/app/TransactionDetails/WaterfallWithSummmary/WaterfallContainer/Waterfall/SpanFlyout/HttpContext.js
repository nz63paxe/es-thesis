"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HttpContext = HttpContext;

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _variables = require("../../../../../../../style/variables");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  padding: ", " ", ";\n  background: ", ";\n  border-radius: ", ";\n  border: 1px solid ", ";\n  font-family: ", ";\n  font-size: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ContextUrl = _styledComponents.default.div(_templateObject(), (0, _variables.px)(_variables.units.half), (0, _variables.px)(_variables.unit), _eui_theme_light.default.euiColorLightestShade, _variables.borderRadius, _eui_theme_light.default.euiColorLightShade, _variables.fontFamilyCode, _variables.fontSize);

function HttpContext(_ref) {
  var httpContext = _ref.httpContext;
  var url = httpContext != null && httpContext.url != null ? httpContext.url.original : undefined;

  if (!url) {
    return null;
  }

  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h3", null, "HTTP URL")), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(ContextUrl, null, url), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }));
}