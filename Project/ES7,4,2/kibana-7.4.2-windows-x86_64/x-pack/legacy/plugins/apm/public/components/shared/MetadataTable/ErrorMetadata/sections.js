"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ERROR_METADATA_SECTIONS = void 0;

var SECTION_LABELS = _interopRequireWildcard(require("../sectionLabels"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ERROR_METADATA_SECTIONS = [{
  key: 'labels',
  label: SECTION_LABELS.LABELS,
  required: true
}, {
  key: 'http',
  label: SECTION_LABELS.HTTP
}, {
  key: 'host',
  label: SECTION_LABELS.HOST
}, {
  key: 'container',
  label: SECTION_LABELS.CONTAINER
}, {
  key: 'service',
  label: SECTION_LABELS.SERVICE
}, {
  key: 'process',
  label: SECTION_LABELS.PROCESS
}, {
  key: 'agent',
  label: SECTION_LABELS.AGENT
}, {
  key: 'url',
  label: SECTION_LABELS.URL
}, {
  key: 'user',
  label: SECTION_LABELS.USER,
  required: true
}, {
  key: 'error.custom',
  label: SECTION_LABELS.CUSTOM
}];
exports.ERROR_METADATA_SECTIONS = ERROR_METADATA_SECTIONS;