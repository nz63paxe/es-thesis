"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SpanFlyout = SpanFlyout;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _react = _interopRequireWildcard(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _DiscoverSpanLink = require("../../../../../../shared/Links/DiscoverLinks/DiscoverSpanLink");

var _Stacktrace = require("../../../../../../shared/Stacktrace");

var _FlyoutTopLevelProperties = require("../FlyoutTopLevelProperties");

var _ResponsiveFlyout = require("../ResponsiveFlyout");

var _DatabaseContext = require("./DatabaseContext");

var _HttpContext = require("./HttpContext");

var _StickySpanProperties = require("./StickySpanProperties");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-weight: bold;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TagName = _styledComponents.default.div(_templateObject());

function SpanFlyout(_ref) {
  var span = _ref.span,
      parentTransaction = _ref.parentTransaction,
      totalDuration = _ref.totalDuration,
      onClose = _ref.onClose;

  if (!span) {
    return null;
  }

  var stackframes = span.span.stacktrace;
  var codeLanguage = parentTransaction != null && parentTransaction.service != null && parentTransaction.service.language != null ? parentTransaction.service.language.name : undefined;
  var dbContext = span != null && span.span != null ? span.span.db : undefined;
  var httpContext = span != null && span.span != null ? span.span.http : undefined;
  var spanLabels = span.labels;
  var labels = (0, _lodash.keys)(spanLabels).map(function (key) {
    return {
      key: key,
      value: (0, _lodash.get)(spanLabels, key)
    };
  });
  return _react.default.createElement(_eui.EuiPortal, null, _react.default.createElement(_ResponsiveFlyout.ResponsiveFlyout, {
    onClose: onClose,
    size: "m",
    ownFocus: true
  }, _react.default.createElement(_eui.EuiFlyoutHeader, {
    hasBorder: true
  }, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h2", null, _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.spanDetailsTitle', {
    defaultMessage: 'Span details'
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_DiscoverSpanLink.DiscoverSpanLink, {
    span: span
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    iconType: "discoverApp"
  }, _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.viewSpanInDiscoverButtonLabel', {
    defaultMessage: 'View span in Discover'
  })))))), _react.default.createElement(_eui.EuiFlyoutBody, null, _react.default.createElement(_FlyoutTopLevelProperties.FlyoutTopLevelProperties, {
    transaction: parentTransaction
  }), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_StickySpanProperties.StickySpanProperties, {
    span: span,
    totalDuration: totalDuration
  }), _react.default.createElement(_eui.EuiHorizontalRule, null), _react.default.createElement(_HttpContext.HttpContext, {
    httpContext: httpContext
  }), _react.default.createElement(_DatabaseContext.DatabaseContext, {
    dbContext: dbContext
  }), _react.default.createElement(_eui.EuiTabbedContent, {
    tabs: [{
      id: 'stack-trace',
      name: _i18n.i18n.translate('xpack.apm.transactionDetails.spanFlyout.stackTraceTabLabel', {
        defaultMessage: 'Stack Trace'
      }),
      content: _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
        size: "l"
      }), _react.default.createElement(_Stacktrace.Stacktrace, {
        stackframes: stackframes,
        codeLanguage: codeLanguage
      }))
    }, {
      id: 'labels',
      name: _i18n.i18n.translate('xpack.apm.propertiesTable.tabs.labelsLabel', {
        defaultMessage: 'Labels'
      }),
      content: _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiBasicTable, {
        columns: [{
          name: '',
          field: 'key',
          render: function render(key) {
            return _react.default.createElement(TagName, null, key);
          }
        }, {
          name: '',
          field: 'value'
        }],
        items: labels
      }))
    }]
  }))));
}