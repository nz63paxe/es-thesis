"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toQuery = toQuery;
exports.fromQuery = fromQuery;

var _querystring = _interopRequireDefault(require("querystring"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function toQuery(search) {
  return search ? _querystring.default.parse(search.slice(1)) : {};
}

function fromQuery(query) {
  return _querystring.default.stringify(query, undefined, undefined, {
    encodeURIComponent: function (_encodeURIComponent) {
      function encodeURIComponent(_x) {
        return _encodeURIComponent.apply(this, arguments);
      }

      encodeURIComponent.toString = function () {
        return _encodeURIComponent.toString();
      };

      return encodeURIComponent;
    }(function (value) {
      return encodeURIComponent(value).replace(/%3A/g, ':');
    })
  });
}