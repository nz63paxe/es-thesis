"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WaterfallItem = WaterfallItem;

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _i18n = require("@kbn/i18n");

var _agent_name = require("../../../../../../../common/agent_name");

var _variables = require("../../../../../../style/variables");

var _formatters = require("../../../../../../utils/formatters");

var _ErrorCountBadge = require("../../ErrorCountBadge");

var _ErrorOverviewLink = require("../../../../../shared/Links/apm/ErrorOverviewLink");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  position: absolute;\n  right: 0;\n  display: flex;\n  align-items: center;\n  height: ", ";\n\n  /* add margin to all direct descendants */\n  & > * {\n    margin-right: ", ";\n    white-space: nowrap;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  box-sizing: border-box;\n  position: relative;\n  height: ", ";\n  min-width: 2px;\n  background-color: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n  display: block;\n  user-select: none;\n  padding-top: ", ";\n  padding-bottom: ", ";\n  margin-right: ", ";\n  margin-left: ", ";\n  border-top: 1px solid ", ";\n  background-color: ", ";\n  cursor: pointer;\n\n  &:hover {\n    background-color: ", ";\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = (0, _styledComponents.default)('div')(_templateObject(), (0, _variables.px)(_variables.units.half), (0, _variables.px)(_variables.units.plus), function (props) {
  return (0, _variables.px)(props.timelineMargins.right);
}, function (props) {
  return (0, _variables.px)(props.timelineMargins.left);
}, _eui_theme_light.default.euiColorLightShade, function (props) {
  return props.isSelected ? _eui_theme_light.default.euiColorLightestShade : 'initial';
}, _eui_theme_light.default.euiColorLightestShade);
var ItemBar = (0, _styledComponents.default)('div')(_templateObject2(), (0, _variables.px)(_variables.unit), function (props) {
  return props.color;
});

var ItemText = _styledComponents.default.span(_templateObject3(), (0, _variables.px)(_variables.units.plus), (0, _variables.px)(_variables.units.half));

function PrefixIcon(_ref) {
  var item = _ref.item;

  if (item.docType === 'span') {
    // icon for database spans
    var isDbType = item.span.span.type.startsWith('db');

    if (isDbType) {
      return _react.default.createElement(_eui.EuiIcon, {
        type: "database"
      });
    } // omit icon for other spans


    return null;
  } // icon for RUM agent transactions


  if ((0, _agent_name.isRumAgentName)(item.transaction.agent.name)) {
    return _react.default.createElement(_eui.EuiIcon, {
      type: "globe"
    });
  } // icon for other transactions


  return _react.default.createElement(_eui.EuiIcon, {
    type: "merge"
  });
}

var SpanActionToolTip = function SpanActionToolTip(_ref2) {
  var item = _ref2.item,
      children = _ref2.children;

  if (item && item.docType === 'span') {
    return _react.default.createElement(_eui.EuiToolTip, {
      content: "".concat(item.span.span.subtype, ".").concat(item.span.span.action)
    }, _react.default.createElement(_react.default.Fragment, null, children));
  }

  return _react.default.createElement(_react.default.Fragment, null, children);
};

function Duration(_ref3) {
  var item = _ref3.item;
  return _react.default.createElement(_eui.EuiText, {
    color: "subdued",
    size: "xs"
  }, (0, _formatters.asTime)(item.duration));
}

function HttpStatusCode(_ref4) {
  var item = _ref4.item;
  // http status code for transactions of type 'request'
  var httpStatusCode = item.docType === 'transaction' && item.transaction.transaction.type === 'request' ? item.transaction.transaction.result : undefined;

  if (!httpStatusCode) {
    return null;
  }

  return _react.default.createElement(_eui.EuiText, {
    size: "xs"
  }, httpStatusCode);
}

function NameLabel(_ref5) {
  var item = _ref5.item;

  if (item.docType === 'span') {
    return _react.default.createElement(_eui.EuiText, {
      size: "s"
    }, item.name);
  }

  return _react.default.createElement(_eui.EuiTitle, {
    size: "xxs"
  }, _react.default.createElement("h5", null, item.name));
}

function WaterfallItem(_ref6) {
  var timelineMargins = _ref6.timelineMargins,
      totalDuration = _ref6.totalDuration,
      item = _ref6.item,
      color = _ref6.color,
      isSelected = _ref6.isSelected,
      errorCount = _ref6.errorCount,
      onClick = _ref6.onClick;

  if (!totalDuration) {
    return null;
  }

  var width = item.duration / totalDuration * 100;
  var left = (item.offset + item.skew) / totalDuration * 100;

  var tooltipContent = _i18n.i18n.translate('xpack.apm.transactionDetails.errorsOverviewLinkTooltip', {
    values: {
      errorCount: errorCount
    },
    defaultMessage: '{errorCount, plural, one {View 1 related error} other {View # related errors}}'
  });

  return _react.default.createElement(Container, {
    type: item.docType,
    timelineMargins: timelineMargins,
    isSelected: isSelected,
    onClick: onClick
  }, _react.default.createElement(ItemBar // using inline styles instead of props to avoid generating a css class for each item
  , {
    style: {
      left: "".concat(left, "%"),
      width: "".concat(width, "%")
    },
    color: color,
    type: item.docType
  }), _react.default.createElement(ItemText // using inline styles instead of props to avoid generating a css class for each item
  , {
    style: {
      minWidth: "".concat(Math.max(100 - left, 0), "%")
    }
  }, _react.default.createElement(SpanActionToolTip, {
    item: item
  }, _react.default.createElement(PrefixIcon, {
    item: item
  })), _react.default.createElement(HttpStatusCode, {
    item: item
  }), _react.default.createElement(NameLabel, {
    item: item
  }), errorCount > 0 && item.docType === 'transaction' ? _react.default.createElement(_ErrorOverviewLink.ErrorOverviewLink, {
    serviceName: item.transaction.service.name,
    query: {
      kuery: encodeURIComponent("trace.id : \"".concat(item.transaction.trace.id, "\" and transaction.id : \"").concat(item.transaction.transaction.id, "\""))
    },
    color: "danger",
    style: {
      textDecoration: 'none'
    }
  }, _react.default.createElement(_eui.EuiToolTip, {
    content: tooltipContent
  }, _react.default.createElement(_ErrorCountBadge.ErrorCountBadge, {
    errorCount: errorCount,
    onClick: function onClick(event) {
      event.stopPropagation();
    },
    onClickAriaLabel: tooltipContent
  }))) : null, _react.default.createElement(Duration, {
    item: item
  })));
}