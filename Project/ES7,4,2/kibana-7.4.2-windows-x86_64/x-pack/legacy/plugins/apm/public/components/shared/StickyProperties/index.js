"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StickyProperties = StickyProperties;

var _eui = require("@elastic/eui");

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _i18n = require("../../../../common/i18n");

var _variables = require("../../../style/variables");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\n  display: inline-block;\n  line-height: ", ";\n  ", ";\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  display: inline-block;\n  line-height: ", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: ", ";\n  font-size: ", ";\n  color: ", ";\n\n  span {\n    cursor: help;\n  }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  font-family: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var TooltipFieldName = _styledComponents.default.span(_templateObject(), _variables.fontFamilyCode);

var PropertyLabel = _styledComponents.default.div(_templateObject2(), (0, _variables.px)(_variables.units.half), _variables.fontSizes.small, _eui_theme_light.default.euiColorMediumShade);

PropertyLabel.displayName = 'PropertyLabel';

var PropertyValueDimmed = _styledComponents.default.span(_templateObject3(), _eui_theme_light.default.euiColorMediumShade);

var propertyValueLineHeight = 1.2;

var PropertyValue = _styledComponents.default.div(_templateObject4(), propertyValueLineHeight);

PropertyValue.displayName = 'PropertyValue';

var PropertyValueTruncated = _styledComponents.default.span(_templateObject5(), propertyValueLineHeight, (0, _variables.truncate)('100%'));

function TimestampValue(_ref) {
  var timestamp = _ref.timestamp;
  var time = (0, _moment.default)(timestamp);
  var timeAgo = timestamp ? time.fromNow() : _i18n.NOT_AVAILABLE_LABEL;
  var timestampFull = timestamp ? time.format('MMMM Do YYYY, HH:mm:ss.SSS') : _i18n.NOT_AVAILABLE_LABEL;
  return _react.default.createElement(PropertyValue, null, timeAgo, " ", _react.default.createElement(PropertyValueDimmed, null, "(", timestampFull, ")"));
}

function getPropertyLabel(_ref2) {
  var fieldName = _ref2.fieldName,
      label = _ref2.label;

  if (fieldName) {
    return _react.default.createElement(PropertyLabel, null, _react.default.createElement(_eui.EuiToolTip, {
      content: _react.default.createElement(TooltipFieldName, null, fieldName)
    }, _react.default.createElement("span", null, label)));
  }

  return _react.default.createElement(PropertyLabel, null, label);
}

function getPropertyValue(_ref3) {
  var val = _ref3.val,
      fieldName = _ref3.fieldName,
      _ref3$truncated = _ref3.truncated,
      truncated = _ref3$truncated === void 0 ? false : _ref3$truncated;

  if (fieldName === '@timestamp') {
    return _react.default.createElement(TimestampValue, {
      timestamp: val
    });
  }

  if (truncated) {
    return _react.default.createElement(_eui.EuiToolTip, {
      content: String(val)
    }, _react.default.createElement(PropertyValueTruncated, null, String(val)));
  }

  return _react.default.createElement(PropertyValue, null, val);
}

function StickyProperties(_ref4) {
  var stickyProperties = _ref4.stickyProperties;

  /**
   * Note: the padding and margin styles here are strange because
   * EUI flex groups and items have a default "gutter" applied that
   * won't allow percentage widths to line up correctly, so we have
   * to turn the gutter off with gutterSize: none. When we do that,
   * the top/bottom spacing *also* collapses, so we have to add
   * padding between each item without adding it to the outside of
   * the flex group itself.
   *
   * Hopefully we can make EUI handle this better and remove all this.
   */
  var itemStyles = {
    padding: '1em 1em 1em 0'
  };
  var groupStyles = {
    marginTop: '-1em',
    marginBottom: '-1em'
  };
  return _react.default.createElement(_eui.EuiFlexGroup, {
    wrap: true,
    gutterSize: "none",
    style: groupStyles
  }, stickyProperties && stickyProperties.map(function (_ref5, i) {
    var _ref5$width = _ref5.width,
        width = _ref5$width === void 0 ? 0 : _ref5$width,
        prop = _objectWithoutProperties(_ref5, ["width"]);

    return _react.default.createElement(_eui.EuiFlexItem, {
      key: i,
      style: _objectSpread({
        minWidth: width
      }, itemStyles),
      grow: false
    }, getPropertyLabel(prop), getPropertyValue(prop));
  }));
}