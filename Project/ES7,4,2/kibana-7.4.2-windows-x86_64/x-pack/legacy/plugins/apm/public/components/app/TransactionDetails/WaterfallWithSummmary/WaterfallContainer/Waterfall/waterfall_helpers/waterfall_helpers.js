"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getClockSkew = getClockSkew;
exports.getOrderedWaterfallItems = getOrderedWaterfallItems;
exports.getWaterfall = getWaterfall;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function getTransactionItem(transaction, errorsPerTransaction) {
  return {
    id: transaction.transaction.id,
    parentId: transaction.parent && transaction.parent.id,
    serviceName: transaction.service.name,
    name: transaction.transaction.name,
    duration: transaction.transaction.duration.us,
    timestamp: transaction.timestamp.us,
    offset: 0,
    skew: 0,
    docType: 'transaction',
    transaction: transaction,
    errorCount: errorsPerTransaction[transaction.transaction.id] || 0
  };
}

function getSpanItem(span) {
  return {
    id: span.span.id,
    parentId: span.parent && span.parent.id,
    serviceName: span.service.name,
    name: span.span.name,
    duration: span.span.duration.us,
    timestamp: span.timestamp.us,
    offset: 0,
    skew: 0,
    docType: 'span',
    span: span
  };
}

function getClockSkew(item, parentItem) {
  if (!parentItem) {
    return 0;
  }

  switch (item.docType) {
    // don't calculate skew for spans. Just use parent's skew
    case 'span':
      return parentItem.skew;
    // transaction is the inital entry in a service. Calculate skew for this, and it will be propogated to all child spans

    case 'transaction':
      {
        var parentStart = parentItem.timestamp + parentItem.skew; // determine if child starts before the parent

        var offsetStart = parentStart - item.timestamp;

        if (offsetStart > 0) {
          var latency = Math.max(parentItem.duration - item.duration, 0) / 2;
          return offsetStart + latency;
        } // child transaction starts after parent thus no adjustment is needed


        return 0;
      }
  }
}

function getOrderedWaterfallItems(childrenByParentId, entryTransactionItem) {
  var visitedWaterfallItemSet = new Set();

  function getSortedChildren(item, parentItem) {
    if (visitedWaterfallItemSet.has(item)) {
      return [];
    }

    visitedWaterfallItemSet.add(item);
    var children = (0, _lodash.sortBy)(childrenByParentId[item.id] || [], 'timestamp');
    item.childIds = children.map(function (child) {
      return child.id;
    });
    item.offset = item.timestamp - entryTransactionItem.timestamp;
    item.skew = getClockSkew(item, parentItem);
    var deepChildren = (0, _lodash.flatten)(children.map(function (child) {
      return getSortedChildren(child, item);
    }));
    return [item].concat(_toConsumableArray(deepChildren));
  }

  return getSortedChildren(entryTransactionItem);
}

function getTraceRoot(childrenByParentId) {
  var item = (0, _lodash.first)(childrenByParentId.root);

  if (item && item.docType === 'transaction') {
    return item.transaction;
  }
}

function getServices(items) {
  var serviceNames = items.map(function (item) {
    return item.serviceName;
  });
  return (0, _lodash.uniq)(serviceNames);
}

function getServiceColors(services) {
  var assignedColors = [_eui_theme_light.default.euiColorVis1, _eui_theme_light.default.euiColorVis0, _eui_theme_light.default.euiColorVis3, _eui_theme_light.default.euiColorVis2, _eui_theme_light.default.euiColorVis6, _eui_theme_light.default.euiColorVis7, _eui_theme_light.default.euiColorVis5];
  return (0, _lodash.zipObject)(services, assignedColors);
}

function getDuration(items) {
  if (items.length === 0) {
    return 0;
  }

  var timestampStart = items[0].timestamp;
  var timestampEnd = Math.max.apply(Math, _toConsumableArray(items.map(function (item) {
    return item.timestamp + item.duration + item.skew;
  })));
  return timestampEnd - timestampStart;
}

function createGetTransactionById(itemsById) {
  return function (id) {
    if (!id) {
      return undefined;
    }

    var item = itemsById[id];
    var isTransaction = (item != null ? item.docType : undefined) === 'transaction';

    if (isTransaction) {
      return item.transaction;
    }
  };
}

function getWaterfall(_ref, entryTransactionId) {
  var trace = _ref.trace,
      errorsPerTransaction = _ref.errorsPerTransaction;

  if ((0, _lodash.isEmpty)(trace.items) || !entryTransactionId) {
    return {
      services: [],
      duration: 0,
      orderedItems: [],
      itemsById: {},
      getTransactionById: function getTransactionById() {
        return undefined;
      },
      errorCountByTransactionId: errorsPerTransaction,
      serviceColors: {}
    };
  }

  var waterfallItems = trace.items.map(function (traceItem) {
    var docType = traceItem.processor.event;

    switch (docType) {
      case 'span':
        return getSpanItem(traceItem);

      case 'transaction':
        return getTransactionItem(traceItem, errorsPerTransaction);
    }
  });
  var childrenByParentId = (0, _lodash.groupBy)(waterfallItems, function (item) {
    return item.parentId ? item.parentId : 'root';
  });
  var entryTransactionItem = waterfallItems.find(function (waterfallItem) {
    return waterfallItem.docType === 'transaction' && waterfallItem.id === entryTransactionId;
  });
  var itemsById = (0, _lodash.indexBy)(waterfallItems, 'id');
  var orderedItems = entryTransactionItem ? getOrderedWaterfallItems(childrenByParentId, entryTransactionItem) : [];
  var traceRoot = getTraceRoot(childrenByParentId);
  var duration = getDuration(orderedItems);
  var traceRootDuration = traceRoot && traceRoot.transaction.duration.us;
  var services = getServices(orderedItems);
  var getTransactionById = createGetTransactionById(itemsById);
  var serviceColors = getServiceColors(services);
  var entryTransaction = getTransactionById(entryTransactionId);
  return {
    entryTransaction: entryTransaction,
    traceRoot: traceRoot,
    traceRootDuration: traceRootDuration,
    duration: duration,
    services: services,
    orderedItems: orderedItems,
    itemsById: itemsById,
    getTransactionById: getTransactionById,
    errorCountByTransactionId: errorsPerTransaction,
    serviceColors: serviceColors
  };
}