"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KueryBar = KueryBar;

var _react = _interopRequireWildcard(require("react"));

var _lodash = require("lodash");

var _eui = require("@elastic/eui");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _react2 = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _autocomplete_providers = require("ui/autocomplete_providers");

var _index_patterns = require("ui/index_patterns");

var _esQuery = require("@kbn/es-query");

var _url_helpers = require("../Links/url_helpers");

var _KibanaLink = require("../Links/KibanaLink");

var _Typeahead = require("./Typeahead");

var _get_bool_filter = require("./get_bool_filter");

var _useLocation = require("../../../hooks/useLocation");

var _useUrlParams2 = require("../../../hooks/useUrlParams");

var _history = require("../../../utils/history");

var _useMatchedRoutes = require("../../../hooks/useMatchedRoutes");

var _route_names = require("../../app/Main/route_config/route_names");

var _useCore = require("../../../hooks/useCore");

var _savedObjects = require("../../../services/rest/savedObjects");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  margin-bottom: 10px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Container = _styledComponents.default.div(_templateObject());

function convertKueryToEsQuery(kuery, indexPattern) {
  var ast = (0, _esQuery.fromKueryExpression)(kuery);
  return (0, _esQuery.toElasticsearchQuery)(ast, indexPattern);
}

function getAPMIndexPatternForKuery() {
  return _getAPMIndexPatternForKuery.apply(this, arguments);
}

function _getAPMIndexPatternForKuery() {
  _getAPMIndexPatternForKuery = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3() {
    var apmIndexPattern;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _savedObjects.getAPMIndexPattern)();

          case 2:
            apmIndexPattern = _context3.sent;

            if (apmIndexPattern) {
              _context3.next = 5;
              break;
            }

            return _context3.abrupt("return");

          case 5:
            return _context3.abrupt("return", (0, _index_patterns.getFromSavedObject)(apmIndexPattern));

          case 6:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _getAPMIndexPatternForKuery.apply(this, arguments);
}

function getSuggestions(query, selectionStart, apmIndexPattern, boolFilter) {
  var autocompleteProvider = (0, _autocomplete_providers.getAutocompleteProvider)('kuery');

  if (!autocompleteProvider) {
    return [];
  }

  var config = {
    get: function get() {
      return true;
    }
  };
  var getAutocompleteSuggestions = autocompleteProvider({
    config: config,
    indexPatterns: [apmIndexPattern],
    boolFilter: boolFilter
  });
  return getAutocompleteSuggestions({
    query: query,
    selectionStart: selectionStart,
    selectionEnd: selectionStart
  });
}

function KueryBar() {
  var _exampleMap;

  var core = (0, _useCore.useCore)();

  var _useState = (0, _react.useState)({
    indexPattern: null,
    suggestions: [],
    isLoadingIndexPattern: true,
    isLoadingSuggestions: false
  }),
      _useState2 = _slicedToArray(_useState, 2),
      state = _useState2[0],
      setState = _useState2[1];

  var _useUrlParams = (0, _useUrlParams2.useUrlParams)(),
      urlParams = _useUrlParams.urlParams;

  var location = (0, _useLocation.useLocation)();
  var matchedRoutes = (0, _useMatchedRoutes.useMatchedRoutes)();
  var apmIndexPatternTitle = core.injectedMetadata.getInjectedVar('apmIndexPatternTitle');
  var indexPatternMissing = !state.isLoadingIndexPattern && !state.indexPattern;
  var currentRequestCheck;
  var exampleMap = (_exampleMap = {}, _defineProperty(_exampleMap, _route_names.RouteName.TRANSACTIONS, 'transaction.duration.us > 300000'), _defineProperty(_exampleMap, _route_names.RouteName.ERRORS, 'http.response.status_code >= 400'), _defineProperty(_exampleMap, _route_names.RouteName.METRICS, 'process.pid = "1234"'), _exampleMap); // sets queryExample to the first matched example query, else default example

  var queryExample = matchedRoutes.map(function (_ref) {
    var name = _ref.name;
    return exampleMap[name];
  }).find(Boolean) || 'transaction.duration.us > 300000 AND http.response.status_code >= 400';
  (0, _react.useEffect)(function () {
    var didCancel = false;

    function loadIndexPattern() {
      return _loadIndexPattern.apply(this, arguments);
    }

    function _loadIndexPattern() {
      _loadIndexPattern = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var indexPattern;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                setState(function (value) {
                  return _objectSpread({}, value, {
                    isLoadingIndexPattern: true
                  });
                });
                _context.next = 3;
                return getAPMIndexPatternForKuery();

              case 3:
                indexPattern = _context.sent;

                if (!didCancel) {
                  _context.next = 6;
                  break;
                }

                return _context.abrupt("return");

              case 6:
                if (!indexPattern) {
                  setState(function (value) {
                    return _objectSpread({}, value, {
                      isLoadingIndexPattern: false
                    });
                  });
                } else {
                  setState(function (value) {
                    return _objectSpread({}, value, {
                      indexPattern: indexPattern,
                      isLoadingIndexPattern: false
                    });
                  });
                }

              case 7:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));
      return _loadIndexPattern.apply(this, arguments);
    }

    loadIndexPattern();
    return function () {
      didCancel = true;
    };
  }, []);

  function onChange(_x, _x2) {
    return _onChange.apply(this, arguments);
  }

  function _onChange() {
    _onChange = _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2(inputValue, selectionStart) {
      var indexPattern, currentRequest, boolFilter, suggestions;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              indexPattern = state.indexPattern;

              if (!(indexPattern === null)) {
                _context2.next = 3;
                break;
              }

              return _context2.abrupt("return");

            case 3:
              setState(_objectSpread({}, state, {
                suggestions: [],
                isLoadingSuggestions: true
              }));
              currentRequest = (0, _lodash.uniqueId)();
              currentRequestCheck = currentRequest;
              boolFilter = (0, _get_bool_filter.getBoolFilter)(urlParams);
              _context2.prev = 7;
              _context2.next = 10;
              return getSuggestions(inputValue, selectionStart, indexPattern, boolFilter);

            case 10:
              _context2.t0 = function (suggestion) {
                return !(0, _lodash.startsWith)(suggestion.text, 'span.');
              };

              suggestions = _context2.sent.filter(_context2.t0).slice(0, 15);

              if (!(currentRequest !== currentRequestCheck)) {
                _context2.next = 14;
                break;
              }

              return _context2.abrupt("return");

            case 14:
              setState(_objectSpread({}, state, {
                suggestions: suggestions,
                isLoadingSuggestions: false
              }));
              _context2.next = 20;
              break;

            case 17:
              _context2.prev = 17;
              _context2.t1 = _context2["catch"](7);
              // eslint-disable-next-line no-console
              console.error('Error while fetching suggestions', _context2.t1);

            case 20:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, null, [[7, 17]]);
    }));
    return _onChange.apply(this, arguments);
  }

  function onSubmit(inputValue) {
    var indexPattern = state.indexPattern;

    if (indexPattern === null) {
      return;
    }

    try {
      var res = convertKueryToEsQuery(inputValue, indexPattern);

      if (!res) {
        return;
      }

      _history.history.push(_objectSpread({}, location, {
        search: (0, _url_helpers.fromQuery)(_objectSpread({}, (0, _url_helpers.toQuery)(location.search), {
          kuery: encodeURIComponent(inputValue.trim())
        }))
      }));
    } catch (e) {
      console.log('Invalid kuery syntax'); // eslint-disable-line no-console
    }
  }

  return _react.default.createElement(Container, null, _react.default.createElement(_Typeahead.Typeahead, {
    disabled: indexPatternMissing,
    isLoading: state.isLoadingSuggestions,
    initialValue: urlParams.kuery,
    onChange: onChange,
    onSubmit: onSubmit,
    suggestions: state.suggestions,
    queryExample: queryExample
  }), indexPatternMissing && _react.default.createElement(_eui.EuiCallOut, {
    style: {
      display: 'inline-block',
      marginTop: '10px'
    },
    title: _react.default.createElement("div", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.apm.kueryBar.indexPatternMissingWarningMessage",
      defaultMessage: "There's no APM index pattern with the title {apmIndexPatternTitle} available. To use the Query bar, please choose to import the APM index pattern via the {setupInstructionsLink}.",
      values: {
        apmIndexPatternTitle: "\"".concat(apmIndexPatternTitle, "\""),
        setupInstructionsLink: _react.default.createElement(_KibanaLink.KibanaLink, {
          path: "/home/tutorial/apm"
        }, _i18n.i18n.translate('xpack.apm.kueryBar.setupInstructionsLinkLabel', {
          defaultMessage: 'Setup Instructions'
        }))
      }
    })),
    color: "warning",
    iconType: "alert",
    size: "s"
  }));
}