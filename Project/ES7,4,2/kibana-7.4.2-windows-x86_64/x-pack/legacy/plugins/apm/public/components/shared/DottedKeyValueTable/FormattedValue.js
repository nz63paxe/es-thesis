"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FormattedKey = FormattedKey;
exports.FormattedValue = FormattedValue;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

var _lodash = require("lodash");

var _react = _interopRequireDefault(require("react"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _i18n = require("../../../../common/i18n");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n  text-align: left;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var EmptyValue = _styledComponents.default.span(_templateObject(), _eui_theme_light.default.euiColorMediumShade);

function FormattedKey(_ref) {
  var k = _ref.k,
      value = _ref.value;

  if (value == null) {
    return _react.default.createElement(EmptyValue, null, k);
  }

  return _react.default.createElement(_react.default.Fragment, null, k);
}

function FormattedValue(_ref2) {
  var value = _ref2.value;

  if ((0, _lodash.isObject)(value)) {
    return _react.default.createElement("pre", null, JSON.stringify(value, null, 4));
  } else if ((0, _lodash.isBoolean)(value) || (0, _lodash.isNumber)(value)) {
    return _react.default.createElement(_react.default.Fragment, null, String(value));
  } else if (!value) {
    return _react.default.createElement(EmptyValue, null, _i18n.NOT_AVAILABLE_LABEL);
  }

  return _react.default.createElement(_react.default.Fragment, null, value);
}