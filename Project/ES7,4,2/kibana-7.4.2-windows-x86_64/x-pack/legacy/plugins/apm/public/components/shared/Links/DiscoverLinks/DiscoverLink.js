"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DiscoverLink = DiscoverLink;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _url = _interopRequireDefault(require("url"));

var _risonNode = _interopRequireDefault(require("rison-node"));

var _useAPMIndexPattern = require("../../../../hooks/useAPMIndexPattern");

var _useLocation = require("../../../../hooks/useLocation");

var _rison_helpers = require("../rison_helpers");

var _useCore = require("../../../../hooks/useCore");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function DiscoverLink(_ref) {
  var _ref$query = _ref.query,
      query = _ref$query === void 0 ? {} : _ref$query,
      rest = _objectWithoutProperties(_ref, ["query"]);

  var core = (0, _useCore.useCore)();
  var apmIndexPattern = (0, _useAPMIndexPattern.useAPMIndexPattern)();
  var location = (0, _useLocation.useLocation)();

  if (!apmIndexPattern.id) {
    return null;
  }

  var risonQuery = {
    _g: (0, _rison_helpers.getTimepickerRisonData)(location.search),
    _a: _objectSpread({}, query._a, {
      index: apmIndexPattern.id
    })
  };

  var href = _url.default.format({
    pathname: core.http.basePath.prepend('/app/kibana'),
    hash: "/discover?_g=".concat(_risonNode.default.encode(risonQuery._g), "&_a=").concat(_risonNode.default.encode(risonQuery._a))
  });

  return _react.default.createElement(_eui.EuiLink, _extends({}, rest, {
    href: href
  }));
}