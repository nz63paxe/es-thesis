"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WaterfallWithSummmary = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

var _lodash = require("lodash");

var _TransactionDetailLink = require("../../../shared/Links/apm/TransactionDetailLink");

var _TransactionActionMenu = require("../../../shared/TransactionActionMenu/TransactionActionMenu");

var _StickyTransactionProperties = require("./StickyTransactionProperties");

var _TransactionTabs = require("./TransactionTabs");

var _LoadingStatePrompt = require("../../../shared/LoadingStatePrompt");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function MaybeViewTraceLink(_ref) {
  var transaction = _ref.transaction,
      waterfall = _ref.waterfall;

  var viewFullTraceButtonLabel = _i18n.i18n.translate('xpack.apm.transactionDetails.viewFullTraceButtonLabel', {
    defaultMessage: 'View full trace'
  }); // the traceroot cannot be found, so we cannot link to it


  if (!waterfall.traceRoot) {
    return _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_eui.EuiToolTip, {
      content: _i18n.i18n.translate('xpack.apm.transactionDetails.noTraceParentButtonTooltip', {
        defaultMessage: 'The trace parent cannot be found'
      })
    }, _react.default.createElement(_eui.EuiButton, {
      iconType: "apmTrace",
      disabled: true
    }, viewFullTraceButtonLabel)));
  }

  var isRoot = transaction.transaction.id === waterfall.traceRoot.transaction.id; // the user is already viewing the full trace, so don't link to it

  if (isRoot) {
    return _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_eui.EuiToolTip, {
      content: _i18n.i18n.translate('xpack.apm.transactionDetails.viewingFullTraceButtonTooltip', {
        defaultMessage: 'Currently viewing the full trace'
      })
    }, _react.default.createElement(_eui.EuiButton, {
      iconType: "apmTrace",
      disabled: true
    }, viewFullTraceButtonLabel))); // the user is viewing a zoomed in version of the trace. Link to the full trace
  } else {
    var traceRoot = waterfall.traceRoot;
    return _react.default.createElement(_eui.EuiFlexItem, {
      grow: false
    }, _react.default.createElement(_TransactionDetailLink.TransactionDetailLink, {
      serviceName: traceRoot.service.name,
      transactionId: traceRoot.transaction.id,
      traceId: traceRoot.trace.id,
      transactionName: traceRoot.transaction.name,
      transactionType: traceRoot.transaction.type
    }, _react.default.createElement(_eui.EuiButton, {
      iconType: "apmTrace"
    }, viewFullTraceButtonLabel)));
  }
}

var WaterfallWithSummmary = function WaterfallWithSummmary(_ref2) {
  var urlParams = _ref2.urlParams,
      location = _ref2.location,
      waterfall = _ref2.waterfall,
      exceedsMax = _ref2.exceedsMax,
      isLoading = _ref2.isLoading;
  var entryTransaction = waterfall.entryTransaction;

  if (!entryTransaction) {
    var content = isLoading ? _react.default.createElement(_LoadingStatePrompt.LoadingStatePrompt, null) : _react.default.createElement(_eui.EuiEmptyPrompt, {
      title: _react.default.createElement("div", null, _i18n.i18n.translate('xpack.apm.transactionDetails.traceNotFound', {
        defaultMessage: 'The selected trace cannot be found'
      })),
      titleSize: "s"
    });
    return _react.default.createElement(_eui.EuiPanel, {
      paddingSize: "m"
    }, content);
  }

  return _react.default.createElement(_eui.EuiPanel, {
    paddingSize: "m"
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement("h5", null, _i18n.i18n.translate('xpack.apm.transactionDetails.traceSampleTitle', {
    defaultMessage: 'Trace sample'
  })))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_TransactionActionMenu.TransactionActionMenu, {
    transaction: entryTransaction
  })), _react.default.createElement(MaybeViewTraceLink, {
    transaction: entryTransaction,
    waterfall: waterfall
  })))), _react.default.createElement(_StickyTransactionProperties.StickyTransactionProperties, {
    errorCount: (0, _lodash.sum)(Object.values(waterfall.errorCountByTransactionId)),
    transaction: entryTransaction,
    totalDuration: waterfall.traceRootDuration
  }), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_TransactionTabs.TransactionTabs, {
    transaction: entryTransaction,
    location: location,
    urlParams: urlParams,
    waterfall: waterfall,
    exceedsMax: exceedsMax
  }));
};

exports.WaterfallWithSummmary = WaterfallWithSummmary;