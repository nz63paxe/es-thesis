"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMetricsProjection = getMetricsProjection;

var _elasticsearch_fieldnames = require("../elasticsearch_fieldnames");

var _range_filter = require("../../server/lib/helpers/range_filter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getMetricsProjection({
  setup,
  serviceName
}) {
  const {
    start,
    end,
    uiFiltersES,
    config
  } = setup;
  return {
    index: config.get('apm_oss.metricsIndices'),
    body: {
      query: {
        bool: {
          filter: [{
            term: {
              [_elasticsearch_fieldnames.SERVICE_NAME]: serviceName
            }
          }, {
            term: {
              [_elasticsearch_fieldnames.PROCESSOR_EVENT]: 'metric'
            }
          }, {
            range: (0, _range_filter.rangeFilter)(start, end)
          }, ...uiFiltersES]
        }
      }
    }
  };
}