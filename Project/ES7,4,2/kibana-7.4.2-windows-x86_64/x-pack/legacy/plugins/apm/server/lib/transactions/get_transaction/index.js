"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTransaction = getTransaction;

var _elasticsearch_fieldnames = require("../../../../common/elasticsearch_fieldnames");

var _range_filter = require("../../helpers/range_filter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getTransaction(transactionId, traceId, setup) {
  const {
    start,
    end,
    uiFiltersES,
    client,
    config
  } = setup;
  const params = {
    index: config.get('apm_oss.transactionIndices'),
    body: {
      size: 1,
      query: {
        bool: {
          filter: [{
            term: {
              [_elasticsearch_fieldnames.PROCESSOR_EVENT]: 'transaction'
            }
          }, {
            term: {
              [_elasticsearch_fieldnames.TRANSACTION_ID]: transactionId
            }
          }, {
            term: {
              [_elasticsearch_fieldnames.TRACE_ID]: traceId
            }
          }, {
            range: (0, _range_filter.rangeFilter)(start, end)
          }, ...uiFiltersES]
        }
      }
    }
  };
  const resp = await client.search(params);
  return resp != null && resp.hits != null && resp.hits.hits != null && resp.hits.hits[0] != null ? resp.hits.hits[0]._source : undefined;
}