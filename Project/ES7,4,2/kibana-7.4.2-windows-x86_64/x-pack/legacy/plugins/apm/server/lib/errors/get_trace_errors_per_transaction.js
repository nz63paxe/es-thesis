"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTraceErrorsPerTransaction = getTraceErrorsPerTransaction;

var _elasticsearch_fieldnames = require("../../../common/elasticsearch_fieldnames");

var _range_filter = require("../helpers/range_filter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getTraceErrorsPerTransaction(traceId, setup) {
  const {
    start,
    end,
    client,
    config
  } = setup;
  const params = {
    index: config.get('apm_oss.errorIndices'),
    body: {
      size: 0,
      query: {
        bool: {
          filter: [{
            term: {
              [_elasticsearch_fieldnames.TRACE_ID]: traceId
            }
          }, {
            term: {
              [_elasticsearch_fieldnames.PROCESSOR_EVENT]: 'error'
            }
          }, {
            range: (0, _range_filter.rangeFilter)(start, end)
          }]
        }
      },
      aggs: {
        transactions: {
          terms: {
            field: _elasticsearch_fieldnames.TRANSACTION_ID
          }
        }
      }
    }
  };
  const resp = await client.search(params);
  return ((resp.aggregations != null && resp.aggregations.transactions != null ? resp.aggregations.transactions.buckets : undefined) || []).reduce((acc, bucket) => ({ ...acc,
    [bucket.key]: bucket.doc_count
  }), {});
}