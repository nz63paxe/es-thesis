"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isApmIndex = isApmIndex;
exports.getESClient = getESClient;

var _lodash = require("lodash");

var _elasticsearch_fieldnames = require("../../../common/elasticsearch_fieldnames");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/* eslint-disable no-console */
function getApmIndices(config) {
  return [config.get('apm_oss.errorIndices'), config.get('apm_oss.metricsIndices'), config.get('apm_oss.onboardingIndices'), config.get('apm_oss.sourcemapIndices'), config.get('apm_oss.spanIndices'), config.get('apm_oss.transactionIndices')];
}

function isApmIndex(apmIndices, indexParam) {
  if ((0, _lodash.isString)(indexParam)) {
    return apmIndices.includes(indexParam);
  } else if (Array.isArray(indexParam)) {
    // return false if at least one of the indices is not an APM index
    return indexParam.every(index => apmIndices.includes(index));
  }

  return false;
}

function addFilterForLegacyData(apmIndices, params, {
  includeLegacyData = false
} = {}) {
  // search across all data (including data)
  if (includeLegacyData || !isApmIndex(apmIndices, params.index)) {
    return params;
  }

  const nextParams = (0, _lodash.cloneDeep)(params);

  if (!(0, _lodash.has)(nextParams, 'body.query.bool.filter')) {
    (0, _lodash.set)(nextParams, 'body.query.bool.filter', []);
  } // add filter for omitting pre-7.x data


  nextParams.body.query.bool.filter.push({
    range: {
      [_elasticsearch_fieldnames.OBSERVER_VERSION_MAJOR]: {
        gte: 7
      }
    }
  });
  return nextParams;
} // add additional params for search (aka: read) requests


async function getParamsForSearchRequest(req, params, apmOptions) {
  const config = req.server.config();
  const uiSettings = req.getUiSettingsService();
  const apmIndices = getApmIndices(config);
  const includeFrozen = await uiSettings.get('search:includeFrozen');
  return { ...addFilterForLegacyData(apmIndices, params, apmOptions),
    // filter out pre-7.0 data
    ignore_throttled: !includeFrozen,
    // whether to query frozen indices or not
    rest_total_hits_as_int: true // ensure that ES returns accurate hits.total with pre-6.6 format

  };
}

function getESClient(req) {
  const cluster = req.server.plugins.elasticsearch.getCluster('data');
  const query = req.query;
  return {
    search: async (params, apmOptions) => {
      const nextParams = await getParamsForSearchRequest(req, params, apmOptions);

      if (query._debug) {
        console.log(`--DEBUG ES QUERY--`);
        console.log(`${req.method.toUpperCase()} ${req.url.pathname} ${JSON.stringify(query)}`);
        console.log(`GET ${nextParams.index}/_search`);
        console.log(JSON.stringify(nextParams.body, null, 4));
      }

      return cluster.callWithRequest(req, 'search', nextParams);
    },
    index: params => {
      return cluster.callWithRequest(req, 'index', params);
    },
    delete: params => {
      return cluster.callWithRequest(req, 'delete', params);
    },
    indicesCreate: params => {
      return cluster.callWithRequest(req, 'indices.create', params);
    }
  };
}