"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _apm_telemetry = require("../lib/apm_telemetry");

var _create_agent_config_index = require("../lib/settings/agent_configuration/create_agent_config_index");

var _create_apm_api = require("../routes/create_apm_api");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class Plugin {
  setup(core) {
    (0, _create_apm_api.createApmApi)().init(core);
    (0, _create_agent_config_index.createApmAgentConfigurationIndex)(core);
    (0, _apm_telemetry.makeApmUsageCollector)(core);
  }

}

exports.Plugin = Plugin;