"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.COLORS = exports.MAX_KPIS = void 0;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const MAX_KPIS = 20;
exports.MAX_KPIS = MAX_KPIS;
const COLORS = [_eui_theme_light.default.euiColorVis0, _eui_theme_light.default.euiColorVis1, _eui_theme_light.default.euiColorVis2, _eui_theme_light.default.euiColorVis3, _eui_theme_light.default.euiColorVis4, _eui_theme_light.default.euiColorVis5, _eui_theme_light.default.euiColorVis6, _eui_theme_light.default.euiColorVis7, _eui_theme_light.default.euiColorVis8, _eui_theme_light.default.euiColorVis9];
exports.COLORS = COLORS;