"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTransactionDistribution = getTransactionDistribution;

var _get_buckets = require("./get_buckets");

var _get_distribution_max = require("./get_distribution_max");

var _round_to_nearest_five_or_ten = require("../../helpers/round_to_nearest_five_or_ten");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getBucketSize(max, {
  config
}) {
  const minBucketSize = config.get('xpack.apm.minimumBucketSize');
  const bucketTargetCount = config.get('xpack.apm.bucketTargetCount');
  const bucketSize = max / bucketTargetCount;
  return (0, _round_to_nearest_five_or_ten.roundToNearestFiveOrTen)(bucketSize > minBucketSize ? bucketSize : minBucketSize);
}

async function getTransactionDistribution({
  serviceName,
  transactionName,
  transactionType,
  transactionId,
  traceId,
  setup
}) {
  const distributionMax = await (0, _get_distribution_max.getDistributionMax)(serviceName, transactionName, transactionType, setup);

  if (distributionMax == null) {
    return {
      totalHits: 0,
      buckets: [],
      bucketSize: 0
    };
  }

  const bucketSize = getBucketSize(distributionMax, setup);
  const {
    buckets,
    totalHits
  } = await (0, _get_buckets.getBuckets)(serviceName, transactionName, transactionType, transactionId, traceId, distributionMax, bucketSize, setup);
  return {
    totalHits,
    buckets,
    bucketSize
  };
}