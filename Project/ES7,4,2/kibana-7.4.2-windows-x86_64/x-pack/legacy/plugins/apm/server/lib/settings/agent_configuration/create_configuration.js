"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createConfiguration = createConfiguration;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function createConfiguration({
  configuration,
  setup
}) {
  const {
    client,
    config
  } = setup;
  const params = {
    type: '_doc',
    refresh: true,
    index: config.get('apm_oss.apmAgentConfigurationIndex'),
    body: {
      '@timestamp': Date.now(),
      ...configuration
    }
  };
  return client.index(params);
}