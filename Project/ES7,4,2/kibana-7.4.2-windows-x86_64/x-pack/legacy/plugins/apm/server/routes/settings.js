"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.agentConfigurationSearchRoute = exports.updateAgentConfigurationRoute = exports.createAgentConfigurationRoute = exports.listAgentConfigurationEnvironmentsRoute = exports.listAgentConfigurationServicesRoute = exports.deleteAgentConfigurationRoute = exports.agentConfigurationRoute = void 0;

var t = _interopRequireWildcard(require("io-ts"));

var _setup_request = require("../lib/helpers/setup_request");

var _get_service_names = require("../lib/settings/agent_configuration/get_service_names");

var _create_configuration = require("../lib/settings/agent_configuration/create_configuration");

var _update_configuration = require("../lib/settings/agent_configuration/update_configuration");

var _search = require("../lib/settings/agent_configuration/search");

var _list_configurations = require("../lib/settings/agent_configuration/list_configurations");

var _get_environments = require("../lib/settings/agent_configuration/get_environments");

var _delete_configuration = require("../lib/settings/agent_configuration/delete_configuration");

var _create_route = require("./create_route");

var _transaction_sample_rate_rt = require("../../common/runtime_types/transaction_sample_rate_rt");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// get list of configurations
const agentConfigurationRoute = (0, _create_route.createRoute)(core => ({
  path: '/api/apm/settings/agent-configuration',
  handler: async req => {
    const setup = await (0, _setup_request.setupRequest)(req);
    return await (0, _list_configurations.listConfigurations)({
      setup
    });
  }
})); // delete configuration

exports.agentConfigurationRoute = agentConfigurationRoute;
const deleteAgentConfigurationRoute = (0, _create_route.createRoute)(() => ({
  method: 'DELETE',
  path: '/api/apm/settings/agent-configuration/{configurationId}',
  params: {
    path: t.type({
      configurationId: t.string
    })
  },
  handler: async (req, {
    path
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const {
      configurationId
    } = path;
    return await (0, _delete_configuration.deleteConfiguration)({
      configurationId,
      setup
    });
  }
})); // get list of services

exports.deleteAgentConfigurationRoute = deleteAgentConfigurationRoute;
const listAgentConfigurationServicesRoute = (0, _create_route.createRoute)(() => ({
  method: 'GET',
  path: '/api/apm/settings/agent-configuration/services',
  handler: async req => {
    const setup = await (0, _setup_request.setupRequest)(req);
    return await (0, _get_service_names.getServiceNames)({
      setup
    });
  }
}));
exports.listAgentConfigurationServicesRoute = listAgentConfigurationServicesRoute;
const agentPayloadRt = t.type({
  settings: t.type({
    transaction_sample_rate: _transaction_sample_rate_rt.transactionSampleRateRt
  }),
  service: t.intersection([t.type({
    name: t.string
  }), t.partial({
    environments: t.array(t.string)
  })])
}); // get environments for service

const listAgentConfigurationEnvironmentsRoute = (0, _create_route.createRoute)(() => ({
  path: '/api/apm/settings/agent-configuration/services/{serviceName}/environments',
  params: {
    path: t.type({
      serviceName: t.string
    })
  },
  handler: async (req, {
    path
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const {
      serviceName
    } = path;
    return await (0, _get_environments.getEnvironments)({
      serviceName,
      setup
    });
  }
}));
exports.listAgentConfigurationEnvironmentsRoute = listAgentConfigurationEnvironmentsRoute;
const createAgentConfigurationRoute = (0, _create_route.createRoute)(() => ({
  method: 'POST',
  path: '/api/apm/settings/agent-configuration/new',
  params: {
    body: agentPayloadRt
  },
  handler: async (req, {
    body
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    return await (0, _create_configuration.createConfiguration)({
      configuration: body,
      setup
    });
  }
}));
exports.createAgentConfigurationRoute = createAgentConfigurationRoute;
const updateAgentConfigurationRoute = (0, _create_route.createRoute)(() => ({
  method: 'PUT',
  path: `/api/apm/settings/agent-configuration/{configurationId}`,
  params: {
    path: t.type({
      configurationId: t.string
    }),
    body: agentPayloadRt
  },
  handler: async (req, {
    path,
    body
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const {
      configurationId
    } = path;
    return await (0, _update_configuration.updateConfiguration)({
      configurationId,
      configuration: body,
      setup
    });
  }
})); // Lookup single configuration

exports.updateAgentConfigurationRoute = updateAgentConfigurationRoute;
const agentConfigurationSearchRoute = (0, _create_route.createRoute)(core => ({
  method: 'POST',
  path: '/api/apm/settings/agent-configuration/search',
  params: {
    body: t.type({
      service: t.intersection([t.type({
        name: t.string
      }), t.partial({
        environment: t.string
      })])
    })
  },
  handler: async (req, {
    body
  }, h) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const config = await (0, _search.searchConfigurations)({
      serviceName: body.service.name,
      environment: body.service.environment,
      setup
    });

    if (!config) {
      return h.response().code(404);
    }

    return config;
  }
}));
exports.agentConfigurationSearchRoute = agentConfigurationSearchRoute;