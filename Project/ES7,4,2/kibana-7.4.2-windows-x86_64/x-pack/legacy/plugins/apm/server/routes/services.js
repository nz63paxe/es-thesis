"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.serviceTransactionTypesRoute = exports.serviceAgentNameRoute = exports.servicesRoute = void 0;

var t = _interopRequireWildcard(require("io-ts"));

var _apm_telemetry = require("../lib/apm_telemetry");

var _setup_request = require("../lib/helpers/setup_request");

var _get_service_agent_name = require("../lib/services/get_service_agent_name");

var _get_services = require("../lib/services/get_services");

var _get_service_transaction_types = require("../lib/services/get_service_transaction_types");

var _create_route = require("./create_route");

var _default_api_types = require("./default_api_types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const servicesRoute = (0, _create_route.createRoute)(core => ({
  path: '/api/apm/services',
  params: {
    query: t.intersection([_default_api_types.uiFiltersRt, _default_api_types.rangeRt])
  },
  handler: async req => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const services = await (0, _get_services.getServices)(setup);
    const {
      server
    } = core.http; // Store telemetry data derived from services

    const agentNames = services.items.map(({
      agentName
    }) => agentName);
    const apmTelemetry = (0, _apm_telemetry.createApmTelementry)(agentNames);
    (0, _apm_telemetry.storeApmTelemetry)(server, apmTelemetry);
    return services;
  }
}));
exports.servicesRoute = servicesRoute;
const serviceAgentNameRoute = (0, _create_route.createRoute)(() => ({
  path: '/api/apm/services/{serviceName}/agent_name',
  params: {
    path: t.type({
      serviceName: t.string
    }),
    query: _default_api_types.rangeRt
  },
  handler: async (req, {
    path
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const {
      serviceName
    } = path;
    return (0, _get_service_agent_name.getServiceAgentName)(serviceName, setup);
  }
}));
exports.serviceAgentNameRoute = serviceAgentNameRoute;
const serviceTransactionTypesRoute = (0, _create_route.createRoute)(() => ({
  path: '/api/apm/services/{serviceName}/transaction_types',
  params: {
    path: t.type({
      serviceName: t.string
    }),
    query: _default_api_types.rangeRt
  },
  handler: async (req, {
    path
  }) => {
    const setup = await (0, _setup_request.setupRequest)(req);
    const {
      serviceName
    } = path;
    return (0, _get_service_transaction_types.getServiceTransactionTypes)(serviceName, setup);
  }
}));
exports.serviceTransactionTypesRoute = serviceTransactionTypesRoute;