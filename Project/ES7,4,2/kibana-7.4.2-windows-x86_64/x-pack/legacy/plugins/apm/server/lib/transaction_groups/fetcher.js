"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transactionGroupsFetcher = transactionGroupsFetcher;

var _elasticsearch_fieldnames = require("../../../common/elasticsearch_fieldnames");

var _transaction_groups = require("../../../common/projections/transaction_groups");

var _merge_projection = require("../../../common/projections/util/merge_projection");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function transactionGroupsFetcher(options, setup) {
  const {
    client,
    config
  } = setup;
  const projection = (0, _transaction_groups.getTransactionGroupsProjection)({
    setup,
    options
  });
  const params = (0, _merge_projection.mergeProjection)(projection, {
    body: {
      size: 0,
      query: {
        bool: {
          // prefer sampled transactions
          should: [{
            term: {
              [_elasticsearch_fieldnames.TRANSACTION_SAMPLED]: true
            }
          }]
        }
      },
      aggs: {
        transactions: {
          terms: {
            order: {
              sum: 'desc'
            },
            size: config.get('xpack.apm.ui.transactionGroupBucketSize')
          },
          aggs: {
            sample: {
              top_hits: {
                size: 1,
                sort: [{
                  _score: 'desc'
                }, // sort by _score to ensure that buckets with sampled:true ends up on top
                {
                  '@timestamp': {
                    order: 'desc'
                  }
                }]
              }
            },
            avg: {
              avg: {
                field: _elasticsearch_fieldnames.TRANSACTION_DURATION
              }
            },
            p95: {
              percentiles: {
                field: _elasticsearch_fieldnames.TRANSACTION_DURATION,
                percents: [95]
              }
            },
            sum: {
              sum: {
                field: _elasticsearch_fieldnames.TRANSACTION_DURATION
              }
            }
          }
        }
      }
    }
  });
  return client.search(params);
}