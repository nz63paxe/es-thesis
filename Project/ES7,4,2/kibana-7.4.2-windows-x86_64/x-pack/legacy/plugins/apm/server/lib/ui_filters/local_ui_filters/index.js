"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getLocalUIFilters = getLocalUIFilters;

var _lodash = require("lodash");

var _merge_projection = require("../../../../common/projections/util/merge_projection");

var _get_filter_aggregations = require("./get_filter_aggregations");

var _config = require("./config");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getLocalUIFilters({
  server,
  setup,
  projection,
  uiFilters,
  localFilterNames
}) {
  const {
    client
  } = setup;
  const projectionWithoutAggs = (0, _lodash.cloneDeep)(projection);
  delete projectionWithoutAggs.body.aggs;
  const filterAggregations = await (0, _get_filter_aggregations.getFilterAggregations)({
    server,
    uiFilters,
    projection,
    localFilterNames
  });
  const params = (0, _merge_projection.mergeProjection)(projectionWithoutAggs, {
    body: {
      size: 0,
      // help TS infer aggregations by making all aggregations required
      aggs: filterAggregations
    }
  });
  const response = await client.search(params);
  const {
    aggregations
  } = response;

  if (!aggregations) {
    return [];
  }

  return localFilterNames.map(key => {
    const aggregationsForFilter = aggregations[key];
    const filter = _config.localUIFilters[key];
    return { ...filter,
      options: (0, _lodash.sortByOrder)(aggregationsForFilter.by_terms.buckets.map(bucket => {
        return {
          name: bucket.key,
          count: 'bucket_count' in bucket ? bucket.bucket_count.value : bucket.doc_count
        };
      }), 'count', 'desc')
    };
  });
}