"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTrace = getTrace;

var _get_trace_errors_per_transaction = require("../errors/get_trace_errors_per_transaction");

var _get_trace_items = require("./get_trace_items");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getTrace(traceId, setup) {
  const [trace, errorsPerTransaction] = await Promise.all([(0, _get_trace_items.getTraceItems)(traceId, setup), (0, _get_trace_errors_per_transaction.getTraceErrorsPerTransaction)(traceId, setup)]);
  return {
    trace,
    errorsPerTransaction
  };
}