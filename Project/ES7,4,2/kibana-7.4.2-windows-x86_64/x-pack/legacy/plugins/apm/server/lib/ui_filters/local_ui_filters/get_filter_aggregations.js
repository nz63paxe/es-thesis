"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFilterAggregations = void 0;

var _lodash = require("lodash");

var _get_ui_filters_es = require("../../helpers/convert_ui_filters/get_ui_filters_es");

var _config = require("./config");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getFilterAggregations = async ({
  server,
  uiFilters,
  projection,
  localFilterNames
}) => {
  const mappedFilters = localFilterNames.map(name => _config.localUIFilters[name]);
  const aggs = await Promise.all(mappedFilters.map(async field => {
    const filter = await (0, _get_ui_filters_es.getUiFiltersES)(server, (0, _lodash.omit)(uiFilters, field.name));
    const bucketCountAggregation = projection.body.aggs ? {
      aggs: {
        bucket_count: {
          cardinality: {
            field: projection.body.aggs[Object.keys(projection.body.aggs)[0]].terms.field
          }
        }
      }
    } : {};
    return {
      [field.name]: {
        filter: {
          bool: {
            filter
          }
        },
        aggs: {
          by_terms: {
            terms: {
              field: field.fieldName,
              order: {
                _count: 'desc'
              }
            },
            ...bucketCountAggregation
          }
        }
      }
    };
  }));
  const mergedAggregations = Object.assign({}, ...aggs);
  return mergedAggregations;
};

exports.getFilterAggregations = getFilterAggregations;