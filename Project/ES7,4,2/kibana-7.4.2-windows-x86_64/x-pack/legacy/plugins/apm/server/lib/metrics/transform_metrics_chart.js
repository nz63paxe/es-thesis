"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transformDataToMetricsChart = transformDataToMetricsChart;

var _eui_theme_light = _interopRequireDefault(require("@elastic/eui/dist/eui_theme_light.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const colors = [_eui_theme_light.default.euiColorVis0, _eui_theme_light.default.euiColorVis1, _eui_theme_light.default.euiColorVis2, _eui_theme_light.default.euiColorVis3, _eui_theme_light.default.euiColorVis4, _eui_theme_light.default.euiColorVis5, _eui_theme_light.default.euiColorVis6];

function transformDataToMetricsChart(result, chartBase) {
  const {
    aggregations,
    hits
  } = result;
  const timeseriesData = aggregations != null ? aggregations.timeseriesData : undefined;
  return {
    title: chartBase.title,
    key: chartBase.key,
    yUnit: chartBase.yUnit,
    totalHits: hits.total,
    series: Object.keys(chartBase.series).map((seriesKey, i) => {
      const overallValue = aggregations != null && aggregations[seriesKey] != null ? aggregations[seriesKey].value : undefined;
      return {
        title: chartBase.series[seriesKey].title,
        key: seriesKey,
        type: chartBase.type,
        color: chartBase.series[seriesKey].color || colors[i],
        overallValue,
        data: ((timeseriesData != null ? timeseriesData.buckets : undefined) || []).map(bucket => {
          const {
            value
          } = bucket[seriesKey];
          const y = value === null || isNaN(value) ? null : value;
          return {
            x: bucket.key,
            y
          };
        })
      };
    })
  };
}