"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getErrorGroups = getErrorGroups;

var _elasticsearch_fieldnames = require("../../../common/elasticsearch_fieldnames");

var _errors = require("../../../common/projections/errors");

var _merge_projection = require("../../../common/projections/util/merge_projection");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getErrorGroups({
  serviceName,
  sortField,
  sortDirection = 'desc',
  setup
}) {
  const {
    client
  } = setup; // sort buckets by last occurrence of error

  const sortByLatestOccurrence = sortField === 'latestOccurrenceAt';
  const projection = (0, _errors.getErrorGroupsProjection)({
    setup,
    serviceName
  });
  const params = (0, _merge_projection.mergeProjection)(projection, {
    body: {
      size: 0,
      aggs: {
        error_groups: {
          terms: {
            size: 500,
            order: sortByLatestOccurrence ? {
              max_timestamp: sortDirection
            } : {
              _count: sortDirection
            }
          },
          aggs: {
            sample: {
              top_hits: {
                _source: [_elasticsearch_fieldnames.ERROR_LOG_MESSAGE, _elasticsearch_fieldnames.ERROR_EXC_MESSAGE, _elasticsearch_fieldnames.ERROR_EXC_HANDLED, _elasticsearch_fieldnames.ERROR_CULPRIT, _elasticsearch_fieldnames.ERROR_GROUP_ID, '@timestamp'],
                sort: [{
                  '@timestamp': 'desc'
                }],
                size: 1
              }
            },
            ...(sortByLatestOccurrence ? {
              max_timestamp: {
                max: {
                  field: '@timestamp'
                }
              }
            } : {})
          }
        }
      }
    }
  });
  const resp = await client.search(params); // aggregations can be undefined when no matching indices are found.
  // this is an exception rather than the rule so the ES type does not account for this.

  const hits = ((resp != null && resp.aggregations != null && resp.aggregations.error_groups != null ? resp.aggregations.error_groups.buckets : undefined) || []).map(bucket => {
    const source = bucket.sample.hits.hits[0]._source;
    const message = (source != null && source.error != null && source.error.log != null ? source.error.log.message : undefined) || (source != null && source.error != null && source.error.exception != null && source.error.exception[0] != null ? source.error.exception[0].message : undefined);
    return {
      message,
      occurrenceCount: bucket.doc_count,
      culprit: source.error.culprit,
      groupId: source.error.grouping_key,
      latestOccurrenceAt: source['@timestamp'],
      handled: source != null && source.error != null && source.error.exception != null && source.error.exception[0] != null ? source.error.exception[0].handled : undefined
    };
  });
  return hits;
}