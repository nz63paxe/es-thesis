"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getKueryUiFilterES = getKueryUiFilterES;
exports.getFromSavedObject = getFromSavedObject;

var _esQuery = require("@kbn/es-query");

var _index_pattern = require("../../../lib/index_pattern");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getKueryUiFilterES(server, kuery) {
  if (!kuery) {
    return;
  }

  const apmIndexPattern = await (0, _index_pattern.getAPMIndexPattern)(server);
  const formattedIndexPattern = getFromSavedObject(apmIndexPattern);

  if (!formattedIndexPattern) {
    return;
  }

  return convertKueryToEsQuery(kuery, formattedIndexPattern);
} // lifted from src/legacy/ui/public/index_patterns/static_utils/index.js


function getFromSavedObject(apmIndexPattern) {
  if ((apmIndexPattern != null && apmIndexPattern.attributes != null ? apmIndexPattern.attributes.fields : undefined) === undefined) {
    return;
  }

  return {
    id: apmIndexPattern.id,
    fields: JSON.parse(apmIndexPattern.attributes.fields),
    title: apmIndexPattern.attributes.title
  };
}

function convertKueryToEsQuery(kuery, indexPattern) {
  const ast = (0, _esQuery.fromKueryExpression)(kuery);
  return (0, _esQuery.toElasticsearchQuery)(ast, indexPattern);
}