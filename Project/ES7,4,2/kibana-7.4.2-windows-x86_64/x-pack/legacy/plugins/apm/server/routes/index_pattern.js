"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.indexPatternRoute = void 0;

var _index_pattern = require("../lib/index_pattern");

var _create_route = require("./create_route");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const indexPatternRoute = (0, _create_route.createRoute)(core => ({
  path: '/api/apm/index_pattern',
  handler: async () => {
    const {
      server
    } = core.http;
    return await (0, _index_pattern.getAPMIndexPattern)(server);
  }
}));
exports.indexPatternRoute = indexPatternRoute;