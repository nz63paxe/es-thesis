"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.searchConfigurations = searchConfigurations;

var _elasticsearch_fieldnames = require("../../../../common/elasticsearch_fieldnames");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function searchConfigurations({
  serviceName,
  environment,
  setup
}) {
  const {
    client,
    config
  } = setup;
  const filters = [{
    term: {
      [_elasticsearch_fieldnames.SERVICE_NAME]: serviceName
    }
  }];

  if (environment) {
    filters.push({
      term: {
        [_elasticsearch_fieldnames.SERVICE_ENVIRONMENT]: environment
      }
    });
  } else {
    filters.push({
      bool: {
        must_not: {
          exists: {
            field: _elasticsearch_fieldnames.SERVICE_ENVIRONMENT
          }
        }
      }
    });
  }

  const params = {
    index: config.get('apm_oss.apmAgentConfigurationIndex'),
    body: {
      size: 1,
      query: {
        bool: {
          filter: filters
        }
      }
    }
  };
  const resp = await client.search(params);
  return resp.hits.hits[0];
}