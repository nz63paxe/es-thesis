"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getErrorDistribution = getErrorDistribution;

var _get_buckets = require("./get_buckets");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getBucketSize({
  start,
  end,
  config
}) {
  const bucketTargetCount = config.get('xpack.apm.bucketTargetCount');
  return Math.floor((end - start) / bucketTargetCount);
}

async function getErrorDistribution({
  serviceName,
  groupId,
  setup
}) {
  const bucketSize = getBucketSize(setup);
  const {
    buckets,
    totalHits
  } = await (0, _get_buckets.getBuckets)({
    serviceName,
    groupId,
    bucketSize,
    setup
  });
  return {
    totalHits,
    buckets,
    bucketSize
  };
}