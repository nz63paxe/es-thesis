"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.bucketTransformer = bucketTransformer;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getBucket(bucket) {
  const sampleSource = bucket != null && bucket.sample != null && bucket.sample.hits != null && bucket.sample.hits.hits != null && bucket.sample.hits.hits[0] != null ? bucket.sample.hits.hits[0]._source : undefined;
  const isSampled = sampleSource != null && sampleSource.transaction != null ? sampleSource.transaction.sampled : undefined;
  const sample = {
    traceId: sampleSource != null && sampleSource.trace != null ? sampleSource.trace.id : undefined,
    transactionId: sampleSource != null && sampleSource.transaction != null ? sampleSource.transaction.id : undefined
  };
  return {
    key: bucket.key,
    count: bucket.doc_count,
    sample: isSampled ? sample : undefined
  };
}

function bucketTransformer(response) {
  const buckets = ((response.aggregations != null && response.aggregations.distribution != null ? response.aggregations.distribution.buckets : undefined) || []).map(getBucket);
  return {
    totalHits: response.hits.total,
    buckets
  };
}