"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAllEnvironments = getAllEnvironments;

var _elasticsearch_fieldnames = require("../../../../../common/elasticsearch_fieldnames");

var _environment_filter_values = require("../../../../../common/environment_filter_values");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getAllEnvironments({
  serviceName,
  setup
}) {
  const {
    client,
    config
  } = setup;
  const params = {
    index: [config.get('apm_oss.metricsIndices'), config.get('apm_oss.errorIndices'), config.get('apm_oss.transactionIndices')],
    body: {
      size: 0,
      query: {
        bool: {
          filter: [{
            terms: {
              [_elasticsearch_fieldnames.PROCESSOR_EVENT]: ['transaction', 'error', 'metric']
            }
          }, {
            term: {
              [_elasticsearch_fieldnames.SERVICE_NAME]: serviceName
            }
          }]
        }
      },
      aggs: {
        environments: {
          terms: {
            field: _elasticsearch_fieldnames.SERVICE_ENVIRONMENT,
            missing: _environment_filter_values.ENVIRONMENT_NOT_DEFINED,
            size: 100
          }
        }
      }
    }
  };
  const resp = await client.search(params);
  const buckets = (resp.aggregations != null && resp.aggregations.environments != null ? resp.aggregations.environments.buckets : undefined) || [];
  return buckets.map(bucket => bucket.key);
}