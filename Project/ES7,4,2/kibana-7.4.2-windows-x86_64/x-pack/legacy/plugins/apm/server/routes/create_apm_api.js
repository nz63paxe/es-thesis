"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createApmApi = void 0;

var _index_pattern = require("./index_pattern");

var _errors = require("./errors");

var _services = require("./services");

var _settings = require("./settings");

var _metrics = require("./metrics");

var _traces = require("./traces");

var _transaction_groups = require("./transaction_groups");

var _ui_filters = require("./ui_filters");

var _create_api = require("./create_api");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createApmApi = () => {
  const api = (0, _create_api.createApi)().add(_index_pattern.indexPatternRoute).add(_errors.errorDistributionRoute).add(_errors.errorGroupsRoute).add(_errors.errorsRoute).add(_metrics.metricsChartsRoute).add(_services.serviceAgentNameRoute).add(_services.serviceTransactionTypesRoute).add(_services.servicesRoute).add(_settings.agentConfigurationRoute).add(_settings.agentConfigurationSearchRoute).add(_settings.createAgentConfigurationRoute).add(_settings.deleteAgentConfigurationRoute).add(_settings.listAgentConfigurationEnvironmentsRoute).add(_settings.listAgentConfigurationServicesRoute).add(_settings.updateAgentConfigurationRoute).add(_traces.tracesRoute).add(_traces.tracesByIdRoute).add(_transaction_groups.transactionGroupsBreakdownRoute).add(_transaction_groups.transactionGroupsChartsRoute).add(_transaction_groups.transactionGroupsDistributionRoute).add(_transaction_groups.transactionGroupsRoute).add(_transaction_groups.transactionGroupsAvgDurationByCountry).add(_ui_filters.errorGroupsLocalFiltersRoute).add(_ui_filters.metricsLocalFiltersRoute).add(_ui_filters.servicesLocalFiltersRoute).add(_ui_filters.tracesLocalFiltersRoute).add(_ui_filters.transactionGroupsLocalFiltersRoute).add(_ui_filters.transactionsLocalFiltersRoute).add(_ui_filters.uiFiltersEnvironmentsRoute);
  return api;
};

exports.createApmApi = createApmApi;