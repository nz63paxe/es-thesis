"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createApi = createApi;

var _lodash = require("lodash");

var _boom = _interopRequireDefault(require("boom"));

var t = _interopRequireWildcard(require("io-ts"));

var _PathReporter = require("io-ts/lib/PathReporter");

var _json_rt = require("../../../common/runtime_types/json_rt");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createApi() {
  const factoryFns = [];
  const api = {
    _S: {},

    add(fn) {
      factoryFns.push(fn);
      return this;
    },

    init(core) {
      const {
        server
      } = core.http;
      factoryFns.forEach(fn => {
        const {
          params = {},
          ...route
        } = fn(core);
        const rts = {
          // add _debug query parameter to all routes
          query: params.query ? t.exact(t.intersection([params.query, t.partial({
            _debug: _json_rt.jsonRt.pipe(t.boolean)
          })])) : t.union([t.strict({}), t.strict({
            _debug: _json_rt.jsonRt.pipe(t.boolean)
          })]),
          path: params.path || t.strict({}),
          body: params.body || t.null
        };
        server.route((0, _lodash.merge)({
          options: {
            tags: ['access:apm']
          },
          method: 'GET'
        }, route, {
          handler: (request, h) => {
            const paramMap = {
              path: request.params,
              body: request.payload,
              query: request.query
            };
            const parsedParams = Object.keys(rts).reduce((acc, key) => {
              let codec = rts[key];
              const value = paramMap[key]; // Use exact props where possible (only possible for types with props)

              if ('props' in codec) {
                codec = t.exact(codec);
              }

              const result = codec.decode(value);

              if (result.isLeft()) {
                throw _boom.default.badRequest(_PathReporter.PathReporter.report(result)[0]);
              } // hide _debug from route handlers


              const parsedValue = key === 'query' ? (0, _lodash.omit)(result.value, '_debug') : result.value;
              return { ...acc,
                [key]: parsedValue
              };
            }, {});
            return route.handler(request, // only return values for parameters that have runtime types
            (0, _lodash.pick)(parsedParams, Object.keys(params)), h);
          }
        }));
      });
    }

  };
  return api;
}