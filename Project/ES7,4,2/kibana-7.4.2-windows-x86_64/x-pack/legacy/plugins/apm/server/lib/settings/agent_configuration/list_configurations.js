"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listConfigurations = listConfigurations;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function listConfigurations({
  setup
}) {
  const {
    client,
    config
  } = setup;
  const params = {
    index: config.get('apm_oss.apmAgentConfigurationIndex')
  };
  const resp = await client.search(params);
  return resp.hits.hits.map(item => ({
    id: item._id,
    ...item._source
  }));
}