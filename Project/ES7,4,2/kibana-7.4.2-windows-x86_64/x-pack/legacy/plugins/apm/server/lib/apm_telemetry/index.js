"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "storeApmTelemetry", {
  enumerable: true,
  get: function () {
    return _apm_telemetry.storeApmTelemetry;
  }
});
Object.defineProperty(exports, "createApmTelementry", {
  enumerable: true,
  get: function () {
    return _apm_telemetry.createApmTelementry;
  }
});
Object.defineProperty(exports, "APM_TELEMETRY_DOC_ID", {
  enumerable: true,
  get: function () {
    return _apm_telemetry.APM_TELEMETRY_DOC_ID;
  }
});
Object.defineProperty(exports, "makeApmUsageCollector", {
  enumerable: true,
  get: function () {
    return _make_apm_usage_collector.makeApmUsageCollector;
  }
});

var _apm_telemetry = require("./apm_telemetry");

var _make_apm_usage_collector = require("./make_apm_usage_collector");