"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.makeApmUsageCollector = makeApmUsageCollector;

var _saved_objects_client = require("../helpers/saved_objects_client");

var _apm_telemetry = require("./apm_telemetry");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function makeApmUsageCollector(core) {
  const {
    server
  } = core.http;
  const apmUsageCollector = server.usage.collectorSet.makeUsageCollector({
    type: 'apm',
    fetch: async () => {
      const savedObjectsClient = (0, _saved_objects_client.getSavedObjectsClient)(server);

      try {
        const apmTelemetrySavedObject = await savedObjectsClient.get('apm-telemetry', _apm_telemetry.APM_TELEMETRY_DOC_ID);
        return apmTelemetrySavedObject.attributes;
      } catch (err) {
        return (0, _apm_telemetry.createApmTelementry)();
      }
    },
    isReady: () => true
  });
  server.usage.collectorSet.register(apmUsageCollector);
}