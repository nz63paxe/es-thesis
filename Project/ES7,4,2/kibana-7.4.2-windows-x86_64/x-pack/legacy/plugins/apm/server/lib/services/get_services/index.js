"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getServices = getServices;

var _lodash = require("lodash");

var _get_agent_status = require("./get_agent_status");

var _get_legacy_data_status = require("./get_legacy_data_status");

var _get_services_items = require("./get_services_items");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getServices(setup) {
  const items = await (0, _get_services_items.getServicesItems)(setup);
  const hasLegacyData = await (0, _get_legacy_data_status.getLegacyDataStatus)(setup); // conditionally check for historical data if no services were found in the current time range

  const noDataInCurrentTimeRange = (0, _lodash.isEmpty)(items);
  let hasHistorialAgentData = true;

  if (noDataInCurrentTimeRange) {
    hasHistorialAgentData = await (0, _get_agent_status.getAgentStatus)(setup);
  }

  return {
    items,
    hasHistoricalData: hasHistorialAgentData,
    hasLegacyData
  };
}