"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTraceItems = getTraceItems;

var _elasticsearch_fieldnames = require("../../../common/elasticsearch_fieldnames");

var _range_filter = require("../helpers/range_filter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getTraceItems(traceId, setup) {
  const {
    start,
    end,
    client,
    config
  } = setup;
  const maxTraceItems = config.get('xpack.apm.ui.maxTraceItems');
  const params = {
    index: [config.get('apm_oss.spanIndices'), config.get('apm_oss.transactionIndices')],
    body: {
      size: maxTraceItems,
      query: {
        bool: {
          filter: [{
            term: {
              [_elasticsearch_fieldnames.TRACE_ID]: traceId
            }
          }, {
            terms: {
              [_elasticsearch_fieldnames.PROCESSOR_EVENT]: ['span', 'transaction']
            }
          }, {
            range: (0, _range_filter.rangeFilter)(start, end)
          }],
          should: {
            exists: {
              field: _elasticsearch_fieldnames.PARENT_ID
            }
          }
        }
      },
      sort: [{
        _score: {
          order: 'asc'
        }
      }, {
        [_elasticsearch_fieldnames.TRANSACTION_DURATION]: {
          order: 'desc'
        }
      }, {
        [_elasticsearch_fieldnames.SPAN_DURATION]: {
          order: 'desc'
        }
      }]
    }
  };
  const resp = await client.search(params);
  return {
    items: resp.hits.hits.map(hit => hit._source),
    exceedsMax: resp.hits.total > maxTraceItems
  };
}