"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createApmTelementry = createApmTelementry;
exports.storeApmTelemetry = storeApmTelemetry;
exports.APM_TELEMETRY_DOC_ID = void 0;

var _lodash = require("lodash");

var _agent_name = require("../../../common/agent_name");

var _saved_objects_client = require("../helpers/saved_objects_client");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const APM_TELEMETRY_DOC_ID = 'apm-telemetry';
exports.APM_TELEMETRY_DOC_ID = APM_TELEMETRY_DOC_ID;

function createApmTelementry(agentNames = []) {
  const validAgentNames = agentNames.filter(_agent_name.isAgentName);
  return {
    has_any_services: validAgentNames.length > 0,
    services_per_agent: (0, _lodash.countBy)(validAgentNames)
  };
}

async function storeApmTelemetry(server, apmTelemetry) {
  try {
    const savedObjectsClient = (0, _saved_objects_client.getSavedObjectsClient)(server);
    await savedObjectsClient.create('apm-telemetry', apmTelemetry, {
      id: APM_TELEMETRY_DOC_ID,
      overwrite: true
    });
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('Could not send APM telemetry:', e.message);
  }
}