"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupRequest = setupRequest;

var _moment = _interopRequireDefault(require("moment"));

var _es_client = require("./es_client");

var _get_ui_filters_es = require("./convert_ui_filters/get_ui_filters_es");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function decodeUiFilters(server, uiFiltersEncoded) {
  if (!uiFiltersEncoded) {
    return [];
  }

  const uiFilters = JSON.parse(uiFiltersEncoded);
  return (0, _get_ui_filters_es.getUiFiltersES)(server, uiFilters);
}

async function setupRequest(req) {
  const query = req.query;
  const {
    server
  } = req;
  const config = server.config();
  return {
    start: _moment.default.utc(query.start).valueOf(),
    end: _moment.default.utc(query.end).valueOf(),
    uiFiltersES: await decodeUiFilters(server, query.uiFilters),
    client: (0, _es_client.getESClient)(req),
    config
  };
}