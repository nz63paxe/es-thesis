"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getAPMIndexPattern = getAPMIndexPattern;

var _saved_objects_client = require("../helpers/saved_objects_client");

var _index_pattern = _interopRequireDefault(require("../../../../../../../src/legacy/core_plugins/kibana/server/tutorials/apm/index_pattern.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getAPMIndexPattern(server) {
  const config = server.config();
  const apmIndexPatternTitle = config.get('apm_oss.indexPattern');
  const savedObjectsClient = (0, _saved_objects_client.getSavedObjectsClient)(server);

  try {
    return await savedObjectsClient.get('index-pattern', _index_pattern.default.id);
  } catch (error) {
    // if GET fails, then create a new index pattern saved object
    return await savedObjectsClient.create('index-pattern', { ..._index_pattern.default.attributes,
      title: apmIndexPatternTitle
    }, {
      id: _index_pattern.default.id,
      overwrite: false
    });
  }
}