"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encryptedSavedObjects = void 0;

var _audit_logger = require("../../server/lib/audit_logger");

var _plugin = require("./server/plugin");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
const encryptedSavedObjects = kibana => new kibana.Plugin({
  id: _plugin.PLUGIN_ID,
  configPrefix: _plugin.CONFIG_PREFIX,
  require: ['kibana', 'elasticsearch', 'xpack_main'],

  config(Joi) {
    return Joi.object({
      enabled: Joi.boolean().default(true),
      encryptionKey: Joi.when(Joi.ref('$dist'), {
        is: true,
        then: Joi.string().min(32),
        otherwise: Joi.string().min(32).default('a'.repeat(32))
      })
    }).default();
  },

  async init(server) {
    const loggerFacade = {
      fatal: errorOrMessage => server.log(['fatal', _plugin.PLUGIN_ID], errorOrMessage),
      trace: message => server.log(['debug', _plugin.PLUGIN_ID], message),
      error: message => server.log(['error', _plugin.PLUGIN_ID], message),
      warn: message => server.log(['warning', _plugin.PLUGIN_ID], message),
      debug: message => server.log(['debug', _plugin.PLUGIN_ID], message),
      info: message => server.log(['info', _plugin.PLUGIN_ID], message)
    };
    const config = server.config();
    const encryptedSavedObjectsSetup = new _plugin.Plugin(loggerFacade).setup({
      config: {
        encryptionKey: config.get(`${_plugin.CONFIG_PREFIX}.encryptionKey`)
      },
      savedObjects: server.savedObjects,
      elasticsearch: server.plugins.elasticsearch
    }, {
      audit: new _audit_logger.AuditLogger(server, _plugin.PLUGIN_ID, config, server.plugins.xpack_main.info)
    }); // Re-expose plugin setup contract through legacy mechanism.

    for (const [setupMethodName, setupMethod] of Object.entries(encryptedSavedObjectsSetup)) {
      server.expose(setupMethodName, setupMethod);
    }
  }

});

exports.encryptedSavedObjects = encryptedSavedObjects;