"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = exports.CONFIG_PREFIX = exports.PLUGIN_ID = void 0;

var _crypto = _interopRequireDefault(require("crypto"));

var _lib = require("./lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const PLUGIN_ID = 'encrypted_saved_objects';
exports.PLUGIN_ID = PLUGIN_ID;
const CONFIG_PREFIX = `xpack.${PLUGIN_ID}`;
exports.CONFIG_PREFIX = CONFIG_PREFIX;

class Plugin {
  constructor(log) {
    this.log = log;
  }

  setup(core, plugins) {
    let encryptionKey = core.config.encryptionKey;

    if (encryptionKey == null) {
      this.log.warn(`Generating a random key for ${CONFIG_PREFIX}.encryptionKey. To be able ` + 'to decrypt encrypted saved objects attributes after restart, please set ' + `${CONFIG_PREFIX}.encryptionKey in kibana.yml`);
      encryptionKey = _crypto.default.randomBytes(16).toString('hex');
    }

    const service = Object.freeze(new _lib.EncryptedSavedObjectsService(encryptionKey, core.savedObjects.types, this.log, new _lib.EncryptedSavedObjectsAuditLogger(plugins.audit))); // Register custom saved object client that will encrypt, decrypt and strip saved object
    // attributes where appropriate for any saved object repository request. We choose max possible
    // priority for this wrapper to allow all other wrappers to set proper `namespace` for the Saved
    // Object (e.g. wrapper registered by the Spaces plugin) before we encrypt attributes since
    // `namespace` is included into AAD.

    core.savedObjects.addScopedSavedObjectsClientWrapperFactory(Number.MAX_SAFE_INTEGER, 'encrypted_saved_objects', ({
      client: baseClient
    }) => new _lib.EncryptedSavedObjectsClientWrapper({
      baseClient,
      service
    }));
    const internalRepository = core.savedObjects.getSavedObjectsRepository(core.elasticsearch.getCluster('admin').callWithInternalUser);
    return {
      isEncryptionError: error => error instanceof _lib.EncryptionError,
      registerType: typeRegistration => service.registerType(typeRegistration),
      getDecryptedAsInternalUser: async (type, id, options) => {
        const savedObject = await internalRepository.get(type, id, options);
        return { ...savedObject,
          attributes: await service.decryptAttributes({
            type,
            id,
            namespace: options && options.namespace
          }, savedObject.attributes)
        };
      }
    };
  }

}

exports.Plugin = Plugin;