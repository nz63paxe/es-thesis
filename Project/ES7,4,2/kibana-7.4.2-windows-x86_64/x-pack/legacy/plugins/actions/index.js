"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actions = actions;
Object.defineProperty(exports, "ActionsPlugin", {
  enumerable: true,
  get: function () {
    return _server.ActionsPlugin;
  }
});
Object.defineProperty(exports, "ActionsClient", {
  enumerable: true,
  get: function () {
    return _server.ActionsClient;
  }
});
Object.defineProperty(exports, "ActionType", {
  enumerable: true,
  get: function () {
    return _server.ActionType;
  }
});
Object.defineProperty(exports, "ActionTypeExecutorOptions", {
  enumerable: true,
  get: function () {
    return _server.ActionTypeExecutorOptions;
  }
});

var _mappings = _interopRequireDefault(require("./mappings.json"));

var _server = require("./server");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function actions(kibana) {
  return new kibana.Plugin({
    id: 'actions',
    configPrefix: 'xpack.actions',
    require: ['kibana', 'elasticsearch', 'task_manager', 'encrypted_saved_objects'],

    isEnabled(config) {
      return config.get('xpack.encrypted_saved_objects.enabled') === true && config.get('xpack.actions.enabled') === true && config.get('xpack.task_manager.enabled') === true;
    },

    config(Joi) {
      return Joi.object().keys({
        enabled: Joi.boolean().default(false)
      }).default();
    },

    init: _server.init,
    uiExports: {
      mappings: _mappings.default
    }
  });
}