"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.init = init;

var _actions_client = require("./actions_client");

var _action_type_registry = require("./action_type_registry");

var _create_execute_function = require("./create_execute_function");

var _routes = require("./routes");

var _builtin_action_types = require("./builtin_action_types");

var _optional_plugin = require("../../../server/lib/optional_plugin");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function init(server) {
  const config = server.config();
  const taskManager = server.plugins.task_manager;
  const {
    callWithRequest
  } = server.plugins.elasticsearch.getCluster('admin');
  const spaces = (0, _optional_plugin.createOptionalPlugin)(config, 'xpack.spaces', server.plugins, 'spaces');
  server.plugins.xpack_main.registerFeature({
    id: 'actions',
    name: 'Actions',
    app: ['actions', 'kibana'],
    privileges: {
      all: {
        savedObject: {
          all: ['action', 'action_task_params'],
          read: []
        },
        ui: [],
        api: ['actions-read', 'actions-all']
      },
      read: {
        savedObject: {
          all: ['action_task_params'],
          read: ['action']
        },
        ui: [],
        api: ['actions-read']
      }
    }
  }); // Encrypted attributes
  // - `secrets` properties will be encrypted
  // - `config` will be included in AAD
  // - everything else excluded from AAD

  server.plugins.encrypted_saved_objects.registerType({
    type: 'action',
    attributesToEncrypt: new Set(['secrets']),
    attributesToExcludeFromAAD: new Set(['description'])
  });
  server.plugins.encrypted_saved_objects.registerType({
    type: 'action_task_params',
    attributesToEncrypt: new Set(['apiKey'])
  });

  function getServices(request) {
    return {
      log: (...args) => server.log(...args),
      callCluster: (...args) => callWithRequest(request, ...args),
      savedObjectsClient: server.savedObjects.getScopedSavedObjectsClient(request)
    };
  }

  function getBasePath(spaceId) {
    return spaces.isEnabled && spaceId ? spaces.getBasePath(spaceId) : server.config().get('server.basePath') || '';
  }

  function spaceIdToNamespace(spaceId) {
    return spaces.isEnabled && spaceId ? spaces.spaceIdToNamespace(spaceId) : undefined;
  }

  const actionTypeRegistry = new _action_type_registry.ActionTypeRegistry({
    getServices,
    taskManager,
    encryptedSavedObjectsPlugin: server.plugins.encrypted_saved_objects,
    getBasePath,
    spaceIdToNamespace,
    isSecurityEnabled: config.get('xpack.security.enabled')
  });
  (0, _builtin_action_types.registerBuiltInActionTypes)(actionTypeRegistry); // Routes

  (0, _routes.createRoute)(server);
  (0, _routes.deleteRoute)(server);
  (0, _routes.getRoute)(server);
  (0, _routes.findRoute)(server);
  (0, _routes.updateRoute)(server);
  (0, _routes.listActionTypesRoute)(server);
  (0, _routes.executeRoute)({
    server,
    actionTypeRegistry,
    getServices
  });
  const executeFn = (0, _create_execute_function.createExecuteFunction)({
    taskManager,
    getScopedSavedObjectsClient: server.savedObjects.getScopedSavedObjectsClient,
    getBasePath,
    isSecurityEnabled: config.get('xpack.security.enabled')
  }); // Expose functions to server

  server.decorate('request', 'getActionsClient', function () {
    const request = this;
    const savedObjectsClient = request.getSavedObjectsClient();
    const actionsClient = new _actions_client.ActionsClient({
      savedObjectsClient,
      actionTypeRegistry
    });
    return actionsClient;
  });
  const exposedFunctions = {
    execute: executeFn,
    registerType: actionTypeRegistry.register.bind(actionTypeRegistry),
    listTypes: actionTypeRegistry.list.bind(actionTypeRegistry)
  };
  server.expose(exposedFunctions);
}