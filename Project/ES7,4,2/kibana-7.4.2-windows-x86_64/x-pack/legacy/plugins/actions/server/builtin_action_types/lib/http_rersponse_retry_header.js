"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRetryAfterIntervalFromHeaders = getRetryAfterIntervalFromHeaders;

var _Option = require("fp-ts/lib/Option");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getRetryAfterIntervalFromHeaders(headers) {
  return (0, _Option.fromNullable)(headers['retry-after']).map(retryAfter => parseInt(retryAfter, 10)).filter(retryAfter => !isNaN(retryAfter));
}