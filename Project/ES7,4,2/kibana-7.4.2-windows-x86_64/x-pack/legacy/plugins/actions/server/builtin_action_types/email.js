"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actionType = void 0;

var _i18n = require("@kbn/i18n");

var _configSchema = require("@kbn/config-schema");

var _services = _interopRequireDefault(require("nodemailer/lib/well-known/services.json"));

var _send_email = require("./lib/send_email");

var _nullable = require("./lib/nullable");

var _schemas = require("./lib/schemas");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ConfigSchema = _configSchema.schema.object({
  service: (0, _nullable.nullableType)(_configSchema.schema.string()),
  host: (0, _nullable.nullableType)(_configSchema.schema.string()),
  port: (0, _nullable.nullableType)((0, _schemas.portSchema)()),
  secure: (0, _nullable.nullableType)(_configSchema.schema.boolean()),
  from: _configSchema.schema.string()
}, {
  validate: validateConfig
});

function validateConfig(configObject) {
  // avoids circular reference ...
  const config = configObject; // Make sure service is set, or if not, both host/port must be set.
  // If service is set, host/port are ignored, when the email is sent.
  // Note, not currently making these message translated, as will be
  // emitted alongside messages from @kbn/config-schema, which does not
  // translate messages.

  if (config.service == null) {
    if (config.host == null && config.port == null) {
      return 'either [service] or [host]/[port] is required';
    }

    if (config.host == null) {
      return '[host] is required if [service] is not provided';
    }

    if (config.port == null) {
      return '[port] is required if [service] is not provided';
    }
  } else {
    // service is not null
    if (!isValidService(config.service)) {
      return `[service] value "${config.service}" is not valid`;
    }
  }
} // secrets definition


const SecretsSchema = _configSchema.schema.object({
  user: _configSchema.schema.string(),
  password: _configSchema.schema.string()
}); // params definition


const ParamsSchema = _configSchema.schema.object({
  to: _configSchema.schema.arrayOf(_configSchema.schema.string(), {
    defaultValue: []
  }),
  cc: _configSchema.schema.arrayOf(_configSchema.schema.string(), {
    defaultValue: []
  }),
  bcc: _configSchema.schema.arrayOf(_configSchema.schema.string(), {
    defaultValue: []
  }),
  subject: _configSchema.schema.string(),
  message: _configSchema.schema.string()
}, {
  validate: validateParams
});

function validateParams(paramsObject) {
  // avoids circular reference ...
  const params = paramsObject;
  const {
    to,
    cc,
    bcc
  } = params;
  const addrs = to.length + cc.length + bcc.length;

  if (addrs === 0) {
    return 'no [to], [cc], or [bcc] entries';
  }
} // action type definition


const actionType = {
  id: '.email',
  name: 'email',
  validate: {
    config: ConfigSchema,
    secrets: SecretsSchema,
    params: ParamsSchema
  },
  executor
}; // action executor

exports.actionType = actionType;

async function executor(execOptions) {
  const id = execOptions.id;
  const config = execOptions.config;
  const secrets = execOptions.secrets;
  const params = execOptions.params;
  const services = execOptions.services;
  const transport = {
    user: secrets.user,
    password: secrets.password
  };

  if (config.service !== null) {
    transport.service = config.service;
  } else {
    transport.host = config.host;
    transport.port = config.port;
    transport.secure = getSecureValue(config.secure, config.port);
  }

  const sendEmailOptions = {
    transport,
    routing: {
      from: config.from,
      to: params.to,
      cc: params.cc,
      bcc: params.bcc
    },
    content: {
      subject: params.subject,
      message: params.message
    }
  };
  let result;

  try {
    result = await (0, _send_email.sendEmail)(services, sendEmailOptions);
  } catch (err) {
    const message = _i18n.i18n.translate('xpack.actions.builtin.email.errorSendingErrorMessage', {
      defaultMessage: 'error in action "{id}" sending email: {errorMessage}',
      values: {
        id,
        errorMessage: err.message
      }
    });

    return {
      status: 'error',
      message
    };
  }

  return {
    status: 'ok',
    data: result
  };
} // utilities


const ValidServiceNames = getValidServiceNames();

function isValidService(service) {
  return ValidServiceNames.has(service.toLowerCase());
}

function getValidServiceNames() {
  const result = new Set(); // add our special json service

  result.add(_send_email.JSON_TRANSPORT_SERVICE);
  const keys = Object.keys(_services.default);

  for (const key of keys) {
    result.add(key.toLowerCase());
    const record = _services.default[key];
    if (record.aliases == null) continue;

    for (const alias of record.aliases) {
      result.add(alias.toLowerCase());
    }
  }

  return result;
} // Returns the secure value - whether to use TLS or not.
// Respect value if not null | undefined.
// Otherwise, if the port is 465, return true, otherwise return false.
// Based on data here:
// - https://github.com/nodemailer/nodemailer/blob/master/lib/well-known/services.json


function getSecureValue(secure, port) {
  if (secure != null) return secure;
  if (port === 465) return true;
  return false;
}