"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createRoute = createRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createRoute(server) {
  server.route({
    method: 'POST',
    path: `/api/action`,
    options: {
      tags: ['access:actions-all'],
      validate: {
        options: {
          abortEarly: false
        },
        payload: _joi.default.object().keys({
          description: _joi.default.string().required(),
          actionTypeId: _joi.default.string().required(),
          config: _joi.default.object().default({}),
          secrets: _joi.default.object().default({})
        }).required()
      }
    },

    async handler(request) {
      const actionsClient = request.getActionsClient();
      const action = request.payload;
      return await actionsClient.create({
        action
      });
    }

  });
}