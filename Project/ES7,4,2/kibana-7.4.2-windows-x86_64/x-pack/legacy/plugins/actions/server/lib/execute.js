"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.execute = execute;

var _validate_with_schema = require("./validate_with_schema");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function execute({
  actionId,
  namespace,
  actionTypeRegistry,
  services,
  params,
  encryptedSavedObjectsPlugin
}) {
  // Ensure user can read the action before processing
  const {
    attributes: {
      actionTypeId,
      config,
      description
    }
  } = await services.savedObjectsClient.get('action', actionId); // Only get encrypted attributes here, the remaining attributes can be fetched in
  // the savedObjectsClient call

  const {
    attributes: {
      secrets
    }
  } = await encryptedSavedObjectsPlugin.getDecryptedAsInternalUser('action', actionId, {
    namespace
  });
  const actionType = actionTypeRegistry.get(actionTypeId);
  let validatedParams;
  let validatedConfig;
  let validatedSecrets;

  try {
    validatedParams = (0, _validate_with_schema.validateParams)(actionType, params);
    validatedConfig = (0, _validate_with_schema.validateConfig)(actionType, config);
    validatedSecrets = (0, _validate_with_schema.validateSecrets)(actionType, secrets);
  } catch (err) {
    return {
      status: 'error',
      message: err.message,
      retry: false
    };
  }

  let result = null;
  const actionLabel = `${actionId} - ${actionTypeId} - ${description}`;

  try {
    result = await actionType.executor({
      id: actionId,
      services,
      params: validatedParams,
      config: validatedConfig,
      secrets: validatedSecrets
    });
  } catch (err) {
    services.log(['warning', 'x-pack', 'actions'], `action executed unsuccessfully: ${actionLabel} - ${err.message}`);
    throw err;
  }

  services.log(['debug', 'x-pack', 'actions'], `action executed successfully: ${actionLabel}`); // return basic response if none provided

  if (result == null) return {
    status: 'ok'
  };
  return result;
}