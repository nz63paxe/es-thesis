"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMockServer = createMockServer;

var _hapi = _interopRequireDefault(require("hapi"));

var _mocks = require("../../../../../../src/core/server/mocks");

var _actions_client = require("../actions_client.mock");

var _action_type_registry = require("../action_type_registry.mock");

var _plugin = require("../../../encrypted_saved_objects/server/plugin.mock");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const defaultConfig = {
  'kibana.index': '.kibana'
};

function createMockServer(config = defaultConfig) {
  const server = new _hapi.default.Server({
    port: 0
  });

  const actionsClient = _actions_client.actionsClientMock.create();

  const actionTypeRegistry = _action_type_registry.actionTypeRegistryMock.create();

  const savedObjectsClient = _mocks.SavedObjectsClientMock.create();

  const encryptedSavedObjects = _plugin.encryptedSavedObjectsMock.create();

  server.config = () => {
    return {
      get(key) {
        return config[key];
      },

      has(key) {
        return config.hasOwnProperty(key);
      }

    };
  };

  server.register({
    name: 'actions',

    register(pluginServer) {
      pluginServer.expose('registerType', actionTypeRegistry.register);
      pluginServer.expose('listTypes', actionTypeRegistry.list);
    }

  });
  server.register({
    name: 'encrypted_saved_objects',

    register(pluginServer) {
      pluginServer.expose('isEncryptionError', encryptedSavedObjects.isEncryptionError);
      pluginServer.expose('registerType', encryptedSavedObjects.registerType);
      pluginServer.expose('getDecryptedAsInternalUser', encryptedSavedObjects.getDecryptedAsInternalUser);
    }

  });
  server.decorate('request', 'getSavedObjectsClient', () => savedObjectsClient);
  server.decorate('request', 'getActionsClient', () => actionsClient);
  server.decorate('request', 'getBasePath', () => '/s/my-space');
  return {
    server,
    savedObjectsClient,
    actionsClient,
    actionTypeRegistry
  };
}