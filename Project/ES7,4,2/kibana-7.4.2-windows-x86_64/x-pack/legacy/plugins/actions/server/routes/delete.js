"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteRoute = deleteRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function deleteRoute(server) {
  server.route({
    method: 'DELETE',
    path: `/api/action/{id}`,
    options: {
      tags: ['access:actions-all'],
      validate: {
        params: _joi.default.object().keys({
          id: _joi.default.string().required()
        }).required()
      }
    },

    async handler(request, h) {
      const {
        id
      } = request.params;
      const actionsClient = request.getActionsClient();
      await actionsClient.delete({
        id
      });
      return h.response().code(204);
    }

  });
}