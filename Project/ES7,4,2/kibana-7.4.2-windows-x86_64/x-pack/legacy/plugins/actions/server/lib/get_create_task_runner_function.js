"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getCreateTaskRunnerFunction = getCreateTaskRunnerFunction;

var _execute = require("./execute");

var _executor_error = require("./executor_error");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getCreateTaskRunnerFunction({
  getServices,
  actionTypeRegistry,
  encryptedSavedObjectsPlugin,
  spaceIdToNamespace,
  getBasePath,
  isSecurityEnabled
}) {
  return ({
    taskInstance
  }) => {
    return {
      run: async () => {
        const {
          spaceId,
          actionTaskParamsId
        } = taskInstance.params;
        const namespace = spaceIdToNamespace(spaceId);
        const {
          attributes: {
            actionId,
            params,
            apiKey
          }
        } = await encryptedSavedObjectsPlugin.getDecryptedAsInternalUser('action_task_params', actionTaskParamsId, {
          namespace
        });
        const requestHeaders = {};

        if (isSecurityEnabled && !apiKey) {
          throw new _executor_error.ExecutorError('API key is required. The attribute "apiKey" is missing.');
        } else if (isSecurityEnabled) {
          requestHeaders.authorization = `ApiKey ${apiKey}`;
        } // Since we're using API keys and accessing elasticsearch can only be done
        // via a request, we're faking one with the proper authorization headers.


        const fakeRequest = {
          headers: requestHeaders,
          getBasePath: () => getBasePath(spaceId)
        };
        const executorResult = await (0, _execute.execute)({
          namespace,
          actionTypeRegistry,
          encryptedSavedObjectsPlugin,
          actionId,
          services: getServices(fakeRequest),
          params
        });

        if (executorResult.status === 'error') {
          // Task manager error handler only kicks in when an error thrown (at this time)
          // So what we have to do is throw when the return status is `error`.
          throw new _executor_error.ExecutorError(executorResult.message, executorResult.data, executorResult.retry == null ? false : executorResult.retry);
        }
      }
    };
  };
}