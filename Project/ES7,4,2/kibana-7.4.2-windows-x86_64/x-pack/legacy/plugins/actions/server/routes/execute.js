"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.executeRoute = executeRoute;

var _joi = _interopRequireDefault(require("joi"));

var _lib = require("../lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function executeRoute({
  server,
  actionTypeRegistry,
  getServices
}) {
  server.route({
    method: 'POST',
    path: '/api/action/{id}/_execute',
    options: {
      tags: ['access:actions-read'],
      response: {
        emptyStatusCode: 204
      },
      validate: {
        options: {
          abortEarly: false
        },
        params: _joi.default.object().keys({
          id: _joi.default.string().required()
        }).required(),
        payload: _joi.default.object().keys({
          params: _joi.default.object().required()
        }).required()
      }
    },

    async handler(request, h) {
      const {
        id
      } = request.params;
      const {
        params
      } = request.payload;
      const namespace = server.plugins.spaces && server.plugins.spaces.getSpaceId(request);
      const result = await (0, _lib.execute)({
        params,
        actionTypeRegistry,
        actionId: id,
        namespace: namespace === 'default' ? undefined : namespace,
        services: getServices(request),
        encryptedSavedObjectsPlugin: server.plugins.encrypted_saved_objects
      });
      return result;
    }

  });
}