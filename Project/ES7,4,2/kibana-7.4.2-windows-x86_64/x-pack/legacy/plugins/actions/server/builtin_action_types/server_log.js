"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actionType = void 0;

var _i18n = require("@kbn/i18n");

var _configSchema = require("@kbn/config-schema");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DEFAULT_TAGS = ['info', 'alerting']; // params definition

const ParamsSchema = _configSchema.schema.object({
  message: _configSchema.schema.string(),
  tags: _configSchema.schema.arrayOf(_configSchema.schema.string(), {
    defaultValue: DEFAULT_TAGS
  })
}); // action type definition


const actionType = {
  id: '.server-log',
  name: 'server-log',
  validate: {
    params: ParamsSchema
  },
  executor
}; // action executor

exports.actionType = actionType;

async function executor(execOptions) {
  const id = execOptions.id;
  const params = execOptions.params;
  const services = execOptions.services;

  try {
    services.log(params.tags, params.message);
  } catch (err) {
    const message = _i18n.i18n.translate('xpack.actions.builtin.serverLog.errorLoggingErrorMessage', {
      defaultMessage: 'error in action "{id}" logging message: {errorMessage}',
      values: {
        id,
        errorMessage: err.message
      }
    });

    return {
      status: 'error',
      message
    };
  }

  return {
    status: 'ok'
  };
}