"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actionType = void 0;

var _i18n = require("@kbn/i18n");

var _configSchema = require("@kbn/config-schema");

var _nullable = require("./lib/nullable");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ConfigSchema = _configSchema.schema.object({
  index: (0, _nullable.nullableType)(_configSchema.schema.string())
}); // params definition


// see: https://www.elastic.co/guide/en/elastic-stack-overview/current/actions-index.html
// - timeout not added here, as this seems to be a generic thing we want to do
//   eventually: https://github.com/elastic/kibana/projects/26#card-24087404
const ParamsSchema = _configSchema.schema.object({
  index: _configSchema.schema.maybe(_configSchema.schema.string()),
  executionTimeField: _configSchema.schema.maybe(_configSchema.schema.string()),
  refresh: _configSchema.schema.maybe(_configSchema.schema.boolean()),
  documents: _configSchema.schema.arrayOf(_configSchema.schema.recordOf(_configSchema.schema.string(), _configSchema.schema.any()))
}); // action type definition


const actionType = {
  id: '.index',
  name: 'index',
  validate: {
    config: ConfigSchema,
    params: ParamsSchema
  },
  executor
}; // action executor

exports.actionType = actionType;

async function executor(execOptions) {
  const id = execOptions.id;
  const config = execOptions.config;
  const params = execOptions.params;
  const services = execOptions.services;

  if (config.index == null && params.index == null) {
    const message = _i18n.i18n.translate('xpack.actions.builtin.esIndex.indexParamRequiredErrorMessage', {
      defaultMessage: 'index param needs to be set because not set in config for action {id}',
      values: {
        id
      }
    });

    return {
      status: 'error',
      message
    };
  }

  if (config.index != null && params.index != null) {
    services.log(['debug', 'actions'], `index passed in params overridden by index set in config for action ${id}`);
  }

  const index = config.index || params.index;
  const bulkBody = [];

  for (const document of params.documents) {
    if (params.executionTimeField != null) {
      document[params.executionTimeField] = new Date();
    }

    bulkBody.push({
      index: {}
    });
    bulkBody.push(document);
  }

  const bulkParams = {
    index,
    body: bulkBody
  };

  if (params.refresh != null) {
    bulkParams.refresh = params.refresh;
  }

  let result;

  try {
    result = await services.callCluster('bulk', bulkParams);
  } catch (err) {
    const message = _i18n.i18n.translate('xpack.actions.builtin.esIndex.errorIndexingErrorMessage', {
      defaultMessage: 'error in action "{id}" indexing data: {errorMessage}',
      values: {
        id,
        errorMessage: err.message
      }
    });

    return {
      status: 'error',
      message
    };
  }

  return {
    status: 'ok',
    data: result
  };
}