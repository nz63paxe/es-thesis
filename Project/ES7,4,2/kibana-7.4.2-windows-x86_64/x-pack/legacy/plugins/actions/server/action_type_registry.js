"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ActionTypeRegistry = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _i18n = require("@kbn/i18n");

var _lib = require("./lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ActionTypeRegistry {
  constructor({
    getServices,
    taskManager,
    encryptedSavedObjectsPlugin,
    spaceIdToNamespace,
    getBasePath,
    isSecurityEnabled
  }) {
    _defineProperty(this, "taskRunCreatorFunction", void 0);

    _defineProperty(this, "taskManager", void 0);

    _defineProperty(this, "actionTypes", new Map());

    this.taskManager = taskManager;
    this.taskRunCreatorFunction = (0, _lib.getCreateTaskRunnerFunction)({
      isSecurityEnabled,
      getServices,
      actionTypeRegistry: this,
      encryptedSavedObjectsPlugin,
      spaceIdToNamespace,
      getBasePath
    });
  }
  /**
   * Returns if the action type registry has the given action type registered
   */


  has(id) {
    return this.actionTypes.has(id);
  }
  /**
   * Registers an action type to the action type registry
   */


  register(actionType) {
    if (this.has(actionType.id)) {
      throw new Error(_i18n.i18n.translate('xpack.actions.actionTypeRegistry.register.duplicateActionTypeErrorMessage', {
        defaultMessage: 'Action type "{id}" is already registered.',
        values: {
          id: actionType.id
        }
      }));
    }

    this.actionTypes.set(actionType.id, actionType);
    this.taskManager.registerTaskDefinitions({
      [`actions:${actionType.id}`]: {
        title: actionType.name,
        type: `actions:${actionType.id}`,
        maxAttempts: actionType.maxAttempts || 1,

        getRetry(attempts, error) {
          if (error instanceof _lib.ExecutorError) {
            return error.retry == null ? false : error.retry;
          } // Don't retry other kinds of errors


          return false;
        },

        createTaskRunner: this.taskRunCreatorFunction
      }
    });
  }
  /**
   * Returns an action type, throws if not registered
   */


  get(id) {
    if (!this.has(id)) {
      throw _boom.default.badRequest(_i18n.i18n.translate('xpack.actions.actionTypeRegistry.get.missingActionTypeErrorMessage', {
        defaultMessage: 'Action type "{id}" is not registered.',
        values: {
          id
        }
      }));
    }

    return this.actionTypes.get(id);
  }
  /**
   * Returns a list of registered action types [{ id, name }]
   */


  list() {
    return Array.from(this.actionTypes).map(([actionTypeId, actionType]) => ({
      id: actionTypeId,
      name: actionType.name
    }));
  }

}

exports.ActionTypeRegistry = ActionTypeRegistry;