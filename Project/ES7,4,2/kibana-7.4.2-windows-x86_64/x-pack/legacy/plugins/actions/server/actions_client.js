"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ActionsClient = void 0;

var _lib = require("./lib");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class ActionsClient {
  constructor({
    actionTypeRegistry,
    savedObjectsClient
  }) {
    _defineProperty(this, "savedObjectsClient", void 0);

    _defineProperty(this, "actionTypeRegistry", void 0);

    this.actionTypeRegistry = actionTypeRegistry;
    this.savedObjectsClient = savedObjectsClient;
  }
  /**
   * Create an action
   */


  async create({
    action
  }) {
    const {
      actionTypeId,
      description,
      config,
      secrets
    } = action;
    const actionType = this.actionTypeRegistry.get(actionTypeId);
    const validatedActionTypeConfig = (0, _lib.validateConfig)(actionType, config);
    const validatedActionTypeSecrets = (0, _lib.validateSecrets)(actionType, secrets);
    const result = await this.savedObjectsClient.create('action', {
      actionTypeId,
      description,
      config: validatedActionTypeConfig,
      secrets: validatedActionTypeSecrets
    });
    return {
      id: result.id,
      actionTypeId: result.attributes.actionTypeId,
      description: result.attributes.description,
      config: result.attributes.config
    };
  }
  /**
   * Update action
   */


  async update({
    id,
    action
  }) {
    const existingObject = await this.savedObjectsClient.get('action', id);
    const {
      actionTypeId
    } = existingObject.attributes;
    const {
      description,
      config,
      secrets
    } = action;
    const actionType = this.actionTypeRegistry.get(actionTypeId);
    const validatedActionTypeConfig = (0, _lib.validateConfig)(actionType, config);
    const validatedActionTypeSecrets = (0, _lib.validateSecrets)(actionType, secrets);
    const result = await this.savedObjectsClient.update('action', id, {
      actionTypeId,
      description,
      config: validatedActionTypeConfig,
      secrets: validatedActionTypeSecrets
    });
    return {
      id,
      actionTypeId: result.attributes.actionTypeId,
      description: result.attributes.description,
      config: result.attributes.config
    };
  }
  /**
   * Get an action
   */


  async get({
    id
  }) {
    const result = await this.savedObjectsClient.get('action', id);
    return {
      id,
      actionTypeId: result.attributes.actionTypeId,
      description: result.attributes.description,
      config: result.attributes.config
    };
  }
  /**
   * Find actions
   */


  async find({
    options = {}
  }) {
    const findResult = await this.savedObjectsClient.find({ ...options,
      type: 'action'
    });
    return {
      page: findResult.page,
      perPage: findResult.per_page,
      total: findResult.total,
      data: findResult.saved_objects.map(actionFromSavedObject)
    };
  }
  /**
   * Delete action
   */


  async delete({
    id
  }) {
    return await this.savedObjectsClient.delete('action', id);
  }

}

exports.ActionsClient = ActionsClient;

function actionFromSavedObject(savedObject) {
  return {
    id: savedObject.id,
    ...savedObject.attributes
  };
}