"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.updateRoute = updateRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function updateRoute(server) {
  server.route({
    method: 'PUT',
    path: `/api/action/{id}`,
    options: {
      tags: ['access:actions-all'],
      validate: {
        options: {
          abortEarly: false
        },
        params: _joi.default.object().keys({
          id: _joi.default.string().required()
        }).required(),
        payload: _joi.default.object().keys({
          description: _joi.default.string().required(),
          config: _joi.default.object().default({}),
          secrets: _joi.default.object().default({})
        }).required()
      }
    },

    async handler(request) {
      const {
        id
      } = request.params;
      const {
        description,
        config,
        secrets
      } = request.payload;
      const actionsClient = request.getActionsClient();
      return await actionsClient.update({
        id,
        action: {
          description,
          config,
          secrets
        }
      });
    }

  });
}