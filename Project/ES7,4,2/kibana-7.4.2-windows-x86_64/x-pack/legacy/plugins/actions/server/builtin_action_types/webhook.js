"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actionType = void 0;

var _i18n = require("@kbn/i18n");

var _axios = _interopRequireDefault(require("axios"));

var _configSchema = require("@kbn/config-schema");

var _http_rersponse_retry_header = require("./lib/http_rersponse_retry_header");

var _nullable = require("./lib/nullable");

var _result_type = require("./lib/result_type");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// config definition
var WebhookMethods;

(function (WebhookMethods) {
  WebhookMethods["POST"] = "post";
  WebhookMethods["PUT"] = "put";
})(WebhookMethods || (WebhookMethods = {}));

const HeadersSchema = _configSchema.schema.recordOf(_configSchema.schema.string(), _configSchema.schema.string());

const ConfigSchema = _configSchema.schema.object({
  url: _configSchema.schema.string(),
  method: _configSchema.schema.oneOf([_configSchema.schema.literal(WebhookMethods.POST), _configSchema.schema.literal(WebhookMethods.PUT)], {
    defaultValue: WebhookMethods.POST
  }),
  headers: (0, _nullable.nullableType)(HeadersSchema)
}); // secrets definition


const SecretsSchema = _configSchema.schema.object({
  user: _configSchema.schema.string(),
  password: _configSchema.schema.string()
}); // params definition


const ParamsSchema = _configSchema.schema.object({
  body: _configSchema.schema.maybe(_configSchema.schema.string())
}); // action type definition


const actionType = {
  id: '.webhook',
  name: 'webhook',
  validate: {
    config: ConfigSchema,
    secrets: SecretsSchema,
    params: ParamsSchema
  },
  executor
}; // action executor

exports.actionType = actionType;

async function executor(execOptions) {
  const log = (level, msg) => execOptions.services.log([level, 'actions', 'webhook'], msg);

  const id = execOptions.id;
  const {
    method,
    url,
    headers = {}
  } = execOptions.config;
  const {
    user: username,
    password
  } = execOptions.secrets;
  const {
    body: data
  } = execOptions.params;
  const result = await (0, _result_type.promiseResult)(_axios.default.request({
    method,
    url,
    auth: {
      username,
      password
    },
    headers,
    data
  }));

  if ((0, _result_type.isOk)(result)) {
    const {
      value: {
        status,
        statusText
      }
    } = result;
    log('debug', `response from ${id} webhook event: [HTTP ${status}] ${statusText}`);
    return successResult(data);
  } else {
    const {
      error
    } = result;

    if (error.response) {
      const {
        status,
        statusText,
        headers: responseHeaders
      } = error.response;
      const message = `[${status}] ${statusText}`;
      log(`warn`, `error on ${id} webhook event: ${message}`); // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // special handling for 5xx

      if (status >= 500) {
        return retryResult(id, message);
      } // special handling for rate limiting


      if (status === 429) {
        return (0, _http_rersponse_retry_header.getRetryAfterIntervalFromHeaders)(responseHeaders).map(retry => retryResultSeconds(id, message, retry)).getOrElse(retryResult(id, message));
      }

      return errorResultInvalid(id, message);
    }

    const message = _i18n.i18n.translate('xpack.actions.builtin.webhook.unreachableRemoteWebhook', {
      defaultMessage: 'Unreachable Remote Webhook, are you sure the address is correct?'
    });

    log(`warn`, `error on ${id} webhook event: ${message}`);
    return errorResultUnreachable(id, message);
  }
} // Action Executor Result w/ internationalisation


function successResult(data) {
  return {
    status: 'ok',
    data
  };
}

function errorResultInvalid(id, message) {
  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.webhook.invalidResponseErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" calling a remote webhook: {message}',
    values: {
      id,
      message
    }
  });

  return {
    status: 'error',
    message: errMessage
  };
}

function errorResultUnreachable(id, message) {
  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.webhook.unreachableErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" calling a remote webhook: {message}',
    values: {
      id,
      message
    }
  });

  return {
    status: 'error',
    message: errMessage
  };
}

function retryResult(id, message) {
  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.webhook.invalidResponseRetryLaterErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" calling a remote webhook, retry later',
    values: {
      id
    }
  });

  return {
    status: 'error',
    message: errMessage,
    retry: true
  };
}

function retryResultSeconds(id, message, retryAfter) {
  const retryEpoch = Date.now() + retryAfter * 1000;
  const retry = new Date(retryEpoch);
  const retryString = retry.toISOString();

  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.webhook.invalidResponseRetryDateErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" calling a remote webhook, retry at {retryString}: {message}',
    values: {
      id,
      retryString,
      message
    }
  });

  return {
    status: 'error',
    message: errMessage,
    retry
  };
}