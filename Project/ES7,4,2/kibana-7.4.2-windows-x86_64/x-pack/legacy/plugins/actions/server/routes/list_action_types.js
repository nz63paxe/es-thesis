"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.listActionTypesRoute = listActionTypesRoute;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function listActionTypesRoute(server) {
  server.route({
    method: 'GET',
    path: `/api/action/types`,
    options: {
      tags: ['access:actions-read']
    },

    async handler(request) {
      return request.server.plugins.actions.listTypes();
    }

  });
}