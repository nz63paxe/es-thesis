"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getActionType = getActionType;
exports.actionType = void 0;

var _i18n = require("@kbn/i18n");

var _configSchema = require("@kbn/config-schema");

var _webhook = require("@slack/webhook");

var _http_rersponse_retry_header = require("./lib/http_rersponse_retry_header");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const SecretsSchema = _configSchema.schema.object({
  webhookUrl: _configSchema.schema.string()
}); // params definition


const ParamsSchema = _configSchema.schema.object({
  message: _configSchema.schema.string()
}); // action type definition
// customizing executor is only used for tests


function getActionType({
  executor
} = {}) {
  if (executor == null) executor = slackExecutor;
  return {
    id: '.slack',
    name: 'slack',
    validate: {
      secrets: SecretsSchema,
      params: ParamsSchema
    },
    executor
  };
} // the production executor for this action


const actionType = getActionType(); // action executor

exports.actionType = actionType;

async function slackExecutor(execOptions) {
  const id = execOptions.id;
  const secrets = execOptions.secrets;
  const params = execOptions.params;
  let result;
  const {
    webhookUrl
  } = secrets;
  const {
    message
  } = params;

  try {
    const webhook = new _webhook.IncomingWebhook(webhookUrl);
    result = await webhook.send(message);
  } catch (err) {
    if (err.original == null || err.original.response == null) {
      return errorResult(id, err.message);
    }

    const {
      status,
      statusText,
      headers
    } = err.original.response; // special handling for 5xx

    if (status >= 500) {
      return retryResult(id, err.message);
    } // special handling for rate limiting


    if (status === 429) {
      return (0, _http_rersponse_retry_header.getRetryAfterIntervalFromHeaders)(headers).map(retry => retryResultSeconds(id, err.message, retry)).getOrElse(retryResult(id, err.message));
    }

    return errorResult(id, `${err.message} - ${statusText}`);
  }

  if (result == null) {
    const errMessage = _i18n.i18n.translate('xpack.actions.builtin.slack.unexpectedNullResponseErrorMessage', {
      defaultMessage: 'unexpected null response from slack'
    });

    return errorResult(id, errMessage);
  }

  if (result.text !== 'ok') {
    const errMessage = _i18n.i18n.translate('xpack.actions.builtin.slack.unexpectedTextResponseErrorMessage', {
      defaultMessage: 'unexpected text response from slack'
    });

    return errorResult(id, errMessage);
  }

  return successResult(result);
}

function successResult(data) {
  return {
    status: 'ok',
    data
  };
}

function errorResult(id, message) {
  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.slack.errorPostingErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" posting a slack message: {message}',
    values: {
      id,
      message
    }
  });

  return {
    status: 'error',
    message: errMessage
  };
}

function retryResult(id, message) {
  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.slack.errorPostingRetryLaterErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" posting a slack message, retry later',
    values: {
      id
    }
  });

  return {
    status: 'error',
    message: errMessage,
    retry: true
  };
}

function retryResultSeconds(id, message, retryAfter) {
  const retryEpoch = Date.now() + retryAfter * 1000;
  const retry = new Date(retryEpoch);
  const retryString = retry.toISOString();

  const errMessage = _i18n.i18n.translate('xpack.actions.builtin.slack.errorPostingRetryDateErrorMessage', {
    defaultMessage: 'an error occurred in action "{id}" posting a slack message, retry at {retryString}: {message}',
    values: {
      id,
      retryString,
      message
    }
  });

  return {
    status: 'error',
    message: errMessage,
    retry
  };
}