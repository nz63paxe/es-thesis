"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRoute = getRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getRoute(server) {
  server.route({
    method: 'GET',
    path: `/api/action/{id}`,
    options: {
      tags: ['access:actions-read'],
      validate: {
        params: _joi.default.object().keys({
          id: _joi.default.string().required()
        }).required()
      }
    },

    async handler(request) {
      const {
        id
      } = request.params;
      const actionsClient = request.getActionsClient();
      return await actionsClient.get({
        id
      });
    }

  });
}