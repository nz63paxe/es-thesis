"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.findRoute = findRoute;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function findRoute(server) {
  server.route({
    method: 'GET',
    path: `/api/action/_find`,
    options: {
      tags: ['access:actions-read'],
      validate: {
        query: _joi.default.object().keys({
          per_page: _joi.default.number().min(0).default(20),
          page: _joi.default.number().min(1).default(1),
          search: _joi.default.string().allow('').optional(),
          default_search_operator: _joi.default.string().valid('OR', 'AND').default('OR'),
          search_fields: _joi.default.array().items(_joi.default.string()).single(),
          sort_field: _joi.default.string(),
          has_reference: _joi.default.object().keys({
            type: _joi.default.string().required(),
            id: _joi.default.string().required()
          }).optional(),
          fields: _joi.default.array().items(_joi.default.string()).single()
        }).default()
      }
    },

    async handler(request) {
      const query = request.query;
      const actionsClient = request.getActionsClient();
      return await actionsClient.find({
        options: {
          perPage: query.per_page,
          page: query.page,
          search: query.search,
          defaultSearchOperator: query.default_search_operator,
          searchFields: query.search_fields,
          sortField: query.sort_field,
          hasReference: query.has_reference,
          fields: query.fields
        }
      });
    }

  });
}