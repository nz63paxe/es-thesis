"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerBuiltInActionTypes = registerBuiltInActionTypes;

var _server_log = require("./server_log");

var _slack = require("./slack");

var _email = require("./email");

var _es_index = require("./es_index");

var _pagerduty = require("./pagerduty");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerBuiltInActionTypes(actionTypeRegistry) {
  actionTypeRegistry.register(_server_log.actionType);
  actionTypeRegistry.register(_slack.actionType);
  actionTypeRegistry.register(_email.actionType);
  actionTypeRegistry.register(_es_index.actionType);
  actionTypeRegistry.register(_pagerduty.actionType);
}