"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setupEnvironment = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _xhr = _interopRequireDefault(require("axios/lib/adapters/xhr"));

var _i18n = require("@kbn/i18n");

var _http = require("../../../public/app/services/http");

var _navigation = require("../../../public/app/services/navigation");

var _text = require("../../../public/app/services/text");

var _mocks = require("../../../public/test/mocks");

var _http_requests = require("./http_requests");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const setupEnvironment = () => {
  _http.httpService.init(_axios.default.create({
    adapter: _xhr.default
  }), {
    addBasePath: path => path
  });

  _navigation.breadcrumbService.init(_mocks.chrome, {});

  _text.textService.init(_i18n.i18n);

  const {
    server,
    httpRequestsMockHelpers
  } = (0, _http_requests.init)();
  return {
    server,
    httpRequestsMockHelpers
  };
};

exports.setupEnvironment = setupEnvironment;