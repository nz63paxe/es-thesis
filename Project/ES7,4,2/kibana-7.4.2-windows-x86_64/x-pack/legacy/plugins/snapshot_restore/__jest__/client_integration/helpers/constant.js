"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.REPOSITORY_EDIT = exports.REPOSITORY_NAME = void 0;

var _fixtures = require("../../../test/fixtures");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const REPOSITORY_NAME = 'my-test-repository';
exports.REPOSITORY_NAME = REPOSITORY_NAME;
const REPOSITORY_EDIT = (0, _fixtures.getRepository)({
  name: REPOSITORY_NAME
});
exports.REPOSITORY_EDIT = REPOSITORY_EDIT;