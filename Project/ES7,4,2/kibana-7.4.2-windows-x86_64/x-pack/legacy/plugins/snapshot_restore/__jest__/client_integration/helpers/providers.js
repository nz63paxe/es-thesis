"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithProviders = void 0;

var _react = _interopRequireDefault(require("react"));

var _shim = require("../../../public/shim");

var _index = require("../../../public/app/index");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const {
  core,
  plugins
} = (0, _shim.createShim)();
const appDependencies = {
  core,
  plugins
};

const WithProviders = Comp => {
  const AppDependenciesProvider = (0, _index.setAppDependencies)(appDependencies);
  return props => {
    return _react.default.createElement(AppDependenciesProvider, {
      value: appDependencies
    }, _react.default.createElement(Comp, props));
  };
};

exports.WithProviders = WithProviders;