"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabSummary = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _constants = require("../../../../../constants");

var _index = require("../../../../../index");

var _components = require("../../../../../components");

var _navigation = require("../../../../../services/navigation");

var _snapshot_state = require("./snapshot_state");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TabSummary = function TabSummary(_ref) {
  var snapshotDetails = _ref.snapshotDetails;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      FormattedMessage = _useAppDependencies.core.i18n.FormattedMessage;

  var versionId = snapshotDetails.versionId,
      version = snapshotDetails.version,
      includeGlobalState = snapshotDetails.includeGlobalState,
      indices = snapshotDetails.indices,
      state = snapshotDetails.state,
      startTimeInMillis = snapshotDetails.startTimeInMillis,
      endTimeInMillis = snapshotDetails.endTimeInMillis,
      durationInMillis = snapshotDetails.durationInMillis,
      uuid = snapshotDetails.uuid,
      policyName = snapshotDetails.policyName; // Only show 10 indices initially

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isShowingFullIndicesList = _useState2[0],
      setIsShowingFullIndicesList = _useState2[1];

  var hiddenIndicesCount = indices.length > 10 ? indices.length - 10 : 0;
  var shortIndicesList = indices.length ? _react.default.createElement("ul", null, _toConsumableArray(indices).splice(0, 10).map(function (index) {
    return _react.default.createElement("li", {
      key: index
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("span", null, index)));
  }), hiddenIndicesCount ? _react.default.createElement("li", {
    key: "hiddenIndicesCount"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement(_eui.EuiLink, {
    onClick: function onClick() {
      return setIsShowingFullIndicesList(true);
    }
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIndicesShowAllLink",
    defaultMessage: "Show {count} more {count, plural, one {index} other {indices}}",
    values: {
      count: hiddenIndicesCount
    }
  }), ' ', _react.default.createElement(_eui.EuiIcon, {
    type: "arrowDown"
  })))) : null) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIndicesNoneLabel",
    defaultMessage: "-"
  });
  var fullIndicesList = indices.length && indices.length > 10 ? _react.default.createElement("ul", null, indices.map(function (index) {
    return _react.default.createElement("li", {
      key: index
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("span", null, index)));
  }), hiddenIndicesCount ? _react.default.createElement("li", {
    key: "hiddenIndicesCount"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement(_eui.EuiLink, {
    onClick: function onClick() {
      return setIsShowingFullIndicesList(false);
    }
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIndicesCollapseAllLink",
    defaultMessage: "Hide {count, plural, one {# index} other {# indices}}",
    values: {
      count: hiddenIndicesCount
    }
  }), ' ', _react.default.createElement(_eui.EuiIcon, {
    type: "arrowUp"
  })))) : null) : null; // Reset indices list state when clicking through different snapshots

  (0, _react.useEffect)(function () {
    return function () {
      setIsShowingFullIndicesList(false);
    };
  }, []);
  return _react.default.createElement(_eui.EuiDescriptionList, {
    textStyle: "reverse"
  }, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "version"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemVersionLabel",
    defaultMessage: "Version / Version ID"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, version, " / ", versionId)), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "uuid"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemUuidLabel",
    defaultMessage: "UUID"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, uuid))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "state"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemStateLabel",
    defaultMessage: "State"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_snapshot_state.SnapshotState, {
    state: state
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "includeGlobalState"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIncludeGlobalStateLabel",
    defaultMessage: "Includes global state"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, includeGlobalState ? _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIncludeGlobalStateYesLabel",
    defaultMessage: "Yes"
  }) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIncludeGlobalStateNoLabel",
    defaultMessage: "No"
  })))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "indices"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemIndicesLabel",
    defaultMessage: "Indices ({indicesCount})",
    values: {
      indicesCount: indices.length
    }
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_eui.EuiText, null, isShowingFullIndicesList ? fullIndicesList : shortIndicesList)))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "startTime"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemStartTimeLabel",
    defaultMessage: "Start time"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_components.DataPlaceholder, {
    data: startTimeInMillis
  }, _react.default.createElement(_components.FormattedDateTime, {
    epochMs: startTimeInMillis
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "endTime"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemEndTimeLabel",
    defaultMessage: "End time"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, state === _constants.SNAPSHOT_STATE.IN_PROGRESS ? _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: "m"
  }) : _react.default.createElement(_components.DataPlaceholder, {
    data: endTimeInMillis
  }, _react.default.createElement(_components.FormattedDateTime, {
    epochMs: endTimeInMillis
  }))))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "duration"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemDurationLabel",
    defaultMessage: "Duration"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, state === _constants.SNAPSHOT_STATE.IN_PROGRESS ? _react.default.createElement(_eui.EuiLoadingSpinner, {
    size: "m"
  }) : _react.default.createElement(_components.DataPlaceholder, {
    data: durationInMillis
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.itemDurationValueLabel",
    "data-test-subj": "srSnapshotDetailsDurationValue",
    defaultMessage: "{seconds} {seconds, plural, one {second} other {seconds}}",
    values: {
      seconds: Math.ceil(durationInMillis / 1000)
    }
  })))), policyName ? _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "policy"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.snapshotDetails.createdByLabel",
    defaultMessage: "Created by"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_eui.EuiLink, {
    href: (0, _navigation.linkToPolicy)(policyName)
  }, policyName))) : null));
};

exports.TabSummary = TabSummary;