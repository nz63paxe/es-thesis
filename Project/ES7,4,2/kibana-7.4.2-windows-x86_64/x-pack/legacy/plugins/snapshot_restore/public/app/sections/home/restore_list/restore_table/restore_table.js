"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RestoreTable = void 0;

var _react = _interopRequireWildcard(require("react"));

var _lodash = require("lodash");

var _eui = require("@elastic/eui");

var _services = require("@elastic/eui/lib/services");

var _constants = require("../../../../constants");

var _index = require("../../../../index");

var _ui_metric = require("../../../../services/ui_metric");

var _components = require("../../../../components");

var _shards_table = require("./shards_table");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var RestoreTable = function RestoreTable(_ref) {
  var restores = _ref.restores;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  var FormattedMessage = i18n.FormattedMessage;
  var trackUiMetric = _ui_metric.uiMetricService.trackUiMetric; // Track restores to show based on sort and pagination state

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      currentRestores = _useState2[0],
      setCurrentRestores = _useState2[1]; // Sort state


  var _useState3 = (0, _react.useState)({
    sort: {
      field: 'isComplete',
      direction: 'asc'
    }
  }),
      _useState4 = _slicedToArray(_useState3, 2),
      sorting = _useState4[0],
      setSorting = _useState4[1]; // Pagination state


  var _useState5 = (0, _react.useState)({
    pageIndex: 0,
    pageSize: 20,
    totalItemCount: restores.length,
    pageSizeOptions: [10, 20, 50]
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      pagination = _useState6[0],
      setPagination = _useState6[1]; // Track expanded indices


  var _useState7 = (0, _react.useState)({}),
      _useState8 = _slicedToArray(_useState7, 2),
      itemIdToExpandedRowMap = _useState8[0],
      setItemIdToExpandedRowMap = _useState8[1]; // On sorting and pagination change


  var onTableChange = function onTableChange(_ref2) {
    var _ref2$page = _ref2.page,
        page = _ref2$page === void 0 ? {} : _ref2$page,
        _ref2$sort = _ref2.sort,
        sort = _ref2$sort === void 0 ? {} : _ref2$sort;
    var pageIndex = page.index,
        pageSize = page.size;
    var sortField = sort.field,
        sortDirection = sort.direction;
    setSorting({
      sort: {
        field: sortField,
        direction: sortDirection
      }
    });
    setPagination(_objectSpread({}, pagination, {
      pageIndex: pageIndex,
      pageSize: pageSize
    }));
  }; // Expand or collapse index details


  var toggleIndexRestoreDetails = function toggleIndexRestoreDetails(restore) {
    var index = restore.index,
        shards = restore.shards;

    var newItemIdToExpandedRowMap = _objectSpread({}, itemIdToExpandedRowMap);

    if (newItemIdToExpandedRowMap[index]) {
      delete newItemIdToExpandedRowMap[index];
    } else {
      trackUiMetric(_constants.UIM_RESTORE_LIST_EXPAND_INDEX);
      newItemIdToExpandedRowMap[index] = _react.default.createElement(_shards_table.ShardsTable, {
        shards: shards
      });
    }

    setItemIdToExpandedRowMap(newItemIdToExpandedRowMap);
  }; // Refresh expanded index details


  var refreshIndexRestoreDetails = function refreshIndexRestoreDetails() {
    var newItemIdToExpandedRowMap = {};
    restores.forEach(function (restore) {
      var index = restore.index,
          shards = restore.shards;

      if (!itemIdToExpandedRowMap[index]) {
        return;
      }

      newItemIdToExpandedRowMap[index] = _react.default.createElement(_shards_table.ShardsTable, {
        shards: shards
      });
      setItemIdToExpandedRowMap(newItemIdToExpandedRowMap);
    });
  }; // Get restores to show based on sort and pagination state


  var getCurrentRestores = function getCurrentRestores() {
    var newRestoresList = _toConsumableArray(restores);

    var _sorting$sort = sorting.sort,
        field = _sorting$sort.field,
        direction = _sorting$sort.direction;
    var pageIndex = pagination.pageIndex,
        pageSize = pagination.pageSize;
    var sortedRestores = (0, _lodash.sortByOrder)(newRestoresList, [field], [direction]);
    return sortedRestores.slice(pageIndex * pageSize, (pageIndex + 1) * pageSize);
  }; // Update current restores to show if table changes


  (0, _react.useEffect)(function () {
    setCurrentRestores(getCurrentRestores());
  }, [sorting, pagination]); // Update current restores to show if data changes
  // as well as any expanded index details

  (0, _react.useEffect)(function () {
    setPagination(_objectSpread({}, pagination, {
      totalItemCount: restores.length
    }));
    setCurrentRestores(getCurrentRestores());
    refreshIndexRestoreDetails();
  }, [restores]);
  var columns = [{
    field: 'index',
    name: i18n.translate('xpack.snapshotRestore.restoreList.table.indexColumnTitle', {
      defaultMessage: 'Index'
    }),
    truncateText: true,
    sortable: true
  }, {
    field: 'isComplete',
    name: i18n.translate('xpack.snapshotRestore.restoreList.table.statusColumnTitle', {
      defaultMessage: 'Status'
    }),
    truncateText: true,
    sortable: true,
    render: function render(isComplete) {
      return isComplete ? _react.default.createElement(_eui.EuiHealth, {
        color: "success"
      }, _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreList.table.statusColumn.completeLabel",
        defaultMessage: "Complete"
      })) : _react.default.createElement(_eui.EuiHealth, {
        color: "warning"
      }, _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreList.table.statusColumn.inProgressLabel",
        defaultMessage: "In progress"
      }));
    }
  }, {
    field: 'latestActivityTimeInMillis',
    name: i18n.translate('xpack.snapshotRestore.restoreList.table.lastActivityTitle', {
      defaultMessage: 'Last activity'
    }),
    truncateText: true,
    render: function render(latestActivityTimeInMillis, _ref3) {
      var isComplete = _ref3.isComplete;
      return isComplete ? _react.default.createElement(_components.FormattedDateTime, {
        epochMs: latestActivityTimeInMillis
      }) : _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreList.table.lastActivityColumn.nowLabel",
        defaultMessage: "now"
      });
    }
  }, {
    field: 'shards',
    name: i18n.translate('xpack.snapshotRestore.restoreList.table.shardsCompletedTitle', {
      defaultMessage: 'Shards completed'
    }),
    truncateText: true,
    render: function render(shards) {
      return shards.filter(function (shard) {
        return Boolean(shard.stopTimeInMillis);
      }).length;
    }
  }, {
    field: 'shards',
    name: i18n.translate('xpack.snapshotRestore.restoreList.table.shardsInProgressTitle', {
      defaultMessage: 'Shards in progress'
    }),
    truncateText: true,
    render: function render(shards) {
      return shards.filter(function (shard) {
        return !Boolean(shard.stopTimeInMillis);
      }).length;
    }
  }, {
    align: _services.RIGHT_ALIGNMENT,
    width: '40px',
    isExpander: true,
    render: function render(item) {
      return _react.default.createElement(_eui.EuiButtonIcon, {
        onClick: function onClick() {
          return toggleIndexRestoreDetails(item);
        },
        "aria-label": itemIdToExpandedRowMap[item.index] ? 'Collapse' : 'Expand',
        iconType: itemIdToExpandedRowMap[item.index] ? 'arrowUp' : 'arrowDown'
      });
    }
  }];
  return _react.default.createElement(_eui.EuiBasicTable, {
    items: currentRestores,
    itemId: "index",
    itemIdToExpandedRowMap: itemIdToExpandedRowMap,
    isExpandable: true,
    columns: columns,
    sorting: sorting,
    pagination: pagination,
    onChange: onTableChange,
    rowProps: function rowProps(restore) {
      return {
        'data-test-subj': 'row',
        onClick: function onClick() {
          return toggleIndexRestoreDetails(restore);
        }
      };
    },
    cellProps: function cellProps() {
      return {
        'data-test-subj': 'cell'
      };
    },
    "data-test-subj": "restoresTable"
  });
};

exports.RestoreTable = RestoreTable;