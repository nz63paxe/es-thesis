"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RestoreSnapshotStepReview = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _lib = require("../../../../../common/lib");

var _index = require("../../../index");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var RestoreSnapshotStepReview = function RestoreSnapshotStepReview(_ref) {
  var restoreSettings = _ref.restoreSettings,
      updateCurrentStep = _ref.updateCurrentStep;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  var FormattedMessage = i18n.FormattedMessage;
  var restoreIndices = restoreSettings.indices,
      renamePattern = restoreSettings.renamePattern,
      renameReplacement = restoreSettings.renameReplacement,
      partial = restoreSettings.partial,
      includeGlobalState = restoreSettings.includeGlobalState,
      ignoreIndexSettings = restoreSettings.ignoreIndexSettings;
  var serializedRestoreSettings = (0, _lib.serializeRestoreSettings)(restoreSettings);
  var serializedIndexSettings = serializedRestoreSettings.index_settings;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isShowingFullIndicesList = _useState2[0],
      setIsShowingFullIndicesList = _useState2[1];

  var displayIndices = restoreIndices ? typeof restoreIndices === 'string' ? restoreIndices.split(',') : restoreIndices : undefined;
  var hiddenIndicesCount = displayIndices && displayIndices.length > 10 ? displayIndices.length - 10 : 0;

  var renderSummaryTab = function renderSummaryTab() {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.sectionLogisticsTitle",
      defaultMessage: "Logistics"
    }), ' ', _react.default.createElement(_eui.EuiToolTip, {
      content: _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.editStepTooltip",
        defaultMessage: "Edit"
      })
    }, _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return updateCurrentStep(1);
      }
    }, _react.default.createElement(_eui.EuiIcon, {
      type: "pencil"
    }))))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.indicesLabel",
      defaultMessage: "Indices"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, displayIndices ? _react.default.createElement(_eui.EuiText, null, _react.default.createElement("ul", null, (isShowingFullIndicesList ? displayIndices : _toConsumableArray(displayIndices).splice(0, 10)).map(function (index) {
      return _react.default.createElement("li", {
        key: index
      }, _react.default.createElement(_eui.EuiTitle, {
        size: "xs"
      }, _react.default.createElement("span", null, index)));
    }), hiddenIndicesCount ? _react.default.createElement("li", {
      key: "hiddenIndicesCount"
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, isShowingFullIndicesList ? _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return setIsShowingFullIndicesList(false);
      }
    }, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.indicesCollapseAllLink",
      defaultMessage: "Hide {count, plural, one {# index} other {# indices}}",
      values: {
        count: hiddenIndicesCount
      }
    }), ' ', _react.default.createElement(_eui.EuiIcon, {
      type: "arrowUp"
    })) : _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return setIsShowingFullIndicesList(true);
      }
    }, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.indicesShowAllLink",
      defaultMessage: "Show {count} more {count, plural, one {index} other {indices}}",
      values: {
        count: hiddenIndicesCount
      }
    }), ' ', _react.default.createElement(_eui.EuiIcon, {
      type: "arrowDown"
    })))) : null)) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.allIndicesValue",
      defaultMessage: "All indices"
    }))))), renamePattern || renameReplacement ? _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("h4", null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.sectionRenameTitle",
      defaultMessage: "Rename indices"
    }))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, renamePattern ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.renamePatternLabel",
      defaultMessage: "Capture pattern"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, renamePattern))) : null, renameReplacement ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.renameReplacementLabel",
      defaultMessage: "Replacement pattern"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, renameReplacement))) : null)) : null, partial !== undefined || includeGlobalState !== undefined ? _react.default.createElement(_eui.EuiFlexGroup, null, partial !== undefined ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.partialLabel",
      defaultMessage: "Partial restore"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, partial ? _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.partialTrueValue",
      defaultMessage: "Yes"
    }) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.partialFalseValue",
      defaultMessage: "No"
    })))) : null, includeGlobalState !== undefined ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.includeGlobalStateLabel",
      defaultMessage: "Restore global state"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, includeGlobalState ? _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.includeGlobalStateTrueValue",
      defaultMessage: "Yes"
    }) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.includeGlobalStateFalseValue",
      defaultMessage: "No"
    })))) : null) : null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.sectionSettingsTitle",
      defaultMessage: "Index settings"
    }), ' ', _react.default.createElement(_eui.EuiToolTip, {
      content: _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.editStepTooltip",
        defaultMessage: "Edit"
      })
    }, _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return updateCurrentStep(2);
      }
    }, _react.default.createElement(_eui.EuiIcon, {
      type: "pencil"
    }))))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), serializedIndexSettings || ignoreIndexSettings ? _react.default.createElement(_eui.EuiFlexGroup, null, serializedIndexSettings ? _react.default.createElement(_eui.EuiFlexItem, {
      style: {
        maxWidth: '50%'
      }
    }, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.indexSettingsLabel",
      defaultMessage: "Modify"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, _react.default.createElement(_eui.EuiFlexGrid, {
      columns: 2,
      gutterSize: "none"
    }, Object.entries(serializedIndexSettings).map(function (_ref2) {
      var _ref3 = _slicedToArray(_ref2, 2),
          setting = _ref3[0],
          value = _ref3[1];

      return _react.default.createElement(_react.Fragment, {
        key: setting
      }, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
        size: "s"
      }, _react.default.createElement("strong", null, setting))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiText, {
        size: "s"
      }, _react.default.createElement("span", null, " ", value))));
    }))))) : null, ignoreIndexSettings ? _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.ignoreIndexSettingsLabel",
      defaultMessage: "Reset"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, _react.default.createElement(_eui.EuiText, null, _react.default.createElement("ul", null, ignoreIndexSettings.map(function (setting) {
      return _react.default.createElement("li", {
        key: setting
      }, _react.default.createElement(_eui.EuiTitle, {
        size: "xs"
      }, _react.default.createElement("span", null, setting)));
    })))))) : null) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.restoreForm.stepReview.summaryTab.noSettingsValue",
      defaultMessage: "No index setting modifications"
    }));
  };

  var renderJsonTab = function renderJsonTab() {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiCodeEditor, {
      mode: "json",
      theme: "textmate",
      isReadOnly: true,
      setOptions: {
        maxLines: Infinity
      },
      value: JSON.stringify(serializedRestoreSettings, null, 2),
      editorProps: {
        $blockScrolling: Infinity
      },
      "aria-label": _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.restoreForm.stepReview.jsonTab.jsonAriaLabel",
        defaultMessage: "Restore settings to be executed"
      })
    }));
  };

  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.restoreForm.stepReviewTitle",
    defaultMessage: "Review restore details"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiTabbedContent, {
    tabs: [{
      id: 'summary',
      name: i18n.translate('xpack.snapshotRestore.restoreForm.stepReview.summaryTabTitle', {
        defaultMessage: 'Summary'
      }),
      content: renderSummaryTab()
    }, {
      id: 'json',
      name: i18n.translate('xpack.snapshotRestore.restoreForm.stepReview.jsonTabTitle', {
        defaultMessage: 'JSON'
      }),
      content: renderJsonTab()
    }]
  }));
};

exports.RestoreSnapshotStepReview = RestoreSnapshotStepReview;