"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _reactDom = require("react-dom");

var _constants = require("../common/constants");

var _app = require("./app");

var _index = _interopRequireDefault(require("./index.html"));

var _navigation = require("./app/services/navigation");

var _documentation = require("./app/services/documentation");

var _http = require("./app/services/http");

var _text = require("./app/services/text");

var _ui_metric = require("./app/services/ui_metric");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var REACT_ROOT_ID = 'snapshotRestoreReactRoot';

var Plugin =
/*#__PURE__*/
function () {
  function Plugin() {
    _classCallCheck(this, Plugin);
  }

  _createClass(Plugin, [{
    key: "start",
    value: function start(core, plugins) {
      var i18n = core.i18n,
          routing = core.routing,
          http = core.http,
          chrome = core.chrome,
          notification = core.notification,
          documentation = core.documentation,
          docTitle = core.docTitle;
      var management = plugins.management,
          uiMetric = plugins.uiMetric; // Register management section

      var esSection = management.sections.getSection('elasticsearch');
      esSection.register(_constants.PLUGIN.ID, {
        visible: true,
        display: i18n.translate('xpack.snapshotRestore.appName', {
          defaultMessage: 'Snapshot and Restore'
        }),
        order: 7,
        url: "#".concat(_app.CLIENT_BASE_PATH)
      }); // Initialize services

      _text.textService.init(i18n);

      _navigation.breadcrumbService.init(chrome, management.constants.BREADCRUMB);

      _ui_metric.uiMetricService.init(uiMetric.createUiStatsReporter);

      _documentation.documentationLinksService.init(documentation.esDocBasePath, documentation.esPluginDocBasePath, documentation.esStackOverviewDocBasePath);

      _navigation.docTitleService.init(docTitle.change);

      var unmountReactApp = function unmountReactApp() {
        var elem = document.getElementById(REACT_ROOT_ID);

        if (elem) {
          (0, _reactDom.unmountComponentAtNode)(elem);
        }
      }; // Register react root


      routing.registerAngularRoute("".concat(_app.CLIENT_BASE_PATH, "/:section?/:subsection?/:view?/:id?"), {
        template: _index.default,
        controllerAs: 'snapshotRestoreController',
        controller: function controller($scope, $route, $http, $q) {
          // NOTE: We depend upon Angular's $http service because it's decorated with interceptors,
          // e.g. to check license status per request.
          http.setClient($http);

          _http.httpService.init(http.getClient(), chrome); // Angular Lifecycle


          var appRoute = $route.current;
          var stopListeningForLocationChange = $scope.$on('$locationChangeSuccess', function () {
            var currentRoute = $route.current;
            var isNavigationInApp = currentRoute.$$route.template === appRoute.$$route.template; // When we navigate within SR, prevent Angular from re-matching the route and rebuild the app

            if (isNavigationInApp) {
              $route.current = appRoute;
            } else {// Any clean up when user leaves SR
            }

            $scope.$on('$destroy', function () {
              if (stopListeningForLocationChange) {
                stopListeningForLocationChange();
              }

              unmountReactApp();
            });
          });
          $scope.$$postDigest(function () {
            unmountReactApp();
            var elem = document.getElementById(REACT_ROOT_ID);

            if (elem) {
              (0, _app.renderReact)(elem, {
                i18n: i18n,
                notification: notification,
                chrome: chrome
              }, {
                management: {
                  sections: management.sections
                }
              });
            }
          });
        }
      });
    }
  }]);

  return Plugin;
}();

exports.Plugin = Plugin;