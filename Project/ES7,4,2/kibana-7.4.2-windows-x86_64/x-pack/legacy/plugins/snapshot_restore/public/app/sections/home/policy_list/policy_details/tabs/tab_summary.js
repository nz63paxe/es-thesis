"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TabSummary = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _index = require("../../../../../index");

var _components = require("../../../../../components");

var _navigation = require("../../../../../services/navigation");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TabSummary = function TabSummary(_ref) {
  var policy = _ref.policy;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  var FormattedMessage = i18n.FormattedMessage;
  var version = policy.version,
      name = policy.name,
      modifiedDateMillis = policy.modifiedDateMillis,
      snapshotName = policy.snapshotName,
      repository = policy.repository,
      schedule = policy.schedule,
      nextExecutionMillis = policy.nextExecutionMillis,
      config = policy.config;

  var _ref2 = config || {
    includeGlobalState: undefined,
    ignoreUnavailable: undefined,
    indices: undefined,
    partial: undefined
  },
      includeGlobalState = _ref2.includeGlobalState,
      ignoreUnavailable = _ref2.ignoreUnavailable,
      indices = _ref2.indices,
      partial = _ref2.partial; // Only show 10 indices initially


  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isShowingFullIndicesList = _useState2[0],
      setIsShowingFullIndicesList = _useState2[1];

  var displayIndices = typeof indices === 'string' ? indices.split(',') : indices;
  var hiddenIndicesCount = displayIndices && displayIndices.length > 10 ? displayIndices.length - 10 : 0;
  var shortIndicesList = displayIndices && displayIndices.length ? _react.default.createElement(_eui.EuiText, {
    size: "m"
  }, _react.default.createElement("ul", null, _toConsumableArray(displayIndices).splice(0, 10).map(function (index) {
    return _react.default.createElement("li", {
      key: index
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("span", null, index)));
  }), hiddenIndicesCount ? _react.default.createElement("li", {
    key: "hiddenIndicesCount"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement(_eui.EuiLink, {
    onClick: function onClick() {
      return setIsShowingFullIndicesList(true);
    }
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.indicesShowAllLink",
    defaultMessage: "Show {count} more {count, plural, one {index} other {indices}}",
    values: {
      count: hiddenIndicesCount
    }
  }), ' ', _react.default.createElement(_eui.EuiIcon, {
    type: "arrowDown"
  })))) : null)) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.allIndicesLabel",
    defaultMessage: "All indices"
  });
  var fullIndicesList = displayIndices && displayIndices.length && displayIndices.length > 10 ? _react.default.createElement(_eui.EuiText, {
    size: "m"
  }, _react.default.createElement("ul", null, displayIndices.map(function (index) {
    return _react.default.createElement("li", {
      key: index
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, _react.default.createElement("span", null, index)));
  }), hiddenIndicesCount ? _react.default.createElement("li", {
    key: "hiddenIndicesCount"
  }, _react.default.createElement(_eui.EuiTitle, {
    size: "xs"
  }, _react.default.createElement(_eui.EuiLink, {
    onClick: function onClick() {
      return setIsShowingFullIndicesList(false);
    }
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.indicesCollapseAllLink",
    defaultMessage: "Hide {count, plural, one {# index} other {# indices}}",
    values: {
      count: hiddenIndicesCount
    }
  }), ' ', _react.default.createElement(_eui.EuiIcon, {
    type: "arrowUp"
  })))) : null)) : null; // Reset indices list state when clicking through different policies

  (0, _react.useEffect)(function () {
    return function () {
      setIsShowingFullIndicesList(false);
    };
  }, []);
  return _react.default.createElement(_eui.EuiDescriptionList, {
    textStyle: "reverse"
  }, _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "version"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.versionLabel",
    defaultMessage: "Version"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, version)), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "modified"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.modifiedDateLabel",
    defaultMessage: "Last modified"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_components.FormattedDateTime, {
    epochMs: modifiedDateMillis
  })))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "name"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.snapshotNameLabel",
    defaultMessage: "Snapshot name"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_eui.EuiLink, {
    href: (0, _navigation.linkToSnapshots)(undefined, name)
  }, snapshotName))), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "repository"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.repositoryLabel",
    defaultMessage: "Repository"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_eui.EuiLink, {
    href: (0, _navigation.linkToRepository)(repository)
  }, repository)))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "schedule"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.scheduleLabel",
    defaultMessage: "Schedule"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, schedule)), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "execution"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.nextExecutionLabel",
    defaultMessage: "Next snapshot"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, _react.default.createElement(_components.FormattedDateTime, {
    epochMs: nextExecutionMillis
  })))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "indices"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.indicesLabel",
    defaultMessage: "Indices"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, isShowingFullIndicesList ? fullIndicesList : shortIndicesList)), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "includeGlobalState"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.ignoreUnavailableLabel",
    defaultMessage: "Ignore unavailable indices"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, ignoreUnavailable ? _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.ignoreUnavailableTrueLabel",
    defaultMessage: "Yes"
  }) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.ignoreUnavailableFalseLabel",
    defaultMessage: "No"
  })))), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "partial"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.partialLabel",
    defaultMessage: "Allow partial shards"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, partial ? _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.partialTrueLabel",
    defaultMessage: "Yes"
  }) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.partialFalseLabel",
    defaultMessage: "No"
  }))), _react.default.createElement(_eui.EuiFlexItem, {
    "data-test-subj": "includeGlobalState"
  }, _react.default.createElement(_eui.EuiDescriptionListTitle, {
    "data-test-subj": "title"
  }, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.includeGlobalStateLabel",
    defaultMessage: "Include global state"
  })), _react.default.createElement(_eui.EuiDescriptionListDescription, {
    className: "eui-textBreakWord",
    "data-test-subj": "value"
  }, includeGlobalState === false ? _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.includeGlobalStateFalseLabel",
    defaultMessage: "No"
  }) : _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyDetails.includeGlobalStateTrueLabel",
    defaultMessage: "Yes"
  })))));
};

exports.TabSummary = TabSummary;