"use strict";

var _plugin = require("./plugin");

var _shim = require("./shim");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var _createShim = (0, _shim.createShim)(),
    core = _createShim.core,
    plugins = _createShim.plugins;

var snapshotRestorePlugin = new _plugin.Plugin();
snapshotRestorePlugin.start(core, plugins);