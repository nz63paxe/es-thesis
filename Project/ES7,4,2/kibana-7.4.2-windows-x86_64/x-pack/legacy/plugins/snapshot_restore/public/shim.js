"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createShim = createShim;

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _i18n2 = require("ui/i18n");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _documentation_links = require("ui/documentation_links");

var _management = require("ui/management");

var _notify = require("ui/notify");

var _routes = _interopRequireDefault(require("ui/routes"));

var _doc_title = require("ui/doc_title/doc_title");

var _public = require("../../../../../src/legacy/core_plugins/ui_metric/public");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function createShim() {
  // This is an Angular service, which is why we use this provider pattern
  // to access it within our React app.
  var httpClient;
  var reactRouter;
  return {
    core: {
      i18n: _objectSpread({}, _i18n.i18n, {
        Context: _i18n2.I18nContext,
        FormattedMessage: _react.FormattedMessage,
        FormattedDate: _react.FormattedDate,
        FormattedTime: _react.FormattedTime
      }),
      routing: {
        registerAngularRoute: function registerAngularRoute(path, config) {
          _routes.default.when(path, config);
        },
        registerRouter: function registerRouter(router) {
          reactRouter = router;
        },
        getRouter: function getRouter() {
          return reactRouter;
        }
      },
      http: {
        setClient: function setClient(client) {
          httpClient = client;
        },
        getClient: function getClient() {
          return httpClient;
        }
      },
      chrome: _chrome.default,
      notification: {
        fatalError: _notify.fatalError,
        toastNotifications: _notify.toastNotifications
      },
      documentation: {
        esDocBasePath: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "guide/en/elasticsearch/reference/").concat(_documentation_links.DOC_LINK_VERSION, "/"),
        esPluginDocBasePath: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "guide/en/elasticsearch/plugins/").concat(_documentation_links.DOC_LINK_VERSION, "/"),
        esStackOverviewDocBasePath: "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "guide/en/elastic-stack-overview/").concat(_documentation_links.DOC_LINK_VERSION, "/")
      },
      docTitle: {
        change: _doc_title.docTitle.change
      }
    },
    plugins: {
      management: {
        sections: _management.management,
        constants: {
          BREADCRUMB: _management.MANAGEMENT_BREADCRUMB
        }
      },
      uiMetric: {
        createUiStatsReporter: _public.createUiStatsReporter
      }
    }
  };
}