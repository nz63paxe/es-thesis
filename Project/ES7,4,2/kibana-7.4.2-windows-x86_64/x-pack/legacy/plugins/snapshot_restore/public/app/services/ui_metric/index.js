"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "uiMetricService", {
  enumerable: true,
  get: function get() {
    return _ui_metric.uiMetricService;
  }
});

var _ui_metric = require("./ui_metric");