"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SnapshotTable = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _constants = require("../../../../constants");

var _index = require("../../../../index");

var _navigation = require("../../../../services/navigation");

var _ui_metric = require("../../../../services/ui_metric");

var _components = require("../../../../components");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var SnapshotTable = function SnapshotTable(_ref) {
  var snapshots = _ref.snapshots,
      repositories = _ref.repositories,
      reload = _ref.reload,
      openSnapshotDetailsUrl = _ref.openSnapshotDetailsUrl,
      onSnapshotDeleted = _ref.onSnapshotDeleted,
      repositoryFilter = _ref.repositoryFilter,
      policyFilter = _ref.policyFilter;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  var FormattedMessage = i18n.FormattedMessage;
  var trackUiMetric = _ui_metric.uiMetricService.trackUiMetric;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      selectedItems = _useState2[0],
      setSelectedItems = _useState2[1];

  var columns = [{
    field: 'snapshot',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.snapshotColumnTitle', {
      defaultMessage: 'Snapshot'
    }),
    truncateText: true,
    sortable: true,
    render: function render(snapshotId, snapshot) {
      return (
        /* eslint-disable-next-line @elastic/eui/href-or-on-click */
        _react.default.createElement(_eui.EuiLink, {
          onClick: function onClick() {
            return trackUiMetric(_constants.UIM_SNAPSHOT_SHOW_DETAILS_CLICK);
          },
          href: openSnapshotDetailsUrl(snapshot.repository, snapshotId),
          "data-test-subj": "snapshotLink"
        }, snapshotId)
      );
    }
  }, {
    field: 'repository',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.repositoryColumnTitle', {
      defaultMessage: 'Repository'
    }),
    truncateText: true,
    sortable: true,
    render: function render(repositoryName) {
      return _react.default.createElement(_eui.EuiLink, {
        href: (0, _navigation.linkToRepository)(repositoryName),
        "data-test-subj": "repositoryLink"
      }, repositoryName);
    }
  }, {
    field: 'indices',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.indicesColumnTitle', {
      defaultMessage: 'Indices'
    }),
    truncateText: true,
    sortable: true,
    width: '100px',
    render: function render(indices) {
      return indices.length;
    }
  }, {
    field: 'shards.total',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.shardsColumnTitle', {
      defaultMessage: 'Shards'
    }),
    truncateText: true,
    sortable: true,
    width: '100px',
    render: function render(totalShards) {
      return totalShards;
    }
  }, {
    field: 'shards.failed',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.failedShardsColumnTitle', {
      defaultMessage: 'Failed shards'
    }),
    truncateText: true,
    sortable: true,
    width: '100px',
    render: function render(failedShards) {
      return failedShards;
    }
  }, {
    field: 'startTimeInMillis',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.startTimeColumnTitle', {
      defaultMessage: 'Date created'
    }),
    truncateText: true,
    sortable: true,
    render: function render(startTimeInMillis) {
      return _react.default.createElement(_components.DataPlaceholder, {
        data: startTimeInMillis
      }, _react.default.createElement(_components.FormattedDateTime, {
        epochMs: startTimeInMillis
      }));
    }
  }, {
    field: 'durationInMillis',
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.durationColumnTitle', {
      defaultMessage: 'Duration'
    }),
    truncateText: true,
    sortable: true,
    width: '100px',
    render: function render(durationInMillis, _ref2) {
      var state = _ref2.state;

      if (state === _constants.SNAPSHOT_STATE.IN_PROGRESS) {
        return _react.default.createElement(_eui.EuiLoadingSpinner, {
          size: "m"
        });
      }

      return _react.default.createElement(_components.DataPlaceholder, {
        data: durationInMillis
      }, _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.snapshotList.table.durationColumnValueLabel",
        defaultMessage: "{seconds}s",
        values: {
          seconds: Math.ceil(durationInMillis / 1000)
        }
      }));
    }
  }, {
    name: i18n.translate('xpack.snapshotRestore.snapshotList.table.actionsColumnTitle', {
      defaultMessage: 'Actions'
    }),
    actions: [{
      render: function render(_ref3) {
        var snapshot = _ref3.snapshot,
            repository = _ref3.repository,
            state = _ref3.state;
        var canRestore = state === _constants.SNAPSHOT_STATE.SUCCESS || state === _constants.SNAPSHOT_STATE.PARTIAL;
        var label = canRestore ? i18n.translate('xpack.snapshotRestore.snapshotList.table.actionRestoreTooltip', {
          defaultMessage: 'Restore'
        }) : state === _constants.SNAPSHOT_STATE.IN_PROGRESS ? i18n.translate('xpack.snapshotRestore.snapshotList.table.actionRestoreDisabledInProgressTooltip', {
          defaultMessage: "Can't restore in-progress snapshot"
        }) : i18n.translate('xpack.snapshotRestore.snapshotList.table.actionRestoreDisabledInvalidTooltip', {
          defaultMessage: "Can't restore invalid snapshot"
        });
        return _react.default.createElement(_eui.EuiToolTip, {
          content: label
        }, _react.default.createElement(_eui.EuiButtonIcon, {
          "aria-label": i18n.translate('xpack.snapshotRestore.snapshotList.table.actionRestoreAriaLabel', {
            defaultMessage: 'Store snapshot `{name}`',
            values: {
              name: snapshot
            }
          }),
          iconType: "importAction",
          color: "primary",
          "data-test-subj": "srsnapshotListRestoreActionButton",
          href: (0, _navigation.linkToRestoreSnapshot)(repository, snapshot),
          isDisabled: !canRestore
        }));
      }
    }, {
      render: function render(_ref4) {
        var snapshot = _ref4.snapshot,
            repository = _ref4.repository,
            isManagedRepository = _ref4.isManagedRepository;
        return _react.default.createElement(_components.SnapshotDeleteProvider, null, function (deleteSnapshotPrompt) {
          var label = !isManagedRepository ? i18n.translate('xpack.snapshotRestore.snapshotList.table.actionDeleteTooltip', {
            defaultMessage: 'Delete'
          }) : i18n.translate('xpack.snapshotRestore.snapshotList.table.deleteManagedRepositorySnapshotTooltip', {
            defaultMessage: 'You cannot delete a snapshot stored in a managed repository.'
          });
          return _react.default.createElement(_eui.EuiToolTip, {
            content: label
          }, _react.default.createElement(_eui.EuiButtonIcon, {
            "aria-label": i18n.translate('xpack.snapshotRestore.snapshotList.table.actionDeleteAriaLabel', {
              defaultMessage: 'Delete snapshot `{name}`',
              values: {
                name: snapshot
              }
            }),
            iconType: "trash",
            color: "danger",
            "data-test-subj": "srsnapshotListDeleteActionButton",
            onClick: function onClick() {
              return deleteSnapshotPrompt([{
                snapshot: snapshot,
                repository: repository
              }], onSnapshotDeleted);
            },
            isDisabled: isManagedRepository
          }));
        });
      }
    }],
    width: '100px'
  }]; // By default, we'll display the most recent snapshots at the top of the table.

  var sorting = {
    sort: {
      field: 'startTimeInMillis',
      direction: 'desc'
    }
  };
  var pagination = {
    initialPageSize: 20,
    pageSizeOptions: [10, 20, 50]
  };
  var searchSchema = {
    fields: {
      repository: {
        type: 'string'
      },
      policyName: {
        type: 'string'
      }
    }
  };
  var selection = {
    onSelectionChange: function onSelectionChange(newSelectedItems) {
      return setSelectedItems(newSelectedItems);
    },
    selectable: function selectable(_ref5) {
      var isManagedRepository = _ref5.isManagedRepository;
      return !isManagedRepository;
    },
    selectableMessage: function selectableMessage(selectable) {
      if (!selectable) {
        return i18n.translate('xpack.snapshotRestore.snapshotList.table.deleteManagedRepositorySnapshotTooltip', {
          defaultMessage: 'You cannot delete a snapshot stored in a managed repository.'
        });
      }
    }
  };
  var search = {
    toolsLeft: selectedItems.length ? _react.default.createElement(_components.SnapshotDeleteProvider, null, function (deleteSnapshotPrompt) {
      return _react.default.createElement(_eui.EuiButton, {
        onClick: function onClick() {
          return deleteSnapshotPrompt(selectedItems.map(function (_ref6) {
            var snapshot = _ref6.snapshot,
                repository = _ref6.repository;
            return {
              snapshot: snapshot,
              repository: repository
            };
          }), onSnapshotDeleted);
        },
        color: "danger",
        "data-test-subj": "srSnapshotListBulkDeleteActionButton"
      }, _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.snapshotList.table.deleteSnapshotButton",
        defaultMessage: "Delete {count, plural, one {snapshot} other {snapshots}}",
        values: {
          count: selectedItems.length
        }
      }));
    }) : undefined,
    toolsRight: _react.default.createElement(_eui.EuiButton, {
      color: "secondary",
      iconType: "refresh",
      onClick: reload,
      "data-test-subj": "reloadButton"
    }, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.snapshotList.table.reloadSnapshotsButton",
      defaultMessage: "Reload"
    })),
    box: {
      incremental: true,
      schema: searchSchema
    },
    filters: [{
      type: 'field_value_selection',
      field: 'repository',
      name: i18n.translate('xpack.snapshotRestore.snapshotList.table.repositoryFilterLabel', {
        defaultMessage: 'Repository'
      }),
      multiSelect: false,
      options: repositories.map(function (repository) {
        return {
          value: repository,
          view: repository
        };
      })
    }],
    defaultQuery: policyFilter ? _eui.Query.parse("policyName=\"".concat(policyFilter, "\""), {
      schema: _objectSpread({}, searchSchema, {
        strict: true
      })
    }) : repositoryFilter ? _eui.Query.parse("repository=\"".concat(repositoryFilter, "\""), {
      schema: _objectSpread({}, searchSchema, {
        strict: true
      })
    }) : ''
  };
  return _react.default.createElement(_eui.EuiInMemoryTable, {
    items: snapshots,
    itemId: "uuid",
    columns: columns,
    search: search,
    sorting: sorting,
    isSelectable: true,
    selection: selection,
    pagination: pagination,
    rowProps: function rowProps() {
      return {
        'data-test-subj': 'row'
      };
    },
    cellProps: function cellProps() {
      return {
        'data-test-subj': 'cell'
      };
    },
    "data-test-subj": "snapshotTable"
  });
};

exports.SnapshotTable = SnapshotTable;