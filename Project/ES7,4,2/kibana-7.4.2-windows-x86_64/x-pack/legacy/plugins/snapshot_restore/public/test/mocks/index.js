"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "chrome", {
  enumerable: true,
  get: function get() {
    return _chrome.chrome;
  }
});

var _chrome = require("./chrome");