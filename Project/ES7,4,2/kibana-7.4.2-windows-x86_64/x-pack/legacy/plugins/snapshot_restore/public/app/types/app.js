"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AppCore", {
  enumerable: true,
  get: function get() {
    return _shim.AppCore;
  }
});
Object.defineProperty(exports, "AppPlugins", {
  enumerable: true,
  get: function get() {
    return _shim.AppPlugins;
  }
});

var _shim = require("../../shim");