"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PolicyStepReview = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _lib = require("../../../../../common/lib");

var _index = require("../../../index");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var PolicyStepReview = function PolicyStepReview(_ref) {
  var policy = _ref.policy,
      updateCurrentStep = _ref.updateCurrentStep;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  var FormattedMessage = i18n.FormattedMessage;
  var name = policy.name,
      snapshotName = policy.snapshotName,
      schedule = policy.schedule,
      repository = policy.repository,
      config = policy.config;

  var _ref2 = config || {
    indices: undefined,
    includeGlobalState: undefined,
    ignoreUnavailable: undefined,
    partial: undefined
  },
      indices = _ref2.indices,
      includeGlobalState = _ref2.includeGlobalState,
      ignoreUnavailable = _ref2.ignoreUnavailable,
      partial = _ref2.partial;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isShowingFullIndicesList = _useState2[0],
      setIsShowingFullIndicesList = _useState2[1];

  var displayIndices = indices ? typeof indices === 'string' ? indices.split(',') : indices : undefined;
  var hiddenIndicesCount = displayIndices && displayIndices.length > 10 ? displayIndices.length - 10 : 0;

  var renderSummaryTab = function renderSummaryTab() {
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.sectionLogisticsTitle",
      defaultMessage: "Logistics"
    }), ' ', _react.default.createElement(_eui.EuiToolTip, {
      content: _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.editStepTooltip",
        defaultMessage: "Edit"
      })
    }, _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return updateCurrentStep(1);
      }
    }, _react.default.createElement(_eui.EuiIcon, {
      type: "pencil"
    }))))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.nameLabel",
      defaultMessage: "Policy name"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, name))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.snapshotNameLabel",
      defaultMessage: "Snapshot name"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, snapshotName)))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.repositoryLabel",
      defaultMessage: "Repository"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, repository))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.scheduleLabel",
      defaultMessage: "Schedule"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, schedule)))), _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.sectionSettingsTitle",
      defaultMessage: "Snapshot settings"
    }), ' ', _react.default.createElement(_eui.EuiToolTip, {
      content: _react.default.createElement(FormattedMessage, {
        id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.editStepTooltip",
        defaultMessage: "Edit"
      })
    }, _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return updateCurrentStep(2);
      }
    }, _react.default.createElement(_eui.EuiIcon, {
      type: "pencil"
    }))))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.indicesLabel",
      defaultMessage: "Indices"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, displayIndices ? _react.default.createElement(_eui.EuiText, null, _react.default.createElement("ul", null, (isShowingFullIndicesList ? displayIndices : _toConsumableArray(displayIndices).splice(0, 10)).map(function (index) {
      return _react.default.createElement("li", {
        key: index
      }, _react.default.createElement(_eui.EuiTitle, {
        size: "xs"
      }, _react.default.createElement("span", null, index)));
    }), hiddenIndicesCount ? _react.default.createElement("li", {
      key: "hiddenIndicesCount"
    }, _react.default.createElement(_eui.EuiTitle, {
      size: "xs"
    }, isShowingFullIndicesList ? _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return setIsShowingFullIndicesList(false);
      }
    }, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.indicesCollapseAllLink",
      defaultMessage: "Hide {count, plural, one {# index} other {# indices}}",
      values: {
        count: hiddenIndicesCount
      }
    }), ' ', _react.default.createElement(_eui.EuiIcon, {
      type: "arrowUp"
    })) : _react.default.createElement(_eui.EuiLink, {
      onClick: function onClick() {
        return setIsShowingFullIndicesList(true);
      }
    }, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.indicesShowAllLink",
      defaultMessage: "Show {count} more {count, plural, one {index} other {indices}}",
      values: {
        count: hiddenIndicesCount
      }
    }), ' ', _react.default.createElement(_eui.EuiIcon, {
      type: "arrowDown"
    })))) : null)) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.allIndicesValue",
      defaultMessage: "All indices"
    })))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.ignoreUnavailableLabel",
      defaultMessage: "Ignore unavailable indices"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, ignoreUnavailable ? _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.ignoreUnavailableTrueLabel",
      defaultMessage: "Yes"
    }) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.ignoreUnavailableFalseLabel",
      defaultMessage: "No"
    }))))), _react.default.createElement(_eui.EuiSpacer, {
      size: "s"
    }), _react.default.createElement(_eui.EuiFlexGroup, null, _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.partialLabel",
      defaultMessage: "Allow partial shards"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, partial ? _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.partialTrueLabel",
      defaultMessage: "Yes"
    }) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.partialFalseLabel",
      defaultMessage: "No"
    })))), _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiDescriptionList, {
      textStyle: "reverse"
    }, _react.default.createElement(_eui.EuiDescriptionListTitle, null, _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.includeGlobalStateLabel",
      defaultMessage: "Include global state"
    })), _react.default.createElement(_eui.EuiDescriptionListDescription, null, includeGlobalState === false ? _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.includeGlobalStateFalseLabel",
      defaultMessage: "No"
    }) : _react.default.createElement(FormattedMessage, {
      id: "xpack.snapshotRestore.policyForm.stepReview.summaryTab.includeGlobalStateTrueLabel",
      defaultMessage: "Yes"
    }))))));
  };

  var renderRequestTab = function renderRequestTab() {
    var endpoint = "PUT _slm/policy/".concat(name);
    var json = JSON.stringify((0, _lib.serializePolicy)(policy), null, 2);
    return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
      size: "m"
    }), _react.default.createElement(_eui.EuiCodeBlock, {
      language: "json",
      isCopyable: true
    }, "".concat(endpoint, "\n").concat(json)));
  };

  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h3", null, _react.default.createElement(FormattedMessage, {
    id: "xpack.snapshotRestore.policyForm.stepReviewTitle",
    defaultMessage: "Review policy"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiTabbedContent, {
    tabs: [{
      id: 'summary',
      name: i18n.translate('xpack.snapshotRestore.policyForm.stepReview.summaryTabTitle', {
        defaultMessage: 'Summary'
      }),
      content: renderSummaryTab()
    }, {
      id: 'json',
      name: i18n.translate('xpack.snapshotRestore.policyForm.stepReview.requestTabTitle', {
        defaultMessage: 'Request'
      }),
      content: renderRequestTab()
    }]
  }));
};

exports.PolicyStepReview = PolicyStepReview;