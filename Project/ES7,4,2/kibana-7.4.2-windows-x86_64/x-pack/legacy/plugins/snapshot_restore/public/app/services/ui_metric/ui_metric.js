"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uiMetricService = void 0;

var _constants = require("../../constants");

var _public = require("../../../../../../../../src/legacy/core_plugins/ui_metric/public");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var UiMetricService = function UiMetricService() {
  var _this = this;

  _classCallCheck(this, UiMetricService);

  _defineProperty(this, "track", void 0);

  _defineProperty(this, "init", function (getReporter) {
    _this.track = getReporter(_constants.UIM_APP_NAME);
  });

  _defineProperty(this, "trackUiMetric", function (eventName) {
    if (!_this.track) throw Error('UiMetricService not initialized.');
    return _this.track(_public.METRIC_TYPE.COUNT, eventName);
  });
};

var uiMetricService = new UiMetricService();
exports.uiMetricService = uiMetricService;