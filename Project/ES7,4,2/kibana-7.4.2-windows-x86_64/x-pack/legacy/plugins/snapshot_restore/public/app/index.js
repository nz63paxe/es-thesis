"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CLIENT_BASE_PATH", {
  enumerable: true,
  get: function get() {
    return _constants2.BASE_PATH;
  }
});
exports.renderReact = exports.useAppDependencies = exports.setAppDependencies = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactDom = require("react-dom");

var _reactRouterDom = require("react-router-dom");

var _constants = require("../../common/constants");

var _app = require("./app");

var _http = require("./services/http");

var _authorization = require("./lib/authorization");

var _constants2 = require("./constants");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * App dependencies
 */
var DependenciesContext;

var setAppDependencies = function setAppDependencies(deps) {
  DependenciesContext = (0, _react.createContext)(deps);
  return DependenciesContext.Provider;
};

exports.setAppDependencies = setAppDependencies;

var useAppDependencies = function useAppDependencies() {
  if (!DependenciesContext) {
    throw new Error("The app dependencies Context hasn't been set.\n    Use the \"setAppDependencies()\" method when bootstrapping the app.");
  }

  return (0, _react.useContext)(DependenciesContext);
};

exports.useAppDependencies = useAppDependencies;

var getAppProviders = function getAppProviders(deps) {
  var I18nContext = deps.core.i18n.Context; // Create App dependencies context and get its provider

  var AppDependenciesProvider = setAppDependencies(deps);
  return function (_ref) {
    var children = _ref.children;
    return _react.default.createElement(_authorization.AuthorizationProvider, {
      privilegesEndpoint: _http.httpService.addBasePath("".concat(_constants.API_BASE_PATH, "privileges"))
    }, _react.default.createElement(I18nContext, null, _react.default.createElement(_reactRouterDom.HashRouter, null, _react.default.createElement(AppDependenciesProvider, {
      value: deps
    }, children))));
  };
};

var renderReact =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(elem, core, plugins) {
    var Providers;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            Providers = getAppProviders({
              core: core,
              plugins: plugins
            });
            (0, _reactDom.render)(_react.default.createElement(Providers, null, _react.default.createElement(_app.App, null)), elem);

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function renderReact(_x, _x2, _x3) {
    return _ref2.apply(this, arguments);
  };
}();

exports.renderReact = renderReact;