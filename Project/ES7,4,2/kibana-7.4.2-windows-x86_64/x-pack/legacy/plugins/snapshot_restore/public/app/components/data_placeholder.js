"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataPlaceholder = void 0;

var _index = require("../index");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var DataPlaceholder = function DataPlaceholder(_ref) {
  var data = _ref.data,
      children = _ref.children;

  var _useAppDependencies = (0, _index.useAppDependencies)(),
      i18n = _useAppDependencies.core.i18n;

  if (data != null) {
    return children;
  }

  return i18n.translate('xpack.snapshotRestore.dataPlaceholderLabel', {
    defaultMessage: '-'
  });
};

exports.DataPlaceholder = DataPlaceholder;