"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createShim = createShim;

var _lodash = require("lodash");

var _i18n = require("@kbn/i18n");

var _create_router = require("../../server/lib/create_router");

var _register_license_checker = require("../../server/lib/register_license_checker");

var _elasticsearch_slm = require("./server/client/elasticsearch_slm");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createShim(server, pluginId) {
  return {
    core: {
      http: {
        createRouter: basePath => (0, _create_router.createRouter)(server, pluginId, basePath, {
          plugins: [_elasticsearch_slm.elasticsearchJsPlugin]
        })
      },
      i18n: _i18n.i18n
    },
    plugins: {
      license: {
        registerLicenseChecker: _register_license_checker.registerLicenseChecker
      },
      cloud: {
        config: {
          isCloudEnabled: (0, _lodash.get)(server.plugins, 'cloud.config.isCloudEnabled', false)
        }
      },
      settings: {
        config: {
          isSlmEnabled: server.config() ? server.config().get('xpack.snapshot_restore.slm_ui.enabled') : true
        }
      },
      xpack_main: server.plugins.xpack_main,
      elasticsearch: server.plugins.elasticsearch
    }
  };
}