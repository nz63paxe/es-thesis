"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.snapshotRestore = snapshotRestore;

var _path = require("path");

var _constants = require("./common/constants");

var _plugin = require("./plugin");

var _shim = require("./shim");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function snapshotRestore(kibana) {
  return new kibana.Plugin({
    id: _constants.PLUGIN.ID,
    configPrefix: 'xpack.snapshot_restore',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    require: ['kibana', 'elasticsearch', 'xpack_main'],
    uiExports: {
      styleSheetPaths: (0, _path.resolve)(__dirname, 'public/app/index.scss'),
      managementSections: ['plugins/snapshot_restore'],

      injectDefaultVars(server) {
        const config = server.config();
        return {
          slmUiEnabled: config.get('xpack.snapshot_restore.slm_ui.enabled')
        };
      }

    },

    config(Joi) {
      return Joi.object({
        slm_ui: Joi.object({
          enabled: Joi.boolean().default(true)
        }).default(),
        enabled: Joi.boolean().default(true)
      }).default();
    },

    init(server) {
      const {
        core,
        plugins
      } = (0, _shim.createShim)(server, _constants.PLUGIN.ID);
      const {
        i18n
      } = core;
      const snapshotRestorePlugin = new _plugin.Plugin(); // Start plugin

      snapshotRestorePlugin.start(core, plugins); // Register license checker

      plugins.license.registerLicenseChecker(server, _constants.PLUGIN.ID, _constants.PLUGIN.getI18nName(i18n), _constants.PLUGIN.MINIMUM_LICENSE_REQUIRED);
    }

  });
}