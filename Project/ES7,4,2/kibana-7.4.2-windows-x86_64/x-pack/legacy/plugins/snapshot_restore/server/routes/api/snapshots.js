"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerSnapshotsRoutes = registerSnapshotsRoutes;
exports.deleteHandler = exports.getOneHandler = exports.getAllHandler = void 0;

var _error_wrappers = require("../../../../../server/lib/create_router/error_wrappers");

var _lib = require("../../../common/lib");

var _lib2 = require("../../lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let callWithInternalUser;

function registerSnapshotsRoutes(router, plugins) {
  callWithInternalUser = plugins.elasticsearch.getCluster('data').callWithInternalUser;
  router.get('snapshots', getAllHandler);
  router.get('snapshots/{repository}/{snapshot}', getOneHandler);
  router.delete('snapshots/{ids}', deleteHandler);
}

const getAllHandler = async (req, callWithRequest) => {
  const managedRepository = await (0, _lib2.getManagedRepositoryName)(callWithInternalUser);
  let policies = []; // Attempt to retrieve policies
  // This could fail if user doesn't have access to read SLM policies

  try {
    const policiesByName = await callWithRequest('slm.policies');
    policies = Object.keys(policiesByName);
  } catch (e) {// Silently swallow error as policy names aren't required in UI
  }

  const repositoriesByName = await callWithRequest('snapshot.getRepository', {
    repository: '_all'
  });
  const repositoryNames = Object.keys(repositoriesByName);

  if (repositoryNames.length === 0) {
    return {
      snapshots: [],
      errors: [],
      repositories: [],
      policies
    };
  }

  const snapshots = [];
  const errors = {};
  const repositories = [];

  const fetchSnapshotsForRepository = async repository => {
    try {
      // If any of these repositories 504 they will cost the request significant time.
      const {
        snapshots: fetchedSnapshots
      } = await callWithRequest('snapshot.get', {
        repository,
        snapshot: '_all',
        ignore_unavailable: true // Allow request to succeed even if some snapshots are unavailable.

      }); // Decorate each snapshot with the repository with which it's associated.

      fetchedSnapshots.forEach(snapshot => {
        snapshots.push((0, _lib.deserializeSnapshotDetails)(repository, snapshot, managedRepository));
      });
      repositories.push(repository);
    } catch (error) {
      // These errors are commonly due to a misconfiguration in the repository or plugin errors,
      // which can result in a variety of 400, 404, and 500 errors.
      errors[repository] = error;
    }
  };

  await Promise.all(repositoryNames.map(fetchSnapshotsForRepository));
  return {
    snapshots,
    policies,
    repositories,
    errors
  };
};

exports.getAllHandler = getAllHandler;

const getOneHandler = async (req, callWithRequest) => {
  const {
    repository,
    snapshot
  } = req.params;
  const managedRepository = await (0, _lib2.getManagedRepositoryName)(callWithInternalUser);
  const {
    snapshots
  } = await callWithRequest('snapshot.get', {
    repository,
    snapshot
  }); // If the snapshot is missing the endpoint will return a 404, so we'll never get to this point.

  return (0, _lib.deserializeSnapshotDetails)(repository, snapshots[0], managedRepository);
};

exports.getOneHandler = getOneHandler;

const deleteHandler = async (req, callWithRequest) => {
  const {
    ids
  } = req.params;
  const snapshotIds = ids.split(',');
  const response = {
    itemsDeleted: [],
    errors: []
  }; // We intentially perform deletion requests sequentially (blocking) instead of in parallel (non-blocking)
  // because there can only be one snapshot deletion task performed at a time (ES restriction).

  for (let i = 0; i < snapshotIds.length; i++) {
    // IDs come in the format of `repository-name/snapshot-name`
    // Extract the two parts by splitting at last occurrence of `/` in case
    // repository name contains '/` (from older versions)
    const id = snapshotIds[i];
    const indexOfDivider = id.lastIndexOf('/');
    const snapshot = id.substring(indexOfDivider + 1);
    const repository = id.substring(0, indexOfDivider);
    await callWithRequest('snapshot.delete', {
      snapshot,
      repository
    }).then(() => response.itemsDeleted.push({
      snapshot,
      repository
    })).catch(e => response.errors.push({
      id: {
        snapshot,
        repository
      },
      error: (0, _error_wrappers.wrapEsError)(e)
    }));
  }

  return response;
};

exports.deleteHandler = deleteHandler;