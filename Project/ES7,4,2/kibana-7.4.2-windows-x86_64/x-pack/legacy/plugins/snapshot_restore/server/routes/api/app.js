"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerAppRoutes = registerAppRoutes;
exports.getXpackMainPlugin = getXpackMainPlugin;
exports.getPrivilegesHandler = void 0;

var _error_wrappers = require("../../../../../server/lib/create_router/error_wrappers");

var _constants = require("../../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let xpackMainPlugin;

function registerAppRoutes(router, plugins) {
  xpackMainPlugin = plugins.xpack_main;
  router.get('privileges', getPrivilegesHandler);
}

function getXpackMainPlugin() {
  return xpackMainPlugin;
}

const extractMissingPrivileges = (privilegesObject = {}) => Object.keys(privilegesObject).reduce((privileges, privilegeName) => {
  if (!privilegesObject[privilegeName]) {
    privileges.push(privilegeName);
  }

  return privileges;
}, []);

const getPrivilegesHandler = async (req, callWithRequest) => {
  const xpackInfo = getXpackMainPlugin() && getXpackMainPlugin().info;

  if (!xpackInfo) {
    // xpackInfo is updated via poll, so it may not be available until polling has begun.
    // In this rare situation, tell the client the service is temporarily unavailable.
    throw (0, _error_wrappers.wrapCustomError)(new Error('Security info unavailable'), 503);
  }

  const privilegesResult = {
    hasAllPrivileges: true,
    missingPrivileges: {
      cluster: [],
      index: []
    }
  };
  const securityInfo = xpackInfo && xpackInfo.isAvailable() && xpackInfo.feature('security');

  if (!securityInfo || !securityInfo.isAvailable() || !securityInfo.isEnabled()) {
    // If security isn't enabled, let the user use app.
    return privilegesResult;
  } // Get cluster priviliges


  const {
    has_all_requested: hasAllPrivileges,
    cluster
  } = await callWithRequest('transport.request', {
    path: '/_security/user/_has_privileges',
    method: 'POST',
    body: {
      cluster: [..._constants.APP_REQUIRED_CLUSTER_PRIVILEGES, ..._constants.APP_SLM_CLUSTER_PRIVILEGES]
    }
  }); // Find missing cluster privileges and set overall app privileges

  privilegesResult.missingPrivileges.cluster = extractMissingPrivileges(cluster);
  privilegesResult.hasAllPrivileges = hasAllPrivileges; // Get all index privileges the user has

  const {
    indices
  } = await callWithRequest('transport.request', {
    path: '/_security/user/_privileges',
    method: 'GET'
  }); // Check if they have all the required index privileges for at least one index

  const oneIndexWithAllPrivileges = indices.find(({
    privileges
  }) => {
    if (privileges.includes('all')) {
      return true;
    }

    const indexHasAllPrivileges = _constants.APP_RESTORE_INDEX_PRIVILEGES.every(privilege => privileges.includes(privilege));

    return indexHasAllPrivileges;
  }); // If they don't, return list of required index privileges

  if (!oneIndexWithAllPrivileges) {
    privilegesResult.missingPrivileges.index = [..._constants.APP_RESTORE_INDEX_PRIVILEGES];
  }

  return privilegesResult;
};

exports.getPrivilegesHandler = getPrivilegesHandler;