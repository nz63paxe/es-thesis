"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerRestoreRoutes = registerRestoreRoutes;
exports.getAllHandler = exports.createHandler = void 0;

var _lib = require("../../../common/lib");

var _lib2 = require("../../lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerRestoreRoutes(router) {
  router.post('restore/{repository}/{snapshot}', createHandler);
  router.get('restores', getAllHandler);
}

const createHandler = async (req, callWithRequest) => {
  const {
    repository,
    snapshot
  } = req.params;
  const restoreSettings = req.payload;
  return await callWithRequest('snapshot.restore', {
    repository,
    snapshot,
    body: (0, _lib.serializeRestoreSettings)(restoreSettings)
  });
};

exports.createHandler = createHandler;

const getAllHandler = async (req, callWithRequest) => {
  const snapshotRestores = [];
  const recoveryByIndexName = await callWithRequest('indices.recovery', {
    human: true
  }); // Filter to snapshot-recovered shards only

  Object.keys(recoveryByIndexName).forEach(index => {
    const recovery = recoveryByIndexName[index];
    let latestActivityTimeInMillis = 0;
    let latestEndTimeInMillis = null;
    const snapshotShards = (recovery.shards || []).filter(shard => shard.type === 'SNAPSHOT').sort((a, b) => a.id - b.id).map(shard => {
      const deserializedShard = (0, _lib2.deserializeRestoreShard)(shard);
      const {
        startTimeInMillis,
        stopTimeInMillis
      } = deserializedShard; // Set overall latest activity time

      latestActivityTimeInMillis = Math.max(startTimeInMillis || 0, stopTimeInMillis || 0, latestActivityTimeInMillis); // Set overall end time

      if (stopTimeInMillis === undefined) {
        latestEndTimeInMillis = null;
      } else if (latestEndTimeInMillis === null || stopTimeInMillis > latestEndTimeInMillis) {
        latestEndTimeInMillis = stopTimeInMillis;
      }

      return deserializedShard;
    });

    if (snapshotShards.length > 0) {
      snapshotRestores.push({
        index,
        latestActivityTimeInMillis,
        shards: snapshotShards,
        isComplete: latestEndTimeInMillis !== null
      });
    }
  }); // Sort by latest activity

  snapshotRestores.sort((a, b) => b.latestActivityTimeInMillis - a.latestActivityTimeInMillis);
  return snapshotRestores;
};

exports.getAllHandler = getAllHandler;