"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerRoutes = void 0;

var _app = require("./app");

var _repositories = require("./repositories");

var _snapshots = require("./snapshots");

var _restore = require("./restore");

var _policy = require("./policy");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const registerRoutes = (router, plugins) => {
  const isSlmEnabled = plugins.settings.config.isSlmEnabled;
  (0, _app.registerAppRoutes)(router, plugins);
  (0, _repositories.registerRepositoriesRoutes)(router, plugins);
  (0, _snapshots.registerSnapshotsRoutes)(router, plugins);
  (0, _restore.registerRestoreRoutes)(router);

  if (isSlmEnabled) {
    (0, _policy.registerPolicyRoutes)(router);
  }
};

exports.registerRoutes = registerRoutes;