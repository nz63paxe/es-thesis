"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerPolicyRoutes = registerPolicyRoutes;
exports.getIndicesHandler = exports.updateHandler = exports.createHandler = exports.deleteHandler = exports.executeHandler = exports.getOneHandler = exports.getAllHandler = void 0;

var _error_wrappers = require("../../../../../server/lib/create_router/error_wrappers");

var _lib = require("../../../common/lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function registerPolicyRoutes(router) {
  router.get('policies', getAllHandler);
  router.get('policy/{name}', getOneHandler);
  router.post('policy/{name}/run', executeHandler);
  router.delete('policies/{names}', deleteHandler);
  router.put('policies', createHandler);
  router.put('policies/{name}', updateHandler);
  router.get('policies/indices', getIndicesHandler);
}

const getAllHandler = async (req, callWithRequest) => {
  // Get policies
  const policiesByName = await callWithRequest('slm.policies', {
    human: true
  }); // Deserialize policies

  return {
    policies: Object.entries(policiesByName).map(([name, policy]) => (0, _lib.deserializePolicy)(name, policy))
  };
};

exports.getAllHandler = getAllHandler;

const getOneHandler = async (req, callWithRequest) => {
  // Get policy
  const {
    name
  } = req.params;
  const policiesByName = await callWithRequest('slm.policy', {
    name,
    human: true
  });

  if (!policiesByName[name]) {
    // If policy doesn't exist, ES will return 200 with an empty object, so manually throw 404 here
    throw (0, _error_wrappers.wrapCustomError)(new Error('Policy not found'), 404);
  } // Deserialize policy


  return {
    policy: (0, _lib.deserializePolicy)(name, policiesByName[name])
  };
};

exports.getOneHandler = getOneHandler;

const executeHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const {
    snapshot_name: snapshotName
  } = await callWithRequest('slm.executePolicy', {
    name
  });
  return {
    snapshotName
  };
};

exports.executeHandler = executeHandler;

const deleteHandler = async (req, callWithRequest) => {
  const {
    names
  } = req.params;
  const policyNames = names.split(',');
  const response = {
    itemsDeleted: [],
    errors: []
  };
  await Promise.all(policyNames.map(name => {
    return callWithRequest('slm.deletePolicy', {
      name
    }).then(() => response.itemsDeleted.push(name)).catch(e => response.errors.push({
      name,
      error: (0, _error_wrappers.wrapEsError)(e)
    }));
  }));
  return response;
};

exports.deleteHandler = deleteHandler;

const createHandler = async (req, callWithRequest) => {
  const policy = req.payload;
  const {
    name
  } = policy;
  const conflictError = (0, _error_wrappers.wrapCustomError)(new Error('There is already a policy with that name.'), 409); // Check that policy with the same name doesn't already exist

  try {
    const policyByName = await callWithRequest('slm.policy', {
      name
    });

    if (policyByName[name]) {
      throw conflictError;
    }
  } catch (e) {
    // Rethrow conflict error but silently swallow all others
    if (e === conflictError) {
      throw e;
    }
  } // Otherwise create new policy


  return await callWithRequest('slm.updatePolicy', {
    name,
    body: (0, _lib.serializePolicy)(policy)
  });
};

exports.createHandler = createHandler;

const updateHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const policy = req.payload; // Check that policy with the given name exists
  // If it doesn't exist, 404 will be thrown by ES and will be returned

  await callWithRequest('slm.policy', {
    name
  }); // Otherwise update policy

  return await callWithRequest('slm.updatePolicy', {
    name,
    body: (0, _lib.serializePolicy)(policy)
  });
};

exports.updateHandler = updateHandler;

const getIndicesHandler = async (req, callWithRequest) => {
  // Get indices
  const indices = await callWithRequest('cat.indices', {
    format: 'json',
    h: 'index'
  });
  return {
    indices: indices.map(({
      index
    }) => index).sort()
  };
};

exports.getIndicesHandler = getIndicesHandler;