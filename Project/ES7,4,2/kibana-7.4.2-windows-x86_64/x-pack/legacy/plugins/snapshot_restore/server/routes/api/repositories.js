"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerRepositoriesRoutes = registerRepositoriesRoutes;
exports.deleteHandler = exports.updateHandler = exports.createHandler = exports.getTypesHandler = exports.getVerificationHandler = exports.getOneHandler = exports.getAllHandler = void 0;

var _error_wrappers = require("../../../../../server/lib/create_router/error_wrappers");

var _constants = require("../../../common/constants");

var _lib = require("../../lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let isCloudEnabled = false;
let callWithInternalUser;

function registerRepositoriesRoutes(router, plugins) {
  isCloudEnabled = plugins.cloud.config.isCloudEnabled;
  callWithInternalUser = plugins.elasticsearch.getCluster('data').callWithInternalUser;
  router.get('repository_types', getTypesHandler);
  router.get('repositories', getAllHandler);
  router.get('repositories/{name}', getOneHandler);
  router.get('repositories/{name}/verify', getVerificationHandler);
  router.put('repositories', createHandler);
  router.put('repositories/{name}', updateHandler);
  router.delete('repositories/{names}', deleteHandler);
}

const getAllHandler = async (req, callWithRequest) => {
  const managedRepository = await (0, _lib.getManagedRepositoryName)(callWithInternalUser);
  const repositoriesByName = await callWithRequest('snapshot.getRepository', {
    repository: '_all'
  });
  const repositoryNames = Object.keys(repositoriesByName);
  const repositories = repositoryNames.map(name => {
    const {
      type = '',
      settings = {}
    } = repositoriesByName[name];
    return {
      name,
      type,
      settings: (0, _lib.deserializeRepositorySettings)(settings)
    };
  });
  return {
    repositories,
    managedRepository
  };
};

exports.getAllHandler = getAllHandler;

const getOneHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const managedRepository = await (0, _lib.getManagedRepositoryName)(callWithInternalUser);
  const repositoryByName = await callWithRequest('snapshot.getRepository', {
    repository: name
  });
  const {
    snapshots
  } = await callWithRequest('snapshot.get', {
    repository: name,
    snapshot: '_all'
  }).catch(e => ({
    snapshots: null
  }));

  if (repositoryByName[name]) {
    const {
      type = '',
      settings = {}
    } = repositoryByName[name];
    return {
      repository: {
        name,
        type,
        settings: (0, _lib.deserializeRepositorySettings)(settings)
      },
      isManagedRepository: managedRepository === name,
      snapshots: {
        count: snapshots ? snapshots.length : null
      }
    };
  } else {
    return {
      repository: {},
      snapshots: {}
    };
  }
};

exports.getOneHandler = getOneHandler;

const getVerificationHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const verificationResults = await callWithRequest('snapshot.verifyRepository', {
    repository: name
  }).catch(e => ({
    valid: false,
    error: e.response ? JSON.parse(e.response) : e
  }));
  return {
    verification: verificationResults.error ? verificationResults : {
      valid: true,
      response: verificationResults
    }
  };
};

exports.getVerificationHandler = getVerificationHandler;

const getTypesHandler = async () => {
  // In ECE/ESS, do not enable the default types
  const types = isCloudEnabled ? [] : [..._constants.DEFAULT_REPOSITORY_TYPES]; // Call with internal user so that the requesting user does not need `monitoring` cluster
  // privilege just to see list of available repository types

  const plugins = await callWithInternalUser('cat.plugins', {
    format: 'json'
  }); // Filter list of plugins to repository-related ones

  if (plugins && plugins.length) {
    const pluginNames = [...new Set(plugins.map(plugin => plugin.component))];
    pluginNames.forEach(pluginName => {
      if (_constants.REPOSITORY_PLUGINS_MAP[pluginName]) {
        types.push(_constants.REPOSITORY_PLUGINS_MAP[pluginName]);
      }
    });
  }

  return types;
};

exports.getTypesHandler = getTypesHandler;

const createHandler = async (req, callWithRequest) => {
  const {
    name = '',
    type = '',
    settings = {}
  } = req.payload;
  const conflictError = (0, _error_wrappers.wrapCustomError)(new Error('There is already a repository with that name.'), 409); // Check that repository with the same name doesn't already exist

  try {
    const repositoryByName = await callWithRequest('snapshot.getRepository', {
      repository: name
    });

    if (repositoryByName[name]) {
      throw conflictError;
    }
  } catch (e) {
    // Rethrow conflict error but silently swallow all others
    if (e === conflictError) {
      throw e;
    }
  } // Otherwise create new repository


  return await callWithRequest('snapshot.createRepository', {
    repository: name,
    body: {
      type,
      settings: (0, _lib.serializeRepositorySettings)(settings)
    },
    verify: false
  });
};

exports.createHandler = createHandler;

const updateHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const {
    type = '',
    settings = {}
  } = req.payload; // Check that repository with the given name exists
  // If it doesn't exist, 404 will be thrown by ES and will be returned

  await callWithRequest('snapshot.getRepository', {
    repository: name
  }); // Otherwise update repository

  return await callWithRequest('snapshot.createRepository', {
    repository: name,
    body: {
      type,
      settings: (0, _lib.serializeRepositorySettings)(settings)
    },
    verify: false
  });
};

exports.updateHandler = updateHandler;

const deleteHandler = async (req, callWithRequest) => {
  const {
    names
  } = req.params;
  const repositoryNames = names.split(',');
  const response = {
    itemsDeleted: [],
    errors: []
  };
  await Promise.all(repositoryNames.map(name => {
    return callWithRequest('snapshot.deleteRepository', {
      repository: name
    }).then(() => response.itemsDeleted.push(name)).catch(e => response.errors.push({
      name,
      error: (0, _error_wrappers.wrapEsError)(e)
    }));
  }));
  return response;
};

exports.deleteHandler = deleteHandler;