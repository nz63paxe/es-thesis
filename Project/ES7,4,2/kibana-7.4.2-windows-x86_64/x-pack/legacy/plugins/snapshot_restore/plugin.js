"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _constants = require("./common/constants");

var _register_routes = require("./server/routes/api/register_routes");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class Plugin {
  start(core, plugins) {
    const router = core.http.createRouter(_constants.API_BASE_PATH); // Register routes

    (0, _register_routes.registerRoutes)(router, plugins);
  }

}

exports.Plugin = Plugin;