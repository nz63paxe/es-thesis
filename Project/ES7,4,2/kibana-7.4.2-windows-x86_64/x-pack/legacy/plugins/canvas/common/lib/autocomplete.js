"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFnArgDefAtPosition = getFnArgDefAtPosition;
exports.getAutocompleteSuggestions = getAutocompleteSuggestions;

var _lodash = require("lodash");

var _common = require("@kbn/interpreter/common");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore Untyped Library
const MARKER = 'CANVAS_SUGGESTION_MARKER';

// Typeguard for checking if ExpressionArg is a new expression
function isExpression(maybeExpression) {
  return typeof maybeExpression.node === 'object';
} // Overloads to change return type based on specs


function getByAlias(specs, name) {
  return (0, _common.getByAlias)(specs, name);
}
/**
 * Generates the AST with the given expression and then returns the function and argument definitions
 * at the given position in the expression, if there are any.
 */


function getFnArgDefAtPosition(specs, expression, position) {
  try {
    const ast = (0, _common.parse)(expression, {
      addMeta: true
    });
    const {
      ast: newAst,
      fnIndex,
      argName,
      argStart,
      argEnd
    } = getFnArgAtPosition(ast, position);
    const fn = newAst.node.chain[fnIndex].node;
    const fnDef = getByAlias(specs, fn.function);

    if (fnDef && argName) {
      const argDef = getByAlias(fnDef.args, argName);
      return {
        fnDef,
        argDef,
        argStart,
        argEnd
      };
    }

    return {
      fnDef
    };
  } catch (e) {// Fail silently
  }

  return {};
}
/**
 * Gets a list of suggestions for the given expression at the given position. It does this by
 * inserting a marker at the given position, then parsing the resulting expression. This way we can
 * see what the marker would turn into, which tells us what sorts of things to suggest. For
 * example, if the marker turns into a function name, then we suggest functions. If it turns into
 * an unnamed argument, we suggest argument names. If it turns into a value, we suggest values.
 */


function getAutocompleteSuggestions(specs, expression, position) {
  const text = expression.substr(0, position) + MARKER + expression.substr(position);

  try {
    const ast = (0, _common.parse)(text, {
      addMeta: true
    });
    const {
      ast: newAst,
      fnIndex,
      argName,
      argIndex
    } = getFnArgAtPosition(ast, position);
    const fn = newAst.node.chain[fnIndex].node;

    if (fn.function.includes(MARKER)) {
      return getFnNameSuggestions(specs, newAst, fnIndex);
    }

    if (argName === '_' && argIndex !== undefined) {
      return getArgNameSuggestions(specs, newAst, fnIndex, argName, argIndex);
    }

    if (argName && argIndex !== undefined) {
      return getArgValueSuggestions(specs, newAst, fnIndex, argName, argIndex);
    }
  } catch (e) {// Fail silently
  }

  return [];
}
/**
    Each entry of the node.chain has it's overall start and end position.  For instance,
    given the expression "link arg='something' | render" the link functions start position is 0 and end
    position is 21.

    This function is given the full ast and the current cursor position in the expression string.

    It returns which function the cursor is in, as well as which argument for that function the cursor is in
    if any.
*/


function getFnArgAtPosition(ast, position) {
  const fnIndex = ast.node.chain.findIndex(fn => fn.start <= position && position <= fn.end);
  const fn = ast.node.chain[fnIndex];

  for (const [argName, argValues] of Object.entries(fn.node.arguments)) {
    for (let argIndex = 0; argIndex < argValues.length; argIndex++) {
      const value = argValues[argIndex];
      let argStart = value.start;
      let argEnd = value.end;

      if (argName !== '_') {
        // If an arg name is specified, expand our start position to include
        // the arg name plus the `=` character
        argStart = argStart - (argName.length + 1); // If the arg value is an expression, expand our start and end position
        // to include the opening and closing braces

        if (value.node !== null && isExpression(value)) {
          argStart--;
          argEnd++;
        }
      }

      if (argStart <= position && position <= argEnd) {
        // If the current position is on an expression and NOT on the expression's
        // argument name (`font=` for example), recurse within the expression
        if (value.node !== null && isExpression(value) && (argName === '_' || !(argStart <= position && position <= argStart + argName.length + 1))) {
          return getFnArgAtPosition(value, position);
        }

        return {
          ast,
          fnIndex,
          argName,
          argIndex,
          argStart,
          argEnd
        };
      }
    }
  }

  return {
    ast,
    fnIndex
  };
}

function getFnNameSuggestions(specs, ast, fnIndex) {
  // Filter the list of functions by the text at the marker
  const {
    start,
    end,
    node: fn
  } = ast.node.chain[fnIndex];
  const query = fn.function.replace(MARKER, '');
  const matchingFnDefs = specs.filter(({
    name
  }) => textMatches(name, query)); // Sort by whether or not the function expects the previous function's return type, then by
  // whether or not the function name starts with the text at the marker, then alphabetically

  const prevFn = ast.node.chain[fnIndex - 1];
  const prevFnDef = prevFn && getByAlias(specs, prevFn.node.function);
  const prevFnType = prevFnDef && prevFnDef.type;
  const comparator = combinedComparator(prevFnTypeComparator(prevFnType), invokeWithProp(startsWithComparator(query), 'name'), invokeWithProp(alphanumericalComparator, 'name'));
  const fnDefs = matchingFnDefs.sort(comparator);
  return fnDefs.map(fnDef => {
    return {
      type: 'function',
      text: fnDef.name + ' ',
      start,
      end: end - MARKER.length,
      fnDef
    };
  });
}

function getArgNameSuggestions(specs, ast, fnIndex, argName, argIndex) {
  // Get the list of args from the function definition
  const fn = ast.node.chain[fnIndex].node;
  const fnDef = getByAlias(specs, fn.function);

  if (!fnDef) {
    return [];
  } // We use the exact text instead of the value because it is always a string and might be quoted


  const {
    text,
    start,
    end
  } = fn.arguments[argName][argIndex]; // Filter the list of args by the text at the marker

  const query = text.replace(MARKER, '');
  const matchingArgDefs = Object.entries(fnDef.args).filter(([name]) => textMatches(name, query)); // Filter the list of args by those which aren't already present (unless they allow multi)

  const argEntries = Object.entries(fn.arguments).map(([name, values]) => {
    return [name, values.filter(value => !value.text.includes(MARKER))];
  });
  const unusedArgDefs = matchingArgDefs.filter(([matchingArgName, matchingArgDef]) => {
    if (matchingArgDef.multi) {
      return true;
    }

    return !argEntries.some(([name, values]) => {
      return values.length > 0 && (name === matchingArgName || (matchingArgDef.aliases || []).includes(name));
    });
  }); // Sort by whether or not the arg is also the unnamed, then by whether or not the arg name starts
  // with the text at the marker, then alphabetically

  const comparator = combinedComparator(unnamedArgComparator, invokeWithProp(startsWithComparator(query), 'name'), invokeWithProp(alphanumericalComparator, 'name'));
  const argDefs = unusedArgDefs.map(([name, arg]) => ({
    name,
    ...arg
  })).sort(comparator);
  return argDefs.map(argDef => {
    return {
      type: 'argument',
      text: argDef.name + '=',
      start,
      end: end - MARKER.length,
      argDef
    };
  });
}

function getArgValueSuggestions(specs, ast, fnIndex, argName, argIndex) {
  // Get the list of values from the argument definition
  const fn = ast.node.chain[fnIndex].node;
  const fnDef = getByAlias(specs, fn.function);

  if (!fnDef) {
    return [];
  }

  const argDef = getByAlias(fnDef.args, argName);

  if (!argDef) {
    return [];
  } // Get suggestions from the argument definition, including the default


  const {
    start,
    end,
    node
  } = fn.arguments[argName][argIndex];

  if (typeof node !== 'string') {
    return [];
  }

  const query = node.replace(MARKER, '');
  const argOptions = argDef.options ? argDef.options : [];
  let suggestions = [...argOptions];

  if (argDef.default !== undefined) {
    suggestions.push(argDef.default);
  }

  suggestions = (0, _lodash.uniq)(suggestions); // Filter the list of suggestions by the text at the marker

  const filtered = suggestions.filter(option => textMatches(String(option), query)); // Sort by whether or not the value starts with the text at the marker, then alphabetically

  const comparator = combinedComparator(startsWithComparator(query), alphanumericalComparator);
  const sorted = filtered.sort(comparator);
  return sorted.map(value => {
    const text = maybeQuote(value) + ' ';
    return {
      start,
      end: end - MARKER.length,
      type: 'value',
      text
    };
  });
}

function textMatches(text, query) {
  return text.toLowerCase().includes(query.toLowerCase().trim());
}

function maybeQuote(value) {
  if (typeof value === 'string') {
    if (value.match(/^\{.*\}$/)) {
      return value;
    }

    return `"${value.replace(/"/g, '\\"')}"`;
  }

  return value;
}

function prevFnTypeComparator(prevFnType) {
  return (a, b) => {
    return (b.context && b.context.types && b.context.types.includes(prevFnType) ? 1 : 0) - (a.context && a.context.types && a.context.types.includes(prevFnType) ? 1 : 0);
  };
}

function unnamedArgComparator(a, b) {
  return (b.aliases && b.aliases.includes('_') ? 1 : 0) - (a.aliases && a.aliases.includes('_') ? 1 : 0);
}

function alphanumericalComparator(a, b) {
  if (a < b) {
    return -1;
  }

  if (a > b) {
    return 1;
  }

  return 0;
}

function startsWithComparator(query) {
  return (a, b) => (String(b).startsWith(query) ? 1 : 0) - (String(a).startsWith(query) ? 1 : 0);
}

function combinedComparator(...comparators) {
  return (a, b) => comparators.reduce((acc, comparator) => {
    if (acc !== 0) {
      return acc;
    }

    return comparator(a, b);
  }, 0);
}

function invokeWithProp(fn, prop) {
  return (...args) => {
    return fn(...args.map(arg => arg[prop]));
  };
}