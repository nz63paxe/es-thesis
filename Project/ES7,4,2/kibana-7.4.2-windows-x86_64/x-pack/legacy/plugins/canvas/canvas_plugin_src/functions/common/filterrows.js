"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.filterrows = filterrows;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function filterrows() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().filterrows;
  return {
    name: 'filterrows',
    aliases: [],
    type: 'datatable',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      fn: {
        resolve: false,
        aliases: ['_', 'exp', 'expression', 'function'],
        types: ['boolean'],
        required: true,
        help: argHelp.fn
      }
    },

    fn(context, {
      fn
    }) {
      const checks = context.rows.map(row => fn({ ...context,
        rows: [row]
      }));
      return Promise.all(checks).then(results => context.rows.filter((row, i) => results[i])).then(rows => ({ ...context,
        rows
      }));
    }

  };
}