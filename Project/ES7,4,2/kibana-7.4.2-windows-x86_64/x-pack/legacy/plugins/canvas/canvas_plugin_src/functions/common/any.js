"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.any = any;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function any() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().any;
  return {
    name: 'any',
    type: 'boolean',
    context: {
      types: ['null']
    },
    help,
    args: {
      condition: {
        aliases: ['_'],
        types: ['boolean'],
        required: true,
        multi: true,
        help: argHelp.condition
      }
    },
    fn: (_context, args) => {
      const conditions = args.condition || [];
      return conditions.some(Boolean);
    }
  };
}