"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replace = replace;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function replace() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().replace;
  return {
    name: 'replace',
    type: 'string',
    help,
    context: {
      types: ['string']
    },
    args: {
      pattern: {
        aliases: ['_', 'regex'],
        types: ['string'],
        help: argHelp.pattern
      },
      flags: {
        aliases: ['modifiers'],
        types: ['string'],
        help: argHelp.flags,
        default: 'g'
      },
      replacement: {
        types: ['string'],
        help: argHelp.replacement,
        default: '""'
      }
    },
    fn: (context, args) => context.replace(new RegExp(args.pattern, args.flags), args.replacement)
  };
}