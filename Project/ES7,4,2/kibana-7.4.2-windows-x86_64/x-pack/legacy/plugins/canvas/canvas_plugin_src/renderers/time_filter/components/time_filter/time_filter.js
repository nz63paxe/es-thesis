"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimeFilter = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = require("lodash");

var _common = require("@kbn/interpreter/common");

var _time_picker = require("../time_picker");

var _time_picker_popover = require("../time_picker_popover");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getFilterMeta(filter) {
  const ast = (0, _common.fromExpression)(filter);
  const column = (0, _lodash.get)(ast, 'chain[0].arguments.column[0]');
  const from = (0, _lodash.get)(ast, 'chain[0].arguments.from[0]');
  const to = (0, _lodash.get)(ast, 'chain[0].arguments.to[0]');
  return {
    column,
    from,
    to
  };
}

const TimeFilter = ({
  filter,
  commit,
  compact
}) => {
  const setFilter = column => (from, to) => {
    commit(`timefilter from="${from}" to=${to} column=${column}`);
  };

  const {
    column,
    from,
    to
  } = getFilterMeta(filter);

  if (compact) {
    return _react.default.createElement(_time_picker_popover.TimePickerPopover, {
      from: from,
      to: to,
      onSelect: setFilter(column)
    });
  } else {
    return _react.default.createElement(_time_picker.TimePicker, {
      from: from,
      to: to,
      onSelect: setFilter(column)
    });
  }
};

exports.TimeFilter = TimeFilter;
TimeFilter.propTypes = {
  filter: _propTypes.default.string.isRequired,
  commit: _propTypes.default.func.isRequired,
  // Canvas filter
  compact: _propTypes.default.bool
};