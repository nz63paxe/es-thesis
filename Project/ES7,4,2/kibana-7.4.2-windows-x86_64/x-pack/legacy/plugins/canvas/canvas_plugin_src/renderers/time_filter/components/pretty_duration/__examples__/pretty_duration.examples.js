"use strict";

var _react = require("@storybook/react");

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
(0, _react.storiesOf)('renderers/TimeFilter/components/PrettyDuration', module).add('with relative dates', () => _react2.default.createElement(_.PrettyDuration, {
  from: "now-7d",
  to: "now"
})).add('with absolute dates', () => _react2.default.createElement(_.PrettyDuration, {
  from: "01/01/2019",
  to: "02/01/2019"
}));