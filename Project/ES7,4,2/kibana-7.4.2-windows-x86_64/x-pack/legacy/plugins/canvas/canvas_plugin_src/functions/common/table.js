"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.table = table;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function table() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().table;
  return {
    name: 'table',
    aliases: [],
    type: 'render',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      font: {
        types: ['style'],
        default: '{font}',
        help: argHelp.font
      },
      paginate: {
        types: ['boolean'],
        default: true,
        help: argHelp.paginate,
        options: [true, false]
      },
      perPage: {
        types: ['number'],
        default: 10,
        help: argHelp.perPage
      },
      showHeader: {
        types: ['boolean'],
        default: true,
        help: argHelp.showHeader,
        options: [true, false]
      }
    },
    fn: (context, args) => {
      return {
        type: 'render',
        as: 'table',
        value: {
          datatable: context,
          ...args
        }
      };
    }
  };
}