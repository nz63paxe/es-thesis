"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.math = math;

var _tinymath = require("tinymath");

var _pivot_object_array = require("../../../common/lib/pivot_object_array");

var _types = require("../../../types");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore no @typed def; Elastic library
// @ts-ignore untyped local
function math() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().math;
  const errors = (0, _strings.getFunctionErrors)().math;
  return {
    name: 'math',
    type: 'number',
    help,
    context: {
      types: ['number', 'datatable']
    },
    args: {
      expression: {
        aliases: ['_'],
        types: ['string'],
        help: argHelp.expression
      }
    },
    fn: (context, args) => {
      const {
        expression
      } = args;

      if (!expression || expression.trim() === '') {
        throw errors.emptyExpression();
      }

      const mathContext = (0, _types.isDatatable)(context) ? (0, _pivot_object_array.pivotObjectArray)(context.rows, context.columns.map(col => col.name)) : {
        value: context
      };

      try {
        const result = (0, _tinymath.evaluate)(expression, mathContext);

        if (Array.isArray(result)) {
          if (result.length === 1) {
            return result[0];
          }

          throw errors.tooManyResults();
        }

        if (isNaN(result)) {
          throw errors.executionFailed();
        }

        return result;
      } catch (e) {
        if ((0, _types.isDatatable)(context) && context.rows.length === 0) {
          throw errors.emptyDatatable();
        } else {
          throw e;
        }
      }
    }
  };
}