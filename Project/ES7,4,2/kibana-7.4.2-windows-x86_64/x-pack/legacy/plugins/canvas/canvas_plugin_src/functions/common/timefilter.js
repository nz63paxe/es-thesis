"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timefilter = timefilter;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _strings = require("../../strings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function timefilter() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().timefilter;
  const errors = (0, _strings.getFunctionErrors)().timefilter;
  return {
    name: 'timefilter',
    aliases: [],
    type: 'filter',
    context: {
      types: ['filter']
    },
    help,
    args: {
      column: {
        types: ['string'],
        aliases: ['field', 'c'],
        default: '@timestamp',
        help: argHelp.column
      },
      from: {
        types: ['string'],
        aliases: ['f', 'start'],
        help: argHelp.from
      },
      to: {
        types: ['string'],
        aliases: ['t', 'end'],
        help: argHelp.to
      },
      filterGroup: {
        types: ['string'],
        help: 'The group name for the filter'
      }
    },
    fn: (context, args) => {
      if (!args.from && !args.to) {
        return context;
      }

      const {
        from,
        to,
        column
      } = args;
      const filter = {
        type: 'time',
        column,
        and: []
      };

      function parseAndValidate(str) {
        const moment = _datemath.default.parse(str);

        if (!moment || !moment.isValid()) {
          throw errors.invalidString(str);
        }

        return moment.toISOString();
      }

      if (!!to) {
        filter.to = parseAndValidate(to);
      }

      if (!!from) {
        filter.from = parseAndValidate(from);
      }

      return { ...context,
        and: [...context.and, filter]
      };
    }
  };
}