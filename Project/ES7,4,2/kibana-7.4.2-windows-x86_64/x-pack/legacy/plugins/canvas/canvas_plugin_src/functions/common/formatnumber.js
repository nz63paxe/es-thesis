"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatnumber = formatnumber;

var _numeral = _interopRequireDefault(require("@elastic/numeral"));

var _strings = require("../../strings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function formatnumber() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().formatnumber;
  return {
    name: 'formatnumber',
    type: 'string',
    help,
    context: {
      types: ['number']
    },
    args: {
      format: {
        aliases: ['_'],
        types: ['string'],
        help: argHelp.format,
        required: true
      }
    },
    fn: (context, args) => {
      if (!args.format) {
        return String(context);
      }

      return (0, _numeral.default)(context).format(args.format);
    }
  };
}