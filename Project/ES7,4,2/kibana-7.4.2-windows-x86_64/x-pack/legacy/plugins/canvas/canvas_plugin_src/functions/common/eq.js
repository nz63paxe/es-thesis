"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.eq = eq;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function eq() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().eq;
  return {
    name: 'eq',
    type: 'boolean',
    help,
    context: {
      types: ['boolean', 'number', 'string', 'null']
    },
    args: {
      value: {
        aliases: ['_'],
        types: ['boolean', 'number', 'string', 'null'],
        required: true,
        help: argHelp.value
      }
    },
    fn: (context, args) => {
      return context === args.value;
    }
  };
}