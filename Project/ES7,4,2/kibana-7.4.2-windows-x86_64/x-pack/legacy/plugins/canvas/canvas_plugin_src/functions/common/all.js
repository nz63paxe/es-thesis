"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.all = all;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function all() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().all;
  return {
    name: 'all',
    type: 'boolean',
    help,
    context: {
      types: ['null']
    },
    args: {
      condition: {
        aliases: ['_'],
        types: ['boolean'],
        help: argHelp.condition,
        required: true,
        multi: true
      }
    },
    fn: (_context, args) => {
      const conditions = args.condition || [];
      return conditions.every(Boolean);
    }
  };
}