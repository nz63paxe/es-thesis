"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timefilterControl = timefilterControl;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function timefilterControl() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().timefilterControl;
  return {
    name: 'timefilterControl',
    aliases: [],
    type: 'render',
    context: {
      types: ['null']
    },
    help,
    args: {
      column: {
        types: ['string'],
        aliases: ['field', 'c'],
        help: argHelp.column,
        default: '@timestamp'
      },
      compact: {
        types: ['boolean'],
        help: argHelp.compact,
        default: true,
        options: [true, false]
      },
      filterGroup: {
        types: ['string'],
        help: argHelp.filterGroup
      }
    },
    fn: (_context, args) => {
      return {
        type: 'render',
        as: 'time_filter',
        value: args
      };
    }
  };
}