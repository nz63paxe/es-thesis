"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.asFn = asFn;

var _common = require("@kbn/interpreter/common");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped Elastic library
function asFn() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().as;
  return {
    name: 'as',
    type: 'datatable',
    context: {
      types: ['string', 'boolean', 'number', 'null']
    },
    help,
    args: {
      name: {
        types: ['string'],
        aliases: ['_'],
        help: argHelp.name,
        default: 'value'
      }
    },
    fn: (context, args) => {
      return {
        type: 'datatable',
        columns: [{
          name: args.name,
          type: (0, _common.getType)(context)
        }],
        rows: [{
          [args.name]: context
        }]
      };
    }
  };
}