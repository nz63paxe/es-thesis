"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rounddate = rounddate;

var _moment = _interopRequireDefault(require("moment"));

var _strings = require("../../strings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function rounddate() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().rounddate;
  return {
    name: 'rounddate',
    type: 'number',
    help,
    context: {
      types: ['number']
    },
    args: {
      format: {
        aliases: ['_'],
        types: ['string'],
        help: argHelp.format
      }
    },
    fn: (context, args) => {
      if (!args.format) {
        return context;
      }

      return _moment.default.utc(_moment.default.utc(context).format(args.format), args.format).valueOf();
    }
  };
}