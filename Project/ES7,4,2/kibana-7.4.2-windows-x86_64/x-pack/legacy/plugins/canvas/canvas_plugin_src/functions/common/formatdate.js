"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatdate = formatdate;

var _moment = _interopRequireDefault(require("moment"));

var _strings = require("../../strings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function formatdate() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().formatdate;
  return {
    name: 'formatdate',
    type: 'string',
    help,
    context: {
      types: ['number', 'string']
    },
    args: {
      format: {
        aliases: ['_'],
        types: ['string'],
        required: true,
        help: argHelp.format
      }
    },
    fn: (context, args) => {
      if (!args.format) {
        return _moment.default.utc(new Date(context)).toISOString();
      }

      return _moment.default.utc(new Date(context)).format(args.format);
    }
  };
}