"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.string = string;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function string() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().string;
  return {
    name: 'string',
    context: {
      types: ['null']
    },
    aliases: [],
    type: 'string',
    help,
    args: {
      value: {
        aliases: ['_'],
        types: ['string', 'number', 'boolean'],
        multi: true,
        help: argHelp.value
      }
    },
    fn: (_context, args) => args.value.join('')
  };
}