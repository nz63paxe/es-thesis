"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PrettyDuration = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _format_duration = require("./lib/format_duration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const PrettyDuration = ({
  from,
  to
}) => _react.default.createElement("span", null, (0, _format_duration.formatDuration)(from, to));

exports.PrettyDuration = PrettyDuration;
PrettyDuration.propTypes = {
  from: _propTypes.default.string.isRequired,
  to: _propTypes.default.string.isRequired
};