"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sort = sort;

var _lodash = require("lodash");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function sort() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().sort;
  return {
    name: 'sort',
    type: 'datatable',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      by: {
        types: ['string'],
        aliases: ['_', 'column'],
        multi: false,
        // TODO: No reason you couldn't.
        help: argHelp.by
      },
      reverse: {
        types: ['boolean'],
        help: argHelp.reverse,
        options: [true, false],
        default: false
      }
    },
    fn: (context, args) => {
      const column = args.by || context.columns[0].name;
      return { ...context,
        rows: args.reverse ? (0, _lodash.sortBy)(context.rows, column).reverse() : (0, _lodash.sortBy)(context.rows, column)
      };
    }
  };
}