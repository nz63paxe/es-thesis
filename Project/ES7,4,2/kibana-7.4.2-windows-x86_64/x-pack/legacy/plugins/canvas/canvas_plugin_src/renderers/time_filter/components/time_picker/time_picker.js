"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimePicker = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

var _datetime_quick_list = require("../datetime_quick_list");

var _datetime_range_absolute = require("../datetime_range_absolute");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class TimePicker extends _react.Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      range: {
        from: this.props.from,
        to: this.props.to
      },
      isDirty: false
    });

    _defineProperty(this, "_absoluteSelect", (from, to) => {
      if (from && to) {
        this.setState({
          range: {
            from: (0, _moment.default)(from).toISOString(),
            to: (0, _moment.default)(to).toISOString()
          },
          isDirty: true
        });
      }
    });
  }

  // TODO: Refactor to no longer use componentWillReceiveProps since it is being deprecated
  componentWillReceiveProps({
    from,
    to
  }) {
    if (from !== this.props.from || to !== this.props.to) {
      this.setState({
        range: {
          from,
          to
        },
        isDirty: false
      });
    }
  }

  render() {
    const {
      onSelect
    } = this.props;
    const {
      range,
      isDirty
    } = this.state;
    const {
      from,
      to
    } = range;
    return _react.default.createElement("div", {
      className: "canvasTimePicker"
    }, _react.default.createElement(_datetime_range_absolute.DatetimeRangeAbsolute, {
      from: _datemath.default.parse(from),
      to: _datemath.default.parse(to),
      onSelect: this._absoluteSelect
    }), _react.default.createElement(_datetime_quick_list.DatetimeQuickList, {
      from: from,
      to: to,
      onSelect: onSelect
    }, _react.default.createElement(_eui.EuiButton, {
      fill: true,
      size: "s",
      disabled: !isDirty,
      className: "canvasTimePicker__apply",
      onClick: () => {
        this.setState({
          isDirty: false
        });
        onSelect(from, to);
      }
    }, "Apply")));
  }

}

exports.TimePicker = TimePicker;

_defineProperty(TimePicker, "propTypes", {
  from: _propTypes.default.string.isRequired,
  to: _propTypes.default.string.isRequired,
  onSelect: _propTypes.default.func.isRequired
});