"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatetimeInput = void 0;

var _recompose = require("recompose");

var _datetime_input = require("./datetime_input");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DatetimeInput = (0, _recompose.compose)((0, _recompose.withState)('valid', 'setValid', () => true), (0, _recompose.withState)('strValue', 'setStrValue', ({
  moment
}) => moment ? moment.format('YYYY-MM-DD HH:mm:ss') : ''), (0, _recompose.lifecycle)({
  // TODO: Refactor to no longer use componentWillReceiveProps since it is being deprecated
  componentWillReceiveProps({
    moment,
    setStrValue,
    setValid
  }) {
    if (!moment) return;

    if (this.props.moment && this.props.moment.isSame(moment)) {
      return;
    }

    setStrValue(moment.format('YYYY-MM-DD HH:mm:ss'));
    setValid(true);
  }

}))(_datetime_input.DatetimeInput);
exports.DatetimeInput = DatetimeInput;