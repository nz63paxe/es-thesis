"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatetimeCalendar = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactMomentProptypes = require("react-moment-proptypes");

var _eui = require("@elastic/eui");

var _datetime_input = require("../datetime_input");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DatetimeCalendar = ({
  value,
  onValueChange,
  onSelect,
  startDate,
  endDate,
  minDate,
  maxDate
}) => _react.default.createElement("div", {
  className: "canvasDateTimeCal"
}, _react.default.createElement(_datetime_input.DatetimeInput, {
  moment: value,
  setMoment: onValueChange
}), _react.default.createElement(_eui.EuiDatePicker, {
  inline: true,
  showTimeSelect: true,
  shadow: false,
  selected: value && value.isValid() ? value : null,
  onChange: onSelect,
  shouldCloseOnSelect: false,
  startDate: startDate,
  endDate: endDate,
  minDate: minDate,
  maxDate: maxDate
}));

exports.DatetimeCalendar = DatetimeCalendar;
DatetimeCalendar.propTypes = {
  value: _propTypes.default.oneOfType([_reactMomentProptypes.momentObj, _propTypes.default.object]),
  // Handle both valid and invalid moment objects
  onSelect: _propTypes.default.func.isRequired,
  onValueChange: _propTypes.default.func.isRequired,
  // Called with a moment
  startDate: _reactMomentProptypes.momentObj,
  endDate: _reactMomentProptypes.momentObj,
  minDate: _reactMomentProptypes.momentObj,
  maxDate: _reactMomentProptypes.momentObj
};