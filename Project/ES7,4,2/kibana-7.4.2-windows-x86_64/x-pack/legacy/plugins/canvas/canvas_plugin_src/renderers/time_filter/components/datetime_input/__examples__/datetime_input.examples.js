"use strict";

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
(0, _react.storiesOf)('renderers/TimeFilter/components/DatetimeInput', module).add('default', () => _react2.default.createElement(_.DatetimeInput, {
  setMoment: (0, _addonActions.action)('setMoment')
})).add('with date', () => _react2.default.createElement(_.DatetimeInput, {
  moment: _moment.default.utc('2018-02-20 19:26:52'),
  setMoment: (0, _addonActions.action)('setMoment')
})).add('invalid date', () => _react2.default.createElement(_.DatetimeInput, {
  moment: (0, _moment.default)('foo'),
  setMoment: (0, _addonActions.action)('setMoment')
}));