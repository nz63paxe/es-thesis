"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.dropdownControl = dropdownControl;

var _lodash = require("lodash");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function dropdownControl() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().dropdownControl;
  return {
    name: 'dropdownControl',
    aliases: [],
    type: 'render',
    context: {
      types: ['datatable']
    },
    help,
    args: {
      filterColumn: {
        types: ['string'],
        required: true,
        help: argHelp.filterColumn
      },
      valueColumn: {
        types: ['string'],
        required: true,
        help: argHelp.valueColumn
      },
      filterGroup: {
        types: ['string'],
        help: argHelp.filterGroup
      }
    },
    fn: (context, {
      valueColumn,
      filterColumn,
      filterGroup
    }) => {
      let choices = [];

      if (context.rows[0][valueColumn]) {
        choices = (0, _lodash.uniq)(context.rows.map(row => row[valueColumn])).sort();
      }

      const column = filterColumn || valueColumn;
      return {
        type: 'render',
        as: 'dropdown_filter',
        value: {
          column,
          choices,
          filterGroup
        }
      };
    }
  };
}