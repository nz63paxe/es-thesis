"use strict";

var _eui = require("@elastic/eui");

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
(0, _react.storiesOf)('renderers/TimeFilter/components/DatetimeQuickList', module).add('with start and end dates', () => _react2.default.createElement(_.DatetimeQuickList, {
  from: "now-7d",
  to: "now",
  onSelect: (0, _addonActions.action)('onSelect')
})).add('with children', () => _react2.default.createElement(_.DatetimeQuickList, {
  from: "now-7d",
  to: "now",
  onSelect: (0, _addonActions.action)('onSelect')
}, _react2.default.createElement(_eui.EuiButtonEmpty, null, "Apply")));