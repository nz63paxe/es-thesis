"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UTC = exports.URL = exports.TYPE_STRING = exports.TYPE_NUMBER = exports.TYPE_NULL = exports.TYPE_BOOLEAN = exports.TINYMATH_URL = exports.TINYMATH = exports.SVG = exports.SQL = exports.NUMERALJS = exports.MOMENTJS = exports.MARKDOWN = exports.LUCENE = exports.JS = exports.ISO8601 = exports.FONT_WEIGHT = exports.FONT_FAMILY = exports.ELASTICSEARCH = exports.DATATABLE = exports.DATEMATH = exports.CSV = exports.CSS = exports.CONTEXT = exports.CANVAS = exports.BOOLEAN_TRUE = exports.BOOLEAN_FALSE = exports.BASE64 = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const BASE64 = '`base64`';
exports.BASE64 = BASE64;
const BOOLEAN_FALSE = '`false`';
exports.BOOLEAN_FALSE = BOOLEAN_FALSE;
const BOOLEAN_TRUE = '`true`';
exports.BOOLEAN_TRUE = BOOLEAN_TRUE;
const CANVAS = 'Canvas';
exports.CANVAS = CANVAS;
const CONTEXT = '_context_';
exports.CONTEXT = CONTEXT;
const CSS = 'CSS';
exports.CSS = CSS;
const CSV = 'CSV';
exports.CSV = CSV;
const DATEMATH = '`datemath`';
exports.DATEMATH = DATEMATH;
const DATATABLE = '`datatable`';
exports.DATATABLE = DATATABLE;
const ELASTICSEARCH = 'Elasticsearch';
exports.ELASTICSEARCH = ELASTICSEARCH;
const FONT_FAMILY = '`font-family`';
exports.FONT_FAMILY = FONT_FAMILY;
const FONT_WEIGHT = '`font-weight`';
exports.FONT_WEIGHT = FONT_WEIGHT;
const ISO8601 = 'ISO8601';
exports.ISO8601 = ISO8601;
const JS = 'JavaScript';
exports.JS = JS;
const LUCENE = 'Lucene';
exports.LUCENE = LUCENE;
const MARKDOWN = 'Markdown';
exports.MARKDOWN = MARKDOWN;
const MOMENTJS = 'MomentJS';
exports.MOMENTJS = MOMENTJS;
const NUMERALJS = 'NumeralJS';
exports.NUMERALJS = NUMERALJS;
const SQL = 'SQL';
exports.SQL = SQL;
const SVG = 'SVG';
exports.SVG = SVG;
const TINYMATH = '`TinyMath`';
exports.TINYMATH = TINYMATH;
const TINYMATH_URL = 'https://www.elastic.co/guide/en/kibana/current/canvas-tinymath-functions.html';
exports.TINYMATH_URL = TINYMATH_URL;
const TYPE_BOOLEAN = '`boolean`';
exports.TYPE_BOOLEAN = TYPE_BOOLEAN;
const TYPE_NULL = '`null`';
exports.TYPE_NULL = TYPE_NULL;
const TYPE_NUMBER = '`number`';
exports.TYPE_NUMBER = TYPE_NUMBER;
const TYPE_STRING = '`string`';
exports.TYPE_STRING = TYPE_STRING;
const URL = 'URL';
exports.URL = URL;
const UTC = 'UTC';
exports.UTC = UTC;