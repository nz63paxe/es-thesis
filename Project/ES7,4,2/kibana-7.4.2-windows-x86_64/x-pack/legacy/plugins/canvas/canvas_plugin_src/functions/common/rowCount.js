"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.rowCount = rowCount;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function rowCount() {
  const {
    help
  } = (0, _strings.getFunctionHelp)().rowCount;
  return {
    name: 'rowCount',
    aliases: [],
    type: 'number',
    help,
    context: {
      types: ['datatable']
    },
    args: {},
    fn: context => context.rows.length
  };
}