"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFunctionErrors = void 0;

var _alterColumn = require("./alterColumn");

var _axisConfig = require("./axisConfig");

var _compare = require("./compare");

var _containerStyle = require("./containerStyle");

var _csv = require("./csv");

var _date = require("./date");

var _getCell = require("./getCell");

var _join_rows = require("./join_rows");

var _image = require("./image");

var _math = require("./math");

var _ply = require("./ply");

var _progress = require("./progress");

var _revealImage = require("./revealImage");

var _timefilter = require("./timefilter");

var _demodata = require("./demodata");

var _pointseries = require("./pointseries");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getFunctionErrors = () => ({
  alterColumn: _alterColumn.errors,
  axisConfig: _axisConfig.errors,
  compare: _compare.errors,
  containerStyle: _containerStyle.errors,
  csv: _csv.errors,
  date: _date.errors,
  getCell: _getCell.errors,
  image: _image.errors,
  joinRows: _join_rows.errors,
  math: _math.errors,
  ply: _ply.errors,
  progress: _progress.errors,
  revealImage: _revealImage.errors,
  timefilter: _timefilter.errors,
  demodata: _demodata.errors,
  pointseries: _pointseries.errors
});

exports.getFunctionErrors = getFunctionErrors;