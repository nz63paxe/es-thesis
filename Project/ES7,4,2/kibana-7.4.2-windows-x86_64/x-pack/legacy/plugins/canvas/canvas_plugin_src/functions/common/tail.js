"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tail = tail;

var _lodash = require("lodash");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function tail() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().tail;
  return {
    name: 'tail',
    aliases: [],
    type: 'datatable',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      count: {
        aliases: ['_'],
        types: ['number'],
        help: argHelp.count
      }
    },
    fn: (context, args) => ({ ...context,
      rows: (0, _lodash.takeRight)(context.rows, args.count)
    })
  };
}