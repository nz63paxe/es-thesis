"use strict";

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const startDate = _moment.default.utc('2018-04-04').toISOString();

const endDate = _moment.default.utc('2019-04-04').toISOString();

(0, _react.storiesOf)('renderers/TimeFilter/components/TimePicker', module).add('default', () => _react2.default.createElement(_.TimePicker, {
  from: startDate,
  to: endDate,
  onSelect: (0, _addonActions.action)('onSelect')
}));