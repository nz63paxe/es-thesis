"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.alterColumn = alterColumn;

var _lodash = require("lodash");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function alterColumn() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().alterColumn;
  const errors = (0, _strings.getFunctionErrors)().alterColumn;
  return {
    name: 'alterColumn',
    type: 'datatable',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      column: {
        aliases: ['_'],
        types: ['string'],
        required: true,
        help: argHelp.column
      },
      name: {
        types: ['string'],
        help: argHelp.name
      },
      type: {
        types: ['string'],
        help: argHelp.type,
        options: ['null', 'boolean', 'number', 'string', 'date']
      }
    },
    fn: (context, args) => {
      if (!args.column || !args.type && !args.name) {
        return context;
      }

      const column = context.columns.find(col => col.name === args.column);

      if (!column) {
        throw errors.columnNotFound(args.column);
      }

      const name = args.name || column.name;
      const type = args.type || column.type;
      const columns = context.columns.reduce((all, col) => {
        if (col.name !== args.name) {
          if (col.name !== column.name) {
            all.push(col);
          } else {
            all.push({
              name,
              type
            });
          }
        }

        return all;
      }, []);

      let handler = val => val;

      if (args.type) {
        handler = function getHandler() {
          switch (type) {
            case 'string':
              if (column.type === 'date') {
                return v => new Date(v).toISOString();
              }

              return String;

            case 'number':
              return Number;

            case 'date':
              return v => new Date(v).valueOf();

            case 'boolean':
              return Boolean;

            case 'null':
              return () => null;

            default:
              throw errors.cannotConvertType(type);
          }
        }();
      }

      const rows = context.rows.map(row => ({ ...(0, _lodash.omit)(row, column.name),
        [name]: handler(row[column.name])
      }));
      return {
        type: 'datatable',
        columns,
        rows
      };
    }
  };
}