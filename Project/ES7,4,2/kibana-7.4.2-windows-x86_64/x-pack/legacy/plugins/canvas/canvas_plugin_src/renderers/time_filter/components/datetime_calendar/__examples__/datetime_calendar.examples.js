"use strict";

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const startDate = _moment.default.utc('2019-06-27');

const endDate = _moment.default.utc('2019-07-04');

(0, _react.storiesOf)('renderers/TimeFilter/components/DatetimeCalendar', module).add('default', () => _react2.default.createElement(_.DatetimeCalendar, {
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange')
})).add('with value', () => _react2.default.createElement(_.DatetimeCalendar, {
  value: startDate,
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange')
})).add('with start and end date', () => _react2.default.createElement(_.DatetimeCalendar, {
  startDate: startDate,
  endDate: endDate,
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange')
})).add('with min date', () => _react2.default.createElement(_.DatetimeCalendar, {
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange'),
  minDate: endDate
})).add('with max date', () => _react2.default.createElement(_.DatetimeCalendar, {
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange'),
  maxDate: endDate
})).add('invalid date', () => _react2.default.createElement(_.DatetimeCalendar, {
  value: (0, _moment.default)('foo'),
  onSelect: (0, _addonActions.action)('onSelect'),
  onValueChange: (0, _addonActions.action)('onValueChange')
}));