"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFunctionHelp = void 0;

var _all = require("./all");

var _alterColumn = require("./alterColumn");

var _any = require("./any");

var _as = require("./as");

var _axisConfig = require("./axisConfig");

var _case = require("./case");

var _clear = require("./clear");

var _columns = require("./columns");

var _compare = require("./compare");

var _containerStyle = require("./containerStyle");

var _context = require("./context");

var _csv = require("./csv");

var _date = require("./date");

var _demodata = require("./demodata");

var _do = require("./do");

var _dropdownControl = require("./dropdownControl");

var _eq = require("./eq");

var _escount = require("./escount");

var _esdocs = require("./esdocs");

var _essql = require("./essql");

var _exactly = require("./exactly");

var _filterrows = require("./filterrows");

var _formatdate = require("./formatdate");

var _formatnumber = require("./formatnumber");

var _getCell = require("./getCell");

var _gt = require("./gt");

var _gte = require("./gte");

var _head = require("./head");

var _if = require("./if");

var _image = require("./image");

var _join_rows = require("./join_rows");

var _location = require("./location");

var _lt = require("./lt");

var _lte = require("./lte");

var _mapColumn = require("./mapColumn");

var _markdown = require("./markdown");

var _math = require("./math");

var _metric = require("./metric");

var _neq = require("./neq");

var _palette = require("./palette");

var _pie = require("./pie");

var _plot = require("./plot");

var _ply = require("./ply");

var _pointseries = require("./pointseries");

var _progress = require("./progress");

var _render = require("./render");

var _repeatImage = require("./repeatImage");

var _replace = require("./replace");

var _revealImage = require("./revealImage");

var _rounddate = require("./rounddate");

var _rowCount = require("./rowCount");

var _seriesStyle = require("./seriesStyle");

var _shape = require("./shape");

var _sort = require("./sort");

var _staticColumn = require("./staticColumn");

var _string = require("./string");

var _switch = require("./switch");

var _table = require("./table");

var _tail = require("./tail");

var _timefilter = require("./timefilter");

var _timefilterControl = require("./timefilterControl");

var _urlparam = require("./urlparam");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Help text for Canvas Functions should be properly localized. This function will
 * return a dictionary of help strings, organized by `CanvasFunction` specification
 * and then by available arguments within each `CanvasFunction`.
 *
 * This a function, rather than an object, to future-proof string initialization,
 * if ever necessary.
 */
const getFunctionHelp = () => ({
  all: _all.help,
  alterColumn: _alterColumn.help,
  any: _any.help,
  as: _as.help,
  axisConfig: _axisConfig.help,
  case: _case.help,
  clear: _clear.help,
  columns: _columns.help,
  compare: _compare.help,
  containerStyle: _containerStyle.help,
  context: _context.help,
  csv: _csv.help,
  date: _date.help,
  demodata: _demodata.help,
  do: _do.help,
  dropdownControl: _dropdownControl.help,
  eq: _eq.help,
  escount: _escount.help,
  esdocs: _esdocs.help,
  essql: _essql.help,
  exactly: _exactly.help,
  filterrows: _filterrows.help,
  formatdate: _formatdate.help,
  formatnumber: _formatnumber.help,
  getCell: _getCell.help,
  gt: _gt.help,
  gte: _gte.help,
  head: _head.help,
  if: _if.help,
  joinRows: _join_rows.help,
  image: _image.help,
  location: _location.help,
  lt: _lt.help,
  lte: _lte.help,
  mapColumn: _mapColumn.help,
  markdown: _markdown.help,
  math: _math.help,
  metric: _metric.help,
  neq: _neq.help,
  palette: _palette.help,
  pie: _pie.help,
  plot: _plot.help,
  ply: _ply.help,
  pointseries: _pointseries.help,
  progress: _progress.help,
  render: _render.help,
  repeatImage: _repeatImage.help,
  replace: _replace.help,
  revealImage: _revealImage.help,
  rounddate: _rounddate.help,
  rowCount: _rowCount.help,
  seriesStyle: _seriesStyle.help,
  shape: _shape.help,
  sort: _sort.help,
  staticColumn: _staticColumn.help,
  string: _string.help,
  switch: _switch.help,
  table: _table.help,
  tail: _tail.help,
  timefilter: _timefilter.help,
  timefilterControl: _timefilterControl.help,
  urlparam: _urlparam.help
});

exports.getFunctionHelp = getFunctionHelp;