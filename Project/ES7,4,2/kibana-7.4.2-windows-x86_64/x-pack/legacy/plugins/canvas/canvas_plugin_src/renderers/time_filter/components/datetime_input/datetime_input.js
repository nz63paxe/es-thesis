"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatetimeInput = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DatetimeInput = ({
  strValue,
  setStrValue,
  setMoment,
  valid,
  setValid
}) => {
  function check(e) {
    const parsed = (0, _moment.default)(e.target.value, 'YYYY-MM-DD HH:mm:ss', true);

    if (parsed.isValid()) {
      setMoment(parsed);
      setValid(true);
    } else {
      setValid(false);
    }

    setStrValue(e.target.value);
  }

  return _react.default.createElement(_eui.EuiFieldText, {
    compressed: true,
    value: strValue,
    onChange: check,
    isInvalid: !valid,
    style: {
      textAlign: 'center'
    }
  });
};

exports.DatetimeInput = DatetimeInput;
DatetimeInput.propTypes = {
  setMoment: _propTypes.default.func,
  strValue: _propTypes.default.string,
  setStrValue: _propTypes.default.func,
  valid: _propTypes.default.bool,
  setValid: _propTypes.default.func
};