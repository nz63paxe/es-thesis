"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.context = context;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function context() {
  const {
    help
  } = (0, _strings.getFunctionHelp)().context;
  return {
    name: 'context',
    help,
    args: {},
    fn: obj => obj
  };
}