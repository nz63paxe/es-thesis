"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.markdown = markdown;

var _handlebars = require("../../../common/lib/handlebars");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped local
function markdown() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().markdown;
  return {
    name: 'markdown',
    aliases: [],
    type: 'render',
    help,
    context: {
      types: ['datatable', 'null']
    },
    args: {
      content: {
        aliases: ['_', 'expression'],
        types: ['string'],
        help: argHelp.content,
        default: '""',
        multi: true
      },
      font: {
        types: ['style'],
        help: argHelp.font,
        default: '{font}'
      }
    },
    fn: (context, args) => {
      const compileFunctions = args.content.map(str => _handlebars.Handlebars.compile(String(str), {
        knownHelpersOnly: true
      }));
      const ctx = {
        columns: [],
        rows: [],
        type: null,
        ...context
      };
      return {
        type: 'render',
        as: 'markdown',
        value: {
          content: compileFunctions.map(fn => fn(ctx)).join(''),
          font: args.font
        }
      };
    }
  };
}