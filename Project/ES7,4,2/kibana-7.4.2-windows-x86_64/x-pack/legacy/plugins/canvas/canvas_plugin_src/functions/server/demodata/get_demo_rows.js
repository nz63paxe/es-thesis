"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDemoRows = getDemoRows;
exports.DemoRows = void 0;

var _lodash = require("lodash");

var _ci = _interopRequireDefault(require("./ci.json"));

var _shirts = _interopRequireDefault(require("./shirts.json"));

var _strings = require("../../../strings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let DemoRows;
exports.DemoRows = DemoRows;

(function (DemoRows) {
  DemoRows["CI"] = "ci";
  DemoRows["SHIRTS"] = "shirts";
})(DemoRows || (exports.DemoRows = DemoRows = {}));

function getDemoRows(arg) {
  if (arg === DemoRows.CI) {
    return (0, _lodash.cloneDeep)(_ci.default);
  }

  if (arg === DemoRows.SHIRTS) {
    return (0, _lodash.cloneDeep)(_shirts.default);
  }

  throw (0, _strings.getFunctionErrors)().demodata.invalidDataSet(arg);
}