"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lt = lt;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function lt() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().lt;
  return {
    name: 'lt',
    type: 'boolean',
    context: {
      types: ['number', 'string']
    },
    help,
    args: {
      value: {
        aliases: ['_'],
        types: ['number', 'string'],
        required: true,
        help: argHelp.value
      }
    },
    fn: (context, args) => {
      const {
        value
      } = args;

      if (typeof context !== typeof value) {
        return false;
      }

      return context < value;
    }
  };
}