"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatDuration = formatDuration;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _moment = _interopRequireDefault(require("moment"));

var _quick_ranges = require("./quick_ranges");

var _time_units = require("../../../../../../common/lib/time_units");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const lookupByRange = {};

_quick_ranges.quickRanges.forEach(frame => {
  lookupByRange[`${frame.from} to ${frame.to}`] = frame;
});

function formatTime(time, roundUp = false) {
  if (_moment.default.isMoment(time)) {
    return time.format('lll');
  } else {
    if (time === 'now') {
      return 'now';
    } else {
      const tryParse = _datemath.default.parse(time, {
        roundUp
      });

      return _moment.default.isMoment(tryParse) ? '~ ' + tryParse.fromNow() : time;
    }
  }
}

function cantLookup(from, to) {
  return `${formatTime(from)} to ${formatTime(to)}`;
}

function formatDuration(from, to) {
  // If both parts are date math, try to look up a reasonable string
  if (from && to && !_moment.default.isMoment(from) && !_moment.default.isMoment(to)) {
    const tryLookup = lookupByRange[`${from.toString()} to ${to.toString()}`];

    if (tryLookup) {
      return tryLookup.display;
    } else {
      const fromParts = from.toString().split('-');

      if (to.toString() === 'now' && fromParts[0] === 'now' && fromParts[1]) {
        const rounded = fromParts[1].split('/');
        let text = `Last  ${rounded[0]}`;

        if (rounded[1]) {
          const unit = rounded[1];
          text = `${text} rounded to the ${_time_units.timeUnits[unit]}`;
        }

        return text;
      } else {
        return cantLookup(from, to);
      }
    } // If at least one part is a moment, try to make pretty strings by parsing date math

  } else {
    return cantLookup(from, to);
  }
}