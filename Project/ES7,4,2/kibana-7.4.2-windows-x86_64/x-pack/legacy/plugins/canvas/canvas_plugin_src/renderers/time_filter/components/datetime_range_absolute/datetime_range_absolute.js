"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatetimeRangeAbsolute = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactMomentProptypes = require("react-moment-proptypes");

var _datetime_calendar = require("../datetime_calendar");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const DatetimeRangeAbsolute = ({
  from,
  to,
  onSelect
}) => _react.default.createElement("div", {
  className: "canvasDateTimeRangeAbsolute"
}, _react.default.createElement("div", null, _react.default.createElement(_datetime_calendar.DatetimeCalendar, {
  value: from,
  startDate: from,
  endDate: to,
  maxDate: to,
  onValueChange: val => onSelect(val, to),
  onSelect: val => {
    if (!val || !from) {
      return;
    } // sets the time to start of day if only the date was selected


    if (from.format('hh:mm:ss a') === val.format('hh:mm:ss a')) {
      onSelect(val.startOf('day'), to);
    } else {
      onSelect(val, to);
    }
  }
})), _react.default.createElement("div", null, _react.default.createElement(_datetime_calendar.DatetimeCalendar, {
  value: to,
  startDate: from,
  endDate: to,
  minDate: from,
  onValueChange: val => onSelect(from, val),
  onSelect: val => {
    if (!val || !to) {
      return;
    } // set the time to end of day if only the date was selected


    if (to.format('hh:mm:ss a') === val.format('hh:mm:ss a')) {
      onSelect(from, val.endOf('day'));
    } else {
      onSelect(from, val);
    }
  }
})));

exports.DatetimeRangeAbsolute = DatetimeRangeAbsolute;
DatetimeRangeAbsolute.propTypes = {
  from: _reactMomentProptypes.momentObj,
  to: _reactMomentProptypes.momentObj,
  onSelect: _propTypes.default.func.isRequired
};