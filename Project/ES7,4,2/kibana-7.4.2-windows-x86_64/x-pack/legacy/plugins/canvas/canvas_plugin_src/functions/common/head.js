"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.head = head;

var _lodash = require("lodash");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function head() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().head;
  return {
    name: 'head',
    aliases: [],
    type: 'datatable',
    help,
    context: {
      types: ['datatable']
    },
    args: {
      count: {
        aliases: ['_'],
        types: ['number'],
        help: argHelp.count,
        default: 1
      }
    },
    fn: (context, args) => ({ ...context,
      rows: (0, _lodash.take)(context.rows, args.count)
    })
  };
}