"use strict";

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _react2 = _interopRequireDefault(require("react"));

var _ = require("..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
(0, _react.storiesOf)('renderers/TimeFilter', module).add('default', () => _react2.default.createElement(_.TimeFilter, {
  filter: "timefilter from=now-1y to=now-7d column=@timestamp",
  commit: (0, _addonActions.action)('commit')
})).add('compact mode', () => _react2.default.createElement(_.TimeFilter, {
  filter: "timefilter from=now-7d to=now column=@timestamp",
  compact: true,
  commit: (0, _addonActions.action)('commit')
}));