"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formatnumber = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const formatnumber = () => ({
  name: 'formatnumber',
  displayName: 'Number format',
  args: [{
    name: 'format',
    displayName: 'Format',
    argType: 'numberformat'
  }]
});

exports.formatnumber = formatnumber;