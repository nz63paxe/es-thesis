"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DatetimeQuickList = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _eui = require("@elastic/eui");

require("react-datetime/css/react-datetime.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const quickRanges = [{
  from: 'now/d',
  to: 'now',
  display: 'Today'
}, {
  from: 'now-24h',
  to: 'now',
  display: 'Last 24 hours'
}, {
  from: 'now-7d',
  to: 'now',
  display: 'Last 7 days'
}, {
  from: 'now-14d',
  to: 'now',
  display: 'Last 2 weeks'
}, {
  from: 'now-30d',
  to: 'now',
  display: 'Last 30 days'
}, {
  from: 'now-90d',
  to: 'now',
  display: 'Last 90 days'
}, {
  from: 'now-1y',
  to: 'now',
  display: 'Last 1 year'
}];

const DatetimeQuickList = ({
  from,
  to,
  onSelect,
  children
}) => _react.default.createElement("div", {
  style: {
    display: 'grid',
    alignItems: 'center'
  }
}, quickRanges.map((range, i) => from === range.from && to === range.to ? _react.default.createElement(_eui.EuiButton, {
  size: "s",
  fill: true,
  key: i,
  onClick: () => onSelect(range.from, range.to)
}, range.display) : _react.default.createElement(_eui.EuiButtonEmpty, {
  size: "s",
  key: i,
  onClick: () => onSelect(range.from, range.to)
}, range.display)), children);

exports.DatetimeQuickList = DatetimeQuickList;
DatetimeQuickList.propTypes = {
  from: _propTypes.default.string.isRequired,
  to: _propTypes.default.string.isRequired,
  onSelect: _propTypes.default.func.isRequired,
  children: _propTypes.default.node
};