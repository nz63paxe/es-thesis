"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TimePickerPopover = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _popover = require("../../../../../public/components/popover");

var _pretty_duration = require("../pretty_duration");

var _format_duration = require("../pretty_duration/lib/format_duration");

var _time_picker = require("../time_picker");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped local
const TimePickerPopover = ({
  from,
  to,
  onSelect
}) => {
  const button = handleClick => _react.default.createElement("button", {
    className: "canvasTimePickerPopover__button",
    "aria-label": `Displaying data ${(0, _format_duration.formatDuration)(from, to)}. Click to open a calendar tool to select a new time range.`,
    onClick: handleClick
  }, _react.default.createElement(_pretty_duration.PrettyDuration, {
    from: from,
    to: to
  }));

  return _react.default.createElement(_popover.Popover, {
    id: "timefilter-popover-trigger-click",
    className: "canvasTimePickerPopover",
    anchorClassName: "canvasTimePickerPopover__anchor",
    button: button
  }, () => _react.default.createElement(_time_picker.TimePicker, {
    from: from,
    to: to,
    onSelect: onSelect
  }));
};

exports.TimePickerPopover = TimePickerPopover;
TimePickerPopover.propTypes = {
  from: _propTypes.default.string.isRequired,
  to: _propTypes.default.string.isRequired,
  onSelect: _propTypes.default.func.isRequired
};