"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.escount = escount;

var _build_es_request = require("../../../server/lib/build_es_request");

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore untyped local
function escount() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().escount;
  return {
    name: 'escount',
    type: 'number',
    help,
    context: {
      types: ['filter']
    },
    args: {
      query: {
        types: ['string'],
        aliases: ['_', 'q'],
        help: argHelp.query,
        default: '"-_index:.kibana"'
      },
      index: {
        types: ['string'],
        default: '_all',
        help: argHelp.index
      }
    },
    fn: (context, args, handlers) => {
      context.and = context.and.concat([{
        type: 'luceneQueryString',
        query: args.query,
        and: []
      }]);
      const esRequest = (0, _build_es_request.buildESRequest)({
        index: args.index,
        body: {
          query: {
            bool: {
              must: [{
                match_all: {}
              }]
            }
          }
        }
      }, context);
      return handlers.elasticsearchClient('count', esRequest).then(resp => resp.count);
    }
  };
}