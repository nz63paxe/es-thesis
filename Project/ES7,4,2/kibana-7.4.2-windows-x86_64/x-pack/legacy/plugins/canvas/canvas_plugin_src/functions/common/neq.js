"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.neq = neq;

var _strings = require("../../strings");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function neq() {
  const {
    help,
    args: argHelp
  } = (0, _strings.getFunctionHelp)().neq;
  return {
    name: 'neq',
    type: 'boolean',
    help,
    args: {
      value: {
        aliases: ['_'],
        types: ['boolean', 'number', 'string', 'null'],
        required: true,
        help: argHelp.value
      }
    },
    fn: (context, args) => {
      return context !== args.value;
    }
  };
}