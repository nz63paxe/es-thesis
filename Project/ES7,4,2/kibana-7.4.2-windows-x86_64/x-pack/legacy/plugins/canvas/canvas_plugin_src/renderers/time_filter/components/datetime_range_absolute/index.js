"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "DatetimeRangeAbsolute", {
  enumerable: true,
  get: function () {
    return _datetime_range_absolute.DatetimeRangeAbsolute;
  }
});

var _datetime_range_absolute = require("./datetime_range_absolute");