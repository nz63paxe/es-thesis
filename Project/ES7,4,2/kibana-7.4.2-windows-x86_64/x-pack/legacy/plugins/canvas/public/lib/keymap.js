"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.keymap = void 0;

var _lodash = require("lodash");

var _constants = require("../../common/lib/constants");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

// maps key for all OS's with optional modifiers
var getShortcuts = function getShortcuts(shortcuts, _ref) {
  var _ref$modifiers = _ref.modifiers,
      modifiers = _ref$modifiers === void 0 ? [] : _ref$modifiers,
      help = _ref.help;

  // normalize shortcut values
  if (!Array.isArray(shortcuts)) {
    shortcuts = [shortcuts];
  } // normalize modifier values


  if (!Array.isArray(modifiers)) {
    modifiers = [modifiers];
  }

  var macShortcuts = _toConsumableArray(shortcuts); // handle shift modifier


  if (modifiers.includes('shift')) {
    macShortcuts = macShortcuts.map(function (shortcut) {
      return "shift+".concat(shortcut);
    });
    shortcuts = shortcuts.map(function (shortcut) {
      return "shift+".concat(shortcut);
    });
  } // handle alt modifier


  if (modifiers.includes('alt') || modifiers.includes('option')) {
    macShortcuts = macShortcuts.map(function (shortcut) {
      return "option+".concat(shortcut);
    });
    shortcuts = shortcuts.map(function (shortcut) {
      return "alt+".concat(shortcut);
    });
  } // handle ctrl modifier


  if (modifiers.includes('ctrl') || modifiers.includes('command')) {
    macShortcuts = macShortcuts.map(function (shortcut) {
      return "command+".concat(shortcut);
    });
    shortcuts = shortcuts.map(function (shortcut) {
      return "ctrl+".concat(shortcut);
    });
  }

  return {
    osx: macShortcuts,
    windows: shortcuts,
    linux: shortcuts,
    other: shortcuts,
    help: help
  };
};

var refreshShortcut = getShortcuts('r', {
  modifiers: 'alt',
  help: 'Refresh workpad'
});
var previousPageShortcut = getShortcuts('[', {
  modifiers: 'alt',
  help: 'Go to previous page'
});
var nextPageShortcut = getShortcuts(']', {
  modifiers: 'alt',
  help: 'Go to next page'
});
var fullscreenShortcut = getShortcuts(['f', 'p'], {
  modifiers: 'alt',
  help: 'Enter presentation mode'
});
var keymap = {
  ELEMENT: {
    displayName: 'Element controls',
    CUT: getShortcuts('x', {
      modifiers: 'ctrl',
      help: 'Cut'
    }),
    COPY: getShortcuts('c', {
      modifiers: 'ctrl',
      help: 'Copy'
    }),
    PASTE: getShortcuts('v', {
      modifiers: 'ctrl',
      help: 'Paste'
    }),
    CLONE: getShortcuts('d', {
      modifiers: 'ctrl',
      help: 'Clone'
    }),
    DELETE: getShortcuts(['del', 'backspace'], {
      help: 'Delete'
    }),
    BRING_FORWARD: getShortcuts('up', {
      modifiers: 'ctrl',
      help: 'Bring to front'
    }),
    BRING_TO_FRONT: getShortcuts('up', {
      modifiers: ['ctrl', 'shift'],
      help: 'Bring forward'
    }),
    SEND_BACKWARD: getShortcuts('down', {
      modifiers: 'ctrl',
      help: 'Send backward'
    }),
    SEND_TO_BACK: getShortcuts('down', {
      modifiers: ['ctrl', 'shift'],
      help: 'Send to back'
    }),
    GROUP: getShortcuts('g', {
      help: 'Group'
    }),
    UNGROUP: getShortcuts('u', {
      help: 'Ungroup'
    }),
    SHIFT_UP: getShortcuts('up', {
      help: "Shift up by ".concat(_constants.ELEMENT_SHIFT_OFFSET, "px")
    }),
    SHIFT_DOWN: getShortcuts('down', {
      help: "Shift down by ".concat(_constants.ELEMENT_SHIFT_OFFSET, "px")
    }),
    SHIFT_LEFT: getShortcuts('left', {
      help: "Shift left by ".concat(_constants.ELEMENT_SHIFT_OFFSET, "px")
    }),
    SHIFT_RIGHT: getShortcuts('right', {
      help: "Shift right by ".concat(_constants.ELEMENT_SHIFT_OFFSET, "px")
    }),
    NUDGE_UP: getShortcuts('up', {
      modifiers: ['shift'],
      help: "Shift up by ".concat(_constants.ELEMENT_NUDGE_OFFSET, "px")
    }),
    NUDGE_DOWN: getShortcuts('down', {
      modifiers: ['shift'],
      help: "Shift down by ".concat(_constants.ELEMENT_NUDGE_OFFSET, "px")
    }),
    NUDGE_LEFT: getShortcuts('left', {
      modifiers: ['shift'],
      help: "Shift left by ".concat(_constants.ELEMENT_NUDGE_OFFSET, "px")
    }),
    NUDGE_RIGHT: getShortcuts('right', {
      modifiers: ['shift'],
      help: "Shift right by ".concat(_constants.ELEMENT_NUDGE_OFFSET, "px")
    })
  },
  EXPRESSION: {
    displayName: 'Expression controls',
    RUN: getShortcuts('enter', {
      modifiers: 'ctrl',
      help: 'Run whole expression'
    })
  },
  EDITOR: {
    displayName: 'Editor controls',
    // added for documentation purposes, not handled by `react-shortcuts`
    MULTISELECT: getShortcuts('click', {
      modifiers: 'shift',
      help: 'Select multiple elements'
    }),
    // added for documentation purposes, not handled by `react-shortcuts`
    RESIZE_FROM_CENTER: getShortcuts('drag', {
      modifiers: 'alt',
      help: 'Resize from center'
    }),
    // added for documentation purposes, not handled by `react-shortcuts`
    IGNORE_SNAP: getShortcuts('drag', {
      modifiers: 'ctrl',
      help: 'Move, resize, and rotate without snapping'
    }),
    // added for documentation purposes, not handled by `react-shortcuts`
    SELECT_BEHIND: getShortcuts('click', {
      modifiers: 'ctrl',
      help: 'Select element below'
    }),
    UNDO: getShortcuts('z', {
      modifiers: 'ctrl',
      help: 'Undo last action'
    }),
    REDO: getShortcuts('z', {
      modifiers: ['ctrl', 'shift'],
      help: 'Redo last action'
    }),
    PREV: previousPageShortcut,
    NEXT: nextPageShortcut,
    EDITING: getShortcuts('e', {
      modifiers: 'alt',
      help: 'Toggle edit mode'
    }),
    GRID: getShortcuts('g', {
      modifiers: 'alt',
      help: 'Show grid'
    }),
    REFRESH: refreshShortcut,
    ZOOM_IN: getShortcuts('plus', {
      modifiers: ['ctrl', 'alt'],
      help: 'Zoom in'
    }),
    ZOOM_OUT: getShortcuts('minus', {
      modifiers: ['ctrl', 'alt'],
      help: 'Zoom out'
    }),
    ZOOM_RESET: getShortcuts('[', {
      modifiers: ['ctrl', 'alt'],
      help: 'Reset zoom to 100%'
    }),
    FULLSCREEN: fullscreenShortcut
  },
  PRESENTATION: {
    displayName: 'Presentation controls',
    FULLSCREEN: fullscreenShortcut,
    FULLSCREEN_EXIT: getShortcuts('esc', {
      help: 'Exit presentation mode'
    }),
    PREV: (0, _lodash.mapValues)(previousPageShortcut, function (osShortcuts, key) {
      return (// adds 'backspace' and 'left' to list of shortcuts per OS
        key === 'help' ? osShortcuts : osShortcuts.concat(['backspace', 'left'])
      );
    }),
    NEXT: (0, _lodash.mapValues)(nextPageShortcut, function (osShortcuts, key) {
      return (// adds 'space' and 'right' to list of shortcuts per OS
        key === 'help' ? osShortcuts : osShortcuts.concat(['space', 'right'])
      );
    }),
    REFRESH: refreshShortcut,
    PAGE_CYCLE_TOGGLE: getShortcuts('p', {
      help: 'Toggle page cycling'
    })
  }
};
exports.keymap = keymap;