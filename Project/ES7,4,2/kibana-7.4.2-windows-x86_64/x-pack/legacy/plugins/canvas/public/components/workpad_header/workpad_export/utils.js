"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPdfUrl = getPdfUrl;
exports.createPdf = createPdf;

var _risonNode = _interopRequireDefault(require("rison-node"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _query_string = require("ui/utils/query_string");

var _fetch = require("../../../../common/lib/fetch");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore Untyped local.
// type of the desired pdf output (print or preserve_layout)
var PDF_LAYOUT_TYPE = 'preserve_layout';

function getPdfUrl(_ref, _ref2) {
  var id = _ref.id,
      title = _ref.name,
      width = _ref.width,
      height = _ref.height;
  var pageCount = _ref2.pageCount;

  var reportingEntry = _chrome.default.addBasePath('/api/reporting/generate');

  var canvasEntry = '/app/canvas#'; // The viewport in Reporting by specifying the dimensions. In order for things to work,
  // we need a viewport that will include all of the pages in the workpad. The viewport
  // also needs to include any offset values from the 0,0 position, otherwise the cropped
  // screenshot that Reporting takes will be off the mark. Reporting will take a screenshot
  // of the entire viewport and then crop it down to the element that was asked for.
  // NOTE: while the above is true, the scaling seems to be broken. The export screen draws
  // pages at the 0,0 point, so the offset isn't currently required to get the correct
  // viewport size.
  // build a list of all page urls for exporting, they are captured one at a time

  var workpadUrls = [];

  for (var i = 1; i <= pageCount; i++) {
    workpadUrls.push(_risonNode.default.encode("".concat(canvasEntry, "/export/workpad/pdf/").concat(id, "/page/").concat(i)));
  }

  var jobParams = {
    browserTimezone: 'America/Phoenix',
    // TODO: get browser timezone, or Kibana setting?
    layout: {
      dimensions: {
        width: width,
        height: height
      },
      id: PDF_LAYOUT_TYPE
    },
    objectType: 'canvas workpad',
    relativeUrls: workpadUrls,
    title: title
  };
  return "".concat(reportingEntry, "/printablePdf?").concat(_query_string.QueryString.param('jobParams', _risonNode.default.encode(jobParams)));
}

function createPdf() {
  var createPdfUri = getPdfUrl.apply(void 0, arguments);
  return _fetch.fetch.post(createPdfUri);
}