"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MultiElementSettings = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MultiElementSettings = function MultiElementSettings() {
  return _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement("p", null, "Multiple elements are currently selected."), _react.default.createElement("p", null, "Deselect these elements to edit their individual settings, press (G) to group them, or save this selection as a new element to re-use it throughout your workpad."));
};

exports.MultiElementSettings = MultiElementSettings;