"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WorkpadExport = void 0;

var _reactRedux = require("react-redux");

var _recompose = require("recompose");

var _job_completion_notifications = require("../../../../../reporting/public/lib/job_completion_notifications");

var _workpad = require("../../../state/selectors/workpad");

var _app = require("../../../state/selectors/app");

var _notify = require("../../../lib/notify");

var _get_window = require("../../../lib/get_window");

var _download_workpad = require("../../../lib/download_workpad");

var _workpad_export = require("./workpad_export");

var _utils = require("./utils");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore Untyped local
// @ts-ignore Untyped local
// @ts-ignore Untyped local
// @ts-ignore Untyped local
var mapStateToProps = function mapStateToProps(state) {
  return {
    workpad: (0, _workpad.getWorkpad)(state),
    pageCount: (0, _workpad.getPages)(state).length,
    enabled: (0, _app.getReportingBrowserType)(state) === 'chromium'
  };
};

var getAbsoluteUrl = function getAbsoluteUrl(path) {
  var _getWindow = (0, _get_window.getWindow)(),
      location = _getWindow.location;

  if (!location) {
    return path;
  } // fallback for mocked window object


  var protocol = location.protocol,
      hostname = location.hostname,
      port = location.port;
  return "".concat(protocol, "//").concat(hostname, ":").concat(port).concat(path);
};

var WorkpadExport = (0, _recompose.compose)((0, _reactRedux.connect)(mapStateToProps), (0, _recompose.withProps)(function (_ref) {
  var workpad = _ref.workpad,
      pageCount = _ref.pageCount,
      enabled = _ref.enabled;
  return {
    enabled: enabled,
    getExportUrl: function getExportUrl(type) {
      if (type === 'pdf') {
        return getAbsoluteUrl((0, _utils.getPdfUrl)(workpad, {
          pageCount: pageCount
        }));
      }

      throw new Error("Unknown export type: ".concat(type));
    },
    onCopy: function onCopy(type) {
      switch (type) {
        case 'pdf':
          _notify.notify.info('The PDF generation URL was copied to your clipboard.');

          break;

        case 'reportingConfig':
          _notify.notify.info("Copied reporting configuration to clipboard");

          break;

        default:
          throw new Error("Unknown export type: ".concat(type));
      }
    },
    onExport: function onExport(type) {
      switch (type) {
        case 'pdf':
          return (0, _utils.createPdf)(workpad, {
            pageCount: pageCount
          }).then(function (_ref2) {
            var data = _ref2.data;

            _notify.notify.info('Exporting PDF. You can track the progress in Management.', {
              title: "PDF export of workpad '".concat(workpad.name, "'")
            }); // register the job so a completion notification shows up when it's ready


            _job_completion_notifications.jobCompletionNotifications.add(data.job.id);
          }).catch(function (err) {
            _notify.notify.error(err, {
              title: "Failed to create PDF for '".concat(workpad.name, "'")
            });
          });

        case 'json':
          (0, _download_workpad.downloadWorkpad)(workpad.id);
          break;

        default:
          throw new Error("Unknown export type: ".concat(type));
      }
    }
  };
}))(_workpad_export.WorkpadExport);
exports.WorkpadExport = WorkpadExport;