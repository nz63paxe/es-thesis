"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WorkpadExport = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _eui = require("@elastic/eui");

var _popover = require("../../popover");

var _disabled_panel = require("./disabled_panel");

var _pdf_panel = require("./pdf_panel");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore Untyped local

/**
 * The Menu for Exporting a Workpad from Canvas.
 */
var WorkpadExport = function WorkpadExport(_ref) {
  var enabled = _ref.enabled,
      _onCopy = _ref.onCopy,
      _onExport = _ref.onExport,
      getExportUrl = _ref.getExportUrl;

  // TODO: Fix all of this magic from EUI; this code is boilerplate from
  // EUI examples and isn't easily typed.
  var flattenPanelTree = function flattenPanelTree(tree) {
    var array = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
    array.push(tree);

    if (tree.items) {
      tree.items.forEach(function (item) {
        var panel = item.panel;

        if (panel) {
          flattenPanelTree(panel, array);
          item.panel = panel.id;
        }
      });
    }

    return array;
  };

  var getPDFPanel = function getPDFPanel(closePopover) {
    return _react.default.createElement(_pdf_panel.PDFPanel, {
      pdfURL: getExportUrl('pdf'),
      onExport: function onExport() {
        _onExport('pdf');

        closePopover();
      },
      onCopy: function onCopy() {
        _onCopy('pdf');

        closePopover();
      }
    });
  };

  var getPanelTree = function getPanelTree(closePopover) {
    return {
      id: 0,
      title: 'Share this workpad',
      items: [{
        name: 'Download as JSON',
        icon: _react.default.createElement(_eui.EuiIcon, {
          type: "exportAction",
          size: "m"
        }),
        onClick: function onClick() {
          _onExport('json');

          closePopover();
        }
      }, {
        name: 'PDF reports',
        icon: 'document',
        panel: {
          id: 1,
          title: 'PDF reports',
          content: enabled ? getPDFPanel(closePopover) : _react.default.createElement(_disabled_panel.DisabledPanel, {
            onCopy: function onCopy() {
              _onCopy('reportingConfig');

              closePopover();
            }
          })
        }
      }]
    };
  };

  var exportControl = function exportControl(togglePopover) {
    return _react.default.createElement(_eui.EuiButtonIcon, {
      iconType: "share",
      "aria-label": "Share this workpad",
      onClick: togglePopover
    });
  };

  return _react.default.createElement(_popover.Popover, {
    button: exportControl,
    panelPaddingSize: "none",
    tooltip: "Share workpad",
    tooltipPosition: "bottom"
  }, function (_ref2) {
    var closePopover = _ref2.closePopover;
    return _react.default.createElement(_eui.EuiContextMenu, {
      initialPanelId: 0,
      panels: flattenPanelTree(getPanelTree(closePopover))
    });
  });
};

exports.WorkpadExport = WorkpadExport;
WorkpadExport.propTypes = {
  enabled: _propTypes.default.bool.isRequired,
  onCopy: _propTypes.default.func.isRequired,
  onExport: _propTypes.default.func.isRequired,
  getExportUrl: _propTypes.default.func.isRequired
};