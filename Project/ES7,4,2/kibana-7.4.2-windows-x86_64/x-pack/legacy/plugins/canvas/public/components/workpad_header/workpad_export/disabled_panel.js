"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DisabledPanel = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _clipboard = require("../../clipboard");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var REPORTING_CONFIG = "xpack.reporting:\n  enabled: true\n  capture.browser.type: chromium";

/**
 * A panel to display within the Export menu when reporting is disabled.
 */
var DisabledPanel = function DisabledPanel(_ref) {
  var onCopy = _ref.onCopy;
  return _react.default.createElement("div", {
    className: "canvasWorkpadExport__panelContent"
  }, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement("p", null, "Export to PDF is disabled. You must configure reporting to use the Chromium browser. Add this to your ", _react.default.createElement(_eui.EuiCode, null, "kibana.yml"), " file.")), _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_clipboard.Clipboard, {
    content: REPORTING_CONFIG,
    onCopy: onCopy
  }, _react.default.createElement(_eui.EuiCodeBlock, {
    className: "canvasWorkpadExport__reportingConfig",
    paddingSize: "s",
    fontSize: "s",
    language: "yml"
  }, REPORTING_CONFIG)));
};

exports.DisabledPanel = DisabledPanel;