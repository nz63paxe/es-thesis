"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ElementSettings = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactRedux = require("react-redux");

var _workpad = require("../../../state/selectors/workpad");

var _element_settings = require("./element_settings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore unconverted local file
var mapStateToProps = function mapStateToProps(state, _ref) {
  var selectedElementId = _ref.selectedElementId;
  return {
    element: (0, _workpad.getElementById)(state, selectedElementId, (0, _workpad.getSelectedPage)(state))
  };
};

var ElementSettings = (0, _reactRedux.connect)(mapStateToProps)(_element_settings.ElementSettings);
exports.ElementSettings = ElementSettings;
ElementSettings.propTypes = {
  selectedElementId: _propTypes.default.string.isRequired
};