"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Editor", {
  enumerable: true,
  get: function get() {
    return _editor.Editor;
  }
});

var _editor = require("./editor");