"use strict";

var _addonActions = require("@storybook/addon-actions");

var _react = require("@storybook/react");

var _react2 = _interopRequireDefault(require("react"));

var monacoEditor = _interopRequireWildcard(require("monaco-editor/esm/vs/editor/editor.api"));

var _editor2 = require("../editor");

require("monaco-editor/esm/vs/basic-languages/html/html.contribution.js");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// A sample language definition with a few example tokens
var simpleLogLang = {
  tokenizer: {
    root: [[/\[error.*/, 'constant'], [/\[notice.*/, 'variable'], [/\[info.*/, 'string'], [/\[[a-zA-Z 0-9:]+\]/, 'tag']]
  }
};
monacoEditor.languages.register({
  id: 'loglang'
});
monacoEditor.languages.setMonarchTokensProvider('loglang', simpleLogLang);
var logs = "\n[Sun Mar 7 20:54:27 2004] [notice] [client xx.xx.xx.xx] This is a notice!\n[Sun Mar 7 20:58:27 2004] [info] [client xx.xx.xx.xx] (104)Connection reset by peer: client stopped connection before send body completed\n[Sun Mar 7 21:16:17 2004] [error] [client xx.xx.xx.xx] File does not exist: /home/httpd/twiki/view/Main/WebHome\n";
var html = "<section>\n  <span>Hello World!</span>\n</section>";
(0, _react.storiesOf)('components/Editor', module).add('default', function () {
  return _react2.default.createElement("div", null, _react2.default.createElement(_editor2.Editor, {
    languageId: "plaintext",
    height: 250,
    value: "Hello!",
    onChange: (0, _addonActions.action)('onChange')
  }));
}).add('html', function () {
  return _react2.default.createElement("div", null, _react2.default.createElement(_editor2.Editor, {
    languageId: "html",
    height: 250,
    value: html,
    onChange: (0, _addonActions.action)('onChange')
  }));
}).add('custom log language', function () {
  return _react2.default.createElement("div", null, _react2.default.createElement(_editor2.Editor, {
    languageId: "loglang",
    height: 250,
    value: logs,
    onChange: (0, _addonActions.action)('onChange')
  }));
});