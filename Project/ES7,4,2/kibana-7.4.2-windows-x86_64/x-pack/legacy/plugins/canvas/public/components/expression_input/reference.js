"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFunctionReferenceStr = getFunctionReferenceStr;
exports.getArgReferenceStr = getArgReferenceStr;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Given a function definition, this function returns a markdown string
 * that includes the context the function accepts, what the function returns
 * as well as the general help/documentation text associated with the function
 */
function getFunctionReferenceStr(fnDef) {
  var help = fnDef.help,
      context = fnDef.context,
      type = fnDef.type;
  var doc = "**Accepts**: ".concat(context && context.types ? context.types.join(' | ') : 'null', ", **Returns**: ").concat(type ? type : 'null', "\n\n\n").concat(help);
  return doc;
}
/**
 * Given an argument defintion, this function returns a markdown string
 * that includes the aliases of the argument, types accepted for the argument,
 * the default value of the argument, whether or not its required, and
 * the general help/documentation text associated with the argument
 */


function getArgReferenceStr(argDef) {
  var aliases = argDef.aliases,
      types = argDef.types,
      def = argDef.default,
      required = argDef.required,
      help = argDef.help;
  var secondLineArr = [];

  if (def != null) {
    secondLineArr.push("**Default**: ".concat(def));
  }

  if (aliases && aliases.length) {
    secondLineArr.push("**Aliases**: ".concat(aliases.join(' | ')));
  }

  var ref = "**Types**: ".concat(types && types.length ? types.join(' | ') : 'null', ",\n **Required**: ").concat(String(Boolean(required)), "\n\n\n").concat(secondLineArr.join(', '), "\n\n\n").concat(help);
  return ref;
}