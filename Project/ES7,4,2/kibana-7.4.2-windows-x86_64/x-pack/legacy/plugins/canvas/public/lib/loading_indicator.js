"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadingIndicator = void 0;

var _chrome = require("ui/chrome");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var isActive = false;
var loadingIndicator = {
  show: function show() {
    if (!isActive) {
      _chrome.loadingCount.increment();

      isActive = true;
    }
  },
  hide: function hide() {
    if (isActive) {
      _chrome.loadingCount.decrement();

      isActive = false;
    }
  }
};
exports.loadingIndicator = loadingIndicator;