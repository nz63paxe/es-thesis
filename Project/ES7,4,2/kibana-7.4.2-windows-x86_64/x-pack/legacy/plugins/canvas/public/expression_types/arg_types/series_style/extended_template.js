"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExtendedTemplate = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _eui = require("@elastic/eui");

var _objectPathImmutable = _interopRequireDefault(require("object-path-immutable"));

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var set = _objectPathImmutable.default.set,
    del = _objectPathImmutable.default.del;

var ExtendedTemplate = function ExtendedTemplate(props) {
  var typeInstance = props.typeInstance,
      onValueChange = props.onValueChange,
      labels = props.labels,
      argValue = props.argValue;
  var chain = (0, _lodash.get)(argValue, 'chain.0', {});
  var chainArgs = (0, _lodash.get)(chain, 'arguments', {});
  var selectedSeries = (0, _lodash.get)(chainArgs, 'label.0', '');
  var name = '';

  if (typeInstance) {
    name = typeInstance.name;
  }

  var fields = (0, _lodash.get)(typeInstance, 'options.include', []);
  var hasPropFields = fields.some(function (field) {
    return ['lines', 'bars', 'points'].indexOf(field) !== -1;
  });

  var handleChange = function handleChange(argName, ev) {
    var fn = ev.target.value === '' ? del : set;
    var newValue = fn(argValue, "chain.0.arguments.".concat(argName), [ev.target.value]);
    return onValueChange(newValue);
  }; // TODO: add fill and stack options
  // TODO: add label name auto-complete


  var values = [{
    value: 0,
    text: 'None'
  }, {
    value: 1,
    text: '1'
  }, {
    value: 2,
    text: '2'
  }, {
    value: 3,
    text: '3'
  }, {
    value: 4,
    text: '4'
  }, {
    value: 5,
    text: '5'
  }];
  var labelOptions = [{
    value: '',
    text: 'Select Series'
  }];
  labels.sort().forEach(function (val) {
    return labelOptions.push({
      value: val,
      text: val
    });
  });
  return _react.default.createElement("div", null, name !== 'defaultStyle' && _react.default.createElement(_eui.EuiFormRow, {
    label: "Series Identifier",
    compressed: true
  }, _react.default.createElement(_eui.EuiSelect, {
    value: selectedSeries,
    options: labelOptions,
    onChange: function onChange(ev) {
      return handleChange('label', ev);
    }
  })), hasPropFields && _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s"
  }, fields.includes('lines') && _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFormRow, {
    label: "Line",
    compressed: true
  }, _react.default.createElement(_eui.EuiSelect, {
    value: (0, _lodash.get)(chainArgs, 'lines.0', 0),
    options: values,
    onChange: function onChange(ev) {
      return handleChange('lines', ev);
    }
  }))), fields.includes('bars') && _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFormRow, {
    label: "Bar",
    compressed: true
  }, _react.default.createElement(_eui.EuiSelect, {
    value: (0, _lodash.get)(chainArgs, 'bars.0', 0),
    options: values,
    onChange: function onChange(ev) {
      return handleChange('bars', ev);
    }
  }))), fields.includes('points') && _react.default.createElement(_eui.EuiFlexItem, null, _react.default.createElement(_eui.EuiFormRow, {
    label: "Point",
    compressed: true
  }, _react.default.createElement(_eui.EuiSelect, {
    value: (0, _lodash.get)(chainArgs, 'points.0', 0),
    options: values,
    onChange: function onChange(ev) {
      return handleChange('points', ev);
    }
  })))));
};

exports.ExtendedTemplate = ExtendedTemplate;
ExtendedTemplate.displayName = 'SeriesStyleArgAdvancedInput';
ExtendedTemplate.propTypes = {
  onValueChange: _propTypes.default.func.isRequired,
  argValue: _propTypes.default.any.isRequired,
  typeInstance: _propTypes.default.object,
  labels: _propTypes.default.array.isRequired
};