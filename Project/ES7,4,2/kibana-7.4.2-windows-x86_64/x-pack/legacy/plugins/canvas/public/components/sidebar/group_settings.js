"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GroupSettings = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var GroupSettings = function GroupSettings() {
  return _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement("p", null, "Ungroup (U) to edit individual element settings."), _react.default.createElement("p", null, "Save this group as a new element to re-use it throughout your workpad."));
};

exports.GroupSettings = GroupSettings;