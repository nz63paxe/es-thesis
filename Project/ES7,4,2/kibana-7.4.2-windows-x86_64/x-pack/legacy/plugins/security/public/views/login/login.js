"use strict";

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _parse_next = require("plugins/security/lib/parse_next");

var _components = require("plugins/security/views/login/components");

var _login = _interopRequireDefault(require("plugins/security/views/login/login.html"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _i18n2 = require("ui/i18n");

var _url = require("url");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var messageMap = {
  SESSION_EXPIRED: _i18n.i18n.translate('xpack.security.login.sessionExpiredDescription', {
    defaultMessage: 'Your session has timed out. Please log in again.'
  }),
  LOGGED_OUT: _i18n.i18n.translate('xpack.security.login.loggedOutDescription', {
    defaultMessage: 'You have logged out of Kibana.'
  })
};

_chrome.default.setVisible(false).setRootTemplate(_login.default).setRootController('login', function ($scope, $http, $window, secureCookies, loginState) {
  var basePath = _chrome.default.getBasePath();

  var next = (0, _parse_next.parseNext)($window.location.href, basePath);
  var isSecure = !!$window.location.protocol.match(/^https/);
  $scope.$$postDigest(function () {
    var domNode = document.getElementById('reactLoginRoot');
    var msgQueryParam = (0, _url.parse)($window.location.href, true).query.msg || '';
    (0, _reactDom.render)(_react.default.createElement(_i18n2.I18nContext, null, _react.default.createElement(_components.LoginPage, {
      http: $http,
      window: $window,
      infoMessage: (0, _lodash.get)(messageMap, msgQueryParam),
      loginState: loginState,
      isSecureConnection: isSecure,
      requiresSecureConnection: secureCookies,
      next: next
    })), domNode);
  });
});