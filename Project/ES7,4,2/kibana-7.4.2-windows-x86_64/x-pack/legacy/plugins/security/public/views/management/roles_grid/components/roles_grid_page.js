"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RolesGridPage = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = _interopRequireDefault(require("lodash"));

var _react2 = _interopRequireWildcard(require("react"));

var _notify = require("ui/notify");

var _role_utils = require("../../../../lib/role_utils");

var _roles_api = require("../../../../lib/roles_api");

var _confirm_delete = require("./confirm_delete");

var _permission_denied = require("./permission_denied");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getRoleManagementHref = function getRoleManagementHref(action, roleName) {
  return "#/management/security/roles/".concat(action).concat(roleName ? "/".concat(roleName) : '');
};

var RolesGridPageUI =
/*#__PURE__*/
function (_Component) {
  _inherits(RolesGridPageUI, _Component);

  function RolesGridPageUI(props) {
    var _this;

    _classCallCheck(this, RolesGridPageUI);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(RolesGridPageUI).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "getPageContent", function () {
      var roles = _this.state.roles;
      var intl = _this.props.intl;
      return _react2.default.createElement(_eui.EuiPageContent, null, _react2.default.createElement(_eui.EuiPageContentHeader, null, _react2.default.createElement(_eui.EuiPageContentHeaderSection, null, _react2.default.createElement(_eui.EuiTitle, null, _react2.default.createElement("h2", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.roles.roleTitle",
        defaultMessage: "Roles"
      }))), _react2.default.createElement(_eui.EuiText, {
        color: "subdued",
        size: "s"
      }, _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.roles.subtitle",
        defaultMessage: "Apply roles to groups of users and manage permissions across the stack."
      })))), _react2.default.createElement(_eui.EuiPageContentHeaderSection, null, _react2.default.createElement(_eui.EuiButton, {
        "data-test-subj": "createRoleButton",
        href: getRoleManagementHref('edit')
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.roles.createRoleButtonLabel",
        defaultMessage: "Create role"
      })))), _react2.default.createElement(_eui.EuiPageContentBody, null, _this.state.showDeleteConfirmation ? _react2.default.createElement(_confirm_delete.ConfirmDelete, {
        onCancel: _this.onCancelDelete,
        rolesToDelete: _this.state.selection.map(function (role) {
          return role.name;
        }),
        callback: _this.handleDelete
      }) : null, // @ts-ignore missing rowProps typedef
      _react2.default.createElement(_eui.EuiInMemoryTable, {
        itemId: "name",
        responsive: false,
        columns: _this.getColumnConfig(intl),
        hasActions: true,
        selection: {
          itemId: 'name',
          selectable: function selectable(role) {
            return !role.metadata || !role.metadata._reserved;
          },
          selectableMessage: function selectableMessage(selectable) {
            return !selectable ? 'Role is reserved' : undefined;
          },
          onSelectionChange: function onSelectionChange(selection) {
            return _this.setState({
              selection: selection
            });
          }
        },
        pagination: {
          initialPageSize: 20,
          pageSizeOptions: [10, 20, 30, 50, 100]
        },
        items: _this.getVisibleRoles(),
        loading: roles.length === 0,
        search: {
          toolsLeft: _this.renderToolsLeft(),
          box: {
            incremental: true
          },
          onChange: function onChange(query) {
            _this.setState({
              filter: query.queryText
            });
          }
        },
        sorting: {
          sort: {
            field: 'name',
            direction: 'asc'
          }
        } // @ts-ignore missing rowProps typedef
        ,
        rowProps: function rowProps() {
          return {
            'data-test-subj': 'roleRow'
          };
        },
        isSelectable: true
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "getColumnConfig", function (intl) {
      var reservedRoleDesc = intl.formatMessage({
        id: 'xpack.security.management.roles.reservedColumnDescription',
        defaultMessage: 'Reserved roles are built-in and cannot be edited or removed.'
      });
      return [{
        field: 'name',
        name: intl.formatMessage({
          id: 'xpack.security.management.roles.nameColumnName',
          defaultMessage: 'Role'
        }),
        sortable: true,
        truncateText: true,
        render: function render(name, record) {
          return _react2.default.createElement(_eui.EuiText, {
            color: "subdued",
            size: "s"
          }, _react2.default.createElement(_eui.EuiLink, {
            "data-test-subj": "roleRowName",
            href: getRoleManagementHref('edit', name)
          }, name), !(0, _role_utils.isRoleEnabled)(record) && _react2.default.createElement(_react.FormattedMessage, {
            id: "xpack.security.management.roles.disabledTooltip",
            defaultMessage: " (disabled)"
          }));
        }
      }, {
        field: 'metadata._reserved',
        name: _react2.default.createElement(_eui.EuiToolTip, {
          content: reservedRoleDesc
        }, _react2.default.createElement("span", {
          className: "rolesGridPage__reservedRoleTooltip"
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.roles.reservedColumnName",
          defaultMessage: "Reserved"
        }), _react2.default.createElement(_eui.EuiIcon, {
          size: "s",
          color: "subdued",
          type: "questionInCircle",
          className: "eui-alignTop"
        }))),
        sortable: function sortable(_ref) {
          var metadata = _ref.metadata;
          return metadata && metadata._reserved || false;
        },
        dataType: 'boolean',
        align: 'right',
        description: reservedRoleDesc,
        render: function render(reserved) {
          var label = intl.formatMessage({
            id: 'xpack.security.management.roles.reservedRoleIconLabel',
            defaultMessage: 'Reserved role'
          });
          return reserved ? _react2.default.createElement("span", {
            title: label
          }, _react2.default.createElement(_eui.EuiIcon, {
            "aria-label": label,
            "data-test-subj": "reservedRole",
            type: "check"
          })) : null;
        }
      }, {
        name: intl.formatMessage({
          id: 'xpack.security.management.roles.actionsColumnName',
          defaultMessage: 'Actions'
        }),
        width: '150px',
        actions: [{
          available: function available(role) {
            return !(0, _role_utils.isReadOnlyRole)(role);
          },
          render: function render(role) {
            var title = intl.formatMessage({
              id: 'xpack.security.management.roles.editRoleActionName',
              defaultMessage: "Edit {roleName}"
            }, {
              roleName: role.name
            });
            return _react2.default.createElement(_eui.EuiButtonIcon, {
              "aria-label": title,
              "data-test-subj": "edit-role-action-".concat(role.name),
              title: title,
              color: 'primary',
              iconType: 'pencil',
              href: getRoleManagementHref('edit', role.name)
            });
          }
        }, {
          available: function available(role) {
            return !(0, _role_utils.isReservedRole)(role);
          },
          render: function render(role) {
            var title = intl.formatMessage({
              id: 'xpack.security.management.roles.cloneRoleActionName',
              defaultMessage: "Clone {roleName}"
            }, {
              roleName: role.name
            });
            return _react2.default.createElement(_eui.EuiButtonIcon, {
              "aria-label": title,
              "data-test-subj": "clone-role-action-".concat(role.name),
              title: title,
              color: 'primary',
              iconType: 'copy',
              href: getRoleManagementHref('clone', role.name)
            });
          }
        }]
      }];
    });

    _defineProperty(_assertThisInitialized(_this), "getVisibleRoles", function () {
      var _this$state = _this.state,
          roles = _this$state.roles,
          filter = _this$state.filter;
      return filter ? roles.filter(function (_ref2) {
        var name = _ref2.name;
        var normalized = "".concat(name).toLowerCase();
        var normalizedQuery = filter.toLowerCase();
        return normalized.indexOf(normalizedQuery) !== -1;
      }) : roles;
    });

    _defineProperty(_assertThisInitialized(_this), "handleDelete", function () {
      _this.setState({
        selection: [],
        showDeleteConfirmation: false
      });

      _this.loadRoles();
    });

    _defineProperty(_assertThisInitialized(_this), "onCancelDelete", function () {
      _this.setState({
        showDeleteConfirmation: false
      });
    });

    _this.state = {
      roles: [],
      selection: [],
      filter: '',
      showDeleteConfirmation: false,
      permissionDenied: false
    };
    return _this;
  }

  _createClass(RolesGridPageUI, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadRoles();
    }
  }, {
    key: "render",
    value: function render() {
      var permissionDenied = this.state.permissionDenied;
      return permissionDenied ? _react2.default.createElement(_permission_denied.PermissionDenied, null) : this.getPageContent();
    }
  }, {
    key: "loadRoles",
    value: function () {
      var _loadRoles = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var roles;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _roles_api.RolesApi.getRoles();

              case 3:
                roles = _context.sent;
                this.setState({
                  roles: roles
                });
                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);

                if (_lodash.default.get(_context.t0, 'body.statusCode') === 403) {
                  this.setState({
                    permissionDenied: true
                  });
                } else {
                  _notify.toastNotifications.addDanger(this.props.intl.formatMessage({
                    id: 'xpack.security.management.roles.fetchingRolesErrorMessage',
                    defaultMessage: 'Error fetching roles: {message}'
                  }, {
                    message: _lodash.default.get(_context.t0, 'body.message', '')
                  }));
                }

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7]]);
      }));

      function loadRoles() {
        return _loadRoles.apply(this, arguments);
      }

      return loadRoles;
    }()
  }, {
    key: "renderToolsLeft",
    value: function renderToolsLeft() {
      var _this2 = this;

      var selection = this.state.selection;

      if (selection.length === 0) {
        return;
      }

      var numSelected = selection.length;
      return _react2.default.createElement(_eui.EuiButton, {
        "data-test-subj": "deleteRoleButton",
        color: "danger",
        onClick: function onClick() {
          return _this2.setState({
            showDeleteConfirmation: true
          });
        }
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.roles.deleteSelectedRolesButtonLabel",
        defaultMessage: "Delete {numSelected} role{numSelected, plural, one { } other {s}}",
        values: {
          numSelected: numSelected
        }
      }));
    }
  }]);

  return RolesGridPageUI;
}(_react2.Component);

var RolesGridPage = (0, _react.injectI18n)(RolesGridPageUI);
exports.RolesGridPage = RolesGridPage;