"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaBasePrivilegeCalculator = void 0;

var _privilege_calculator_utils = require("../../../common/privilege_calculator_utils");

var _constants = require("../../views/management/edit_role/lib/constants");

var _privilege_utils = require("../privilege_utils");

var _kibana_privilege_calculator_types = require("./kibana_privilege_calculator_types");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var KibanaBasePrivilegeCalculator =
/*#__PURE__*/
function () {
  function KibanaBasePrivilegeCalculator(kibanaPrivileges, globalPrivilege, assignedGlobalBaseActions) {
    _classCallCheck(this, KibanaBasePrivilegeCalculator);

    this.kibanaPrivileges = kibanaPrivileges;
    this.globalPrivilege = globalPrivilege;
    this.assignedGlobalBaseActions = assignedGlobalBaseActions;
  }

  _createClass(KibanaBasePrivilegeCalculator, [{
    key: "getMostPermissiveBasePrivilege",
    value: function getMostPermissiveBasePrivilege(privilegeSpec, ignoreAssigned) {
      var assignedPrivilege = privilegeSpec.base[0] || _constants.NO_PRIVILEGE_VALUE; // If this is the global privilege definition, then there is nothing to supercede it.

      if ((0, _privilege_utils.isGlobalPrivilegeDefinition)(privilegeSpec)) {
        if (assignedPrivilege === _constants.NO_PRIVILEGE_VALUE || ignoreAssigned) {
          return {
            actualPrivilege: _constants.NO_PRIVILEGE_VALUE,
            actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE,
            isDirectlyAssigned: true
          };
        }

        return {
          actualPrivilege: assignedPrivilege,
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE,
          isDirectlyAssigned: true
        };
      } // Otherwise, check to see if the global privilege supercedes this one.


      var baseActions = _toConsumableArray(this.kibanaPrivileges.getSpacesPrivileges().getActions(assignedPrivilege));

      var globalSupercedes = this.hasAssignedGlobalBasePrivilege() && ((0, _privilege_calculator_utils.compareActions)(this.assignedGlobalBaseActions, baseActions) < 0 || ignoreAssigned);

      if (globalSupercedes) {
        var wasDirectlyAssigned = !ignoreAssigned && baseActions.length > 0;
        return _objectSpread({
          actualPrivilege: this.globalPrivilege.base[0],
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE,
          isDirectlyAssigned: false
        }, this.buildSupercededFields(wasDirectlyAssigned, assignedPrivilege, _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE));
      }

      if (!ignoreAssigned) {
        return {
          actualPrivilege: assignedPrivilege,
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE,
          isDirectlyAssigned: true
        };
      }

      return {
        actualPrivilege: _constants.NO_PRIVILEGE_VALUE,
        actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE,
        isDirectlyAssigned: true
      };
    }
  }, {
    key: "hasAssignedGlobalBasePrivilege",
    value: function hasAssignedGlobalBasePrivilege() {
      return this.assignedGlobalBaseActions.length > 0;
    }
  }, {
    key: "buildSupercededFields",
    value: function buildSupercededFields(isSuperceding, supersededPrivilege, supersededPrivilegeSource) {
      if (!isSuperceding) {
        return {};
      }

      return {
        supersededPrivilege: supersededPrivilege,
        supersededPrivilegeSource: supersededPrivilegeSource
      };
    }
  }]);

  return KibanaBasePrivilegeCalculator;
}();

exports.KibanaBasePrivilegeCalculator = KibanaBasePrivilegeCalculator;