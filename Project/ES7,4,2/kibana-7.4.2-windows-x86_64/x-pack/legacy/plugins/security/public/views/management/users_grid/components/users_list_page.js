"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UsersListPage = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _notify = require("ui/notify");

var _react2 = require("@kbn/i18n/react");

var _users = require("../../../../components/management/users");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var UsersListPageUI =
/*#__PURE__*/
function (_Component) {
  _inherits(UsersListPageUI, _Component);

  function UsersListPageUI(props) {
    var _this;

    _classCallCheck(this, UsersListPageUI);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(UsersListPageUI).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleDelete", function (usernames, errors) {
      var users = _this.state.users;

      _this.setState({
        selection: [],
        showDeleteConfirmation: false,
        users: users.filter(function (_ref) {
          var username = _ref.username;
          return !usernames.includes(username) || errors.includes(username);
        })
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onCancelDelete", function () {
      _this.setState({
        showDeleteConfirmation: false
      });
    });

    _this.state = {
      users: [],
      selection: [],
      showDeleteConfirmation: false,
      permissionDenied: false,
      filter: ''
    };
    return _this;
  }

  _createClass(UsersListPageUI, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.loadUsers();
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          users = _this$state.users,
          filter = _this$state.filter,
          permissionDenied = _this$state.permissionDenied,
          showDeleteConfirmation = _this$state.showDeleteConfirmation,
          selection = _this$state.selection;
      var intl = this.props.intl;

      if (permissionDenied) {
        return _react.default.createElement(_eui.EuiFlexGroup, {
          gutterSize: "none"
        }, _react.default.createElement(_eui.EuiPageContent, {
          horizontalPosition: "center"
        }, _react.default.createElement(_eui.EuiEmptyPrompt, {
          iconType: "securityApp",
          title: _react.default.createElement("h2", null, _react.default.createElement(_react2.FormattedMessage, {
            id: "xpack.security.management.users.deniedPermissionTitle",
            defaultMessage: "You need permission to manage users"
          })),
          body: _react.default.createElement("p", {
            "data-test-subj": "permissionDeniedMessage"
          }, _react.default.createElement(_react2.FormattedMessage, {
            id: "xpack.security.management.users.permissionDeniedToManageUsersDescription",
            defaultMessage: "Contact your system administrator."
          }))
        })));
      }

      var path = '#/management/security/';
      var columns = [{
        field: 'full_name',
        name: intl.formatMessage({
          id: 'xpack.security.management.users.fullNameColumnName',
          defaultMessage: 'Full Name'
        }),
        sortable: true,
        truncateText: true,
        render: function render(fullName) {
          return _react.default.createElement("div", {
            "data-test-subj": "userRowFullName"
          }, fullName);
        }
      }, {
        field: 'username',
        name: intl.formatMessage({
          id: 'xpack.security.management.users.userNameColumnName',
          defaultMessage: 'User Name'
        }),
        sortable: true,
        truncateText: true,
        render: function render(username) {
          return _react.default.createElement(_eui.EuiLink, {
            "data-test-subj": "userRowUserName",
            href: "".concat(path, "users/edit/").concat(username)
          }, username);
        }
      }, {
        field: 'email',
        name: intl.formatMessage({
          id: 'xpack.security.management.users.emailAddressColumnName',
          defaultMessage: 'Email Address'
        }),
        sortable: true,
        truncateText: true,
        render: function render(email) {
          return _react.default.createElement("div", {
            "data-test-subj": "userRowEmail"
          }, email);
        }
      }, {
        field: 'roles',
        name: intl.formatMessage({
          id: 'xpack.security.management.users.rolesColumnName',
          defaultMessage: 'Roles'
        }),
        render: function render(rolenames) {
          var roleLinks = rolenames.map(function (rolename, index) {
            return _react.default.createElement(_react.Fragment, {
              key: rolename
            }, _react.default.createElement(_eui.EuiLink, {
              href: "".concat(path, "roles/edit/").concat(rolename)
            }, rolename), index === rolenames.length - 1 ? null : ', ');
          });
          return _react.default.createElement("div", {
            "data-test-subj": "userRowRoles"
          }, roleLinks);
        }
      }, {
        field: 'metadata._reserved',
        name: intl.formatMessage({
          id: 'xpack.security.management.users.reservedColumnName',
          defaultMessage: 'Reserved'
        }),
        sortable: false,
        width: '100px',
        align: 'right',
        description: intl.formatMessage({
          id: 'xpack.security.management.users.reservedColumnDescription',
          defaultMessage: 'Reserved users are built-in and cannot be removed. Only the password can be changed.'
        }),
        render: function render(reserved) {
          return reserved ? _react.default.createElement(_eui.EuiIcon, {
            "aria-label": "Reserved user",
            "data-test-subj": "reservedUser",
            type: "check"
          }) : null;
        }
      }];
      var pagination = {
        initialPageSize: 20,
        pageSizeOptions: [10, 20, 50, 100]
      };
      var selectionConfig = {
        itemId: 'username',
        selectable: function selectable(user) {
          return !(user.metadata && user.metadata._reserved);
        },
        selectableMessage: function selectableMessage(selectable) {
          return !selectable ? 'User is a system user' : undefined;
        },
        onSelectionChange: function onSelectionChange(updatedSelection) {
          return _this2.setState({
            selection: updatedSelection
          });
        }
      };
      var search = {
        toolsLeft: this.renderToolsLeft(),
        box: {
          incremental: true
        },
        onChange: function onChange(query) {
          _this2.setState({
            filter: query.queryText
          });
        }
      };
      var sorting = {
        sort: {
          field: 'full_name',
          direction: 'asc'
        }
      };

      var rowProps = function rowProps() {
        return {
          'data-test-subj': 'userRow'
        };
      };

      var usersToShow = filter ? users.filter(function (_ref2) {
        var username = _ref2.username,
            roles = _ref2.roles,
            _ref2$full_name = _ref2.full_name,
            fullName = _ref2$full_name === void 0 ? '' : _ref2$full_name,
            _ref2$email = _ref2.email,
            email = _ref2$email === void 0 ? '' : _ref2$email;
        var normalized = "".concat(username, " ").concat(roles.join(' '), " ").concat(fullName, " ").concat(email).toLowerCase();
        var normalizedQuery = filter.toLowerCase();
        return normalized.indexOf(normalizedQuery) !== -1;
      }) : users;
      return _react.default.createElement("div", {
        className: "secUsersListingPage"
      }, _react.default.createElement(_eui.EuiPageContent, {
        className: "secUsersListingPage__content"
      }, _react.default.createElement(_eui.EuiPageContentHeader, null, _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h2", null, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.security.management.users.usersTitle",
        defaultMessage: "Users"
      })))), _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": "createUserButton",
        href: "#/management/security/users/edit"
      }, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.security.management.users.createNewUserButtonLabel",
        defaultMessage: "Create user"
      })))), _react.default.createElement(_eui.EuiPageContentBody, null, showDeleteConfirmation ? _react.default.createElement(_users.ConfirmDeleteUsers, {
        onCancel: this.onCancelDelete,
        usersToDelete: selection.map(function (user) {
          return user.username;
        }),
        callback: this.handleDelete,
        apiClient: this.props.apiClient
      }) : null, // @ts-ignore missing responsive from typedef
      _react.default.createElement(_eui.EuiInMemoryTable, {
        itemId: "username",
        columns: columns,
        selection: selectionConfig,
        pagination: pagination,
        items: usersToShow,
        loading: users.length === 0,
        search: search,
        sorting: sorting // @ts-ignore missing responsive from typedef
        ,
        rowProps: rowProps,
        isSelectable: true
      }))));
    }
  }, {
    key: "loadUsers",
    value: function () {
      var _loadUsers = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        var users;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return this.props.apiClient.getUsers();

              case 3:
                users = _context.sent;
                this.setState({
                  users: users
                });
                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);

                if (_context.t0.body.statusCode === 403) {
                  this.setState({
                    permissionDenied: true
                  });
                } else {
                  _notify.toastNotifications.addDanger(this.props.intl.formatMessage({
                    id: 'xpack.security.management.users.fetchingUsersErrorMessage',
                    defaultMessage: 'Error fetching users: {message}'
                  }, {
                    message: _context.t0.body.message
                  }));
                }

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this, [[0, 7]]);
      }));

      function loadUsers() {
        return _loadUsers.apply(this, arguments);
      }

      return loadUsers;
    }()
  }, {
    key: "renderToolsLeft",
    value: function renderToolsLeft() {
      var _this3 = this;

      var selection = this.state.selection;

      if (selection.length === 0) {
        return;
      }

      var numSelected = selection.length;
      return _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": "deleteUserButton",
        color: "danger",
        onClick: function onClick() {
          return _this3.setState({
            showDeleteConfirmation: true
          });
        }
      }, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.security.management.users.deleteUsersButtonLabel",
        defaultMessage: "Delete {numSelected} user{numSelected, plural, one { } other {s}}",
        values: {
          numSelected: numSelected
        }
      }));
    }
  }]);

  return UsersListPageUI;
}(_react.Component);

var UsersListPage = (0, _react2.injectI18n)(UsersListPageUI);
exports.UsersListPage = UsersListPage;