"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BasicLoginForm = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var BasicLoginFormUI =
/*#__PURE__*/
function (_Component) {
  _inherits(BasicLoginFormUI, _Component);

  function BasicLoginFormUI() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, BasicLoginFormUI);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(BasicLoginFormUI)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      hasError: false,
      isLoading: false,
      username: '',
      password: '',
      message: ''
    });

    _defineProperty(_assertThisInitialized(_this), "renderMessage", function () {
      if (_this.state.message) {
        return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiCallOut, {
          size: "s",
          color: "danger",
          "data-test-subj": "loginErrorMessage",
          title: _this.state.message,
          role: "alert"
        }), _react2.default.createElement(_eui.EuiSpacer, {
          size: "l"
        }));
      }

      if (_this.props.infoMessage) {
        return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiCallOut, {
          size: "s",
          color: "primary",
          "data-test-subj": "loginInfoMessage",
          title: _this.props.infoMessage,
          role: "status"
        }), _react2.default.createElement(_eui.EuiSpacer, {
          size: "l"
        }));
      }

      return null;
    });

    _defineProperty(_assertThisInitialized(_this), "isFormValid", function () {
      var _this$state = _this.state,
          username = _this$state.username,
          password = _this$state.password;
      return username && password;
    });

    _defineProperty(_assertThisInitialized(_this), "onUsernameChange", function (e) {
      _this.setState({
        username: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onPasswordChange", function (e) {
      _this.setState({
        password: e.target.value
      });
    });

    _defineProperty(_assertThisInitialized(_this), "submit", function (e) {
      e.preventDefault();

      if (!_this.isFormValid()) {
        return;
      }

      _this.setState({
        isLoading: true,
        message: ''
      });

      var _this$props = _this.props,
          http = _this$props.http,
          window = _this$props.window,
          next = _this$props.next,
          intl = _this$props.intl;
      var _this$state2 = _this.state,
          username = _this$state2.username,
          password = _this$state2.password;
      http.post('./api/security/v1/login', {
        username: username,
        password: password
      }).then(function () {
        return window.location.href = next;
      }, function (error) {
        var _ref = error.data || {},
            _ref$statusCode = _ref.statusCode,
            statusCode = _ref$statusCode === void 0 ? 500 : _ref$statusCode;

        var message = intl.formatMessage({
          id: 'xpack.security.login.basicLoginForm.unknownErrorMessage',
          defaultMessage: 'Oops! Error. Try again.'
        });

        if (statusCode === 401) {
          message = intl.formatMessage({
            id: 'xpack.security.login.basicLoginForm.invalidUsernameOrPasswordErrorMessage',
            defaultMessage: 'Invalid username or password. Please try again.'
          });
        }

        _this.setState({
          hasError: true,
          message: message,
          isLoading: false
        });
      });
    });

    return _this;
  }

  _createClass(BasicLoginFormUI, [{
    key: "render",
    value: function render() {
      return _react2.default.createElement(_react2.Fragment, null, this.renderMessage(), _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement("form", {
        onSubmit: this.submit
      }, _react2.default.createElement(_eui.EuiFormRow, {
        label: _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.login.basicLoginForm.usernameFormRowLabel",
          defaultMessage: "Username"
        })
      }, _react2.default.createElement(_eui.EuiFieldText, {
        id: "username",
        name: "username",
        "data-test-subj": "loginUsername",
        value: this.state.username,
        onChange: this.onUsernameChange,
        disabled: this.state.isLoading,
        isInvalid: false,
        "aria-required": true,
        inputRef: this.setUsernameInputRef
      })), _react2.default.createElement(_eui.EuiFormRow, {
        label: _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.login.basicLoginForm.passwordFormRowLabel",
          defaultMessage: "Password"
        })
      }, _react2.default.createElement(_eui.EuiFieldText, {
        id: "password",
        name: "password",
        "data-test-subj": "loginPassword",
        type: "password",
        value: this.state.password,
        onChange: this.onPasswordChange,
        disabled: this.state.isLoading,
        isInvalid: false,
        "aria-required": true
      })), _react2.default.createElement(_eui.EuiButton, {
        fill: true,
        type: "submit",
        color: "primary",
        onClick: this.submit,
        isLoading: this.state.isLoading,
        "data-test-subj": "loginSubmit"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.login.basicLoginForm.logInButtonLabel",
        defaultMessage: "Log in"
      })))));
    }
  }, {
    key: "setUsernameInputRef",
    value: function setUsernameInputRef(ref) {
      if (ref) {
        ref.focus();
      }
    }
  }]);

  return BasicLoginFormUI;
}(_react2.Component);

var BasicLoginForm = (0, _react.injectI18n)(BasicLoginFormUI);
exports.BasicLoginForm = BasicLoginForm;