"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaPrivilegeCalculatorFactory = void 0;

var _privilege_calculator_utils = require("../../../common/privilege_calculator_utils");

var _role_utils = require("../../lib/role_utils");

var _kibana_privilege_calculator = require("./kibana_privilege_calculator");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var KibanaPrivilegeCalculatorFactory =
/*#__PURE__*/
function () {
  /** All feature privileges, sorted from most permissive => least permissive. */
  function KibanaPrivilegeCalculatorFactory(kibanaPrivileges) {
    var _this = this;

    _classCallCheck(this, KibanaPrivilegeCalculatorFactory);

    this.kibanaPrivileges = kibanaPrivileges;

    _defineProperty(this, "rankedFeaturePrivileges", void 0);

    this.rankedFeaturePrivileges = {};
    var featurePrivilegeSet = kibanaPrivileges.getFeaturePrivileges().getAllPrivileges();
    Object.entries(featurePrivilegeSet).forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          featureId = _ref2[0],
          privileges = _ref2[1];

      _this.rankedFeaturePrivileges[featureId] = privileges.sort(function (privilege1, privilege2) {
        var privilege1Actions = kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilege1);
        var privilege2Actions = kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilege2);
        return (0, _privilege_calculator_utils.compareActions)(privilege1Actions, privilege2Actions);
      });
    });
  }
  /**
   * Creates an KibanaPrivilegeCalculator instance for the specified role.
   * @param role
   */


  _createClass(KibanaPrivilegeCalculatorFactory, [{
    key: "getInstance",
    value: function getInstance(role) {
      var roleCopy = (0, _role_utils.copyRole)(role);
      this.sortPrivileges(roleCopy);
      return new _kibana_privilege_calculator.KibanaPrivilegeCalculator(this.kibanaPrivileges, roleCopy, this.rankedFeaturePrivileges);
    }
  }, {
    key: "sortPrivileges",
    value: function sortPrivileges(role) {
      var _this2 = this;

      role.kibana.forEach(function (privilege) {
        privilege.base.sort(function (privilege1, privilege2) {
          var privilege1Actions = _this2.kibanaPrivileges.getSpacesPrivileges().getActions(privilege1);

          var privilege2Actions = _this2.kibanaPrivileges.getSpacesPrivileges().getActions(privilege2);

          return (0, _privilege_calculator_utils.compareActions)(privilege1Actions, privilege2Actions);
        });
        Object.entries(privilege.feature).forEach(function (_ref3) {
          var _ref4 = _slicedToArray(_ref3, 2),
              featureId = _ref4[0],
              featurePrivs = _ref4[1];

          featurePrivs.sort(function (privilege1, privilege2) {
            var privilege1Actions = _this2.kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilege1);

            var privilege2Actions = _this2.kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilege2);

            return (0, _privilege_calculator_utils.compareActions)(privilege1Actions, privilege2Actions);
          });
        });
      });
    }
  }]);

  return KibanaPrivilegeCalculatorFactory;
}();

exports.KibanaPrivilegeCalculatorFactory = KibanaPrivilegeCalculatorFactory;