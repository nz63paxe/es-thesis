"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.buildRole = void 0;

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var buildRole = function buildRole() {
  var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var role = {
    name: 'unit test role',
    elasticsearch: {
      indices: [],
      cluster: [],
      run_as: []
    },
    kibana: []
  };

  if (options.spacesPrivileges) {
    var _role$kibana;

    (_role$kibana = role.kibana).push.apply(_role$kibana, _toConsumableArray(options.spacesPrivileges));
  } else {
    role.kibana.push({
      spaces: [],
      base: [],
      feature: {}
    });
  }

  return role;
};

exports.buildRole = buildRole;