"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "BasicLoginForm", {
  enumerable: true,
  get: function get() {
    return _basic_login_form.BasicLoginForm;
  }
});

var _basic_login_form = require("./basic_login_form");