"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  defaultPrivilegeDefinition: true,
  buildRole: true,
  BuildRoleOpts: true
};
Object.defineProperty(exports, "defaultPrivilegeDefinition", {
  enumerable: true,
  get: function get() {
    return _default_privilege_definition.defaultPrivilegeDefinition;
  }
});
Object.defineProperty(exports, "buildRole", {
  enumerable: true,
  get: function get() {
    return _build_role.buildRole;
  }
});
Object.defineProperty(exports, "BuildRoleOpts", {
  enumerable: true,
  get: function get() {
    return _build_role.BuildRoleOpts;
  }
});

var _default_privilege_definition = require("./default_privilege_definition");

var _build_role = require("./build_role");

var _common_allowed_privileges = require("./common_allowed_privileges");

Object.keys(_common_allowed_privileges).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _common_allowed_privileges[key];
    }
  });
});