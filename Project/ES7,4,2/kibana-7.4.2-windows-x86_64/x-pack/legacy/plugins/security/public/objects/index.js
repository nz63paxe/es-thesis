"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "saveRole", {
  enumerable: true,
  get: function get() {
    return _roles.saveRole;
  }
});
Object.defineProperty(exports, "deleteRole", {
  enumerable: true,
  get: function get() {
    return _roles.deleteRole;
  }
});
Object.defineProperty(exports, "getFields", {
  enumerable: true,
  get: function get() {
    return _get_fields.getFields;
  }
});

var _roles = require("./lib/roles");

var _get_fields = require("./lib/get_fields");