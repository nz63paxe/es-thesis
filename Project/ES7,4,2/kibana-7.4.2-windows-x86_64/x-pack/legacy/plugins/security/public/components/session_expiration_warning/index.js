"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SessionExpirationWarning", {
  enumerable: true,
  get: function get() {
    return _session_expiration_warning.SessionExpirationWarning;
  }
});

var _session_expiration_warning = require("./session_expiration_warning");