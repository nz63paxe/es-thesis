"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LoginPage = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _classnames = _interopRequireDefault(require("classnames"));

var _basic_login_form = require("../basic_login_form");

var _disabled_login_form = require("../disabled_login_form");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LoginPage =
/*#__PURE__*/
function (_Component) {
  _inherits(LoginPage, _Component);

  function LoginPage() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LoginPage);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LoginPage)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "allowLogin", function () {
      if (_this.props.requiresSecureConnection && !_this.props.isSecureConnection) {
        return false;
      }

      return _this.props.loginState.allowLogin && _this.props.loginState.layout === 'form';
    });

    _defineProperty(_assertThisInitialized(_this), "getLoginForm", function () {
      if (_this.props.requiresSecureConnection && !_this.props.isSecureConnection) {
        return _react.default.createElement(_disabled_login_form.DisabledLoginForm, {
          title: _react.default.createElement(_react2.FormattedMessage, {
            id: "xpack.security.loginPage.requiresSecureConnectionTitle",
            defaultMessage: "A secure connection is required for log in"
          }),
          message: _react.default.createElement(_react2.FormattedMessage, {
            id: "xpack.security.loginPage.requiresSecureConnectionMessage",
            defaultMessage: "Contact your system administrator."
          })
        });
      }

      var layout = _this.props.loginState.layout;

      switch (layout) {
        case 'form':
          return _react.default.createElement(_basic_login_form.BasicLoginForm, _this.props);

        case 'error-es-unavailable':
          return _react.default.createElement(_disabled_login_form.DisabledLoginForm, {
            title: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.esUnavailableTitle",
              defaultMessage: "Cannot connect to the Elasticsearch cluster"
            }),
            message: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.esUnavailableMessage",
              defaultMessage: "See the Kibana logs for details and try reloading the page."
            })
          });

        case 'error-xpack-unavailable':
          return _react.default.createElement(_disabled_login_form.DisabledLoginForm, {
            title: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.xpackUnavailableTitle",
              defaultMessage: "Cannot connect to the Elasticsearch cluster currently configured for Kibana."
            }),
            message: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.xpackUnavailableMessage",
              defaultMessage: "To use the full set of free features in this distribution of Kibana, please update Elasticsearch to the default distribution."
            })
          });

        default:
          return _react.default.createElement(_disabled_login_form.DisabledLoginForm, {
            title: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.unknownLayoutTitle",
              defaultMessage: "Unsupported login form layout."
            }),
            message: _react.default.createElement(_react2.FormattedMessage, {
              id: "xpack.security.loginPage.unknownLayoutMessage",
              defaultMessage: "Refer to the Kibana logs for more details and refresh to try again."
            })
          });
      }
    });

    return _this;
  }

  _createClass(LoginPage, [{
    key: "render",
    value: function render() {
      var allowLogin = this.allowLogin();
      var contentHeaderClasses = (0, _classnames.default)('loginWelcome__content', 'eui-textCenter', _defineProperty({}, 'loginWelcome__contentDisabledForm', !allowLogin));
      var contentBodyClasses = (0, _classnames.default)('loginWelcome__content', 'loginWelcome-body', _defineProperty({}, 'loginWelcome__contentDisabledForm', !allowLogin));
      return _react.default.createElement("div", {
        className: "loginWelcome login-form"
      }, _react.default.createElement("header", {
        className: "loginWelcome__header"
      }, _react.default.createElement("div", {
        className: contentHeaderClasses
      }, _react.default.createElement(_eui.EuiSpacer, {
        size: "xxl"
      }), _react.default.createElement("span", {
        className: "loginWelcome__logo"
      }, _react.default.createElement(_eui.EuiIcon, {
        type: "logoKibana",
        size: "xxl"
      })), _react.default.createElement(_eui.EuiTitle, {
        size: "l",
        className: "loginWelcome__title"
      }, _react.default.createElement("h1", null, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.security.loginPage.welcomeTitle",
        defaultMessage: "Welcome to Kibana"
      }))), _react.default.createElement(_eui.EuiText, {
        size: "s",
        color: "subdued",
        className: "loginWelcome__subtitle"
      }, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
        id: "xpack.security.loginPage.welcomeDescription",
        defaultMessage: "Your window into the Elastic Stack"
      }))), _react.default.createElement(_eui.EuiSpacer, {
        size: "xl"
      }))), _react.default.createElement("div", {
        className: contentBodyClasses
      }, _react.default.createElement(_eui.EuiFlexGroup, {
        gutterSize: "l"
      }, _react.default.createElement(_eui.EuiFlexItem, null, this.getLoginForm()))));
    }
  }]);

  return LoginPage;
}(_react.Component);

exports.LoginPage = LoginPage;