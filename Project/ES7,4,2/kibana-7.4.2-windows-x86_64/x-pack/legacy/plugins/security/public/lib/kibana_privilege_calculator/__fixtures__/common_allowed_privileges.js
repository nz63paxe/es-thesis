"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fullyRestrictedFeaturePrivileges = exports.fullyRestrictedBasePrivileges = exports.unrestrictedFeaturePrivileges = exports.unrestrictedBasePrivileges = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var unrestrictedBasePrivileges = {
  base: {
    privileges: ['all', 'read'],
    canUnassign: true
  }
};
exports.unrestrictedBasePrivileges = unrestrictedBasePrivileges;
var unrestrictedFeaturePrivileges = {
  feature: {
    feature1: {
      privileges: ['all', 'read'],
      canUnassign: true
    },
    feature2: {
      privileges: ['all', 'read'],
      canUnassign: true
    },
    feature3: {
      privileges: ['all'],
      canUnassign: true
    },
    feature4: {
      privileges: ['all', 'read'],
      canUnassign: true
    }
  }
};
exports.unrestrictedFeaturePrivileges = unrestrictedFeaturePrivileges;
var fullyRestrictedBasePrivileges = {
  base: {
    privileges: ['all'],
    canUnassign: false
  }
};
exports.fullyRestrictedBasePrivileges = fullyRestrictedBasePrivileges;
var fullyRestrictedFeaturePrivileges = {
  feature: {
    feature1: {
      privileges: ['all'],
      canUnassign: false
    },
    feature2: {
      privileges: ['all'],
      canUnassign: false
    },
    feature3: {
      privileges: ['all'],
      canUnassign: false
    },
    feature4: {
      privileges: ['all', 'read'],
      canUnassign: false
    }
  }
};
exports.fullyRestrictedFeaturePrivileges = fullyRestrictedFeaturePrivileges;