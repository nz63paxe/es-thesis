"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RolesApi = void 0;

var _kfetch = require("ui/kfetch");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var RolesApi =
/*#__PURE__*/
function () {
  function RolesApi() {
    _classCallCheck(this, RolesApi);
  }

  _createClass(RolesApi, null, [{
    key: "getRoles",
    value: function () {
      var _getRoles = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                return _context.abrupt("return", (0, _kfetch.kfetch)({
                  pathname: '/api/security/role'
                }));

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getRoles() {
        return _getRoles.apply(this, arguments);
      }

      return getRoles;
    }()
  }, {
    key: "getRole",
    value: function () {
      var _getRole = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(roleName) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                return _context2.abrupt("return", (0, _kfetch.kfetch)({
                  pathname: "/api/security/role/".concat(encodeURIComponent(roleName))
                }));

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getRole(_x) {
        return _getRole.apply(this, arguments);
      }

      return getRole;
    }()
  }, {
    key: "deleteRole",
    value: function () {
      var _deleteRole = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(roleName) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                return _context3.abrupt("return", (0, _kfetch.kfetch)({
                  pathname: "/api/security/role/".concat(encodeURIComponent(roleName)),
                  method: 'DELETE'
                }));

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function deleteRole(_x2) {
        return _deleteRole.apply(this, arguments);
      }

      return deleteRole;
    }()
  }]);

  return RolesApi;
}();

exports.RolesApi = RolesApi;