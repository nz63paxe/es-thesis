"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EffectivePrivilegeDisplay = exports.SupersededPrivilegeDisplay = exports.PrivilegeDisplay = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = _interopRequireDefault(require("lodash"));

var _react2 = _interopRequireDefault(require("react"));

var _kibana_privilege_calculator = require("../../../../../../../lib/kibana_privilege_calculator");

var _constants = require("../../../../lib/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var PrivilegeDisplay = function PrivilegeDisplay(props) {
  var explanation = props.explanation;

  if (!explanation) {
    return _react2.default.createElement(SimplePrivilegeDisplay, props);
  }

  if (explanation.supersededPrivilege) {
    return _react2.default.createElement(SupersededPrivilegeDisplay, props);
  }

  if (!explanation.isDirectlyAssigned) {
    return _react2.default.createElement(EffectivePrivilegeDisplay, props);
  }

  return _react2.default.createElement(SimplePrivilegeDisplay, props);
};

exports.PrivilegeDisplay = PrivilegeDisplay;

var SimplePrivilegeDisplay = function SimplePrivilegeDisplay(props) {
  var privilege = props.privilege,
      iconType = props.iconType,
      iconTooltipContent = props.iconTooltipContent,
      explanation = props.explanation,
      tooltipContent = props.tooltipContent,
      rest = _objectWithoutProperties(props, ["privilege", "iconType", "iconTooltipContent", "explanation", "tooltipContent"]);

  var text = _react2.default.createElement(_eui.EuiText, rest, getDisplayValue(privilege), " ", getIconTip(iconType, iconTooltipContent));

  if (tooltipContent) {
    return _react2.default.createElement(_eui.EuiToolTip, {
      content: tooltipContent
    }, text);
  }

  return text;
};

var SupersededPrivilegeDisplay = function SupersededPrivilegeDisplay(props) {
  var _ref = props.explanation || {},
      supersededPrivilege = _ref.supersededPrivilege,
      actualPrivilegeSource = _ref.actualPrivilegeSource;

  return _react2.default.createElement(SimplePrivilegeDisplay, _extends({}, props, {
    iconType: 'lock',
    iconTooltipContent: _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.privilegeSupercededMessage",
      defaultMessage: "Original privilege of {supersededPrivilege} has been overriden by {actualPrivilegeSource}",
      values: {
        supersededPrivilege: "'".concat(supersededPrivilege, "'"),
        actualPrivilegeSource: getReadablePrivilegeSource(actualPrivilegeSource)
      }
    })
  }));
};

exports.SupersededPrivilegeDisplay = SupersededPrivilegeDisplay;

var EffectivePrivilegeDisplay = function EffectivePrivilegeDisplay(props) {
  var explanation = props.explanation,
      rest = _objectWithoutProperties(props, ["explanation"]);

  var source = getReadablePrivilegeSource(explanation.actualPrivilegeSource);

  var iconTooltipContent = _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.effectivePrivilegeMessage",
    defaultMessage: "Granted via {source}.",
    values: {
      source: source
    }
  });

  return _react2.default.createElement(SimplePrivilegeDisplay, _extends({}, rest, {
    iconType: 'lock',
    iconTooltipContent: iconTooltipContent
  }));
};

exports.EffectivePrivilegeDisplay = EffectivePrivilegeDisplay;
PrivilegeDisplay.defaultProps = {
  privilege: []
};

function getDisplayValue(privilege) {
  var privileges = coerceToArray(privilege);
  var displayValue;
  var isPrivilegeMissing = privileges.length === 0 || privileges.length === 1 && privileges.includes(_constants.NO_PRIVILEGE_VALUE);

  if (isPrivilegeMissing) {
    displayValue = _react2.default.createElement(_eui.EuiIcon, {
      color: "subdued",
      type: 'minusInCircle'
    });
  } else {
    displayValue = privileges.map(function (p) {
      return _lodash.default.capitalize(p);
    }).join(', ');
  }

  return displayValue;
}

function getIconTip(iconType, tooltipContent) {
  if (!iconType || !tooltipContent) {
    return null;
  }

  return _react2.default.createElement(_eui.EuiIconTip, {
    iconProps: {
      className: 'eui-alignTop'
    },
    color: "subdued",
    type: iconType,
    content: tooltipContent,
    size: 's'
  });
}

function coerceToArray(privilege) {
  if (privilege === undefined) {
    return [];
  }

  if (Array.isArray(privilege)) {
    return privilege;
  }

  return [privilege];
}

function getReadablePrivilegeSource(privilegeSource) {
  switch (privilegeSource) {
    case _kibana_privilege_calculator.PRIVILEGE_SOURCE.GLOBAL_BASE:
      return _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.globalBasePrivilegeSource",
        defaultMessage: "global base privilege"
      });

    case _kibana_privilege_calculator.PRIVILEGE_SOURCE.GLOBAL_FEATURE:
      return _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.globalFeaturePrivilegeSource",
        defaultMessage: "global feature privilege"
      });

    case _kibana_privilege_calculator.PRIVILEGE_SOURCE.SPACE_BASE:
      return _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.spaceBasePrivilegeSource",
        defaultMessage: "space base privilege"
      });

    case _kibana_privilege_calculator.PRIVILEGE_SOURCE.SPACE_FEATURE:
      return _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.spaceFeaturePrivilegeSource",
        defaultMessage: "space feature privilege"
      });

    default:
      return _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spaceAwarePrivilegeDisplay.unknownPrivilegeSource",
        defaultMessage: "**UNKNOWN**"
      });
  }
}