"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FeatureTable = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = _interopRequireDefault(require("lodash"));

var _react2 = _interopRequireWildcard(require("react"));

var _privilege_utils = require("../../../../../../../lib/privilege_utils");

var _constants = require("../../../../lib/constants");

var _privilege_display = require("../space_aware_privilege_section/privilege_display");

var _change_all_privileges = require("./change_all_privileges");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var FeatureTable =
/*#__PURE__*/
function (_Component) {
  _inherits(FeatureTable, _Component);

  function FeatureTable() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, FeatureTable);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(FeatureTable)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "onChange", function (featureId) {
      return function (featurePrivilegeId) {
        var privilege = featurePrivilegeId.substr("".concat(featureId, "_").length);

        if (privilege === _constants.NO_PRIVILEGE_VALUE) {
          _this.props.onChange(featureId, []);
        } else {
          _this.props.onChange(featureId, [privilege]);
        }
      };
    });

    _defineProperty(_assertThisInitialized(_this), "getColumns", function (availablePrivileges) {
      return [{
        field: 'feature',
        name: _this.props.intl.formatMessage({
          id: 'xpack.security.management.editRole.featureTable.enabledRoleFeaturesFeatureColumnTitle',
          defaultMessage: 'Feature'
        }),
        render: function render(feature) {
          var tooltipElement = null;

          if (feature.privilegesTooltip) {
            var tooltipContent = _react2.default.createElement(_eui.EuiText, null, _react2.default.createElement("p", null, feature.privilegesTooltip));

            tooltipElement = _react2.default.createElement(_eui.EuiIconTip, {
              iconProps: {
                className: 'eui-alignTop'
              },
              type: 'iInCircle',
              color: 'subdued',
              content: tooltipContent
            });
          }

          return _react2.default.createElement("span", null, _react2.default.createElement(_eui.EuiIcon, {
            size: "m",
            type: feature.icon,
            className: "secPrivilegeFeatureIcon"
          }), feature.name, " ", tooltipElement);
        }
      }, {
        field: 'privilege',
        name: _react2.default.createElement("span", null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.featureTable.enabledRoleFeaturesEnabledColumnTitle",
          defaultMessage: "Privilege"
        }), !_this.props.disabled && _react2.default.createElement(_change_all_privileges.ChangeAllPrivilegesControl, {
          privileges: [].concat(_toConsumableArray(availablePrivileges), [_constants.NO_PRIVILEGE_VALUE]),
          onChange: _this.onChangeAllFeaturePrivileges
        })),
        render: function render(roleEntry, record) {
          var _record$feature = record.feature,
              featureId = _record$feature.id,
              featureName = _record$feature.name,
              reserved = _record$feature.reserved,
              privileges = _record$feature.privileges;

          if (reserved && Object.keys(privileges).length === 0) {
            return _react2.default.createElement(_eui.EuiText, {
              size: 's'
            }, reserved.description);
          }

          var featurePrivileges = _this.props.kibanaPrivileges.getFeaturePrivileges().getPrivileges(featureId);

          if (featurePrivileges.length === 0) {
            return null;
          }

          var enabledFeaturePrivileges = _this.getEnabledFeaturePrivileges(featurePrivileges, featureId);

          var privilegeExplanation = _this.getPrivilegeExplanation(featureId);

          var allowsNone = _this.allowsNoneForPrivilegeAssignment(featureId);

          var actualPrivilegeValue = privilegeExplanation.actualPrivilege;
          var canChangePrivilege = !_this.props.disabled && (allowsNone || enabledFeaturePrivileges.length > 1);

          if (!canChangePrivilege) {
            var assignedBasePrivilege = _this.props.role.kibana[_this.props.spacesIndex].base.length > 0;

            var excludedFromBasePrivilegsTooltip = _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.security.management.editRole.featureTable.excludedFromBasePrivilegsTooltip",
              defaultMessage: "Use \"Custom\" privileges to grant access. {featureName} isn't part of the base privileges.",
              values: {
                featureName: featureName
              }
            });

            return _react2.default.createElement(_privilege_display.PrivilegeDisplay, {
              privilege: actualPrivilegeValue,
              explanation: privilegeExplanation,
              tooltipContent: assignedBasePrivilege && actualPrivilegeValue === _constants.NO_PRIVILEGE_VALUE ? excludedFromBasePrivilegsTooltip : undefined
            });
          }

          var options = availablePrivileges.map(function (priv) {
            return {
              id: "".concat(featureId, "_").concat(priv),
              label: _lodash.default.capitalize(priv),
              isDisabled: !enabledFeaturePrivileges.includes(priv)
            };
          });
          options.push({
            id: "".concat(featureId, "_").concat(_constants.NO_PRIVILEGE_VALUE),
            label: 'None',
            isDisabled: !allowsNone
          });
          return (// @ts-ignore missing name from typedef
            _react2.default.createElement(_eui.EuiButtonGroup // @ts-ignore missing rowProps from typedef
            , {
              name: "featurePrivilege_".concat(featureId),
              options: options,
              idSelected: "".concat(featureId, "_").concat(actualPrivilegeValue || _constants.NO_PRIVILEGE_VALUE),
              onChange: _this.onChange(featureId)
            })
          );
        }
      }];
    });

    _defineProperty(_assertThisInitialized(_this), "getEnabledFeaturePrivileges", function (featurePrivileges, featureId) {
      var allowedPrivileges = _this.props.allowedPrivileges;

      if (_this.isConfiguringGlobalPrivileges()) {
        // Global feature privileges are not limited by effective privileges.
        return featurePrivileges;
      }

      var allowedFeaturePrivileges = allowedPrivileges.feature[featureId];

      if (allowedFeaturePrivileges == null) {
        throw new Error('Unable to get enabled feature privileges for a feature without privileges');
      }

      return allowedFeaturePrivileges.privileges;
    });

    _defineProperty(_assertThisInitialized(_this), "getPrivilegeExplanation", function (featureId) {
      var calculatedPrivileges = _this.props.calculatedPrivileges;
      var calculatedFeaturePrivileges = calculatedPrivileges.feature[featureId];

      if (calculatedFeaturePrivileges == null) {
        throw new Error('Unable to get privilege explanation for a feature without privileges');
      }

      return calculatedFeaturePrivileges;
    });

    _defineProperty(_assertThisInitialized(_this), "allowsNoneForPrivilegeAssignment", function (featureId) {
      var allowedPrivileges = _this.props.allowedPrivileges;
      var allowedFeaturePrivileges = allowedPrivileges.feature[featureId];

      if (allowedFeaturePrivileges == null) {
        throw new Error('Unable to determine if none is allowed for a feature without privileges');
      }

      return allowedFeaturePrivileges.canUnassign;
    });

    _defineProperty(_assertThisInitialized(_this), "onChangeAllFeaturePrivileges", function (privilege) {
      if (privilege === _constants.NO_PRIVILEGE_VALUE) {
        _this.props.onChangeAll([]);
      } else {
        _this.props.onChangeAll([privilege]);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "isConfiguringGlobalPrivileges", function () {
      return (0, _privilege_utils.isGlobalPrivilegeDefinition)(_this.props.role.kibana[_this.props.spacesIndex]);
    });

    return _this;
  }

  _createClass(FeatureTable, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          role = _this$props.role,
          features = _this$props.features,
          calculatedPrivileges = _this$props.calculatedPrivileges,
          rankedFeaturePrivileges = _this$props.rankedFeaturePrivileges;
      var items = features.sort(function (feature1, feature2) {
        if (Object.keys(feature1.privileges).length === 0 && Object.keys(feature2.privileges).length > 0) {
          return 1;
        }

        if (Object.keys(feature2.privileges).length === 0 && Object.keys(feature1.privileges).length > 0) {
          return -1;
        }

        return 0;
      }).map(function (feature) {
        var calculatedFeaturePrivileges = calculatedPrivileges.feature[feature.id];
        var hasAnyPrivilegeAssigned = Boolean(calculatedFeaturePrivileges && calculatedFeaturePrivileges.actualPrivilege !== _constants.NO_PRIVILEGE_VALUE);
        return {
          feature: _objectSpread({}, feature, {
            hasAnyPrivilegeAssigned: hasAnyPrivilegeAssigned
          }),
          role: role
        };
      }); // TODO: This simply grabs the available privileges from the first feature we encounter.
      // As of now, features can have 'all' and 'read' as available privileges. Once that assumption breaks,
      // this will need updating. This is a simplifying measure to enable the new UI.

      var availablePrivileges = Object.values(rankedFeaturePrivileges)[0];
      return (// @ts-ignore missing responsive from typedef
        _react2.default.createElement(_eui.EuiInMemoryTable // @ts-ignore missing rowProps from typedef
        , {
          responsive: false,
          columns: this.getColumns(availablePrivileges),
          items: items
        })
      );
    }
  }]);

  return FeatureTable;
}(_react2.Component);

exports.FeatureTable = FeatureTable;

_defineProperty(FeatureTable, "defaultProps", {
  spacesIndex: -1,
  showLocks: true
});