"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaFeaturePrivilegeCalculator = void 0;

var _privilege_calculator_utils = require("../../../common/privilege_calculator_utils");

var _constants = require("../../views/management/edit_role/lib/constants");

var _privilege_utils = require("../privilege_utils");

var _kibana_privilege_calculator_types = require("./kibana_privilege_calculator_types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var KibanaFeaturePrivilegeCalculator =
/*#__PURE__*/
function () {
  function KibanaFeaturePrivilegeCalculator(kibanaPrivileges, globalPrivilege, assignedGlobalBaseActions, rankedFeaturePrivileges) {
    _classCallCheck(this, KibanaFeaturePrivilegeCalculator);

    this.kibanaPrivileges = kibanaPrivileges;
    this.globalPrivilege = globalPrivilege;
    this.assignedGlobalBaseActions = assignedGlobalBaseActions;
    this.rankedFeaturePrivileges = rankedFeaturePrivileges;
  }

  _createClass(KibanaFeaturePrivilegeCalculator, [{
    key: "getMostPermissiveFeaturePrivilege",
    value: function getMostPermissiveFeaturePrivilege(privilegeSpec, basePrivilegeExplanation, featureId, ignoreAssigned) {
      var scenarios = this.buildFeaturePrivilegeScenarios(privilegeSpec, basePrivilegeExplanation, featureId, ignoreAssigned);
      var featurePrivileges = this.rankedFeaturePrivileges[featureId] || []; // inspect feature privileges in ranked order (most permissive -> least permissive)

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = featurePrivileges[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var featurePrivilege = _step.value;
          var actions = this.kibanaPrivileges.getFeaturePrivileges().getActions(featureId, featurePrivilege); // check if any of the scenarios satisfy the privilege - first one wins.

          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = scenarios[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var scenario = _step2.value;

              if ((0, _privilege_calculator_utils.areActionsFullyCovered)(scenario.actions, actions)) {
                return _objectSpread({
                  actualPrivilege: featurePrivilege,
                  actualPrivilegeSource: scenario.actualPrivilegeSource,
                  isDirectlyAssigned: scenario.isDirectlyAssigned,
                  directlyAssignedFeaturePrivilegeMorePermissiveThanBase: scenario.directlyAssignedFeaturePrivilegeMorePermissiveThanBase
                }, this.buildSupercededFields(!scenario.isDirectlyAssigned, scenario.supersededPrivilege, scenario.supersededPrivilegeSource));
              }
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                _iterator2.return();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var isGlobal = (0, _privilege_utils.isGlobalPrivilegeDefinition)(privilegeSpec);
      return {
        actualPrivilege: _constants.NO_PRIVILEGE_VALUE,
        actualPrivilegeSource: isGlobal ? _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_FEATURE : _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_FEATURE,
        isDirectlyAssigned: true
      };
    }
  }, {
    key: "buildFeaturePrivilegeScenarios",
    value: function buildFeaturePrivilegeScenarios(privilegeSpec, basePrivilegeExplanation, featureId, ignoreAssigned) {
      var scenarios = [];
      var isGlobalPrivilege = (0, _privilege_utils.isGlobalPrivilegeDefinition)(privilegeSpec);
      var assignedGlobalFeaturePrivilege = this.getAssignedFeaturePrivilege(this.globalPrivilege, featureId);
      var assignedFeaturePrivilege = this.getAssignedFeaturePrivilege(privilegeSpec, featureId);
      var hasAssignedFeaturePrivilege = !ignoreAssigned && assignedFeaturePrivilege !== _constants.NO_PRIVILEGE_VALUE;
      scenarios.push(_objectSpread({
        actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE,
        isDirectlyAssigned: false,
        actions: _toConsumableArray(this.assignedGlobalBaseActions)
      }, this.buildSupercededFields(hasAssignedFeaturePrivilege, assignedFeaturePrivilege, isGlobalPrivilege ? _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_FEATURE : _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_FEATURE)));

      if (!isGlobalPrivilege || !ignoreAssigned) {
        scenarios.push(_objectSpread({
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_FEATURE,
          actions: this.getFeatureActions(featureId, assignedGlobalFeaturePrivilege),
          isDirectlyAssigned: isGlobalPrivilege && hasAssignedFeaturePrivilege
        }, this.buildSupercededFields(hasAssignedFeaturePrivilege && !isGlobalPrivilege, assignedFeaturePrivilege, _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_FEATURE)));
      }

      if (isGlobalPrivilege) {
        return this.rankScenarios(scenarios);
      } // Otherwise, this is a space feature privilege


      var includeSpaceBaseScenario = basePrivilegeExplanation.actualPrivilegeSource === _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE || basePrivilegeExplanation.supersededPrivilegeSource === _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE;
      var spaceBasePrivilege = basePrivilegeExplanation.supersededPrivilege || basePrivilegeExplanation.actualPrivilege;

      if (includeSpaceBaseScenario) {
        scenarios.push(_objectSpread({
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE,
          isDirectlyAssigned: false,
          actions: this.getBaseActions(_kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE, spaceBasePrivilege)
        }, this.buildSupercededFields(hasAssignedFeaturePrivilege, assignedFeaturePrivilege, _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_FEATURE)));
      }

      if (!ignoreAssigned) {
        var actions = this.getFeatureActions(featureId, this.getAssignedFeaturePrivilege(privilegeSpec, featureId));
        var directlyAssignedFeaturePrivilegeMorePermissiveThanBase = !(0, _privilege_calculator_utils.areActionsFullyCovered)(this.assignedGlobalBaseActions, actions);
        scenarios.push({
          actualPrivilegeSource: _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_FEATURE,
          isDirectlyAssigned: true,
          directlyAssignedFeaturePrivilegeMorePermissiveThanBase: directlyAssignedFeaturePrivilegeMorePermissiveThanBase,
          actions: actions
        });
      }

      return this.rankScenarios(scenarios);
    }
  }, {
    key: "rankScenarios",
    value: function rankScenarios(scenarios) {
      return scenarios.sort(function (scenario1, scenario2) {
        return scenario1.actualPrivilegeSource - scenario2.actualPrivilegeSource;
      });
    }
  }, {
    key: "getBaseActions",
    value: function getBaseActions(source, privilegeId) {
      switch (source) {
        case _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE:
          return this.assignedGlobalBaseActions;

        case _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE:
          return this.kibanaPrivileges.getSpacesPrivileges().getActions(privilegeId);

        default:
          throw new Error("Cannot get base actions for unsupported privilege source ".concat(_kibana_privilege_calculator_types.PRIVILEGE_SOURCE[source]));
      }
    }
  }, {
    key: "getFeatureActions",
    value: function getFeatureActions(featureId, privilegeId) {
      return this.kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilegeId);
    }
  }, {
    key: "getAssignedFeaturePrivilege",
    value: function getAssignedFeaturePrivilege(privilegeSpec, featureId) {
      var featureEntry = privilegeSpec.feature[featureId] || [];
      return featureEntry[0] || _constants.NO_PRIVILEGE_VALUE;
    }
  }, {
    key: "buildSupercededFields",
    value: function buildSupercededFields(isSuperceding, supersededPrivilege, supersededPrivilegeSource) {
      if (!isSuperceding) {
        return {};
      }

      return {
        supersededPrivilege: supersededPrivilege,
        supersededPrivilegeSource: supersededPrivilegeSource
      };
    }
  }]);

  return KibanaFeaturePrivilegeCalculator;
}();

exports.KibanaFeaturePrivilegeCalculator = KibanaFeaturePrivilegeCalculator;