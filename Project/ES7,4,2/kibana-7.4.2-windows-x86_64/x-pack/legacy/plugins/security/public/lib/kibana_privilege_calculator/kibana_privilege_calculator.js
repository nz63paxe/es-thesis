"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaPrivilegeCalculator = void 0;

var _privilege_utils = require("../privilege_utils");

var _kibana_allowed_privileges_calculator = require("./kibana_allowed_privileges_calculator");

var _kibana_base_privilege_calculator = require("./kibana_base_privilege_calculator");

var _kibana_feature_privilege_calculator = require("./kibana_feature_privilege_calculator");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var KibanaPrivilegeCalculator =
/*#__PURE__*/
function () {
  function KibanaPrivilegeCalculator(kibanaPrivileges, role, rankedFeaturePrivileges) {
    _classCallCheck(this, KibanaPrivilegeCalculator);

    this.kibanaPrivileges = kibanaPrivileges;
    this.role = role;
    this.rankedFeaturePrivileges = rankedFeaturePrivileges;

    _defineProperty(this, "allowedPrivilegesCalculator", void 0);

    _defineProperty(this, "effectiveBasePrivilegesCalculator", void 0);

    _defineProperty(this, "effectiveFeaturePrivilegesCalculator", void 0);

    var globalPrivilege = this.locateGlobalPrivilege(role);
    var assignedGlobalBaseActions = globalPrivilege.base[0] ? kibanaPrivileges.getGlobalPrivileges().getActions(globalPrivilege.base[0]) : [];
    this.allowedPrivilegesCalculator = new _kibana_allowed_privileges_calculator.KibanaAllowedPrivilegesCalculator(kibanaPrivileges, role);
    this.effectiveBasePrivilegesCalculator = new _kibana_base_privilege_calculator.KibanaBasePrivilegeCalculator(kibanaPrivileges, globalPrivilege, assignedGlobalBaseActions);
    this.effectiveFeaturePrivilegesCalculator = new _kibana_feature_privilege_calculator.KibanaFeaturePrivilegeCalculator(kibanaPrivileges, globalPrivilege, assignedGlobalBaseActions, rankedFeaturePrivileges);
  }

  _createClass(KibanaPrivilegeCalculator, [{
    key: "calculateEffectivePrivileges",
    value: function calculateEffectivePrivileges() {
      var _this = this;

      var ignoreAssigned = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var _this$role$kibana = this.role.kibana,
          kibana = _this$role$kibana === void 0 ? [] : _this$role$kibana;
      return kibana.map(function (privilegeSpec) {
        return _this.calculateEffectivePrivilege(privilegeSpec, ignoreAssigned);
      });
    }
  }, {
    key: "calculateAllowedPrivileges",
    value: function calculateAllowedPrivileges() {
      var effectivePrivs = this.calculateEffectivePrivileges(true);
      return this.allowedPrivilegesCalculator.calculateAllowedPrivileges(effectivePrivs);
    }
  }, {
    key: "calculateEffectivePrivilege",
    value: function calculateEffectivePrivilege(privilegeSpec, ignoreAssigned) {
      var _this2 = this;

      var result = {
        base: this.effectiveBasePrivilegesCalculator.getMostPermissiveBasePrivilege(privilegeSpec, ignoreAssigned),
        feature: {},
        reserved: privilegeSpec._reserved
      }; // If calculations wish to ignoreAssigned, then we still need to know what the real effective base privilege is
      // without ignoring assigned, in order to calculate the correct feature privileges.

      var effectiveBase = ignoreAssigned ? this.effectiveBasePrivilegesCalculator.getMostPermissiveBasePrivilege(privilegeSpec, false) : result.base;
      var allFeaturePrivileges = this.kibanaPrivileges.getFeaturePrivileges().getAllPrivileges();
      result.feature = Object.keys(allFeaturePrivileges).reduce(function (acc, featureId) {
        return _objectSpread({}, acc, _defineProperty({}, featureId, _this2.effectiveFeaturePrivilegesCalculator.getMostPermissiveFeaturePrivilege(privilegeSpec, effectiveBase, featureId, ignoreAssigned)));
      }, {});
      return result;
    }
  }, {
    key: "locateGlobalPrivilege",
    value: function locateGlobalPrivilege(role) {
      var spacePrivileges = role.kibana;
      return spacePrivileges.find(function (privileges) {
        return (0, _privilege_utils.isGlobalPrivilegeDefinition)(privileges);
      }) || {
        spaces: [],
        base: [],
        feature: {}
      };
    }
  }]);

  return KibanaPrivilegeCalculator;
}();

exports.KibanaPrivilegeCalculator = KibanaPrivilegeCalculator;