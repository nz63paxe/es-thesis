"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EditRolePage = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = require("lodash");

var _react2 = _interopRequireWildcard(require("react"));

var _notify = require("ui/notify");

var _model = require("../../../../../common/model");

var _role_utils = require("../../../../lib/role_utils");

var _objects = require("../../../../objects");

var _management_urls = require("../../management_urls");

var _validate_role = require("../lib/validate_role");

var _delete_role_button = require("./delete_role_button");

var _privileges = require("./privileges");

var _reserved_role_badge = require("./reserved_role_badge");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var EditRolePageUI =
/*#__PURE__*/
function (_Component) {
  _inherits(EditRolePageUI, _Component);

  function EditRolePageUI(_props) {
    var _this;

    _classCallCheck(this, EditRolePageUI);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(EditRolePageUI).call(this, _props));

    _defineProperty(_assertThisInitialized(_this), "validator", void 0);

    _defineProperty(_assertThisInitialized(_this), "getFormTitle", function () {
      var titleText;
      var props = {
        tabIndex: 0
      };

      if ((0, _role_utils.isReservedRole)(_this.state.role)) {
        titleText = _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.viewingRoleTitle",
          defaultMessage: "Viewing role"
        });
        props['aria-describedby'] = 'reservedRoleDescription';
      } else if (_this.editingExistingRole()) {
        titleText = _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.editRoleTitle",
          defaultMessage: "Edit role"
        });
      } else {
        titleText = _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.createRoleTitle",
          defaultMessage: "Create role"
        });
      }

      return _react2.default.createElement(_eui.EuiTitle, {
        size: "l"
      }, _react2.default.createElement("h1", props, titleText, " ", _react2.default.createElement(_reserved_role_badge.ReservedRoleBadge, {
        role: _this.state.role
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "getActionButton", function () {
      if (_this.editingExistingRole() && !(0, _role_utils.isReadOnlyRole)(_this.state.role)) {
        return _react2.default.createElement(_eui.EuiFlexItem, {
          grow: false
        }, _react2.default.createElement(_delete_role_button.DeleteRoleButton, {
          canDelete: true,
          onDelete: _this.handleDeleteRole
        }));
      }

      return null;
    });

    _defineProperty(_assertThisInitialized(_this), "getRoleName", function () {
      return _react2.default.createElement(_eui.EuiPanel, null, _react2.default.createElement(_eui.EuiFormRow, _extends({
        label: _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.roleNameFormRowTitle",
          defaultMessage: "Role name"
        }),
        helpText: !(0, _role_utils.isReservedRole)(_this.state.role) && _this.editingExistingRole() ? _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.roleNameFormRowHelpText",
          defaultMessage: "A role's name cannot be changed once it has been created."
        }) : undefined
      }, _this.validator.validateRoleName(_this.state.role)), _react2.default.createElement(_eui.EuiFieldText, {
        name: 'name',
        value: _this.state.role.name || '',
        onChange: _this.onNameChange,
        "data-test-subj": 'roleFormNameInput',
        readOnly: (0, _role_utils.isReservedRole)(_this.state.role) || _this.editingExistingRole()
      })));
    });

    _defineProperty(_assertThisInitialized(_this), "onNameChange", function (e) {
      var rawValue = e.target.value;
      var name = rawValue.replace(/\s/g, '_');

      _this.setState({
        role: _objectSpread({}, _this.state.role, {
          name: name
        })
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onRoleChange", function (role) {
      _this.setState({
        role: role
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getKibanaPrivileges", function () {
      return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_privileges.KibanaPrivilegesRegion, {
        kibanaPrivileges: new _model.KibanaPrivileges(_this.props.kibanaPrivileges),
        spaces: _this.props.spaces,
        spacesEnabled: _this.props.spacesEnabled,
        features: _this.props.features,
        uiCapabilities: _this.props.uiCapabilities,
        editable: !(0, _role_utils.isReadOnlyRole)(_this.state.role),
        role: _this.state.role,
        onChange: _this.onRoleChange,
        validator: _this.validator,
        intl: _this.props.intl
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "getFormButtons", function () {
      if ((0, _role_utils.isReadOnlyRole)(_this.state.role)) {
        return _this.getReturnToRoleListButton();
      }

      return _react2.default.createElement(_eui.EuiFlexGroup, {
        responsive: false
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _this.getSaveButton()), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _this.getCancelButton()), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: true
      }), _this.getActionButton());
    });

    _defineProperty(_assertThisInitialized(_this), "getReturnToRoleListButton", function () {
      return _react2.default.createElement(_eui.EuiButton, {
        onClick: _this.backToRoleList,
        "data-test-subj": "roleFormReturnButton"
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.returnToRoleListButtonLabel",
        defaultMessage: "Return to role list"
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "getSaveButton", function () {
      var saveText = _this.editingExistingRole() ? _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.updateRoleText",
        defaultMessage: "Update role"
      }) : _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.createRoleText",
        defaultMessage: "Create role"
      });
      return _react2.default.createElement(_eui.EuiButton, {
        "data-test-subj": "roleFormSaveButton",
        fill: true,
        onClick: _this.saveRole,
        disabled: (0, _role_utils.isReservedRole)(_this.state.role)
      }, saveText);
    });

    _defineProperty(_assertThisInitialized(_this), "getCancelButton", function () {
      return _react2.default.createElement(_eui.EuiButtonEmpty, {
        "data-test-subj": "roleFormCancelButton",
        onClick: _this.backToRoleList
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.cancelButtonLabel",
        defaultMessage: "Cancel"
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "editingExistingRole", function () {
      return !!_this.props.role.name && _this.props.action === 'edit';
    });

    _defineProperty(_assertThisInitialized(_this), "saveRole", function () {
      _this.validator.enableValidation();

      var result = _this.validator.validateForSave(_this.state.role);

      if (result.isInvalid) {
        _this.setState({
          formError: result
        });
      } else {
        _this.setState({
          formError: null
        });

        var _this$props = _this.props,
            httpClient = _this$props.httpClient,
            intl = _this$props.intl,
            spacesEnabled = _this$props.spacesEnabled;
        (0, _objects.saveRole)(httpClient, _this.state.role, spacesEnabled).then(function () {
          _notify.toastNotifications.addSuccess(intl.formatMessage({
            id: 'xpack.security.management.editRole.roleSuccessfullySavedNotificationMessage',
            defaultMessage: 'Saved role'
          }));

          _this.backToRoleList();
        }).catch(function (error) {
          _notify.toastNotifications.addDanger((0, _lodash.get)(error, 'data.message'));
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleDeleteRole", function () {
      var _this$props2 = _this.props,
          httpClient = _this$props2.httpClient,
          role = _this$props2.role,
          intl = _this$props2.intl;
      (0, _objects.deleteRole)(httpClient, role.name).then(function () {
        _notify.toastNotifications.addSuccess(intl.formatMessage({
          id: 'xpack.security.management.editRole.roleSuccessfullyDeletedNotificationMessage',
          defaultMessage: 'Deleted role'
        }));

        _this.backToRoleList();
      }).catch(function (error) {
        _notify.toastNotifications.addDanger((0, _lodash.get)(error, 'data.message'));
      });
    });

    _defineProperty(_assertThisInitialized(_this), "backToRoleList", function () {
      window.location.hash = _management_urls.ROLES_PATH;
    });

    _this.validator = new _validate_role.RoleValidator({
      shouldValidate: false
    });

    var _role;

    if (_props.action === 'clone') {
      _role = (0, _role_utils.prepareRoleClone)(_props.role);
    } else {
      _role = (0, _role_utils.copyRole)(_props.role);
    }

    _this.state = {
      role: _role,
      formError: null
    };
    return _this;
  }

  _createClass(EditRolePageUI, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      if (this.props.action === 'clone' && (0, _role_utils.isReservedRole)(this.props.role)) {
        this.backToRoleList();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var description = this.props.spacesEnabled ? _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.setPrivilegesToKibanaSpacesDescription",
        defaultMessage: "Set privileges on your Elasticsearch data and control access to your Kibana spaces."
      }) : _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.setPrivilegesToKibanaDescription",
        defaultMessage: "Set privileges on your Elasticsearch data and control access to Kibana."
      });
      return _react2.default.createElement("div", {
        className: "editRolePage"
      }, _react2.default.createElement(_eui.EuiForm, this.state.formError, this.getFormTitle(), _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_eui.EuiText, {
        size: "s"
      }, description), (0, _role_utils.isReservedRole)(this.state.role) && _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiSpacer, {
        size: "s"
      }), _react2.default.createElement(_eui.EuiText, {
        size: "s",
        color: "subdued"
      }, _react2.default.createElement("p", {
        id: "reservedRoleDescription",
        tabIndex: 0
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.modifyingReversedRolesDescription",
        defaultMessage: "Reserved roles are built-in and cannot be removed or modified."
      })))), _react2.default.createElement(_eui.EuiSpacer, null), this.getRoleName(), this.getElasticsearchPrivileges(), this.getKibanaPrivileges(), _react2.default.createElement(_eui.EuiSpacer, null), this.getFormButtons()));
    }
  }, {
    key: "getElasticsearchPrivileges",
    value: function getElasticsearchPrivileges() {
      return _react2.default.createElement("div", null, _react2.default.createElement(_eui.EuiSpacer, null), _react2.default.createElement(_privileges.ElasticsearchPrivileges, {
        role: this.state.role,
        editable: !(0, _role_utils.isReadOnlyRole)(this.state.role),
        httpClient: this.props.httpClient,
        onChange: this.onRoleChange,
        runAsUsers: this.props.runAsUsers,
        validator: this.validator,
        indexPatterns: this.props.indexPatterns,
        builtinESPrivileges: this.props.builtinESPrivileges,
        allowDocumentLevelSecurity: this.props.allowDocumentLevelSecurity,
        allowFieldLevelSecurity: this.props.allowFieldLevelSecurity
      }));
    }
  }]);

  return EditRolePageUI;
}(_react2.Component);

var EditRolePage = (0, _react.injectI18n)(EditRolePageUI);
exports.EditRolePage = EditRolePage;