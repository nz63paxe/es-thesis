"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaAllowedPrivilegesCalculator = void 0;

var _privilege_calculator_utils = require("../../../common/privilege_calculator_utils");

var _constants = require("../../views/management/edit_role/lib/constants");

var _privilege_utils = require("../privilege_utils");

var _kibana_privilege_calculator_types = require("./kibana_privilege_calculator_types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var KibanaAllowedPrivilegesCalculator =
/*#__PURE__*/
function () {
  // reference to the global privilege definition
  // list of privilege actions that comprise the global base privilege
  function KibanaAllowedPrivilegesCalculator(kibanaPrivileges, role) {
    _classCallCheck(this, KibanaAllowedPrivilegesCalculator);

    this.kibanaPrivileges = kibanaPrivileges;
    this.role = role;

    _defineProperty(this, "globalPrivilege", void 0);

    _defineProperty(this, "assignedGlobalBaseActions", void 0);

    this.globalPrivilege = this.locateGlobalPrivilege(role);
    this.assignedGlobalBaseActions = this.globalPrivilege.base[0] ? kibanaPrivileges.getGlobalPrivileges().getActions(this.globalPrivilege.base[0]) : [];
  }

  _createClass(KibanaAllowedPrivilegesCalculator, [{
    key: "calculateAllowedPrivileges",
    value: function calculateAllowedPrivileges(effectivePrivileges) {
      var _this = this;

      var _this$role$kibana = this.role.kibana,
          kibana = _this$role$kibana === void 0 ? [] : _this$role$kibana;
      return kibana.map(function (privilegeSpec, index) {
        return _this.calculateAllowedPrivilege(privilegeSpec, effectivePrivileges[index]);
      });
    }
  }, {
    key: "calculateAllowedPrivilege",
    value: function calculateAllowedPrivilege(privilegeSpec, effectivePrivileges) {
      var _this2 = this;

      var result = {
        base: {
          privileges: [],
          canUnassign: true
        },
        feature: {}
      };

      if ((0, _privilege_utils.isGlobalPrivilegeDefinition)(privilegeSpec)) {
        // nothing can impede global privileges
        result.base.canUnassign = true;
        result.base.privileges = this.kibanaPrivileges.getGlobalPrivileges().getAllPrivileges();
      } else {
        // space base privileges are restricted based on the assigned global privileges
        var spacePrivileges = this.kibanaPrivileges.getSpacesPrivileges().getAllPrivileges();
        result.base.canUnassign = this.assignedGlobalBaseActions.length === 0;
        result.base.privileges = spacePrivileges.filter(function (privilege) {
          // always allowed to assign the calculated effective privilege
          if (privilege === effectivePrivileges.base.actualPrivilege) {
            return true;
          }

          var privilegeActions = _this2.getBaseActions(_kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE, privilege);

          return !(0, _privilege_calculator_utils.areActionsFullyCovered)(_this2.assignedGlobalBaseActions, privilegeActions);
        });
      }

      var allFeaturePrivileges = this.kibanaPrivileges.getFeaturePrivileges().getAllPrivileges();
      result.feature = Object.entries(allFeaturePrivileges).reduce(function (acc, _ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            featureId = _ref2[0],
            featurePrivileges = _ref2[1];

        return _objectSpread({}, acc, _defineProperty({}, featureId, _this2.getAllowedFeaturePrivileges(effectivePrivileges, featureId, featurePrivileges)));
      }, {});
      return result;
    }
  }, {
    key: "getAllowedFeaturePrivileges",
    value: function getAllowedFeaturePrivileges(effectivePrivileges, featureId, candidateFeaturePrivileges) {
      var _this3 = this;

      var effectiveFeaturePrivilegeExplanation = effectivePrivileges.feature[featureId];

      if (effectiveFeaturePrivilegeExplanation == null) {
        throw new Error('To calculate allowed feature privileges, we need the effective privileges');
      }

      var effectiveFeatureActions = this.getFeatureActions(featureId, effectiveFeaturePrivilegeExplanation.actualPrivilege);
      var privileges = [];

      if (effectiveFeaturePrivilegeExplanation.actualPrivilege !== _constants.NO_PRIVILEGE_VALUE) {
        // Always allowed to assign the calculated effective privilege
        privileges.push(effectiveFeaturePrivilegeExplanation.actualPrivilege);
      }

      privileges.push.apply(privileges, _toConsumableArray(candidateFeaturePrivileges.filter(function (privilegeId) {
        var candidateActions = _this3.getFeatureActions(featureId, privilegeId);

        return (0, _privilege_calculator_utils.compareActions)(effectiveFeatureActions, candidateActions) > 0;
      })));
      var result = {
        privileges: privileges.sort(),
        canUnassign: effectiveFeaturePrivilegeExplanation.actualPrivilege === _constants.NO_PRIVILEGE_VALUE
      };
      return result;
    }
  }, {
    key: "getBaseActions",
    value: function getBaseActions(source, privilegeId) {
      switch (source) {
        case _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.GLOBAL_BASE:
          return this.assignedGlobalBaseActions;

        case _kibana_privilege_calculator_types.PRIVILEGE_SOURCE.SPACE_BASE:
          return this.kibanaPrivileges.getSpacesPrivileges().getActions(privilegeId);

        default:
          throw new Error("Cannot get base actions for unsupported privilege source ".concat(_kibana_privilege_calculator_types.PRIVILEGE_SOURCE[source]));
      }
    }
  }, {
    key: "getFeatureActions",
    value: function getFeatureActions(featureId, privilegeId) {
      return this.kibanaPrivileges.getFeaturePrivileges().getActions(featureId, privilegeId);
    }
  }, {
    key: "locateGlobalPrivilege",
    value: function locateGlobalPrivilege(role) {
      var spacePrivileges = role.kibana;
      return spacePrivileges.find(function (privileges) {
        return (0, _privilege_utils.isGlobalPrivilegeDefinition)(privileges);
      }) || {
        spaces: [],
        base: [],
        feature: {}
      };
    }
  }]);

  return KibanaAllowedPrivilegesCalculator;
}();

exports.KibanaAllowedPrivilegesCalculator = KibanaAllowedPrivilegesCalculator;