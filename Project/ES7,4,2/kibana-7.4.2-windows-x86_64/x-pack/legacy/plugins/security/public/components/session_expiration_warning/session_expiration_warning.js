"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SessionExpirationWarning = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SessionExpirationWarning = function SessionExpirationWarning(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.security.components.sessionExpiration.logoutNotification",
    defaultMessage: "You will soon be logged out due to inactivity. Click OK to resume."
  })), _react.default.createElement("div", {
    className: "eui-textRight"
  }, _react.default.createElement(_eui.EuiButton, {
    size: "s",
    color: "warning",
    onClick: props.onRefreshSession,
    "data-test-subj": "refreshSessionButton"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.security.components.sessionExpiration.okButtonText",
    defaultMessage: "OK"
  }))));
};

exports.SessionExpirationWarning = SessionExpirationWarning;