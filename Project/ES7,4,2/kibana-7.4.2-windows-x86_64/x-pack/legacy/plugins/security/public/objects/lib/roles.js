"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saveRole = saveRole;
exports.deleteRole = deleteRole;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _role_utils = require("../../lib/role_utils");

var _transform_role_for_save = require("../../lib/transform_role_for_save");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var apiBase = _chrome.default.addBasePath("/api/security/role");

function saveRole(_x, _x2, _x3) {
  return _saveRole.apply(this, arguments);
}

function _saveRole() {
  _saveRole = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee($http, role, spacesEnabled) {
    var data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            data = (0, _transform_role_for_save.transformRoleForSave)((0, _role_utils.copyRole)(role), spacesEnabled);
            _context.next = 3;
            return $http.put("".concat(apiBase, "/").concat(role.name), data);

          case 3:
            return _context.abrupt("return", _context.sent);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _saveRole.apply(this, arguments);
}

function deleteRole(_x4, _x5) {
  return _deleteRole.apply(this, arguments);
}

function _deleteRole() {
  _deleteRole = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2($http, name) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return $http.delete("".concat(apiBase, "/").concat(name));

          case 2:
            return _context2.abrupt("return", _context2.sent);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _deleteRole.apply(this, arguments);
}