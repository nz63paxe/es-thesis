"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  KibanaPrivilegeCalculatorFactory: true
};
Object.defineProperty(exports, "KibanaPrivilegeCalculatorFactory", {
  enumerable: true,
  get: function get() {
    return _kibana_privileges_calculator_factory.KibanaPrivilegeCalculatorFactory;
  }
});

var _kibana_privileges_calculator_factory = require("./kibana_privileges_calculator_factory");

var _kibana_privilege_calculator_types = require("./kibana_privilege_calculator_types");

Object.keys(_kibana_privilege_calculator_types).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _kibana_privilege_calculator_types[key];
    }
  });
});