"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "LoginPage", {
  enumerable: true,
  get: function get() {
    return _login_page.LoginPage;
  }
});

var _login_page = require("./login_page");