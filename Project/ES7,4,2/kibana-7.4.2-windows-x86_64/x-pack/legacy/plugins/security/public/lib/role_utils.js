"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isRoleEnabled = isRoleEnabled;
exports.isReservedRole = isReservedRole;
exports.isReadOnlyRole = isReadOnlyRole;
exports.copyRole = copyRole;
exports.prepareRoleClone = prepareRoleClone;

var _lodash = require("lodash");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns whether given role is enabled or not
 *
 * @param role Object Role JSON, as returned by roles API
 * @return Boolean true if role is enabled; false otherwise
 */
function isRoleEnabled(role) {
  return (0, _lodash.get)(role, 'transient_metadata.enabled', true);
}
/**
 * Returns whether given role is reserved or not.
 *
 * @param {role} the Role as returned by roles API
 */


function isReservedRole(role) {
  return (0, _lodash.get)(role, 'metadata._reserved', false);
}
/**
 * Returns whether given role is editable through the UI or not.
 *
 * @param role the Role as returned by roles API
 */


function isReadOnlyRole(role) {
  return isReservedRole(role) || !!(role._transform_error && role._transform_error.length > 0);
}
/**
 * Returns a deep copy of the role.
 *
 * @param role the Role to copy.
 */


function copyRole(role) {
  return (0, _lodash.cloneDeep)(role);
}
/**
 * Creates a deep copy of the role suitable for cloning.
 *
 * @param role the Role to clone.
 */


function prepareRoleClone(role) {
  var clone = copyRole(role);
  clone.name = '';
  return clone;
}