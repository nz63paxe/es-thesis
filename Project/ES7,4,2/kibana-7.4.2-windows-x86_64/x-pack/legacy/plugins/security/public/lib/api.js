"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UserAPIClient = void 0;

var _kfetch = require("ui/kfetch");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var usersUrl = '/api/security/v1/users';
var rolesUrl = '/api/security/role';

var UserAPIClient =
/*#__PURE__*/
function () {
  function UserAPIClient() {
    _classCallCheck(this, UserAPIClient);
  }

  _createClass(UserAPIClient, [{
    key: "getCurrentUser",
    value: function () {
      var _getCurrentUser = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/security/v1/me"
                });

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getCurrentUser() {
        return _getCurrentUser.apply(this, arguments);
      }

      return getCurrentUser;
    }()
  }, {
    key: "getUsers",
    value: function () {
      var _getUsers = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: usersUrl
                });

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getUsers() {
        return _getUsers.apply(this, arguments);
      }

      return getUsers;
    }()
  }, {
    key: "getUser",
    value: function () {
      var _getUser = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(username) {
        var url;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                url = "".concat(usersUrl, "/").concat(encodeURIComponent(username));
                _context3.next = 3;
                return (0, _kfetch.kfetch)({
                  pathname: url
                });

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 4:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function getUser(_x) {
        return _getUser.apply(this, arguments);
      }

      return getUser;
    }()
  }, {
    key: "deleteUser",
    value: function () {
      var _deleteUser = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(username) {
        var url;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                url = "".concat(usersUrl, "/").concat(encodeURIComponent(username));
                _context4.next = 3;
                return (0, _kfetch.kfetch)({
                  pathname: url,
                  method: 'DELETE'
                }, {});

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function deleteUser(_x2) {
        return _deleteUser.apply(this, arguments);
      }

      return deleteUser;
    }()
  }, {
    key: "saveUser",
    value: function () {
      var _saveUser = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(user) {
        var url;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                url = "".concat(usersUrl, "/").concat(encodeURIComponent(user.username));
                _context5.next = 3;
                return (0, _kfetch.kfetch)({
                  pathname: url,
                  body: JSON.stringify(user),
                  method: 'POST'
                });

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function saveUser(_x3) {
        return _saveUser.apply(this, arguments);
      }

      return saveUser;
    }()
  }, {
    key: "getRoles",
    value: function () {
      var _getRoles = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6() {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: rolesUrl
                });

              case 2:
                return _context6.abrupt("return", _context6.sent);

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function getRoles() {
        return _getRoles.apply(this, arguments);
      }

      return getRoles;
    }()
  }, {
    key: "getRole",
    value: function () {
      var _getRole = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7(name) {
        var url;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                url = "".concat(rolesUrl, "/").concat(encodeURIComponent(name));
                _context7.next = 3;
                return (0, _kfetch.kfetch)({
                  pathname: url
                });

              case 3:
                return _context7.abrupt("return", _context7.sent);

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function getRole(_x4) {
        return _getRole.apply(this, arguments);
      }

      return getRole;
    }()
  }, {
    key: "changePassword",
    value: function () {
      var _changePassword = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee8(username, password, currentPassword) {
        var data;
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                data = {
                  newPassword: password
                };

                if (currentPassword) {
                  data.password = currentPassword;
                }

                _context8.next = 4;
                return (0, _kfetch.kfetch)({
                  pathname: "".concat(usersUrl, "/").concat(encodeURIComponent(username), "/password"),
                  method: 'POST',
                  body: JSON.stringify(data)
                });

              case 4:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function changePassword(_x5, _x6, _x7) {
        return _changePassword.apply(this, arguments);
      }

      return changePassword;
    }()
  }]);

  return UserAPIClient;
}();

exports.UserAPIClient = UserAPIClient;