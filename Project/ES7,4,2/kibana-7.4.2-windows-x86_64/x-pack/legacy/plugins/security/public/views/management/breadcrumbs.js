"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getUsersBreadcrumbs = getUsersBreadcrumbs;
exports.getEditUserBreadcrumbs = getEditUserBreadcrumbs;
exports.getCreateUserBreadcrumbs = getCreateUserBreadcrumbs;
exports.getRolesBreadcrumbs = getRolesBreadcrumbs;
exports.getEditRoleBreadcrumbs = getEditRoleBreadcrumbs;
exports.getCreateRoleBreadcrumbs = getCreateRoleBreadcrumbs;

var _i18n = require("@kbn/i18n");

var _breadcrumbs = require("ui/management/breadcrumbs");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function getUsersBreadcrumbs() {
  return [_breadcrumbs.MANAGEMENT_BREADCRUMB, {
    text: _i18n.i18n.translate('xpack.security.users.breadcrumb', {
      defaultMessage: 'Users'
    }),
    href: '#/management/security/users'
  }];
}

function getEditUserBreadcrumbs($route) {
  var username = $route.current.params.username;
  return [].concat(_toConsumableArray(getUsersBreadcrumbs()), [{
    text: username,
    href: "#/management/security/users/edit/".concat(username)
  }]);
}

function getCreateUserBreadcrumbs() {
  return [].concat(_toConsumableArray(getUsersBreadcrumbs()), [{
    text: _i18n.i18n.translate('xpack.security.users.createBreadcrumb', {
      defaultMessage: 'Create'
    })
  }]);
}

function getRolesBreadcrumbs() {
  return [_breadcrumbs.MANAGEMENT_BREADCRUMB, {
    text: _i18n.i18n.translate('xpack.security.roles.breadcrumb', {
      defaultMessage: 'Roles'
    }),
    href: '#/management/security/roles'
  }];
}

function getEditRoleBreadcrumbs($route) {
  var name = $route.current.params.name;
  return [].concat(_toConsumableArray(getRolesBreadcrumbs()), [{
    text: name,
    href: "#/management/security/roles/edit/".concat(name)
  }]);
}

function getCreateRoleBreadcrumbs() {
  return [].concat(_toConsumableArray(getUsersBreadcrumbs()), [{
    text: _i18n.i18n.translate('xpack.security.roles.createBreadcrumb', {
      defaultMessage: 'Create'
    })
  }]);
}