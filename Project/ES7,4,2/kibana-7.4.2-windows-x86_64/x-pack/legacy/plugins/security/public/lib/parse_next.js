"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.parseNext = parseNext;

var _url = require("url");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function parseNext(href) {
  var basePath = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

  var _parse = (0, _url.parse)(href, true),
      query = _parse.query,
      hash = _parse.hash;

  if (!query.next) {
    return "".concat(basePath, "/");
  }

  var next;

  if (Array.isArray(query.next) && query.next.length > 0) {
    next = query.next[0];
  } else {
    next = query.next;
  } // validate that `next` is not attempting a redirect to somewhere
  // outside of this Kibana install


  var _parse2 = (0, _url.parse)(next, false
  /* parseQueryString */
  , true
  /* slashesDenoteHost */
  ),
      protocol = _parse2.protocol,
      hostname = _parse2.hostname,
      port = _parse2.port,
      pathname = _parse2.pathname; // We should explicitly compare `protocol`, `port` and `hostname` to null to make sure these are not
  // detected in the URL at all. For example `hostname` can be empty string for Node URL parser, but
  // browser (because of various bwc reasons) processes URL differently (e.g. `///abc.com` - for browser
  // hostname is `abc.com`, but for Node hostname is an empty string i.e. everything between schema (`//`)
  // and the first slash that belongs to path.


  if (protocol !== null || hostname !== null || port !== null) {
    return "".concat(basePath, "/");
  }

  if (!String(pathname).startsWith(basePath)) {
    return "".concat(basePath, "/");
  }

  return query.next + (hash || '');
}