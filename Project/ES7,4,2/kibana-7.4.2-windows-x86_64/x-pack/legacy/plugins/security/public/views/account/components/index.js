"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AccountManagementPage", {
  enumerable: true,
  get: function get() {
    return _account_management_page.AccountManagementPage;
  }
});

var _account_management_page = require("./account_management_page");