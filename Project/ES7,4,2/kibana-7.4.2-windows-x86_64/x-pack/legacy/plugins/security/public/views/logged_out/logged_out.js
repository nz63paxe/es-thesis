"use strict";

var _react = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _authentication_state_page = require("plugins/security/components/authentication_state_page");

var _logged_out = _interopRequireDefault(require("plugins/security/views/logged_out/logged_out.html"));

var _react2 = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _i18n = require("ui/i18n");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
_chrome.default.setVisible(false).setRootTemplate(_logged_out.default).setRootController('logout', function ($scope) {
  $scope.$$postDigest(function () {
    var domNode = document.getElementById('reactLoggedOutRoot');
    (0, _reactDom.render)(_react2.default.createElement(_i18n.I18nContext, null, _react2.default.createElement(_authentication_state_page.AuthenticationStatePage, {
      title: _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.loggedOut.title",
        defaultMessage: "Successfully logged out"
      })
    }, _react2.default.createElement(_eui.EuiButton, {
      href: _chrome.default.addBasePath('/')
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.security.loggedOut.login",
      defaultMessage: "Login"
    })))), domNode);
  });
});