"use strict";

var _react = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _react2 = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _i18n = require("ui/i18n");

var _authentication_state_page = require("../../components/authentication_state_page");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
_chrome.default.setVisible(false).setRootTemplate('<div id="reactOverwrittenSessionRoot" />').setRootController('overwritten_session', function ($scope, ShieldUser) {
  $scope.$$postDigest(function () {
    ShieldUser.getCurrent().$promise.then(function (user) {
      var overwrittenSessionPage = _react2.default.createElement(_i18n.I18nContext, null, _react2.default.createElement(_authentication_state_page.AuthenticationStatePage, {
        title: _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.overwrittenSession.title",
          defaultMessage: "You previously logged in as a different user."
        })
      }, _react2.default.createElement(_eui.EuiButton, {
        href: _chrome.default.addBasePath('/')
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.overwrittenSession.continueAsUserText",
        defaultMessage: "Continue as {username}",
        values: {
          username: user.username
        }
      }))));

      (0, _reactDom.render)(overwrittenSessionPage, document.getElementById('reactOverwrittenSessionRoot'));
    });
  });
});