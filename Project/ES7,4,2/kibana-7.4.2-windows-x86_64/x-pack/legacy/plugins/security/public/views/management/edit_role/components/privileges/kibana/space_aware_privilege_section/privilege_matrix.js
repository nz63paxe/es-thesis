"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PrivilegeMatrix = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _components = require("../../../../../../../../../spaces/public/components");

var _privilege_utils = require("../../../../../../../lib/privilege_utils");

var _spaces_popover_list = require("../../../spaces_popover_list");

var _privilege_display = require("./privilege_display");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SPACES_DISPLAY_COUNT = 4;

var PrivilegeMatrix =
/*#__PURE__*/
function (_Component) {
  _inherits(PrivilegeMatrix, _Component);

  function PrivilegeMatrix() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, PrivilegeMatrix);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(PrivilegeMatrix)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      showModal: false
    });

    _defineProperty(_assertThisInitialized(_this), "renderTable", function () {
      var _this$props = _this.props,
          role = _this$props.role,
          features = _this$props.features,
          intl = _this$props.intl;
      var spacePrivileges = role.kibana;

      var globalPrivilege = _this.locateGlobalPrivilege();

      var spacesColumns = [];
      spacePrivileges.forEach(function (spacePrivs, spacesIndex) {
        spacesColumns.push({
          isGlobal: (0, _privilege_utils.isGlobalPrivilegeDefinition)(spacePrivs),
          spacesIndex: spacesIndex,
          spaces: spacePrivs.spaces.map(function (spaceId) {
            return _this.props.spaces.find(function (space) {
              return space.id === spaceId;
            });
          }).filter(Boolean),
          privileges: {
            base: spacePrivs.base,
            feature: spacePrivs.feature
          }
        });
      });
      var rows = [{
        feature: {
          id: '*base*',
          isBase: true,
          name: intl.formatMessage({
            id: 'xpack.security.management.editRole.spacePrivilegeMatrix.basePrivilegeText',
            defaultMessage: 'Base privilege'
          }),
          app: [],
          privileges: {}
        },
        role: role
      }].concat(_toConsumableArray(features.map(function (feature) {
        return {
          feature: _objectSpread({}, feature, {
            isBase: false
          }),
          role: role
        };
      })));
      var columns = [{
        field: 'feature',
        name: intl.formatMessage({
          id: 'xpack.security.management.editRole.spacePrivilegeMatrix.featureColumnTitle',
          defaultMessage: 'Feature'
        }),
        width: '230px',
        render: function render(feature) {
          return feature.isBase ? _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement("strong", null, feature.name), _react2.default.createElement(_eui.EuiIconTip, {
            iconProps: {
              className: 'eui-alignTop'
            },
            type: "questionInCircle",
            content: intl.formatMessage({
              id: 'xpack.security.management.editRole.spacePrivilegeMatrix.basePrivilegeTooltip',
              defaultMessage: 'The base privilege is automatically granted to all features.'
            }),
            color: "subdued"
          })) : _react2.default.createElement(_react2.Fragment, null, feature.icon && _react2.default.createElement(_eui.EuiIcon, {
            className: "secPrivilegeFeatureIcon",
            size: "m",
            type: feature.icon
          }), feature.name);
        }
      }].concat(_toConsumableArray(spacesColumns.map(function (item) {
        var columnWidth;

        if (item.isGlobal) {
          columnWidth = '100px';
        } else if (item.spaces.length - SPACES_DISPLAY_COUNT) {
          columnWidth = '90px';
        } else {
          columnWidth = '80px';
        }

        return {
          // TODO: this is a hacky way to determine if we are looking at the global feature
          // used for cellProps below...
          field: item.isGlobal ? 'global' : 'feature',
          width: columnWidth,
          name: _react2.default.createElement("div", null, item.spaces.slice(0, SPACES_DISPLAY_COUNT).map(function (space) {
            return _react2.default.createElement("span", {
              key: space.id
            }, _react2.default.createElement(_components.SpaceAvatar, {
              size: "s",
              space: space
            }), ' ', item.isGlobal && _react2.default.createElement("span", null, _react2.default.createElement(_react.FormattedMessage, {
              id: "xpack.security.management.editRole.spacePrivilegeMatrix.globalSpaceName",
              defaultMessage: "Global"
            }), _react2.default.createElement("br", null), _react2.default.createElement(_spaces_popover_list.SpacesPopoverList, {
              spaces: _this.props.spaces.filter(function (s) {
                return s.id !== '*';
              }),
              intl: _this.props.intl,
              buttonText: _this.props.intl.formatMessage({
                id: 'xpack.security.management.editRole.spacePrivilegeMatrix.showAllSpacesLink',
                defaultMessage: '(all spaces)'
              })
            })));
          }), item.spaces.length > SPACES_DISPLAY_COUNT && _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement("br", null), _react2.default.createElement(_spaces_popover_list.SpacesPopoverList, {
            spaces: item.spaces,
            intl: _this.props.intl,
            buttonText: _this.props.intl.formatMessage({
              id: 'xpack.security.management.editRole.spacePrivilegeMatrix.showNMoreSpacesLink',
              defaultMessage: '+{count} more'
            }, {
              count: item.spaces.length - SPACES_DISPLAY_COUNT
            })
          }))),
          render: function render(feature, record) {
            return _this.renderPrivilegeDisplay(item, record, globalPrivilege.base);
          }
        };
      })));
      return (// @ts-ignore missing rowProps from typedef
        _react2.default.createElement(_eui.EuiInMemoryTable, {
          columns: columns,
          items: rows // @ts-ignore missing rowProps from typedef
          ,
          rowProps: function rowProps(item) {
            return {
              className: item.feature.isBase ? 'secPrivilegeMatrix__row--isBasePrivilege' : ''
            };
          },
          cellProps: function cellProps(item, column) {
            return {
              className: column.field === 'global' ? 'secPrivilegeMatrix__cell--isGlobalPrivilege' : ''
            };
          }
        })
      );
    });

    _defineProperty(_assertThisInitialized(_this), "renderPrivilegeDisplay", function (column, _ref, globalBasePrivilege) {
      var feature = _ref.feature;

      if (column.isGlobal) {
        if (feature.isBase) {
          return _react2.default.createElement(_privilege_display.PrivilegeDisplay, {
            privilege: globalBasePrivilege
          });
        }

        var featureCalculatedPrivilege = _this.props.calculatedPrivileges[column.spacesIndex].feature[feature.id];
        return _react2.default.createElement(_privilege_display.PrivilegeDisplay, {
          privilege: featureCalculatedPrivilege && featureCalculatedPrivilege.actualPrivilege
        });
      } else {
        // not global
        var calculatedPrivilege = _this.props.calculatedPrivileges[column.spacesIndex];

        if (feature.isBase) {
          // Space base privilege
          var actualBasePrivileges = calculatedPrivilege.base.actualPrivilege;
          return _react2.default.createElement(_privilege_display.PrivilegeDisplay, {
            explanation: calculatedPrivilege.base,
            privilege: actualBasePrivileges
          });
        }

        var featurePrivilegeExplanation = calculatedPrivilege.feature[feature.id];
        return _react2.default.createElement(_privilege_display.PrivilegeDisplay, {
          explanation: featurePrivilegeExplanation,
          privilege: featurePrivilegeExplanation && featurePrivilegeExplanation.actualPrivilege
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "locateGlobalPrivilege", function () {
      return _this.props.role.kibana.find(function (spacePriv) {
        return (0, _privilege_utils.isGlobalPrivilegeDefinition)(spacePriv);
      }) || {
        spaces: ['*'],
        base: [],
        feature: []
      };
    });

    _defineProperty(_assertThisInitialized(_this), "hideModal", function () {
      _this.setState({
        showModal: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "showModal", function () {
      _this.setState({
        showModal: true
      });
    });

    return _this;
  }

  _createClass(PrivilegeMatrix, [{
    key: "render",
    value: function render() {
      var modal = null;

      if (this.state.showModal) {
        modal = _react2.default.createElement(_eui.EuiOverlayMask, null, _react2.default.createElement(_eui.EuiModal, {
          className: "secPrivilegeMatrix__modal",
          onClose: this.hideModal
        }, _react2.default.createElement(_eui.EuiModalHeader, null, _react2.default.createElement(_eui.EuiModalHeaderTitle, null, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.spacePrivilegeMatrix.modalTitle",
          defaultMessage: "Privilege summary"
        }))), _react2.default.createElement(_eui.EuiModalBody, null, this.renderTable()), _react2.default.createElement(_eui.EuiModalFooter, null, _react2.default.createElement(_eui.EuiButton, {
          onClick: this.hideModal,
          fill: true
        }, _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.security.management.editRole.spacePrivilegeMatrix.closeButton",
          defaultMessage: "Close"
        })))));
      }

      return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiButtonEmpty, {
        onClick: this.showModal
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.security.management.editRole.spacePrivilegeMatrix.showSummaryText",
        defaultMessage: "View privilege summary"
      })), modal);
    }
  }]);

  return PrivilegeMatrix;
}(_react2.Component);

exports.PrivilegeMatrix = PrivilegeMatrix;