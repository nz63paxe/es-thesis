"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "UsersListPage", {
  enumerable: true,
  get: function get() {
    return _users_list_page.UsersListPage;
  }
});

var _users_list_page = require("./users_list_page");