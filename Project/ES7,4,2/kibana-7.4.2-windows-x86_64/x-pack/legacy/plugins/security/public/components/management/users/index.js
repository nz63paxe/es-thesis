"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ConfirmDeleteUsers", {
  enumerable: true,
  get: function get() {
    return _confirm_delete.ConfirmDeleteUsers;
  }
});

var _confirm_delete = require("./confirm_delete");