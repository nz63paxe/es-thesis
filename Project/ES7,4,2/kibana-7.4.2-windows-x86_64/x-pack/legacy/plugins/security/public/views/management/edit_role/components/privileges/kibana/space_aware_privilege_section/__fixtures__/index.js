"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "rawKibanaPrivileges", {
  enumerable: true,
  get: function get() {
    return _raw_kibana_privileges.rawKibanaPrivileges;
  }
});

var _raw_kibana_privileges = require("./raw_kibana_privileges");