"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkSavedObjectsPrivilegesWithRequestFactory = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const checkSavedObjectsPrivilegesWithRequestFactory = (checkPrivilegesWithRequest, spaces) => {
  return function checkSavedObjectsPrivilegesWithRequest(request) {
    return async function checkSavedObjectsPrivileges(actions, namespace) {
      if (spaces.isEnabled) {
        return checkPrivilegesWithRequest(request).atSpace(spaces.namespaceToSpaceId(namespace), actions);
      }

      return checkPrivilegesWithRequest(request).globally(actions);
    };
  };
};

exports.checkSavedObjectsPrivilegesWithRequestFactory = checkSavedObjectsPrivilegesWithRequestFactory;