"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initPrivilegesApi = initPrivilegesApi;

var _route_pre_check_license = require("../../../../lib/route_pre_check_license");

var _get = require("./get");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
function initPrivilegesApi(server) {
  const routePreCheckLicenseFn = (0, _route_pre_check_license.routePreCheckLicense)(server);
  (0, _get.initGetPrivilegesApi)(server, routePreCheckLicenseFn);
}