"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createAuthorizationService = createAuthorizationService;

var _get_client_shield = require("../../../../../server/lib/get_client_shield");

var _constants = require("../../../common/constants");

var _actions = require("./actions");

var _check_privileges = require("./check_privileges");

var _check_privileges_dynamically = require("./check_privileges_dynamically");

var _mode = require("./mode");

var _privileges = require("./privileges");

var _check_saved_objects_privileges = require("./check_saved_objects_privileges");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createAuthorizationService(server, securityXPackFeature, xpackMainPlugin, spaces) {
  const shieldClient = (0, _get_client_shield.getClient)(server);
  const config = server.config();
  const actions = (0, _actions.actionsFactory)(config);
  const application = `${_constants.APPLICATION_PREFIX}${config.get('kibana.index')}`;
  const checkPrivilegesWithRequest = (0, _check_privileges.checkPrivilegesWithRequestFactory)(actions, application, shieldClient);
  const checkPrivilegesDynamicallyWithRequest = (0, _check_privileges_dynamically.checkPrivilegesDynamicallyWithRequestFactory)(checkPrivilegesWithRequest, spaces);
  const checkSavedObjectsPrivilegesWithRequest = (0, _check_saved_objects_privileges.checkSavedObjectsPrivilegesWithRequestFactory)(checkPrivilegesWithRequest, spaces);
  const mode = (0, _mode.authorizationModeFactory)(securityXPackFeature);
  const privileges = (0, _privileges.privilegesFactory)(actions, xpackMainPlugin);
  return {
    actions,
    application,
    checkPrivilegesWithRequest,
    checkPrivilegesDynamicallyWithRequest,
    checkSavedObjectsPrivilegesWithRequest,
    mode,
    privileges
  };
}