"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "privilegesFactory", {
  enumerable: true,
  get: function () {
    return _privileges.privilegesFactory;
  }
});
Object.defineProperty(exports, "PrivilegesService", {
  enumerable: true,
  get: function () {
    return _privileges.PrivilegesService;
  }
});

var _privileges = require("./privileges");