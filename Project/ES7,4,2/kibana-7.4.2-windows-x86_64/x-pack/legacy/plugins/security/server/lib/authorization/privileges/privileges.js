"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.privilegesFactory = privilegesFactory;

var _lodash = require("lodash");

var _feature_privilege_builder = require("./feature_privilege_builder");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function privilegesFactory(actions, xpackMainPlugin) {
  const featurePrivilegeBuilder = (0, _feature_privilege_builder.featurePrivilegeBuilderFactory)(actions);
  return {
    get() {
      const features = xpackMainPlugin.getFeatures();
      const basePrivilegeFeatures = features.filter(feature => !feature.excludeFromBasePrivileges);
      const allActions = (0, _lodash.uniq)((0, _lodash.flatten)(basePrivilegeFeatures.map(feature => Object.values(feature.privileges).reduce((acc, privilege) => {
        if (privilege.excludeFromBasePrivileges) {
          return acc;
        }

        return [...acc, ...featurePrivilegeBuilder.getActions(privilege, feature)];
      }, []))));
      const readActions = (0, _lodash.uniq)((0, _lodash.flatten)(basePrivilegeFeatures.map(feature => Object.entries(feature.privileges).reduce((acc, [privilegeId, privilege]) => {
        if (privilegeId !== 'read' || privilege.excludeFromBasePrivileges) {
          return acc;
        }

        return [...acc, ...featurePrivilegeBuilder.getActions(privilege, feature)];
      }, []))));
      return {
        features: features.reduce((acc, feature) => {
          if (Object.keys(feature.privileges).length > 0) {
            acc[feature.id] = (0, _lodash.mapValues)(feature.privileges, (privilege, privilegeId) => [actions.login, actions.version, ...featurePrivilegeBuilder.getActions(privilege, feature), ...(privilegeId === 'all' ? [actions.allHack] : [])]);
          }

          return acc;
        }, {}),
        global: {
          all: [actions.login, actions.version, actions.api.get('features'), actions.space.manage, actions.ui.get('spaces', 'manage'), actions.ui.get('management', 'kibana', 'spaces'), ...allActions, actions.allHack],
          read: [actions.login, actions.version, ...readActions]
        },
        space: {
          all: [actions.login, actions.version, ...allActions, actions.allHack],
          read: [actions.login, actions.version, ...readActions]
        },
        reserved: features.reduce((acc, feature) => {
          if (feature.reserved) {
            acc[feature.id] = [actions.version, ...featurePrivilegeBuilder.getActions(feature.reserved.privilege, feature)];
          }

          return acc;
        }, {})
      };
    }

  };
}