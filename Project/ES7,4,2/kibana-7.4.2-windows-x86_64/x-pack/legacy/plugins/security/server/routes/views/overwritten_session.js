"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initOverwrittenSessionView = initOverwrittenSessionView;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initOverwrittenSessionView(server) {
  server.route({
    method: 'GET',
    path: '/overwritten_session',

    handler(request, h) {
      return h.renderAppWithDefaultConfig(server.getHiddenUiAppById('overwritten_session'));
    }

  });
}