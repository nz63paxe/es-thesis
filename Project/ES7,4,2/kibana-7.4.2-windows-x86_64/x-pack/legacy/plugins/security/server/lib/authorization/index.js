"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Actions", {
  enumerable: true,
  get: function () {
    return _actions.Actions;
  }
});
Object.defineProperty(exports, "createAuthorizationService", {
  enumerable: true,
  get: function () {
    return _service.createAuthorizationService;
  }
});
Object.defineProperty(exports, "disableUICapabilitesFactory", {
  enumerable: true,
  get: function () {
    return _disable_ui_capabilities.disableUICapabilitesFactory;
  }
});
Object.defineProperty(exports, "initAPIAuthorization", {
  enumerable: true,
  get: function () {
    return _api_authorization.initAPIAuthorization;
  }
});
Object.defineProperty(exports, "initAppAuthorization", {
  enumerable: true,
  get: function () {
    return _app_authorization.initAppAuthorization;
  }
});
Object.defineProperty(exports, "PrivilegeSerializer", {
  enumerable: true,
  get: function () {
    return _privilege_serializer.PrivilegeSerializer;
  }
});
Object.defineProperty(exports, "registerPrivilegesWithCluster", {
  enumerable: true,
  get: function () {
    return _register_privileges_with_cluster.registerPrivilegesWithCluster;
  }
});
Object.defineProperty(exports, "ResourceSerializer", {
  enumerable: true,
  get: function () {
    return _resource_serializer.ResourceSerializer;
  }
});
Object.defineProperty(exports, "validateFeaturePrivileges", {
  enumerable: true,
  get: function () {
    return _validate_feature_privileges.validateFeaturePrivileges;
  }
});

var _actions = require("./actions");

var _service = require("./service");

var _disable_ui_capabilities = require("./disable_ui_capabilities");

var _api_authorization = require("./api_authorization");

var _app_authorization = require("./app_authorization");

var _privilege_serializer = require("./privilege_serializer");

var _register_privileges_with_cluster = require("./register_privileges_with_cluster");

var _resource_serializer = require("./resource_serializer");

var _validate_feature_privileges = require("./validate_feature_privileges");