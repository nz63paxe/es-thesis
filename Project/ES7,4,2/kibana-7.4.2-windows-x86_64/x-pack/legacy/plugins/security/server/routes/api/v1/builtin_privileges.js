"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initGetBuiltinPrivilegesApi = initGetBuiltinPrivilegesApi;

var _get_client_shield = require("../../../../../../server/lib/get_client_shield");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initGetBuiltinPrivilegesApi(server) {
  server.route({
    method: 'GET',
    path: '/api/security/v1/esPrivileges/builtin',

    async handler(req) {
      const callWithRequest = (0, _get_client_shield.getClient)(server).callWithRequest;
      const privileges = await callWithRequest(req, 'shield.getBuiltinPrivileges'); // Exclude the `none` privilege, as it doesn't make sense as an option within the Kibana UI

      privileges.cluster = privileges.cluster.filter(privilege => privilege !== 'none');
      privileges.index = privileges.index.filter(privilege => privilege !== 'none');
      return privileges;
    }

  });
}