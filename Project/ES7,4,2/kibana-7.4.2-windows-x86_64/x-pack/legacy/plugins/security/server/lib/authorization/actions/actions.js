"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.actionsFactory = actionsFactory;
exports.Actions = void 0;

var _api = require("./api");

var _app = require("./app");

var _saved_object = require("./saved_object");

var _space = require("./space");

var _ui = require("./ui");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/** Actions are used to create the "actions" that are associated with Elasticsearch's
 * application privileges, and are used to perform the authorization checks implemented
 * by the various `checkPrivilegesWithRequest` derivatives
 */
class Actions {
  /**
   * The allHack action is used to differentiate the `all` privilege from the `read` privilege
   * for those applications which register the same set of actions for both privileges. This is a
   * temporary hack until we remove this assumption in the role management UI
   */
  constructor(versionNumber) {
    this.versionNumber = versionNumber;

    _defineProperty(this, "allHack", 'allHack:');

    _defineProperty(this, "api", new _api.ApiActions(this.versionNumber));

    _defineProperty(this, "app", new _app.AppActions(this.versionNumber));

    _defineProperty(this, "login", 'login:');

    _defineProperty(this, "savedObject", new _saved_object.SavedObjectActions(this.versionNumber));

    _defineProperty(this, "space", new _space.SpaceActions(this.versionNumber));

    _defineProperty(this, "ui", new _ui.UIActions(this.versionNumber));

    _defineProperty(this, "version", `version:${this.versionNumber}`);
  }

}

exports.Actions = Actions;

function actionsFactory(config) {
  const version = config.get('pkg.version');

  if (typeof version !== 'string') {
    throw new Error('version should be a string');
  }

  if (version === '') {
    throw new Error(`version can't be an empty string`);
  }

  return new Actions(version);
}