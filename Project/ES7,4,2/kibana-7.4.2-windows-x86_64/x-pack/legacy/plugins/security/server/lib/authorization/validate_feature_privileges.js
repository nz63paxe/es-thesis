"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateFeaturePrivileges = validateFeaturePrivileges;

var _privilege_calculator_utils = require("../../../common/privilege_calculator_utils");

var _feature_privilege_builder = require("./privileges/feature_privilege_builder");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function validateFeaturePrivileges(actions, features) {
  const featurePrivilegeBuilder = (0, _feature_privilege_builder.featurePrivilegeBuilderFactory)(actions);

  for (const feature of features) {
    if (feature.privileges.all != null && feature.privileges.read != null) {
      const allActions = featurePrivilegeBuilder.getActions(feature.privileges.all, feature);
      const readActions = featurePrivilegeBuilder.getActions(feature.privileges.read, feature);

      if (!(0, _privilege_calculator_utils.areActionsFullyCovered)(allActions, readActions)) {
        throw new Error(`${feature.id}'s "all" privilege should be a superset of the "read" privilege.`);
      }
    }
  }
}