"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initGetPrivilegesApi = initGetPrivilegesApi;

var _joi = _interopRequireDefault(require("joi"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initGetPrivilegesApi(server, routePreCheckLicenseFn) {
  server.route({
    method: 'GET',
    path: '/api/security/privileges',

    handler(req) {
      const {
        authorization
      } = server.plugins.security;
      const privileges = authorization.privileges.get();

      if (req.query.includeActions) {
        return privileges;
      }

      return {
        global: Object.keys(privileges.global),
        space: Object.keys(privileges.space),
        features: Object.entries(privileges.features).reduce((acc, [featureId, featurePrivileges]) => {
          return { ...acc,
            [featureId]: Object.keys(featurePrivileges)
          };
        }, {}),
        reserved: Object.keys(privileges.reserved)
      };
    },

    config: {
      pre: [routePreCheckLicenseFn],
      validate: {
        query: _joi.default.object().keys({
          includeActions: _joi.default.bool()
        })
      }
    }
  });
}