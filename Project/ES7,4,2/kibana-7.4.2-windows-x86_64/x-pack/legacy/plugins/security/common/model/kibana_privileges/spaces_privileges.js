"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaSpacesPrivileges = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class KibanaSpacesPrivileges {
  constructor(spacesPrivilegesMap) {
    this.spacesPrivilegesMap = spacesPrivilegesMap;
  }

  getAllPrivileges() {
    return Object.keys(this.spacesPrivilegesMap);
  }

  getActions(privilege) {
    return this.spacesPrivilegesMap[privilege] || [];
  }

}

exports.KibanaSpacesPrivileges = KibanaSpacesPrivileges;