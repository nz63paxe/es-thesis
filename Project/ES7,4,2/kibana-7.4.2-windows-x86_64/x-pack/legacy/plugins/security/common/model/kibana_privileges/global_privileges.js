"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaGlobalPrivileges = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class KibanaGlobalPrivileges {
  constructor(globalPrivilegesMap) {
    this.globalPrivilegesMap = globalPrivilegesMap;
  }

  getAllPrivileges() {
    return Object.keys(this.globalPrivilegesMap);
  }

  getActions(privilege) {
    return this.globalPrivilegesMap[privilege] || [];
  }

}

exports.KibanaGlobalPrivileges = KibanaGlobalPrivileges;