"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaPrivileges = void 0;

var _feature_privileges = require("./feature_privileges");

var _global_privileges = require("./global_privileges");

var _spaces_privileges = require("./spaces_privileges");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class KibanaPrivileges {
  constructor(rawKibanaPrivileges) {
    this.rawKibanaPrivileges = rawKibanaPrivileges;
  }

  getGlobalPrivileges() {
    return new _global_privileges.KibanaGlobalPrivileges(this.rawKibanaPrivileges.global);
  }

  getSpacesPrivileges() {
    return new _spaces_privileges.KibanaSpacesPrivileges(this.rawKibanaPrivileges.space);
  }

  getFeaturePrivileges() {
    return new _feature_privileges.KibanaFeaturePrivileges(this.rawKibanaPrivileges.features);
  }

}

exports.KibanaPrivileges = KibanaPrivileges;