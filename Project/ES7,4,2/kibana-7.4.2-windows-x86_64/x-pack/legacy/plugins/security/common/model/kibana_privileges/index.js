"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "KibanaPrivileges", {
  enumerable: true,
  get: function () {
    return _kibana_privileges.KibanaPrivileges;
  }
});

var _kibana_privileges = require("./kibana_privileges");