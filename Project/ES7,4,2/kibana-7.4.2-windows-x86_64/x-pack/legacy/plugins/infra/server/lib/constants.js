"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CLOUD_METRICS_MODULES = exports.IP_FIELDS = exports.NAME_FIELDS = void 0;

var _types = require("../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Used for metadata and snapshots resolvers to find the field that contains
// a displayable name of a node.
// Intentionally not the same as xpack.infra.sources.default.fields.{host,container,pod}.
// TODO: consider moving this to source configuration too.
const NAME_FIELDS = {
  [_types.InfraNodeType.host]: 'host.name',
  [_types.InfraNodeType.pod]: 'kubernetes.pod.name',
  [_types.InfraNodeType.container]: 'container.name'
};
exports.NAME_FIELDS = NAME_FIELDS;
const IP_FIELDS = {
  [_types.InfraNodeType.host]: 'host.ip',
  [_types.InfraNodeType.pod]: 'kubernetes.pod.ip',
  [_types.InfraNodeType.container]: 'container.ip_address'
};
exports.IP_FIELDS = IP_FIELDS;
const CLOUD_METRICS_MODULES = ['aws'];
exports.CLOUD_METRICS_MODULES = CLOUD_METRICS_MODULES;