"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.KibanaMetricsAdapter = void 0;

var _i18n = require("@kbn/i18n");

var _lodash = require("lodash");

var _types = require("../../../graphql/types");

var _check_valid_node = require("./lib/check_valid_node");

var _errors = require("./lib/errors");

var _models = require("./models");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class KibanaMetricsAdapter {
  constructor(framework) {
    _defineProperty(this, "framework", void 0);

    this.framework = framework;
  }

  async getMetrics(req, options) {
    const fields = {
      [_types.InfraNodeType.host]: options.sourceConfiguration.fields.host,
      [_types.InfraNodeType.container]: options.sourceConfiguration.fields.container,
      [_types.InfraNodeType.pod]: options.sourceConfiguration.fields.pod
    };
    const indexPattern = `${options.sourceConfiguration.metricAlias},${options.sourceConfiguration.logAlias}`;
    const timeField = options.sourceConfiguration.fields.timestamp;
    const interval = options.timerange.interval;
    const nodeField = fields[options.nodeType];
    const timerange = {
      min: options.timerange.from,
      max: options.timerange.to
    };

    const search = searchOptions => this.framework.callWithRequest(req, 'search', searchOptions);

    const validNode = await (0, _check_valid_node.checkValidNode)(search, indexPattern, nodeField, options.nodeIds.nodeId);

    if (!validNode) {
      throw new _errors.InvalidNodeError(_i18n.i18n.translate('xpack.infra.kibanaMetrics.nodeDoesNotExistErrorMessage', {
        defaultMessage: '{nodeId} does not exist.',
        values: {
          nodeId: options.nodeIds.nodeId
        }
      }));
    }

    const requests = options.metrics.map(metricId => {
      const model = _models.metricModels[metricId](timeField, indexPattern, interval);

      if (model.id_type === 'cloud' && !options.nodeIds.cloudId) {
        throw new _errors.InvalidNodeError(_i18n.i18n.translate('xpack.infra.kibanaMetrics.cloudIdMissingErrorMessage', {
          defaultMessage: 'Model for {metricId} requires a cloudId, but none was given for {nodeId}.',
          values: {
            metricId,
            nodeId: options.nodeIds.nodeId
          }
        }));
      }

      const id = model.id_type === 'cloud' ? options.nodeIds.cloudId : options.nodeIds.nodeId;
      const filters = model.map_field_to ? [{
        match: {
          [model.map_field_to]: id
        }
      }] : [{
        match: {
          [nodeField]: id
        }
      }];
      return this.framework.makeTSVBRequest(req, model, timerange, filters);
    });
    return Promise.all(requests).then(results => {
      return results.map(result => {
        const metricIds = Object.keys(result).filter(k => !['type', 'uiRestrictions'].includes(k));
        return metricIds.map(id => {
          const infraMetricId = _types.InfraMetric[id];

          if (!infraMetricId) {
            throw new Error(_i18n.i18n.translate('xpack.infra.kibanaMetrics.invalidInfraMetricErrorMessage', {
              defaultMessage: '{id} is not a valid InfraMetric',
              values: {
                id
              }
            }));
          }

          const panel = result[infraMetricId];
          return {
            id: infraMetricId,
            series: panel.series.map(series => {
              return {
                id: series.id,
                data: series.data.map(point => ({
                  timestamp: point[0],
                  value: point[1]
                }))
              };
            })
          };
        });
      });
    }).then(result => (0, _lodash.flatten)(result));
  }

}

exports.KibanaMetricsAdapter = KibanaMetricsAdapter;