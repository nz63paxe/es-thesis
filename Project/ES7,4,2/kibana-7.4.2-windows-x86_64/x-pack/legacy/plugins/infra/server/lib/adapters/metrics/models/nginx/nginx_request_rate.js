"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nginxRequestRate = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const nginxRequestRate = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.nginxRequestRate,
  requires: ['nginx.stubstatus'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'rate',
    metrics: [{
      field: 'nginx.stubstatus.requests',
      id: 'max-requests',
      type: _adapter_types.InfraMetricModelMetricType.max
    }, {
      field: 'max-requests',
      id: 'derv-max-requests',
      type: _adapter_types.InfraMetricModelMetricType.derivative,
      unit: '1s'
    }, {
      id: 'posonly-derv-max-requests',
      type: _adapter_types.InfraMetricModelMetricType.calculation,
      variables: [{
        id: 'var-rate',
        name: 'rate',
        field: 'derv-max-requests'
      }],
      script: 'params.rate > 0.0 ? params.rate : 0.0'
    }],
    split_mode: 'everything'
  }]
});

exports.nginxRequestRate = nginxRequestRate;