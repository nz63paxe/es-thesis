"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLogEntriesResolvers = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

var _sources = require("../../lib/sources");

var _usage_collector = require("../../usage/usage_collector");

var _serialized_query = require("../../utils/serialized_query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createLogEntriesResolvers = libs => ({
  InfraSource: {
    async logEntriesAround(source, args, {
      req
    }) {
      const countBefore = args.countBefore || 0;
      const countAfter = args.countAfter || 0;
      const {
        entriesBefore,
        entriesAfter
      } = await libs.logEntries.getLogEntriesAround(req, source.id, args.key, countBefore + 1, countAfter + 1, (0, _serialized_query.parseFilterQuery)(args.filterQuery));
      const hasMoreBefore = entriesBefore.length > countBefore;
      const hasMoreAfter = entriesAfter.length > countAfter;
      const entries = [...(hasMoreBefore ? entriesBefore.slice(1) : entriesBefore), ...(hasMoreAfter ? entriesAfter.slice(0, -1) : entriesAfter)];
      return {
        start: entries.length > 0 ? entries[0].key : null,
        end: entries.length > 0 ? entries[entries.length - 1].key : null,
        hasMoreBefore,
        hasMoreAfter,
        filterQuery: args.filterQuery,
        entries
      };
    },

    async logEntriesBetween(source, args, {
      req
    }) {
      const entries = await libs.logEntries.getLogEntriesBetween(req, source.id, args.startKey, args.endKey, (0, _serialized_query.parseFilterQuery)(args.filterQuery));
      return {
        start: entries.length > 0 ? entries[0].key : null,
        end: entries.length > 0 ? entries[entries.length - 1].key : null,
        hasMoreBefore: true,
        hasMoreAfter: true,
        filterQuery: args.filterQuery,
        entries
      };
    },

    async logEntryHighlights(source, args, {
      req
    }) {
      const highlightedLogEntrySets = await libs.logEntries.getLogEntryHighlights(req, source.id, args.startKey, args.endKey, args.highlights.filter(highlightInput => !!highlightInput.query), (0, _serialized_query.parseFilterQuery)(args.filterQuery));
      return highlightedLogEntrySets.map(entries => ({
        start: entries.length > 0 ? entries[0].key : null,
        end: entries.length > 0 ? entries[entries.length - 1].key : null,
        hasMoreBefore: true,
        hasMoreAfter: true,
        filterQuery: args.filterQuery,
        entries
      }));
    },

    async logSummaryBetween(source, args, {
      req
    }) {
      _usage_collector.UsageCollector.countLogs();

      const buckets = await libs.logEntries.getLogSummaryBucketsBetween(req, source.id, args.start, args.end, args.bucketSize, (0, _serialized_query.parseFilterQuery)(args.filterQuery));
      return {
        start: buckets.length > 0 ? buckets[0].start : null,
        end: buckets.length > 0 ? buckets[buckets.length - 1].end : null,
        buckets
      };
    },

    async logSummaryHighlightsBetween(source, args, {
      req
    }) {
      const summaryHighlightSets = await libs.logEntries.getLogSummaryHighlightBucketsBetween(req, source.id, args.start, args.end, args.bucketSize, args.highlightQueries.filter(highlightQuery => !!highlightQuery), (0, _serialized_query.parseFilterQuery)(args.filterQuery));
      return summaryHighlightSets.map(buckets => ({
        start: buckets.length > 0 ? buckets[0].start : null,
        end: buckets.length > 0 ? buckets[buckets.length - 1].end : null,
        buckets
      }));
    },

    async logItem(source, args, {
      req
    }) {
      const sourceConfiguration = _sources.SourceConfigurationRuntimeType.decode(source.configuration).getOrElseL(errors => {
        throw new Error((0, _PathReporter.failure)(errors).join('\n'));
      });

      return await libs.logEntries.getLogItem(req, args.id, sourceConfiguration);
    }

  },
  InfraLogEntryColumn: {
    __resolveType(logEntryColumn) {
      if (isTimestampColumn(logEntryColumn)) {
        return 'InfraLogEntryTimestampColumn';
      }

      if (isMessageColumn(logEntryColumn)) {
        return 'InfraLogEntryMessageColumn';
      }

      if (isFieldColumn(logEntryColumn)) {
        return 'InfraLogEntryFieldColumn';
      }

      return null;
    }

  },
  InfraLogMessageSegment: {
    __resolveType(messageSegment) {
      if (isConstantSegment(messageSegment)) {
        return 'InfraLogMessageConstantSegment';
      }

      if (isFieldSegment(messageSegment)) {
        return 'InfraLogMessageFieldSegment';
      }

      return null;
    }

  }
});

exports.createLogEntriesResolvers = createLogEntriesResolvers;

const isTimestampColumn = column => 'timestamp' in column;

const isMessageColumn = column => 'message' in column;

const isFieldColumn = column => 'field' in column && 'value' in column;

const isConstantSegment = segment => 'constant' in segment;

const isFieldSegment = segment => 'field' in segment && 'value' in segment && 'highlights' in segment;