"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createSnapshotResolvers", {
  enumerable: true,
  get: function () {
    return _resolvers.createSnapshotResolvers;
  }
});
Object.defineProperty(exports, "snapshotSchema", {
  enumerable: true,
  get: function () {
    return _schema.snapshotSchema;
  }
});

var _resolvers = require("./resolvers");

var _schema = require("./schema.gql");