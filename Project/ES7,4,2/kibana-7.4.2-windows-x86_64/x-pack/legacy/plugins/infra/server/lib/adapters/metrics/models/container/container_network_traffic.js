"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.containerNetworkTraffic = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const containerNetworkTraffic = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.containerNetworkTraffic,
  requires: ['docker.network'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'tx',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.network.out.bytes',
      id: 'avg-network-out',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }, {
    id: 'rx',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.network.in.bytes',
      id: 'avg-network-in',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }, {
      id: 'invert-posonly-deriv-max-network-in',
      script: 'params.rate * -1',
      type: _adapter_types.InfraMetricModelMetricType.calculation,
      variables: [{
        field: 'avg-network-in',
        id: 'var-rate',
        name: 'rate'
      }]
    }]
  }]
});

exports.containerNetworkTraffic = containerNetworkTraffic;