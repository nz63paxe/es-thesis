"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tx = void 0;

var _types = require("../../../graphql/types");

var _network_traffic = require("./network_traffic");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const METRIC_FIELDS = {
  [_types.InfraNodeType.host]: 'system.network.out.bytes',
  [_types.InfraNodeType.pod]: 'kubernetes.pod.network.tx.bytes',
  [_types.InfraNodeType.container]: 'docker.network.out.bytes'
};
const INTERFACE_FIELDS = {
  [_types.InfraNodeType.host]: 'system.network.name',
  [_types.InfraNodeType.pod]: null,
  [_types.InfraNodeType.container]: 'docker.network.interface'
};
const tx = (0, _network_traffic.networkTraffic)('tx', METRIC_FIELDS, INTERFACE_FIELDS);
exports.tx = tx;