"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.awsDiskioOps = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// see discussion in: https://github.com/elastic/kibana/issues/42687
const awsDiskioOps = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.awsDiskioOps,
  requires: ['aws.ec2'],
  index_pattern: indexPattern,
  map_field_to: 'cloud.instance.id',
  id_type: 'cloud',
  interval: '>=5m',
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'writes',
    metrics: [{
      field: 'aws.ec2.diskio.write.count',
      id: 'sum-diskio-writes',
      type: _adapter_types.InfraMetricModelMetricType.sum
    }, {
      id: 'csum-sum-diskio-writes',
      field: 'sum-diskio-writes',
      type: _adapter_types.InfraMetricModelMetricType.cumulative_sum
    }, {
      id: 'deriv-csum-sum-diskio-writes',
      unit: '1s',
      type: _adapter_types.InfraMetricModelMetricType.derivative,
      field: 'csum-sum-diskio-writes'
    }, {
      id: 'posonly-deriv-csum-sum-diskio-writes',
      field: 'deriv-csum-sum-diskio-writes',
      type: _adapter_types.InfraMetricModelMetricType.positive_only
    }],
    split_mode: 'everything'
  }, {
    id: 'reads',
    metrics: [{
      field: 'aws.ec2.diskio.read.count',
      id: 'sum-diskio-reads',
      type: _adapter_types.InfraMetricModelMetricType.sum
    }, {
      id: 'csum-sum-diskio-reads',
      field: 'sum-diskio-reads',
      type: _adapter_types.InfraMetricModelMetricType.cumulative_sum
    }, {
      id: 'deriv-csum-sum-diskio-reads',
      unit: '1s',
      type: _adapter_types.InfraMetricModelMetricType.derivative,
      field: 'csum-sum-diskio-reads'
    }, {
      id: 'posonly-deriv-csum-sum-diskio-reads',
      field: 'deriv-csum-sum-diskio-reads',
      type: _adapter_types.InfraMetricModelMetricType.positive_only
    }, {
      id: 'inverted-posonly-deriv-csum-sum-diskio-reads',
      type: _adapter_types.InfraMetricModelMetricType.calculation,
      variables: [{
        id: 'var-rate',
        name: 'rate',
        field: 'posonly-deriv-csum-sum-diskio-reads'
      }],
      script: 'params.rate * -1'
    }],
    split_mode: 'everything'
  }]
});

exports.awsDiskioOps = awsDiskioOps;