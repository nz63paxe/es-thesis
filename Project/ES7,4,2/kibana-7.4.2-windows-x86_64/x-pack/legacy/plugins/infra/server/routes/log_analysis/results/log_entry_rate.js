"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initLogAnalysisGetLogEntryRateRoute = void 0;

var _boom = _interopRequireDefault(require("boom"));

var _log_analysis = require("../../../../common/http_api/log_analysis");

var _runtime_types = require("../../../../common/runtime_types");

var _log_analysis2 = require("../../../lib/log_analysis");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const initLogAnalysisGetLogEntryRateRoute = ({
  framework,
  logAnalysis
}) => {
  framework.registerRoute({
    method: 'POST',
    path: _log_analysis.LOG_ANALYSIS_GET_LOG_ENTRY_RATE_PATH,
    handler: async (req, res) => {
      const payload = _log_analysis.getLogEntryRateRequestPayloadRT.decode(req.payload).getOrElseL((0, _runtime_types.throwErrors)(_boom.default.badRequest));

      const logEntryRateBuckets = await logAnalysis.getLogEntryRateBuckets(req, payload.data.sourceId, payload.data.timeRange.startTime, payload.data.timeRange.endTime, payload.data.bucketDuration).catch(err => {
        if (err instanceof _log_analysis2.NoLogRateResultsIndexError) {
          throw _boom.default.boomify(err, {
            statusCode: 404
          });
        }

        throw _boom.default.boomify(err, {
          statusCode: 'statusCode' in err && err.statusCode || 500
        });
      });
      return res.response(_log_analysis.getLogEntryRateSuccessReponsePayloadRT.encode({
        data: {
          bucketDuration: payload.data.bucketDuration,
          histogramBuckets: logEntryRateBuckets
        }
      }));
    }
  });
};

exports.initLogAnalysisGetLogEntryRateRoute = initLogAnalysisGetLogEntryRateRoute;