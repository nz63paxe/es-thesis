"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FrameworkFieldsAdapter = void 0;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class FrameworkFieldsAdapter {
  constructor(framework) {
    _defineProperty(this, "framework", void 0);

    this.framework = framework;
  }

  async getIndexFields(request, indices) {
    const indexPatternsService = this.framework.getIndexPatternsService(request);
    const response = await indexPatternsService.getFieldsForWildcard({
      pattern: indices
    });
    return response;
  }

}

exports.FrameworkFieldsAdapter = FrameworkFieldsAdapter;