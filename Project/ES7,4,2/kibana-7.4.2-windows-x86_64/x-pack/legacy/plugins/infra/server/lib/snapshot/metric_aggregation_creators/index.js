"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.metricAggregationCreators = void 0;

var _types = require("../../../graphql/types");

var _count = require("./count");

var _cpu = require("./cpu");

var _load = require("./load");

var _log_rate = require("./log_rate");

var _memory = require("./memory");

var _rx = require("./rx");

var _tx = require("./tx");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const metricAggregationCreators = {
  [_types.InfraSnapshotMetricType.count]: _count.count,
  [_types.InfraSnapshotMetricType.cpu]: _cpu.cpu,
  [_types.InfraSnapshotMetricType.memory]: _memory.memory,
  [_types.InfraSnapshotMetricType.rx]: _rx.rx,
  [_types.InfraSnapshotMetricType.tx]: _tx.tx,
  [_types.InfraSnapshotMetricType.load]: _load.load,
  [_types.InfraSnapshotMetricType.logRate]: _log_rate.logRate
};
exports.metricAggregationCreators = metricAggregationCreators;