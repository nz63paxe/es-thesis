"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hostK8sMemoryCap = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const hostK8sMemoryCap = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.hostK8sMemoryCap,
  map_field_to: 'kubernetes.node.name',
  requires: ['kubernetes.node'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'capacity',
    metrics: [{
      field: 'kubernetes.node.memory.allocatable.bytes',
      id: 'max-memory-cap',
      type: _adapter_types.InfraMetricModelMetricType.max
    }],
    split_mode: 'everything'
  }, {
    id: 'used',
    metrics: [{
      field: 'kubernetes.node.memory.usage.bytes',
      id: 'avg-memory-usage',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }],
    split_mode: 'everything'
  }]
});

exports.hostK8sMemoryCap = hostK8sMemoryCap;