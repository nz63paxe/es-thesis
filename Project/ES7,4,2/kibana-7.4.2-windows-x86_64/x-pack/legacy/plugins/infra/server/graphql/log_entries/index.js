"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createLogEntriesResolvers", {
  enumerable: true,
  get: function () {
    return _resolvers.createLogEntriesResolvers;
  }
});

var _resolvers = require("./resolvers");