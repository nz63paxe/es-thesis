"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.schemas = void 0;

var _schema = require("../../common/graphql/root/schema.gql");

var _schema2 = require("../../common/graphql/shared/schema.gql");

var _schema3 = require("./log_entries/schema.gql");

var _schema4 = require("./metrics/schema.gql");

var _schema5 = require("./snapshot/schema.gql");

var _schema6 = require("./source_status/schema.gql");

var _schema7 = require("./sources/schema.gql");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const schemas = [_schema.rootSchema, _schema2.sharedSchema, _schema3.logEntriesSchema, _schema5.snapshotSchema, _schema7.sourcesSchema, _schema6.sourceStatusSchema, _schema4.metricsSchema];
exports.schemas = schemas;