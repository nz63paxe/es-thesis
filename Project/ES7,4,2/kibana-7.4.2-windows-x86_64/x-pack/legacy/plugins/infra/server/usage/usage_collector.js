"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UsageCollector = void 0;

var _types = require("../graphql/types");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const KIBANA_REPORTING_TYPE = 'infraops';

class UsageCollector {
  static getUsageCollector(server) {
    const {
      collectorSet
    } = server.usage;
    return collectorSet.makeUsageCollector({
      type: KIBANA_REPORTING_TYPE,
      isReady: () => true,
      fetch: async () => {
        return this.getReport();
      }
    });
  }

  static countNode(nodeType) {
    const bucket = this.getBucket();
    this.maybeInitializeBucket(bucket);

    switch (nodeType) {
      case _types.InfraNodeType.pod:
        this.counters[bucket].infraopsKubernetes += 1;
        break;

      case _types.InfraNodeType.container:
        this.counters[bucket].infraopsDocker += 1;
        break;

      default:
        this.counters[bucket].infraopsHosts += 1;
    }
  }

  static countLogs() {
    const bucket = this.getBucket();
    this.maybeInitializeBucket(bucket);
    this.counters[bucket].logs += 1;
  }

  // report the last 24 hours
  static getBucket() {
    const now = Math.floor(Date.now() / 1000);
    return now - now % this.BUCKET_SIZE;
  }

  static maybeInitializeBucket(bucket) {
    if (!this.counters[bucket]) {
      this.counters[bucket] = {
        infraopsHosts: 0,
        infraopsDocker: 0,
        infraopsKubernetes: 0,
        logs: 0
      };
    }
  }

  static getReport() {
    const keys = Object.keys(this.counters); // only keep the newest BUCKET_NUMBER buckets

    const cutoff = this.getBucket() - this.BUCKET_SIZE * (this.BUCKET_NUMBER - 1);
    keys.forEach(key => {
      if (parseInt(key, 10) < cutoff) {
        delete this.counters[key];
      }
    }); // all remaining buckets are current

    const sums = Object.keys(this.counters).reduce((a, b) => {
      const key = parseInt(b, 10);
      return {
        infraopsHosts: a.infraopsHosts + this.counters[key].infraopsHosts,
        infraopsDocker: a.infraopsDocker + this.counters[key].infraopsDocker,
        infraopsKubernetes: a.infraopsKubernetes + this.counters[key].infraopsKubernetes,
        logs: a.logs + this.counters[key].logs
      };
    }, {
      infraopsHosts: 0,
      infraopsDocker: 0,
      infraopsKubernetes: 0,
      logs: 0
    });
    return {
      last_24_hours: {
        hits: {
          infraops_hosts: sums.infraopsHosts,
          infraops_docker: sums.infraopsDocker,
          infraops_kubernetes: sums.infraopsKubernetes,
          logs: sums.logs
        }
      }
    };
  }

}

exports.UsageCollector = UsageCollector;

_defineProperty(UsageCollector, "counters", {});

_defineProperty(UsageCollector, "BUCKET_SIZE", 3600);

_defineProperty(UsageCollector, "BUCKET_NUMBER", 24);