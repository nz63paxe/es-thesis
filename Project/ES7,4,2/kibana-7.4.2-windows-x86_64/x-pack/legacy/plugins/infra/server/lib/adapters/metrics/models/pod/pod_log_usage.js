"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.podLogUsage = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const podLogUsage = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.podLogUsage,
  requires: ['kubernetes.pod'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'logs',
    split_mode: 'everything',
    metrics: [{
      field: 'kubernetes.container.logs.used.bytes',
      id: 'avg-log-used',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }, {
      field: 'kubernetes.container.logs.capacity.bytes',
      id: 'max-log-cap',
      type: _adapter_types.InfraMetricModelMetricType.max
    }, {
      id: 'calc-usage-limit',
      script: 'params.usage / params.limit',
      type: _adapter_types.InfraMetricModelMetricType.calculation,
      variables: [{
        field: 'avg-log-userd',
        id: 'var-usage',
        name: 'usage'
      }, {
        field: 'max-log-cap',
        id: 'var-limit',
        name: 'limit'
      }]
    }]
  }]
});

exports.podLogUsage = podLogUsage;