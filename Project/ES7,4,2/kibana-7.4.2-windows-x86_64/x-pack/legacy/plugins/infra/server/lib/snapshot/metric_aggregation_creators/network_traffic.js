"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.networkTraffic = void 0;

var _types = require("../../../graphql/types");

var _rate = require("./rate");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const networkTraffic = (id, metricFields, interfaceFields) => {
  const rateAggregations = (0, _rate.rate)(id, metricFields);
  return nodeType => {
    // Metricbeat doesn't collect interface data for Kubernetes Pods,
    // for these we'll use a standard rate calculation.
    if (nodeType === _types.InfraNodeType.pod) {
      return rateAggregations(nodeType);
    }

    const metricField = metricFields[nodeType];
    const interfaceField = interfaceFields[nodeType];

    if (metricField && interfaceField) {
      return {
        [`${id}_interfaces`]: {
          terms: {
            field: interfaceField
          },
          aggregations: {
            [`${id}_interface_max`]: {
              max: {
                field: metricField
              }
            }
          }
        },
        [`${id}_sum_of_interfaces`]: {
          sum_bucket: {
            buckets_path: `${id}_interfaces>${id}_interface_max`
          }
        },
        [`${id}_deriv`]: {
          derivative: {
            buckets_path: `${id}_sum_of_interfaces`,
            gap_policy: 'skip',
            unit: '1s'
          }
        },
        [id]: {
          bucket_script: {
            buckets_path: {
              value: `${id}_deriv[normalized_value]`
            },
            script: {
              source: 'params.value > 0.0 ? params.value : 0.0',
              lang: 'painless'
            },
            gap_policy: 'skip'
          }
        }
      };
    }
  };
};

exports.networkTraffic = networkTraffic;