"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDateHistogramOffset = exports.getMetricsAggregations = exports.getMetricsSources = exports.getGroupedNodesSources = void 0;

var _metric_aggregation_creators = require("./metric_aggregation_creators");

var _constants = require("../constants");

var _get_interval_in_seconds = require("../../utils/get_interval_in_seconds");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getGroupedNodesSources = options => {
  const sources = options.groupBy.map(gb => {
    return {
      [`${gb.field}`]: {
        terms: {
          field: gb.field
        }
      }
    };
  });
  sources.push({
    id: {
      terms: {
        field: options.sourceConfiguration.fields[options.nodeType]
      }
    }
  });
  sources.push({
    name: {
      terms: {
        field: _constants.NAME_FIELDS[options.nodeType],
        missing_bucket: true
      }
    }
  });
  return sources;
};

exports.getGroupedNodesSources = getGroupedNodesSources;

const getMetricsSources = options => {
  return [{
    id: {
      terms: {
        field: options.sourceConfiguration.fields[options.nodeType]
      }
    }
  }];
};

exports.getMetricsSources = getMetricsSources;

const getMetricsAggregations = options => {
  return _metric_aggregation_creators.metricAggregationCreators[options.metric.type](options.nodeType);
};

exports.getMetricsAggregations = getMetricsAggregations;

const getDateHistogramOffset = options => {
  const {
    from,
    interval
  } = options.timerange;
  const fromInSeconds = Math.floor(from / 1000);
  const bucketSizeInSeconds = (0, _get_interval_in_seconds.getIntervalInSeconds)(interval); // negative offset to align buckets with full intervals (e.g. minutes)

  const offset = fromInSeconds % bucketSizeInSeconds - bucketSizeInSeconds;
  return `${offset}s`;
};

exports.getDateHistogramOffset = getDateHistogramOffset;