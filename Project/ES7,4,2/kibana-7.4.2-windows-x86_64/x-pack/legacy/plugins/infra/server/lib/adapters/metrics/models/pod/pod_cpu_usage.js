"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.podCpuUsage = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const podCpuUsage = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.podCpuUsage,
  requires: ['kubernetes.pod'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'cpu',
    split_mode: 'everything',
    metrics: [{
      field: 'kubernetes.pod.cpu.usage.node.pct',
      id: 'avg-cpu-usage',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }]
});

exports.podCpuUsage = podCpuUsage;