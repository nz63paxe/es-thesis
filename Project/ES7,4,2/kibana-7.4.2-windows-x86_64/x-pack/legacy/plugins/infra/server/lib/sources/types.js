"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SourceConfigurationSavedObjectRuntimeType = exports.SourceConfigurationRuntimeType = exports.StaticSourceConfigurationRuntimeType = exports.pickSavedSourceConfiguration = exports.SavedSourceConfigurationRuntimeType = exports.SavedSourceConfigurationColumnRuntimeType = exports.SavedSourceConfigurationFieldColumnRuntimeType = exports.SavedSourceConfigurationMessageColumnRuntimeType = exports.SavedSourceConfigurationTimestampColumnRuntimeType = exports.TimestampFromString = void 0;

var runtimeTypes = _interopRequireWildcard(require("io-ts"));

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/* eslint-disable @typescript-eslint/no-empty-interface */
const TimestampFromString = new runtimeTypes.Type('TimestampFromString', input => typeof input === 'number', (input, context) => runtimeTypes.string.validate(input, context).chain(stringInput => {
  const momentValue = (0, _moment.default)(stringInput);
  return momentValue.isValid() ? runtimeTypes.success(momentValue.valueOf()) : runtimeTypes.failure(stringInput, context);
}), output => new Date(output).toISOString());
/**
 * Stored source configuration as read from and written to saved objects
 */

exports.TimestampFromString = TimestampFromString;
const SavedSourceConfigurationFieldsRuntimeType = runtimeTypes.partial({
  container: runtimeTypes.string,
  host: runtimeTypes.string,
  pod: runtimeTypes.string,
  tiebreaker: runtimeTypes.string,
  timestamp: runtimeTypes.string
});
const SavedSourceConfigurationTimestampColumnRuntimeType = runtimeTypes.type({
  timestampColumn: runtimeTypes.type({
    id: runtimeTypes.string
  })
});
exports.SavedSourceConfigurationTimestampColumnRuntimeType = SavedSourceConfigurationTimestampColumnRuntimeType;
const SavedSourceConfigurationMessageColumnRuntimeType = runtimeTypes.type({
  messageColumn: runtimeTypes.type({
    id: runtimeTypes.string
  })
});
exports.SavedSourceConfigurationMessageColumnRuntimeType = SavedSourceConfigurationMessageColumnRuntimeType;
const SavedSourceConfigurationFieldColumnRuntimeType = runtimeTypes.type({
  fieldColumn: runtimeTypes.type({
    id: runtimeTypes.string,
    field: runtimeTypes.string
  })
});
exports.SavedSourceConfigurationFieldColumnRuntimeType = SavedSourceConfigurationFieldColumnRuntimeType;
const SavedSourceConfigurationColumnRuntimeType = runtimeTypes.union([SavedSourceConfigurationTimestampColumnRuntimeType, SavedSourceConfigurationMessageColumnRuntimeType, SavedSourceConfigurationFieldColumnRuntimeType]);
exports.SavedSourceConfigurationColumnRuntimeType = SavedSourceConfigurationColumnRuntimeType;
const SavedSourceConfigurationRuntimeType = runtimeTypes.partial({
  name: runtimeTypes.string,
  description: runtimeTypes.string,
  metricAlias: runtimeTypes.string,
  logAlias: runtimeTypes.string,
  fields: SavedSourceConfigurationFieldsRuntimeType,
  logColumns: runtimeTypes.array(SavedSourceConfigurationColumnRuntimeType)
});
exports.SavedSourceConfigurationRuntimeType = SavedSourceConfigurationRuntimeType;

const pickSavedSourceConfiguration = value => {
  const {
    name,
    description,
    metricAlias,
    logAlias,
    fields,
    logColumns
  } = value;
  const {
    container,
    host,
    pod,
    tiebreaker,
    timestamp
  } = fields;
  return {
    name,
    description,
    metricAlias,
    logAlias,
    fields: {
      container,
      host,
      pod,
      tiebreaker,
      timestamp
    },
    logColumns
  };
};
/**
 * Static source configuration as read from the configuration file
 */


exports.pickSavedSourceConfiguration = pickSavedSourceConfiguration;
const StaticSourceConfigurationFieldsRuntimeType = runtimeTypes.partial({ ...SavedSourceConfigurationFieldsRuntimeType.props,
  message: runtimeTypes.array(runtimeTypes.string)
});
const StaticSourceConfigurationRuntimeType = runtimeTypes.partial({
  name: runtimeTypes.string,
  description: runtimeTypes.string,
  metricAlias: runtimeTypes.string,
  logAlias: runtimeTypes.string,
  fields: StaticSourceConfigurationFieldsRuntimeType,
  logColumns: runtimeTypes.array(SavedSourceConfigurationColumnRuntimeType)
});
exports.StaticSourceConfigurationRuntimeType = StaticSourceConfigurationRuntimeType;

/**
 * Full source configuration type after all cleanup has been done at the edges
 */
const SourceConfigurationFieldsRuntimeType = runtimeTypes.type({ ...StaticSourceConfigurationFieldsRuntimeType.props
});
const SourceConfigurationRuntimeType = runtimeTypes.type({ ...SavedSourceConfigurationRuntimeType.props,
  fields: SourceConfigurationFieldsRuntimeType,
  logColumns: runtimeTypes.array(SavedSourceConfigurationColumnRuntimeType)
});
exports.SourceConfigurationRuntimeType = SourceConfigurationRuntimeType;

/**
 * Saved object type with metadata
 */
const SourceConfigurationSavedObjectRuntimeType = runtimeTypes.intersection([runtimeTypes.type({
  id: runtimeTypes.string,
  attributes: SavedSourceConfigurationRuntimeType
}), runtimeTypes.partial({
  version: runtimeTypes.string,
  updated_at: TimestampFromString
})]);
exports.SourceConfigurationSavedObjectRuntimeType = SourceConfigurationSavedObjectRuntimeType;