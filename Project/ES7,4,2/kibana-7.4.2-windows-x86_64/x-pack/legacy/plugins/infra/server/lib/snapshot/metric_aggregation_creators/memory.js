"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.memory = void 0;

var _types = require("../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const FIELDS = {
  [_types.InfraNodeType.host]: 'system.memory.actual.used.pct',
  [_types.InfraNodeType.pod]: 'kubernetes.pod.memory.usage.node.pct',
  [_types.InfraNodeType.container]: 'docker.memory.usage.pct'
};

const memory = nodeType => {
  return {
    memory: {
      avg: {
        field: FIELDS[nodeType]
      }
    }
  };
};

exports.memory = memory;