"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.load = void 0;

var _types = require("../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const FIELDS = {
  [_types.InfraNodeType.host]: 'system.load.5',
  [_types.InfraNodeType.pod]: '',
  [_types.InfraNodeType.container]: ''
};

const load = nodeType => {
  const field = FIELDS[nodeType];

  if (field) {
    return {
      load: {
        avg: {
          field
        }
      }
    };
  }
};

exports.load = load;