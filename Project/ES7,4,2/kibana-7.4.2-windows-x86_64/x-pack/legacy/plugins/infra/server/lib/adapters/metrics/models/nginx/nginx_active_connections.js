"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nginxActiveConnections = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const nginxActiveConnections = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.nginxActiveConnections,
  requires: ['nginx.stubstatus'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'connections',
    metrics: [{
      field: 'nginx.stubstatus.active',
      id: 'avg-active',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }],
    split_mode: 'everything'
  }]
});

exports.nginxActiveConnections = nginxActiveConnections;