"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPodNodeName = void 0;

var _lodash = require("lodash");

var _get_id_field_name = require("./get_id_field_name");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getPodNodeName = async (framework, req, sourceConfiguration, nodeId, nodeType) => {
  const params = {
    allowNoIndices: true,
    ignoreUnavailable: true,
    terminateAfter: 1,
    index: sourceConfiguration.metricAlias,
    body: {
      size: 1,
      _source: ['kubernetes.node.name'],
      query: {
        bool: {
          filter: [{
            match: {
              [(0, _get_id_field_name.getIdFieldName)(sourceConfiguration, nodeType)]: nodeId
            }
          }, {
            exists: {
              field: `kubernetes.node.name`
            }
          }]
        }
      }
    }
  };
  const response = await framework.callWithRequest(req, 'search', params);
  const firstHit = (0, _lodash.first)(response.hits.hits);

  if (firstHit) {
    return (0, _lodash.get)(firstHit, '_source.kubernetes.node.name');
  }
};

exports.getPodNodeName = getPodNodeName;