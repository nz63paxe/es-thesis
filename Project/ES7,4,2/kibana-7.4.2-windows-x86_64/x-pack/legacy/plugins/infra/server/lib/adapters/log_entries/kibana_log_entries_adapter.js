"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraKibanaLogEntriesAdapter = void 0;

var _d3Time = require("d3-time");

var runtimeTypes = _interopRequireWildcard(require("io-ts"));

var _first = _interopRequireDefault(require("lodash/fp/first"));

var _get = _interopRequireDefault(require("lodash/fp/get"));

var _has = _interopRequireDefault(require("lodash/fp/has"));

var _zip = _interopRequireDefault(require("lodash/fp/zip"));

var _time = require("../../../../common/time");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/* eslint-disable @typescript-eslint/no-empty-interface */
const DAY_MILLIS = 24 * 60 * 60 * 1000;
const LOOKUP_OFFSETS = [0, 1, 7, 30, 365, 10000, Infinity].map(days => days * DAY_MILLIS);
const TIMESTAMP_FORMAT = 'epoch_millis';

class InfraKibanaLogEntriesAdapter {
  constructor(framework) {
    this.framework = framework;
  }

  async getAdjacentLogEntryDocuments(request, sourceConfiguration, fields, start, direction, maxCount, filterQuery, highlightQuery) {
    if (maxCount <= 0) {
      return [];
    }

    const intervals = getLookupIntervals(start.time, direction);
    let documents = [];

    for (const [intervalStart, intervalEnd] of intervals) {
      if (documents.length >= maxCount) {
        break;
      }

      const documentsInInterval = await this.getLogEntryDocumentsBetween(request, sourceConfiguration, fields, intervalStart, intervalEnd, documents.length > 0 ? documents[documents.length - 1].key : start, maxCount - documents.length, filterQuery, highlightQuery);
      documents = [...documents, ...documentsInInterval];
    }

    return direction === 'asc' ? documents : documents.reverse();
  }

  async getContainedLogEntryDocuments(request, sourceConfiguration, fields, start, end, filterQuery, highlightQuery) {
    const documents = await this.getLogEntryDocumentsBetween(request, sourceConfiguration, fields, start.time, end.time, start, 10000, filterQuery, highlightQuery);
    return documents.filter(document => (0, _time.compareTimeKeys)(document.key, end) < 0);
  }

  async getContainedLogSummaryBuckets(request, sourceConfiguration, start, end, bucketSize, filterQuery) {
    const bucketIntervalStarts = (0, _d3Time.timeMilliseconds)(new Date(start), new Date(end), bucketSize);
    const query = {
      allowNoIndices: true,
      index: sourceConfiguration.logAlias,
      ignoreUnavailable: true,
      body: {
        aggregations: {
          count_by_date: {
            date_range: {
              field: sourceConfiguration.fields.timestamp,
              format: TIMESTAMP_FORMAT,
              ranges: bucketIntervalStarts.map(bucketIntervalStart => ({
                from: bucketIntervalStart.getTime(),
                to: bucketIntervalStart.getTime() + bucketSize
              }))
            },
            aggregations: {
              top_hits_by_key: {
                top_hits: {
                  size: 1,
                  sort: [{
                    [sourceConfiguration.fields.timestamp]: 'asc'
                  }, {
                    [sourceConfiguration.fields.tiebreaker]: 'asc'
                  }],
                  _source: false
                }
              }
            }
          }
        },
        query: {
          bool: {
            filter: [...createQueryFilterClauses(filterQuery), {
              range: {
                [sourceConfiguration.fields.timestamp]: {
                  gte: start,
                  lte: end,
                  format: TIMESTAMP_FORMAT
                }
              }
            }]
          }
        },
        size: 0,
        track_total_hits: false
      }
    };
    const response = await this.framework.callWithRequest(request, 'search', query);
    return LogSummaryResponseRuntimeType.decode(response).map(logSummaryResponse => logSummaryResponse.aggregations.count_by_date.buckets.map(convertDateRangeBucketToSummaryBucket)).getOrElse([]);
  }

  async getLogItem(request, id, sourceConfiguration) {
    const search = searchOptions => this.framework.callWithRequest(request, 'search', searchOptions);

    const params = {
      index: sourceConfiguration.logAlias,
      terminate_after: 1,
      body: {
        size: 1,
        sort: [{
          [sourceConfiguration.fields.timestamp]: 'desc'
        }, {
          [sourceConfiguration.fields.tiebreaker]: 'desc'
        }],
        query: {
          ids: {
            values: [id]
          }
        }
      }
    };
    const response = await search(params);
    const document = (0, _first.default)(response.hits.hits);

    if (!document) {
      throw new Error('Document not found');
    }

    return document;
  }

  async getLogEntryDocumentsBetween(request, sourceConfiguration, fields, start, end, after, maxCount, filterQuery, highlightQuery) {
    if (maxCount <= 0) {
      return [];
    }

    const sortDirection = start <= end ? 'asc' : 'desc';
    const startRange = {
      [sortDirection === 'asc' ? 'gte' : 'lte']: start
    };
    const endRange = end === Infinity ? {} : {
      [sortDirection === 'asc' ? 'lte' : 'gte']: end
    };
    const highlightClause = highlightQuery ? {
      highlight: {
        boundary_scanner: 'word',
        fields: fields.reduce((highlightFieldConfigs, fieldName) => ({ ...highlightFieldConfigs,
          [fieldName]: {}
        }), {}),
        fragment_size: 1,
        number_of_fragments: 100,
        post_tags: [''],
        pre_tags: [''],
        highlight_query: highlightQuery
      }
    } : {};
    const searchAfterClause = (0, _time.isTimeKey)(after) ? {
      search_after: [after.time, after.tiebreaker]
    } : {};
    const query = {
      allowNoIndices: true,
      index: sourceConfiguration.logAlias,
      ignoreUnavailable: true,
      body: {
        query: {
          bool: {
            filter: [...createQueryFilterClauses(filterQuery), {
              range: {
                [sourceConfiguration.fields.timestamp]: { ...startRange,
                  ...endRange,
                  format: TIMESTAMP_FORMAT
                }
              }
            }]
          }
        },
        ...highlightClause,
        ...searchAfterClause,
        _source: fields,
        size: maxCount,
        sort: [{
          [sourceConfiguration.fields.timestamp]: sortDirection
        }, {
          [sourceConfiguration.fields.tiebreaker]: sortDirection
        }],
        track_total_hits: false
      }
    };
    const response = await this.framework.callWithRequest(request, 'search', query);
    const hits = response.hits.hits;
    const documents = hits.map(convertHitToLogEntryDocument(fields));
    return documents;
  }

}

exports.InfraKibanaLogEntriesAdapter = InfraKibanaLogEntriesAdapter;

function getLookupIntervals(start, direction) {
  const offsetSign = direction === 'asc' ? 1 : -1;
  const translatedOffsets = LOOKUP_OFFSETS.map(offset => start + offset * offsetSign);
  const intervals = (0, _zip.default)(translatedOffsets.slice(0, -1), translatedOffsets.slice(1));
  return intervals;
}

const convertHitToLogEntryDocument = fields => hit => ({
  gid: hit._id,
  fields: fields.reduce((flattenedFields, fieldName) => (0, _has.default)(fieldName, hit._source) ? { ...flattenedFields,
    [fieldName]: (0, _get.default)(fieldName, hit._source)
  } : flattenedFields, {}),
  highlights: hit.highlight || {},
  key: {
    time: hit.sort[0],
    tiebreaker: hit.sort[1]
  }
});

const convertDateRangeBucketToSummaryBucket = bucket => ({
  entriesCount: bucket.doc_count,
  start: bucket.from || 0,
  end: bucket.to || 0,
  topEntryKeys: bucket.top_hits_by_key.hits.hits.map(hit => ({
    tiebreaker: hit.sort[1],
    time: hit.sort[0]
  }))
});

const createQueryFilterClauses = filterQuery => filterQuery ? [filterQuery] : [];

const LogSummaryDateRangeBucketRuntimeType = runtimeTypes.intersection([runtimeTypes.type({
  doc_count: runtimeTypes.number,
  key: runtimeTypes.string,
  top_hits_by_key: runtimeTypes.type({
    hits: runtimeTypes.type({
      hits: runtimeTypes.array(runtimeTypes.type({
        sort: runtimeTypes.tuple([runtimeTypes.number, runtimeTypes.number])
      }))
    })
  })
}), runtimeTypes.partial({
  from: runtimeTypes.number,
  to: runtimeTypes.number
})]);
const LogSummaryResponseRuntimeType = runtimeTypes.type({
  aggregations: runtimeTypes.type({
    count_by_date: runtimeTypes.type({
      buckets: runtimeTypes.array(LogSummaryDateRangeBucketRuntimeType)
    })
  })
});