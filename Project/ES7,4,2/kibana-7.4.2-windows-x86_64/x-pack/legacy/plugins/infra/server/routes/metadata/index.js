"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initMetadataRoute = void 0;

var _boom = _interopRequireWildcard(require("boom"));

var _lodash = require("lodash");

var _metadata_api = require("../../../common/http_api/metadata_api");

var _get_metric_metadata = require("./lib/get_metric_metadata");

var _pick_feature_name = require("./lib/pick_feature_name");

var _has_apm_data = require("./lib/has_apm_data");

var _get_cloud_metric_metadata = require("./lib/get_cloud_metric_metadata");

var _get_node_info = require("./lib/get_node_info");

var _runtime_types = require("../../../common/runtime_types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const initMetadataRoute = libs => {
  const {
    framework
  } = libs;
  framework.registerRoute({
    method: 'POST',
    path: '/api/infra/metadata',
    handler: async req => {
      try {
        const {
          nodeId,
          nodeType,
          sourceId
        } = _metadata_api.InfraMetadataRequestRT.decode(req.payload).getOrElseL((0, _runtime_types.throwErrors)(_boom.default.badRequest));

        const {
          configuration
        } = await libs.sources.getSourceConfiguration(req, sourceId);
        const metricsMetadata = await (0, _get_metric_metadata.getMetricMetadata)(framework, req, configuration, nodeId, nodeType);
        const metricFeatures = (0, _pick_feature_name.pickFeatureName)(metricsMetadata.buckets).map(nameToFeature('metrics'));
        const info = await (0, _get_node_info.getNodeInfo)(framework, req, configuration, nodeId, nodeType);
        const cloudInstanceId = (0, _lodash.get)(info, 'cloud.instance.id');
        const cloudMetricsMetadata = cloudInstanceId ? await (0, _get_cloud_metric_metadata.getCloudMetricsMetadata)(framework, req, configuration, cloudInstanceId) : {
          buckets: []
        };
        const cloudMetricsFeatures = (0, _pick_feature_name.pickFeatureName)(cloudMetricsMetadata.buckets).map(nameToFeature('metrics'));
        const hasAPM = await (0, _has_apm_data.hasAPMData)(framework, req, configuration, nodeId, nodeType);
        const apmMetricFeatures = hasAPM ? [{
          name: 'apm.transaction',
          source: 'apm'
        }] : [];
        const id = metricsMetadata.id;
        const name = metricsMetadata.name || id;
        return _metadata_api.InfraMetadataRT.decode({
          id,
          name,
          features: [...metricFeatures, ...cloudMetricsFeatures, ...apmMetricFeatures],
          info
        }).getOrElseL((0, _runtime_types.throwErrors)(_boom.default.badImplementation));
      } catch (error) {
        throw (0, _boom.boomify)(error);
      }
    }
  });
};

exports.initMetadataRoute = initMetadataRoute;

const nameToFeature = source => name => ({
  name,
  source
});