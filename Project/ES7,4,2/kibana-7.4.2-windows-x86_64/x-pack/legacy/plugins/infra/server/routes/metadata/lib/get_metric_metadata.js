"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getMetricMetadata = void 0;

var _lodash = require("lodash");

var _get_id_field_name = require("./get_id_field_name");

var _constants = require("../../../lib/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getMetricMetadata = async (framework, req, sourceConfiguration, nodeId, nodeType) => {
  const idFieldName = (0, _get_id_field_name.getIdFieldName)(sourceConfiguration, nodeType);
  const metricQuery = {
    allowNoIndices: true,
    ignoreUnavailable: true,
    index: sourceConfiguration.metricAlias,
    body: {
      query: {
        bool: {
          must_not: [{
            match: {
              'event.dataset': 'aws.ec2'
            }
          }],
          filter: [{
            match: {
              [idFieldName]: nodeId
            }
          }]
        }
      },
      size: 0,
      aggs: {
        nodeName: {
          terms: {
            field: _constants.NAME_FIELDS[nodeType],
            size: 1
          }
        },
        metrics: {
          terms: {
            field: 'event.dataset',
            size: 1000
          }
        }
      }
    }
  };
  const response = await framework.callWithRequest(req, 'search', metricQuery);
  const buckets = response.aggregations && response.aggregations.metrics ? response.aggregations.metrics.buckets : [];
  return {
    id: nodeId,
    name: (0, _lodash.get)(response, ['aggregations', 'nodeName', 'buckets', 0, 'key'], nodeId),
    buckets
  };
};

exports.getMetricMetadata = getMetricMetadata;