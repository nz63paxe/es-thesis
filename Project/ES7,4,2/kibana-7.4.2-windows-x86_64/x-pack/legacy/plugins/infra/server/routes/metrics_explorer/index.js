"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initMetricExplorerRoute = void 0;

var _boom = require("boom");

var _get_groupings = require("./lib/get_groupings");

var _populate_series_with_tsvb_data = require("./lib/populate_series_with_tsvb_data");

var _schema = require("./schema");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const initMetricExplorerRoute = libs => {
  const {
    framework
  } = libs;
  const {
    callWithRequest
  } = framework;
  framework.registerRoute({
    method: 'POST',
    path: '/api/infra/metrics_explorer',
    options: {
      validate: {
        payload: _schema.metricsExplorerSchema
      }
    },
    handler: async req => {
      try {
        const search = searchOptions => callWithRequest(req, 'search', searchOptions);

        const options = req.payload; // First we get the groupings from a composite aggregation

        const response = await (0, _get_groupings.getGroupings)(search, options); // Then we take the results and fill in the data from TSVB with the
        // user's custom metrics

        const seriesWithMetrics = await Promise.all(response.series.map((0, _populate_series_with_tsvb_data.populateSeriesWithTSVBData)(req, options, framework)));
        return { ...response,
          series: seriesWithMetrics
        };
      } catch (error) {
        throw (0, _boom.boomify)(error);
      }
    }
  });
};

exports.initMetricExplorerRoute = initMetricExplorerRoute;