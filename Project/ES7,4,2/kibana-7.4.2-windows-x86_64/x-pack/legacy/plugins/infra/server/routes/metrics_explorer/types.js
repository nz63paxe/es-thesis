"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsExplorerColumnType = exports.MetricsExplorerAggregation = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let MetricsExplorerAggregation;
exports.MetricsExplorerAggregation = MetricsExplorerAggregation;

(function (MetricsExplorerAggregation) {
  MetricsExplorerAggregation["avg"] = "avg";
  MetricsExplorerAggregation["max"] = "max";
  MetricsExplorerAggregation["min"] = "min";
  MetricsExplorerAggregation["cardinality"] = "cardinality";
  MetricsExplorerAggregation["rate"] = "rate";
  MetricsExplorerAggregation["count"] = "count";
})(MetricsExplorerAggregation || (exports.MetricsExplorerAggregation = MetricsExplorerAggregation = {}));

let MetricsExplorerColumnType;
exports.MetricsExplorerColumnType = MetricsExplorerColumnType;

(function (MetricsExplorerColumnType) {
  MetricsExplorerColumnType["date"] = "date";
  MetricsExplorerColumnType["number"] = "number";
  MetricsExplorerColumnType["string"] = "string";
})(MetricsExplorerColumnType || (exports.MetricsExplorerColumnType = MetricsExplorerColumnType = {}));