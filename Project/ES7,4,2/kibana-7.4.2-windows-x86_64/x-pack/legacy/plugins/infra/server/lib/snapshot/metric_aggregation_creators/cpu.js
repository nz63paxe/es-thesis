"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.cpu = void 0;

var _types = require("../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const FIELDS = {
  [_types.InfraNodeType.host]: 'system.cpu.user.pct',
  [_types.InfraNodeType.pod]: 'kubernetes.pod.cpu.usage.node.pct',
  [_types.InfraNodeType.container]: 'docker.cpu.total.pct'
};

const cpu = nodeType => {
  if (nodeType === _types.InfraNodeType.host) {
    return {
      cpu_user: {
        avg: {
          field: 'system.cpu.user.pct'
        }
      },
      cpu_system: {
        avg: {
          field: 'system.cpu.system.pct'
        }
      },
      cpu_cores: {
        max: {
          field: 'system.cpu.cores'
        }
      },
      cpu: {
        bucket_script: {
          buckets_path: {
            user: 'cpu_user',
            system: 'cpu_system',
            cores: 'cpu_cores'
          },
          script: {
            source: '(params.user + params.system) / params.cores',
            lang: 'painless'
          },
          gap_policy: 'skip'
        }
      }
    };
  } else {
    return {
      cpu: {
        avg: {
          field: FIELDS[nodeType]
        }
      }
    };
  }
};

exports.cpu = cpu;