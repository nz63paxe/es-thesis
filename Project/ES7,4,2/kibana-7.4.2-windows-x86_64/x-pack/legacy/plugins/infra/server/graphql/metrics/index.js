"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "createMetricResolvers", {
  enumerable: true,
  get: function () {
    return _resolvers.createMetricResolvers;
  }
});
Object.defineProperty(exports, "metricsSchema", {
  enumerable: true,
  get: function () {
    return _schema.metricsSchema;
  }
});

var _resolvers = require("./resolvers");

var _schema = require("./schema.gql");