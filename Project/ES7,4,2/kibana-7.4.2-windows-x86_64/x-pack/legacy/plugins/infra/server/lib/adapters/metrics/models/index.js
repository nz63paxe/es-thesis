"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.metricModels = void 0;

var _types = require("../../../../graphql/types");

var _host_cpu_usage = require("./host/host_cpu_usage");

var _host_filesystem = require("./host/host_filesystem");

var _host_k8s_cpu_cap = require("./host/host_k8s_cpu_cap");

var _host_k8s_disk_cap = require("./host/host_k8s_disk_cap");

var _host_k8s_memory_cap = require("./host/host_k8s_memory_cap");

var _host_k8s_overview = require("./host/host_k8s_overview");

var _host_k8s_pod_cap = require("./host/host_k8s_pod_cap");

var _host_load = require("./host/host_load");

var _host_memory_usage = require("./host/host_memory_usage");

var _host_network_traffic = require("./host/host_network_traffic");

var _host_system_overview = require("./host/host_system_overview");

var _pod_cpu_usage = require("./pod/pod_cpu_usage");

var _pod_log_usage = require("./pod/pod_log_usage");

var _pod_memory_usage = require("./pod/pod_memory_usage");

var _pod_network_traffic = require("./pod/pod_network_traffic");

var _pod_overview = require("./pod/pod_overview");

var _container_cpu_kernel = require("./container/container_cpu_kernel");

var _container_cpu_usage = require("./container/container_cpu_usage");

var _container_disk_io_bytes = require("./container/container_disk_io_bytes");

var _container_diskio_ops = require("./container/container_diskio_ops");

var _container_memory = require("./container/container_memory");

var _container_network_traffic = require("./container/container_network_traffic");

var _container_overview = require("./container/container_overview");

var _nginx_active_connections = require("./nginx/nginx_active_connections");

var _nginx_hits = require("./nginx/nginx_hits");

var _nginx_request_rate = require("./nginx/nginx_request_rate");

var _nginx_requests_per_connection = require("./nginx/nginx_requests_per_connection");

var _aws_overview = require("./aws/aws_overview");

var _aws_cpu_utilization = require("./aws/aws_cpu_utilization");

var _aws_network_bytes = require("./aws/aws_network_bytes");

var _aws_network_packets = require("./aws/aws_network_packets");

var _aws_diskio_bytes = require("./aws/aws_diskio_bytes");

var _aws_diskio_ops = require("./aws/aws_diskio_ops");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const metricModels = {
  [_types.InfraMetric.hostSystemOverview]: _host_system_overview.hostSystemOverview,
  [_types.InfraMetric.hostCpuUsage]: _host_cpu_usage.hostCpuUsage,
  [_types.InfraMetric.hostFilesystem]: _host_filesystem.hostFilesystem,
  [_types.InfraMetric.hostK8sOverview]: _host_k8s_overview.hostK8sOverview,
  [_types.InfraMetric.hostK8sCpuCap]: _host_k8s_cpu_cap.hostK8sCpuCap,
  [_types.InfraMetric.hostK8sDiskCap]: _host_k8s_disk_cap.hostK8sDiskCap,
  [_types.InfraMetric.hostK8sMemoryCap]: _host_k8s_memory_cap.hostK8sMemoryCap,
  [_types.InfraMetric.hostK8sPodCap]: _host_k8s_pod_cap.hostK8sPodCap,
  [_types.InfraMetric.hostLoad]: _host_load.hostLoad,
  [_types.InfraMetric.hostMemoryUsage]: _host_memory_usage.hostMemoryUsage,
  [_types.InfraMetric.hostNetworkTraffic]: _host_network_traffic.hostNetworkTraffic,
  [_types.InfraMetric.podOverview]: _pod_overview.podOverview,
  [_types.InfraMetric.podCpuUsage]: _pod_cpu_usage.podCpuUsage,
  [_types.InfraMetric.podMemoryUsage]: _pod_memory_usage.podMemoryUsage,
  [_types.InfraMetric.podLogUsage]: _pod_log_usage.podLogUsage,
  [_types.InfraMetric.podNetworkTraffic]: _pod_network_traffic.podNetworkTraffic,
  [_types.InfraMetric.containerCpuKernel]: _container_cpu_kernel.containerCpuKernel,
  [_types.InfraMetric.containerCpuUsage]: _container_cpu_usage.containerCpuUsage,
  [_types.InfraMetric.containerDiskIOBytes]: _container_disk_io_bytes.containerDiskIOBytes,
  [_types.InfraMetric.containerDiskIOOps]: _container_diskio_ops.containerDiskIOOps,
  [_types.InfraMetric.containerNetworkTraffic]: _container_network_traffic.containerNetworkTraffic,
  [_types.InfraMetric.containerMemory]: _container_memory.containerMemory,
  [_types.InfraMetric.containerOverview]: _container_overview.containerOverview,
  [_types.InfraMetric.nginxHits]: _nginx_hits.nginxHits,
  [_types.InfraMetric.nginxRequestRate]: _nginx_request_rate.nginxRequestRate,
  [_types.InfraMetric.nginxActiveConnections]: _nginx_active_connections.nginxActiveConnections,
  [_types.InfraMetric.nginxRequestsPerConnection]: _nginx_requests_per_connection.nginxRequestsPerConnection,
  [_types.InfraMetric.awsOverview]: _aws_overview.awsOverview,
  [_types.InfraMetric.awsCpuUtilization]: _aws_cpu_utilization.awsCpuUtilization,
  [_types.InfraMetric.awsNetworkBytes]: _aws_network_bytes.awsNetworkBytes,
  [_types.InfraMetric.awsNetworkPackets]: _aws_network_packets.awsNetworkPackets,
  [_types.InfraMetric.awsDiskioBytes]: _aws_diskio_bytes.awsDiskioBytes,
  [_types.InfraMetric.awsDiskioOps]: _aws_diskio_ops.awsDiskioOps
};
exports.metricModels = metricModels;