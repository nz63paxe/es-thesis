"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initIpToHostName = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _boom = require("boom");

var _lodash = require("lodash");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const ipToHostSchema = _joi.default.object({
  ip: _joi.default.string().required(),
  index_pattern: _joi.default.string().required()
});

const initIpToHostName = ({
  framework
}) => {
  const {
    callWithRequest
  } = framework;
  framework.registerRoute({
    method: 'POST',
    path: '/api/infra/ip_to_host',
    options: {
      validate: {
        payload: ipToHostSchema
      }
    },
    handler: async req => {
      try {
        const params = {
          index: req.payload.index_pattern,
          body: {
            size: 1,
            query: {
              match: {
                'host.ip': req.payload.ip
              }
            },
            _source: ['host.name']
          }
        };
        const response = await callWithRequest(req, 'search', params);

        if (response.hits.total.value === 0) {
          throw (0, _boom.notFound)('Host with matching IP address not found.');
        }

        const hostDoc = (0, _lodash.first)(response.hits.hits);
        return {
          host: hostDoc._source.host.name
        };
      } catch (e) {
        throw (0, _boom.boomify)(e);
      }
    }
  });
};

exports.initIpToHostName = initIpToHostName;