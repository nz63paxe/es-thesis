"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.metricsExplorerSchema = void 0;

var Joi = _interopRequireWildcard(require("joi"));

var _lodash = require("lodash");

var _color_palette = require("../../../common/color_palette");

var _types = require("./types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const metricsExplorerSchema = Joi.object({
  limit: Joi.number().min(1).default(9),
  afterKey: Joi.string().allow(null),
  groupBy: Joi.string().allow(null),
  indexPattern: Joi.string().required(),
  metrics: Joi.array().items(Joi.object().keys({
    aggregation: Joi.string().valid((0, _lodash.values)(_types.MetricsExplorerAggregation)).required(),
    field: Joi.string(),
    rate: Joi.bool().default(false),
    color: Joi.string().valid((0, _lodash.values)(_color_palette.MetricsExplorerColor)),
    label: Joi.string()
  })).required(),
  filterQuery: Joi.string(),
  timerange: Joi.object().keys({
    field: Joi.string().required(),
    from: Joi.number().required(),
    to: Joi.number().required(),
    interval: Joi.string().required()
  }).required()
});
exports.metricsExplorerSchema = metricsExplorerSchema;