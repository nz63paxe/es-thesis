"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createSnapshotResolvers = void 0;

var _usage_collector = require("../../usage/usage_collector");

var _serialized_query = require("../../utils/serialized_query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createSnapshotResolvers = libs => ({
  InfraSource: {
    async snapshot(source, args) {
      return {
        source,
        timerange: args.timerange,
        filterQuery: args.filterQuery
      };
    }

  },
  InfraSnapshotResponse: {
    async nodes(snapshotResponse, args, {
      req
    }) {
      const {
        source,
        timerange,
        filterQuery
      } = snapshotResponse;

      _usage_collector.UsageCollector.countNode(args.type);

      const options = {
        filterQuery: (0, _serialized_query.parseFilterQuery)(filterQuery),
        nodeType: args.type,
        groupBy: args.groupBy,
        sourceConfiguration: source.configuration,
        metric: args.metric,
        timerange
      };
      return await libs.snapshot.getNodes(req, options);
    }

  }
});

exports.createSnapshotResolvers = createSnapshotResolvers;