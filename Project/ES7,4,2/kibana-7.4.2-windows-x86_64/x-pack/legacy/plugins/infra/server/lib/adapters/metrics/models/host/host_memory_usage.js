"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hostMemoryUsage = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const hostMemoryUsage = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.hostMemoryUsage,
  requires: ['system.memory'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'free',
    metrics: [{
      field: 'system.memory.free',
      id: 'avg-memory-free',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }],
    split_mode: 'everything'
  }, {
    id: 'used',
    metrics: [{
      field: 'system.memory.actual.used.bytes',
      id: 'avg-memory-used',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }],
    split_mode: 'everything'
  }, {
    id: 'cache',
    metrics: [{
      field: 'system.memory.actual.used.bytes',
      id: 'avg-memory-actual-used',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }, {
      field: 'system.memory.used.bytes',
      id: 'avg-memory-used',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }, {
      id: 'calc-used-actual',
      script: 'params.used - params.actual',
      type: _adapter_types.InfraMetricModelMetricType.calculation,
      variables: [{
        field: 'avg-memory-actual-used',
        id: 'var-actual',
        name: 'actual'
      }, {
        field: 'avg-memory-used',
        id: 'var-used',
        name: 'used'
      }]
    }],
    split_mode: 'everything'
  }]
});

exports.hostMemoryUsage = hostMemoryUsage;