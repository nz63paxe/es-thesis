"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initInfraServer = void 0;

var _graphqlTools = require("graphql-tools");

var _ip_to_hostname = require("./routes/ip_to_hostname");

var _graphql = require("./graphql");

var _log_entries = require("./graphql/log_entries");

var _resolvers = require("./graphql/metrics/resolvers");

var _snapshot = require("./graphql/snapshot");

var _source_status = require("./graphql/source_status");

var _sources = require("./graphql/sources");

var _log_analysis = require("./routes/log_analysis");

var _metrics_explorer = require("./routes/metrics_explorer");

var _metadata = require("./routes/metadata");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const initInfraServer = libs => {
  const schema = (0, _graphqlTools.makeExecutableSchema)({
    resolvers: [(0, _log_entries.createLogEntriesResolvers)(libs), (0, _snapshot.createSnapshotResolvers)(libs), (0, _sources.createSourcesResolvers)(libs), (0, _source_status.createSourceStatusResolvers)(libs), (0, _resolvers.createMetricResolvers)(libs)],
    typeDefs: _graphql.schemas
  });
  libs.framework.registerGraphQLEndpoint('/api/infra/graphql', schema);
  (0, _ip_to_hostname.initIpToHostName)(libs);
  (0, _log_analysis.initLogAnalysisGetLogEntryRateRoute)(libs);
  (0, _metrics_explorer.initMetricExplorerRoute)(libs);
  (0, _metadata.initMetadataRoute)(libs);
};

exports.initInfraServer = initInfraServer;