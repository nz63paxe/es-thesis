"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.wrapRequest = wrapRequest;
exports.InfraKibanaBackendFrameworkAdapter = void 0;

var _lodash = require("lodash");

var _adapter_types = require("./adapter_types");

var _apollo_server_hapi = require("./apollo_server_hapi");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class InfraKibanaBackendFrameworkAdapter {
  constructor(server) {
    this.server = server;

    _defineProperty(this, "version", void 0);

    this.version = server.config().get('pkg.version');
  }

  config(req) {
    const internalRequest = req[_adapter_types.internalInfraFrameworkRequest];
    return internalRequest.server.config();
  }

  exposeStaticDir(urlPath, dir) {
    this.server.route({
      handler: {
        directory: {
          path: dir
        }
      },
      method: 'GET',
      path: urlPath
    });
  }

  registerGraphQLEndpoint(routePath, schema) {
    this.server.register({
      options: {
        graphqlOptions: req => ({
          context: {
            req: wrapRequest(req)
          },
          schema
        }),
        path: routePath,
        route: {
          tags: ['access:infra']
        }
      },
      plugin: _apollo_server_hapi.graphqlHapi
    });
    this.server.register({
      options: {
        graphiqlOptions: request => ({
          endpointURL: request ? `${request.getBasePath()}${routePath}` : routePath,
          passHeader: `'kbn-version': '${this.version}'`
        }),
        path: `${routePath}/graphiql`,
        route: {
          tags: ['access:infra']
        }
      },
      plugin: _apollo_server_hapi.graphiqlHapi
    });
  }

  registerRoute(route) {
    const wrappedHandler = (request, h) => route.handler(wrapRequest(request), h);

    this.server.route({
      handler: wrappedHandler,
      options: route.options,
      method: route.method,
      path: route.path
    });
  }

  async callWithRequest(req, endpoint, params, ...rest) {
    const internalRequest = req[_adapter_types.internalInfraFrameworkRequest];
    const {
      elasticsearch
    } = internalRequest.server.plugins;
    const {
      callWithRequest
    } = elasticsearch.getCluster('data');
    const includeFrozen = await internalRequest.getUiSettingsService().get('search:includeFrozen');

    if (endpoint === 'msearch') {
      const maxConcurrentShardRequests = await internalRequest.getUiSettingsService().get('courier:maxConcurrentShardRequests');

      if (maxConcurrentShardRequests > 0) {
        params = { ...params,
          max_concurrent_shard_requests: maxConcurrentShardRequests
        };
      }
    }

    const frozenIndicesParams = ['search', 'msearch'].includes(endpoint) ? {
      ignore_throttled: !includeFrozen
    } : {};
    const fields = await callWithRequest(internalRequest, endpoint, { ...params,
      ...frozenIndicesParams
    }, ...rest);
    return fields;
  }

  getIndexPatternsService(request) {
    return this.server.indexPatternsServiceFactory({
      callCluster: async (method, args, ...rest) => {
        const fieldCaps = await this.callWithRequest(request, method, { ...args,
          allowNoIndices: true
        }, ...rest);
        return fieldCaps;
      }
    });
  }

  getSpaceId(request) {
    const spacesPlugin = this.server.plugins.spaces;

    if (spacesPlugin && typeof spacesPlugin.getSpaceId === 'function') {
      return spacesPlugin.getSpaceId(request[_adapter_types.internalInfraFrameworkRequest]);
    } else {
      return 'default';
    }
  }

  getSavedObjectsService() {
    return this.server.savedObjects;
  }

  async makeTSVBRequest(req, model, timerange, filters) {
    const internalRequest = req[_adapter_types.internalInfraFrameworkRequest];
    const server = internalRequest.server;
    const getVisData = (0, _lodash.get)(server, 'plugins.metrics.getVisData');

    if (typeof getVisData !== 'function') {
      throw new Error('TSVB is not available');
    } // getBasePath returns randomized base path AND spaces path


    const basePath = internalRequest.getBasePath();
    const url = `${basePath}/api/metrics/vis/data`; // For the following request we need a copy of the instnace of the internal request
    // but modified for our TSVB request. This will ensure all the instance methods
    // are available along with our overriden values

    const request = Object.assign(Object.create(Object.getPrototypeOf(internalRequest)), internalRequest, {
      url,
      method: 'POST',
      payload: {
        timerange,
        panels: [model],
        filters
      }
    });
    const result = await getVisData(request);
    return result;
  }

}

exports.InfraKibanaBackendFrameworkAdapter = InfraKibanaBackendFrameworkAdapter;

function wrapRequest(req) {
  const {
    params,
    payload,
    query
  } = req;
  return {
    [_adapter_types.internalInfraFrameworkRequest]: req,
    params,
    payload,
    query
  };
}