"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraLogEntriesDomain = void 0;

var _jsonStableStringify = _interopRequireDefault(require("json-stable-stringify"));

var _lodash = require("lodash");

var _sources = require("../../sources");

var _builtin_rules = require("./builtin_rules");

var _convert_document_source_to_log_item_fields = require("./convert_document_source_to_log_item_fields");

var _message = require("./message");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
class InfraLogEntriesDomain {
  constructor(adapter, libs) {
    this.adapter = adapter;
    this.libs = libs;
  }

  async getLogEntriesAround(request, sourceId, key, maxCountBefore, maxCountAfter, filterQuery, highlightQuery) {
    if (maxCountBefore <= 0 && maxCountAfter <= 0) {
      return {
        entriesBefore: [],
        entriesAfter: []
      };
    }

    const {
      configuration
    } = await this.libs.sources.getSourceConfiguration(request, sourceId);
    const messageFormattingRules = (0, _message.compileFormattingRules)((0, _builtin_rules.getBuiltinRules)(configuration.fields.message));
    const requiredFields = getRequiredFields(configuration, messageFormattingRules);
    const documentsBefore = await this.adapter.getAdjacentLogEntryDocuments(request, configuration, requiredFields, key, 'desc', Math.max(maxCountBefore, 1), filterQuery, highlightQuery);
    const lastKeyBefore = documentsBefore.length > 0 ? documentsBefore[documentsBefore.length - 1].key : {
      time: key.time - 1,
      tiebreaker: 0
    };
    const documentsAfter = await this.adapter.getAdjacentLogEntryDocuments(request, configuration, requiredFields, lastKeyBefore, 'asc', maxCountAfter, filterQuery, highlightQuery);
    return {
      entriesBefore: (maxCountBefore > 0 ? documentsBefore : []).map(convertLogDocumentToEntry(sourceId, configuration.logColumns, messageFormattingRules.format)),
      entriesAfter: documentsAfter.map(convertLogDocumentToEntry(sourceId, configuration.logColumns, messageFormattingRules.format))
    };
  }

  async getLogEntriesBetween(request, sourceId, startKey, endKey, filterQuery, highlightQuery) {
    const {
      configuration
    } = await this.libs.sources.getSourceConfiguration(request, sourceId);
    const messageFormattingRules = (0, _message.compileFormattingRules)((0, _builtin_rules.getBuiltinRules)(configuration.fields.message));
    const requiredFields = getRequiredFields(configuration, messageFormattingRules);
    const documents = await this.adapter.getContainedLogEntryDocuments(request, configuration, requiredFields, startKey, endKey, filterQuery, highlightQuery);
    const entries = documents.map(convertLogDocumentToEntry(sourceId, configuration.logColumns, messageFormattingRules.format));
    return entries;
  }

  async getLogEntryHighlights(request, sourceId, startKey, endKey, highlights, filterQuery) {
    const {
      configuration
    } = await this.libs.sources.getSourceConfiguration(request, sourceId);
    const messageFormattingRules = (0, _message.compileFormattingRules)((0, _builtin_rules.getBuiltinRules)(configuration.fields.message));
    const requiredFields = getRequiredFields(configuration, messageFormattingRules);
    const documentSets = await Promise.all(highlights.map(async highlight => {
      const highlightQuery = createHighlightQueryDsl(highlight.query, requiredFields);
      const query = filterQuery ? {
        bool: {
          filter: [filterQuery, highlightQuery]
        }
      } : highlightQuery;
      const [documentsBefore, documents, documentsAfter] = await Promise.all([this.adapter.getAdjacentLogEntryDocuments(request, configuration, requiredFields, startKey, 'desc', highlight.countBefore, query, highlightQuery), this.adapter.getContainedLogEntryDocuments(request, configuration, requiredFields, startKey, endKey, query, highlightQuery), this.adapter.getAdjacentLogEntryDocuments(request, configuration, requiredFields, endKey, 'asc', highlight.countAfter, query, highlightQuery)]);
      const entries = [...documentsBefore, ...documents, ...documentsAfter].map(convertLogDocumentToEntry(sourceId, configuration.logColumns, messageFormattingRules.format));
      return entries;
    }));
    return documentSets;
  }

  async getLogSummaryBucketsBetween(request, sourceId, start, end, bucketSize, filterQuery) {
    const {
      configuration
    } = await this.libs.sources.getSourceConfiguration(request, sourceId);
    const dateRangeBuckets = await this.adapter.getContainedLogSummaryBuckets(request, configuration, start, end, bucketSize, filterQuery);
    return dateRangeBuckets;
  }

  async getLogSummaryHighlightBucketsBetween(request, sourceId, start, end, bucketSize, highlightQueries, filterQuery) {
    const {
      configuration
    } = await this.libs.sources.getSourceConfiguration(request, sourceId);
    const messageFormattingRules = (0, _message.compileFormattingRules)((0, _builtin_rules.getBuiltinRules)(configuration.fields.message));
    const requiredFields = getRequiredFields(configuration, messageFormattingRules);
    const summaries = await Promise.all(highlightQueries.map(async highlightQueryPhrase => {
      const highlightQuery = createHighlightQueryDsl(highlightQueryPhrase, requiredFields);
      const query = filterQuery ? {
        bool: {
          must: [filterQuery, highlightQuery]
        }
      } : highlightQuery;
      const summaryBuckets = await this.adapter.getContainedLogSummaryBuckets(request, configuration, start, end, bucketSize, query);
      const summaryHighlightBuckets = summaryBuckets.filter(logSummaryBucketHasEntries).map(convertLogSummaryBucketToSummaryHighlightBucket);
      return summaryHighlightBuckets;
    }));
    return summaries;
  }

  async getLogItem(request, id, sourceConfiguration) {
    const document = await this.adapter.getLogItem(request, id, sourceConfiguration);
    const defaultFields = [{
      field: '_index',
      value: document._index
    }, {
      field: '_id',
      value: document._id
    }];
    return {
      id: document._id,
      index: document._index,
      key: {
        time: document.sort[0],
        tiebreaker: document.sort[1]
      },
      fields: (0, _lodash.sortBy)([...defaultFields, ...(0, _convert_document_source_to_log_item_fields.convertDocumentSourceToLogItemFields)(document._source)], 'field')
    };
  }

}

exports.InfraLogEntriesDomain = InfraLogEntriesDomain;

const convertLogDocumentToEntry = (sourceId, logColumns, formatLogMessage) => document => ({
  key: document.key,
  gid: document.gid,
  source: sourceId,
  columns: logColumns.map(logColumn => {
    if (_sources.SavedSourceConfigurationTimestampColumnRuntimeType.is(logColumn)) {
      return {
        columnId: logColumn.timestampColumn.id,
        timestamp: document.key.time
      };
    } else if (_sources.SavedSourceConfigurationMessageColumnRuntimeType.is(logColumn)) {
      return {
        columnId: logColumn.messageColumn.id,
        message: formatLogMessage(document.fields, document.highlights)
      };
    } else {
      return {
        columnId: logColumn.fieldColumn.id,
        field: logColumn.fieldColumn.field,
        highlights: document.highlights[logColumn.fieldColumn.field] || [],
        value: (0, _jsonStableStringify.default)(document.fields[logColumn.fieldColumn.field] || null)
      };
    }
  })
});

const logSummaryBucketHasEntries = bucket => bucket.entriesCount > 0 && bucket.topEntryKeys.length > 0;

const convertLogSummaryBucketToSummaryHighlightBucket = bucket => ({
  entriesCount: bucket.entriesCount,
  start: bucket.start,
  end: bucket.end,
  representativeKey: bucket.topEntryKeys[0]
});

const getRequiredFields = (configuration, messageFormattingRules) => {
  const fieldsFromCustomColumns = configuration.logColumns.reduce((accumulatedFields, logColumn) => {
    if (_sources.SavedSourceConfigurationFieldColumnRuntimeType.is(logColumn)) {
      return [...accumulatedFields, logColumn.fieldColumn.field];
    }

    return accumulatedFields;
  }, []);
  const fieldsFromFormattingRules = messageFormattingRules.requiredFields;
  return Array.from(new Set([...fieldsFromCustomColumns, ...fieldsFromFormattingRules]));
};

const createHighlightQueryDsl = (phrase, fields) => ({
  multi_match: {
    fields,
    lenient: true,
    query: phrase,
    type: 'phrase'
  }
});