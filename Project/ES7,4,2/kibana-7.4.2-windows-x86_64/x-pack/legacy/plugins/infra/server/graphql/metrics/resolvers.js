"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createMetricResolvers = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

var _sources = require("../../lib/sources");

var _usage_collector = require("../../usage/usage_collector");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createMetricResolvers = libs => ({
  InfraSource: {
    async metrics(source, args, {
      req
    }) {
      const sourceConfiguration = _sources.SourceConfigurationRuntimeType.decode(source.configuration).getOrElseL(errors => {
        throw new Error((0, _PathReporter.failure)(errors).join('\n'));
      });

      _usage_collector.UsageCollector.countNode(args.nodeType);

      const options = {
        nodeIds: args.nodeIds,
        nodeType: args.nodeType,
        timerange: args.timerange,
        metrics: args.metrics,
        sourceConfiguration
      };
      return libs.metrics.getMetrics(req, options);
    }

  }
});

exports.createMetricResolvers = createMetricResolvers;