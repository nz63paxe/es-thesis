"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNodeInfo = void 0;

var _lodash = require("lodash");

var _types = require("../../../graphql/types");

var _get_pod_node_name = require("./get_pod_node_name");

var _constants = require("../../../lib/constants");

var _get_id_field_name = require("./get_id_field_name");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const getNodeInfo = async (framework, req, sourceConfiguration, nodeId, nodeType) => {
  // If the nodeType is a Kubernetes pod then we need to get the node info
  // from a host record instead of a pod. This is due to the fact that any host
  // can report pod details and we can't rely on the host/cloud information associated
  // with the kubernetes.pod.uid. We need to first lookup the `kubernetes.node.name`
  // then use that to lookup the host's node information.
  if (nodeType === _types.InfraNodeType.pod) {
    const kubernetesNodeName = await (0, _get_pod_node_name.getPodNodeName)(framework, req, sourceConfiguration, nodeId, nodeType);

    if (kubernetesNodeName) {
      return getNodeInfo(framework, req, sourceConfiguration, kubernetesNodeName, _types.InfraNodeType.host);
    }

    return {};
  }

  const params = {
    allowNoIndices: true,
    ignoreUnavailable: true,
    terminateAfter: 1,
    index: sourceConfiguration.metricAlias,
    body: {
      size: 1,
      _source: ['host.*', 'cloud.*'],
      query: {
        bool: {
          must_not: _constants.CLOUD_METRICS_MODULES.map(module => ({
            match: {
              'event.module': module
            }
          })),
          filter: [{
            match: {
              [(0, _get_id_field_name.getIdFieldName)(sourceConfiguration, nodeType)]: nodeId
            }
          }]
        }
      }
    }
  };
  const response = await framework.callWithRequest(req, 'search', params);
  const firstHit = (0, _lodash.first)(response.hits.hits);

  if (firstHit) {
    return firstHit._source;
  }

  return {};
};

exports.getNodeInfo = getNodeInfo;