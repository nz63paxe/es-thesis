"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.containerOverview = void 0;

var _adapter_types = require("../../adapter_types");

var _types = require("../../../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const containerOverview = (timeField, indexPattern, interval) => ({
  id: _types.InfraMetric.containerOverview,
  requires: ['docker'],
  index_pattern: indexPattern,
  interval,
  time_field: timeField,
  type: 'timeseries',
  series: [{
    id: 'cpu',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.cpu.total.pct',
      id: 'avg-cpu-total',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }, {
    id: 'memory',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.memory.usage.pct',
      id: 'avg-memory',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }, {
    id: 'tx',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.network.out.bytes',
      id: 'avg-network-out',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }, {
    id: 'rx',
    split_mode: 'everything',
    metrics: [{
      field: 'docker.network.in.bytes',
      id: 'avg-network-in',
      type: _adapter_types.InfraMetricModelMetricType.avg
    }]
  }]
});

exports.containerOverview = containerOverview;