"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraSources = void 0;

var runtimeTypes = _interopRequireWildcard(require("io-ts"));

var _PathReporter = require("io-ts/lib/PathReporter");

var _framework = require("../adapters/framework");

var _defaults = require("./defaults");

var _errors = require("./errors");

var _saved_object_mappings = require("./saved_object_mappings");

var _types = require("./types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class InfraSources {
  constructor(libs) {
    this.libs = libs;

    _defineProperty(this, "internalSourceConfigurations", new Map());
  }

  async getSourceConfiguration(request, sourceId) {
    const staticDefaultSourceConfiguration = await this.getStaticDefaultSourceConfiguration();
    const savedSourceConfiguration = await this.getInternalSourceConfiguration(sourceId).then(internalSourceConfiguration => ({
      id: sourceId,
      version: undefined,
      updatedAt: undefined,
      origin: 'internal',
      configuration: mergeSourceConfiguration(staticDefaultSourceConfiguration, internalSourceConfiguration)
    })).catch(err => err instanceof _errors.NotFoundError ? this.getSavedSourceConfiguration(request, sourceId).then(result => ({ ...result,
      configuration: mergeSourceConfiguration(staticDefaultSourceConfiguration, result.configuration)
    })) : Promise.reject(err)).catch(err => this.libs.savedObjects.SavedObjectsClient.errors.isNotFoundError(err) ? Promise.resolve({
      id: sourceId,
      version: undefined,
      updatedAt: undefined,
      origin: 'fallback',
      configuration: staticDefaultSourceConfiguration
    }) : Promise.reject(err));
    return savedSourceConfiguration;
  }

  async getAllSourceConfigurations(request) {
    const staticDefaultSourceConfiguration = await this.getStaticDefaultSourceConfiguration();
    const savedSourceConfigurations = await this.getAllSavedSourceConfigurations(request);
    return savedSourceConfigurations.map(savedSourceConfiguration => ({ ...savedSourceConfiguration,
      configuration: mergeSourceConfiguration(staticDefaultSourceConfiguration, savedSourceConfiguration.configuration)
    }));
  }

  async createSourceConfiguration(request, sourceId, source) {
    const staticDefaultSourceConfiguration = await this.getStaticDefaultSourceConfiguration();
    const newSourceConfiguration = mergeSourceConfiguration(staticDefaultSourceConfiguration, source);
    const createdSourceConfiguration = convertSavedObjectToSavedSourceConfiguration((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalInfraFrameworkRequest]).create(_saved_object_mappings.infraSourceConfigurationSavedObjectType, (0, _types.pickSavedSourceConfiguration)(newSourceConfiguration), {
      id: sourceId
    })));
    return { ...createdSourceConfiguration,
      configuration: mergeSourceConfiguration(staticDefaultSourceConfiguration, createdSourceConfiguration.configuration)
    };
  }

  async deleteSourceConfiguration(request, sourceId) {
    await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalInfraFrameworkRequest]).delete(_saved_object_mappings.infraSourceConfigurationSavedObjectType, sourceId);
  }

  async updateSourceConfiguration(request, sourceId, sourceProperties) {
    const staticDefaultSourceConfiguration = await this.getStaticDefaultSourceConfiguration();
    const {
      configuration,
      version
    } = await this.getSourceConfiguration(request, sourceId);
    const updatedSourceConfigurationAttributes = mergeSourceConfiguration(configuration, sourceProperties);
    const updatedSourceConfiguration = convertSavedObjectToSavedSourceConfiguration((await this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalInfraFrameworkRequest]).update(_saved_object_mappings.infraSourceConfigurationSavedObjectType, sourceId, (0, _types.pickSavedSourceConfiguration)(updatedSourceConfigurationAttributes), {
      version
    })));
    return { ...updatedSourceConfiguration,
      configuration: mergeSourceConfiguration(staticDefaultSourceConfiguration, updatedSourceConfiguration.configuration)
    };
  }

  async defineInternalSourceConfiguration(sourceId, sourceProperties) {
    this.internalSourceConfigurations.set(sourceId, sourceProperties);
  }

  async getInternalSourceConfiguration(sourceId) {
    const internalSourceConfiguration = this.internalSourceConfigurations.get(sourceId);

    if (!internalSourceConfiguration) {
      throw new _errors.NotFoundError(`Failed to load internal source configuration: no configuration "${sourceId}" found.`);
    }

    return internalSourceConfiguration;
  }

  async getStaticDefaultSourceConfiguration() {
    const staticConfiguration = await this.libs.configuration.get();
    const staticSourceConfiguration = runtimeTypes.type({
      sources: runtimeTypes.type({
        default: _types.StaticSourceConfigurationRuntimeType
      })
    }).decode(staticConfiguration).map(({
      sources: {
        default: defaultConfiguration
      }
    }) => defaultConfiguration).getOrElse({});
    return mergeSourceConfiguration(_defaults.defaultSourceConfiguration, staticSourceConfiguration);
  }

  async getSavedSourceConfiguration(request, sourceId) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalInfraFrameworkRequest]);
    const savedObject = await savedObjectsClient.get(_saved_object_mappings.infraSourceConfigurationSavedObjectType, sourceId);
    return convertSavedObjectToSavedSourceConfiguration(savedObject);
  }

  async getAllSavedSourceConfigurations(request) {
    const savedObjectsClient = this.libs.savedObjects.getScopedSavedObjectsClient(request[_framework.internalInfraFrameworkRequest]);
    const savedObjects = await savedObjectsClient.find({
      type: _saved_object_mappings.infraSourceConfigurationSavedObjectType
    });
    return savedObjects.saved_objects.map(convertSavedObjectToSavedSourceConfiguration);
  }

}

exports.InfraSources = InfraSources;

const mergeSourceConfiguration = (first, ...others) => others.reduce((previousSourceConfiguration, currentSourceConfiguration) => ({ ...previousSourceConfiguration,
  ...currentSourceConfiguration,
  fields: { ...previousSourceConfiguration.fields,
    ...currentSourceConfiguration.fields
  }
}), first);

const convertSavedObjectToSavedSourceConfiguration = savedObject => _types.SourceConfigurationSavedObjectRuntimeType.decode(savedObject).map(savedSourceConfiguration => ({
  id: savedSourceConfiguration.id,
  version: savedSourceConfiguration.version,
  updatedAt: savedSourceConfiguration.updated_at,
  origin: 'stored',
  configuration: savedSourceConfiguration.attributes
})).getOrElseL(errors => {
  throw new Error((0, _PathReporter.failure)(errors).join('\n'));
});