"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.savedObjectMappings = void 0;

var _sources = require("./lib/sources");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const savedObjectMappings = { ..._sources.infraSourceConfigurationSavedObjectMappings
};
exports.savedObjectMappings = savedObjectMappings;