"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.infra = infra;

var _i18n = require("@kbn/i18n");

var _path = require("path");

var _kibana = require("./server/kibana.index");

var _saved_objects = require("./server/saved_objects");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const APP_ID = 'infra';

function infra(kibana) {
  return new kibana.Plugin({
    id: APP_ID,
    configPrefix: 'xpack.infra',
    publicDir: (0, _path.resolve)(__dirname, 'public'),
    require: ['kibana', 'elasticsearch', 'metrics'],
    uiExports: {
      app: {
        description: _i18n.i18n.translate('xpack.infra.infrastructureDescription', {
          defaultMessage: 'Explore your infrastructure'
        }),
        icon: 'plugins/infra/images/infra_mono_white.svg',
        main: 'plugins/infra/app',
        title: _i18n.i18n.translate('xpack.infra.infrastructureTitle', {
          defaultMessage: 'Infrastructure'
        }),
        listed: false,
        url: `/app/${APP_ID}#/infrastructure`
      },
      styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),
      home: ['plugins/infra/register_feature'],
      links: [{
        description: _i18n.i18n.translate('xpack.infra.linkInfrastructureDescription', {
          defaultMessage: 'Explore your infrastructure'
        }),
        icon: 'plugins/infra/images/infra_mono_white.svg',
        euiIconType: 'infraApp',
        id: 'infra:home',
        order: 8000,
        title: _i18n.i18n.translate('xpack.infra.linkInfrastructureTitle', {
          defaultMessage: 'Infrastructure'
        }),
        url: `/app/${APP_ID}#/infrastructure`
      }, {
        description: _i18n.i18n.translate('xpack.infra.linkLogsDescription', {
          defaultMessage: 'Explore your logs'
        }),
        icon: 'plugins/infra/images/logging_mono_white.svg',
        euiIconType: 'loggingApp',
        id: 'infra:logs',
        order: 8001,
        title: _i18n.i18n.translate('xpack.infra.linkLogsTitle', {
          defaultMessage: 'Logs'
        }),
        url: `/app/${APP_ID}#/logs`
      }],
      mappings: _saved_objects.savedObjectMappings
    },

    config(Joi) {
      return (0, _kibana.getConfigSchema)(Joi);
    },

    init(server) {
      (0, _kibana.initServerWithKibana)(server);
    }

  });
}