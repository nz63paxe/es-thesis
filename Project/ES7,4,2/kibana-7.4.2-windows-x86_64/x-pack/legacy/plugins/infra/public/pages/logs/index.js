"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogsPage = void 0;

var _react = require("@kbn/i18n/react");

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _react2 = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _react3 = require("ui/capabilities/react");

var _document_title = require("../../components/document_title");

var _help_center_content = require("../../components/help_center_content");

var _header = require("../../components/header");

var _routed_tabs = require("../../components/navigation/routed_tabs");

var _page = require("../../components/page");

var _source_loading_page = require("../../components/source_loading_page");

var _source_error_page = require("../../components/source_error_page");

var _source = require("../../containers/source");

var _stream = require("./stream");

var _settings = require("../shared/settings");

var _app_navigation = require("../../components/navigation/app_navigation");

var _analysis = require("./analysis");

var _log_analysis = require("../../containers/logs/log_analysis");

var _source_id = require("../../containers/source_id");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var LOG_ANALYSIS_ON = false;
var LogsPage = (0, _react3.injectUICapabilities)((0, _react.injectI18n)(function (_ref) {
  var match = _ref.match,
      intl = _ref.intl,
      uiCapabilities = _ref.uiCapabilities;

  var _useSourceId = (0, _source_id.useSourceId)(),
      _useSourceId2 = _slicedToArray(_useSourceId, 1),
      sourceId = _useSourceId2[0];

  var source = (0, _source.useSource)({
    sourceId: sourceId
  });
  var logAnalysisCapabilities = (0, _log_analysis.useLogAnalysisCapabilities)();
  var streamTab = {
    title: intl.formatMessage({
      id: 'xpack.infra.logs.index.streamTabTitle',
      defaultMessage: 'Stream'
    }),
    path: "".concat(match.path, "/stream")
  };

  var analysisBetaBadgeTitle = _i18n.i18n.translate('xpack.infra.logs.index.analysisBetaBadgeTitle', {
    defaultMessage: 'Analysis'
  });

  var analysisBetaBadgeLabel = _i18n.i18n.translate('xpack.infra.logs.index.analysisBetaBadgeLabel', {
    defaultMessage: 'Beta'
  });

  var analysisBetaBadgeTooltipContent = _i18n.i18n.translate('xpack.infra.logs.index.analysisBetaBadgeTooltipContent', {
    defaultMessage: 'This feature is under active development. Extra functionality is coming, and some functionality may change.'
  });

  var analysisBetaBadge = _react2.default.createElement(_eui.EuiBetaBadge, {
    label: analysisBetaBadgeLabel,
    "aria-label": analysisBetaBadgeLabel,
    title: analysisBetaBadgeTitle,
    tooltipContent: analysisBetaBadgeTooltipContent
  });

  var analysisTab = {
    title: _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement("span", {
      style: {
        display: 'inline-block',
        position: 'relative',
        top: '-4px',
        marginRight: '5px'
      }
    }, intl.formatMessage({
      id: 'xpack.infra.logs.index.analysisTabTitle',
      defaultMessage: 'Analysis'
    })), analysisBetaBadge),
    path: "".concat(match.path, "/analysis")
  };
  var settingsTab = {
    title: intl.formatMessage({
      id: 'xpack.infra.logs.index.settingsTabTitle',
      defaultMessage: 'Settings'
    }),
    path: "".concat(match.path, "/settings")
  };
  return _react2.default.createElement(_source.Source.Context.Provider, {
    value: source
  }, _react2.default.createElement(_log_analysis.LogAnalysisCapabilities.Context.Provider, {
    value: logAnalysisCapabilities
  }, _react2.default.createElement(_page.ColumnarPage, null, _react2.default.createElement(_document_title.DocumentTitle, {
    title: intl.formatMessage({
      id: 'xpack.infra.logs.index.documentTitle',
      defaultMessage: 'Logs'
    })
  }), _react2.default.createElement(_help_center_content.HelpCenterContent, {
    feedbackLink: "https://discuss.elastic.co/c/logs",
    feedbackLinkText: intl.formatMessage({
      id: 'xpack.infra.logsPage.logsHelpContent.feedbackLinkText',
      defaultMessage: 'Provide feedback for Logs'
    })
  }), _react2.default.createElement(_header.Header, {
    breadcrumbs: [{
      text: _i18n.i18n.translate('xpack.infra.header.logsTitle', {
        defaultMessage: 'Logs'
      })
    }],
    readOnlyBadge: !uiCapabilities.logs.save
  }), source.isLoadingSource || !source.isLoadingSource && !source.hasFailedLoadingSource && source.source === undefined ? _react2.default.createElement(_source_loading_page.SourceLoadingPage, null) : source.hasFailedLoadingSource ? _react2.default.createElement(_source_error_page.SourceErrorPage, {
    errorMessage: source.loadSourceFailureMessage || '',
    retry: source.loadSource
  }) : _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_app_navigation.AppNavigation, null, _react2.default.createElement(_routed_tabs.RoutedTabs, {
    tabs: logAnalysisCapabilities.hasLogAnalysisCapabilites && LOG_ANALYSIS_ON ? [streamTab, analysisTab, settingsTab] : [streamTab, settingsTab]
  })), _react2.default.createElement(_reactRouterDom.Switch, null, _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/stream"),
    component: _stream.StreamPage
  }), _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/analysis"),
    component: _analysis.AnalysisPage
  }), _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/settings"),
    component: _settings.SettingsPage
  }))))));
}));
exports.LogsPage = LogsPage;