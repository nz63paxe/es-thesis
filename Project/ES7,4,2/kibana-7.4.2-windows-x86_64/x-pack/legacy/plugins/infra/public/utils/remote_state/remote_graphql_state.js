"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createGraphqlStateSelectors = exports.createGraphqlQueryEpic = exports.createGraphqlOperationReducer = exports.createGraphqlOperationActionCreators = exports.createGraphqlInitialState = void 0;

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _typescriptFsa = require("typescript-fsa");

var _dist = require("typescript-fsa-reducers/dist");

var _reselect = require("reselect");

var _loading_state = require("../loading_state");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var createGraphqlInitialState = function createGraphqlInitialState(initialData) {
  return {
    current: {
      progress: 'idle'
    },
    last: {
      result: 'uninitialized'
    },
    data: initialData
  };
};

exports.createGraphqlInitialState = createGraphqlInitialState;

var createGraphqlOperationActionCreators = function createGraphqlOperationActionCreators(stateKey, operationKey) {
  var actionCreator = (0, _typescriptFsa.actionCreatorFactory)("x-pack/infra/remote/".concat(stateKey, "/").concat(operationKey));
  var resolve = actionCreator('RESOLVE');
  var resolveEffect = actionCreator.async('RESOLVE');
  return {
    resolve: resolve,
    resolveStarted: resolveEffect.started,
    resolveDone: resolveEffect.done,
    resolveFailed: resolveEffect.failed
  };
};

exports.createGraphqlOperationActionCreators = createGraphqlOperationActionCreators;

var createGraphqlOperationReducer = function createGraphqlOperationReducer(operationKey, initialState, actionCreators) {
  var reduceSuccess = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (state) {
    return state;
  };
  var reduceFailure = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : function (state) {
    return state;
  };
  return (0, _dist.reducerWithInitialState)(initialState).caseWithAction(actionCreators.resolveStarted, function (state, action) {
    return _objectSpread({}, state, {
      current: {
        progress: 'running',
        time: Date.now(),
        parameters: {
          operationKey: operationKey,
          variables: action.payload
        }
      }
    });
  }).caseWithAction(actionCreators.resolveDone, function (state, action) {
    return _objectSpread({}, state, {
      current: {
        progress: 'idle'
      },
      last: {
        result: 'success',
        parameters: {
          operationKey: operationKey,
          variables: action.payload.params
        },
        time: Date.now(),
        isExhausted: false
      },
      data: reduceSuccess(state.data, action)
    });
  }).caseWithAction(actionCreators.resolveFailed, function (state, action) {
    return _objectSpread({}, state, {
      current: {
        progress: 'idle'
      },
      last: {
        result: 'failure',
        reason: "".concat(action.payload),
        time: Date.now(),
        parameters: {
          operationKey: operationKey,
          variables: action.payload.params
        }
      },
      data: reduceFailure(state.data, action)
    });
  }).build();
};

exports.createGraphqlOperationReducer = createGraphqlOperationReducer;

var createGraphqlQueryEpic = function createGraphqlQueryEpic(graphqlQuery, actionCreators) {
  return function (action$, state$, _ref) {
    var apolloClient$ = _ref.apolloClient$;
    return action$.pipe((0, _operators.filter)(actionCreators.resolve.match), (0, _operators.withLatestFrom)(apolloClient$), (0, _operators.switchMap)(function (_ref2) {
      var _ref3 = _slicedToArray(_ref2, 2),
          variables = _ref3[0].payload,
          apolloClient = _ref3[1];

      return (0, _rxjs.from)(apolloClient.query({
        query: graphqlQuery,
        variables: variables,
        fetchPolicy: 'no-cache'
      })).pipe((0, _operators.map)(function (result) {
        return actionCreators.resolveDone({
          params: variables,
          result: result
        });
      }), (0, _operators.catchError)(function (error) {
        return [actionCreators.resolveFailed({
          params: variables,
          error: error
        })];
      }), (0, _operators.startWith)(actionCreators.resolveStarted(variables)));
    }));
  };
};

exports.createGraphqlQueryEpic = createGraphqlQueryEpic;

var createGraphqlStateSelectors = function createGraphqlStateSelectors() {
  var selectState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function (parentState) {
    return parentState;
  };
  var selectData = (0, _reselect.createSelector)(selectState, function (state) {
    return state.data;
  });
  var selectLoadingProgress = (0, _reselect.createSelector)(selectState, function (state) {
    return state.current;
  });
  var selectLoadingProgressOperationInfo = (0, _reselect.createSelector)(selectLoadingProgress, function (progress) {
    return (0, _loading_state.isRunningLoadingProgress)(progress) ? progress.parameters : null;
  });
  var selectIsLoading = (0, _reselect.createSelector)(selectLoadingProgress, _loading_state.isRunningLoadingProgress);
  var selectIsIdle = (0, _reselect.createSelector)(selectLoadingProgress, _loading_state.isIdleLoadingProgress);
  var selectLoadingResult = (0, _reselect.createSelector)(selectState, function (state) {
    return state.last;
  });
  var selectLoadingResultOperationInfo = (0, _reselect.createSelector)(selectLoadingResult, function (result) {
    return !(0, _loading_state.isUninitializedLoadingResult)(result) ? result.parameters : null;
  });
  var selectLoadingResultTime = (0, _reselect.createSelector)(selectLoadingResult, function (result) {
    return !(0, _loading_state.isUninitializedLoadingResult)(result) ? result.time : null;
  });
  var selectIsUninitialized = (0, _reselect.createSelector)(selectLoadingResult, _loading_state.isUninitializedLoadingResult);
  var selectIsSuccess = (0, _reselect.createSelector)(selectLoadingResult, _loading_state.isSuccessLoadingResult);
  var selectIsFailure = (0, _reselect.createSelector)(selectLoadingResult, _loading_state.isFailureLoadingResult);
  var selectLoadingState = (0, _reselect.createSelector)(selectLoadingProgress, selectLoadingResult, function (loadingProgress, loadingResult) {
    return {
      current: loadingProgress,
      last: loadingResult,
      policy: {
        policy: 'manual'
      }
    };
  });
  return {
    selectData: selectData,
    selectIsFailure: selectIsFailure,
    selectIsIdle: selectIsIdle,
    selectIsLoading: selectIsLoading,
    selectIsSuccess: selectIsSuccess,
    selectIsUninitialized: selectIsUninitialized,
    selectLoadingProgress: selectLoadingProgress,
    selectLoadingProgressOperationInfo: selectLoadingProgressOperationInfo,
    selectLoadingResult: selectLoadingResult,
    selectLoadingResultOperationInfo: selectLoadingResultOperationInfo,
    selectLoadingResultTime: selectLoadingResultTime,
    selectLoadingState: selectLoadingState
  };
};

exports.createGraphqlStateSelectors = createGraphqlStateSelectors;