"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logPositionReducer = exports.initialLogPositionState = void 0;

var _redux = require("redux");

var _dist = require("typescript-fsa-reducers/dist");

var _actions = require("./actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialLogPositionState = {
  targetPosition: null,
  updatePolicy: {
    policy: 'manual'
  },
  visiblePositions: {
    endKey: null,
    middleKey: null,
    startKey: null
  },
  controlsShouldDisplayTargetPosition: false
};
exports.initialLogPositionState = initialLogPositionState;
var targetPositionReducer = (0, _dist.reducerWithInitialState)(initialLogPositionState.targetPosition).case(_actions.jumpToTargetPosition, function (state, target) {
  return target;
});
var targetPositionUpdatePolicyReducer = (0, _dist.reducerWithInitialState)(initialLogPositionState.updatePolicy).case(_actions.startAutoReload, function (state, interval) {
  return {
    policy: 'interval',
    interval: interval
  };
}).case(_actions.stopAutoReload, function () {
  return {
    policy: 'manual'
  };
});
var visiblePositionReducer = (0, _dist.reducerWithInitialState)(initialLogPositionState.visiblePositions).case(_actions.reportVisiblePositions, function (state, _ref) {
  var startKey = _ref.startKey,
      middleKey = _ref.middleKey,
      endKey = _ref.endKey;
  return {
    endKey: endKey,
    middleKey: middleKey,
    startKey: startKey
  };
}); // Determines whether to use the target position or the visible midpoint when
// displaying a timestamp or time range in the toolbar and log minimap. When the
// user jumps to a new target, the final visible midpoint is indeterminate until
// all the new data has finished loading, so using this flag reduces the perception
// that the UI is jumping around inaccurately

var controlsShouldDisplayTargetPositionReducer = (0, _dist.reducerWithInitialState)(initialLogPositionState.controlsShouldDisplayTargetPosition).case(_actions.jumpToTargetPosition, function () {
  return true;
}).case(_actions.reportVisiblePositions, function (state, _ref2) {
  var fromScroll = _ref2.fromScroll;
  if (fromScroll) return false;
  return state;
});
var logPositionReducer = (0, _redux.combineReducers)({
  targetPosition: targetPositionReducer,
  updatePolicy: targetPositionUpdatePolicyReducer,
  visiblePositions: visiblePositionReducer,
  controlsShouldDisplayTargetPosition: controlsShouldDisplayTargetPositionReducer
});
exports.logPositionReducer = logPositionReducer;