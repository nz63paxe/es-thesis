"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Toolbar = void 0;

var _eui = require("@elastic/eui");

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  border-top: none;\n  border-right: none;\n  border-left: none;\n  border-radius: 0;\n  padding: ", " ", ";\n  z-index: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var Toolbar = (0, _eui_styled_components.default)(_eui.EuiPanel).attrs({
  grow: false,
  paddingSize: 'none'
})(_templateObject(), function (props) {
  return props.theme.eui.euiSizeS;
}, function (props) {
  return props.theme.eui.euiSizeL;
}, function (props) {
  return props.theme.eui.euiZLevel1;
});
exports.Toolbar = Toolbar;