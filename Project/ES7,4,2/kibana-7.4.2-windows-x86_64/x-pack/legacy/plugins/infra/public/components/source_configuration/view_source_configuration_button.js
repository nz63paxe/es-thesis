"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ViewSourceConfigurationButton = exports.ViewSourceConfigurationButtonHrefBase = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var ViewSourceConfigurationButtonHrefBase;
exports.ViewSourceConfigurationButtonHrefBase = ViewSourceConfigurationButtonHrefBase;

(function (ViewSourceConfigurationButtonHrefBase) {
  ViewSourceConfigurationButtonHrefBase["infrastructure"] = "infrastructure";
  ViewSourceConfigurationButtonHrefBase["logs"] = "logs";
})(ViewSourceConfigurationButtonHrefBase || (exports.ViewSourceConfigurationButtonHrefBase = ViewSourceConfigurationButtonHrefBase = {}));

var ViewSourceConfigurationButton = function ViewSourceConfigurationButton(_ref) {
  var dataTestSubj = _ref['data-test-subj'],
      hrefBase = _ref.hrefBase,
      _children = _ref.children;
  var href = "/".concat(hrefBase, "/settings");
  return _react.default.createElement(_reactRouterDom.Route, {
    key: href,
    path: href,
    children: function children(_ref2) {
      var match = _ref2.match,
          history = _ref2.history;
      return _react.default.createElement(_eui.EuiButton, {
        "data-test-subj": dataTestSubj,
        color: "primary",
        onClick: function onClick() {
          return history.push(href);
        }
      }, _children);
    }
  });
};

exports.ViewSourceConfigurationButton = ViewSourceConfigurationButton;