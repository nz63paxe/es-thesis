"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsExplorerToolbar = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _types = require("../../../server/routes/metrics_explorer/types");

var _toolbar = require("../eui/toolbar");

var _kuery_bar = require("./kuery_bar");

var _metrics = require("./metrics");

var _group_by = require("./group_by");

var _aggregation = require("./aggregation");

var _chart_options = require("./chart_options");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MetricsExplorerToolbar = (0, _react.injectI18n)(function (_ref) {
  var timeRange = _ref.timeRange,
      derivedIndexPattern = _ref.derivedIndexPattern,
      options = _ref.options,
      _onTimeChange = _ref.onTimeChange,
      onRefresh = _ref.onRefresh,
      onGroupByChange = _ref.onGroupByChange,
      onFilterQuerySubmit = _ref.onFilterQuerySubmit,
      onMetricsChange = _ref.onMetricsChange,
      onAggregationChange = _ref.onAggregationChange,
      chartOptions = _ref.chartOptions,
      onChartOptionsChange = _ref.onChartOptionsChange;
  var isDefaultOptions = options.aggregation === _types.MetricsExplorerAggregation.avg && options.metrics.length === 0;
  return _react2.default.createElement(_toolbar.Toolbar, null, _react2.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: options.aggregation === _types.MetricsExplorerAggregation.count ? 2 : false
  }, _react2.default.createElement(_aggregation.MetricsExplorerAggregationPicker, {
    fullWidth: true,
    options: options,
    onChange: onAggregationChange
  })), options.aggregation !== _types.MetricsExplorerAggregation.count && _react2.default.createElement(_eui.EuiText, {
    size: "s",
    color: "subdued"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.infra.metricsExplorer.aggregationLabel",
    defaultMessage: "of"
  })), options.aggregation !== _types.MetricsExplorerAggregation.count && _react2.default.createElement(_eui.EuiFlexItem, {
    grow: 2
  }, _react2.default.createElement(_metrics.MetricsExplorerMetrics, {
    autoFocus: isDefaultOptions,
    fields: derivedIndexPattern.fields,
    options: options,
    onChange: onMetricsChange
  })), _react2.default.createElement(_eui.EuiText, {
    size: "s",
    color: "subdued"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.infra.metricsExplorer.groupByToolbarLabel",
    defaultMessage: "graph per"
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: 1
  }, _react2.default.createElement(_group_by.MetricsExplorerGroupBy, {
    onChange: onGroupByChange,
    fields: derivedIndexPattern.fields,
    options: options
  }))), _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_kuery_bar.MetricsExplorerKueryBar, {
    derivedIndexPattern: derivedIndexPattern,
    onSubmit: onFilterQuerySubmit,
    value: options.filterQuery
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_chart_options.MetricsExplorerChartOptions, {
    onChange: onChartOptionsChange,
    chartOptions: chartOptions
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false,
    style: {
      marginRight: 5
    }
  }, _react2.default.createElement(_eui.EuiSuperDatePicker, {
    start: timeRange.from,
    end: timeRange.to,
    onTimeChange: function onTimeChange(_ref2) {
      var start = _ref2.start,
          end = _ref2.end;
      return _onTimeChange(start, end);
    },
    onRefresh: onRefresh
  }))));
});
exports.MetricsExplorerToolbar = MetricsExplorerToolbar;