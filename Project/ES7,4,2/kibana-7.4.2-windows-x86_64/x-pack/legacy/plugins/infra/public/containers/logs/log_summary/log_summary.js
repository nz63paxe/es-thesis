"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLogSummary = void 0;

var _react = require("react");

var _apollo_context = require("../../../utils/apollo_context");

var _cancellable_effect = require("../../../utils/cancellable_effect");

var _log_summary = require("./log_summary.gql_query");

var _use_log_summary_buffer_interval = require("./use_log_summary_buffer_interval");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogSummary = function useLogSummary(sourceId, midpointTime, intervalSize, filterQuery) {
  var _useState = (0, _react.useState)({
    buckets: []
  }),
      _useState2 = _slicedToArray(_useState, 2),
      logSummaryBetween = _useState2[0],
      setLogSummaryBetween = _useState2[1];

  var apolloClient = (0, _apollo_context.useApolloClient)();

  var _useLogSummaryBufferI = (0, _use_log_summary_buffer_interval.useLogSummaryBufferInterval)(midpointTime, intervalSize),
      bufferStart = _useLogSummaryBufferI.start,
      bufferEnd = _useLogSummaryBufferI.end,
      bucketSize = _useLogSummaryBufferI.bucketSize;

  (0, _cancellable_effect.useCancellableEffect)(function (getIsCancelled) {
    if (!apolloClient || bufferStart === null || bufferEnd === null) {
      return;
    }

    apolloClient.query({
      fetchPolicy: 'no-cache',
      query: _log_summary.logSummaryQuery,
      variables: {
        filterQuery: filterQuery,
        sourceId: sourceId,
        start: bufferStart,
        end: bufferEnd,
        bucketSize: bucketSize
      }
    }).then(function (response) {
      if (!getIsCancelled()) {
        setLogSummaryBetween(response.data.source.logSummaryBetween);
      }
    });
  }, [apolloClient, sourceId, filterQuery, bufferStart, bufferEnd, bucketSize]);
  return {
    buckets: logSummaryBetween.buckets,
    start: bufferStart,
    end: bufferEnd
  };
};

exports.useLogSummary = useLogSummary;