"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteJobsRequestPayloadRT = exports.callCleanupMLResources = void 0;

var rt = _interopRequireWildcard(require("io-ts"));

var _kfetch = require("ui/kfetch");

var _log_analysis = require("../../../../../common/log_analysis");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var callCleanupMLResources =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(spaceId, sourceId) {
    var deleteJobsResponse;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _kfetch.kfetch)({
              method: 'POST',
              pathname: '/api/ml/jobs/delete_jobs',
              body: JSON.stringify(deleteJobsRequestPayloadRT.encode({
                jobIds: [(0, _log_analysis.getJobId)(spaceId, sourceId, 'log-entry-rate')]
              }))
            });

          case 2:
            deleteJobsResponse = _context.sent;
            return _context.abrupt("return", deleteJobsResponse);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function callCleanupMLResources(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.callCleanupMLResources = callCleanupMLResources;
var deleteJobsRequestPayloadRT = rt.type({
  jobIds: rt.array(rt.string)
});
exports.deleteJobsRequestPayloadRT = deleteJobsRequestPayloadRT;