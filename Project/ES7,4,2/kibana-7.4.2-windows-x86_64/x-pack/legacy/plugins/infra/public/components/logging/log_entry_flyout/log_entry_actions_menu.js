"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogEntryActionsMenu = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _url = _interopRequireDefault(require("url"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _use_visibility_state = require("../../../utils/use_visibility_state");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var UPTIME_FIELDS = ['container.id', 'host.ip', 'kubernetes.pod.uid'];

var LogEntryActionsMenu = function LogEntryActionsMenu(_ref) {
  var logItem = _ref.logItem;

  var _useVisibilityState = (0, _use_visibility_state.useVisibilityState)(false),
      hide = _useVisibilityState.hide,
      isVisible = _useVisibilityState.isVisible,
      show = _useVisibilityState.show;

  var uptimeLink = (0, _react2.useMemo)(function () {
    return getUptimeLink(logItem);
  }, [logItem]);
  var apmLink = (0, _react2.useMemo)(function () {
    return getAPMLink(logItem);
  }, [logItem]);
  var menuItems = (0, _react2.useMemo)(function () {
    return [_react2.default.createElement(_eui.EuiContextMenuItem, {
      "data-test-subj": "logEntryActionsMenuItem uptimeLogEntryActionsMenuItem",
      disabled: !uptimeLink,
      href: uptimeLink,
      icon: "uptimeApp",
      key: "uptimeLink"
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.infra.logEntryActionsMenu.uptimeActionLabel",
      defaultMessage: "View monitor status"
    })), _react2.default.createElement(_eui.EuiContextMenuItem, {
      "data-test-subj": "logEntryActionsMenuItem apmLogEntryActionsMenuItem",
      disabled: !apmLink,
      href: apmLink,
      icon: "apmApp",
      key: "apmLink"
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.infra.logEntryActionsMenu.apmActionLabel",
      defaultMessage: "View in APM"
    }))];
  }, [uptimeLink]);
  var hasMenuItems = (0, _react2.useMemo)(function () {
    return menuItems.length > 0;
  }, [menuItems]);
  return _react2.default.createElement(_eui.EuiPopover, {
    anchorPosition: "downRight",
    button: _react2.default.createElement(_eui.EuiButtonEmpty, {
      "data-test-subj": "logEntryActionsMenuButton",
      disabled: !hasMenuItems,
      iconSide: "right",
      iconType: "arrowDown",
      onClick: show
    }, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.infra.logEntryActionsMenu.buttonLabel",
      defaultMessage: "Actions"
    })),
    closePopover: hide,
    id: "logEntryActionsMenu",
    isOpen: isVisible,
    panelPaddingSize: "none"
  }, _react2.default.createElement(_eui.EuiContextMenuPanel, {
    items: menuItems
  }));
};

exports.LogEntryActionsMenu = LogEntryActionsMenu;

var getUptimeLink = function getUptimeLink(logItem) {
  var searchExpressions = logItem.fields.filter(function (_ref2) {
    var field = _ref2.field,
        value = _ref2.value;
    return value != null && UPTIME_FIELDS.includes(field);
  }).map(function (_ref3) {
    var field = _ref3.field,
        value = _ref3.value;
    return "".concat(field, ":").concat(value);
  });

  if (searchExpressions.length === 0) {
    return undefined;
  }

  return _url.default.format({
    pathname: _chrome.default.addBasePath('/app/uptime'),
    hash: "/?search=(".concat(searchExpressions.join(' OR '), ")")
  });
};

var getAPMLink = function getAPMLink(logItem) {
  var traceIdEntry = logItem.fields.find(function (_ref4) {
    var field = _ref4.field,
        value = _ref4.value;
    return value != null && field === 'trace.id';
  });

  if (!traceIdEntry) {
    return undefined;
  }

  var timestampField = logItem.fields.find(function (_ref5) {
    var field = _ref5.field;
    return field === '@timestamp';
  });
  var timestamp = timestampField ? timestampField.value : null;

  var _ref6 = timestamp ? function () {
    var from = new Date(timestamp);
    var to = new Date(timestamp);
    from.setMinutes(from.getMinutes() - 10);
    to.setMinutes(to.getMinutes() + 10);
    return {
      rangeFrom: from.toISOString(),
      rangeTo: to.toISOString()
    };
  }() : {
    rangeFrom: 'now-1y',
    rangeTo: 'now'
  },
      rangeFrom = _ref6.rangeFrom,
      rangeTo = _ref6.rangeTo;

  return _url.default.format({
    pathname: _chrome.default.addBasePath('/app/apm'),
    hash: "/traces?kuery=".concat(encodeURIComponent("trace.id:".concat(traceIdEntry.value)), "&rangeFrom=").concat(rangeFrom, "&rangeTo=").concat(rangeTo)
  });
};