"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisPageProviders = void 0;

var _react = _interopRequireWildcard(require("react"));

var _log_analysis = require("../../../containers/logs/log_analysis");

var _source = require("../../../containers/source");

var _use_kibana_space_id = require("../../../utils/use_kibana_space_id");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AnalysisPageProviders = function AnalysisPageProviders(_ref) {
  var children = _ref.children;

  var _useContext = (0, _react.useContext)(_source.Source.Context),
      sourceId = _useContext.sourceId,
      source = _useContext.source;

  var spaceId = (0, _use_kibana_space_id.useKibanaSpaceId)();
  return _react.default.createElement(_log_analysis.LogAnalysisJobs.Provider, {
    indexPattern: source ? source.configuration.logAlias : '',
    sourceId: sourceId,
    spaceId: spaceId,
    timeField: source ? source.configuration.fields.timestamp : ''
  }, children);
};

exports.AnalysisPageProviders = AnalysisPageProviders;