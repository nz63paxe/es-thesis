"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsTimeControls = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui = require("@elastic/eui");

var _moment = _interopRequireDefault(require("moment"));

var _react = _interopRequireDefault(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  max-width: 750px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var EuiSuperDatePickerAbsoluteFormat = 'YYYY-MM-DDTHH:mm:ss.sssZ';

var MetricsTimeControls =
/*#__PURE__*/
function (_React$Component) {
  _inherits(MetricsTimeControls, _React$Component);

  function MetricsTimeControls() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, MetricsTimeControls);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(MetricsTimeControls)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleTimeChange", function (_ref) {
      var start = _ref.start,
          end = _ref.end;

      var parsedStart = _datemath.default.parse(start);

      var parsedEnd = _datemath.default.parse(end, {
        roundUp: true
      });

      if (parsedStart && parsedEnd) {
        _this.props.onChangeTimeRange({
          from: parsedStart.valueOf(),
          to: parsedEnd.valueOf(),
          interval: '>=1m'
        });
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleRefreshChange", function (_ref2) {
      var isPaused = _ref2.isPaused,
          refreshInterval = _ref2.refreshInterval;

      if (isPaused) {
        _this.props.setAutoReload(false);
      } else {
        _this.props.setRefreshInterval(refreshInterval);

        _this.props.setAutoReload(true);
      }
    });

    return _this;
  }

  _createClass(MetricsTimeControls, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          currentTimeRange = _this$props.currentTimeRange,
          isLiveStreaming = _this$props.isLiveStreaming,
          refreshInterval = _this$props.refreshInterval;
      return _react.default.createElement(MetricsTimeControlsContainer, null, _react.default.createElement(_eui.EuiSuperDatePicker, {
        start: (0, _moment.default)(currentTimeRange.from).format(EuiSuperDatePickerAbsoluteFormat),
        end: (0, _moment.default)(currentTimeRange.to).format(EuiSuperDatePickerAbsoluteFormat),
        isPaused: !isLiveStreaming,
        refreshInterval: refreshInterval ? refreshInterval : 0,
        onTimeChange: this.handleTimeChange,
        onRefreshChange: this.handleRefreshChange
      }));
    }
  }]);

  return MetricsTimeControls;
}(_react.default.Component);

exports.MetricsTimeControls = MetricsTimeControls;

var MetricsTimeControlsContainer = _eui_styled_components.default.div(_templateObject());