"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useHTTPRequest = useHTTPRequest;

var _react = _interopRequireWildcard(require("react"));

var _kfetch = require("ui/kfetch");

var _notify = require("ui/notify");

var _i18n = require("@kbn/i18n");

var _target = require("@kbn/elastic-idx/target");

var _use_tracked_promise = require("../utils/use_tracked_promise");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function useHTTPRequest(pathname, method, body) {
  var decode = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : function (response) {
    return response;
  };
  return function () {
    var _useState = (0, _react.useState)(null),
        _useState2 = _slicedToArray(_useState, 2),
        response = _useState2[0],
        setResponse = _useState2[1];

    var _useState3 = (0, _react.useState)(null),
        _useState4 = _slicedToArray(_useState3, 2),
        error = _useState4[0],
        setError = _useState4[1];

    var _useTrackedPromise = (0, _use_tracked_promise.useTrackedPromise)({
      cancelPreviousOn: 'resolution',
      createPromise: function createPromise() {
        return (0, _kfetch.kfetch)({
          method: method,
          pathname: pathname,
          body: body
        });
      },
      onResolve: function onResolve(resp) {
        return setResponse(decode(resp));
      },
      onReject: function onReject(e) {
        var err = e;
        setError(err);

        _notify.toastNotifications.addWarning({
          title: _i18n.i18n.translate('xpack.infra.useHTTPRequest.error.title', {
            defaultMessage: "Error while fetching resource"
          }),
          text: _react.default.createElement("div", null, _react.default.createElement("h5", null, _i18n.i18n.translate('xpack.infra.useHTTPRequest.error.status', {
            defaultMessage: "Error"
          })), (0, _target.idx)(err.res, function (r) {
            return r.statusText;
          }), " (", (0, _target.idx)(err.res, function (r) {
            return r.status;
          }), ")", _react.default.createElement("h5", null, _i18n.i18n.translate('xpack.infra.useHTTPRequest.error.url', {
            defaultMessage: "URL"
          })), (0, _target.idx)(err.res, function (r) {
            return r.url;
          }))
        });
      }
    }, [pathname, body, method]),
        _useTrackedPromise2 = _slicedToArray(_useTrackedPromise, 2),
        request = _useTrackedPromise2[0],
        makeRequest = _useTrackedPromise2[1];

    var loading = (0, _react.useMemo)(function () {
      if (request.state === 'resolved' && response === null) {
        return true;
      }

      return request.state === 'pending';
    }, [request.state, response]);
    return {
      response: response,
      error: error,
      loading: loading,
      makeRequest: makeRequest
    };
  }();
}