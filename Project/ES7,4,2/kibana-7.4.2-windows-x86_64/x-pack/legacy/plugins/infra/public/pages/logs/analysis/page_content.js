"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisPageContent = void 0;

var _i18n = require("@kbn/i18n");

var _react = _interopRequireWildcard(require("react"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _loading_page = require("../../../components/loading_page");

var _log_analysis = require("../../../containers/logs/log_analysis");

var _source = require("../../../containers/source");

var _page_results_content = require("./page_results_content");

var _page_setup_content = require("./page_setup_content");

var _page_unavailable_content = require("./page_unavailable_content");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AnalysisPageContent = function AnalysisPageContent() {
  var _useContext = (0, _react.useContext)(_source.Source.Context),
      sourceId = _useContext.sourceId,
      source = _useContext.source;

  var _useContext2 = (0, _react.useContext)(_log_analysis.LogAnalysisCapabilities.Context),
      hasLogAnalysisCapabilites = _useContext2.hasLogAnalysisCapabilites;

  var spaceId = _chrome.default.getInjected('activeSpace').space.id;

  var _useContext3 = (0, _react.useContext)(_log_analysis.LogAnalysisJobs.Context),
      isSetupRequired = _useContext3.isSetupRequired,
      isLoadingSetupStatus = _useContext3.isLoadingSetupStatus,
      setupMlModule = _useContext3.setupMlModule,
      isSettingUpMlModule = _useContext3.isSettingUpMlModule,
      didSetupFail = _useContext3.didSetupFail,
      hasCompletedSetup = _useContext3.hasCompletedSetup;

  var _useLogAnalysisCleanu = (0, _log_analysis.useLogAnalysisCleanup)({
    sourceId: sourceId,
    spaceId: spaceId
  }),
      cleanupMLResources = _useLogAnalysisCleanu.cleanupMLResources,
      isCleaningUp = _useLogAnalysisCleanu.isCleaningUp;

  (0, _react.useEffect)(function () {
    if (didSetupFail) {
      cleanupMLResources();
    }
  }, [didSetupFail, cleanupMLResources]);

  if (!hasLogAnalysisCapabilites) {
    return _react.default.createElement(_page_unavailable_content.AnalysisUnavailableContent, null);
  } else if (isLoadingSetupStatus) {
    return _react.default.createElement(_loading_page.LoadingPage, {
      message: _i18n.i18n.translate('xpack.infra.logs.analysisPage.loadingMessage', {
        defaultMessage: 'Checking status of analysis jobs...'
      })
    });
  } else if (isSetupRequired) {
    return _react.default.createElement(_page_setup_content.AnalysisSetupContent, {
      didSetupFail: didSetupFail,
      isSettingUp: isSettingUpMlModule,
      setupMlModule: setupMlModule,
      isCleaningUpAFailedSetup: isCleaningUp,
      indexPattern: source ? source.configuration.logAlias : ''
    });
  } else {
    return _react.default.createElement(_page_results_content.AnalysisResultsContent, {
      sourceId: sourceId,
      isFirstUse: hasCompletedSetup
    });
  }
};

exports.AnalysisPageContent = AnalysisPageContent;