"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogColumnHeaders = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _source_configuration = require("../../../utils/source_configuration");

var _log_entry_column = require("./log_entry_column");

var _vertical_scroll_panel = require("./vertical_scroll_panel");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  color: ", ";\n  font-size: ", ";\n  font-weight: ", ";\n  line-height: ", ";\n  text-overflow: clip;\n  white-space: pre;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  border-bottom: ", ";\n  display: flex;\n  flex-direction: row;\n  height: 32px;\n  overflow: hidden;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: stretch;\n  display: flex;\n  flex-direction: row;\n  flex-wrap: nowrap;\n  justify-content: flex-start;\n  overflow: hidden;\n  padding-right: ", "px;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var LogColumnHeaders = (0, _react.injectI18n)(function (_ref) {
  var columnConfigurations = _ref.columnConfigurations,
      columnWidths = _ref.columnWidths,
      intl = _ref.intl;
  return _react2.default.createElement(LogColumnHeadersWrapper, null, columnConfigurations.map(function (columnConfiguration) {
    if ((0, _source_configuration.isTimestampLogColumnConfiguration)(columnConfiguration)) {
      return _react2.default.createElement(LogColumnHeader, {
        columnWidth: columnWidths[columnConfiguration.timestampColumn.id],
        "data-test-subj": "logColumnHeader timestampLogColumnHeader",
        key: columnConfiguration.timestampColumn.id
      }, "Timestamp");
    } else if ((0, _source_configuration.isMessageLogColumnConfiguration)(columnConfiguration)) {
      return _react2.default.createElement(LogColumnHeader, {
        columnWidth: columnWidths[columnConfiguration.messageColumn.id],
        "data-test-subj": "logColumnHeader messageLogColumnHeader",
        key: columnConfiguration.messageColumn.id
      }, "Message");
    } else if ((0, _source_configuration.isFieldLogColumnConfiguration)(columnConfiguration)) {
      return _react2.default.createElement(LogColumnHeader, {
        columnWidth: columnWidths[columnConfiguration.fieldColumn.id],
        "data-test-subj": "logColumnHeader fieldLogColumnHeader",
        key: columnConfiguration.fieldColumn.id
      }, columnConfiguration.fieldColumn.field);
    }
  }));
});
exports.LogColumnHeaders = LogColumnHeaders;

var LogColumnHeader = function LogColumnHeader(_ref2) {
  var children = _ref2.children,
      columnWidth = _ref2.columnWidth,
      dataTestSubj = _ref2['data-test-subj'];
  return _react2.default.createElement(LogColumnHeaderWrapper, _extends({
    "data-test-subj": dataTestSubj
  }, columnWidth), _react2.default.createElement(LogColumnHeaderContent, null, children));
};

var LogColumnHeadersWrapper = _eui_styled_components.default.div.attrs({
  role: 'row'
})(_templateObject(), _vertical_scroll_panel.ASSUMED_SCROLLBAR_WIDTH);

var LogColumnHeaderWrapper = _log_entry_column.LogEntryColumn.extend.attrs({
  role: 'columnheader'
})(_templateObject2(), function (props) {
  return props.theme.eui.euiBorderThick;
});

var LogColumnHeaderContent = _log_entry_column.LogEntryColumnContent.extend(_templateObject3(), function (props) {
  return props.theme.eui.euiTitleColor;
}, function (props) {
  return props.theme.eui.euiFontSizeS;
}, function (props) {
  return props.theme.eui.euiFontWeightSemiBold;
}, function (props) {
  return props.theme.eui.euiLineHeight;
});