"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogAnalysisCleanup = exports.useLogAnalysisCleanup = void 0;

var _constateLatest = _interopRequireDefault(require("constate-latest"));

var _react = require("react");

var _use_tracked_promise = require("../../../utils/use_tracked_promise");

var _ml_cleanup_everything = require("./api/ml_cleanup_everything");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogAnalysisCleanup = function useLogAnalysisCleanup(_ref) {
  var sourceId = _ref.sourceId,
      spaceId = _ref.spaceId;

  var _useTrackedPromise = (0, _use_tracked_promise.useTrackedPromise)({
    cancelPreviousOn: 'resolution',
    createPromise: function () {
      var _createPromise = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return (0, _ml_cleanup_everything.callCleanupMLResources)(spaceId, sourceId);

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function createPromise() {
        return _createPromise.apply(this, arguments);
      }

      return createPromise;
    }()
  }, [spaceId, sourceId]),
      _useTrackedPromise2 = _slicedToArray(_useTrackedPromise, 2),
      cleanupMLResourcesRequest = _useTrackedPromise2[0],
      cleanupMLResources = _useTrackedPromise2[1];

  var isCleaningUp = (0, _react.useMemo)(function () {
    return cleanupMLResourcesRequest.state === 'pending';
  }, [cleanupMLResourcesRequest.state]);
  return {
    cleanupMLResources: cleanupMLResources,
    isCleaningUp: isCleaningUp
  };
};

exports.useLogAnalysisCleanup = useLogAnalysisCleanup;
var LogAnalysisCleanup = (0, _constateLatest.default)(useLogAnalysisCleanup);
exports.LogAnalysisCleanup = LogAnalysisCleanup;