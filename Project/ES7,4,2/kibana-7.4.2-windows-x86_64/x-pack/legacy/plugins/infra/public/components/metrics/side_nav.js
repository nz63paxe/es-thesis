"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsSideNav = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../common/eui_styled_components"));

var _class, _temp;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  position: fixed;\n  z-index: 1;\n  height: 88vh;\n  padding-left: 16px;\n  margin-left: -16px;\n  overflow-y: auto;\n  overflow-x: hidden;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MetricsSideNav = (0, _react.injectI18n)((_temp = _class =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(_class, _React$PureComponent);

  function _class() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, _class);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(_class)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      isOpenOnMobile: false
    });

    _defineProperty(_assertThisInitialized(_this), "toggleOpenOnMobile", function () {
      _this.setState({
        isOpenOnMobile: !_this.state.isOpenOnMobile
      });
    });

    return _this;
  }

  _createClass(_class, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var content;
      var mobileContent;

      if (!this.props.loading) {
        var entries = this.props.layouts.map(function (item) {
          return {
            name: item.label,
            id: item.id,
            items: item.sections.map(function (section) {
              return {
                id: section.id,
                name: section.label,
                onClick: _this2.props.handleClick(section)
              };
            })
          };
        });
        content = _react2.default.createElement(_eui.EuiSideNav, {
          items: entries
        });
        mobileContent = _react2.default.createElement(_eui.EuiSideNav, {
          items: entries,
          mobileTitle: this.props.nodeName,
          toggleOpenOnMobile: this.toggleOpenOnMobile,
          isOpenOnMobile: this.state.isOpenOnMobile
        });
      }

      return _react2.default.createElement(_eui.EuiPageSideBar, null, _react2.default.createElement(_eui.EuiHideFor, {
        sizes: ['xs', 's']
      }, _react2.default.createElement(SideNavContainer, null, content)), _react2.default.createElement(_eui.EuiShowFor, {
        sizes: ['xs', 's']
      }, mobileContent));
    }
  }]);

  return _class;
}(_react2.default.PureComponent), _defineProperty(_class, "displayName", 'MetricsSideNav'), _temp));
exports.MetricsSideNav = MetricsSideNav;

var SideNavContainer = _eui_styled_components.default.div(_templateObject());