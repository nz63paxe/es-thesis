"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricExplorerSeriesChart = void 0;

var _react = _interopRequireDefault(require("react"));

var _charts = require("@elastic/charts");

var _color_palette = require("../../../common/color_palette");

var _create_metric_label = require("./helpers/create_metric_label");

var _use_metrics_explorer_options = require("../../containers/metrics_explorer/use_metrics_explorer_options");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MetricExplorerSeriesChart = function MetricExplorerSeriesChart(_ref) {
  var metric = _ref.metric,
      id = _ref.id,
      series = _ref.series,
      type = _ref.type,
      stack = _ref.stack;
  var color = metric.color && (0, _color_palette.colorTransformer)(metric.color) || (0, _color_palette.colorTransformer)(_color_palette.MetricsExplorerColor.color0);
  var yAccessor = "metric_".concat(id);
  var specId = (0, _charts.getSpecId)(yAccessor);
  var colors = {
    colorValues: [],
    specId: specId
  };
  var customColors = new Map();
  customColors.set(colors, color);
  var chartId = "series-".concat(series.id, "-").concat(yAccessor);
  var seriesAreaStyle = {
    line: {
      strokeWidth: 2,
      visible: true
    },
    area: {
      opacity: 0.5,
      visible: type === _use_metrics_explorer_options.MetricsExplorerChartType.area
    },
    point: {
      visible: false,
      radius: 0.2,
      strokeWidth: 2,
      opacity: 1
    }
  };
  return _react.default.createElement(_charts.AreaSeries, {
    key: chartId,
    id: specId,
    name: (0, _create_metric_label.createMetricLabel)(metric),
    xScaleType: _charts.ScaleType.Time,
    yScaleType: _charts.ScaleType.Linear,
    xAccessor: "timestamp",
    yAccessors: [yAccessor],
    data: series.rows,
    stackAccessors: stack ? ['timestamp'] : void 0,
    areaSeriesStyle: seriesAreaStyle,
    customSeriesColors: customColors
  });
};

exports.MetricExplorerSeriesChart = MetricExplorerSeriesChart;