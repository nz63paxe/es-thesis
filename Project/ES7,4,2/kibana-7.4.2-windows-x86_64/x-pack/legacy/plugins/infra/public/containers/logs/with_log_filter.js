"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceLogFilterInQueryString = exports.WithLogFilterUrlState = exports.WithLogFilter = exports.withLogFilter = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _store = require("../../store");

var _kuery = require("../../utils/kuery");

var _typed_react = require("../../utils/typed_react");

var _typed_redux = require("../../utils/typed_redux");

var _url_state = require("../../utils/url_state");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var withLogFilter = (0, _reactRedux.connect)(function (state) {
  return {
    filterQuery: _store.logFilterSelectors.selectLogFilterQuery(state),
    serializedFilterQuery: _store.logFilterSelectors.selectLogFilterQueryAsJson(state),
    filterQueryDraft: _store.logFilterSelectors.selectLogFilterQueryDraft(state),
    isFilterQueryDraftValid: _store.logFilterSelectors.selectIsLogFilterQueryDraftValid(state)
  };
}, function (dispatch, ownProps) {
  return (0, _typed_redux.bindPlainActionCreators)({
    applyFilterQuery: function applyFilterQuery(query) {
      return _store.logFilterActions.applyLogFilterQuery({
        query: query,
        serializedQuery: (0, _kuery.convertKueryToElasticSearchQuery)(query.expression, ownProps.indexPattern)
      });
    },
    applyFilterQueryFromKueryExpression: function applyFilterQueryFromKueryExpression(expression) {
      return _store.logFilterActions.applyLogFilterQuery({
        query: {
          kind: 'kuery',
          expression: expression
        },
        serializedQuery: (0, _kuery.convertKueryToElasticSearchQuery)(expression, ownProps.indexPattern)
      });
    },
    setFilterQueryDraft: _store.logFilterActions.setLogFilterQueryDraft,
    setFilterQueryDraftFromKueryExpression: function setFilterQueryDraftFromKueryExpression(expression) {
      return _store.logFilterActions.setLogFilterQueryDraft({
        kind: 'kuery',
        expression: expression
      });
    }
  })(dispatch);
});
exports.withLogFilter = withLogFilter;
var WithLogFilter = (0, _typed_react.asChildFunctionRenderer)(withLogFilter);
/**
 * Url State
 */

exports.WithLogFilter = WithLogFilter;

var WithLogFilterUrlState = function WithLogFilterUrlState(_ref) {
  var indexPattern = _ref.indexPattern;
  return _react.default.createElement(WithLogFilter, {
    indexPattern: indexPattern
  }, function (_ref2) {
    var applyFilterQuery = _ref2.applyFilterQuery,
        filterQuery = _ref2.filterQuery;
    return _react.default.createElement(_url_state.UrlStateContainer, {
      urlState: filterQuery,
      urlStateKey: "logFilter",
      mapToUrlState: mapToFilterQuery,
      onChange: function onChange(urlState) {
        if (urlState) {
          applyFilterQuery(urlState);
        }
      },
      onInitialize: function onInitialize(urlState) {
        if (urlState) {
          applyFilterQuery(urlState);
        }
      }
    });
  });
};

exports.WithLogFilterUrlState = WithLogFilterUrlState;

var mapToFilterQuery = function mapToFilterQuery(value) {
  return value && value.kind === 'kuery' && typeof value.expression === 'string' ? {
    kind: value.kind,
    expression: value.expression
  } : undefined;
};

var replaceLogFilterInQueryString = function replaceLogFilterInQueryString(expression) {
  return (0, _url_state.replaceStateKeyInQueryString)('logFilter', {
    kind: 'kuery',
    expression: expression
  });
};

exports.replaceLogFilterInQueryString = replaceLogFilterInQueryString;