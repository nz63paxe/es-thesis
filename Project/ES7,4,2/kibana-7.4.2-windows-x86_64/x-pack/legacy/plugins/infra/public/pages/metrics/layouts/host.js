"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.hostLayoutCreator = void 0;

var _i18n = require("@kbn/i18n");

var _types = require("../../../graphql/types");

var _lib = require("../../../lib/lib");

var _nginx = require("./nginx");

var _aws = require("./aws");

var _types2 = require("./types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var hostLayoutCreator = function hostLayoutCreator(theme) {
  return [{
    id: 'hostOverview',
    label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.layoutLabel', {
      defaultMessage: 'Host'
    }),
    sections: [{
      id: _types.InfraMetric.hostSystemOverview,
      linkToId: 'hostOverview',
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.sectionLabel', {
        defaultMessage: 'Overview'
      }),
      requires: ['system.cpu', 'system.load', 'system.memory', 'system.network'],
      type: _types2.InfraMetricLayoutSectionType.gauges,
      visConfig: {
        seriesOverrides: {
          cpu: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.cpuUsageSeriesLabel', {
              defaultMessage: 'CPU Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          load: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.loadSeriesLabel', {
              defaultMessage: 'Load (5m)'
            }),
            color: theme.eui.euiColorFullShade
          },
          memory: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.memoryCapacitySeriesLabel', {
              defaultMessage: 'Memory Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          rx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.inboundRXSeriesLabel', {
              defaultMessage: 'Inbound (RX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          },
          tx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.overviewSection.outboundTXSeriesLabel', {
              defaultMessage: 'Outbound (TX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostCpuUsage,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.cpuUsageSection.sectionLabel', {
        defaultMessage: 'CPU Usage'
      }),
      requires: ['system.cpu'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        stacked: true,
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.percent,
        seriesOverrides: {
          user: {
            color: theme.eui.euiColorVis0
          },
          system: {
            color: theme.eui.euiColorVis2
          },
          steal: {
            color: theme.eui.euiColorVis9
          },
          irq: {
            color: theme.eui.euiColorVis4
          },
          softirq: {
            color: theme.eui.euiColorVis6
          },
          iowait: {
            color: theme.eui.euiColorVis7
          },
          nice: {
            color: theme.eui.euiColorVis5
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostLoad,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.loadSection.sectionLabel', {
        defaultMessage: 'Load'
      }),
      requires: ['system.load'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        seriesOverrides: {
          load_1m: {
            color: theme.eui.euiColorVis0,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.loadSection.oneMinuteSeriesLabel', {
              defaultMessage: '1m'
            })
          },
          load_5m: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.loadSection.fiveMinuteSeriesLabel', {
              defaultMessage: '5m'
            })
          },
          load_15m: {
            color: theme.eui.euiColorVis3,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.loadSection.fifteenMinuteSeriesLabel', {
              defaultMessage: '15m'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostMemoryUsage,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.memoryUsageSection.sectionLabel', {
        defaultMessage: 'Memory Usage'
      }),
      requires: ['system.memory'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        stacked: true,
        formatter: _lib.InfraFormatterType.bytes,
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          used: {
            color: theme.eui.euiColorVis2
          },
          free: {
            color: theme.eui.euiColorVis0
          },
          cache: {
            color: theme.eui.euiColorVis1
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostNetworkTraffic,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.networkTrafficSection.sectionLabel', {
        defaultMessage: 'Network Traffic'
      }),
      requires: ['system.network'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bits,
        formatterTemplate: '{{value}}/s',
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          rx: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.networkTrafficSection.networkRxRateSeriesLabel', {
              defaultMessage: 'in'
            })
          },
          tx: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.hostMetricsLayout.networkTrafficSection.networkTxRateSeriesLabel', {
              defaultMessage: 'out'
            })
          }
        }
      }
    }]
  }, {
    id: 'k8sOverview',
    label: 'Kubernetes',
    sections: [{
      id: _types.InfraMetric.hostK8sOverview,
      linkToId: 'k8sOverview',
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.sectionLabel', {
        defaultMessage: 'Overview'
      }),
      requires: ['kubernetes.node'],
      type: _types2.InfraMetricLayoutSectionType.gauges,
      visConfig: {
        seriesOverrides: {
          cpucap: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.cpuUsageSeriesLabel', {
              defaultMessage: 'CPU Capacity'
            }),
            color: 'secondary',
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          load: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.loadSeriesLabel', {
              defaultMessage: 'Load (5m)'
            }),
            color: 'secondary'
          },
          memorycap: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.memoryUsageSeriesLabel', {
              defaultMessage: 'Memory Capacity'
            }),
            color: 'secondary',
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          podcap: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.podCapacitySeriesLabel', {
              defaultMessage: 'Pod Capacity'
            }),
            color: 'secondary',
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          diskcap: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.overviewSection.diskCapacitySeriesLabel', {
              defaultMessage: 'Disk Capacity'
            }),
            color: 'secondary',
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostK8sCpuCap,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.nodeCpuCapacitySection.sectionLabel', {
        defaultMessage: 'Node CPU Capacity'
      }),
      requires: ['kubernetes.node'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.abbreviatedNumber,
        seriesOverrides: {
          capacity: {
            color: theme.eui.euiColorVis2
          },
          used: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostK8sMemoryCap,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.nodeMemoryCapacitySection.sectionLabel', {
        defaultMessage: 'Node Memory Capacity'
      }),
      requires: ['kubernetes.node'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bytes,
        seriesOverrides: {
          capacity: {
            color: theme.eui.euiColorVis2
          },
          used: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostK8sDiskCap,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.nodeDiskCapacitySection.sectionLabel', {
        defaultMessage: 'Node Disk Capacity'
      }),
      requires: ['kubernetes.node'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bytes,
        seriesOverrides: {
          capacity: {
            color: theme.eui.euiColorVis2
          },
          used: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.hostK8sPodCap,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.kubernetesMetricsLayout.nodePodCapacitySection.sectionLabel', {
        defaultMessage: 'Node Pod Capacity'
      }),
      requires: ['kubernetes.node'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.number,
        seriesOverrides: {
          capacity: {
            color: theme.eui.euiColorVis2
          },
          used: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }]
  }].concat(_toConsumableArray((0, _nginx.nginxLayoutCreator)(theme)), _toConsumableArray((0, _aws.awsLayoutCreator)(theme)));
};

exports.hostLayoutCreator = hostLayoutCreator;