"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PageRouter = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _react2 = require("ui/capabilities/react");

var _ = require("./pages/404");

var _infrastructure = require("./pages/infrastructure");

var _link_to = require("./pages/link_to");

var _logs = require("./pages/logs");

var _metrics = require("./pages/metrics");

var _redirect_with_query_params = require("./utils/redirect_with_query_params");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var PageRouterComponent = function PageRouterComponent(_ref) {
  var history = _ref.history,
      uiCapabilities = _ref.uiCapabilities;
  return _react.default.createElement(_reactRouterDom.Router, {
    history: history
  }, _react.default.createElement(_reactRouterDom.Switch, null, uiCapabilities.infrastructure.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/",
    exact: true,
    to: "/infrastructure/inventory"
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/infrastructure",
    exact: true,
    to: "/infrastructure/inventory"
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/infrastructure/snapshot",
    exact: true,
    to: "/infrastructure/inventory"
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/home",
    exact: true,
    to: "/infrastructure/inventory"
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_reactRouterDom.Route, {
    path: "/infrastructure/metrics/:type/:node",
    component: _metrics.MetricDetail
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/metrics",
    to: "/infrastructure/metrics"
  }), uiCapabilities.logs.show && _react.default.createElement(_redirect_with_query_params.RedirectWithQueryParams, {
    from: "/logs",
    exact: true,
    to: "/logs/stream"
  }), uiCapabilities.logs.show && _react.default.createElement(_reactRouterDom.Route, {
    path: "/logs",
    component: _logs.LogsPage
  }), uiCapabilities.infrastructure.show && _react.default.createElement(_reactRouterDom.Route, {
    path: "/infrastructure",
    component: _infrastructure.InfrastructurePage
  }), _react.default.createElement(_reactRouterDom.Route, {
    path: "/link-to",
    component: _link_to.LinkToPage
  }), _react.default.createElement(_reactRouterDom.Route, {
    component: _.NotFoundPage
  })));
};

var PageRouter = (0, _react2.injectUICapabilities)(PageRouterComponent);
exports.PageRouter = PageRouter;