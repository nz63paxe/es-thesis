"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ChartView = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _moment = _interopRequireDefault(require("moment"));

var _charts = require("@elastic/charts");

var _eui = require("@elastic/eui");

var _chart_helpers = require("../../chart_helpers");

var _log_entry_rate = require("../../../../../containers/logs/log_analysis/log_analysis_graph_data/log_entry_rate");

var _use_kibana_ui_setting = require("../../../../../utils/use_kibana_ui_setting");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var areaSeriesColour = 'rgb(224, 237, 255)';
var lineSeriesColour = 'rgb(49, 133, 252)';

var ChartView = function ChartView(_ref) {
  var data = _ref.data,
      timeRange = _ref.timeRange;

  var showModelBoundsLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionModelBoundsCheckboxLabel', {
    defaultMessage: 'Show model bounds'
  });

  var _useLogEntryRateGraph = (0, _log_entry_rate.useLogEntryRateGraphData)({
    data: data
  }),
      areaSeries = _useLogEntryRateGraph.areaSeries,
      lineSeries = _useLogEntryRateGraph.lineSeries,
      anomalySeries = _useLogEntryRateGraph.anomalySeries;

  var dateFormatter = (0, _react.useMemo)(function () {
    return lineSeries.length > 0 ? (0, _charts.niceTimeFormatter)([timeRange.startTime, timeRange.endTime]) : function (value) {
      return "".concat(value);
    };
  }, [lineSeries, timeRange]);
  var areaSpecId = (0, _charts.getSpecId)('modelBounds');
  var lineSpecId = (0, _charts.getSpecId)('averageValues');
  var anomalySpecId = (0, _charts.getSpecId)('anomalies');

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)('dateFormat'),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      dateFormat = _useKibanaUiSetting2[0];

  var tooltipProps = {
    headerFormatter: (0, _react.useCallback)(function (tooltipData) {
      return (0, _moment.default)(tooltipData.value).format(dateFormat || 'Y-MM-DD HH:mm:ss.SSS');
    }, [dateFormat])
  };

  var _useState = (0, _react.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      isShowingModelBounds = _useState2[0],
      setIsShowingModelBounds = _useState2[1];

  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "flexEnd"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiCheckbox, {
    id: 'showModelBoundsCheckbox',
    label: showModelBoundsLabel,
    "aria-label": showModelBoundsLabel,
    checked: isShowingModelBounds,
    onChange: function onChange(e) {
      setIsShowingModelBounds(e.target.checked);
    }
  }))), _react.default.createElement("div", {
    style: {
      height: 400,
      width: '100%'
    }
  }, _react.default.createElement(_charts.Chart, {
    className: "log-entry-rate-chart"
  }, _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)('timestamp'),
    title: _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionXaxisTitle', {
      defaultMessage: 'Time'
    }),
    position: "bottom",
    showOverlappingTicks: true,
    tickFormat: dateFormatter
  }), _react.default.createElement(_charts.Axis, {
    id: (0, _charts.getAxisId)('values'),
    title: _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionYaxisTitle', {
      defaultMessage: 'Log entries per 15 minutes'
    }),
    position: "left",
    tickFormat: function tickFormat(value) {
      return Number(value).toFixed(0);
    }
  }), isShowingModelBounds ? _react.default.createElement(_charts.AreaSeries, {
    id: areaSpecId,
    name: _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionAreaSeriesName', {
      defaultMessage: 'Expected'
    }),
    xScaleType: "time",
    yScaleType: "linear",
    xAccessor: "x",
    yAccessors: ['max'],
    y0Accessors: ['min'],
    data: areaSeries,
    yScaleToDataExtent: true,
    curve: 2,
    areaSeriesStyle: !(0, _chart_helpers.isDarkMode)() ? {
      line: {
        stroke: areaSeriesColour
      },
      area: {
        fill: areaSeriesColour,
        visible: true,
        opacity: 0.8
      }
    } : undefined,
    customSeriesColors: !(0, _chart_helpers.isDarkMode)() ? (0, _chart_helpers.getColorsMap)(areaSeriesColour, areaSpecId) : undefined
  }) : null, _react.default.createElement(_charts.LineSeries, {
    id: lineSpecId,
    name: _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionLineSeriesName', {
      defaultMessage: 'Log entries per 15 minutes (avg)'
    }),
    xScaleType: "time",
    yScaleType: "linear",
    xAccessor: 0,
    yAccessors: [1],
    data: lineSeries,
    yScaleToDataExtent: true,
    curve: 2,
    lineSeriesStyle: !(0, _chart_helpers.isDarkMode)() ? {
      line: {
        stroke: lineSeriesColour
      },
      point: {
        radius: 2,
        fill: lineSeriesColour
      }
    } : undefined,
    customSeriesColors: !(0, _chart_helpers.isDarkMode)() ? (0, _chart_helpers.getColorsMap)(lineSeriesColour, lineSpecId) : undefined
  }), _react.default.createElement(_charts.LineSeries, {
    id: anomalySpecId,
    name: _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSectionAnomalySeriesName', {
      defaultMessage: 'Anomalies'
    }),
    xScaleType: "time",
    yScaleType: "linear",
    xAccessor: 0,
    yAccessors: [1],
    data: anomalySeries,
    yScaleToDataExtent: true,
    curve: 2,
    lineSeriesStyle: !(0, _chart_helpers.isDarkMode)() ? {
      line: {
        stroke: 'red',
        opacity: 0
      },
      point: {
        radius: 3,
        fill: 'red'
      }
    } : undefined,
    customSeriesColors: !(0, _chart_helpers.isDarkMode)() ? (0, _chart_helpers.getColorsMap)('red', anomalySpecId) : undefined
  }), _react.default.createElement(_charts.Settings, {
    tooltip: tooltipProps,
    theme: (0, _chart_helpers.getChartTheme)(),
    xDomain: {
      min: timeRange.startTime,
      max: timeRange.endTime
    }
  }))));
};

exports.ChartView = ChartView;