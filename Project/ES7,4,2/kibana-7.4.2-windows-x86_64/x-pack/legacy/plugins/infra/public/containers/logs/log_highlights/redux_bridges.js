"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogHighlightsBridge = exports.LogHighlightsFilterQueryBridge = exports.LogHighlightsPositionBridge = exports.LogHighlightsStreamItemsBridge = void 0;

var _react = _interopRequireWildcard(require("react"));

var _with_log_filter = require("../with_log_filter");

var _with_stream_items = require("../with_stream_items");

var _with_log_position = require("../with_log_position");

var _log_highlights = require("./log_highlights");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// Bridges Redux container state with Hooks state. Once state is moved fully from
// Redux to Hooks this can be removed.
var LogHighlightsStreamItemsBridge = (0, _with_stream_items.withStreamItems)(function (_ref) {
  var entriesStart = _ref.entriesStart,
      entriesEnd = _ref.entriesEnd;

  var _useContext = (0, _react.useContext)(_log_highlights.LogHighlightsState.Context),
      setStartKey = _useContext.setStartKey,
      setEndKey = _useContext.setEndKey;

  (0, _react.useEffect)(function () {
    setStartKey(entriesStart);
    setEndKey(entriesEnd);
  }, [entriesStart, entriesEnd]);
  return null;
});
exports.LogHighlightsStreamItemsBridge = LogHighlightsStreamItemsBridge;
var LogHighlightsPositionBridge = (0, _with_log_position.withLogPosition)(function (_ref2) {
  var visibleMidpoint = _ref2.visibleMidpoint,
      jumpToTargetPosition = _ref2.jumpToTargetPosition;

  var _useContext2 = (0, _react.useContext)(_log_highlights.LogHighlightsState.Context),
      setJumpToTarget = _useContext2.setJumpToTarget,
      setVisibleMidpoint = _useContext2.setVisibleMidpoint;

  (0, _react.useEffect)(function () {
    setVisibleMidpoint(visibleMidpoint);
  }, [visibleMidpoint]);
  (0, _react.useEffect)(function () {
    setJumpToTarget(function () {
      return jumpToTargetPosition;
    });
  }, [jumpToTargetPosition]);
  return null;
});
exports.LogHighlightsPositionBridge = LogHighlightsPositionBridge;
var LogHighlightsFilterQueryBridge = (0, _with_log_filter.withLogFilter)(function (_ref3) {
  var serializedFilterQuery = _ref3.serializedFilterQuery;

  var _useContext3 = (0, _react.useContext)(_log_highlights.LogHighlightsState.Context),
      setFilterQuery = _useContext3.setFilterQuery;

  (0, _react.useEffect)(function () {
    setFilterQuery(serializedFilterQuery);
  }, [serializedFilterQuery]);
  return null;
});
exports.LogHighlightsFilterQueryBridge = LogHighlightsFilterQueryBridge;

var LogHighlightsBridge = function LogHighlightsBridge(_ref4) {
  var indexPattern = _ref4.indexPattern;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(LogHighlightsStreamItemsBridge, null), _react.default.createElement(LogHighlightsPositionBridge, null), _react.default.createElement(LogHighlightsFilterQueryBridge, {
    indexPattern: indexPattern
  }));
};

exports.LogHighlightsBridge = LogHighlightsBridge;