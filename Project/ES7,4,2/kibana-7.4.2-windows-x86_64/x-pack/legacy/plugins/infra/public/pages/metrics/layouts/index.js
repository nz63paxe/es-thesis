"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.layoutCreators = void 0;

var _container = require("./container");

var _host = require("./host");

var _pod = require("./pod");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var layoutCreators = {
  host: _host.hostLayoutCreator,
  pod: _pod.podLayoutCreator,
  container: _container.containerLayoutCreator
};
exports.layoutCreators = layoutCreators;