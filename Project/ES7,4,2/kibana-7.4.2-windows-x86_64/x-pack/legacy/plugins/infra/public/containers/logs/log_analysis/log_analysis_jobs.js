"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogAnalysisJobs = exports.useLogAnalysisJobs = void 0;

var _constateLatest = _interopRequireDefault(require("constate-latest"));

var _react = require("react");

var _log_analysis = require("../../../../common/log_analysis");

var _use_tracked_promise = require("../../../utils/use_tracked_promise");

var _ml_get_jobs_summary_api = require("./api/ml_get_jobs_summary_api");

var _ml_setup_module_api = require("./api/ml_setup_module_api");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogAnalysisJobs = function useLogAnalysisJobs(_ref) {
  var indexPattern = _ref.indexPattern,
      sourceId = _ref.sourceId,
      spaceId = _ref.spaceId,
      timeField = _ref.timeField;

  var _useState = (0, _react.useState)({
    'log-entry-rate': 'unknown'
  }),
      _useState2 = _slicedToArray(_useState, 2),
      jobStatus = _useState2[0],
      setJobStatus = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      hasCompletedSetup = _useState4[0],
      setHasCompletedSetup = _useState4[1];

  var _useTrackedPromise = (0, _use_tracked_promise.useTrackedPromise)({
    cancelPreviousOn: 'resolution',
    createPromise: function () {
      var _createPromise = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(start, end) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                setJobStatus(function (currentJobStatus) {
                  return _objectSpread({}, currentJobStatus, {
                    'log-entry-rate': 'initializing'
                  });
                });
                _context.next = 3;
                return (0, _ml_setup_module_api.callSetupMlModuleAPI)(start, end, spaceId, sourceId, indexPattern, timeField, _log_analysis.bucketSpan);

              case 3:
                return _context.abrupt("return", _context.sent);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function createPromise(_x, _x2) {
        return _createPromise.apply(this, arguments);
      }

      return createPromise;
    }(),
    onResolve: function onResolve(_ref2) {
      var datafeeds = _ref2.datafeeds,
          jobs = _ref2.jobs;
      setJobStatus(function (currentJobStatus) {
        return _objectSpread({}, currentJobStatus, {
          'log-entry-rate': hasSuccessfullyCreatedJob((0, _log_analysis.getJobId)(spaceId, sourceId, 'log-entry-rate'))(jobs) && hasSuccessfullyStartedDatafeed((0, _log_analysis.getDatafeedId)(spaceId, sourceId, 'log-entry-rate'))(datafeeds) ? 'started' : 'failed'
        });
      });
      setHasCompletedSetup(true);
    },
    onReject: function onReject() {
      setJobStatus(function (currentJobStatus) {
        return _objectSpread({}, currentJobStatus, {
          'log-entry-rate': 'failed'
        });
      });
    }
  }, [indexPattern, spaceId, sourceId]),
      _useTrackedPromise2 = _slicedToArray(_useTrackedPromise, 2),
      setupMlModuleRequest = _useTrackedPromise2[0],
      setupMlModule = _useTrackedPromise2[1];

  var _useTrackedPromise3 = (0, _use_tracked_promise.useTrackedPromise)({
    cancelPreviousOn: 'resolution',
    createPromise: function () {
      var _createPromise2 = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2() {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return (0, _ml_get_jobs_summary_api.callJobsSummaryAPI)(spaceId, sourceId);

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function createPromise() {
        return _createPromise2.apply(this, arguments);
      }

      return createPromise;
    }(),
    onResolve: function onResolve(response) {
      setJobStatus(function (currentJobStatus) {
        return _objectSpread({}, currentJobStatus, {
          'log-entry-rate': getJobStatus((0, _log_analysis.getJobId)(spaceId, sourceId, 'log-entry-rate'))(response)
        });
      });
    },
    onReject: function onReject(err) {
      setJobStatus(function (currentJobStatus) {
        return _objectSpread({}, currentJobStatus, {
          'log-entry-rate': 'unknown'
        });
      });
    }
  }, [indexPattern, spaceId, sourceId]),
      _useTrackedPromise4 = _slicedToArray(_useTrackedPromise3, 2),
      fetchJobStatusRequest = _useTrackedPromise4[0],
      fetchJobStatus = _useTrackedPromise4[1];

  (0, _react.useEffect)(function () {
    fetchJobStatus();
  }, []);
  var isSetupRequired = (0, _react.useMemo)(function () {
    return !Object.values(jobStatus).every(function (state) {
      return ['started', 'finished'].includes(state);
    });
  }, [jobStatus]);
  var isLoadingSetupStatus = (0, _react.useMemo)(function () {
    return fetchJobStatusRequest.state === 'pending';
  }, [fetchJobStatusRequest.state]);
  var isSettingUpMlModule = (0, _react.useMemo)(function () {
    return setupMlModuleRequest.state === 'pending';
  }, [setupMlModuleRequest.state]);
  var didSetupFail = (0, _react.useMemo)(function () {
    var jobStates = Object.values(jobStatus);
    return jobStates.filter(function (state) {
      return state === 'failed';
    }).length > 0;
  }, [jobStatus]);
  return {
    jobStatus: jobStatus,
    isSetupRequired: isSetupRequired,
    isLoadingSetupStatus: isLoadingSetupStatus,
    setupMlModule: setupMlModule,
    setupMlModuleRequest: setupMlModuleRequest,
    isSettingUpMlModule: isSettingUpMlModule,
    didSetupFail: didSetupFail,
    hasCompletedSetup: hasCompletedSetup
  };
};

exports.useLogAnalysisJobs = useLogAnalysisJobs;
var LogAnalysisJobs = (0, _constateLatest.default)(useLogAnalysisJobs);
exports.LogAnalysisJobs = LogAnalysisJobs;

var hasSuccessfullyCreatedJob = function hasSuccessfullyCreatedJob(jobId) {
  return function (jobSetupResponses) {
    return jobSetupResponses.filter(function (jobSetupResponse) {
      return jobSetupResponse.id === jobId && jobSetupResponse.success && !jobSetupResponse.error;
    }).length > 0;
  };
};

var hasSuccessfullyStartedDatafeed = function hasSuccessfullyStartedDatafeed(datafeedId) {
  return function (datafeedSetupResponses) {
    return datafeedSetupResponses.filter(function (datafeedSetupResponse) {
      return datafeedSetupResponse.id === datafeedId && datafeedSetupResponse.success && datafeedSetupResponse.started && !datafeedSetupResponse.error;
    }).length > 0;
  };
};

var getJobStatus = function getJobStatus(jobId) {
  return function (jobSummaries) {
    return jobSummaries.filter(function (jobSummary) {
      return jobSummary.id === jobId;
    }).map(function (jobSummary) {
      if (jobSummary.jobState === 'failed') {
        return 'failed';
      } else if (jobSummary.jobState === 'closed' && jobSummary.datafeedState === 'stopped' && jobSummary.fullJob && jobSummary.fullJob.finished_time != null) {
        return 'finished';
      } else if (jobSummary.jobState === 'closed' || jobSummary.jobState === 'closing' || jobSummary.datafeedState === 'stopped') {
        return 'stopped';
      } else if (jobSummary.jobState === 'opening') {
        return 'initializing';
      } else if (jobSummary.jobState === 'opened' && jobSummary.datafeedState === 'started') {
        return 'started';
      }

      return 'unknown';
    })[0] || 'missing';
  };
};