"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraMetric = exports.InfraSnapshotMetricType = exports.InfraNodeType = exports.InfraIndexType = void 0;

/* tslint:disable */
// ====================================================
// START: Typescript template
// ====================================================
// ====================================================
// Types
// ====================================================

/** A source of infrastructure data */

/** A set of configuration options for an infrastructure data source */

/** A mapping of semantic fields to their document counterparts */

/** The built-in timestamp log column */

/** The built-in message log column */

/** A log column containing a field value */

/** The status of an infrastructure data source */

/** A descriptor of a field in an index */

/** A consecutive sequence of log entries */

/** A representation of the log entry's position in the event stream */

/** A log entry */

/** A special built-in column that contains the log entry's timestamp */

/** A special built-in column that contains the log entry's constructed message */

/** A segment of the log entry message that was derived from a field */

/** A segment of the log entry message that was derived from a string literal */

/** A column that contains the value of a field of the log entry */

/** A consecutive sequence of log summary buckets */

/** A log summary bucket */

/** A consecutive sequence of log summary highlight buckets */

/** A log summary highlight bucket */

/** The result of a successful source update */

/** The result of a source deletion operations */
// ====================================================
// InputTypes
// ====================================================

/** A highlighting definition */

/** The properties to update the source with */

/** The mapping of semantic fields of the source to be created */

/** One of the log column types to display for this source */
// ====================================================
// Arguments
// ====================================================
// ====================================================
// Enums
// ====================================================
var InfraIndexType;
exports.InfraIndexType = InfraIndexType;

(function (InfraIndexType) {
  InfraIndexType["ANY"] = "ANY";
  InfraIndexType["LOGS"] = "LOGS";
  InfraIndexType["METRICS"] = "METRICS";
})(InfraIndexType || (exports.InfraIndexType = InfraIndexType = {}));

var InfraNodeType;
exports.InfraNodeType = InfraNodeType;

(function (InfraNodeType) {
  InfraNodeType["pod"] = "pod";
  InfraNodeType["container"] = "container";
  InfraNodeType["host"] = "host";
})(InfraNodeType || (exports.InfraNodeType = InfraNodeType = {}));

var InfraSnapshotMetricType;
exports.InfraSnapshotMetricType = InfraSnapshotMetricType;

(function (InfraSnapshotMetricType) {
  InfraSnapshotMetricType["count"] = "count";
  InfraSnapshotMetricType["cpu"] = "cpu";
  InfraSnapshotMetricType["load"] = "load";
  InfraSnapshotMetricType["memory"] = "memory";
  InfraSnapshotMetricType["tx"] = "tx";
  InfraSnapshotMetricType["rx"] = "rx";
  InfraSnapshotMetricType["logRate"] = "logRate";
})(InfraSnapshotMetricType || (exports.InfraSnapshotMetricType = InfraSnapshotMetricType = {}));

var InfraMetric; // ====================================================
// Unions
// ====================================================

/** All known log column types */

exports.InfraMetric = InfraMetric;

(function (InfraMetric) {
  InfraMetric["hostSystemOverview"] = "hostSystemOverview";
  InfraMetric["hostCpuUsage"] = "hostCpuUsage";
  InfraMetric["hostFilesystem"] = "hostFilesystem";
  InfraMetric["hostK8sOverview"] = "hostK8sOverview";
  InfraMetric["hostK8sCpuCap"] = "hostK8sCpuCap";
  InfraMetric["hostK8sDiskCap"] = "hostK8sDiskCap";
  InfraMetric["hostK8sMemoryCap"] = "hostK8sMemoryCap";
  InfraMetric["hostK8sPodCap"] = "hostK8sPodCap";
  InfraMetric["hostLoad"] = "hostLoad";
  InfraMetric["hostMemoryUsage"] = "hostMemoryUsage";
  InfraMetric["hostNetworkTraffic"] = "hostNetworkTraffic";
  InfraMetric["podOverview"] = "podOverview";
  InfraMetric["podCpuUsage"] = "podCpuUsage";
  InfraMetric["podMemoryUsage"] = "podMemoryUsage";
  InfraMetric["podLogUsage"] = "podLogUsage";
  InfraMetric["podNetworkTraffic"] = "podNetworkTraffic";
  InfraMetric["containerOverview"] = "containerOverview";
  InfraMetric["containerCpuKernel"] = "containerCpuKernel";
  InfraMetric["containerCpuUsage"] = "containerCpuUsage";
  InfraMetric["containerDiskIOOps"] = "containerDiskIOOps";
  InfraMetric["containerDiskIOBytes"] = "containerDiskIOBytes";
  InfraMetric["containerMemory"] = "containerMemory";
  InfraMetric["containerNetworkTraffic"] = "containerNetworkTraffic";
  InfraMetric["nginxHits"] = "nginxHits";
  InfraMetric["nginxRequestRate"] = "nginxRequestRate";
  InfraMetric["nginxActiveConnections"] = "nginxActiveConnections";
  InfraMetric["nginxRequestsPerConnection"] = "nginxRequestsPerConnection";
  InfraMetric["awsOverview"] = "awsOverview";
  InfraMetric["awsCpuUtilization"] = "awsCpuUtilization";
  InfraMetric["awsNetworkBytes"] = "awsNetworkBytes";
  InfraMetric["awsNetworkPackets"] = "awsNetworkPackets";
  InfraMetric["awsDiskioBytes"] = "awsDiskioBytes";
  InfraMetric["awsDiskioOps"] = "awsDiskioOps";
  InfraMetric["custom"] = "custom";
})(InfraMetric || (exports.InfraMetric = InfraMetric = {})); // ====================================================
// END: Typescript template
// ====================================================
// ====================================================
// Documents
// ====================================================