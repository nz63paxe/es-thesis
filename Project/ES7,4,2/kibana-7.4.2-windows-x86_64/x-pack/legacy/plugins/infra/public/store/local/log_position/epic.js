"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createLogPositionEpic = void 0;

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _actions = require("./actions");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var createLogPositionEpic = function createLogPositionEpic() {
  return function (action$) {
    return action$.pipe((0, _operators.filter)(_actions.startAutoReload.match), (0, _operators.exhaustMap)(function (_ref) {
      var payload = _ref.payload;
      return (0, _rxjs.timer)(0, payload).pipe((0, _operators.map)(function () {
        return (0, _actions.jumpToTargetPositionTime)(Date.now());
      }), (0, _operators.takeUntil)(action$.pipe((0, _operators.filter)(_actions.stopAutoReload.match))));
    }));
  };
};

exports.createLogPositionEpic = createLogPositionEpic;