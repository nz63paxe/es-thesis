"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableView = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _lodash = require("lodash");

var _react2 = _interopRequireDefault(require("react"));

var _nodes_to_wafflemap = require("../../containers/waffle/nodes_to_wafflemap");

var _field_to_display_name = require("../waffle/lib/field_to_display_name");

var _node_context_menu = require("../waffle/node_context_menu");

var _temp;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  isPopoverOpen: []
};

var getGroupPaths = function getGroupPaths(path) {
  switch (path.length) {
    case 3:
      return path.slice(0, 2);

    case 2:
      return path.slice(0, 1);

    default:
      return [];
  }
};

var TableView = (0, _react.injectI18n)((_temp =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(_temp, _React$PureComponent);

  function _temp() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, _temp);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(_temp)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", initialState);

    _defineProperty(_assertThisInitialized(_this), "openPopoverFor", function (id) {
      return function () {
        _this.setState(function (prevState) {
          return {
            isPopoverOpen: [].concat(_toConsumableArray(prevState.isPopoverOpen), [id])
          };
        });
      };
    });

    _defineProperty(_assertThisInitialized(_this), "closePopoverFor", function (id) {
      return function () {
        if (_this.state.isPopoverOpen.includes(id)) {
          _this.setState(function (prevState) {
            return {
              isPopoverOpen: prevState.isPopoverOpen.filter(function (subject) {
                return subject !== id;
              })
            };
          });
        }
      };
    });

    return _this;
  }

  _createClass(_temp, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          nodes = _this$props.nodes,
          options = _this$props.options,
          formatter = _this$props.formatter,
          intl = _this$props.intl,
          timeRange = _this$props.timeRange,
          nodeType = _this$props.nodeType;
      var columns = [{
        field: 'name',
        name: intl.formatMessage({
          id: 'xpack.infra.tableView.columnName.name',
          defaultMessage: 'Name'
        }),
        sortable: true,
        truncateText: true,
        textOnly: true,
        render: function render(value, item) {
          var tooltipText = item.node.id === value ? "".concat(value) : "".concat(value, " (").concat(item.node.id, ")");
          return _react2.default.createElement(_node_context_menu.NodeContextMenu, {
            node: item.node,
            nodeType: nodeType,
            closePopover: _this2.closePopoverFor(item.node.pathId),
            timeRange: timeRange,
            isPopoverOpen: _this2.state.isPopoverOpen.includes(item.node.pathId),
            options: options,
            popoverPosition: "rightCenter"
          }, _react2.default.createElement(_eui.EuiToolTip, {
            content: tooltipText
          }, _react2.default.createElement(_eui.EuiButtonEmpty, {
            onClick: _this2.openPopoverFor(item.node.pathId)
          }, value)));
        }
      }].concat(_toConsumableArray(options.groupBy.map(function (grouping, index) {
        return {
          field: "group_".concat(index),
          name: (0, _field_to_display_name.fieldToName)(grouping && grouping.field || '', intl),
          sortable: true,
          truncateText: true,
          textOnly: true,
          render: function render(value) {
            var handleClick = function handleClick() {
              return _this2.props.onFilter("".concat(grouping.field, ":\"").concat(value, "\""));
            };

            return _react2.default.createElement(_eui.EuiToolTip, {
              content: "Set Filter"
            }, _react2.default.createElement(_eui.EuiButtonEmpty, {
              onClick: handleClick
            }, value));
          }
        };
      })), [{
        field: 'value',
        name: intl.formatMessage({
          id: 'xpack.infra.tableView.columnName.last1m',
          defaultMessage: 'Last 1m'
        }),
        sortable: true,
        truncateText: true,
        dataType: 'number',
        render: function render(value) {
          return _react2.default.createElement("span", null, formatter(value));
        }
      }, {
        field: 'avg',
        name: intl.formatMessage({
          id: 'xpack.infra.tableView.columnName.avg',
          defaultMessage: 'Avg'
        }),
        sortable: true,
        truncateText: true,
        dataType: 'number',
        render: function render(value) {
          return _react2.default.createElement("span", null, formatter(value));
        }
      }, {
        field: 'max',
        name: intl.formatMessage({
          id: 'xpack.infra.tableView.columnName.max',
          defaultMessage: 'Max'
        }),
        sortable: true,
        truncateText: true,
        dataType: 'number',
        render: function render(value) {
          return _react2.default.createElement("span", null, formatter(value));
        }
      }]);
      var items = nodes.map(function (node) {
        var name = (0, _lodash.last)(node.path);
        return _objectSpread({
          name: name && name.label || 'unknown'
        }, getGroupPaths(node.path).reduce(function (acc, path, index) {
          return _objectSpread({}, acc, _defineProperty({}, "group_".concat(index), path.label));
        }, {}), {
          value: node.metric.value,
          avg: node.metric.avg,
          max: node.metric.max,
          node: (0, _nodes_to_wafflemap.createWaffleMapNode)(node)
        });
      });
      var initialSorting = {
        sort: {
          field: 'value',
          direction: 'desc'
        }
      };
      return _react2.default.createElement(_eui.EuiInMemoryTable, {
        pagination: true,
        sorting: initialSorting,
        items: items,
        columns: columns
      });
    }
  }]);

  return _temp;
}(_react2.default.PureComponent), _temp));
exports.TableView = TableView;