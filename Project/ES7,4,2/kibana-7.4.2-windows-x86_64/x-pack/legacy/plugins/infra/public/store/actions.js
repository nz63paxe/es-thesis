"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "logFilterActions", {
  enumerable: true,
  get: function get() {
    return _local.logFilterActions;
  }
});
Object.defineProperty(exports, "logPositionActions", {
  enumerable: true,
  get: function get() {
    return _local.logPositionActions;
  }
});
Object.defineProperty(exports, "waffleFilterActions", {
  enumerable: true,
  get: function get() {
    return _local.waffleFilterActions;
  }
});
Object.defineProperty(exports, "waffleTimeActions", {
  enumerable: true,
  get: function get() {
    return _local.waffleTimeActions;
  }
});
Object.defineProperty(exports, "waffleOptionsActions", {
  enumerable: true,
  get: function get() {
    return _local.waffleOptionsActions;
  }
});
Object.defineProperty(exports, "logEntriesActions", {
  enumerable: true,
  get: function get() {
    return _remote.logEntriesActions;
  }
});

var _local = require("./local");

var _remote = require("./remote");