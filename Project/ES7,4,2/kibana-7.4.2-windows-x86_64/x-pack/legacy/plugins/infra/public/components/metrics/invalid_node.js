"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InvalidNodeError = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../common/eui_styled_components"));

var _with_kibana_chrome = require("../../containers/with_kibana_chrome");

var _source_configuration = require("../../components/source_configuration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-self: center;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var InvalidNodeError = function InvalidNodeError(_ref) {
  var nodeName = _ref.nodeName;
  return _react2.default.createElement(_with_kibana_chrome.WithKibanaChrome, null, function (_ref2) {
    var basePath = _ref2.basePath;
    return _react2.default.createElement(CenteredEmptyPrompt, {
      title: _react2.default.createElement("h2", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.metrics.invalidNodeErrorTitle",
        defaultMessage: "Looks like {nodeName} isn't collecting any metrics data",
        values: {
          nodeName: nodeName
        }
      })),
      body: _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.metrics.invalidNodeErrorDescription",
        defaultMessage: "Double check your configuration"
      })),
      actions: _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiButton, {
        href: "".concat(basePath, "/app/kibana#/home/tutorial_directory/metrics"),
        color: "primary",
        fill: true
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.homePage.noMetricsIndicesInstructionsActionLabel",
        defaultMessage: "View setup instructions"
      }))), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_source_configuration.ViewSourceConfigurationButton, {
        "data-test-subj": "configureSourceButton",
        hrefBase: _source_configuration.ViewSourceConfigurationButtonHrefBase.infrastructure
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.configureSourceActionLabel",
        defaultMessage: "Change source configuration"
      }))))
    });
  });
};

exports.InvalidNodeError = InvalidNodeError;
var CenteredEmptyPrompt = (0, _eui_styled_components.default)(_eui.EuiEmptyPrompt)(_templateObject());