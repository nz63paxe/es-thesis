"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logSummaryHighlightsQuery = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

var _shared = require("../../../../common/graphql/shared");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  query LogSummaryHighlightsQuery(\n    $sourceId: ID = \"default\"\n    $start: Float!\n    $end: Float!\n    $bucketSize: Float!\n    $highlightQueries: [String!]!\n    $filterQuery: String\n  ) {\n    source(id: $sourceId) {\n      id\n      logSummaryHighlightsBetween(\n        start: $start\n        end: $end\n        bucketSize: $bucketSize\n        highlightQueries: $highlightQueries\n        filterQuery: $filterQuery\n      ) {\n        start\n        end\n        buckets {\n          start\n          end\n          entriesCount\n          representativeKey {\n            ...InfraTimeKeyFields\n          }\n        }\n      }\n    }\n  }\n\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var logSummaryHighlightsQuery = (0, _graphqlTag.default)(_templateObject(), _shared.sharedFragments.InfraTimeKey);
exports.logSummaryHighlightsQuery = logSummaryHighlightsQuery;