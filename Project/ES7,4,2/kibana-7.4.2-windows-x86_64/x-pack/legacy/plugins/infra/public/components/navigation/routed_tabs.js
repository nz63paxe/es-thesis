"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RoutedTabs = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _eui_styled_components = _interopRequireDefault(require("../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  .euiLink {\n    color: inherit !important;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var noop = function noop() {};

var RoutedTabs =
/*#__PURE__*/
function (_React$Component) {
  _inherits(RoutedTabs, _React$Component);

  function RoutedTabs() {
    _classCallCheck(this, RoutedTabs);

    return _possibleConstructorReturn(this, _getPrototypeOf(RoutedTabs).apply(this, arguments));
  }

  _createClass(RoutedTabs, [{
    key: "render",
    value: function render() {
      return _react.default.createElement(_eui.EuiTabs, {
        display: "condensed"
      }, this.renderTabs());
    }
  }, {
    key: "renderTabs",
    value: function renderTabs() {
      return this.props.tabs.map(function (tab) {
        return _react.default.createElement(_reactRouterDom.Route, {
          key: "".concat(tab.path, "-").concat(tab.title),
          path: tab.path,
          children: function children(_ref) {
            var match = _ref.match,
                history = _ref.history;
            return _react.default.createElement(TabContainer, {
              className: "euiTab"
            }, _react.default.createElement(_eui.EuiLink, {
              href: "#".concat(tab.path),
              onClick: function onClick(e) {
                e.preventDefault();
                history.push(tab.path);
              }
            }, _react.default.createElement(_eui.EuiTab, {
              onClick: noop,
              isSelected: match !== null
            }, tab.title)));
          }
        });
      });
    }
  }]);

  return RoutedTabs;
}(_react.default.Component);

exports.RoutedTabs = RoutedTabs;

var TabContainer = _eui_styled_components.default.div(_templateObject());