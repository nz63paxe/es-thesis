"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.remoteReducer = exports.initialRemoteState = void 0;

var _redux = require("redux");

var _log_entries = require("./log_entries");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialRemoteState = {
  logEntries: _log_entries.initialLogEntriesState
};
exports.initialRemoteState = initialRemoteState;
var remoteReducer = (0, _redux.combineReducers)({
  logEntries: _log_entries.logEntriesReducer
});
exports.remoteReducer = remoteReducer;