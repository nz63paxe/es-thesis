"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ReduxSourceIdBridge = exports.WithStreamItems = exports.withStreamItems = void 0;

var _react = require("react");

var _reactRedux = require("react-redux");

var _store = require("../../store");

var _typed_redux = require("../../utils/typed_redux");

var _log_highlights = require("./log_highlights/log_highlights");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var withStreamItems = (0, _reactRedux.connect)(function (state) {
  return {
    isAutoReloading: _store.logPositionSelectors.selectIsAutoReloading(state),
    isReloading: _store.logEntriesSelectors.selectIsReloadingEntries(state),
    isLoadingMore: _store.logEntriesSelectors.selectIsLoadingMoreEntries(state),
    hasMoreBeforeStart: _store.logEntriesSelectors.selectHasMoreBeforeStart(state),
    hasMoreAfterEnd: _store.logEntriesSelectors.selectHasMoreAfterEnd(state),
    lastLoadedTime: _store.logEntriesSelectors.selectEntriesLastLoadedTime(state),
    entries: _store.logEntriesSelectors.selectEntries(state),
    entriesStart: _store.logEntriesSelectors.selectEntriesStart(state),
    entriesEnd: _store.logEntriesSelectors.selectEntriesEnd(state)
  };
}, (0, _typed_redux.bindPlainActionCreators)({
  loadNewerEntries: _store.logEntriesActions.loadNewerEntries,
  reloadEntries: _store.logEntriesActions.reloadEntries,
  setSourceId: _store.logEntriesActions.setSourceId
}));
exports.withStreamItems = withStreamItems;
var WithStreamItems = withStreamItems(function (_ref) {
  var children = _ref.children,
      initializeOnMount = _ref.initializeOnMount,
      props = _objectWithoutProperties(_ref, ["children", "initializeOnMount"]);

  var _useContext = (0, _react.useContext)(_log_highlights.LogHighlightsState.Context),
      currentHighlightKey = _useContext.currentHighlightKey,
      logEntryHighlightsById = _useContext.logEntryHighlightsById;

  var items = (0, _react.useMemo)(function () {
    return props.isReloading && !props.isAutoReloading ? [] : props.entries.map(function (logEntry) {
      return createLogEntryStreamItem(logEntry, logEntryHighlightsById[logEntry.gid] || []);
    });
  }, [props.isReloading, props.isAutoReloading, props.entries, logEntryHighlightsById]);
  (0, _react.useEffect)(function () {
    if (initializeOnMount && !props.isReloading && !props.isLoadingMore) {
      props.reloadEntries();
    }
  }, []);
  return children(_objectSpread({}, props, {
    currentHighlightKey: currentHighlightKey,
    items: items
  }));
});
exports.WithStreamItems = WithStreamItems;

var createLogEntryStreamItem = function createLogEntryStreamItem(logEntry, highlights) {
  return {
    kind: 'logEntry',
    logEntry: logEntry,
    highlights: highlights
  };
};
/**
 * This component serves as connection between the state and side-effects
 * managed by redux and the state and effects managed by hooks. In particular,
 * it forwards changes of the source id to redux via the action creator
 * `setSourceId`.
 *
 * It will be mounted beneath the hierachy level where the redux store and the
 * source state are initialized. Once the log entry state and loading
 * side-effects have been migrated from redux to hooks it can be removed.
 */


var ReduxSourceIdBridge = withStreamItems(function (_ref2) {
  var setSourceId = _ref2.setSourceId,
      sourceId = _ref2.sourceId;
  (0, _react.useEffect)(function () {
    setSourceId(sourceId);
  }, [setSourceId, sourceId]);
  return null;
});
exports.ReduxSourceIdBridge = ReduxSourceIdBridge;