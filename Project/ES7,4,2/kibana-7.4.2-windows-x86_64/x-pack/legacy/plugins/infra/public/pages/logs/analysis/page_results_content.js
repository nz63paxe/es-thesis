"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisResultsContent = void 0;

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = require("@kbn/i18n/react");

var _moment = _interopRequireDefault(require("moment"));

var _react2 = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _log_analysis = require("../../../../common/log_analysis");

var _loading_page = require("../../../components/loading_page");

var _log_analysis2 = require("../../../containers/logs/log_analysis");

var _use_track_metric = require("../../../hooks/use_track_metric");

var _first_use = require("./first_use");

var _log_rate = require("./sections/log_rate");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  flex: 1 0 0%;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var AnalysisResultsContent = function AnalysisResultsContent(_ref) {
  var sourceId = _ref.sourceId,
      isFirstUse = _ref.isFirstUse;
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_logs',
    path: 'analysis_results'
  });
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_logs',
    path: 'analysis_results',
    delay: 15000
  });

  var _useLogAnalysisResult = (0, _log_analysis2.useLogAnalysisResultsUrlState)(),
      selectedTimeRange = _useLogAnalysisResult.timeRange,
      setSelectedTimeRange = _useLogAnalysisResult.setTimeRange,
      autoRefresh = _useLogAnalysisResult.autoRefresh,
      setAutoRefresh = _useLogAnalysisResult.setAutoRefresh;

  var _useState = (0, _react2.useState)(stringToNumericTimeRange(selectedTimeRange)),
      _useState2 = _slicedToArray(_useState, 2),
      queryTimeRange = _useState2[0],
      setQueryTimeRange = _useState2[1];

  var bucketDuration = (0, _react2.useMemo)(function () {
    // This function takes the current time range in ms,
    // works out the bucket interval we'd need to always
    // display 200 data points, and then takes that new
    // value and works out the nearest multiple of
    // 900000 (15 minutes) to it, so that we don't end up with
    // jaggy bucket boundaries between the ML buckets and our
    // aggregation buckets.
    var msRange = (0, _moment.default)(queryTimeRange.endTime).diff((0, _moment.default)(queryTimeRange.startTime));
    var bucketIntervalInMs = msRange / 200;
    var result = _log_analysis.bucketSpan * Math.round(bucketIntervalInMs / _log_analysis.bucketSpan);
    var roundedResult = parseInt(Number(result).toFixed(0), 10);
    return roundedResult < _log_analysis.bucketSpan ? _log_analysis.bucketSpan : roundedResult;
  }, [queryTimeRange.startTime, queryTimeRange.endTime]);

  var _useLogAnalysisResult2 = (0, _log_analysis2.useLogAnalysisResults)({
    sourceId: sourceId,
    startTime: queryTimeRange.startTime,
    endTime: queryTimeRange.endTime,
    bucketDuration: bucketDuration
  }),
      isLoading = _useLogAnalysisResult2.isLoading,
      logEntryRate = _useLogAnalysisResult2.logEntryRate;

  var hasResults = (0, _react2.useMemo)(function () {
    return logEntryRate && logEntryRate.histogramBuckets.length > 0;
  }, [logEntryRate]);
  var handleQueryTimeRangeChange = (0, _react2.useCallback)(function (_ref2) {
    var startTime = _ref2.start,
        endTime = _ref2.end;
    setQueryTimeRange(stringToNumericTimeRange({
      startTime: startTime,
      endTime: endTime
    }));
  }, [setQueryTimeRange]);
  var handleSelectedTimeRangeChange = (0, _react2.useCallback)(function (selectedTime) {
    if (selectedTime.isInvalid) {
      return;
    }

    setSelectedTimeRange({
      startTime: selectedTime.start,
      endTime: selectedTime.end
    });
    handleQueryTimeRangeChange(selectedTime);
  }, [setSelectedTimeRange, handleQueryTimeRangeChange]);
  var handleAutoRefreshChange = (0, _react2.useCallback)(function (_ref3) {
    var isPaused = _ref3.isPaused,
        interval = _ref3.refreshInterval;
    setAutoRefresh({
      isPaused: isPaused,
      interval: interval
    });
  }, [setAutoRefresh]);
  var anomaliesDetected = (0, _react2.useMemo)(function () {
    if (!logEntryRate) {
      return null;
    } else {
      if (logEntryRate.histogramBuckets && logEntryRate.histogramBuckets.length) {
        return logEntryRate.histogramBuckets.reduce(function (acc, bucket) {
          return acc + bucket.anomalies.length;
        }, 0);
      } else {
        return null;
      }
    }
  }, [logEntryRate]);
  return _react2.default.createElement(_react2.default.Fragment, null, isLoading && !logEntryRate ? _react2.default.createElement(_loading_page.LoadingPage, {
    message: _i18n.i18n.translate('xpack.infra.logs.logsAnalysisResults.loadingMessage', {
      defaultMessage: 'Loading results...'
    })
  }) : _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_eui.EuiPage, null, _react2.default.createElement(_eui.EuiPanel, {
    paddingSize: "l"
  }, _react2.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: 7
  }, _react2.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center"
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, anomaliesDetected !== null ? _react2.default.createElement("span", null, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.infra.logs.analysis.anomaliesDetectedText",
    defaultMessage: "Detected {formattedNumber} {number, plural, one {anomaly} other {anomalies}}",
    values: {
      formattedNumber: _react2.default.createElement(_eui.EuiBadge, {
        color: anomaliesDetected === 0 ? 'default' : 'warning'
      }, anomaliesDetected),
      number: anomaliesDetected
    }
  })) : null))), _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiSuperDatePicker, {
    start: selectedTimeRange.startTime,
    end: selectedTimeRange.endTime,
    onTimeChange: handleSelectedTimeRangeChange,
    isPaused: autoRefresh.isPaused,
    refreshInterval: autoRefresh.interval,
    onRefreshChange: handleAutoRefreshChange,
    onRefresh: handleQueryTimeRangeChange
  }))))), _react2.default.createElement(ExpandingPage, null, _react2.default.createElement(_eui.EuiPageBody, null, _react2.default.createElement(_eui.EuiPageContent, null, _react2.default.createElement(_eui.EuiPageContentBody, null, isFirstUse && !hasResults ? _react2.default.createElement(_first_use.FirstUseCallout, null) : null, _react2.default.createElement(_log_rate.LogRateResults, {
    isLoading: isLoading,
    results: logEntryRate,
    timeRange: queryTimeRange
  })))))));
};

exports.AnalysisResultsContent = AnalysisResultsContent;

var stringToNumericTimeRange = function stringToNumericTimeRange(timeRange) {
  return {
    startTime: (0, _moment.default)(_datemath.default.parse(timeRange.startTime, {
      momentInstance: _moment.default
    })).valueOf(),
    endTime: (0, _moment.default)(_datemath.default.parse(timeRange.endTime, {
      momentInstance: _moment.default,
      roundUp: true
    })).valueOf()
  };
};

var ExpandingPage = (0, _eui_styled_components.default)(_eui.EuiPage)(_templateObject());