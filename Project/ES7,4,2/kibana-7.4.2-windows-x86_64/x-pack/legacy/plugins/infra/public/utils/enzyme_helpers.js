"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.shallowWithIntl = shallowWithIntl;
exports.mountWithIntl = mountWithIntl;
exports.renderWithIntl = renderWithIntl;
exports.mountHook = void 0;

var _react = require("@kbn/i18n/react");

var _enzyme = require("enzyme");

var _react2 = _interopRequireDefault(require("react"));

var _testUtils = require("react-dom/test-utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// Use fake component to extract `intl` property to use in tests.
var _instance$getChildCon = (0, _enzyme.mount)(_react2.default.createElement(_react.I18nProvider, null, _react2.default.createElement("br", null))).find('IntlProvider').instance().getChildContext(),
    intl = _instance$getChildCon.intl;

function getOptions() {
  var context = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var childContextTypes = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  var props = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  return _objectSpread({
    context: _objectSpread({}, context, {
      intl: intl
    }),
    childContextTypes: _objectSpread({}, childContextTypes, {
      intl: _react.intlShape
    })
  }, props);
}
/**
 * When using React-Intl `injectIntl` on components, props.intl is required.
 */


function nodeWithIntlProp(node) {
  return _react2.default.cloneElement(node, {
    intl: intl
  });
}
/**
 *  Creates the wrapper instance using shallow with provided intl object into context
 *
 *  @param node The React element or cheerio wrapper
 *  @param options properties to pass into shallow wrapper
 *  @return The wrapper instance around the rendered output with intl object in context
 */


function shallowWithIntl(node) {
  var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      context = _ref.context,
      childContextTypes = _ref.childContextTypes,
      props = _objectWithoutProperties(_ref, ["context", "childContextTypes"]);

  var options = getOptions(context, childContextTypes, props);
  return (0, _enzyme.shallow)(nodeWithIntlProp(node), options);
}
/**
 *  Creates the wrapper instance using mount with provided intl object into context
 *
 *  @param node The React element or cheerio wrapper
 *  @param options properties to pass into mount wrapper
 *  @return The wrapper instance around the rendered output with intl object in context
 */


function mountWithIntl(node) {
  var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      context = _ref2.context,
      childContextTypes = _ref2.childContextTypes,
      props = _objectWithoutProperties(_ref2, ["context", "childContextTypes"]);

  var options = getOptions(context, childContextTypes, props);
  return (0, _enzyme.mount)(nodeWithIntlProp(node), options);
}
/**
 *  Creates the wrapper instance using render with provided intl object into context
 *
 *  @param node The React element or cheerio wrapper
 *  @param options properties to pass into render wrapper
 *  @return The wrapper instance around the rendered output with intl object in context
 */


function renderWithIntl(node) {
  var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
      context = _ref3.context,
      childContextTypes = _ref3.childContextTypes,
      props = _objectWithoutProperties(_ref3, ["context", "childContextTypes"]);

  var options = getOptions(context, childContextTypes, props);
  return (0, _enzyme.render)(nodeWithIntlProp(node), options);
}
/**
 * A wrapper object to provide access to the state of a hook under test and to
 * enable interaction with that hook.
 */


/**
 * Allows for execution of hooks inside of a test component which records the
 * returned values.
 *
 * @param body A function that calls the hook and returns data derived from it
 * @param WrapperComponent A component that, if provided, will be wrapped
 * around the test component. This can be useful to provide context values.
 * @return {ReactHookWrapper} An object providing access to the hook state and
 * functions to interact with it.
 */
var mountHook = function mountHook(body, WrapperComponent) {
  var initialArgs = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
  var hookValueCallback = jest.fn();
  var component;

  var act = function act(actor) {
    (0, _testUtils.act)(function () {
      actor(getLastHookValue(), function (args) {
        return component.setProps(args);
      });
      component.update();
    });
  };

  var getLastHookValue = function getLastHookValue() {
    var calls = hookValueCallback.mock.calls;

    if (calls.length <= 0) {
      throw Error('No recent hook value present.');
    }

    return calls[calls.length - 1][0];
  };

  var HookComponent = function HookComponent(props) {
    hookValueCallback(body(props));
    return null;
  };

  var TestComponent = function TestComponent(args) {
    return WrapperComponent ? _react2.default.createElement(WrapperComponent, null, _react2.default.createElement(HookComponent, args)) : _react2.default.createElement(HookComponent, args);
  };

  (0, _testUtils.act)(function () {
    component = (0, _enzyme.mount)(_react2.default.createElement(TestComponent, initialArgs));
  });
  return {
    act: act,
    component: component,
    getLastHookValue: getLastHookValue,
    hookValueCallback: hookValueCallback
  };
};

exports.mountHook = mountHook;