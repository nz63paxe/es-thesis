"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLogSummaryBufferInterval = void 0;

var _react = require("react");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LOAD_BUCKETS_PER_PAGE = 100;
var UNKNOWN_BUFFER_INTERVAL = {
  start: null,
  end: null,
  bucketSize: 0
};

var useLogSummaryBufferInterval = function useLogSummaryBufferInterval(midpointTime, intervalSize) {
  return (0, _react.useMemo)(function () {
    if (midpointTime === null || intervalSize <= 0) {
      return UNKNOWN_BUFFER_INTERVAL;
    }

    var halfIntervalSize = intervalSize / 2;
    return {
      start: (Math.floor((midpointTime - halfIntervalSize) / intervalSize) - 0.5) * intervalSize,
      end: (Math.ceil((midpointTime + halfIntervalSize) / intervalSize) + 0.5) * intervalSize,
      bucketSize: intervalSize / LOAD_BUCKETS_PER_PAGE
    };
  }, [midpointTime, intervalSize]);
};

exports.useLogSummaryBufferInterval = useLogSummaryBufferInterval;