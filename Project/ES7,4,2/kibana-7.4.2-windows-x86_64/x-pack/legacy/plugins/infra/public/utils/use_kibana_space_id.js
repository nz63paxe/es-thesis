"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useKibanaSpaceId = void 0;

var rt = _interopRequireWildcard(require("io-ts"));

var _use_kibana_injected_var = require("./use_kibana_injected_var");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useKibanaSpaceId = function useKibanaSpaceId() {
  var activeSpace = (0, _use_kibana_injected_var.useKibanaInjectedVar)('activeSpace');
  return activeSpaceRT.decode(activeSpace).fold(function () {
    return 'default';
  }, function (decodedActiveSpace) {
    return decodedActiveSpace.space.id;
  });
};

exports.useKibanaSpaceId = useKibanaSpaceId;
var activeSpaceRT = rt.type({
  space: rt.type({
    id: rt.string
  })
});