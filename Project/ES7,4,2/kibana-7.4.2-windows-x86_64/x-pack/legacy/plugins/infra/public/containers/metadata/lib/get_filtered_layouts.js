"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getFilteredLayout = exports.getFilteredLayouts = void 0;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getFilteredLayouts = function getFilteredLayouts(layouts, metadata) {
  if (!metadata) {
    return layouts;
  }

  var metricMetadata = metadata.filter(function (data) {
    return data && data.source === 'metrics';
  }).map(function (data) {
    return data && data.name;
  }); // After filtering out sections that can't be displayed, a layout may end up empty and can be removed.

  var filteredLayouts = layouts.map(function (layout) {
    return getFilteredLayout(layout, metricMetadata);
  }).filter(function (layout) {
    return layout.sections.length > 0;
  });
  return filteredLayouts;
};

exports.getFilteredLayouts = getFilteredLayouts;

var getFilteredLayout = function getFilteredLayout(layout, metricMetadata) {
  // A section is only displayed if at least one of its requirements is met
  // All others are filtered out.
  var filteredSections = layout.sections.filter(function (section) {
    return _.intersection(section.requires, metricMetadata).length > 0;
  });
  return _objectSpread({}, layout, {
    sections: filteredSections
  });
};

exports.getFilteredLayout = getFilteredLayout;