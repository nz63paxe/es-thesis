"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

require("ui/autoload/all");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _modules = require("ui/modules");

var _routes = _interopRequireDefault(require("ui/routes"));

var _timezone = require("ui/vis/lib/timezone");

var _apolloCacheInmemory = require("apollo-cache-inmemory");

var _apolloClient = _interopRequireDefault(require("apollo-client"));

var _apolloLink = require("apollo-link");

var _apolloLinkHttp = require("apollo-link-http");

var _apolloLinkState = require("apollo-link-state");

var _introspection = _interopRequireDefault(require("../../graphql/introspection.json"));

var _kibana_framework_adapter = require("../adapters/framework/kibana_framework_adapter");

var _kibana_observable_api = require("../adapters/observable_api/kibana_observable_api");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore: path dynamic for kibana
// @ts-ignore: path dynamic for kibana
// @ts-ignore: path dynamic for kibana
function compose() {
  var cache = new _apolloCacheInmemory.InMemoryCache({
    addTypename: false,
    fragmentMatcher: new _apolloCacheInmemory.IntrospectionFragmentMatcher({
      introspectionQueryResultData: _introspection.default
    })
  });
  var observableApi = new _kibana_observable_api.InfraKibanaObservableApiAdapter({
    basePath: _chrome.default.getBasePath(),
    xsrfToken: _chrome.default.getXsrfToken()
  });
  var graphQLOptions = {
    cache: cache,
    link: _apolloLink.ApolloLink.from([(0, _apolloLinkState.withClientState)({
      cache: cache,
      resolvers: {}
    }), new _apolloLinkHttp.HttpLink({
      credentials: 'same-origin',
      headers: {
        'kbn-xsrf': _chrome.default.getXsrfToken()
      },
      uri: "".concat(_chrome.default.getBasePath(), "/api/infra/graphql")
    })])
  };
  var apolloClient = new _apolloClient.default(graphQLOptions);

  var infraModule = _modules.uiModules.get('app/infa');

  var framework = new _kibana_framework_adapter.InfraKibanaFrameworkAdapter(infraModule, _routes.default, _timezone.timezoneProvider);
  var libs = {
    apolloClient: apolloClient,
    framework: framework,
    observableApi: observableApi
  };
  return libs;
}