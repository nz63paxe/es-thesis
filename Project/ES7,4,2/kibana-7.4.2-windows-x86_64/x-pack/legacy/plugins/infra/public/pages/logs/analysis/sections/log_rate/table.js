"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableView = void 0;

var _react = _interopRequireWildcard(require("react"));

var _i18n = require("@kbn/i18n");

var _moment = _interopRequireDefault(require("moment"));

var _eui = require("@elastic/eui");

var _use_kibana_ui_setting = require("../../../../../utils/use_kibana_ui_setting");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var startTimeLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.table.startTimeLabel', {
  defaultMessage: 'Start time'
});

var anomalyScoreLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.table.anomalyScoreLabel', {
  defaultMessage: 'Anomaly score'
});

var actualLogEntryRateLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.table.actualLogEntryRateLabel', {
  defaultMessage: 'Actual rate'
});

var typicalLogEntryRateLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.table.typicalLogEntryRateLabel', {
  defaultMessage: 'Typical rate'
});

var TableView = function TableView(_ref) {
  var data = _ref.data;

  var _useKibanaUiSetting = (0, _use_kibana_ui_setting.useKibanaUiSetting)('dateFormat'),
      _useKibanaUiSetting2 = _slicedToArray(_useKibanaUiSetting, 1),
      dateFormat = _useKibanaUiSetting2[0];

  var formattedAnomalies = (0, _react.useMemo)(function () {
    return data.histogramBuckets.reduce(function (acc, bucket) {
      if (bucket.anomalies.length > 0) {
        bucket.anomalies.forEach(function (anomaly) {
          var formattedAnomaly = {
            startTime: (0, _moment.default)(anomaly.startTime).format(dateFormat || 'Y-MM-DD HH:mm:ss.SSS'),
            anomalyScore: Number(anomaly.anomalyScore).toFixed(3),
            typicalLogEntryRate: Number(anomaly.typicalLogEntryRate).toFixed(3),
            actualLogEntryRate: Number(anomaly.actualLogEntryRate).toFixed(3)
          };
          acc.push(formattedAnomaly);
        });
        return acc;
      } else {
        return acc;
      }
    }, []);
  }, [data]);
  var columns = [{
    field: 'startTime',
    name: startTimeLabel,
    sortable: true,
    'data-test-subj': 'startTimeCell'
  }, {
    field: 'anomalyScore',
    name: anomalyScoreLabel,
    sortable: true,
    'data-test-subj': 'anomalyScoreCell'
  }, {
    field: 'actualLogEntryRate',
    name: actualLogEntryRateLabel,
    sortable: true,
    'data-test-subj': 'actualLogEntryRateCell'
  }, {
    field: 'typicalLogEntryRate',
    name: typicalLogEntryRateLabel,
    sortable: true,
    'data-test-subj': 'typicalLogEntryRateCell'
  }];
  var initialSorting = {
    sort: {
      field: 'anomalyScore',
      direction: 'desc'
    }
  };
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiInMemoryTable, {
    items: formattedAnomalies,
    columns: columns,
    sorting: initialSorting
  }));
};

exports.TableView = TableView;