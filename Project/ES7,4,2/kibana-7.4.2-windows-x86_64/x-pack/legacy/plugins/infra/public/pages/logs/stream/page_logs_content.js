"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogsPageLogsContent = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _auto_sizer = require("../../../components/auto_sizer");

var _log_entry_flyout = require("../../../components/logging/log_entry_flyout");

var _log_minimap = require("../../../components/logging/log_minimap");

var _log_text_stream = require("../../../components/logging/log_text_stream");

var _page = require("../../../components/page");

var _log_summary = require("../../../containers/logs/log_summary");

var _log_view_configuration = require("../../../containers/logs/log_view_configuration");

var _with_log_filter = require("../../../containers/logs/with_log_filter");

var _log_flyout = require("../../../containers/logs/log_flyout");

var _with_log_minimap = require("../../../containers/logs/with_log_minimap");

var _with_log_position = require("../../../containers/logs/with_log_position");

var _with_log_textview = require("../../../containers/logs/with_log_textview");

var _with_stream_items = require("../../../containers/logs/with_stream_items");

var _source = require("../../../containers/source");

var _page_toolbar = require("./page_toolbar");

var _log_highlights = require("../../../containers/logs/log_highlights");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  flex: 1 0 0%;\n  overflow: hidden;\n  min-width: 100px;\n  max-width: 100px;\n  display: flex;\n  flex-direction: column;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LogsPageLogsContent = function LogsPageLogsContent() {
  var _useContext = (0, _react.useContext)(_source.Source.Context),
      createDerivedIndexPattern = _useContext.createDerivedIndexPattern,
      source = _useContext.source,
      sourceId = _useContext.sourceId,
      version = _useContext.version;

  var _useContext2 = (0, _react.useContext)(_log_view_configuration.LogViewConfiguration.Context),
      intervalSize = _useContext2.intervalSize,
      textScale = _useContext2.textScale,
      textWrap = _useContext2.textWrap;

  var _useContext3 = (0, _react.useContext)(_log_flyout.LogFlyout.Context),
      setFlyoutVisibility = _useContext3.setFlyoutVisibility,
      flyoutVisible = _useContext3.flyoutVisible,
      setFlyoutId = _useContext3.setFlyoutId,
      surroundingLogsId = _useContext3.surroundingLogsId,
      setSurroundingLogsId = _useContext3.setSurroundingLogsId,
      flyoutItem = _useContext3.flyoutItem,
      isLoading = _useContext3.isLoading;

  var _useContext4 = (0, _react.useContext)(_log_highlights.LogHighlightsState.Context),
      logSummaryHighlights = _useContext4.logSummaryHighlights;

  var derivedIndexPattern = createDerivedIndexPattern('logs');
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_with_stream_items.ReduxSourceIdBridge, {
    sourceId: sourceId
  }), _react.default.createElement(_log_highlights.LogHighlightsBridge, {
    indexPattern: derivedIndexPattern
  }), _react.default.createElement(_with_log_filter.WithLogFilterUrlState, {
    indexPattern: derivedIndexPattern
  }), _react.default.createElement(_with_log_position.WithLogPositionUrlState, null), _react.default.createElement(_with_log_minimap.WithLogMinimapUrlState, null), _react.default.createElement(_with_log_textview.WithLogTextviewUrlState, null), _react.default.createElement(_log_flyout.WithFlyoutOptionsUrlState, null), _react.default.createElement(_page_toolbar.LogsToolbar, null), _react.default.createElement(_with_log_filter.WithLogFilter, {
    indexPattern: derivedIndexPattern
  }, function (_ref) {
    var applyFilterQueryFromKueryExpression = _ref.applyFilterQueryFromKueryExpression;
    return _react.default.createElement(_with_log_position.WithLogPosition, null, function (_ref2) {
      var jumpToTargetPosition = _ref2.jumpToTargetPosition,
          stopLiveStreaming = _ref2.stopLiveStreaming;
      return flyoutVisible ? _react.default.createElement(_log_entry_flyout.LogEntryFlyout, {
        setFilter: applyFilterQueryFromKueryExpression,
        setTarget: function setTarget(timeKey, flyoutItemId) {
          jumpToTargetPosition(timeKey);
          setSurroundingLogsId(flyoutItemId);
          stopLiveStreaming();
        },
        setFlyoutVisibility: setFlyoutVisibility,
        flyoutItem: flyoutItem,
        loading: isLoading
      }) : null;
    });
  }), _react.default.createElement(_page.PageContent, {
    key: "".concat(sourceId, "-").concat(version)
  }, _react.default.createElement(_with_log_position.WithLogPosition, null, function (_ref3) {
    var isAutoReloading = _ref3.isAutoReloading,
        jumpToTargetPosition = _ref3.jumpToTargetPosition,
        reportVisiblePositions = _ref3.reportVisiblePositions,
        targetPosition = _ref3.targetPosition;
    return _react.default.createElement(_with_stream_items.WithStreamItems, {
      initializeOnMount: !isAutoReloading
    }, function (_ref4) {
      var currentHighlightKey = _ref4.currentHighlightKey,
          hasMoreAfterEnd = _ref4.hasMoreAfterEnd,
          hasMoreBeforeStart = _ref4.hasMoreBeforeStart,
          isLoadingMore = _ref4.isLoadingMore,
          isReloading = _ref4.isReloading,
          items = _ref4.items,
          lastLoadedTime = _ref4.lastLoadedTime,
          loadNewerEntries = _ref4.loadNewerEntries;
      return _react.default.createElement(_log_text_stream.ScrollableLogTextStreamView, {
        columnConfigurations: source && source.configuration.logColumns || [],
        hasMoreAfterEnd: hasMoreAfterEnd,
        hasMoreBeforeStart: hasMoreBeforeStart,
        isLoadingMore: isLoadingMore,
        isReloading: isReloading,
        isStreaming: isAutoReloading,
        items: items,
        jumpToTarget: jumpToTargetPosition,
        lastLoadedTime: lastLoadedTime,
        loadNewerItems: loadNewerEntries,
        reportVisibleInterval: reportVisiblePositions,
        scale: textScale,
        target: targetPosition,
        wrap: textWrap,
        setFlyoutItem: setFlyoutId,
        setFlyoutVisibility: setFlyoutVisibility,
        highlightedItem: surroundingLogsId ? surroundingLogsId : null,
        currentHighlightKey: currentHighlightKey
      });
    });
  }), _react.default.createElement(_auto_sizer.AutoSizer, {
    content: true,
    bounds: true,
    detectAnyWindowResize: "height"
  }, function (_ref5) {
    var measureRef = _ref5.measureRef,
        _ref5$bounds$height = _ref5.bounds.height,
        height = _ref5$bounds$height === void 0 ? 0 : _ref5$bounds$height,
        _ref5$content$width = _ref5.content.width,
        width = _ref5$content$width === void 0 ? 0 : _ref5$content$width;
    return _react.default.createElement(LogPageMinimapColumn, {
      innerRef: measureRef
    }, _react.default.createElement(_log_summary.WithSummary, null, function (_ref6) {
      var buckets = _ref6.buckets;
      return _react.default.createElement(_with_log_position.WithLogPosition, null, function (_ref7) {
        var isAutoReloading = _ref7.isAutoReloading,
            jumpToTargetPosition = _ref7.jumpToTargetPosition,
            visibleMidpointTime = _ref7.visibleMidpointTime,
            visibleTimeInterval = _ref7.visibleTimeInterval;
        return _react.default.createElement(_with_stream_items.WithStreamItems, {
          initializeOnMount: !isAutoReloading
        }, function (_ref8) {
          var isReloading = _ref8.isReloading;
          return _react.default.createElement(_log_minimap.LogMinimap, {
            height: height,
            width: width,
            highlightedInterval: isReloading ? null : visibleTimeInterval,
            intervalSize: intervalSize,
            jumpToTarget: jumpToTargetPosition,
            summaryBuckets: buckets,
            summaryHighlightBuckets: logSummaryHighlights.length > 0 ? logSummaryHighlights[0].buckets : [],
            target: visibleMidpointTime
          });
        });
      });
    }));
  })));
};

exports.LogsPageLogsContent = LogsPageLogsContent;

var LogPageMinimapColumn = _eui_styled_components.default.div(_templateObject());