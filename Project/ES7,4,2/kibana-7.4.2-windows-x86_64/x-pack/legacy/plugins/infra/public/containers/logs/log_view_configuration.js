"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.availableIntervalSizes = exports.availableTextScales = exports.LogViewConfiguration = exports.useLogViewConfiguration = void 0;

var _i18n = require("@kbn/i18n");

var _constateLatest = _interopRequireDefault(require("constate-latest"));

var _react = require("react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogViewConfiguration = function useLogViewConfiguration() {
  // text scale
  var _useState = (0, _react.useState)('medium'),
      _useState2 = _slicedToArray(_useState, 2),
      textScale = _useState2[0],
      setTextScale = _useState2[1]; // text wrap


  var _useState3 = (0, _react.useState)(true),
      _useState4 = _slicedToArray(_useState3, 2),
      textWrap = _useState4[0],
      setTextWrap = _useState4[1]; // minimap interval


  var _useState5 = (0, _react.useState)(1000 * 60 * 60 * 24),
      _useState6 = _slicedToArray(_useState5, 2),
      intervalSize = _useState6[0],
      setIntervalSize = _useState6[1];

  return {
    availableIntervalSizes: availableIntervalSizes,
    availableTextScales: availableTextScales,
    setTextScale: setTextScale,
    setTextWrap: setTextWrap,
    textScale: textScale,
    textWrap: textWrap,
    intervalSize: intervalSize,
    setIntervalSize: setIntervalSize
  };
};

exports.useLogViewConfiguration = useLogViewConfiguration;
var LogViewConfiguration = (0, _constateLatest.default)(useLogViewConfiguration);
/**
 * constants
 */

exports.LogViewConfiguration = LogViewConfiguration;
var availableTextScales = ['large', 'medium', 'small'];
exports.availableTextScales = availableTextScales;
var availableIntervalSizes = [{
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneYearLabel', {
    defaultMessage: '1 Year'
  }),
  intervalSize: 1000 * 60 * 60 * 24 * 365
}, {
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneMonthLabel', {
    defaultMessage: '1 Month'
  }),
  intervalSize: 1000 * 60 * 60 * 24 * 30
}, {
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneWeekLabel', {
    defaultMessage: '1 Week'
  }),
  intervalSize: 1000 * 60 * 60 * 24 * 7
}, {
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneDayLabel', {
    defaultMessage: '1 Day'
  }),
  intervalSize: 1000 * 60 * 60 * 24
}, {
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneHourLabel', {
    defaultMessage: '1 Hour'
  }),
  intervalSize: 1000 * 60 * 60
}, {
  label: _i18n.i18n.translate('xpack.infra.mapLogs.oneMinuteLabel', {
    defaultMessage: '1 Minute'
  }),
  intervalSize: 1000 * 60
}];
exports.availableIntervalSizes = availableIntervalSizes;