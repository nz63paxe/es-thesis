"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogsPageProviders = void 0;

var _react = _interopRequireWildcard(require("react"));

var _log_flyout = require("../../../containers/logs/log_flyout");

var _log_view_configuration = require("../../../containers/logs/log_view_configuration");

var _log_highlights = require("../../../containers/logs/log_highlights/log_highlights");

var _source = require("../../../containers/source");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LogsPageProviders = function LogsPageProviders(_ref) {
  var children = _ref.children;

  var _useContext = (0, _react.useContext)(_source.Source.Context),
      sourceId = _useContext.sourceId,
      version = _useContext.version;

  return _react.default.createElement(_log_view_configuration.LogViewConfiguration.Provider, null, _react.default.createElement(_log_flyout.LogFlyout.Provider, null, _react.default.createElement(_log_highlights.LogHighlightsState.Provider, {
    sourceId: sourceId,
    sourceVersion: version
  }, children)));
};

exports.LogsPageProviders = LogsPageProviders;