"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sharedSelectors = exports.logEntriesSelectors = exports.waffleOptionsSelectors = exports.waffleTimeSelectors = exports.waffleFilterSelectors = exports.logPositionSelectors = exports.logFilterSelectors = void 0;

var _reselect = require("reselect");

var _log_entry = require("../utils/log_entry");

var _typed_redux = require("../utils/typed_redux");

var _local = require("./local");

var _remote = require("./remote");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * local selectors
 */
var selectLocal = function selectLocal(state) {
  return state.local;
};

var logFilterSelectors = (0, _typed_redux.globalizeSelectors)(selectLocal, _local.logFilterSelectors);
exports.logFilterSelectors = logFilterSelectors;
var logPositionSelectors = (0, _typed_redux.globalizeSelectors)(selectLocal, _local.logPositionSelectors);
exports.logPositionSelectors = logPositionSelectors;
var waffleFilterSelectors = (0, _typed_redux.globalizeSelectors)(selectLocal, _local.waffleFilterSelectors);
exports.waffleFilterSelectors = waffleFilterSelectors;
var waffleTimeSelectors = (0, _typed_redux.globalizeSelectors)(selectLocal, _local.waffleTimeSelectors);
exports.waffleTimeSelectors = waffleTimeSelectors;
var waffleOptionsSelectors = (0, _typed_redux.globalizeSelectors)(selectLocal, _local.waffleOptionsSelectors);
/**
 * remote selectors
 */

exports.waffleOptionsSelectors = waffleOptionsSelectors;

var selectRemote = function selectRemote(state) {
  return state.remote;
};

var logEntriesSelectors = (0, _typed_redux.globalizeSelectors)(selectRemote, _remote.logEntriesSelectors);
/**
 * shared selectors
 */

exports.logEntriesSelectors = logEntriesSelectors;
var sharedSelectors = {
  selectFirstVisibleLogEntry: (0, _reselect.createSelector)(logEntriesSelectors.selectEntries, logPositionSelectors.selectFirstVisiblePosition, function (entries, firstVisiblePosition) {
    return firstVisiblePosition ? (0, _log_entry.getLogEntryAtTime)(entries, firstVisiblePosition) : null;
  }),
  selectMiddleVisibleLogEntry: (0, _reselect.createSelector)(logEntriesSelectors.selectEntries, logPositionSelectors.selectMiddleVisiblePosition, function (entries, middleVisiblePosition) {
    return middleVisiblePosition ? (0, _log_entry.getLogEntryAtTime)(entries, middleVisiblePosition) : null;
  }),
  selectLastVisibleLogEntry: (0, _reselect.createSelector)(logEntriesSelectors.selectEntries, logPositionSelectors.selectLastVisiblePosition, function (entries, lastVisiblePosition) {
    return lastVisiblePosition ? (0, _log_entry.getLogEntryAtTime)(entries, lastVisiblePosition) : null;
  })
};
exports.sharedSelectors = sharedSelectors;