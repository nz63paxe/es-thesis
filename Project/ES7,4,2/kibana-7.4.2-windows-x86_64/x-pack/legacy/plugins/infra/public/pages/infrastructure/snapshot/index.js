"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SnapshotPage = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _react3 = require("ui/capabilities/react");

var _page_content = require("./page_content");

var _toolbar = require("./toolbar");

var _document_title = require("../../../components/document_title");

var _no_indices = require("../../../components/empty_states/no_indices");

var _page = require("../../../components/page");

var _source_error_page = require("../../../components/source_error_page");

var _source_loading_page = require("../../../components/source_loading_page");

var _source_configuration = require("../../../components/source_configuration");

var _source = require("../../../containers/source");

var _with_waffle_filters = require("../../../containers/waffle/with_waffle_filters");

var _with_waffle_options = require("../../../containers/waffle/with_waffle_options");

var _with_waffle_time = require("../../../containers/waffle/with_waffle_time");

var _with_kibana_chrome = require("../../../containers/with_kibana_chrome");

var _use_track_metric = require("../../../hooks/use_track_metric");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SnapshotPage = (0, _react3.injectUICapabilities)((0, _react.injectI18n)(function (props) {
  var intl = props.intl,
      uiCapabilities = props.uiCapabilities;

  var _useContext = (0, _react2.useContext)(_source.Source.Context),
      createDerivedIndexPattern = _useContext.createDerivedIndexPattern,
      hasFailedLoadingSource = _useContext.hasFailedLoadingSource,
      isLoading = _useContext.isLoading,
      loadSourceFailureMessage = _useContext.loadSourceFailureMessage,
      loadSource = _useContext.loadSource,
      metricIndicesExist = _useContext.metricIndicesExist;

  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_metrics',
    path: 'inventory'
  });
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_metrics',
    path: 'inventory',
    delay: 15000
  });
  return _react2.default.createElement(_page.ColumnarPage, null, _react2.default.createElement(_document_title.DocumentTitle, {
    title: function title(previousTitle) {
      return intl.formatMessage({
        id: 'xpack.infra.infrastructureSnapshotPage.documentTitle',
        defaultMessage: '{previousTitle} | Inventory'
      }, {
        previousTitle: previousTitle
      });
    }
  }), isLoading ? _react2.default.createElement(_source_loading_page.SourceLoadingPage, null) : metricIndicesExist ? _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_with_waffle_time.WithWaffleTimeUrlState, null), _react2.default.createElement(_with_waffle_filters.WithWaffleFilterUrlState, {
    indexPattern: createDerivedIndexPattern('metrics')
  }), _react2.default.createElement(_with_waffle_options.WithWaffleOptionsUrlState, null), _react2.default.createElement(_toolbar.SnapshotToolbar, null), _react2.default.createElement(_page_content.SnapshotPageContent, null)) : hasFailedLoadingSource ? _react2.default.createElement(_source_error_page.SourceErrorPage, {
    errorMessage: loadSourceFailureMessage || '',
    retry: loadSource
  }) : _react2.default.createElement(_with_kibana_chrome.WithKibanaChrome, null, function (_ref) {
    var basePath = _ref.basePath;
    return _react2.default.createElement(_no_indices.NoIndices, {
      title: intl.formatMessage({
        id: 'xpack.infra.homePage.noMetricsIndicesTitle',
        defaultMessage: "Looks like you don't have any metrics indices."
      }),
      message: intl.formatMessage({
        id: 'xpack.infra.homePage.noMetricsIndicesDescription',
        defaultMessage: "Let's add some!"
      }),
      actions: _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiButton, {
        href: "".concat(basePath, "/app/kibana#/home/tutorial_directory/metrics"),
        color: "primary",
        fill: true,
        "data-test-subj": "infrastructureViewSetupInstructionsButton"
      }, intl.formatMessage({
        id: 'xpack.infra.homePage.noMetricsIndicesInstructionsActionLabel',
        defaultMessage: 'View setup instructions'
      }))), uiCapabilities.infrastructure.configureSource ? _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_source_configuration.ViewSourceConfigurationButton, {
        "data-test-subj": "configureSourceButton",
        hrefBase: _source_configuration.ViewSourceConfigurationButtonHrefBase.infrastructure
      }, intl.formatMessage({
        id: 'xpack.infra.configureSourceActionLabel',
        defaultMessage: 'Change source configuration'
      }))) : null),
      "data-test-subj": "noMetricsIndicesPrompt"
    });
  }));
}));
exports.SnapshotPage = SnapshotPage;