"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useReduxBridgeSetters = void 0;

var _react = require("react");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useReduxBridgeSetters = function useReduxBridgeSetters() {
  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      startKey = _useState2[0],
      setStartKey = _useState2[1];

  var _useState3 = (0, _react.useState)(null),
      _useState4 = _slicedToArray(_useState3, 2),
      endKey = _useState4[0],
      setEndKey = _useState4[1];

  var _useState5 = (0, _react.useState)(null),
      _useState6 = _slicedToArray(_useState5, 2),
      filterQuery = _useState6[0],
      setFilterQuery = _useState6[1];

  var _useState7 = (0, _react.useState)(null),
      _useState8 = _slicedToArray(_useState7, 2),
      visibleMidpoint = _useState8[0],
      setVisibleMidpoint = _useState8[1];

  var _useState9 = (0, _react.useState)(function () {
    return undefined;
  }),
      _useState10 = _slicedToArray(_useState9, 2),
      jumpToTarget = _useState10[0],
      setJumpToTarget = _useState10[1];

  return {
    startKey: startKey,
    endKey: endKey,
    filterQuery: filterQuery,
    visibleMidpoint: visibleMidpoint,
    setStartKey: setStartKey,
    setEndKey: setEndKey,
    setFilterQuery: setFilterQuery,
    setVisibleMidpoint: setVisibleMidpoint,
    jumpToTarget: jumpToTarget,
    setJumpToTarget: setJumpToTarget
  };
};

exports.useReduxBridgeSetters = useReduxBridgeSetters;