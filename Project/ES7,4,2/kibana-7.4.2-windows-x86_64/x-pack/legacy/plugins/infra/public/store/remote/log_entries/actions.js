"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.reloadEntries = exports.loadNewerEntries = exports.loadMoreEntries = exports.loadEntries = exports.setSourceId = void 0;

var _typescriptFsa = _interopRequireDefault(require("typescript-fsa"));

var _load = require("./operations/load");

var _load_more = require("./operations/load_more");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var actionCreator = (0, _typescriptFsa.default)('x-pack/infra/remote/log_entries');
var setSourceId = actionCreator('SET_SOURCE_ID');
exports.setSourceId = setSourceId;
var loadEntries = _load.loadEntriesActionCreators.resolve;
exports.loadEntries = loadEntries;
var loadMoreEntries = _load_more.loadMoreEntriesActionCreators.resolve;
exports.loadMoreEntries = loadMoreEntries;
var loadNewerEntries = actionCreator('LOAD_NEWER_LOG_ENTRIES');
exports.loadNewerEntries = loadNewerEntries;
var reloadEntries = actionCreator('RELOAD_LOG_ENTRIES');
exports.reloadEntries = reloadEntries;