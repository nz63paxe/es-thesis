"use strict";

var _feature_catalogue = require("ui/registry/feature_catalogue");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var APP_ID = 'infra';

_feature_catalogue.FeatureCatalogueRegistryProvider.register(function (i18n) {
  return {
    id: 'infraops',
    title: i18n('xpack.infra.registerFeatures.infraOpsTitle', {
      defaultMessage: 'Infrastructure'
    }),
    description: i18n('xpack.infra.registerFeatures.infraOpsDescription', {
      defaultMessage: 'Explore infrastructure metrics and logs for common servers, containers, and services.'
    }),
    icon: 'infraApp',
    path: "/app/".concat(APP_ID, "#infrastructure"),
    showOnHomePage: true,
    category: _feature_catalogue.FeatureCatalogueCategory.DATA
  };
});

_feature_catalogue.FeatureCatalogueRegistryProvider.register(function (i18n) {
  return {
    id: 'infralogging',
    title: i18n('xpack.infra.registerFeatures.logsTitle', {
      defaultMessage: 'Logs'
    }),
    description: i18n('xpack.infra.registerFeatures.logsDescription', {
      defaultMessage: 'Stream logs in real time or scroll through historical views in a console-like experience.'
    }),
    icon: 'loggingApp',
    path: "/app/".concat(APP_ID, "#logs"),
    showOnHomePage: true,
    category: _feature_catalogue.FeatureCatalogueCategory.DATA
  };
});