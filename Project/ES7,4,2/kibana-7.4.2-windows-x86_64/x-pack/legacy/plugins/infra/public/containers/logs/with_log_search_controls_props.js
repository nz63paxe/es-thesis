"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withLogSearchControlsProps = void 0;

var _reactRedux = require("react-redux");

var _typed_redux = require("../../utils/typed_redux");

var _store = require("../../store");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Temporary Workaround
 * This is not a well-designed container. It only exists to enable quick
 * migration of the redux-based logging ui into the infra-ui codebase. It will
 * be removed during the refactoring to graphql/apollo.
 */
var withLogSearchControlsProps = (0, _reactRedux.connect)(function (state) {
  return {// isLoadingSearchResults: searchResultsSelectors.selectIsLoadingSearchResults(state),
    // nextSearchResult: sharedSelectors.selectNextSearchResultKey(state),
    // previousSearchResult: sharedSelectors.selectPreviousSearchResultKey(state),
  };
}, (0, _typed_redux.bindPlainActionCreators)({
  // clearSearch: searchActions.clearSearch,
  jumpToTarget: _store.logPositionActions.jumpToTargetPosition // search: searchActions.search,

}));
exports.withLogSearchControlsProps = withLogSearchControlsProps;