"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CreateMLJobsButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var CreateMLJobsButton = function CreateMLJobsButton(_ref) {
  var isLoading = _ref.isLoading,
      onClick = _ref.onClick;
  return _react.default.createElement(_eui.EuiButton, {
    fill: true,
    isLoading: isLoading,
    iconSide: "right",
    iconType: "check",
    onClick: onClick
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.createMlJobButton",
    defaultMessage: "Create ML Job"
  }));
};

exports.CreateMLJobsButton = CreateMLJobsButton;