"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logEntriesSelectors = void 0;

var _typed_redux = require("../../utils/typed_redux");

var _log_entries = require("./log_entries");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var logEntriesSelectors = (0, _typed_redux.globalizeSelectors)(function (state) {
  return state.logEntries;
}, _log_entries.logEntriesSelectors);
exports.logEntriesSelectors = logEntriesSelectors;