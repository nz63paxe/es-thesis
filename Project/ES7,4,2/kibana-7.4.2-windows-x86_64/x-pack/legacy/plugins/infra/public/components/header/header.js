"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Header = void 0;

var _react = _interopRequireDefault(require("react"));

var _react2 = require("@kbn/i18n/react");

var _with_kibana_chrome = require("../../containers/with_kibana_chrome");

var _external_header = require("./external_header");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var Header = (0, _react2.injectI18n)(function (_ref) {
  var _ref$breadcrumbs = _ref.breadcrumbs,
      breadcrumbs = _ref$breadcrumbs === void 0 ? [] : _ref$breadcrumbs,
      _ref$readOnlyBadge = _ref.readOnlyBadge,
      readOnlyBadge = _ref$readOnlyBadge === void 0 ? false : _ref$readOnlyBadge,
      intl = _ref.intl;
  return _react.default.createElement(_with_kibana_chrome.WithKibanaChrome, null, function (_ref2) {
    var setBreadcrumbs = _ref2.setBreadcrumbs,
        setBadge = _ref2.setBadge;
    return _react.default.createElement(_external_header.ExternalHeader, {
      breadcrumbs: breadcrumbs,
      setBreadcrumbs: setBreadcrumbs,
      badge: readOnlyBadge ? {
        text: intl.formatMessage({
          defaultMessage: 'Read only',
          id: 'xpack.infra.header.badge.readOnly.text'
        }),
        tooltip: intl.formatMessage({
          defaultMessage: 'Unable to change source configuration',
          id: 'xpack.infra.header.badge.readOnly.tooltip'
        }),
        iconType: 'glasses'
      } : undefined,
      setBadge: setBadge
    });
  });
});
exports.Header = Header;