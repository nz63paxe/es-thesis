"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RedirectToLogs = void 0;

var _react = require("@kbn/i18n/react");

var _compose = _interopRequireDefault(require("lodash/fp/compose"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _with_log_filter = require("../../containers/logs/with_log_filter");

var _with_log_position = require("../../containers/logs/with_log_position");

var _source_id = require("../../containers/source_id");

var _query_params = require("./query_params");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var RedirectToLogs = (0, _react.injectI18n)(function (_ref) {
  var location = _ref.location,
      match = _ref.match;
  var sourceId = match.params.sourceId || 'default';
  var filter = (0, _query_params.getFilterFromLocation)(location);
  var searchString = (0, _compose.default)((0, _with_log_filter.replaceLogFilterInQueryString)(filter), (0, _with_log_position.replaceLogPositionInQueryString)((0, _query_params.getTimeFromLocation)(location)), (0, _source_id.replaceSourceIdInQueryString)(sourceId))('');
  return _react2.default.createElement(_reactRouterDom.Redirect, {
    to: "/logs/stream?".concat(searchString)
  });
});
exports.RedirectToLogs = RedirectToLogs;