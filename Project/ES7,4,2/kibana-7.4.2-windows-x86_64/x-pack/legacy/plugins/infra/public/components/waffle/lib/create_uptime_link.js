"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createUptimeLink = void 0;

var _lodash = require("lodash");

var _types = require("../../../graphql/types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var BASE_URL = '../app/uptime#/?search=';

var createUptimeLink = function createUptimeLink(options, nodeType, node) {
  if (nodeType === _types.InfraNodeType.host && node.ip) {
    return "".concat(BASE_URL, "host.ip:\"").concat(node.ip, "\"");
  }

  var field = (0, _lodash.get)(options, ['fields', nodeType], '');
  return "".concat(BASE_URL).concat(field ? field + ':' : '', "\"").concat(node.id, "\"");
};

exports.createUptimeLink = createUptimeLink;