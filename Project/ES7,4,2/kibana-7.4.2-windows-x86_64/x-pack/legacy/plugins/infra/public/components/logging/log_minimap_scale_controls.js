"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogMinimapScaleControls = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LogMinimapScaleControls =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(LogMinimapScaleControls, _React$PureComponent);

  function LogMinimapScaleControls() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, LogMinimapScaleControls);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(LogMinimapScaleControls)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "handleScaleChange", function (intervalSizeDescriptorKey) {
      var _this$props = _this.props,
          availableIntervalSizes = _this$props.availableIntervalSizes,
          setIntervalSize = _this$props.setIntervalSize;

      var _availableIntervalSiz = availableIntervalSizes.filter(intervalKeyEquals(intervalSizeDescriptorKey)),
          _availableIntervalSiz2 = _slicedToArray(_availableIntervalSiz, 1),
          sizeDescriptor = _availableIntervalSiz2[0];

      if (sizeDescriptor) {
        setIntervalSize(sizeDescriptor.intervalSize);
      }
    });

    return _this;
  }

  _createClass(LogMinimapScaleControls, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          availableIntervalSizes = _this$props2.availableIntervalSizes,
          intervalSize = _this$props2.intervalSize;

      var _availableIntervalSiz3 = availableIntervalSizes.filter(intervalSizeEquals(intervalSize)),
          _availableIntervalSiz4 = _slicedToArray(_availableIntervalSiz3, 1),
          currentSizeDescriptor = _availableIntervalSiz4[0];

      return React.createElement(_eui.EuiFormRow, {
        label: React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.customizeLogs.minimapScaleFormRowLabel",
          defaultMessage: "Minimap Scale"
        })
      }, React.createElement(_eui.EuiRadioGroup, {
        options: availableIntervalSizes.map(function (sizeDescriptor) {
          return {
            id: getIntervalSizeDescriptorKey(sizeDescriptor),
            label: sizeDescriptor.label
          };
        }),
        onChange: this.handleScaleChange,
        idSelected: getIntervalSizeDescriptorKey(currentSizeDescriptor)
      }));
    }
  }]);

  return LogMinimapScaleControls;
}(React.PureComponent);

exports.LogMinimapScaleControls = LogMinimapScaleControls;

var getIntervalSizeDescriptorKey = function getIntervalSizeDescriptorKey(sizeDescriptor) {
  return "".concat(sizeDescriptor.intervalSize);
};

var intervalKeyEquals = function intervalKeyEquals(key) {
  return function (sizeDescriptor) {
    return getIntervalSizeDescriptorKey(sizeDescriptor) === key;
  };
};

var intervalSizeEquals = function intervalSizeEquals(size) {
  return function (sizeDescriptor) {
    return sizeDescriptor.intervalSize === size;
  };
};