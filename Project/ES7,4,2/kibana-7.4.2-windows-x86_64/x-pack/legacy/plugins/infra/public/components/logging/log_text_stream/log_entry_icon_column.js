"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogEntryDetailsIconColumn = exports.LogEntryIconColumn = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _log_entry_column = require("./log_entry_column");

var _text_styles = require("./text_styles");

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  position: absolute;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  background-color: ", ";\n  overflow: hidden;\n  user-select: none;\n\n  ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var LogEntryIconColumn = function LogEntryIconColumn(_ref) {
  var children = _ref.children,
      isHighlighted = _ref.isHighlighted,
      isHovered = _ref.isHovered;
  return _react2.default.createElement(IconColumnContent, {
    isHighlighted: isHighlighted,
    isHovered: isHovered
  }, children);
};

exports.LogEntryIconColumn = LogEntryIconColumn;
var LogEntryDetailsIconColumn = (0, _react.injectI18n)(function (_ref2) {
  var intl = _ref2.intl,
      isHighlighted = _ref2.isHighlighted,
      isHovered = _ref2.isHovered,
      openFlyout = _ref2.openFlyout;
  var label = intl.formatMessage({
    id: 'xpack.infra.logEntryItemView.viewDetailsToolTip',
    defaultMessage: 'View Details'
  });
  return _react2.default.createElement(LogEntryIconColumn, {
    isHighlighted: isHighlighted,
    isHovered: isHovered
  }, isHovered ? _react2.default.createElement(AbsoluteIconButtonWrapper, null, _react2.default.createElement(_eui.EuiButtonIcon, {
    onClick: openFlyout,
    iconType: "expand",
    title: label,
    "aria-label": label
  })) : null);
});
exports.LogEntryDetailsIconColumn = LogEntryDetailsIconColumn;

var IconColumnContent = _log_entry_column.LogEntryColumnContent.extend.attrs({})(_templateObject(), function (props) {
  return props.theme.eui.euiColorEmptyShade;
}, function (props) {
  return props.isHovered || props.isHighlighted ? _text_styles.hoveredContentStyle : '';
}); // this prevents the button from influencing the line height


var AbsoluteIconButtonWrapper = _eui_styled_components.default.div(_templateObject2());