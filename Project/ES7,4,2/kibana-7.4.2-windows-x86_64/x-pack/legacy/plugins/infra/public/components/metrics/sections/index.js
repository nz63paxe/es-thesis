"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.sections = void 0;

var _types = require("../../../pages/metrics/layouts/types");

var _gauges_section = require("./gauges_section");

var _chart_section = require("./chart_section");

var _sections;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var sections = (_sections = {}, _defineProperty(_sections, _types.InfraMetricLayoutSectionType.chart, _chart_section.ChartSection), _defineProperty(_sections, _types.InfraMetricLayoutSectionType.gauges, _gauges_section.GaugesSection), _sections);
exports.sections = sections;