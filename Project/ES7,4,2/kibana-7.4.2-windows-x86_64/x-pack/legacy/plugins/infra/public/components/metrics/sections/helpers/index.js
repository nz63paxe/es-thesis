"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getChartType = exports.getChartColor = exports.getChartName = exports.getMaxMinTimestamp = exports.seriesHasLessThen2DataPoints = exports.getFormatter = void 0;

var _color = _interopRequireDefault(require("color"));

var _lodash = require("lodash");

var _formatters = require("../../../../utils/formatters");

var _types = require("../../../../pages/metrics/layouts/types");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * Returns a formatter
 */
var getFormatter = function getFormatter(formatter, template) {
  return function (val) {
    return val != null ? (0, _formatters.createFormatter)(formatter, template)(val) : '';
  };
};
/**
 * Does a series have more then two points?
 */


exports.getFormatter = getFormatter;

var seriesHasLessThen2DataPoints = function seriesHasLessThen2DataPoints(series) {
  return series.data.length < 2;
};
/**
 * Returns the minimum and maximum timestamp for a metric
 */


exports.seriesHasLessThen2DataPoints = seriesHasLessThen2DataPoints;

var getMaxMinTimestamp = function getMaxMinTimestamp(metric) {
  if (metric.series.some(seriesHasLessThen2DataPoints)) {
    return [0, 0];
  }

  var values = metric.series.reduce(function (acc, item) {
    var firstRow = (0, _lodash.first)(item.data);
    var lastRow = (0, _lodash.last)(item.data);
    return acc.concat([firstRow && firstRow.timestamp || 0, lastRow && lastRow.timestamp || 0]);
  }, []);
  return [(0, _lodash.min)(values), (0, _lodash.max)(values)];
};
/**
 * Returns the chart name from the visConfig based on the series id, otherwise it
 * just returns the seriesId
 */


exports.getMaxMinTimestamp = getMaxMinTimestamp;

var getChartName = function getChartName(section, seriesId) {
  return (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', seriesId, 'name'], seriesId);
};
/**
 * Returns the chart color from the visConfig based on the series id, otherwise it
 * just returns a default color of #999
 */


exports.getChartName = getChartName;

var getChartColor = function getChartColor(section, seriesId) {
  var color = new _color.default((0, _lodash.get)(section, ['visConfig', 'seriesOverrides', seriesId, 'color'], '#999'));
  return color.hex().toString();
};
/**
 * Type guard for InfraMetricLayoutVisualizationType
 */


exports.getChartColor = getChartColor;

var isInfraMetricLayoutVisualizationType = function isInfraMetricLayoutVisualizationType(subject) {
  return _types.InfraMetricLayoutVisualizationType[subject] != null;
};
/**
 * Gets the chart type based on the section and seriesId
 */


var getChartType = function getChartType(section, seriesId) {
  var value = (0, _lodash.get)(section, ['visConfig', 'type']);
  var overrideValue = (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', seriesId, 'type']);

  if (isInfraMetricLayoutVisualizationType(overrideValue)) {
    return overrideValue;
  }

  if (isInfraMetricLayoutVisualizationType(value)) {
    return value;
  }

  return _types.InfraMetricLayoutVisualizationType.line;
};

exports.getChartType = getChartType;