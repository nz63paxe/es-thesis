"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectIsLogFilterQueryDraftValid = exports.selectLogFilterQueryDraft = exports.selectLogFilterQueryAsJson = exports.selectLogFilterQuery = void 0;

var _reselect = require("reselect");

var _esQuery = require("@kbn/es-query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var selectLogFilterQuery = function selectLogFilterQuery(state) {
  return state.filterQuery ? state.filterQuery.query : null;
};

exports.selectLogFilterQuery = selectLogFilterQuery;

var selectLogFilterQueryAsJson = function selectLogFilterQueryAsJson(state) {
  return state.filterQuery ? state.filterQuery.serializedQuery : null;
};

exports.selectLogFilterQueryAsJson = selectLogFilterQueryAsJson;

var selectLogFilterQueryDraft = function selectLogFilterQueryDraft(state) {
  return state.filterQueryDraft;
};

exports.selectLogFilterQueryDraft = selectLogFilterQueryDraft;
var selectIsLogFilterQueryDraftValid = (0, _reselect.createSelector)(selectLogFilterQueryDraft, function (filterQueryDraft) {
  if (filterQueryDraft && filterQueryDraft.kind === 'kuery') {
    try {
      (0, _esQuery.fromKueryExpression)(filterQueryDraft.expression);
    } catch (err) {
      return false;
    }
  }

  return true;
});
exports.selectIsLogFilterQueryDraftValid = selectIsLogFilterQueryDraftValid;