"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadEntriesEpic = exports.loadEntriesReducer = exports.loadEntriesActionCreators = void 0;

var _remote_graphql_state = require("../../../../utils/remote_state/remote_graphql_state");

var _state = require("../state");

var _log_entries = require("./log_entries.gql_query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var operationKey = 'load';
var loadEntriesActionCreators = (0, _remote_graphql_state.createGraphqlOperationActionCreators)('log_entries', operationKey);
exports.loadEntriesActionCreators = loadEntriesActionCreators;
var loadEntriesReducer = (0, _remote_graphql_state.createGraphqlOperationReducer)(operationKey, _state.initialLogEntriesState, loadEntriesActionCreators, function (state, action) {
  return action.payload.result.data.source.logEntriesAround;
}, function () {
  return {
    entries: [],
    hasMoreAfter: false,
    hasMoreBefore: false
  };
});
exports.loadEntriesReducer = loadEntriesReducer;
var loadEntriesEpic = (0, _remote_graphql_state.createGraphqlQueryEpic)(_log_entries.logEntriesQuery, loadEntriesActionCreators);
exports.loadEntriesEpic = loadEntriesEpic;