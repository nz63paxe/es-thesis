"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithLogMinimapUrlState = void 0;

var _react = _interopRequireWildcard(require("react"));

var _url_state = require("../../utils/url_state");

var _log_view_configuration = require("./log_view_configuration");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var WithLogMinimapUrlState = function WithLogMinimapUrlState() {
  var _useContext = (0, _react.useContext)(_log_view_configuration.LogViewConfiguration.Context),
      intervalSize = _useContext.intervalSize,
      setIntervalSize = _useContext.setIntervalSize;

  var urlState = (0, _react.useMemo)(function () {
    return {
      intervalSize: intervalSize
    };
  }, [intervalSize]);
  return _react.default.createElement(_url_state.UrlStateContainer, {
    urlState: urlState,
    urlStateKey: "logMinimap",
    mapToUrlState: mapToUrlState,
    onChange: function onChange(newUrlState) {
      if (newUrlState && newUrlState.intervalSize) {
        setIntervalSize(newUrlState.intervalSize);
      }
    },
    onInitialize: function onInitialize(newUrlState) {
      if (newUrlState && newUrlState.intervalSize) {
        setIntervalSize(newUrlState.intervalSize);
      }
    }
  });
};

exports.WithLogMinimapUrlState = WithLogMinimapUrlState;

var mapToUrlState = function mapToUrlState(value) {
  return value ? {
    intervalSize: mapToIntervalSizeUrlState(value.intervalSize)
  } : undefined;
};

var mapToIntervalSizeUrlState = function mapToIntervalSizeUrlState(value) {
  return value && typeof value === 'number' ? value : undefined;
};