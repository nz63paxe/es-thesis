"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogHighlightsState = exports.useLogHighlightsState = void 0;

var _constateLatest = _interopRequireDefault(require("constate-latest"));

var _react = require("react");

var _log_entry_highlights = require("./log_entry_highlights");

var _log_summary_highlights = require("./log_summary_highlights");

var _next_and_previous = require("./next_and_previous");

var _redux_bridge_setters = require("./redux_bridge_setters");

var _log_summary = require("../log_summary");

var _log_view_configuration = require("../log_view_configuration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogHighlightsState = function useLogHighlightsState(_ref) {
  var sourceId = _ref.sourceId,
      sourceVersion = _ref.sourceVersion;

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      highlightTerms = _useState2[0],
      setHighlightTerms = _useState2[1];

  var _useReduxBridgeSetter = (0, _redux_bridge_setters.useReduxBridgeSetters)(),
      startKey = _useReduxBridgeSetter.startKey,
      endKey = _useReduxBridgeSetter.endKey,
      filterQuery = _useReduxBridgeSetter.filterQuery,
      visibleMidpoint = _useReduxBridgeSetter.visibleMidpoint,
      setStartKey = _useReduxBridgeSetter.setStartKey,
      setEndKey = _useReduxBridgeSetter.setEndKey,
      setFilterQuery = _useReduxBridgeSetter.setFilterQuery,
      setVisibleMidpoint = _useReduxBridgeSetter.setVisibleMidpoint,
      jumpToTarget = _useReduxBridgeSetter.jumpToTarget,
      setJumpToTarget = _useReduxBridgeSetter.setJumpToTarget;

  var _useContext = (0, _react.useContext)(_log_view_configuration.LogViewConfiguration.Context),
      summaryIntervalSize = _useContext.intervalSize;

  var _useLogSummaryBufferI = (0, _log_summary.useLogSummaryBufferInterval)(visibleMidpoint ? visibleMidpoint.time : null, summaryIntervalSize),
      summaryStart = _useLogSummaryBufferI.start,
      summaryEnd = _useLogSummaryBufferI.end,
      summaryBucketSize = _useLogSummaryBufferI.bucketSize;

  var _useLogEntryHighlight = (0, _log_entry_highlights.useLogEntryHighlights)(sourceId, sourceVersion, startKey, endKey, filterQuery, highlightTerms),
      logEntryHighlights = _useLogEntryHighlight.logEntryHighlights,
      logEntryHighlightsById = _useLogEntryHighlight.logEntryHighlightsById,
      loadLogEntryHighlightsRequest = _useLogEntryHighlight.loadLogEntryHighlightsRequest;

  var _useLogSummaryHighlig = (0, _log_summary_highlights.useLogSummaryHighlights)(sourceId, sourceVersion, summaryStart, summaryEnd, summaryBucketSize, filterQuery, highlightTerms),
      logSummaryHighlights = _useLogSummaryHighlig.logSummaryHighlights,
      loadLogSummaryHighlightsRequest = _useLogSummaryHighlig.loadLogSummaryHighlightsRequest;

  var _useNextAndPrevious = (0, _next_and_previous.useNextAndPrevious)({
    visibleMidpoint: visibleMidpoint,
    logEntryHighlights: logEntryHighlights,
    highlightTerms: highlightTerms,
    jumpToTarget: jumpToTarget
  }),
      currentHighlightKey = _useNextAndPrevious.currentHighlightKey,
      hasPreviousHighlight = _useNextAndPrevious.hasPreviousHighlight,
      hasNextHighlight = _useNextAndPrevious.hasNextHighlight,
      goToPreviousHighlight = _useNextAndPrevious.goToPreviousHighlight,
      goToNextHighlight = _useNextAndPrevious.goToNextHighlight;

  return {
    highlightTerms: highlightTerms,
    setHighlightTerms: setHighlightTerms,
    setStartKey: setStartKey,
    setEndKey: setEndKey,
    setFilterQuery: setFilterQuery,
    logEntryHighlights: logEntryHighlights,
    logEntryHighlightsById: logEntryHighlightsById,
    logSummaryHighlights: logSummaryHighlights,
    loadLogEntryHighlightsRequest: loadLogEntryHighlightsRequest,
    loadLogSummaryHighlightsRequest: loadLogSummaryHighlightsRequest,
    setVisibleMidpoint: setVisibleMidpoint,
    currentHighlightKey: currentHighlightKey,
    hasPreviousHighlight: hasPreviousHighlight,
    hasNextHighlight: hasNextHighlight,
    goToPreviousHighlight: goToPreviousHighlight,
    goToNextHighlight: goToNextHighlight,
    setJumpToTarget: setJumpToTarget
  };
};

exports.useLogHighlightsState = useLogHighlightsState;
var LogHighlightsState = (0, _constateLatest.default)(useLogHighlightsState);
exports.LogHighlightsState = LogHighlightsState;