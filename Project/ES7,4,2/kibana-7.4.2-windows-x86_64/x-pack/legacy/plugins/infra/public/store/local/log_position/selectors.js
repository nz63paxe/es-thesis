"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectVisibleTimeInterval = exports.selectVisibleMidpointOrTargetTime = exports.selectVisibleMidpointOrTarget = exports.selectControlsShouldDisplayTargetPosition = exports.selectLastVisiblePosition = exports.selectMiddleVisiblePosition = exports.selectFirstVisiblePosition = exports.selectIsAutoReloading = exports.selectTargetPosition = void 0;

var _reselect = require("reselect");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var selectTargetPosition = function selectTargetPosition(state) {
  return state.targetPosition;
};

exports.selectTargetPosition = selectTargetPosition;

var selectIsAutoReloading = function selectIsAutoReloading(state) {
  return state.updatePolicy.policy === 'interval';
};

exports.selectIsAutoReloading = selectIsAutoReloading;

var selectFirstVisiblePosition = function selectFirstVisiblePosition(state) {
  return state.visiblePositions.startKey ? state.visiblePositions.startKey : null;
};

exports.selectFirstVisiblePosition = selectFirstVisiblePosition;

var selectMiddleVisiblePosition = function selectMiddleVisiblePosition(state) {
  return state.visiblePositions.middleKey ? state.visiblePositions.middleKey : null;
};

exports.selectMiddleVisiblePosition = selectMiddleVisiblePosition;

var selectLastVisiblePosition = function selectLastVisiblePosition(state) {
  return state.visiblePositions.endKey ? state.visiblePositions.endKey : null;
};

exports.selectLastVisiblePosition = selectLastVisiblePosition;

var selectControlsShouldDisplayTargetPosition = function selectControlsShouldDisplayTargetPosition(state) {
  return state.controlsShouldDisplayTargetPosition;
};

exports.selectControlsShouldDisplayTargetPosition = selectControlsShouldDisplayTargetPosition;
var selectVisibleMidpointOrTarget = (0, _reselect.createSelector)(selectMiddleVisiblePosition, selectTargetPosition, selectControlsShouldDisplayTargetPosition, function (middleVisiblePosition, targetPosition, displayTargetPosition) {
  if (displayTargetPosition) {
    return targetPosition;
  } else if (middleVisiblePosition) {
    return middleVisiblePosition;
  } else if (targetPosition) {
    return targetPosition;
  } else {
    return null;
  }
});
exports.selectVisibleMidpointOrTarget = selectVisibleMidpointOrTarget;
var selectVisibleMidpointOrTargetTime = (0, _reselect.createSelector)(selectVisibleMidpointOrTarget, function (visibleMidpointOrTarget) {
  return visibleMidpointOrTarget ? visibleMidpointOrTarget.time : null;
});
exports.selectVisibleMidpointOrTargetTime = selectVisibleMidpointOrTargetTime;
var selectVisibleTimeInterval = (0, _reselect.createSelector)(selectFirstVisiblePosition, selectLastVisiblePosition, function (firstVisiblePosition, lastVisiblePosition) {
  return firstVisiblePosition && lastVisiblePosition ? {
    start: firstVisiblePosition.time,
    end: lastVisiblePosition.time
  } : null;
});
exports.selectVisibleTimeInterval = selectVisibleTimeInterval;