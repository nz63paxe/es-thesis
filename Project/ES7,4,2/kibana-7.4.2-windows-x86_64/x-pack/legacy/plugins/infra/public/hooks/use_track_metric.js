"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useTrackMetric = useTrackMetric;
exports.useTrackPageview = useTrackPageview;
exports.trackEvent = trackEvent;
Object.defineProperty(exports, "METRIC_TYPE", {
  enumerable: true,
  get: function get() {
    return _public.METRIC_TYPE;
  }
});

var _react = require("react");

var _public = require("../../../../../../src/legacy/core_plugins/ui_metric/public");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var trackerCache = new Map();

function getTrackerForApp(app) {
  var cached = trackerCache.get(app);

  if (cached) {
    return cached;
  }

  var tracker = (0, _public.createUiStatsReporter)(app);
  trackerCache.set(app, tracker);
  return tracker;
}

function useTrackMetric(_ref) {
  var app = _ref.app,
      metric = _ref.metric,
      _ref$metricType = _ref.metricType,
      metricType = _ref$metricType === void 0 ? _public.METRIC_TYPE.COUNT : _ref$metricType,
      _ref$delay = _ref.delay,
      delay = _ref$delay === void 0 ? 0 : _ref$delay;
  var effectDependencies = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  (0, _react.useEffect)(function () {
    var decoratedMetric = metric;

    if (delay > 0) {
      decoratedMetric += "__delayed_".concat(delay, "ms");
    }

    var trackUiMetric = getTrackerForApp(app);
    var id = setTimeout(function () {
      return trackUiMetric(metricType, decoratedMetric);
    }, Math.max(delay, 0));
    return function () {
      return clearTimeout(id);
    };
  }, effectDependencies);
}
/**
 * useTrackPageview is a convenience wrapper for tracking a pageview
 * Its metrics will be found at:
 * stack_stats.kibana.plugins.ui_metric.{app}.pageview__{path}(__delayed_{n}ms)?
 */


function useTrackPageview(_ref2) {
  var path = _ref2.path,
      rest = _objectWithoutProperties(_ref2, ["path"]);

  var effectDependencies = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  useTrackMetric(_objectSpread({}, rest, {
    metric: "pageview__".concat(path)
  }), effectDependencies);
}

function trackEvent(_ref3) {
  var app = _ref3.app,
      name = _ref3.name,
      _ref3$metricType = _ref3.metricType,
      metricType = _ref3$metricType === void 0 ? _public.METRIC_TYPE.CLICK : _ref3$metricType;
  var trackUiMetric = getTrackerForApp(app);
  trackUiMetric(metricType, "event__".concat(name));
}