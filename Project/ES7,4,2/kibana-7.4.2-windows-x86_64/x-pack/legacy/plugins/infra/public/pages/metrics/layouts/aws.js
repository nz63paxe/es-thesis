"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.awsLayoutCreator = void 0;

var _i18n = require("@kbn/i18n");

var _types = require("../../../graphql/types");

var _lib = require("../../../lib/lib");

var _types2 = require("./types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var awsLayoutCreator = function awsLayoutCreator(theme) {
  return [{
    id: 'awsOverview',
    label: 'AWS',
    sections: [{
      id: _types.InfraMetric.awsOverview,
      linkToId: 'awsOverview',
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.overviewSection.sectionLabel', {
        defaultMessage: 'Overview'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.gauges,
      visConfig: {
        seriesOverrides: {
          'cpu-util': {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.overviewSection.cpuUtilizationSeriesLabel', {
              defaultMessage: 'CPU Utilization'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          'status-check-failed': {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.overviewSection.statusCheckFailedLabel', {
              defaultMessage: 'Status check failed'
            }),
            color: theme.eui.euiColorFullShade
          },
          'packets-in': {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.overviewSection.networkPacketsInLabel', {
              defaultMessage: 'Packets (in)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.number
          },
          'packets-out': {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.overviewSection.networkPacketsOutLabel', {
              defaultMessage: 'Packets (out)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.number
          }
        }
      }
    }, {
      id: _types.InfraMetric.awsCpuUtilization,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.cpuUtilSection.sectionLabel', {
        defaultMessage: 'CPU Utilization'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.number,
        seriesOverrides: {
          'cpu-util': {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.cpuUtilSection.percentSeriesLabel', {
              defaultMessage: 'percent'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.awsNetworkBytes,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkBytesSection.sectionLabel', {
        defaultMessage: 'Network Traffic'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.bits,
        formatterTemplate: '{{value}}/s',
        seriesOverrides: {
          tx: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkBytesSection.txSeriesLabel', {
              defaultMessage: 'out'
            })
          },
          rx: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkBytesSection.rxSeriesLabel', {
              defaultMessage: 'in'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.awsNetworkPackets,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkPacketsSection.sectionLabel', {
        defaultMessage: 'Network Packets (Average)'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.number,
        seriesOverrides: {
          'packets-out': {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkPacketsSection.packetsOutSeriesLabel', {
              defaultMessage: 'out'
            })
          },
          'packets-in': {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.networkPacketsSection.packetsInSeriesLabel', {
              defaultMessage: 'in'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.awsDiskioOps,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioOperationsSection.sectionLabel', {
        defaultMessage: 'Disk I/O Operations'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.number,
        seriesOverrides: {
          writes: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioOperationsSection.writesSeriesLabel', {
              defaultMessage: 'writes'
            })
          },
          reads: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioOperationsSection.readsSeriesLabel', {
              defaultMessage: 'reads'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.awsDiskioBytes,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioBytesSection.sectionLabel', {
        defaultMessage: 'Disk I/O Bytes'
      }),
      requires: ['aws.ec2'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.number,
        seriesOverrides: {
          writes: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioBytesSection.writesSeriesLabel', {
              defaultMessage: 'writes'
            })
          },
          reads: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.awsMetricsLayout.diskioBytesSection.readsSeriesLabel', {
              defaultMessage: 'reads'
            })
          }
        }
      }
    }]
  }];
};

exports.awsLayoutCreator = awsLayoutCreator;