"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithSummary = void 0;

var _react = require("react");

var _reactRedux = require("react-redux");

var _store = require("../../../store");

var _source = require("../../source");

var _log_view_configuration = require("../log_view_configuration");

var _log_summary = require("./log_summary");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var WithSummary = (0, _reactRedux.connect)(function (state) {
  return {
    visibleMidpointTime: _store.logPositionSelectors.selectVisibleMidpointOrTargetTime(state),
    filterQuery: _store.logFilterSelectors.selectLogFilterQueryAsJson(state)
  };
})(function (_ref) {
  var children = _ref.children,
      filterQuery = _ref.filterQuery,
      visibleMidpointTime = _ref.visibleMidpointTime;

  var _useContext = (0, _react.useContext)(_log_view_configuration.LogViewConfiguration.Context),
      intervalSize = _useContext.intervalSize;

  var _useContext2 = (0, _react.useContext)(_source.Source.Context),
      sourceId = _useContext2.sourceId;

  var _useLogSummary = (0, _log_summary.useLogSummary)(sourceId, visibleMidpointTime, intervalSize, filterQuery),
      buckets = _useLogSummary.buckets,
      start = _useLogSummary.start,
      end = _useLogSummary.end;

  return children({
    buckets: buckets,
    start: start,
    end: end
  });
});
exports.WithSummary = WithSummary;