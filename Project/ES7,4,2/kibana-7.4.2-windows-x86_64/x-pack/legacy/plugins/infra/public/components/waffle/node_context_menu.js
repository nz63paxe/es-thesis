"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NodeContextMenu = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _react3 = require("ui/capabilities/react");

var _types = require("../../graphql/types");

var _link_to = require("../../pages/link_to");

var _create_uptime_link = require("./lib/create_uptime_link");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var NodeContextMenu = (0, _react3.injectUICapabilities)((0, _react.injectI18n)(function (_ref) {
  var _APM_FIELDS;

  var options = _ref.options,
      timeRange = _ref.timeRange,
      children = _ref.children,
      node = _ref.node,
      isPopoverOpen = _ref.isPopoverOpen,
      closePopover = _ref.closePopover,
      nodeType = _ref.nodeType,
      intl = _ref.intl,
      uiCapabilities = _ref.uiCapabilities,
      popoverPosition = _ref.popoverPosition;
  // Due to the changing nature of the fields between APM and this UI,
  // We need to have some exceptions until 7.0 & ECS is finalized. Reference
  // #26620 for the details for these fields.
  // TODO: This is tech debt, remove it after 7.0 & ECS migration.
  var APM_FIELDS = (_APM_FIELDS = {}, _defineProperty(_APM_FIELDS, _types.InfraNodeType.host, 'host.hostname'), _defineProperty(_APM_FIELDS, _types.InfraNodeType.container, 'container.id'), _defineProperty(_APM_FIELDS, _types.InfraNodeType.pod, 'kubernetes.pod.uid'), _APM_FIELDS);
  var nodeLogsMenuItem = {
    name: intl.formatMessage({
      id: 'xpack.infra.nodeContextMenu.viewLogsName',
      defaultMessage: 'View logs'
    }),
    href: (0, _link_to.getNodeLogsUrl)({
      nodeType: nodeType,
      nodeId: node.id,
      time: timeRange.to
    }),
    'data-test-subj': 'viewLogsContextMenuItem'
  };
  var nodeDetailMenuItem = {
    name: intl.formatMessage({
      id: 'xpack.infra.nodeContextMenu.viewMetricsName',
      defaultMessage: 'View metrics'
    }),
    href: (0, _link_to.getNodeDetailUrl)({
      nodeType: nodeType,
      nodeId: node.id,
      from: timeRange.from,
      to: timeRange.to
    })
  };
  var apmTracesMenuItem = {
    name: intl.formatMessage({
      id: 'xpack.infra.nodeContextMenu.viewAPMTraces',
      defaultMessage: 'View {nodeType} APM traces'
    }, {
      nodeType: nodeType
    }),
    href: "../app/apm#/traces?_g=()&kuery=".concat(APM_FIELDS[nodeType], ":\"").concat(node.id, "\""),
    'data-test-subj': 'viewApmTracesContextMenuItem'
  };
  var uptimeMenuItem = {
    name: intl.formatMessage({
      id: 'xpack.infra.nodeContextMenu.viewUptimeLink',
      defaultMessage: 'View {nodeType} in Uptime'
    }, {
      nodeType: nodeType
    }),
    href: (0, _create_uptime_link.createUptimeLink)(options, nodeType, node)
  };
  var showLogsLink = node.id && uiCapabilities.logs.show;
  var showAPMTraceLink = uiCapabilities.apm && uiCapabilities.apm.show;
  var showUptimeLink = [_types.InfraNodeType.pod, _types.InfraNodeType.container].includes(nodeType) || node.ip;
  var panels = [{
    id: 0,
    title: '',
    items: [].concat(_toConsumableArray(showLogsLink ? [nodeLogsMenuItem] : []), [nodeDetailMenuItem], _toConsumableArray(showAPMTraceLink ? [apmTracesMenuItem] : []), _toConsumableArray(showUptimeLink ? [uptimeMenuItem] : []))
  }];
  return _react2.default.createElement(_eui.EuiPopover, {
    closePopover: closePopover,
    id: "".concat(node.pathId, "-popover"),
    isOpen: isPopoverOpen,
    button: children,
    panelPaddingSize: "none",
    anchorPosition: popoverPosition
  }, _react2.default.createElement(_eui.EuiContextMenu, {
    initialPanelId: 0,
    panels: panels,
    "data-test-subj": "nodeContextMenu"
  }));
}));
exports.NodeContextMenu = NodeContextMenu;