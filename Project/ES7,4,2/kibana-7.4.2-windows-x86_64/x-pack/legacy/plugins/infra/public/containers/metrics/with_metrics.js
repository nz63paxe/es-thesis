"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithMetrics = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _metrics = require("./metrics.gql_query");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var WithMetrics = function WithMetrics(_ref) {
  var children = _ref.children,
      layouts = _ref.layouts,
      sourceId = _ref.sourceId,
      timerange = _ref.timerange,
      nodeType = _ref.nodeType,
      nodeId = _ref.nodeId,
      cloudId = _ref.cloudId;
  var metrics = layouts.reduce(function (acc, item) {
    return acc.concat(item.sections.map(function (s) {
      return s.id;
    }));
  }, []);
  return _react.default.createElement(_reactApollo.Query, {
    query: _metrics.metricsQuery,
    fetchPolicy: "no-cache",
    notifyOnNetworkStatusChange: true,
    variables: {
      sourceId: sourceId,
      metrics: metrics,
      nodeType: nodeType,
      nodeId: nodeId,
      cloudId: cloudId,
      timerange: timerange
    }
  }, function (_ref2) {
    var data = _ref2.data,
        error = _ref2.error,
        loading = _ref2.loading,
        refetch = _ref2.refetch;
    return children({
      metrics: filterOnlyInfraMetricData(data && data.source && data.source.metrics),
      error: error,
      loading: loading,
      refetch: refetch
    });
  });
};

exports.WithMetrics = WithMetrics;

var filterOnlyInfraMetricData = function filterOnlyInfraMetricData(metrics) {
  if (!metrics) {
    return [];
  }

  return metrics.filter(function (m) {
    return m !== null;
  }).map(function (m) {
    return m;
  });
};