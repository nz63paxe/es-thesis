"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogsPageNoIndicesContent = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _react3 = require("ui/capabilities/react");

var _no_indices = require("../../../components/empty_states/no_indices");

var _with_kibana_chrome = require("../../../containers/with_kibana_chrome");

var _source_configuration = require("../../../components/source_configuration");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LogsPageNoIndicesContent = (0, _react3.injectUICapabilities)((0, _react.injectI18n)(function (props) {
  var intl = props.intl,
      uiCapabilities = props.uiCapabilities;
  return _react2.default.createElement(_with_kibana_chrome.WithKibanaChrome, null, function (_ref) {
    var basePath = _ref.basePath;
    return _react2.default.createElement(_no_indices.NoIndices, {
      "data-test-subj": "noLogsIndicesPrompt",
      title: intl.formatMessage({
        id: 'xpack.infra.logsPage.noLoggingIndicesTitle',
        defaultMessage: "Looks like you don't have any logging indices."
      }),
      message: intl.formatMessage({
        id: 'xpack.infra.logsPage.noLoggingIndicesDescription',
        defaultMessage: "Let's add some!"
      }),
      actions: _react2.default.createElement(_eui.EuiFlexGroup, null, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_eui.EuiButton, {
        href: "".concat(basePath, "/app/kibana#/home/tutorial_directory/logging"),
        color: "primary",
        fill: true,
        "data-test-subj": "logsViewSetupInstructionsButton"
      }, intl.formatMessage({
        id: 'xpack.infra.logsPage.noLoggingIndicesInstructionsActionLabel',
        defaultMessage: 'View setup instructions'
      }))), uiCapabilities.logs.configureSource ? _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_source_configuration.ViewSourceConfigurationButton, {
        "data-test-subj": "configureSourceButton",
        hrefBase: _source_configuration.ViewSourceConfigurationButtonHrefBase.logs
      }, intl.formatMessage({
        id: 'xpack.infra.configureSourceActionLabel',
        defaultMessage: 'Change source configuration'
      }))) : null)
    });
  });
}));
exports.LogsPageNoIndicesContent = LogsPageNoIndicesContent;