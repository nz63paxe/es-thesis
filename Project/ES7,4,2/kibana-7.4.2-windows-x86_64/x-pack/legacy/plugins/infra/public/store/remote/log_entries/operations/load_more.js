"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadMoreEntriesEpic = exports.loadMoreEntriesReducer = exports.loadMoreEntriesActionCreators = void 0;

var _log_entry = require("../../../../utils/log_entry");

var _remote_graphql_state = require("../../../../utils/remote_state/remote_graphql_state");

var _state = require("../state");

var _log_entries = require("./log_entries.gql_query");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var operationKey = 'load_more';
var loadMoreEntriesActionCreators = (0, _remote_graphql_state.createGraphqlOperationActionCreators)('log_entries', operationKey);
exports.loadMoreEntriesActionCreators = loadMoreEntriesActionCreators;
var loadMoreEntriesReducer = (0, _remote_graphql_state.createGraphqlOperationReducer)(operationKey, _state.initialLogEntriesState, loadMoreEntriesActionCreators, function (state, action) {
  var logEntriesAround = action.payload.result.data.source.logEntriesAround;
  var newEntries = logEntriesAround.entries;
  var oldEntries = state && state.entries ? state.entries : [];
  var oldStart = state && state.start ? state.start : null;
  var oldEnd = state && state.end ? state.end : null;

  if (newEntries.length <= 0) {
    return state;
  }

  if ((action.payload.params.countBefore || 0) > 0) {
    var lastLogEntry = newEntries[newEntries.length - 1];
    var prependAtIndex = (0, _log_entry.getLogEntryIndexAfterTime)(oldEntries, (0, _log_entry.getLogEntryKey)(lastLogEntry));
    return {
      start: logEntriesAround.start,
      end: oldEnd,
      hasMoreBefore: logEntriesAround.hasMoreBefore,
      hasMoreAfter: state ? state.hasMoreAfter : logEntriesAround.hasMoreAfter,
      entries: [].concat(_toConsumableArray(newEntries), _toConsumableArray(oldEntries.slice(prependAtIndex)))
    };
  } else if ((action.payload.params.countAfter || 0) > 0) {
    var firstLogEntry = newEntries[0];
    var appendAtIndex = (0, _log_entry.getLogEntryIndexBeforeTime)(oldEntries, (0, _log_entry.getLogEntryKey)(firstLogEntry));
    return {
      start: oldStart,
      end: logEntriesAround.end,
      hasMoreBefore: state ? state.hasMoreBefore : logEntriesAround.hasMoreBefore,
      hasMoreAfter: logEntriesAround.hasMoreAfter,
      entries: [].concat(_toConsumableArray(oldEntries.slice(0, appendAtIndex)), _toConsumableArray(newEntries))
    };
  } else {
    return state;
  }
});
exports.loadMoreEntriesReducer = loadMoreEntriesReducer;
var loadMoreEntriesEpic = (0, _remote_graphql_state.createGraphqlQueryEpic)(_log_entries.logEntriesQuery, loadMoreEntriesActionCreators);
exports.loadMoreEntriesEpic = loadMoreEntriesEpic;