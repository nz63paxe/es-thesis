"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SettingsPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _react2 = require("ui/capabilities/react");

var _source_configuration_settings = require("../../../components/source_configuration/source_configuration_settings");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var SettingsPage = (0, _react2.injectUICapabilities)(function (_ref) {
  var uiCapabilities = _ref.uiCapabilities;
  return _react.default.createElement(_source_configuration_settings.SourceConfigurationSettings, {
    shouldAllowEdit: uiCapabilities.logs.configureSource
  });
});
exports.SettingsPage = SettingsPage;