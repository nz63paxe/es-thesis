"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfrastructurePage = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _react3 = require("ui/capabilities/react");

var _document_title = require("../../components/document_title");

var _help_center_content = require("../../components/help_center_content");

var _routed_tabs = require("../../components/navigation/routed_tabs");

var _page = require("../../components/page");

var _header = require("../../components/header");

var _use_metrics_explorer_options = require("../../containers/metrics_explorer/use_metrics_explorer_options");

var _with_metrics_explorer_options_url_state = require("../../containers/metrics_explorer/with_metrics_explorer_options_url_state");

var _with_source = require("../../containers/with_source");

var _source = require("../../containers/source");

var _metrics_explorer = require("./metrics_explorer");

var _snapshot = require("./snapshot");

var _settings = require("../shared/settings");

var _app_navigation = require("../../components/navigation/app_navigation");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var InfrastructurePage = (0, _react3.injectUICapabilities)((0, _react.injectI18n)(function (_ref) {
  var match = _ref.match,
      intl = _ref.intl,
      uiCapabilities = _ref.uiCapabilities;
  return _react2.default.createElement(_source.Source.Provider, {
    sourceId: "default"
  }, _react2.default.createElement(_page.ColumnarPage, null, _react2.default.createElement(_document_title.DocumentTitle, {
    title: intl.formatMessage({
      id: 'xpack.infra.homePage.documentTitle',
      defaultMessage: 'Infrastructure'
    })
  }), _react2.default.createElement(_help_center_content.HelpCenterContent, {
    feedbackLink: "https://discuss.elastic.co/c/infrastructure",
    feedbackLinkText: intl.formatMessage({
      id: 'xpack.infra.infrastructure.infrastructureHelpContent.feedbackLinkText',
      defaultMessage: 'Provide feedback for Infrastructure'
    })
  }), _react2.default.createElement(_header.Header, {
    breadcrumbs: [{
      text: intl.formatMessage({
        id: 'xpack.infra.header.infrastructureTitle',
        defaultMessage: 'Infrastructure'
      })
    }],
    readOnlyBadge: !uiCapabilities.infrastructure.save
  }), _react2.default.createElement(_app_navigation.AppNavigation, null, _react2.default.createElement(_routed_tabs.RoutedTabs, {
    tabs: [{
      title: intl.formatMessage({
        id: 'xpack.infra.homePage.inventoryTabTitle',
        defaultMessage: 'Inventory'
      }),
      path: "".concat(match.path, "/inventory")
    }, {
      title: intl.formatMessage({
        id: 'xpack.infra.homePage.metricsExplorerTabTitle',
        defaultMessage: 'Metrics Explorer'
      }),
      path: "".concat(match.path, "/metrics-explorer")
    }, {
      title: intl.formatMessage({
        id: 'xpack.infra.homePage.settingsTabTitle',
        defaultMessage: 'Settings'
      }),
      path: "".concat(match.path, "/settings")
    }]
  })), _react2.default.createElement(_reactRouterDom.Switch, null, _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/inventory"),
    component: _snapshot.SnapshotPage
  }), _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/metrics-explorer"),
    render: function render(props) {
      return _react2.default.createElement(_with_source.WithSource, null, function (_ref2) {
        var configuration = _ref2.configuration,
            createDerivedIndexPattern = _ref2.createDerivedIndexPattern;
        return _react2.default.createElement(_use_metrics_explorer_options.MetricsExplorerOptionsContainer.Provider, null, _react2.default.createElement(_with_metrics_explorer_options_url_state.WithMetricsExplorerOptionsUrlState, null), _react2.default.createElement(_metrics_explorer.MetricsExplorerPage, _extends({
          derivedIndexPattern: createDerivedIndexPattern('metrics'),
          source: configuration
        }, props)));
      });
    }
  }), _react2.default.createElement(_reactRouterDom.Route, {
    path: "".concat(match.path, "/settings"),
    component: _settings.SettingsPage
  }))));
}));
exports.InfrastructurePage = InfrastructurePage;