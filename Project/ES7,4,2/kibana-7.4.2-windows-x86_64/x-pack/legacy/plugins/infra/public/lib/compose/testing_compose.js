"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.compose = compose;

require("ui/autoload/all");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _modules = require("ui/modules");

var _routes = _interopRequireDefault(require("ui/routes"));

var _timezone = require("ui/vis/lib/timezone");

var _apolloCacheInmemory = require("apollo-cache-inmemory");

var _apolloClient = _interopRequireDefault(require("apollo-client"));

var _apolloLinkSchema = require("apollo-link-schema");

var _graphqlTools = require("graphql-tools");

var _kibana_framework_adapter = require("../adapters/framework/kibana_framework_adapter");

var _kibana_observable_api = require("../adapters/observable_api/kibana_observable_api");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore: path dynamic for kibana
// @ts-ignore: path dynamic for kibana
// @ts-ignore: path dynamic for kibana
function compose() {
  var infraModule = _modules.uiModules.get('app/infa');

  var observableApi = new _kibana_observable_api.InfraKibanaObservableApiAdapter({
    basePath: _chrome.default.getBasePath(),
    xsrfToken: _chrome.default.getXsrfToken()
  });
  var framework = new _kibana_framework_adapter.InfraKibanaFrameworkAdapter(infraModule, _routes.default, _timezone.timezoneProvider);
  var typeDefs = "\n  Query {}\n";
  var mocks = {
    Mutation: function Mutation() {
      return undefined;
    },
    Query: function Query() {
      return undefined;
    }
  };
  var schema = (0, _graphqlTools.makeExecutableSchema)({
    typeDefs: typeDefs
  });
  (0, _graphqlTools.addMockFunctionsToSchema)({
    mocks: mocks,
    schema: schema
  });
  var cache = new _apolloCacheInmemory.InMemoryCache(window.__APOLLO_CLIENT__);
  var apolloClient = new _apolloClient.default({
    cache: cache,
    link: new _apolloLinkSchema.SchemaLink({
      schema: schema
    })
  });
  var libs = {
    apolloClient: apolloClient,
    framework: framework,
    observableApi: observableApi
  };
  return libs;
}