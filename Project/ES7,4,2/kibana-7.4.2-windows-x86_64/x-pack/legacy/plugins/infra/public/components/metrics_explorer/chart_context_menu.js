"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsExplorerChartContextMenu = exports.createNodeDetailLink = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _datemath = _interopRequireDefault(require("@elastic/datemath"));

var _create_tsvb_link = require("./helpers/create_tsvb_link");

var _types = require("../../graphql/types");

var _redirect_to_node_detail = require("../../pages/link_to/redirect_to_node_detail");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var fieldToNodeType = function fieldToNodeType(source, field) {
  if (source.fields.host === field) {
    return _types.InfraNodeType.host;
  }

  if (source.fields.pod === field) {
    return _types.InfraNodeType.pod;
  }

  if (source.fields.container === field) {
    return _types.InfraNodeType.container;
  }
};

var dateMathExpressionToEpoch = function dateMathExpressionToEpoch(dateMathExpression) {
  var roundUp = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

  var dateObj = _datemath.default.parse(dateMathExpression, {
    roundUp: roundUp
  });

  if (!dateObj) throw new Error("\"".concat(dateMathExpression, "\" is not a valid time string"));
  return dateObj.valueOf();
};

var createNodeDetailLink = function createNodeDetailLink(nodeType, nodeId, from, to) {
  return (0, _redirect_to_node_detail.getNodeDetailUrl)({
    nodeType: nodeType,
    nodeId: nodeId,
    from: dateMathExpressionToEpoch(from),
    to: dateMathExpressionToEpoch(to, true)
  });
};

exports.createNodeDetailLink = createNodeDetailLink;
var MetricsExplorerChartContextMenu = (0, _react2.injectI18n)(function (_ref) {
  var intl = _ref.intl,
      onFilter = _ref.onFilter,
      options = _ref.options,
      series = _ref.series,
      source = _ref.source,
      timeRange = _ref.timeRange,
      uiCapabilities = _ref.uiCapabilities,
      chartOptions = _ref.chartOptions;

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      isPopoverOpen = _useState2[0],
      setPopoverState = _useState2[1];

  var supportFiltering = options.groupBy != null && onFilter != null;
  var handleFilter = (0, _react.useCallback)(function () {
    // onFilter needs check for Typescript even though it's
    // covered by supportFiltering variable
    if (supportFiltering && onFilter) {
      onFilter("".concat(options.groupBy, ": \"").concat(series.id, "\""));
    }

    setPopoverState(false);
  }, [supportFiltering, options.groupBy, series.id, onFilter]);
  var tsvbUrl = (0, _create_tsvb_link.createTSVBLink)(source, options, series, timeRange, chartOptions); // Only display the "Add Filter" option if it's supported

  var filterByItem = supportFiltering ? [{
    name: intl.formatMessage({
      id: 'xpack.infra.metricsExplorer.filterByLabel',
      defaultMessage: 'Add filter'
    }),
    icon: 'infraApp',
    onClick: handleFilter,
    'data-test-subj': 'metricsExplorerAction-AddFilter'
  }] : [];
  var nodeType = source && options.groupBy && fieldToNodeType(source, options.groupBy);
  var viewNodeDetail = nodeType ? [{
    name: intl.formatMessage({
      id: 'xpack.infra.metricsExplorer.viewNodeDetail',
      defaultMessage: 'View metrics for {name}'
    }, {
      name: nodeType
    }),
    icon: 'infraApp',
    href: createNodeDetailLink(nodeType, series.id, timeRange.from, timeRange.to),
    'data-test-subj': 'metricsExplorerAction-ViewNodeMetrics'
  }] : [];
  var openInVisualize = uiCapabilities.visualize.show ? [{
    name: intl.formatMessage({
      id: 'xpack.infra.metricsExplorer.openInTSVB',
      defaultMessage: 'Open in Visualize'
    }),
    href: tsvbUrl,
    icon: 'visualizeApp',
    disabled: options.metrics.length === 0,
    'data-test-subj': 'metricsExplorerAction-OpenInTSVB'
  }] : [];
  var itemPanels = [].concat(filterByItem, openInVisualize, viewNodeDetail); // If there are no itemPanels then there is no reason to show the actions button.

  if (itemPanels.length === 0) return null;
  var panels = [{
    id: 0,
    title: 'Actions',
    items: itemPanels
  }];

  var handleClose = function handleClose() {
    return setPopoverState(false);
  };

  var handleOpen = function handleOpen() {
    return setPopoverState(true);
  };

  var actionAriaLabel = intl.formatMessage({
    id: 'xpack.infra.metricsExplorer.actionsLabel.aria',
    defaultMessage: 'Actions for {grouping}'
  }, {
    grouping: series.id
  });
  var actionLabel = intl.formatMessage({
    id: 'xpack.infra.metricsExplorer.actionsLabel.button',
    defaultMessage: 'Actions'
  });

  var button = _react.default.createElement(_eui.EuiButtonEmpty, {
    contentProps: {
      'aria-label': actionAriaLabel
    },
    onClick: handleOpen,
    size: "s",
    iconType: "arrowDown",
    iconSide: "right"
  }, actionLabel);

  return _react.default.createElement(_eui.EuiPopover, {
    closePopover: handleClose,
    id: "".concat(series.id, "-popover"),
    button: button,
    isOpen: isPopoverOpen,
    panelPaddingSize: "none"
  }, _react.default.createElement(_eui.EuiContextMenu, {
    initialPanelId: 0,
    panels: panels
  }));
});
exports.MetricsExplorerChartContextMenu = MetricsExplorerChartContextMenu;