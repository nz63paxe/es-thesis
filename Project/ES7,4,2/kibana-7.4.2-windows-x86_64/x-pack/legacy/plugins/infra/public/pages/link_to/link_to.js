"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LinkToPage = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _redirect_to_logs = require("./redirect_to_logs");

var _redirect_to_node_detail = require("./redirect_to_node_detail");

var _redirect_to_node_logs = require("./redirect_to_node_logs");

var _redirect_to_host_detail_via_ip = require("./redirect_to_host_detail_via_ip");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var LinkToPage =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LinkToPage, _React$Component);

  function LinkToPage() {
    _classCallCheck(this, LinkToPage);

    return _possibleConstructorReturn(this, _getPrototypeOf(LinkToPage).apply(this, arguments));
  }

  _createClass(LinkToPage, [{
    key: "render",
    value: function render() {
      var match = this.props.match;
      return _react.default.createElement(_reactRouterDom.Switch, null, _react.default.createElement(_reactRouterDom.Route, {
        path: "".concat(match.url, "/:sourceId?/:nodeType(host|container|pod)-logs/:nodeId"),
        component: _redirect_to_node_logs.RedirectToNodeLogs
      }), _react.default.createElement(_reactRouterDom.Route, {
        path: "".concat(match.url, "/:nodeType(host|container|pod)-detail/:nodeId"),
        component: _redirect_to_node_detail.RedirectToNodeDetail
      }), _react.default.createElement(_reactRouterDom.Route, {
        path: "".concat(match.url, "/host-detail-via-ip/:hostIp"),
        component: _redirect_to_host_detail_via_ip.RedirectToHostDetailViaIP
      }), _react.default.createElement(_reactRouterDom.Route, {
        path: "".concat(match.url, "/:sourceId?/logs"),
        component: _redirect_to_logs.RedirectToLogs
      }), _react.default.createElement(_reactRouterDom.Redirect, {
        to: "/infrastructure"
      }));
    }
  }]);

  return LinkToPage;
}(_react.default.Component);

exports.LinkToPage = LinkToPage;