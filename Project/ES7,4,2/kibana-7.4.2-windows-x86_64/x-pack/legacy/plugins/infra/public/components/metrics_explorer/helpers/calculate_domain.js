"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.calculateDomain = void 0;

var _lodash = require("lodash");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var calculateDomain = function calculateDomain(series, metrics) {
  var stacked = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  var values = series.rows.reduce(function (acc, row) {
    var rowValues = metrics.map(function (m, index) {
      return row["metric_".concat(index)] || null;
    }).filter(function (v) {
      return v;
    });
    var minValue = (0, _lodash.min)(rowValues); // For stacked domains we want to add 10% head room so the charts have
    // enough room to draw the 2 pixel line as well.

    var maxValue = stacked ? (0, _lodash.sum)(rowValues) * 1.1 : (0, _lodash.max)(rowValues);
    return acc.concat([minValue || null, maxValue || null]);
  }, []).filter(function (v) {
    return v;
  });
  return {
    min: (0, _lodash.min)(values) || 0,
    max: (0, _lodash.max)(values) || 0
  };
};

exports.calculateDomain = calculateDomain;