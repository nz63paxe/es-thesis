"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.HelpCenterContent = void 0;

var _eui = require("@elastic/eui");

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _chrome = _interopRequireDefault(require("ui/chrome"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Content = function Content(_ref) {
  var feedbackLink = _ref.feedbackLink,
      feedbackLinkText = _ref.feedbackLinkText;
  return _react.default.createElement(_eui.EuiLink, {
    href: feedbackLink,
    target: "_blank",
    rel: "noopener"
  }, feedbackLinkText);
};

var HelpCenterContent =
/*#__PURE__*/
function (_React$Component) {
  _inherits(HelpCenterContent, _React$Component);

  function HelpCenterContent() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, HelpCenterContent);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(HelpCenterContent)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "componentDidMount", function () {
      _chrome.default.helpExtension.set(function (domElement) {
        _reactDom.default.render(_react.default.createElement(Content, _this.props), domElement);

        return function () {
          _reactDom.default.unmountComponentAtNode(domElement);
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "render", function () {
      return null;
    });

    return _this;
  }

  return HelpCenterContent;
}(_react.default.Component);

exports.HelpCenterContent = HelpCenterContent;