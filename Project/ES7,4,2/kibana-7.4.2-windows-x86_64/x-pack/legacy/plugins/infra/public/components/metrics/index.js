"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Metrics = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _empty_states = require("../empty_states");

var _loading = require("../loading");

var _section = require("./section");

var _class, _temp;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Metrics = (0, _react.injectI18n)((_temp = _class =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(_class, _React$PureComponent);

  function _class() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, _class);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(_class)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      crosshairValue: null
    });

    _defineProperty(_assertThisInitialized(_this), "handleRefetch", function () {
      _this.props.refetch();
    });

    _defineProperty(_assertThisInitialized(_this), "renderLayout", function (layout) {
      return _react2.default.createElement(_react2.default.Fragment, {
        key: layout.id
      }, _react2.default.createElement(_eui.EuiPageContentBody, null, _react2.default.createElement(_eui.EuiTitle, {
        size: "m"
      }, _react2.default.createElement("h2", {
        id: layout.id
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.metrics.layoutLabelOverviewTitle",
        defaultMessage: "{layoutLabel} Overview",
        values: {
          layoutLabel: layout.label
        }
      })))), layout.sections.map(_this.renderSection(layout)));
    });

    _defineProperty(_assertThisInitialized(_this), "renderSection", function (layout) {
      return function (section) {
        var sectionProps = {};

        if (section.type === 'chart') {
          var _this$props = _this.props,
              onChangeRangeTime = _this$props.onChangeRangeTime,
              isLiveStreaming = _this$props.isLiveStreaming,
              stopLiveStreaming = _this$props.stopLiveStreaming;
          sectionProps = {
            onChangeRangeTime: onChangeRangeTime,
            isLiveStreaming: isLiveStreaming,
            stopLiveStreaming: stopLiveStreaming,
            crosshairValue: _this.state.crosshairValue,
            onCrosshairUpdate: _this.onCrosshairUpdate
          };
        }

        return _react2.default.createElement(_section.Section, _extends({
          section: section,
          metrics: _this.props.metrics,
          key: "".concat(layout.id, "-").concat(section.id)
        }, sectionProps));
      };
    });

    _defineProperty(_assertThisInitialized(_this), "onCrosshairUpdate", function (crosshairValue) {
      _this.setState({
        crosshairValue: crosshairValue
      });
    });

    return _this;
  }

  _createClass(_class, [{
    key: "render",
    value: function render() {
      var intl = this.props.intl;

      if (this.props.loading) {
        return _react2.default.createElement(_loading.InfraLoadingPanel, {
          height: "100vh",
          width: "auto",
          text: intl.formatMessage({
            id: 'xpack.infra.metrics.loadingNodeDataText',
            defaultMessage: 'Loading data'
          })
        });
      } else if (!this.props.loading && this.props.metrics && this.props.metrics.length === 0) {
        return _react2.default.createElement(_empty_states.NoData, {
          titleText: intl.formatMessage({
            id: 'xpack.infra.metrics.emptyViewTitle',
            defaultMessage: 'There is no data to display.'
          }),
          bodyText: intl.formatMessage({
            id: 'xpack.infra.metrics.emptyViewDescription',
            defaultMessage: 'Try adjusting your time or filter.'
          }),
          refetchText: intl.formatMessage({
            id: 'xpack.infra.metrics.refetchButtonLabel',
            defaultMessage: 'Check for new data'
          }),
          onRefetch: this.handleRefetch,
          testString: "metricsEmptyViewState"
        });
      }

      return _react2.default.createElement(_react2.default.Fragment, null, this.props.layouts.map(this.renderLayout));
    }
  }]);

  return _class;
}(_react2.default.PureComponent), _defineProperty(_class, "displayName", 'Metrics'), _temp));
exports.Metrics = Metrics;