"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.callSetupMlModuleAPI = void 0;

var rt = _interopRequireWildcard(require("io-ts"));

var _kfetch = require("ui/kfetch");

var _log_analysis = require("../../../../../common/log_analysis");

var _runtime_types = require("../../../../../common/runtime_types");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var MODULE_ID = 'logs_ui_analysis'; // This is needed due to: https://github.com/elastic/kibana/issues/43671

var removeSampleDataIndex = function removeSampleDataIndex(indexPattern) {
  var SAMPLE_DATA_INDEX = 'kibana_sample_data_logs*';
  return indexPattern.split(',').filter(function (index) {
    return index !== SAMPLE_DATA_INDEX;
  }).join(',');
};

var callSetupMlModuleAPI =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(start, end, spaceId, sourceId, indexPattern, timeField, bucketSpan) {
    var response;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0, _kfetch.kfetch)({
              method: 'POST',
              pathname: "/api/ml/modules/setup/".concat(MODULE_ID),
              body: JSON.stringify(setupMlModuleRequestPayloadRT.encode({
                start: start,
                end: end,
                indexPatternName: removeSampleDataIndex(indexPattern),
                prefix: (0, _log_analysis.getJobIdPrefix)(spaceId, sourceId),
                startDatafeed: true,
                jobOverrides: [{
                  job_id: 'log-entry-rate',
                  analysis_config: {
                    bucket_span: "".concat(bucketSpan, "ms")
                  },
                  data_description: {
                    time_field: timeField
                  }
                }],
                datafeedOverrides: []
              }))
            });

          case 2:
            response = _context.sent;
            return _context.abrupt("return", setupMlModuleResponsePayloadRT.decode(response).getOrElseL((0, _runtime_types.throwErrors)(_runtime_types.createPlainError)));

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function callSetupMlModuleAPI(_x, _x2, _x3, _x4, _x5, _x6, _x7) {
    return _ref.apply(this, arguments);
  };
}();

exports.callSetupMlModuleAPI = callSetupMlModuleAPI;
var setupMlModuleTimeParamsRT = rt.partial({
  start: rt.number,
  end: rt.number
});
var setupMlModuleRequestParamsRT = rt.type({
  indexPatternName: rt.string,
  prefix: rt.string,
  startDatafeed: rt.boolean,
  jobOverrides: rt.array(rt.object),
  datafeedOverrides: rt.array(rt.object)
});
var setupMlModuleRequestPayloadRT = rt.intersection([setupMlModuleTimeParamsRT, setupMlModuleRequestParamsRT]);
var setupMlModuleResponsePayloadRT = rt.type({
  datafeeds: rt.array(rt.type({
    id: rt.string,
    started: rt.boolean,
    success: rt.boolean,
    error: rt.any
  })),
  jobs: rt.array(rt.type({
    id: rt.string,
    success: rt.boolean,
    error: rt.any
  }))
});