"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceLogPositionInQueryString = exports.WithLogPositionUrlState = exports.WithLogPosition = exports.withLogPosition = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _reselect = require("reselect");

var _time = require("../../../common/time");

var _store = require("../../store");

var _typed_react = require("../../utils/typed_react");

var _typed_redux = require("../../utils/typed_redux");

var _url_state = require("../../utils/url_state");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var withLogPosition = (0, _reactRedux.connect)(function (state) {
  return {
    firstVisiblePosition: _store.logPositionSelectors.selectFirstVisiblePosition(state),
    isAutoReloading: _store.logPositionSelectors.selectIsAutoReloading(state),
    lastVisiblePosition: _store.logPositionSelectors.selectFirstVisiblePosition(state),
    targetPosition: _store.logPositionSelectors.selectTargetPosition(state),
    urlState: selectPositionUrlState(state),
    visibleTimeInterval: _store.logPositionSelectors.selectVisibleTimeInterval(state),
    visibleMidpoint: _store.logPositionSelectors.selectVisibleMidpointOrTarget(state),
    visibleMidpointTime: _store.logPositionSelectors.selectVisibleMidpointOrTargetTime(state)
  };
}, (0, _typed_redux.bindPlainActionCreators)({
  jumpToTargetPosition: _store.logPositionActions.jumpToTargetPosition,
  jumpToTargetPositionTime: _store.logPositionActions.jumpToTargetPositionTime,
  reportVisiblePositions: _store.logPositionActions.reportVisiblePositions,
  startLiveStreaming: _store.logPositionActions.startAutoReload,
  stopLiveStreaming: _store.logPositionActions.stopAutoReload
}));
exports.withLogPosition = withLogPosition;
var WithLogPosition = (0, _typed_react.asChildFunctionRenderer)(withLogPosition, {
  onCleanup: function onCleanup(_ref) {
    var stopLiveStreaming = _ref.stopLiveStreaming;
    return stopLiveStreaming();
  }
});
/**
 * Url State
 */

exports.WithLogPosition = WithLogPosition;

var WithLogPositionUrlState = function WithLogPositionUrlState() {
  return _react.default.createElement(WithLogPosition, null, function (_ref2) {
    var jumpToTargetPosition = _ref2.jumpToTargetPosition,
        jumpToTargetPositionTime = _ref2.jumpToTargetPositionTime,
        startLiveStreaming = _ref2.startLiveStreaming,
        stopLiveStreaming = _ref2.stopLiveStreaming,
        urlState = _ref2.urlState;
    return _react.default.createElement(_url_state.UrlStateContainer, {
      urlState: urlState,
      urlStateKey: "logPosition",
      mapToUrlState: mapToUrlState,
      onChange: function onChange(newUrlState) {
        if (newUrlState && newUrlState.position) {
          jumpToTargetPosition(newUrlState.position);
        }

        if (newUrlState && newUrlState.streamLive) {
          startLiveStreaming(5000);
        } else if (newUrlState && typeof newUrlState.streamLive !== 'undefined' && !newUrlState.streamLive) {
          stopLiveStreaming();
        }
      },
      onInitialize: function onInitialize(initialUrlState) {
        if (initialUrlState && initialUrlState.position) {
          jumpToTargetPosition(initialUrlState.position);
        } else {
          jumpToTargetPositionTime(Date.now());
        }

        if (initialUrlState && initialUrlState.streamLive) {
          startLiveStreaming(5000);
        }
      }
    });
  });
};

exports.WithLogPositionUrlState = WithLogPositionUrlState;
var selectPositionUrlState = (0, _reselect.createSelector)(_store.logPositionSelectors.selectVisibleMidpointOrTarget, _store.logPositionSelectors.selectIsAutoReloading, function (position, streamLive) {
  return {
    position: position ? (0, _time.pickTimeKey)(position) : null,
    streamLive: streamLive
  };
});

var mapToUrlState = function mapToUrlState(value) {
  return value ? {
    position: mapToPositionUrlState(value.position),
    streamLive: mapToStreamLiveUrlState(value.streamLive)
  } : undefined;
};

var mapToPositionUrlState = function mapToPositionUrlState(value) {
  return value && typeof value.time === 'number' && typeof value.tiebreaker === 'number' ? (0, _time.pickTimeKey)(value) : undefined;
};

var mapToStreamLiveUrlState = function mapToStreamLiveUrlState(value) {
  return typeof value === 'boolean' ? value : undefined;
};

var replaceLogPositionInQueryString = function replaceLogPositionInQueryString(time) {
  return Number.isNaN(time) ? function (value) {
    return value;
  } : (0, _url_state.replaceStateKeyInQueryString)('logPosition', {
    position: {
      time: time,
      tiebreaker: 0
    }
  });
};

exports.replaceLogPositionInQueryString = replaceLogPositionInQueryString;