"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.selectLoadedEntriesTimeInterval = exports.selectLastEntry = exports.selectFirstEntry = exports.selectEntriesEndLoadingState = exports.selectEntriesStartLoadingState = exports.selectEntriesLastLoadedTime = exports.selectHasMoreAfterEnd = exports.selectHasMoreBeforeStart = exports.selectEntriesEnd = exports.selectEntriesStart = exports.selectIsLoadingMoreEntries = exports.selectIsReloadingEntries = exports.selectIsLoadingEntries = exports.selectEntries = void 0;

var _reselect = require("reselect");

var _remote_graphql_state = require("../../../utils/remote_state/remote_graphql_state");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var entriesGraphlStateSelectors = (0, _remote_graphql_state.createGraphqlStateSelectors)();
var selectEntries = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return data ? data.entries : [];
});
exports.selectEntries = selectEntries;
var selectIsLoadingEntries = entriesGraphlStateSelectors.selectIsLoading;
exports.selectIsLoadingEntries = selectIsLoadingEntries;
var selectIsReloadingEntries = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectIsLoading, entriesGraphlStateSelectors.selectLoadingProgressOperationInfo, function (isLoading, operationInfo) {
  return isLoading && operationInfo ? operationInfo.operationKey === 'load' : false;
});
exports.selectIsReloadingEntries = selectIsReloadingEntries;
var selectIsLoadingMoreEntries = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectIsLoading, entriesGraphlStateSelectors.selectLoadingProgressOperationInfo, function (isLoading, operationInfo) {
  return isLoading && operationInfo ? operationInfo.operationKey === 'load_more' : false;
});
exports.selectIsLoadingMoreEntries = selectIsLoadingMoreEntries;
var selectEntriesStart = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return data && data.start ? data.start : null;
});
exports.selectEntriesStart = selectEntriesStart;
var selectEntriesEnd = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return data && data.end ? data.end : null;
});
exports.selectEntriesEnd = selectEntriesEnd;
var selectHasMoreBeforeStart = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return data ? data.hasMoreBefore : true;
});
exports.selectHasMoreBeforeStart = selectHasMoreBeforeStart;
var selectHasMoreAfterEnd = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return data ? data.hasMoreAfter : true;
});
exports.selectHasMoreAfterEnd = selectHasMoreAfterEnd;
var selectEntriesLastLoadedTime = entriesGraphlStateSelectors.selectLoadingResultTime;
exports.selectEntriesLastLoadedTime = selectEntriesLastLoadedTime;
var selectEntriesStartLoadingState = entriesGraphlStateSelectors.selectLoadingState;
exports.selectEntriesStartLoadingState = selectEntriesStartLoadingState;
var selectEntriesEndLoadingState = entriesGraphlStateSelectors.selectLoadingState;
exports.selectEntriesEndLoadingState = selectEntriesEndLoadingState;
var selectFirstEntry = (0, _reselect.createSelector)(selectEntries, function (entries) {
  return entries.length > 0 ? entries[0] : null;
});
exports.selectFirstEntry = selectFirstEntry;
var selectLastEntry = (0, _reselect.createSelector)(selectEntries, function (entries) {
  return entries.length > 0 ? entries[entries.length - 1] : null;
});
exports.selectLastEntry = selectLastEntry;
var selectLoadedEntriesTimeInterval = (0, _reselect.createSelector)(entriesGraphlStateSelectors.selectData, function (data) {
  return {
    end: data && data.end ? data.end.time : null,
    start: data && data.start ? data.start.time : null
  };
});
exports.selectLoadedEntriesTimeInterval = selectLoadedEntriesTimeInterval;