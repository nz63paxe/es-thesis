"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "useTrackPageview", {
  enumerable: true,
  get: function get() {
    return _use_track_metric.useTrackPageview;
  }
});

var _use_track_metric = require("./hooks/use_track_metric");