"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createEntriesEffectsEpic = exports.createLogEntriesEpic = void 0;

var _reduxObservable = require("redux-observable");

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _2 = require("../..");

var _time = require("../../../../common/time");

var _actions = require("./actions");

var _load = require("./operations/load");

var _load_more = require("./operations/load_more");

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var LOAD_CHUNK_SIZE = 200;
var DESIRED_BUFFER_PAGES = 2;

var createLogEntriesEpic = function createLogEntriesEpic() {
  return (0, _reduxObservable.combineEpics)(createEntriesEffectsEpic(), _load.loadEntriesEpic, _load_more.loadMoreEntriesEpic);
};

exports.createLogEntriesEpic = createLogEntriesEpic;

var createEntriesEffectsEpic = function createEntriesEffectsEpic() {
  return function (action$, state$, _ref) {
    var selectLogEntriesStart = _ref.selectLogEntriesStart,
        selectLogEntriesEnd = _ref.selectLogEntriesEnd,
        selectHasMoreLogEntriesBeforeStart = _ref.selectHasMoreLogEntriesBeforeStart,
        selectHasMoreLogEntriesAfterEnd = _ref.selectHasMoreLogEntriesAfterEnd,
        selectIsAutoReloadingLogEntries = _ref.selectIsAutoReloadingLogEntries,
        selectIsLoadingLogEntries = _ref.selectIsLoadingLogEntries,
        selectLogFilterQueryAsJson = _ref.selectLogFilterQueryAsJson,
        selectVisibleLogMidpointOrTarget = _ref.selectVisibleLogMidpointOrTarget;
    var filterQuery$ = state$.pipe((0, _operators.map)(selectLogFilterQueryAsJson));
    var visibleMidpointOrTarget$ = state$.pipe((0, _operators.map)(selectVisibleLogMidpointOrTarget), (0, _operators.filter)(isNotNull), (0, _operators.map)(_time.pickTimeKey));
    var sourceId$ = action$.pipe((0, _operators.filter)(_actions.setSourceId.match), (0, _operators.map)(function (_ref2) {
      var payload = _ref2.payload;
      return payload;
    }));
    var shouldLoadAroundNewPosition$ = action$.pipe((0, _operators.filter)(_2.logPositionActions.jumpToTargetPosition.match), (0, _operators.withLatestFrom)(state$), (0, _operators.filter)(function (_ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
          payload = _ref4[0].payload,
          state = _ref4[1];

      var entriesStart = selectLogEntriesStart(state);
      var entriesEnd = selectLogEntriesEnd(state);
      return entriesStart && entriesEnd ? !(0, _time.timeKeyIsBetween)(entriesStart, entriesEnd, payload) : true;
    }), (0, _operators.map)(function (_ref5) {
      var _ref6 = _slicedToArray(_ref5, 1),
          payload = _ref6[0].payload;

      return (0, _time.pickTimeKey)(payload);
    }));
    var shouldLoadWithNewFilter$ = action$.pipe((0, _operators.filter)(_2.logFilterActions.applyLogFilterQuery.match), (0, _operators.withLatestFrom)(filterQuery$, function (filterQuery, filterQueryString) {
      return filterQueryString;
    }));
    var shouldReload$ = (0, _rxjs.merge)(action$.pipe((0, _operators.filter)(_actions.reloadEntries.match)), sourceId$);
    var shouldLoadMoreBefore$ = action$.pipe((0, _operators.filter)(_2.logPositionActions.reportVisiblePositions.match), (0, _operators.filter)(function (_ref7) {
      var pagesBeforeStart = _ref7.payload.pagesBeforeStart;
      return pagesBeforeStart < DESIRED_BUFFER_PAGES;
    }), (0, _operators.withLatestFrom)(state$), (0, _operators.filter)(function (_ref8) {
      var _ref9 = _slicedToArray(_ref8, 2),
          action = _ref9[0],
          state = _ref9[1];

      return !selectIsAutoReloadingLogEntries(state) && !selectIsLoadingLogEntries(state) && selectHasMoreLogEntriesBeforeStart(state);
    }), (0, _operators.map)(function (_ref10) {
      var _ref11 = _slicedToArray(_ref10, 2),
          action = _ref11[0],
          state = _ref11[1];

      return selectLogEntriesStart(state);
    }), (0, _operators.filter)(isNotNull), (0, _operators.map)(_time.pickTimeKey));
    var shouldLoadMoreAfter$ = (0, _rxjs.merge)(action$.pipe((0, _operators.filter)(_2.logPositionActions.reportVisiblePositions.match), (0, _operators.filter)(function (_ref12) {
      var pagesAfterEnd = _ref12.payload.pagesAfterEnd;
      return pagesAfterEnd < DESIRED_BUFFER_PAGES;
    }), (0, _operators.withLatestFrom)(state$, function (action, state) {
      return state;
    }), (0, _operators.filter)(function (state) {
      return !selectIsAutoReloadingLogEntries(state) && !selectIsLoadingLogEntries(state) && selectHasMoreLogEntriesAfterEnd(state);
    })), action$.pipe((0, _operators.filter)(_actions.loadNewerEntries.match), (0, _operators.withLatestFrom)(state$, function (action, state) {
      return state;
    }))).pipe((0, _operators.map)(function (state) {
      return selectLogEntriesEnd(state);
    }), (0, _operators.filter)(isNotNull), (0, _operators.map)(_time.pickTimeKey));
    return (0, _rxjs.merge)(shouldLoadAroundNewPosition$.pipe((0, _operators.withLatestFrom)(filterQuery$, sourceId$), (0, _operators.exhaustMap)(function (_ref13) {
      var _ref14 = _slicedToArray(_ref13, 3),
          timeKey = _ref14[0],
          filterQuery = _ref14[1],
          sourceId = _ref14[2];

      return [(0, _actions.loadEntries)({
        sourceId: sourceId,
        timeKey: timeKey,
        countBefore: LOAD_CHUNK_SIZE,
        countAfter: LOAD_CHUNK_SIZE,
        filterQuery: filterQuery
      })];
    })), shouldLoadWithNewFilter$.pipe((0, _operators.withLatestFrom)(visibleMidpointOrTarget$, sourceId$), (0, _operators.exhaustMap)(function (_ref15) {
      var _ref16 = _slicedToArray(_ref15, 3),
          filterQuery = _ref16[0],
          timeKey = _ref16[1],
          sourceId = _ref16[2];

      return [(0, _actions.loadEntries)({
        sourceId: sourceId,
        timeKey: timeKey,
        countBefore: LOAD_CHUNK_SIZE,
        countAfter: LOAD_CHUNK_SIZE,
        filterQuery: filterQuery
      })];
    })), shouldReload$.pipe((0, _operators.withLatestFrom)(visibleMidpointOrTarget$, filterQuery$, sourceId$), (0, _operators.exhaustMap)(function (_ref17) {
      var _ref18 = _slicedToArray(_ref17, 4),
          _ = _ref18[0],
          timeKey = _ref18[1],
          filterQuery = _ref18[2],
          sourceId = _ref18[3];

      return [(0, _actions.loadEntries)({
        sourceId: sourceId,
        timeKey: timeKey,
        countBefore: LOAD_CHUNK_SIZE,
        countAfter: LOAD_CHUNK_SIZE,
        filterQuery: filterQuery
      })];
    })), shouldLoadMoreAfter$.pipe((0, _operators.withLatestFrom)(filterQuery$, sourceId$), (0, _operators.exhaustMap)(function (_ref19) {
      var _ref20 = _slicedToArray(_ref19, 3),
          timeKey = _ref20[0],
          filterQuery = _ref20[1],
          sourceId = _ref20[2];

      return [(0, _actions.loadMoreEntries)({
        sourceId: sourceId,
        timeKey: timeKey,
        countBefore: 0,
        countAfter: LOAD_CHUNK_SIZE,
        filterQuery: filterQuery
      })];
    })), shouldLoadMoreBefore$.pipe((0, _operators.withLatestFrom)(filterQuery$, sourceId$), (0, _operators.exhaustMap)(function (_ref21) {
      var _ref22 = _slicedToArray(_ref21, 3),
          timeKey = _ref22[0],
          filterQuery = _ref22[1],
          sourceId = _ref22[2];

      return [(0, _actions.loadMoreEntries)({
        sourceId: sourceId,
        timeKey: timeKey,
        countBefore: LOAD_CHUNK_SIZE,
        countAfter: 0,
        filterQuery: filterQuery
      })];
    })));
  };
};

exports.createEntriesEffectsEpic = createEntriesEffectsEpic;

var isNotNull = function isNotNull(value) {
  return value !== null;
};