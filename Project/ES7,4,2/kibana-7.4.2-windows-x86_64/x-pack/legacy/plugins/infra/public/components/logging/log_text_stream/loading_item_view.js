"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogTextStreamLoadingItemView = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var React = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  padding: 8px 16px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  align-items: center;\n  display: flex;\n  min-height: ", ";\n  position: relative;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var LogTextStreamLoadingItemView =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(LogTextStreamLoadingItemView, _React$PureComponent);

  function LogTextStreamLoadingItemView() {
    _classCallCheck(this, LogTextStreamLoadingItemView);

    return _possibleConstructorReturn(this, _getPrototypeOf(LogTextStreamLoadingItemView).apply(this, arguments));
  }

  _createClass(LogTextStreamLoadingItemView, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          alignment = _this$props.alignment,
          className = _this$props.className,
          hasMore = _this$props.hasMore,
          isLoading = _this$props.isLoading,
          isStreaming = _this$props.isStreaming,
          lastStreamingUpdate = _this$props.lastStreamingUpdate,
          onLoadMore = _this$props.onLoadMore;

      if (isStreaming) {
        return React.createElement(ProgressEntry, {
          alignment: alignment,
          className: className,
          color: "primary",
          isLoading: true
        }, React.createElement(ProgressMessage, null, React.createElement(_eui.EuiText, {
          color: "subdued"
        }, React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.streamingNewEntriesText",
          defaultMessage: "Streaming new entries"
        }))), lastStreamingUpdate ? React.createElement(ProgressMessage, null, React.createElement(_eui.EuiText, {
          color: "subdued"
        }, React.createElement(_eui.EuiIcon, {
          type: "clock"
        }), React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.lastStreamingUpdateText",
          defaultMessage: " last updated {lastUpdateTime}",
          values: {
            lastUpdateTime: React.createElement(_react.FormattedRelative, {
              value: lastStreamingUpdate,
              updateInterval: 1000
            })
          }
        }))) : null);
      } else if (isLoading) {
        return React.createElement(ProgressEntry, {
          alignment: alignment,
          className: className,
          color: "subdued",
          isLoading: true
        }, React.createElement(ProgressMessage, null, React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.loadingAdditionalEntriesText",
          defaultMessage: "Loading additional entries"
        })));
      } else if (!hasMore) {
        return React.createElement(ProgressEntry, {
          alignment: alignment,
          className: className,
          color: "subdued",
          isLoading: false
        }, React.createElement(ProgressMessage, null, React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.noAdditionalEntriesFoundText",
          defaultMessage: "No additional entries found"
        })), onLoadMore ? React.createElement(_eui.EuiButtonEmpty, {
          size: "xs",
          onClick: onLoadMore,
          iconType: "refresh"
        }, React.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.loadAgainButtonLabel",
          defaultMessage: "Load again"
        })) : null);
      } else {
        return null;
      }
    }
  }]);

  return LogTextStreamLoadingItemView;
}(React.PureComponent);

exports.LogTextStreamLoadingItemView = LogTextStreamLoadingItemView;

var ProgressEntry =
/*#__PURE__*/
function (_React$PureComponent2) {
  _inherits(ProgressEntry, _React$PureComponent2);

  function ProgressEntry() {
    _classCallCheck(this, ProgressEntry);

    return _possibleConstructorReturn(this, _getPrototypeOf(ProgressEntry).apply(this, arguments));
  }

  _createClass(ProgressEntry, [{
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          alignment = _this$props2.alignment,
          children = _this$props2.children,
          className = _this$props2.className,
          color = _this$props2.color,
          isLoading = _this$props2.isLoading; // NOTE: styled-components seems to make all props in EuiProgress required, so this
      // style attribute hacking replaces styled-components here for now until that can be fixed
      // see: https://github.com/elastic/eui/issues/1655

      var alignmentStyle = alignment === 'top' ? {
        top: 0,
        bottom: 'initial'
      } : {
        top: 'initial',
        bottom: 0
      };
      return React.createElement(ProgressEntryWrapper, {
        className: className
      }, React.createElement(_eui.EuiProgress, _extends({
        style: alignmentStyle,
        color: color,
        size: "xs",
        position: "absolute"
      }, !isLoading ? {
        max: 1,
        value: 1
      } : {})), children);
    }
  }]);

  return ProgressEntry;
}(React.PureComponent);

var ProgressEntryWrapper = _eui_styled_components.default.div(_templateObject(), function (props) {
  return props.theme.eui.euiSizeXXL;
});

var ProgressMessage = _eui_styled_components.default.div(_templateObject2());