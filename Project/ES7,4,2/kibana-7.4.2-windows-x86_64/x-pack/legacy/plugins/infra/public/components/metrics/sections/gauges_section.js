"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GaugesSection = void 0;

var _eui = require("@elastic/eui");

var _lodash = require("lodash");

var _react = _interopRequireDefault(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _lib = require("../../../lib/lib");

var _formatters = require("../../../utils/formatters");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-flow: row wrap;\n  justify-content: space-evenly;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var getFormatter = function getFormatter(section, seriesId) {
  return function (val) {
    if (val == null) {
      return '';
    }

    var defaultFormatter = (0, _lodash.get)(section, ['visConfig', 'formatter'], _lib.InfraFormatterType.number);
    var defaultFormatterTemplate = (0, _lodash.get)(section, ['visConfig', 'formatterTemplate'], '{{value}}');
    var formatter = (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', seriesId, 'formatter'], defaultFormatter);
    var formatterTemplate = (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', seriesId, 'formatterTemplate'], defaultFormatterTemplate);
    return (0, _formatters.createFormatter)(formatter, formatterTemplate)(val);
  };
};

var GaugesSection =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(GaugesSection, _React$PureComponent);

  function GaugesSection() {
    _classCallCheck(this, GaugesSection);

    return _possibleConstructorReturn(this, _getPrototypeOf(GaugesSection).apply(this, arguments));
  }

  _createClass(GaugesSection, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          metric = _this$props.metric,
          section = _this$props.section;
      return _react.default.createElement(_eui.EuiPageContentBody, null, _react.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }), _react.default.createElement(GroupBox, null, metric.series.map(function (series) {
        var lastDataPoint = (0, _lodash.last)(series.data);

        if (!lastDataPoint) {
          return null;
        }

        var formatter = getFormatter(section, series.id);
        var value = formatter(lastDataPoint.value || 0);
        var name = (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', series.id, 'name'], series.id);
        var dataMax = (0, _lodash.max)(series.data.map(function (d) {
          return d.value || 0;
        }));
        var gaugeMax = (0, _lodash.get)(section, ['visConfig', 'seriesOverrides', series.id, 'gaugeMax'], dataMax);
        return _react.default.createElement(_eui.EuiFlexItem, {
          key: "".concat(section.id, "-").concat(series.id),
          style: {
            margin: '0.4rem'
          }
        }, _react.default.createElement(_eui.EuiPanel, {
          style: {
            minWidth: '160px'
          }
        }, _react.default.createElement(_eui.EuiText, {
          style: {
            textAlign: 'right'
          },
          size: "s"
        }, name), _react.default.createElement(_eui.EuiTitle, {
          size: "s"
        }, _react.default.createElement("h1", {
          style: {
            textAlign: 'right',
            whiteSpace: 'nowrap'
          }
        }, value)), _react.default.createElement(_eui.EuiProgress, {
          value: lastDataPoint.value || 0,
          max: gaugeMax,
          size: "s",
          color: "primary"
        })));
      })), _react.default.createElement(_eui.EuiSpacer, {
        size: "m"
      }));
    }
  }]);

  return GaugesSection;
}(_react.default.PureComponent);

exports.GaugesSection = GaugesSection;

var GroupBox = _eui_styled_components.default.div(_templateObject());