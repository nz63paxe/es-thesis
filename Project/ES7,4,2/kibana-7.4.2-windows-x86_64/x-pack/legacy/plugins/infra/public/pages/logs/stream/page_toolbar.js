"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogsToolbar = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _autocomplete_field = require("../../../components/autocomplete_field");

var _eui2 = require("../../../components/eui");

var _log_customization_menu = require("../../../components/logging/log_customization_menu");

var _log_highlights_menu = require("../../../components/logging/log_highlights_menu");

var _log_highlights = require("../../../containers/logs/log_highlights/log_highlights");

var _log_minimap_scale_controls = require("../../../components/logging/log_minimap_scale_controls");

var _log_text_scale_controls = require("../../../components/logging/log_text_scale_controls");

var _log_text_wrap_controls = require("../../../components/logging/log_text_wrap_controls");

var _log_time_controls = require("../../../components/logging/log_time_controls");

var _log_flyout = require("../../../containers/logs/log_flyout");

var _log_view_configuration = require("../../../containers/logs/log_view_configuration");

var _with_log_filter = require("../../../containers/logs/with_log_filter");

var _with_log_position = require("../../../containers/logs/with_log_position");

var _source = require("../../../containers/source");

var _with_kuery_autocompletion = require("../../../containers/with_kuery_autocompletion");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var LogsToolbar = (0, _react.injectI18n)(function (_ref) {
  var intl = _ref.intl;

  var _useContext = (0, _react2.useContext)(_source.Source.Context),
      createDerivedIndexPattern = _useContext.createDerivedIndexPattern;

  var derivedIndexPattern = createDerivedIndexPattern('logs');

  var _useContext2 = (0, _react2.useContext)(_log_view_configuration.LogViewConfiguration.Context),
      availableIntervalSizes = _useContext2.availableIntervalSizes,
      availableTextScales = _useContext2.availableTextScales,
      intervalSize = _useContext2.intervalSize,
      setIntervalSize = _useContext2.setIntervalSize,
      setTextScale = _useContext2.setTextScale,
      setTextWrap = _useContext2.setTextWrap,
      textScale = _useContext2.textScale,
      textWrap = _useContext2.textWrap;

  var _useContext3 = (0, _react2.useContext)(_log_flyout.LogFlyout.Context),
      setSurroundingLogsId = _useContext3.setSurroundingLogsId;

  var _useContext4 = (0, _react2.useContext)(_log_highlights.LogHighlightsState.Context),
      setHighlightTerms = _useContext4.setHighlightTerms,
      loadLogEntryHighlightsRequest = _useContext4.loadLogEntryHighlightsRequest,
      highlightTerms = _useContext4.highlightTerms,
      hasPreviousHighlight = _useContext4.hasPreviousHighlight,
      hasNextHighlight = _useContext4.hasNextHighlight,
      goToPreviousHighlight = _useContext4.goToPreviousHighlight,
      goToNextHighlight = _useContext4.goToNextHighlight;

  return _react2.default.createElement(_eui2.Toolbar, null, _react2.default.createElement(_eui.EuiFlexGroup, {
    alignItems: "center",
    justifyContent: "spaceBetween",
    gutterSize: "s"
  }, _react2.default.createElement(_eui.EuiFlexItem, null, _react2.default.createElement(_with_kuery_autocompletion.WithKueryAutocompletion, {
    indexPattern: derivedIndexPattern
  }, function (_ref2) {
    var isLoadingSuggestions = _ref2.isLoadingSuggestions,
        loadSuggestions = _ref2.loadSuggestions,
        suggestions = _ref2.suggestions;
    return _react2.default.createElement(_with_log_filter.WithLogFilter, {
      indexPattern: derivedIndexPattern
    }, function (_ref3) {
      var applyFilterQueryFromKueryExpression = _ref3.applyFilterQueryFromKueryExpression,
          filterQueryDraft = _ref3.filterQueryDraft,
          isFilterQueryDraftValid = _ref3.isFilterQueryDraftValid,
          setFilterQueryDraftFromKueryExpression = _ref3.setFilterQueryDraftFromKueryExpression;
      return _react2.default.createElement(_autocomplete_field.AutocompleteField, {
        isLoadingSuggestions: isLoadingSuggestions,
        isValid: isFilterQueryDraftValid,
        loadSuggestions: loadSuggestions,
        onChange: function onChange(expression) {
          setSurroundingLogsId(null);
          setFilterQueryDraftFromKueryExpression(expression);
        },
        onSubmit: function onSubmit(expression) {
          setSurroundingLogsId(null);
          applyFilterQueryFromKueryExpression(expression);
        },
        placeholder: intl.formatMessage({
          id: 'xpack.infra.logsPage.toolbar.kqlSearchFieldPlaceholder',
          defaultMessage: 'Search for log entries… (e.g. host.name:host-1)'
        }),
        suggestions: suggestions,
        value: filterQueryDraft ? filterQueryDraft.expression : '',
        "aria-label": intl.formatMessage({
          id: 'xpack.infra.logsPage.toolbar.kqlSearchFieldAriaLabel',
          defaultMessage: 'Search for log entries'
        })
      });
    });
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_log_customization_menu.LogCustomizationMenu, null, _react2.default.createElement(_log_minimap_scale_controls.LogMinimapScaleControls, {
    availableIntervalSizes: availableIntervalSizes,
    setIntervalSize: setIntervalSize,
    intervalSize: intervalSize
  }), _react2.default.createElement(_log_text_wrap_controls.LogTextWrapControls, {
    wrap: textWrap,
    setTextWrap: setTextWrap
  }), _react2.default.createElement(_log_text_scale_controls.LogTextScaleControls, {
    availableTextScales: availableTextScales,
    textScale: textScale,
    setTextScale: setTextScale
  }))), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_log_highlights_menu.LogHighlightsMenu, {
    onChange: setHighlightTerms,
    isLoading: loadLogEntryHighlightsRequest.state === 'pending',
    activeHighlights: highlightTerms.filter(function (highlightTerm) {
      return highlightTerm.length > 0;
    }).length > 0,
    goToPreviousHighlight: goToPreviousHighlight,
    goToNextHighlight: goToNextHighlight,
    hasPreviousHighlight: hasPreviousHighlight,
    hasNextHighlight: hasNextHighlight
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_with_log_position.WithLogPosition, {
    resetOnUnmount: true
  }, function (_ref4) {
    var visibleMidpointTime = _ref4.visibleMidpointTime,
        isAutoReloading = _ref4.isAutoReloading,
        jumpToTargetPositionTime = _ref4.jumpToTargetPositionTime,
        _startLiveStreaming = _ref4.startLiveStreaming,
        stopLiveStreaming = _ref4.stopLiveStreaming,
        targetPosition = _ref4.targetPosition;
    return _react2.default.createElement(_log_time_controls.LogTimeControls, {
      currentTime: visibleMidpointTime,
      isLiveStreaming: isAutoReloading,
      jumpToTime: jumpToTargetPositionTime,
      startLiveStreaming: function startLiveStreaming(interval) {
        _startLiveStreaming(interval);

        setSurroundingLogsId(null);
      },
      stopLiveStreaming: stopLiveStreaming
    });
  }))));
});
exports.LogsToolbar = LogsToolbar;