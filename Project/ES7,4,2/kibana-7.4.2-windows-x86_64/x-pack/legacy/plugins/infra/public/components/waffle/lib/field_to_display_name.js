"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fieldToName = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var fieldToName = function fieldToName(field, intl) {
  var LOOKUP = {
    'kubernetes.namespace': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.kubernetesNamespace',
      defaultMessage: 'Namespace'
    }),
    'kubernetes.node.name': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.kubernetesNodeName',
      defaultMessage: 'Node'
    }),
    'host.name': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.hostName',
      defaultMessage: 'Host'
    }),
    'cloud.availability_zone': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.availabilityZone',
      defaultMessage: 'Availability zone'
    }),
    'cloud.machine.type': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.machineType',
      defaultMessage: 'Machine type'
    }),
    'cloud.project.id': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.projectID',
      defaultMessage: 'Project ID'
    }),
    'cloud.provider': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.provider',
      defaultMessage: 'Cloud provider'
    }),
    'service.type': intl.formatMessage({
      id: 'xpack.infra.groupByDisplayNames.serviceType',
      defaultMessage: 'Service type'
    })
  };
  return LOOKUP[field] || field;
};

exports.fieldToName = fieldToName;