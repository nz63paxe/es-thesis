"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getChartTheme = exports.isDarkMode = exports.getColorsMap = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _charts = require("@elastic/charts");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var getColorsMap = function getColorsMap(color, specId) {
  var map = new Map();
  map.set({
    colorValues: [],
    specId: specId
  }, color);
  return map;
};

exports.getColorsMap = getColorsMap;

var isDarkMode = function isDarkMode() {
  return _chrome.default.getUiSettingsClient().get('theme:darkMode');
};

exports.isDarkMode = isDarkMode;

var getChartTheme = function getChartTheme() {
  return isDarkMode() ? _charts.DARK_THEME : _charts.LIGHT_THEME;
};

exports.getChartTheme = getChartTheme;