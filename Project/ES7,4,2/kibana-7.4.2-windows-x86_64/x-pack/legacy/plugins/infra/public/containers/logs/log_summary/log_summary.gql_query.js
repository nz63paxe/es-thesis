"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logSummaryQuery = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  query LogSummary(\n    $sourceId: ID = \"default\"\n    $start: Float!\n    $end: Float!\n    $bucketSize: Float!\n    $filterQuery: String\n  ) {\n    source(id: $sourceId) {\n      id\n      logSummaryBetween(\n        start: $start\n        end: $end\n        bucketSize: $bucketSize\n        filterQuery: $filterQuery\n      ) {\n        start\n        end\n        buckets {\n          start\n          end\n          entriesCount\n        }\n      }\n    }\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var logSummaryQuery = (0, _graphqlTag.default)(_templateObject());
exports.logSummaryQuery = logSummaryQuery;