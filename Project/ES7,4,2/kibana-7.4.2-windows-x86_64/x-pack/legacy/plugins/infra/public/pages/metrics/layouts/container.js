"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.containerLayoutCreator = void 0;

var _i18n = require("@kbn/i18n");

var _types = require("../../../graphql/types");

var _lib = require("../../../lib/lib");

var _nginx = require("./nginx");

var _types2 = require("./types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var containerLayoutCreator = function containerLayoutCreator(theme) {
  return [{
    id: 'containerOverview',
    label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.layoutLabel', {
      defaultMessage: 'Container'
    }),
    sections: [{
      id: _types.InfraMetric.containerOverview,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.overviewSection.sectionLabel', {
        defaultMessage: 'Overview'
      }),
      requires: ['docker.cpu', 'docker.memory', 'docker.network'],
      type: _types2.InfraMetricLayoutSectionType.gauges,
      visConfig: {
        seriesOverrides: {
          cpu: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.overviewSection.cpuUsageSeriesLabel', {
              defaultMessage: 'CPU Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          memory: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.overviewSection.memoryUsageSeriesLabel', {
              defaultMessage: 'Memory Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          rx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.overviewSection.inboundRXSeriesLabel', {
              defaultMessage: 'Inbound (RX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          },
          tx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.overviewSection.outboundTXSeriesLabel', {
              defaultMessage: 'Outbound (TX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          }
        }
      }
    }, {
      id: _types.InfraMetric.containerCpuUsage,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.cpuUsageSection.sectionLabel', {
        defaultMessage: 'CPU Usage'
      }),
      requires: ['docker.cpu'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        stacked: true,
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.percent,
        seriesOverrides: {
          cpu: {
            color: theme.eui.euiColorVis1
          }
        }
      }
    }, {
      id: _types.InfraMetric.containerMemory,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.memoryUsageSection.sectionLabel', {
        defaultMessage: 'Memory Usage'
      }),
      requires: ['docker.memory'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        stacked: true,
        type: _types2.InfraMetricLayoutVisualizationType.area,
        formatter: _lib.InfraFormatterType.percent,
        seriesOverrides: {
          memory: {
            color: theme.eui.euiColorVis1
          }
        }
      }
    }, {
      id: _types.InfraMetric.containerNetworkTraffic,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.networkTrafficSection.sectionLabel', {
        defaultMessage: 'Network Traffic'
      }),
      requires: ['docker.network'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bits,
        formatterTemplate: '{{value}}/s',
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          rx: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.networkTrafficSection.networkRxRateSeriesLabel', {
              defaultMessage: 'in'
            })
          },
          tx: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.networkTrafficSection.networkTxRateSeriesLabel', {
              defaultMessage: 'out'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.containerDiskIOOps,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoOpsSection.sectionLabel', {
        defaultMessage: 'Disk IO (Ops)'
      }),
      requires: ['docker.diskio'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.number,
        formatterTemplate: '{{value}}/s',
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          read: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoOpsSection.readRateSeriesLabel', {
              defaultMessage: 'reads'
            })
          },
          write: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoOpsSection.writeRateSeriesLabel', {
              defaultMessage: 'writes'
            })
          }
        }
      }
    }, {
      id: _types.InfraMetric.containerDiskIOBytes,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoBytesSection.sectionLabel', {
        defaultMessage: 'Disk IO (Bytes)'
      }),
      requires: ['docker.diskio'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bytes,
        formatterTemplate: '{{value}}/s',
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          read: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoBytesSection.readRateSeriesLabel', {
              defaultMessage: 'reads'
            })
          },
          write: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.containerMetricsLayout.diskIoBytesSection.writeRateSeriesLabel', {
              defaultMessage: 'writes'
            })
          }
        }
      }
    }]
  }].concat(_toConsumableArray((0, _nginx.nginxLayoutCreator)(theme)));
};

exports.containerLayoutCreator = containerLayoutCreator;