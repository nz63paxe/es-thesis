"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getNodeLogsUrl = exports.RedirectToNodeLogs = void 0;

var _react = require("@kbn/i18n/react");

var _compose = _interopRequireDefault(require("lodash/fp/compose"));

var _react2 = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _loading_page = require("../../components/loading_page");

var _with_log_filter = require("../../containers/logs/with_log_filter");

var _with_log_position = require("../../containers/logs/with_log_position");

var _source_id = require("../../containers/source_id");

var _query_params = require("./query_params");

var _source = require("../../containers/source/source");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var RedirectToNodeLogs = (0, _react.injectI18n)(function (_ref) {
  var _ref$match$params = _ref.match.params,
      nodeId = _ref$match$params.nodeId,
      nodeType = _ref$match$params.nodeType,
      _ref$match$params$sou = _ref$match$params.sourceId,
      sourceId = _ref$match$params$sou === void 0 ? 'default' : _ref$match$params$sou,
      location = _ref.location,
      intl = _ref.intl;

  var _useSource = (0, _source.useSource)({
    sourceId: sourceId
  }),
      source = _useSource.source,
      isLoading = _useSource.isLoading;

  var configuration = source && source.configuration;

  if (isLoading) {
    return _react2.default.createElement(_loading_page.LoadingPage, {
      message: intl.formatMessage({
        id: 'xpack.infra.redirectToNodeLogs.loadingNodeLogsMessage',
        defaultMessage: 'Loading {nodeType} logs'
      }, {
        nodeType: nodeType
      })
    });
  }

  if (!configuration) {
    return null;
  }

  var nodeFilter = "".concat(configuration.fields[nodeType], ": ").concat(nodeId);
  var userFilter = (0, _query_params.getFilterFromLocation)(location);
  var filter = userFilter ? "(".concat(nodeFilter, ") and (").concat(userFilter, ")") : nodeFilter;
  var searchString = (0, _compose.default)((0, _with_log_filter.replaceLogFilterInQueryString)(filter), (0, _with_log_position.replaceLogPositionInQueryString)((0, _query_params.getTimeFromLocation)(location)), (0, _source_id.replaceSourceIdInQueryString)(sourceId))('');
  return _react2.default.createElement(_reactRouterDom.Redirect, {
    to: "/logs?".concat(searchString)
  });
});
exports.RedirectToNodeLogs = RedirectToNodeLogs;

var getNodeLogsUrl = function getNodeLogsUrl(_ref2) {
  var nodeId = _ref2.nodeId,
      nodeType = _ref2.nodeType,
      time = _ref2.time;
  return ["#/link-to/".concat(nodeType, "-logs/"), nodeId].concat(_toConsumableArray(time ? ["?time=".concat(time)] : [])).join('');
};

exports.getNodeLogsUrl = getNodeLogsUrl;