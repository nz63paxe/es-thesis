"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogMinimap = void 0;

var _d3Scale = require("d3-scale");

var React = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _density_chart = require("./density_chart");

var _highlighted_interval = require("./highlighted_interval");

var _search_markers = require("./search_markers");

var _time_ruler = require("./time_ruler");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  background: ", ";\n  & ", " {\n    visibility: hidden;\n  }\n  &:hover ", " {\n    visibility: visible;\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  pointer-events: none;\n  stroke-width: 1px;\n  stroke: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  stroke: ", ";\n  stroke-width: 1px;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  fill: transparent;\n  cursor: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function calculateYScale(target, height, intervalSize) {
  var domainStart = target ? target - intervalSize / 2 : 0;
  var domainEnd = target ? target + intervalSize / 2 : 0;
  return (0, _d3Scale.scaleLinear)().domain([domainStart, domainEnd]).range([0, height]);
}

var LogMinimap =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LogMinimap, _React$Component);

  function LogMinimap(props) {
    var _this;

    _classCallCheck(this, LogMinimap);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LogMinimap).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "dragTargetArea", null);

    _defineProperty(_assertThisInitialized(_this), "handleClick", function (event) {
      var svgPosition = _this.state.svgPosition;
      var clickedYPosition = event.clientY - svgPosition.top;
      var clickedTime = Math.floor(_this.getYScale().invert(clickedYPosition));

      _this.setState({
        drag: null
      });

      _this.props.jumpToTarget({
        tiebreaker: 0,
        time: clickedTime
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleMouseDown", function (event) {
      var clientY = event.clientY,
          target = event.target;

      if (target === _this.dragTargetArea) {
        var svgPosition = event.currentTarget.getBoundingClientRect();

        _this.setState({
          drag: {
            startY: clientY,
            currentY: null
          },
          svgPosition: svgPosition
        });

        window.addEventListener('mousemove', _this.handleDragMove);
      }

      window.addEventListener('mouseup', _this.handleMouseUp);
    });

    _defineProperty(_assertThisInitialized(_this), "handleMouseUp", function (event) {
      window.removeEventListener('mousemove', _this.handleDragMove);
      window.removeEventListener('mouseup', _this.handleMouseUp);
      var _this$state = _this.state,
          drag = _this$state.drag,
          svgPosition = _this$state.svgPosition;

      if (!drag || !drag.currentY) {
        _this.handleClick(event);

        return;
      }

      var getTime = function getTime(pos) {
        return Math.floor(_this.getYScale().invert(pos));
      };

      var startYPosition = drag.startY - svgPosition.top;
      var endYPosition = event.clientY - svgPosition.top;
      var startTime = getTime(startYPosition);
      var endTime = getTime(endYPosition);
      var timeDifference = endTime - startTime;
      var newTime = (_this.props.target || 0) - timeDifference;

      _this.setState({
        drag: null,
        target: newTime
      });

      _this.props.jumpToTarget({
        tiebreaker: 0,
        time: newTime
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleDragMove", function (event) {
      var drag = _this.state.drag;
      if (!drag) return;

      _this.setState({
        drag: _objectSpread({}, drag, {
          currentY: event.clientY
        })
      });
    });

    _defineProperty(_assertThisInitialized(_this), "getYScale", function () {
      var target = _this.state.target;
      var _this$props = _this.props,
          height = _this$props.height,
          intervalSize = _this$props.intervalSize;
      return calculateYScale(target, height, intervalSize);
    });

    _defineProperty(_assertThisInitialized(_this), "getPositionOfTime", function (time) {
      var _this$props2 = _this.props,
          height = _this$props2.height,
          intervalSize = _this$props2.intervalSize;

      var _this$getYScale$domai = _this.getYScale().domain(),
          _this$getYScale$domai2 = _slicedToArray(_this$getYScale$domai, 1),
          minTime = _this$getYScale$domai2[0];

      return (time - minTime) * height / intervalSize; //
    });

    _defineProperty(_assertThisInitialized(_this), "updateTimeCursor", function (event) {
      var svgPosition = event.currentTarget.getBoundingClientRect();
      var timeCursorY = event.clientY - svgPosition.top;

      _this.setState({
        timeCursorY: timeCursorY
      });
    });

    _this.state = {
      timeCursorY: 0,
      target: props.target,
      drag: null,
      svgPosition: {
        width: 0,
        height: 0,
        top: 0,
        right: 0,
        bottom: 0,
        left: 0
      }
    };
    return _this;
  }

  _createClass(LogMinimap, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props3 = this.props,
          className = _this$props3.className,
          height = _this$props3.height,
          highlightedInterval = _this$props3.highlightedInterval,
          jumpToTarget = _this$props3.jumpToTarget,
          summaryBuckets = _this$props3.summaryBuckets,
          summaryHighlightBuckets = _this$props3.summaryHighlightBuckets,
          width = _this$props3.width,
          intervalSize = _this$props3.intervalSize;
      var _this$state2 = this.state,
          timeCursorY = _this$state2.timeCursorY,
          drag = _this$state2.drag,
          target = _this$state2.target; // Render the time ruler and density map beyond the visible range of time, so that
      // the user doesn't run out of ruler when they click and drag

      var overscanHeight = Math.round(window.screen.availHeight * 2.5) || height * 3;

      var _calculateYScale$doma = calculateYScale(target, overscanHeight, intervalSize * (overscanHeight / height)).domain(),
          _calculateYScale$doma2 = _slicedToArray(_calculateYScale$doma, 2),
          minTime = _calculateYScale$doma2[0],
          maxTime = _calculateYScale$doma2[1];

      var tickCount = height ? Math.round(overscanHeight / height * 144) : 12;
      var overscanTranslate = height ? -(overscanHeight - height) / 2 : 0;
      var dragTransform = !drag || !drag.currentY ? 0 : drag.currentY - drag.startY;
      return React.createElement(MinimapWrapper, {
        className: className,
        height: height,
        preserveAspectRatio: "none",
        viewBox: "0 0 ".concat(width, " ").concat(height),
        width: width,
        onMouseDown: this.handleMouseDown,
        onMouseMove: this.updateTimeCursor,
        showOverscanBoundaries: Boolean(height && summaryBuckets.length)
      }, React.createElement("g", {
        transform: "translate(0, ".concat(dragTransform + overscanTranslate, ")")
      }, React.createElement(_density_chart.DensityChart, {
        buckets: summaryBuckets,
        start: minTime,
        end: maxTime,
        width: width,
        height: overscanHeight
      }), React.createElement(MinimapBorder, {
        x1: width / 3,
        y1: 0,
        x2: width / 3,
        y2: overscanHeight
      }), React.createElement(_time_ruler.TimeRuler, {
        start: minTime,
        end: maxTime,
        width: width,
        height: overscanHeight,
        tickCount: tickCount
      })), highlightedInterval ? React.createElement(_highlighted_interval.HighlightedInterval, {
        end: highlightedInterval.end,
        getPositionOfTime: this.getPositionOfTime,
        start: highlightedInterval.start,
        width: width,
        target: target
      }) : null, React.createElement("g", {
        transform: "translate(".concat(width * 0.5, ", 0)")
      }, React.createElement(_search_markers.SearchMarkers, {
        buckets: summaryHighlightBuckets || [],
        start: minTime,
        end: maxTime,
        width: width / 2,
        height: height,
        jumpToTarget: jumpToTarget
      })), React.createElement(TimeCursor, {
        x1: width / 3,
        x2: width,
        y1: timeCursorY,
        y2: timeCursorY
      }), React.createElement(DragTargetArea, {
        isGrabbing: Boolean(drag),
        innerRef: function innerRef(node) {
          _this2.dragTargetArea = node;
        },
        x: 0,
        y: 0,
        width: width / 3,
        height: height
      }));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(_ref, _ref2) {
      var target = _ref.target;
      var drag = _ref2.drag;

      if (!drag) {
        return {
          target: target
        };
      }

      return null;
    }
  }]);

  return LogMinimap;
}(React.Component);

exports.LogMinimap = LogMinimap;

var DragTargetArea = _eui_styled_components.default.rect(_templateObject(), function (_ref3) {
  var isGrabbing = _ref3.isGrabbing;
  return isGrabbing ? 'grabbing' : 'grab';
});

var MinimapBorder = _eui_styled_components.default.line(_templateObject2(), function (props) {
  return props.theme.eui.euiColorMediumShade;
});

var TimeCursor = _eui_styled_components.default.line(_templateObject3(), function (props) {
  return props.theme.darkMode ? props.theme.eui.euiColorDarkestShade : props.theme.eui.euiColorDarkShade;
});

var MinimapWrapper = _eui_styled_components.default.svg(_templateObject4(), function (props) {
  return props.showOverscanBoundaries ? props.theme.eui.euiColorMediumShade : 'transparent';
}, TimeCursor, TimeCursor);