"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initialLogEntriesState = void 0;

var _remote_graphql_state = require("../../../utils/remote_state/remote_graphql_state");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialLogEntriesState = (0, _remote_graphql_state.createGraphqlInitialState)();
exports.initialLogEntriesState = initialLogEntriesState;