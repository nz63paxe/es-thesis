"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useKibanaInjectedVar = void 0;

var _new_platform = require("ui/new_platform");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */

/**
 * This hook provides access to the "injected variables" provided by the
 * platform. While it doesn't need to be a hook right now, it has been written
 * as one in order to keep the API stable despite the upcoming injection
 * through the context after the new platform migration.
 */
var useKibanaInjectedVar = function useKibanaInjectedVar(name, defaultValue) {
  var injectedMetadata = _new_platform.npSetup.core.injectedMetadata;
  return injectedMetadata.getInjectedVar(name, defaultValue);
};

exports.useKibanaInjectedVar = useKibanaInjectedVar;