"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.flyoutItemQuery = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

var _shared = require("../../../common/graphql/shared");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  query FlyoutItemQuery($sourceId: ID!, $itemId: ID!) {\n    source(id: $sourceId) {\n      id\n      logItem(id: $itemId) {\n        id\n        index\n        key {\n          ...InfraTimeKeyFields\n        }\n        fields {\n          field\n          value\n        }\n      }\n    }\n  }\n\n  ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var flyoutItemQuery = (0, _graphqlTag.default)(_templateObject(), _shared.sharedFragments.InfraTimeKey);
exports.flyoutItemQuery = flyoutItemQuery;