"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraKibanaFrameworkAdapter = void 0;

var ReactDOM = _interopRequireWildcard(require("react-dom"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ROOT_ELEMENT_ID = 'react-infra-root';
var BREADCRUMBS_ELEMENT_ID = 'react-infra-breadcrumbs';

var InfraKibanaFrameworkAdapter = function InfraKibanaFrameworkAdapter(uiModule, _uiRoutes, timezoneProvider) {
  var _this = this;

  _classCallCheck(this, InfraKibanaFrameworkAdapter);

  _defineProperty(this, "appState", void 0);

  _defineProperty(this, "kbnVersion", void 0);

  _defineProperty(this, "timezone", void 0);

  _defineProperty(this, "adapterService", void 0);

  _defineProperty(this, "timezoneProvider", void 0);

  _defineProperty(this, "rootComponent", null);

  _defineProperty(this, "breadcrumbsComponent", null);

  _defineProperty(this, "setUISettings", function (key, value) {
    _this.adapterService.callOrBuffer(function (_ref) {
      var config = _ref.config;
      config.set(key, value);
    });
  });

  _defineProperty(this, "render", function (component) {
    _this.adapterService.callOrBuffer(function () {
      return _this.rootComponent = component;
    });
  });

  _defineProperty(this, "renderBreadcrumbs", function (component) {
    _this.adapterService.callOrBuffer(function () {
      return _this.breadcrumbsComponent = component;
    });
  });

  _defineProperty(this, "register", function (adapterModule, uiRoutes) {
    adapterModule.provider('kibanaAdapter', _this.adapterService);
    adapterModule.directive('infraUiKibanaAdapter', function () {
      return {
        controller: function controller($scope, $element) {
          return {
            $onDestroy: function $onDestroy() {
              var targetRootElement = $element[0].querySelector("#".concat(ROOT_ELEMENT_ID));
              var targetBreadcrumbsElement = $element[0].querySelector("#".concat(ROOT_ELEMENT_ID));

              if (targetRootElement) {
                ReactDOM.unmountComponentAtNode(targetRootElement);
              }

              if (targetBreadcrumbsElement) {
                ReactDOM.unmountComponentAtNode(targetBreadcrumbsElement);
              }
            },
            $onInit: function $onInit() {
              $scope.topNavMenu = [];
            },
            $postLink: function $postLink() {
              $scope.$watchGroup([function () {
                return _this.breadcrumbsComponent;
              }, function () {
                return $element[0].querySelector("#".concat(BREADCRUMBS_ELEMENT_ID));
              }], function (_ref2) {
                var _ref3 = _slicedToArray(_ref2, 2),
                    breadcrumbsComponent = _ref3[0],
                    targetElement = _ref3[1];

                if (!targetElement) {
                  return;
                }

                if (breadcrumbsComponent) {
                  ReactDOM.render(breadcrumbsComponent, targetElement);
                } else {
                  ReactDOM.unmountComponentAtNode(targetElement);
                }
              });
              $scope.$watchGroup([function () {
                return _this.rootComponent;
              }, function () {
                return $element[0].querySelector("#".concat(ROOT_ELEMENT_ID));
              }], function (_ref4) {
                var _ref5 = _slicedToArray(_ref4, 2),
                    rootComponent = _ref5[0],
                    targetElement = _ref5[1];

                if (!targetElement) {
                  return;
                }

                if (rootComponent) {
                  ReactDOM.render(rootComponent, targetElement);
                } else {
                  ReactDOM.unmountComponentAtNode(targetElement);
                }
              });
            }
          };
        },
        scope: true,
        template: "\n        <div\n          id=\"".concat(ROOT_ELEMENT_ID, "\"\n          class=\"infReactRoot\"\n        ></div>\n      ")
      };
    });
    adapterModule.run(function (config, kbnVersion, Private, kibanaAdapter) {
      _this.timezone = Private(_this.timezoneProvider)();
      _this.kbnVersion = kbnVersion;
    });
    uiRoutes.enable();
    uiRoutes.otherwise({
      reloadOnSearch: false,
      template: '<infra-ui-kibana-adapter style="display: flex; align-items: stretch; flex: 1 0 0%;"></infra-ui-kibana-adapter>'
    });
  });

  this.adapterService = new KibanaAdapterServiceProvider();
  this.timezoneProvider = timezoneProvider;
  this.appState = {};
  this.register(uiModule, _uiRoutes);
};

exports.InfraKibanaFrameworkAdapter = InfraKibanaFrameworkAdapter;

var KibanaAdapterServiceProvider =
/*#__PURE__*/
function () {
  function KibanaAdapterServiceProvider() {
    _classCallCheck(this, KibanaAdapterServiceProvider);

    _defineProperty(this, "serviceRefs", null);

    _defineProperty(this, "bufferedCalls", []);
  }

  _createClass(KibanaAdapterServiceProvider, [{
    key: "$get",
    value: function $get($rootScope, config) {
      this.serviceRefs = {
        config: config,
        rootScope: $rootScope
      };
      this.applyBufferedCalls(this.bufferedCalls);
      return this;
    }
  }, {
    key: "callOrBuffer",
    value: function callOrBuffer(serviceCall) {
      if (this.serviceRefs !== null) {
        this.applyBufferedCalls([serviceCall]);
      } else {
        this.bufferedCalls.push(serviceCall);
      }
    }
  }, {
    key: "applyBufferedCalls",
    value: function applyBufferedCalls(bufferedCalls) {
      var _this2 = this;

      if (!this.serviceRefs) {
        return;
      }

      this.serviceRefs.rootScope.$apply(function () {
        bufferedCalls.forEach(function (serviceCall) {
          if (!_this2.serviceRefs) {
            return;
          }

          return serviceCall(_this2.serviceRefs);
        });
      });
    }
  }]);

  return KibanaAdapterServiceProvider;
}();