"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLogEntryRate = void 0;

var _react = require("react");

var _kfetch = require("ui/kfetch");

var _log_analysis = require("../../../../common/http_api/log_analysis");

var _runtime_types = require("../../../../common/runtime_types");

var _use_tracked_promise = require("../../../utils/use_tracked_promise");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var useLogEntryRate = function useLogEntryRate(_ref) {
  var sourceId = _ref.sourceId,
      startTime = _ref.startTime,
      endTime = _ref.endTime,
      bucketDuration = _ref.bucketDuration;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      logEntryRate = _useState2[0],
      setLogEntryRate = _useState2[1];

  var _useTrackedPromise = (0, _use_tracked_promise.useTrackedPromise)({
    cancelPreviousOn: 'resolution',
    createPromise: function () {
      var _createPromise = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee() {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return (0, _kfetch.kfetch)({
                  method: 'POST',
                  pathname: _log_analysis.LOG_ANALYSIS_GET_LOG_ENTRY_RATE_PATH,
                  body: JSON.stringify(_log_analysis.getLogEntryRateRequestPayloadRT.encode({
                    data: {
                      sourceId: sourceId,
                      timeRange: {
                        startTime: startTime,
                        endTime: endTime
                      },
                      bucketDuration: bucketDuration
                    }
                  }))
                });

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function createPromise() {
        return _createPromise.apply(this, arguments);
      }

      return createPromise;
    }(),
    onResolve: function onResolve(response) {
      var _getLogEntryRateSucce = _log_analysis.getLogEntryRateSuccessReponsePayloadRT.decode(response).getOrElseL((0, _runtime_types.throwErrors)(_runtime_types.createPlainError)),
          data = _getLogEntryRateSucce.data;

      setLogEntryRate(data);
    }
  }, [sourceId, startTime, endTime, bucketDuration]),
      _useTrackedPromise2 = _slicedToArray(_useTrackedPromise, 2),
      getLogEntryRateRequest = _useTrackedPromise2[0],
      getLogEntryRate = _useTrackedPromise2[1];

  var isLoading = (0, _react.useMemo)(function () {
    return getLogEntryRateRequest.state === 'pending';
  }, [getLogEntryRateRequest.state]);
  return {
    getLogEntryRate: getLogEntryRate,
    isLoading: isLoading,
    logEntryRate: logEntryRate
  };
};

exports.useLogEntryRate = useLogEntryRate;