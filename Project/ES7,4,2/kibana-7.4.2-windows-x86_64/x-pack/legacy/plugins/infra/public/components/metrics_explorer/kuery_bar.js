"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsExplorerKueryBar = void 0;

var _esQuery = require("@kbn/es-query");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _with_kuery_autocompletion = require("../../containers/with_kuery_autocompletion");

var _autocomplete_field = require("../autocomplete_field");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function validateQuery(query) {
  try {
    (0, _esQuery.fromKueryExpression)(query);
  } catch (err) {
    return false;
  }

  return true;
}

var MetricsExplorerKueryBar = (0, _react.injectI18n)(function (_ref) {
  var intl = _ref.intl,
      derivedIndexPattern = _ref.derivedIndexPattern,
      onSubmit = _ref.onSubmit,
      value = _ref.value;

  var _useState = (0, _react2.useState)(value || ''),
      _useState2 = _slicedToArray(_useState, 2),
      draftQuery = _useState2[0],
      setDraftQuery = _useState2[1];

  var _useState3 = (0, _react2.useState)(true),
      _useState4 = _slicedToArray(_useState3, 2),
      isValid = _useState4[0],
      setValidation = _useState4[1]; // This ensures that if value changes out side this component it will update.


  (0, _react2.useEffect)(function () {
    if (value) {
      setDraftQuery(value);
    }
  }, [value]);

  var handleChange = function handleChange(query) {
    setValidation(validateQuery(query));
    setDraftQuery(query);
  };

  return _react2.default.createElement(_with_kuery_autocompletion.WithKueryAutocompletion, {
    indexPattern: derivedIndexPattern
  }, function (_ref2) {
    var isLoadingSuggestions = _ref2.isLoadingSuggestions,
        loadSuggestions = _ref2.loadSuggestions,
        suggestions = _ref2.suggestions;
    return _react2.default.createElement(_autocomplete_field.AutocompleteField, {
      isLoadingSuggestions: isLoadingSuggestions,
      isValid: isValid,
      loadSuggestions: loadSuggestions,
      onChange: handleChange,
      onSubmit: onSubmit,
      placeholder: intl.formatMessage({
        id: 'xpack.infra.homePage.toolbar.kqlSearchFieldPlaceholder',
        defaultMessage: 'Search for infrastructure data… (e.g. host.name:host-1)'
      }),
      suggestions: suggestions,
      value: draftQuery
    });
  });
});
exports.MetricsExplorerKueryBar = MetricsExplorerKueryBar;