"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ScrollableLogTextStreamView", {
  enumerable: true,
  get: function get() {
    return _scrollable_log_text_stream_view.ScrollableLogTextStreamView;
  }
});

var _scrollable_log_text_stream_view = require("./scrollable_log_text_stream_view");