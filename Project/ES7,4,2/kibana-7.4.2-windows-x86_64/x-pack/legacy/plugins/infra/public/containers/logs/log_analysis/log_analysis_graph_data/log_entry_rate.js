"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLogEntryRateGraphData = void 0;

var _react = require("react");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useLogEntryRateGraphData = function useLogEntryRateGraphData(_ref) {
  var data = _ref.data;
  var areaSeries = (0, _react.useMemo)(function () {
    if (!data || data && data.histogramBuckets && !data.histogramBuckets.length) {
      return [];
    }

    return data.histogramBuckets.reduce(function (acc, bucket) {
      acc.push({
        x: bucket.startTime,
        min: bucket.modelLowerBoundStats.min,
        max: bucket.modelUpperBoundStats.max
      });
      return acc;
    }, []);
  }, [data]);
  var lineSeries = (0, _react.useMemo)(function () {
    if (!data || data && data.histogramBuckets && !data.histogramBuckets.length) {
      return [];
    }

    return data.histogramBuckets.reduce(function (acc, bucket) {
      acc.push([bucket.startTime, bucket.logEntryRateStats.avg]);
      return acc;
    }, []);
  }, [data]);
  var anomalySeries = (0, _react.useMemo)(function () {
    if (!data || data && data.histogramBuckets && !data.histogramBuckets.length) {
      return [];
    }

    return data.histogramBuckets.reduce(function (acc, bucket) {
      if (bucket.anomalies.length > 0) {
        bucket.anomalies.forEach(function (anomaly) {
          acc.push([anomaly.startTime, anomaly.actualLogEntryRate]);
        });
        return acc;
      } else {
        return acc;
      }
    }, []);
  }, [data]);
  return {
    areaSeries: areaSeries,
    lineSeries: lineSeries,
    anomalySeries: anomalySeries
  };
};

exports.useLogEntryRateGraphData = useLogEntryRateGraphData;