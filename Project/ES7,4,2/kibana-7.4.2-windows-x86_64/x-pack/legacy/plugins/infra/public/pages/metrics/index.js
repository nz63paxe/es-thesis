"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricDetail = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _react3 = require("ui/capabilities/react");

var _eui_styled_components = _interopRequireWildcard(require("../../../../../common/eui_styled_components"));

var _errors = require("../../../common/errors");

var _auto_sizer = require("../../components/auto_sizer");

var _document_title = require("../../components/document_title");

var _header = require("../../components/header");

var _metrics = require("../../components/metrics");

var _invalid_node = require("../../components/metrics/invalid_node");

var _side_nav = require("../../components/metrics/side_nav");

var _time_controls = require("../../components/metrics/time_controls");

var _page = require("../../components/page");

var _with_metrics = require("../../containers/metrics/with_metrics");

var _with_metrics_time = require("../../containers/metrics/with_metrics_time");

var _error = require("../error");

var _layouts = require("./layouts");

var _page_providers = require("./page_providers");

var _use_metadata = require("../../containers/metadata/use_metadata");

var _source = require("../../containers/source");

var _loading = require("../../components/loading");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\n  display: flex;\n  flex-flow: row wrap;\n  justify-content: space-between;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  flex: 1 0 0%;\n  display: flex;\n  flex-direction: column;\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  position: relative;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: auto;\n  background-color: ", ";\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DetailPageContent = (0, _eui_styled_components.default)(_page.PageContent)(_templateObject(), function (props) {
  return props.theme.eui.euiColorLightestShade;
});
var EuiPageContentWithRelative = (0, _eui_styled_components.default)(_eui.EuiPageContent)(_templateObject2());
var MetricDetail = (0, _page_providers.withMetricPageProviders)((0, _react3.injectUICapabilities)((0, _eui_styled_components.withTheme)((0, _react.injectI18n)(function (_ref) {
  var intl = _ref.intl,
      uiCapabilities = _ref.uiCapabilities,
      match = _ref.match,
      theme = _ref.theme;
  var nodeId = match.params.node;
  var nodeType = match.params.type;
  var layoutCreator = _layouts.layoutCreators[nodeType];

  if (!layoutCreator) {
    return _react2.default.createElement(_error.Error, {
      message: intl.formatMessage({
        id: 'xpack.infra.metricDetailPage.invalidNodeTypeErrorMessage',
        defaultMessage: '{nodeType} is not a valid node type'
      }, {
        nodeType: "\"".concat(nodeType, "\"")
      })
    });
  }

  var _useContext = (0, _react2.useContext)(_source.Source.Context),
      sourceId = _useContext.sourceId;

  var layouts = layoutCreator(theme);

  var _useMetadata = (0, _use_metadata.useMetadata)(nodeId, nodeType, layouts, sourceId),
      name = _useMetadata.name,
      filteredLayouts = _useMetadata.filteredLayouts,
      metadataLoading = _useMetadata.loading,
      cloudId = _useMetadata.cloudId;

  var breadcrumbs = [{
    href: '#/',
    text: intl.formatMessage({
      id: 'xpack.infra.header.infrastructureTitle',
      defaultMessage: 'Infrastructure'
    })
  }, {
    text: name
  }];
  var handleClick = (0, _react2.useCallback)(function (section) {
    return function () {
      var id = section.linkToId || section.id;
      var el = document.getElementById(id);

      if (el) {
        el.scrollIntoView();
      }
    };
  }, []);

  if (metadataLoading && !filteredLayouts.length) {
    return _react2.default.createElement(_loading.InfraLoadingPanel, {
      height: "100vh",
      width: "100%",
      text: intl.formatMessage({
        id: 'xpack.infra.metrics.loadingNodeDataText',
        defaultMessage: 'Loading data'
      })
    });
  }

  return _react2.default.createElement(_with_metrics_time.WithMetricsTime, null, function (_ref2) {
    var timeRange = _ref2.timeRange,
        setTimeRange = _ref2.setTimeRange,
        refreshInterval = _ref2.refreshInterval,
        setRefreshInterval = _ref2.setRefreshInterval,
        isAutoReloading = _ref2.isAutoReloading,
        setAutoReload = _ref2.setAutoReload;
    return _react2.default.createElement(_page.ColumnarPage, null, _react2.default.createElement(_header.Header, {
      breadcrumbs: breadcrumbs,
      readOnlyBadge: !uiCapabilities.infrastructure.save
    }), _react2.default.createElement(_with_metrics_time.WithMetricsTimeUrlState, null), _react2.default.createElement(_document_title.DocumentTitle, {
      title: intl.formatMessage({
        id: 'xpack.infra.metricDetailPage.documentTitle',
        defaultMessage: 'Infrastructure | Metrics | {name}'
      }, {
        name: name
      })
    }), _react2.default.createElement(DetailPageContent, {
      "data-test-subj": "infraMetricsPage"
    }, _react2.default.createElement(_with_metrics.WithMetrics, {
      layouts: filteredLayouts,
      sourceId: sourceId,
      timerange: timeRange,
      nodeType: nodeType,
      nodeId: nodeId,
      cloudId: cloudId
    }, function (_ref3) {
      var metrics = _ref3.metrics,
          error = _ref3.error,
          loading = _ref3.loading,
          refetch = _ref3.refetch;

      if (error) {
        var invalidNodeError = error.graphQLErrors.some(function (err) {
          return err.code === _errors.InfraMetricsErrorCodes.invalid_node;
        });
        return _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_document_title.DocumentTitle, {
          title: function title(previousTitle) {
            return intl.formatMessage({
              id: 'xpack.infra.metricDetailPage.documentTitleError',
              defaultMessage: '{previousTitle} | Uh oh'
            }, {
              previousTitle: previousTitle
            });
          }
        }), invalidNodeError ? _react2.default.createElement(_invalid_node.InvalidNodeError, {
          nodeName: name
        }) : _react2.default.createElement(_error.ErrorPageBody, {
          message: error.message
        }));
      }

      return _react2.default.createElement(_eui.EuiPage, {
        style: {
          flex: '1 0 auto'
        }
      }, _react2.default.createElement(_side_nav.MetricsSideNav, {
        layouts: filteredLayouts,
        loading: metadataLoading,
        nodeName: name,
        handleClick: handleClick
      }), _react2.default.createElement(_auto_sizer.AutoSizer, {
        content: false,
        bounds: true,
        detectAnyWindowResize: true
      }, function (_ref4) {
        var measureRef = _ref4.measureRef,
            _ref4$bounds$width = _ref4.bounds.width,
            width = _ref4$bounds$width === void 0 ? 0 : _ref4$bounds$width;
        var w = width ? "".concat(width, "px") : "100%";
        return _react2.default.createElement(MetricsDetailsPageColumn, {
          innerRef: measureRef
        }, _react2.default.createElement(_eui.EuiPageBody, {
          style: {
            width: w
          }
        }, _react2.default.createElement(_eui.EuiPageHeader, {
          style: {
            flex: '0 0 auto'
          }
        }, _react2.default.createElement(_eui.EuiPageHeaderSection, {
          style: {
            width: '100%'
          }
        }, _react2.default.createElement(MetricsTitleTimeRangeContainer, null, _react2.default.createElement(_eui.EuiHideFor, {
          sizes: ['xs', 's']
        }, _react2.default.createElement(_eui.EuiTitle, {
          size: "m"
        }, _react2.default.createElement("h1", null, name))), _react2.default.createElement(_time_controls.MetricsTimeControls, {
          currentTimeRange: timeRange,
          isLiveStreaming: isAutoReloading,
          refreshInterval: refreshInterval,
          setRefreshInterval: setRefreshInterval,
          onChangeTimeRange: setTimeRange,
          setAutoReload: setAutoReload
        })))), _react2.default.createElement(EuiPageContentWithRelative, null, _react2.default.createElement(_metrics.Metrics, {
          label: name,
          nodeId: nodeId,
          layouts: filteredLayouts,
          metrics: metrics,
          loading: metrics.length > 0 && isAutoReloading ? false : loading,
          refetch: refetch,
          onChangeRangeTime: setTimeRange,
          isLiveStreaming: isAutoReloading,
          stopLiveStreaming: function stopLiveStreaming() {
            return setAutoReload(false);
          }
        }))));
      }));
    })));
  });
}))));
exports.MetricDetail = MetricDetail;

var MetricsDetailsPageColumn = _eui_styled_components.default.div(_templateObject3());

var MetricsTitleTimeRangeContainer = _eui_styled_components.default.div(_templateObject4());