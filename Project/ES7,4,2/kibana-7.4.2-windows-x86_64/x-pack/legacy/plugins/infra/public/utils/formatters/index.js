"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createFormatter = exports.FORMATTERS = void 0;

var _mustache = _interopRequireDefault(require("mustache"));

var _lib = require("../../lib/lib");

var _bytes = require("./bytes");

var _number = require("./number");

var _percent = require("./percent");

var _FORMATTERS;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var FORMATTERS = (_FORMATTERS = {}, _defineProperty(_FORMATTERS, _lib.InfraFormatterType.number, _number.formatNumber), _defineProperty(_FORMATTERS, _lib.InfraFormatterType.abbreviatedNumber, (0, _bytes.createBytesFormatter)(_lib.InfraWaffleMapDataFormat.abbreviatedNumber)), _defineProperty(_FORMATTERS, _lib.InfraFormatterType.bytes, (0, _bytes.createBytesFormatter)(_lib.InfraWaffleMapDataFormat.bytesDecimal)), _defineProperty(_FORMATTERS, _lib.InfraFormatterType.bits, (0, _bytes.createBytesFormatter)(_lib.InfraWaffleMapDataFormat.bitsDecimal)), _defineProperty(_FORMATTERS, _lib.InfraFormatterType.percent, _percent.formatPercent), _FORMATTERS);
exports.FORMATTERS = FORMATTERS;

var createFormatter = function createFormatter(format) {
  var template = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '{{value}}';
  return function (val) {
    if (val == null) {
      return '';
    }

    var fmtFn = FORMATTERS[format];
    var value = fmtFn(Number(val));
    return _mustache.default.render(template, {
      value: value
    });
  };
};

exports.createFormatter = createFormatter;