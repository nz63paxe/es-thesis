"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.convertKueryToElasticSearchQuery = void 0;

var _esQuery = require("@kbn/es-query");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var convertKueryToElasticSearchQuery = function convertKueryToElasticSearchQuery(kueryExpression, indexPattern) {
  try {
    return kueryExpression ? JSON.stringify((0, _esQuery.toElasticsearchQuery)((0, _esQuery.fromKueryExpression)(kueryExpression), indexPattern)) : '';
  } catch (err) {
    return '';
  }
};

exports.convertKueryToElasticSearchQuery = convertKueryToElasticSearchQuery;