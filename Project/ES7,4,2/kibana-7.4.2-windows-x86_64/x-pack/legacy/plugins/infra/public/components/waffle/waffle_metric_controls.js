"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WaffleMetricControls = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _types = require("../../graphql/types");

var _class, _temp;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var OPTIONS;

var getOptions = function getOptions(nodeType, intl) {
  if (!OPTIONS) {
    var _OPTIONS;

    var CPUUsage = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.cpuUsageText',
      defaultMessage: 'CPU usage'
    });
    var MemoryUsage = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.memoryUsageText',
      defaultMessage: 'Memory usage'
    });
    var InboundTraffic = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.inboundTrafficText',
      defaultMessage: 'Inbound traffic'
    });
    var OutboundTraffic = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.outboundTrafficText',
      defaultMessage: 'Outbound traffic'
    });
    var LogRate = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.hostLogRateText',
      defaultMessage: 'Log rate'
    });
    var Load = intl.formatMessage({
      id: 'xpack.infra.waffle.metricOptions.loadText',
      defaultMessage: 'Load'
    });
    OPTIONS = (_OPTIONS = {}, _defineProperty(_OPTIONS, _types.InfraNodeType.pod, [{
      text: CPUUsage,
      value: _types.InfraSnapshotMetricType.cpu
    }, {
      text: MemoryUsage,
      value: _types.InfraSnapshotMetricType.memory
    }, {
      text: InboundTraffic,
      value: _types.InfraSnapshotMetricType.rx
    }, {
      text: OutboundTraffic,
      value: _types.InfraSnapshotMetricType.tx
    }]), _defineProperty(_OPTIONS, _types.InfraNodeType.container, [{
      text: CPUUsage,
      value: _types.InfraSnapshotMetricType.cpu
    }, {
      text: MemoryUsage,
      value: _types.InfraSnapshotMetricType.memory
    }, {
      text: InboundTraffic,
      value: _types.InfraSnapshotMetricType.rx
    }, {
      text: OutboundTraffic,
      value: _types.InfraSnapshotMetricType.tx
    }]), _defineProperty(_OPTIONS, _types.InfraNodeType.host, [{
      text: CPUUsage,
      value: _types.InfraSnapshotMetricType.cpu
    }, {
      text: MemoryUsage,
      value: _types.InfraSnapshotMetricType.memory
    }, {
      text: Load,
      value: _types.InfraSnapshotMetricType.load
    }, {
      text: InboundTraffic,
      value: _types.InfraSnapshotMetricType.rx
    }, {
      text: OutboundTraffic,
      value: _types.InfraSnapshotMetricType.tx
    }, {
      text: LogRate,
      value: _types.InfraSnapshotMetricType.logRate
    }]), _OPTIONS);
  }

  return OPTIONS[nodeType];
};

var initialState = {
  isPopoverOpen: false
};
var WaffleMetricControls = (0, _react.injectI18n)((_temp = _class =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(_class, _React$PureComponent);

  function _class() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, _class);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(_class)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", initialState);

    _defineProperty(_assertThisInitialized(_this), "handleClose", function () {
      _this.setState({
        isPopoverOpen: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleToggle", function () {
      _this.setState(function (state) {
        return {
          isPopoverOpen: !state.isPopoverOpen
        };
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleClick", function (value) {
      return function () {
        _this.props.onChange({
          type: value
        });

        _this.handleClose();
      };
    });

    return _this;
  }

  _createClass(_class, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          metric = _this$props.metric,
          nodeType = _this$props.nodeType,
          intl = _this$props.intl;
      var options = getOptions(nodeType, intl);
      var value = metric.type;

      if (!options.length || !value) {
        throw Error(intl.formatMessage({
          id: 'xpack.infra.waffle.unableToSelectMetricErrorTitle',
          defaultMessage: 'Unable to select options or value for metric.'
        }));
      }

      var currentLabel = options.find(function (o) {
        return o.value === metric.type;
      });

      if (!currentLabel) {
        return 'null';
      }

      var panels = [{
        id: 0,
        title: '',
        items: options.map(function (o) {
          var icon = o.value === metric.type ? 'check' : 'empty';
          var panel = {
            name: o.text,
            onClick: _this2.handleClick(o.value),
            icon: icon
          };
          return panel;
        })
      }];

      var button = _react2.default.createElement(_eui.EuiFilterButton, {
        iconType: "arrowDown",
        onClick: this.handleToggle
      }, _react2.default.createElement(_react.FormattedMessage, {
        id: "xpack.infra.waffle.metricButtonLabel",
        defaultMessage: "Metric: {selectedMetric}",
        values: {
          selectedMetric: currentLabel.text
        }
      }));

      return _react2.default.createElement(_eui.EuiFilterGroup, null, _react2.default.createElement(_eui.EuiPopover, {
        isOpen: this.state.isPopoverOpen,
        id: "metricsPanel",
        button: button,
        panelPaddingSize: "none",
        closePopover: this.handleClose
      }, _react2.default.createElement(_eui.EuiContextMenu, {
        initialPanelId: 0,
        panels: panels
      })));
    }
  }]);

  return _class;
}(_react2.default.PureComponent), _defineProperty(_class, "displayName", 'WaffleMetricControls'), _temp));
exports.WaffleMetricControls = WaffleMetricControls;