"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogAnalysisResults = exports.useLogAnalysisResults = void 0;

var _constateLatest = _interopRequireDefault(require("constate-latest"));

var _react = require("react");

var _log_entry_rate = require("./log_entry_rate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var useLogAnalysisResults = function useLogAnalysisResults(_ref) {
  var sourceId = _ref.sourceId,
      startTime = _ref.startTime,
      endTime = _ref.endTime,
      _ref$bucketDuration = _ref.bucketDuration,
      bucketDuration = _ref$bucketDuration === void 0 ? 15 * 60 * 1000 : _ref$bucketDuration;

  var _useLogEntryRate = (0, _log_entry_rate.useLogEntryRate)({
    sourceId: sourceId,
    startTime: startTime,
    endTime: endTime,
    bucketDuration: bucketDuration
  }),
      isLoadingLogEntryRate = _useLogEntryRate.isLoading,
      logEntryRate = _useLogEntryRate.logEntryRate,
      getLogEntryRate = _useLogEntryRate.getLogEntryRate;

  var isLoading = (0, _react.useMemo)(function () {
    return isLoadingLogEntryRate;
  }, [isLoadingLogEntryRate]);
  (0, _react.useEffect)(function () {
    getLogEntryRate();
  }, [sourceId, startTime, endTime, bucketDuration]);
  return {
    isLoading: isLoading,
    logEntryRate: logEntryRate
  };
};

exports.useLogAnalysisResults = useLogAnalysisResults;
var LogAnalysisResults = (0, _constateLatest.default)(useLogAnalysisResults);
exports.LogAnalysisResults = LogAnalysisResults;