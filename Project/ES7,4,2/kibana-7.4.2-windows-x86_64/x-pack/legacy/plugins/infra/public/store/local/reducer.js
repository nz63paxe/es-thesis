"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.localReducer = exports.initialLocalState = void 0;

var _redux = require("redux");

var _log_filter = require("./log_filter");

var _log_position = require("./log_position");

var _waffle_filter = require("./waffle_filter");

var _waffle_options = require("./waffle_options");

var _waffle_time = require("./waffle_time");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var initialLocalState = {
  logFilter: _log_filter.initialLogFilterState,
  logPosition: _log_position.initialLogPositionState,
  waffleFilter: _waffle_filter.initialWaffleFilterState,
  waffleTime: _waffle_time.initialWaffleTimeState,
  waffleMetrics: _waffle_options.initialWaffleOptionsState
};
exports.initialLocalState = initialLocalState;
var localReducer = (0, _redux.combineReducers)({
  logFilter: _log_filter.logFilterReducer,
  logPosition: _log_position.logPositionReducer,
  waffleFilter: _waffle_filter.waffleFilterReducer,
  waffleTime: _waffle_time.waffleTimeReducer,
  waffleMetrics: _waffle_options.waffleOptionsReducer
});
exports.localReducer = localReducer;