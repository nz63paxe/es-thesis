"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createRemoteEpic = void 0;

var _log_entries = require("./log_entries");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var createRemoteEpic = function createRemoteEpic() {
  return (0, _log_entries.createLogEntriesEpic)();
};

exports.createRemoteEpic = createRemoteEpic;