"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithWaffleNodes = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _waffle_nodes = require("./waffle_nodes.gql_query");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var WithWaffleNodes = function WithWaffleNodes(_ref) {
  var children = _ref.children,
      filterQuery = _ref.filterQuery,
      metric = _ref.metric,
      groupBy = _ref.groupBy,
      nodeType = _ref.nodeType,
      sourceId = _ref.sourceId,
      timerange = _ref.timerange;
  return _react.default.createElement(_reactApollo.Query, {
    query: _waffle_nodes.waffleNodesQuery,
    fetchPolicy: "network-only",
    notifyOnNetworkStatusChange: true,
    variables: {
      sourceId: sourceId,
      metric: metric,
      groupBy: _toConsumableArray(groupBy),
      type: nodeType,
      timerange: timerange,
      filterQuery: filterQuery
    }
  }, function (_ref2) {
    var data = _ref2.data,
        loading = _ref2.loading,
        refetch = _ref2.refetch,
        error = _ref2.error;
    return children({
      loading: loading,
      nodes: !error && data && data.source && data.source.snapshot && data.source.snapshot.nodes ? data.source.snapshot.nodes : [],
      refetch: refetch
    });
  });
};

exports.WithWaffleNodes = WithWaffleNodes;