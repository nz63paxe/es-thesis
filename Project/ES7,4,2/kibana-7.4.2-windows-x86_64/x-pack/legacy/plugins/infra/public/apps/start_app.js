"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.startApp = startApp;

var _constate = require("constate");

var _history = require("history");

var _react = _interopRequireDefault(require("react"));

var _reactApollo = require("react-apollo");

var _reactRedux = require("react-redux");

var _rxjs = require("rxjs");

var _operators = require("rxjs/operators");

var _eui = require("@elastic/eui");

var _react2 = require("ui/capabilities/react");

var _i18n = require("ui/i18n");

var _new_platform = require("ui/new_platform");

var _eui_styled_components = require("../../../../common/eui_styled_components");

var _routes = require("../routes");

var _store = require("../store");

var _apollo_context = require("../utils/apollo_context");

var _history_context = require("../utils/history_context");

var _public = require("../../../../../../src/plugins/kibana_react/public");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var uiSettings = _new_platform.npStart.core.uiSettings;

function startApp(_x) {
  return _startApp.apply(this, arguments);
}

function _startApp() {
  _startApp = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(libs) {
    var history, libs$, store, InfraPluginRoot;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            history = (0, _history.createHashHistory)();
            libs$ = new _rxjs.BehaviorSubject(libs);
            store = (0, _store.createStore)({
              apolloClient: libs$.pipe((0, _operators.pluck)('apolloClient')),
              observableApi: libs$.pipe((0, _operators.pluck)('observableApi'))
            });

            InfraPluginRoot = function InfraPluginRoot() {
              var _useUiSetting$ = (0, _public.useUiSetting$)('theme:darkMode'),
                  _useUiSetting$2 = _slicedToArray(_useUiSetting$, 1),
                  darkMode = _useUiSetting$2[0];

              return _react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_react2.UICapabilitiesProvider, null, _react.default.createElement(_eui.EuiErrorBoundary, null, _react.default.createElement(_constate.Provider, {
                devtools: true
              }, _react.default.createElement(_reactRedux.Provider, {
                store: store
              }, _react.default.createElement(_reactApollo.ApolloProvider, {
                client: libs.apolloClient
              }, _react.default.createElement(_apollo_context.ApolloClientContext.Provider, {
                value: libs.apolloClient
              }, _react.default.createElement(_eui_styled_components.EuiThemeProvider, {
                darkMode: darkMode
              }, _react.default.createElement(_history_context.HistoryContext.Provider, {
                value: history
              }, _react.default.createElement(_routes.PageRouter, {
                history: history
              }))))))))));
            };

            libs.framework.render(_react.default.createElement(_public.KibanaContextProvider, {
              services: {
                uiSettings: uiSettings
              }
            }, _react.default.createElement(InfraPluginRoot, null)));

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _startApp.apply(this, arguments);
}