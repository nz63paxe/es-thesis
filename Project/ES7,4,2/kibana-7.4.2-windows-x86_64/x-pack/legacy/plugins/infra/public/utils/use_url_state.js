"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.replaceStateKeyInQueryString = exports.useUrlState = void 0;

var _react = require("react");

var _risonNode = require("rison-node");

var _query_string = require("ui/utils/query_string");

var _history_context = require("./history_context");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var useUrlState = function useUrlState(_ref) {
  var defaultState = _ref.defaultState,
      decodeUrlState = _ref.decodeUrlState,
      encodeUrlState = _ref.encodeUrlState,
      urlStateKey = _ref.urlStateKey;
  var history = (0, _history_context.useHistory)();
  var urlStateString = (0, _react.useMemo)(function () {
    if (!history) {
      return;
    }

    return getParamFromQueryString(getQueryStringFromLocation(history.location), urlStateKey);
  }, [history && history.location, urlStateKey]);
  var decodedState = (0, _react.useMemo)(function () {
    return decodeUrlState(decodeRisonUrlState(urlStateString));
  }, [decodeUrlState, urlStateString]);
  var state = (0, _react.useMemo)(function () {
    return typeof decodedState !== 'undefined' ? decodedState : defaultState;
  }, [defaultState, decodedState]);
  var setState = (0, _react.useCallback)(function (newState) {
    if (!history) {
      return;
    }

    var location = history.location;
    var newLocation = replaceQueryStringInLocation(location, replaceStateKeyInQueryString(urlStateKey, typeof newState !== 'undefined' ? encodeUrlState(newState) : undefined)(getQueryStringFromLocation(location)));

    if (newLocation !== location) {
      history.replace(newLocation);
    }
  }, [encodeUrlState, history, history && history.location, urlStateKey]);
  return [state, setState];
};

exports.useUrlState = useUrlState;

var decodeRisonUrlState = function decodeRisonUrlState(value) {
  try {
    return value ? (0, _risonNode.decode)(value) : undefined;
  } catch (error) {
    if (error instanceof Error && error.message.startsWith('rison decoder error')) {
      return {};
    }

    throw error;
  }
};

var encodeRisonUrlState = function encodeRisonUrlState(state) {
  return (0, _risonNode.encode)(state);
};

var getQueryStringFromLocation = function getQueryStringFromLocation(location) {
  return location.search.substring(1);
};

var getParamFromQueryString = function getParamFromQueryString(queryString, key) {
  var queryParam = _query_string.QueryString.decode(queryString)[key];

  return Array.isArray(queryParam) ? queryParam[0] : queryParam;
};

var replaceStateKeyInQueryString = function replaceStateKeyInQueryString(stateKey, urlState) {
  return function (queryString) {
    var previousQueryValues = _query_string.QueryString.decode(queryString);

    var encodedUrlState = typeof urlState !== 'undefined' ? encodeRisonUrlState(urlState) : undefined;
    return _query_string.QueryString.encode(_objectSpread({}, previousQueryValues, _defineProperty({}, stateKey, encodedUrlState)));
  };
};

exports.replaceStateKeyInQueryString = replaceStateKeyInQueryString;

var replaceQueryStringInLocation = function replaceQueryStringInLocation(location, queryString) {
  if (queryString === getQueryStringFromLocation(location)) {
    return location;
  } else {
    return _objectSpread({}, location, {
      search: "?".concat(queryString)
    });
  }
};