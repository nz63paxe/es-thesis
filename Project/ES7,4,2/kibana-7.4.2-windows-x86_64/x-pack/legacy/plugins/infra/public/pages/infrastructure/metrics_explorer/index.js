"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MetricsExplorerPage = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _document_title = require("../../../components/document_title");

var _charts = require("../../../components/metrics_explorer/charts");

var _toolbar = require("../../../components/metrics_explorer/toolbar");

var _empty_states = require("../../../components/empty_states");

var _use_metric_explorer_state = require("./use_metric_explorer_state");

var _use_track_metric = require("../../../hooks/use_track_metric");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var MetricsExplorerPage = (0, _react.injectI18n)(function (_ref) {
  var intl = _ref.intl,
      source = _ref.source,
      derivedIndexPattern = _ref.derivedIndexPattern;

  if (!source) {
    return null;
  }

  var _useMetricsExplorerSt = (0, _use_metric_explorer_state.useMetricsExplorerState)(source, derivedIndexPattern),
      loading = _useMetricsExplorerSt.loading,
      error = _useMetricsExplorerSt.error,
      data = _useMetricsExplorerSt.data,
      currentTimerange = _useMetricsExplorerSt.currentTimerange,
      options = _useMetricsExplorerSt.options,
      chartOptions = _useMetricsExplorerSt.chartOptions,
      setChartOptions = _useMetricsExplorerSt.setChartOptions,
      handleAggregationChange = _useMetricsExplorerSt.handleAggregationChange,
      handleMetricsChange = _useMetricsExplorerSt.handleMetricsChange,
      handleFilterQuerySubmit = _useMetricsExplorerSt.handleFilterQuerySubmit,
      handleGroupByChange = _useMetricsExplorerSt.handleGroupByChange,
      handleTimeChange = _useMetricsExplorerSt.handleTimeChange,
      handleRefresh = _useMetricsExplorerSt.handleRefresh,
      handleLoadMore = _useMetricsExplorerSt.handleLoadMore;

  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_metrics',
    path: 'metrics_explorer'
  });
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_metrics',
    path: 'metrics_explorer',
    delay: 15000
  });
  return _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_document_title.DocumentTitle, {
    title: function title(previousTitle) {
      return intl.formatMessage({
        id: 'xpack.infra.infrastructureMetricsExplorerPage.documentTitle',
        defaultMessage: '{previousTitle} | Metrics Explorer'
      }, {
        previousTitle: previousTitle
      });
    }
  }), _react2.default.createElement(_toolbar.MetricsExplorerToolbar, {
    derivedIndexPattern: derivedIndexPattern,
    timeRange: currentTimerange,
    options: options,
    chartOptions: chartOptions,
    onRefresh: handleRefresh,
    onTimeChange: handleTimeChange,
    onGroupByChange: handleGroupByChange,
    onFilterQuerySubmit: handleFilterQuerySubmit,
    onMetricsChange: handleMetricsChange,
    onAggregationChange: handleAggregationChange,
    onChartOptionsChange: setChartOptions
  }), error ? _react2.default.createElement(_empty_states.NoData, {
    titleText: "Whoops!",
    bodyText: intl.formatMessage({
      id: 'xpack.infra.metricsExplorer.errorMessage',
      defaultMessage: 'It looks like the request failed with "{message}"'
    }, {
      message: error.message
    }),
    onRefetch: handleRefresh,
    refetchText: "Try Again"
  }) : _react2.default.createElement(_charts.MetricsExplorerCharts, {
    timeRange: currentTimerange,
    loading: loading,
    data: data,
    source: source,
    options: options,
    chartOptions: chartOptions,
    onLoadMore: handleLoadMore,
    onFilter: handleFilterQuerySubmit,
    onRefetch: handleRefresh,
    onTimeChange: handleTimeChange
  }));
});
exports.MetricsExplorerPage = MetricsExplorerPage;