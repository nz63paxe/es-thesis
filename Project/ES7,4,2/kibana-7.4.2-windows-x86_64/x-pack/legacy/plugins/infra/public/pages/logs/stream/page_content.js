"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StreamPageContent = void 0;

var _react = _interopRequireWildcard(require("react"));

var _source = require("../../../containers/source");

var _page_logs_content = require("./page_logs_content");

var _page_no_indices_content = require("./page_no_indices_content");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var StreamPageContent = function StreamPageContent() {
  var _useContext = (0, _react.useContext)(_source.Source.Context),
      logIndicesExist = _useContext.logIndicesExist;

  return _react.default.createElement(_react.default.Fragment, null, logIndicesExist ? _react.default.createElement(_page_logs_content.LogsPageLogsContent, null) : _react.default.createElement(_page_no_indices_content.LogsPageNoIndicesContent, null));
};

exports.StreamPageContent = StreamPageContent;