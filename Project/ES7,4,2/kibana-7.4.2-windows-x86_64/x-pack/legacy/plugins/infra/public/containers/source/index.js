"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Source", {
  enumerable: true,
  get: function get() {
    return _source.Source;
  }
});
Object.defineProperty(exports, "useSource", {
  enumerable: true,
  get: function get() {
    return _source.useSource;
  }
});

var _source = require("./source");