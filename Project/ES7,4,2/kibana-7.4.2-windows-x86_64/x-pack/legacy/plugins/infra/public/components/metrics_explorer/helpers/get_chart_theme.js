"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getChartTheme = getChartTheme;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _charts = require("@elastic/charts");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function getChartTheme() {
  var isDarkMode = _chrome.default.getUiSettingsClient().get('theme:darkMode');

  return isDarkMode ? _charts.DARK_THEME : _charts.LIGHT_THEME;
}