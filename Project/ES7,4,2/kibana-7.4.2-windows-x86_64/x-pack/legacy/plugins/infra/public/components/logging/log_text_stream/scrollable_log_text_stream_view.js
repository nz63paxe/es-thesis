"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ScrollableLogTextStreamView = void 0;

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _handlers = require("../../../utils/handlers");

var _auto_sizer = require("../../auto_sizer");

var _empty_states = require("../../empty_states");

var _formatted_time = require("../../formatted_time");

var _loading = require("../../loading");

var _item = require("./item");

var _column_headers = require("./column_headers");

var _loading_item_view = require("./loading_item_view");

var _log_entry_row = require("./log_entry_row");

var _measurable_item_view = require("./measurable_item_view");

var _vertical_scroll_panel = require("./vertical_scroll_panel");

var _log_entry_column = require("./log_entry_column");

var _text_styles = require("./text_styles");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  flex: 1 1 0%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  overflow: hidden;\n  display: flex;\n  flex: 1 1 0%;\n  flex-direction: column;\n  align-items: stretch;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ScrollableLogTextStreamViewClass =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(ScrollableLogTextStreamViewClass, _React$PureComponent);

  function ScrollableLogTextStreamViewClass() {
    var _getPrototypeOf2;

    var _this;

    _classCallCheck(this, ScrollableLogTextStreamViewClass);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = _possibleConstructorReturn(this, (_getPrototypeOf2 = _getPrototypeOf(ScrollableLogTextStreamViewClass)).call.apply(_getPrototypeOf2, [this].concat(args)));

    _defineProperty(_assertThisInitialized(_this), "state", {
      target: null,
      targetId: null
    });

    _defineProperty(_assertThisInitialized(_this), "handleOpenFlyout", function (id) {
      _this.props.setFlyoutItem(id);

      _this.props.setFlyoutVisibility(true);
    });

    _defineProperty(_assertThisInitialized(_this), "handleReload", function () {
      var _this$props = _this.props,
          jumpToTarget = _this$props.jumpToTarget,
          target = _this$props.target;

      if (target) {
        jumpToTarget(target);
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleLoadNewerItems", function () {
      var loadNewerItems = _this.props.loadNewerItems;

      if (loadNewerItems) {
        loadNewerItems();
      }
    });

    _defineProperty(_assertThisInitialized(_this), "handleVisibleChildrenChange", (0, _handlers.callWithoutRepeats)(function (_ref) {
      var topChild = _ref.topChild,
          middleChild = _ref.middleChild,
          bottomChild = _ref.bottomChild,
          pagesAbove = _ref.pagesAbove,
          pagesBelow = _ref.pagesBelow,
          fromScroll = _ref.fromScroll;

      _this.props.reportVisibleInterval({
        endKey: (0, _item.parseStreamItemId)(bottomChild),
        middleKey: (0, _item.parseStreamItemId)(middleChild),
        pagesAfterEnd: pagesBelow,
        pagesBeforeStart: pagesAbove,
        startKey: (0, _item.parseStreamItemId)(topChild),
        fromScroll: fromScroll
      });
    }));

    return _this;
  }

  _createClass(ScrollableLogTextStreamViewClass, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          columnConfigurations = _this$props2.columnConfigurations,
          currentHighlightKey = _this$props2.currentHighlightKey,
          hasMoreAfterEnd = _this$props2.hasMoreAfterEnd,
          hasMoreBeforeStart = _this$props2.hasMoreBeforeStart,
          highlightedItem = _this$props2.highlightedItem,
          intl = _this$props2.intl,
          isLoadingMore = _this$props2.isLoadingMore,
          isReloading = _this$props2.isReloading,
          isStreaming = _this$props2.isStreaming,
          items = _this$props2.items,
          lastLoadedTime = _this$props2.lastLoadedTime,
          scale = _this$props2.scale,
          wrap = _this$props2.wrap;
      var targetId = this.state.targetId;
      var hasItems = items.length > 0;
      return _react2.default.createElement(ScrollableLogTextStreamViewWrapper, null, isReloading && !hasItems ? _react2.default.createElement(_loading.InfraLoadingPanel, {
        width: "100%",
        height: "100%",
        text: _react2.default.createElement(_react.FormattedMessage, {
          id: "xpack.infra.logs.scrollableLogTextStreamView.loadingEntriesLabel",
          defaultMessage: "Loading entries"
        })
      }) : !hasItems ? _react2.default.createElement(_empty_states.NoData, {
        titleText: intl.formatMessage({
          id: 'xpack.infra.logs.emptyView.noLogMessageTitle',
          defaultMessage: 'There are no log messages to display.'
        }),
        bodyText: intl.formatMessage({
          id: 'xpack.infra.logs.emptyView.noLogMessageDescription',
          defaultMessage: 'Try adjusting your filter.'
        }),
        refetchText: intl.formatMessage({
          id: 'xpack.infra.logs.emptyView.checkForNewDataButtonLabel',
          defaultMessage: 'Check for new data'
        }),
        onRefetch: this.handleReload,
        testString: "logsNoDataPrompt"
      }) : _react2.default.createElement(WithColumnWidths, {
        columnConfigurations: columnConfigurations,
        scale: scale
      }, function (_ref2) {
        var columnWidths = _ref2.columnWidths,
            CharacterDimensionsProbe = _ref2.CharacterDimensionsProbe;
        return _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(CharacterDimensionsProbe, null), _react2.default.createElement(_column_headers.LogColumnHeaders, {
          columnConfigurations: columnConfigurations,
          columnWidths: columnWidths
        }), _react2.default.createElement(_auto_sizer.AutoSizer, {
          bounds: true,
          content: true,
          detectAnyWindowResize: "height"
        }, function (_ref3) {
          var measureRef = _ref3.measureRef,
              _ref3$bounds$height = _ref3.bounds.height,
              height = _ref3$bounds$height === void 0 ? 0 : _ref3$bounds$height,
              _ref3$content$width = _ref3.content.width,
              width = _ref3$content$width === void 0 ? 0 : _ref3$content$width;
          return _react2.default.createElement(ScrollPanelSizeProbe, {
            innerRef: measureRef
          }, _react2.default.createElement(_vertical_scroll_panel.VerticalScrollPanel, {
            height: height,
            width: width,
            onVisibleChildrenChange: _this2.handleVisibleChildrenChange,
            target: targetId,
            hideScrollbar: true,
            "data-test-subj": 'logStream'
          }, function (registerChild) {
            return _react2.default.createElement(_react2.default.Fragment, null, _react2.default.createElement(_loading_item_view.LogTextStreamLoadingItemView, {
              alignment: "bottom",
              isLoading: isLoadingMore,
              hasMore: hasMoreBeforeStart,
              isStreaming: false,
              lastStreamingUpdate: null
            }), items.map(function (item) {
              return _react2.default.createElement(_measurable_item_view.MeasurableItemView, {
                register: registerChild,
                registrationKey: (0, _item.getStreamItemId)(item),
                key: (0, _item.getStreamItemId)(item)
              }, function (itemMeasureRef) {
                return _react2.default.createElement(_log_entry_row.LogEntryRow, {
                  columnConfigurations: columnConfigurations,
                  columnWidths: columnWidths,
                  openFlyoutWithItem: _this2.handleOpenFlyout,
                  boundingBoxRef: itemMeasureRef,
                  logEntry: item.logEntry,
                  highlights: item.highlights,
                  isActiveHighlight: !!currentHighlightKey && currentHighlightKey.gid === item.logEntry.gid,
                  scale: scale,
                  wrap: wrap,
                  isHighlighted: highlightedItem ? item.logEntry.gid === highlightedItem : false
                });
              });
            }), _react2.default.createElement(_loading_item_view.LogTextStreamLoadingItemView, {
              alignment: "top",
              isLoading: isStreaming || isLoadingMore,
              hasMore: hasMoreAfterEnd,
              isStreaming: isStreaming,
              lastStreamingUpdate: isStreaming ? lastLoadedTime : null,
              onLoadMore: _this2.handleLoadNewerItems
            }));
          }));
        }));
      }));
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(nextProps, prevState) {
      var hasNewTarget = nextProps.target && nextProps.target !== prevState.target;
      var hasItems = nextProps.items.length > 0;

      if (nextProps.isStreaming && hasItems) {
        return {
          target: nextProps.target,
          targetId: (0, _item.getStreamItemId)(nextProps.items[nextProps.items.length - 1])
        };
      } else if (hasNewTarget && hasItems) {
        return {
          target: nextProps.target,
          targetId: (0, _item.getStreamItemId)((0, _item.getStreamItemBeforeTimeKey)(nextProps.items, nextProps.target))
        };
      } else if (!nextProps.target || !hasItems) {
        return {
          target: null,
          targetId: null
        };
      }

      return null;
    }
  }]);

  return ScrollableLogTextStreamViewClass;
}(_react2.default.PureComponent);

var ScrollableLogTextStreamView = (0, _react.injectI18n)(ScrollableLogTextStreamViewClass);
/**
 * This function-as-child component calculates the column widths based on the
 * given configuration. It depends on the `CharacterDimensionsProbe` it returns
 * being rendered so it can measure the monospace character size.
 *
 * If the above component wasn't a class component, this would have been
 * written as a hook.
 */

exports.ScrollableLogTextStreamView = ScrollableLogTextStreamView;

var WithColumnWidths = function WithColumnWidths(_ref4) {
  var children = _ref4.children,
      columnConfigurations = _ref4.columnConfigurations,
      scale = _ref4.scale;

  var _useMeasuredCharacter = (0, _text_styles.useMeasuredCharacterDimensions)(scale),
      CharacterDimensionsProbe = _useMeasuredCharacter.CharacterDimensionsProbe,
      dimensions = _useMeasuredCharacter.dimensions;

  var referenceTime = (0, _react2.useMemo)(function () {
    return Date.now();
  }, []);
  var formattedCurrentDate = (0, _formatted_time.useFormattedTime)(referenceTime);
  var columnWidths = (0, _react2.useMemo)(function () {
    return (0, _log_entry_column.getColumnWidths)(columnConfigurations, dimensions.width, formattedCurrentDate.length);
  }, [columnConfigurations, dimensions.width, formattedCurrentDate]);
  var childParams = (0, _react2.useMemo)(function () {
    return {
      columnWidths: columnWidths,
      CharacterDimensionsProbe: CharacterDimensionsProbe
    };
  }, [columnWidths, CharacterDimensionsProbe]);
  return children(childParams);
};

var ScrollableLogTextStreamViewWrapper = _eui_styled_components.default.div(_templateObject());

var ScrollPanelSizeProbe = _eui_styled_components.default.div(_templateObject2());