"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisSetupTimerangeForm = void 0;

var _react = _interopRequireWildcard(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _create_ml_jobs_button = require("./create_ml_jobs_button");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var startTimeLabel = _i18n.i18n.translate('xpack.infra.analysisSetup.startTimeLabel', {
  defaultMessage: 'Start time'
});

var endTimeLabel = _i18n.i18n.translate('xpack.infra.analysisSetup.endTimeLabel', {
  defaultMessage: 'End time'
});

var startTimeDefaultDescription = _i18n.i18n.translate('xpack.infra.analysisSetup.startTimeDefaultDescription', {
  defaultMessage: 'Start of log indices'
});

var endTimeDefaultDescription = _i18n.i18n.translate('xpack.infra.analysisSetup.endTimeDefaultDescription', {
  defaultMessage: 'Indefinitely'
});

function selectedDateToParam(selectedDate) {
  if (selectedDate) {
    return selectedDate.valueOf(); // To ms unix timestamp
  }

  return undefined;
}

var AnalysisSetupTimerangeForm = function AnalysisSetupTimerangeForm(_ref) {
  var isSettingUp = _ref.isSettingUp,
      setupMlModule = _ref.setupMlModule;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      startTime = _useState2[0],
      setStartTime = _useState2[1];

  var _useState3 = (0, _react.useState)(null),
      _useState4 = _slicedToArray(_useState3, 2),
      endTime = _useState4[0],
      setEndTime = _useState4[1];

  var now = (0, _react.useMemo)(function () {
    return (0, _moment.default)();
  }, []);
  var selectedEndTimeIsToday = !endTime || endTime.isSame(now, 'day');

  var onClickCreateJob = function onClickCreateJob() {
    return setupMlModule(selectedDateToParam(startTime), selectedDateToParam(endTime));
  };

  return _react.default.createElement(_eui.EuiForm, null, _react.default.createElement(_eui.EuiDescribedFormGroup, {
    idAria: "timeRange",
    title: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.infra.analysisSetup.timeRangeTitle",
      defaultMessage: "Choose a time range"
    }),
    description: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.infra.analysisSetup.timeRangeDescription",
      defaultMessage: "By default, Machine Learning analyzes log messages from the start of your log indices and continues indefinitely. You can specify a different date to begin, to end, or both."
    })
  }, _react.default.createElement(_eui.EuiFormRow, {
    describedByIds: ['timeRange'],
    error: false,
    fullWidth: true,
    isInvalid: false,
    label: startTimeLabel
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s"
  }, _react.default.createElement(_eui.EuiFormControlLayout, {
    clear: startTime ? {
      onClick: function onClick() {
        return setStartTime(null);
      }
    } : undefined
  }, _react.default.createElement(_eui.EuiDatePicker, {
    showTimeSelect: true,
    selected: startTime,
    onChange: setStartTime,
    placeholder: startTimeDefaultDescription,
    maxDate: now
  })))), _react.default.createElement(_eui.EuiFormRow, {
    describedByIds: ['timeRange'],
    error: false,
    fullWidth: true,
    isInvalid: false,
    label: endTimeLabel
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s"
  }, _react.default.createElement(_eui.EuiFormControlLayout, {
    clear: endTime ? {
      onClick: function onClick() {
        return setEndTime(null);
      }
    } : undefined
  }, _react.default.createElement(_eui.EuiDatePicker, {
    showTimeSelect: true,
    selected: endTime,
    onChange: setEndTime,
    placeholder: endTimeDefaultDescription,
    openToDate: now,
    minDate: now,
    minTime: selectedEndTimeIsToday ? now : (0, _moment.default)().hour(0).minutes(0),
    maxTime: (0, _moment.default)().hour(23).minutes(59)
  })))), _react.default.createElement(_create_ml_jobs_button.CreateMLJobsButton, {
    isLoading: isSettingUp,
    onClick: onClickCreateJob
  })));
};

exports.AnalysisSetupTimerangeForm = AnalysisSetupTimerangeForm;