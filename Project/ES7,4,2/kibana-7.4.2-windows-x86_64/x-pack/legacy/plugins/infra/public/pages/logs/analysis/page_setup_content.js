"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisSetupContent = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

var _use_track_metric = require("../../../hooks/use_track_metric");

var _analysis_setup_timerange_form = require("./analysis_setup_timerange_form");

var _create_ml_jobs_button = require("./create_ml_jobs_button");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  & .euiButtonEmpty {\n    font-size: inherit;\n    line-height: inherit;\n    height: initial;\n  }\n\n  & .euiButtonEmpty__content {\n    padding: 0;\n  }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  height: 100%;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  max-width: 518px !important;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var errorTitle = _i18n.i18n.translate('xpack.infra.analysisSetup.errorTitle', {
  defaultMessage: 'Sorry, there was an error setting up Machine Learning'
});

var AnalysisSetupContent = function AnalysisSetupContent(_ref) {
  var setupMlModule = _ref.setupMlModule,
      isSettingUp = _ref.isSettingUp,
      didSetupFail = _ref.didSetupFail,
      isCleaningUpAFailedSetup = _ref.isCleaningUpAFailedSetup,
      indexPattern = _ref.indexPattern;
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_logs',
    path: 'analysis_setup'
  });
  (0, _use_track_metric.useTrackPageview)({
    app: 'infra_logs',
    path: 'analysis_setup',
    delay: 15000
  });

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      showTimeRangeForm = _useState2[0],
      setShowTimeRangeForm = _useState2[1];

  return _react.default.createElement(AnalysisSetupPage, null, _react.default.createElement(_eui.EuiPageBody, null, _react.default.createElement(AnalysisPageContent, {
    verticalPosition: "center",
    horizontalPosition: "center",
    "data-test-subj": "analysisSetupContent"
  }, _react.default.createElement(_eui.EuiPageContentHeader, null, _react.default.createElement(_eui.EuiPageContentHeaderSection, null, _react.default.createElement(_eui.EuiTitle, {
    size: "m"
  }, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.analysisSetupTitle",
    defaultMessage: "Enable Machine Learning analysis"
  }))))), _react.default.createElement(_eui.EuiPageContentBody, null, _react.default.createElement(_eui.EuiText, {
    size: "s"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.analysisSetupDescription",
    defaultMessage: "Use Machine Learning to automatically detect anomalous log rate counts."
  })), showTimeRangeForm ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_analysis_setup_timerange_form.AnalysisSetupTimerangeForm, {
    isSettingUp: isSettingUp,
    setupMlModule: setupMlModule
  })) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(ByDefaultText, null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.timeRangeByDefault",
    defaultMessage: "By default, we'll analyze all past and future log messages in your logs indices."
  }), ' ', _react.default.createElement(_eui.EuiButtonEmpty, {
    onClick: function onClick() {
      return setShowTimeRangeForm(true);
    }
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.configureTimeRange",
    defaultMessage: "Configure time range?"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_create_ml_jobs_button.CreateMLJobsButton, {
    isLoading: isSettingUp || isCleaningUpAFailedSetup,
    onClick: function onClick() {
      return setupMlModule();
    }
  })), didSetupFail && _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_eui.EuiSpacer, null), _react.default.createElement(_eui.EuiCallOut, {
    color: "danger",
    iconType: "alert",
    title: errorTitle
  }, _react.default.createElement(_eui.EuiText, null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.infra.analysisSetup.errorText",
    defaultMessage: "Please ensure your configured logs indices ({indexPattern}) exist. If your indices do exist, please try again.",
    values: {
      indexPattern: indexPattern
    }
  }))))))));
}; // !important due to https://github.com/elastic/eui/issues/2232


exports.AnalysisSetupContent = AnalysisSetupContent;
var AnalysisPageContent = (0, _eui_styled_components.default)(_eui.EuiPageContent)(_templateObject());
var AnalysisSetupPage = (0, _eui_styled_components.default)(_eui.EuiPage)(_templateObject2());
var ByDefaultText = (0, _eui_styled_components.default)(_eui.EuiText).attrs({
  size: 's'
})(_templateObject3());