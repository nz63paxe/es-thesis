"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logEntriesReducer = void 0;

var _reduceReducers = _interopRequireDefault(require("reduce-reducers"));

var _load = require("./operations/load");

var _load_more = require("./operations/load_more");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var logEntriesReducer = (0, _reduceReducers.default)(_load.loadEntriesReducer, _load_more.loadMoreEntriesReducer);
exports.logEntriesReducer = logEntriesReducer;