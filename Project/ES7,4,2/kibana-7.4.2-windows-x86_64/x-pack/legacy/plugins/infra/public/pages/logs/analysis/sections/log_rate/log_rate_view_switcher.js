"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.LogRateViewSwitcher = exports.isValidLogRateView = void 0;

var _eui = require("@elastic/eui");

var _i18n = require("@kbn/i18n");

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var isValidLogRateView = function isValidLogRateView(maybeView) {
  return ['chart', 'table'].includes(maybeView);
};

exports.isValidLogRateView = isValidLogRateView;

var chartLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.viewSwitcher.chartLabel', {
  defaultMessage: 'Rate chart'
});

var tableLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.viewSwitcher.tableLabel', {
  defaultMessage: 'Anomaly table'
});

var legendLabel = _i18n.i18n.translate('xpack.infra.logs.analysis.logRateSection.viewSwitcher.legendLabel', {
  defaultMessage: 'Switch between the log rate chart and the anomalies table view'
});

var buttons = [{
  id: 'chart',
  label: chartLabel,
  iconType: 'apps'
}, {
  id: 'table',
  label: tableLabel,
  iconType: 'editorUnorderedList'
}];

var LogRateViewSwitcher = function LogRateViewSwitcher(_ref) {
  var selectedView = _ref.selectedView,
      onChange = _ref.onChange;
  return _react.default.createElement(_eui.EuiButtonGroup, {
    legend: legendLabel,
    options: buttons,
    color: "primary",
    idSelected: selectedView,
    onChange: onChange
  });
};

exports.LogRateViewSwitcher = LogRateViewSwitcher;