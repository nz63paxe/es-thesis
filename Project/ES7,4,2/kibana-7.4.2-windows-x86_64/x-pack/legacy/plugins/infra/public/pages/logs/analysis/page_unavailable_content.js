"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AnalysisUnavailableContent = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  max-width: 50vw;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var AnalysisUnavailableContent = function AnalysisUnavailableContent() {
  return _react.default.createElement(EmptyPrompt, {
    title: _react.default.createElement("h2", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.infra.logs.analysisPage.unavailable.mLDisabledTitle",
      defaultMessage: "The Analysis feature requires Machine Learning"
    })),
    body: _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.infra.logs.analysisPage.unavailable.mlDisabledBody",
      defaultMessage: "Check the {machineLearningAppLink} for more information.",
      values: {
        machineLearningAppLink: _react.default.createElement(_eui.EuiLink, {
          href: "ml",
          target: "_blank"
        }, _react.default.createElement(_react2.FormattedMessage, {
          id: "xpack.infra.logs.analysisPage.unavailable.mlAppLink",
          defaultMessage: "Machine Learning app"
        }))
      }
    })),
    actions: _react.default.createElement(_eui.EuiButton, {
      target: "_blank",
      href: "ml",
      color: "primary",
      fill: true
    }, _i18n.i18n.translate('xpack.infra.logs.analysisPage.unavailable.mlAppButton', {
      defaultMessage: 'Open Machine Learning'
    }))
  });
};

exports.AnalysisUnavailableContent = AnalysisUnavailableContent;
var EmptyPrompt = (0, _eui_styled_components.default)(_eui.EuiEmptyPrompt)(_templateObject());