"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfraMetricLayoutSectionType = exports.InfraMetricLayoutVisualizationType = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var InfraMetricLayoutVisualizationType;
exports.InfraMetricLayoutVisualizationType = InfraMetricLayoutVisualizationType;

(function (InfraMetricLayoutVisualizationType) {
  InfraMetricLayoutVisualizationType["line"] = "line";
  InfraMetricLayoutVisualizationType["area"] = "area";
  InfraMetricLayoutVisualizationType["bar"] = "bar";
})(InfraMetricLayoutVisualizationType || (exports.InfraMetricLayoutVisualizationType = InfraMetricLayoutVisualizationType = {}));

var InfraMetricLayoutSectionType;
exports.InfraMetricLayoutSectionType = InfraMetricLayoutSectionType;

(function (InfraMetricLayoutSectionType) {
  InfraMetricLayoutSectionType["chart"] = "chart";
  InfraMetricLayoutSectionType["gauges"] = "gauges";
})(InfraMetricLayoutSectionType || (exports.InfraMetricLayoutSectionType = InfraMetricLayoutSectionType = {}));