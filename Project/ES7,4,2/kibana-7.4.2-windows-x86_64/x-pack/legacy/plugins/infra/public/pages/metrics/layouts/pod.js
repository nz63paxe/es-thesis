"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.podLayoutCreator = void 0;

var _i18n = require("@kbn/i18n");

var _types = require("../../../graphql/types");

var _lib = require("../../../lib/lib");

var _nginx = require("./nginx");

var _types2 = require("./types");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

var podLayoutCreator = function podLayoutCreator(theme) {
  return [{
    id: 'podOverview',
    label: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.layoutLabel', {
      defaultMessage: 'Pod'
    }),
    sections: [{
      id: _types.InfraMetric.podOverview,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.overviewSection.sectionLabel', {
        defaultMessage: 'Overview'
      }),
      requires: ['kubernetes.pod'],
      type: _types2.InfraMetricLayoutSectionType.gauges,
      visConfig: {
        seriesOverrides: {
          cpu: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.overviewSection.cpuUsageSeriesLabel', {
              defaultMessage: 'CPU Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          memory: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.overviewSection.memoryUsageSeriesLabel', {
              defaultMessage: 'Memory Usage'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.percent,
            gaugeMax: 1
          },
          rx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.overviewSection.inboundRXSeriesLabel', {
              defaultMessage: 'Inbound (RX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          },
          tx: {
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.overviewSection.outboundTXSeriesLabel', {
              defaultMessage: 'Outbound (TX)'
            }),
            color: theme.eui.euiColorFullShade,
            formatter: _lib.InfraFormatterType.bits,
            formatterTemplate: '{{value}}/s'
          }
        }
      }
    }, {
      id: _types.InfraMetric.podCpuUsage,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.cpuUsageSection.sectionLabel', {
        defaultMessage: 'CPU Usage'
      }),
      requires: ['kubernetes.pod'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.percent,
        seriesOverrides: {
          cpu: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.podMemoryUsage,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.memoryUsageSection.sectionLabel', {
        defaultMessage: 'Memory Usage'
      }),
      requires: ['kubernetes.pod'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.percent,
        seriesOverrides: {
          memory: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.podNetworkTraffic,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.networkTrafficSection.sectionLabel', {
        defaultMessage: 'Network Traffic'
      }),
      requires: ['kubernetes.pod'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.bits,
        formatterTemplate: '{{value}}/s',
        type: _types2.InfraMetricLayoutVisualizationType.area,
        seriesOverrides: {
          rx: {
            color: theme.eui.euiColorVis1,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.networkTrafficSection.networkRxRateSeriesLabel', {
              defaultMessage: 'in'
            })
          },
          tx: {
            color: theme.eui.euiColorVis2,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.podMetricsLayout.networkTrafficSection.networkTxRateSeriesLabel', {
              defaultMessage: 'out'
            })
          }
        }
      }
    }]
  }].concat(_toConsumableArray((0, _nginx.nginxLayoutCreator)(theme)));
};

exports.podLayoutCreator = podLayoutCreator;