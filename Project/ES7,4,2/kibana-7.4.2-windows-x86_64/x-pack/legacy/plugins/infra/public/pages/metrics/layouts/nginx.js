"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.nginxLayoutCreator = void 0;

var _i18n = require("@kbn/i18n");

var _types = require("../../../graphql/types");

var _lib = require("../../../lib/lib");

var _types2 = require("./types");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var nginxLayoutCreator = function nginxLayoutCreator(theme) {
  return [{
    id: 'nginxOverview',
    label: 'Nginx',
    sections: [{
      id: _types.InfraMetric.nginxHits,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.nginxMetricsLayout.hitsSection.sectionLabel', {
        defaultMessage: 'Hits'
      }),
      requires: ['nginx.access'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.abbreviatedNumber,
        stacked: true,
        seriesOverrides: {
          '200s': {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.bar
          },
          '300s': {
            color: theme.eui.euiColorVis5,
            type: _types2.InfraMetricLayoutVisualizationType.bar
          },
          '400s': {
            color: theme.eui.euiColorVis2,
            type: _types2.InfraMetricLayoutVisualizationType.bar
          },
          '500s': {
            color: theme.eui.euiColorVis9,
            type: _types2.InfraMetricLayoutVisualizationType.bar
          }
        }
      }
    }, {
      id: _types.InfraMetric.nginxRequestRate,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.nginxMetricsLayout.requestRateSection.sectionLabel', {
        defaultMessage: 'Request Rate'
      }),
      requires: ['nginx.stubstatus'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.abbreviatedNumber,
        formatterTemplate: '{{value}}/s',
        seriesOverrides: {
          rate: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.area
          }
        }
      }
    }, {
      id: _types.InfraMetric.nginxActiveConnections,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.nginxMetricsLayout.activeConnectionsSection.sectionLabel', {
        defaultMessage: 'Active Connections'
      }),
      requires: ['nginx.stubstatus'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.abbreviatedNumber,
        seriesOverrides: {
          connections: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.bar
          }
        }
      }
    }, {
      id: _types.InfraMetric.nginxRequestsPerConnection,
      label: _i18n.i18n.translate('xpack.infra.metricDetailPage.nginxMetricsLayout.requestsPerConnectionsSection.sectionLabel', {
        defaultMessage: 'Requests per Connections'
      }),
      requires: ['nginx.stubstatus'],
      type: _types2.InfraMetricLayoutSectionType.chart,
      visConfig: {
        formatter: _lib.InfraFormatterType.abbreviatedNumber,
        seriesOverrides: {
          reqPerConns: {
            color: theme.eui.euiColorVis1,
            type: _types2.InfraMetricLayoutVisualizationType.bar,
            name: _i18n.i18n.translate('xpack.infra.metricDetailPage.nginxMetricsLayout.requestsPerConnectionsSection.reqsPerConnSeriesLabel', {
              defaultMessage: 'reqs per conn'
            })
          }
        }
      }
    }]
  }];
};

exports.nginxLayoutCreator = nginxLayoutCreator;