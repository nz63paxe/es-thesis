"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DensityChart = void 0;

var _d3Scale = require("d3-scale");

var _d3Shape = require("d3-shape");

var _max = _interopRequireDefault(require("lodash/fp/max"));

var React = _interopRequireWildcard(require("react"));

var _eui_styled_components = _interopRequireDefault(require("../../../../../../common/eui_styled_components"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\n  fill: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n  fill: ", ";\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  fill: white;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var DensityChart = function DensityChart(_ref) {
  var buckets = _ref.buckets,
      start = _ref.start,
      end = _ref.end,
      width = _ref.width,
      height = _ref.height;

  if (start >= end || height <= 0 || width <= 0 || buckets.length <= 0) {
    return null;
  }

  var yScale = (0, _d3Scale.scaleTime)().domain([start, end]).range([0, height]);
  var xMax = (0, _max.default)(buckets.map(function (bucket) {
    return bucket.entriesCount;
  })) || 0;
  var xScale = (0, _d3Scale.scaleLinear)().domain([0, xMax]).range([0, width * (2 / 3)]);
  var path = (0, _d3Shape.area)().x0(xScale(0)).x1(function (bucket) {
    return xScale(bucket.entriesCount);
  }).y(function (bucket) {
    return yScale((bucket.start + bucket.end) / 2);
  }).curve(_d3Shape.curveMonotoneY);
  var pathData = path(buckets);
  var highestPathCoord = String(pathData).replace(/[^.0-9,]/g, ' ').split(/[ ,]/).reduce(function (result, num) {
    return Number(num) > result ? Number(num) : result;
  }, 0);
  return React.createElement("g", {
    transform: "translate(".concat(width / 3, ", 0)")
  }, React.createElement(DensityChartNegativeBackground, {
    transform: "translate(".concat(-width / 3, ", 0)"),
    width: width / 2,
    height: highestPathCoord
  }), React.createElement(DensityChartPositiveBackground, {
    width: width * (2 / 3),
    height: highestPathCoord
  }), React.createElement(PositiveAreaPath, {
    d: pathData || ''
  }));
};

exports.DensityChart = DensityChart;

var DensityChartNegativeBackground = _eui_styled_components.default.rect(_templateObject());

var DensityChartPositiveBackground = _eui_styled_components.default.rect(_templateObject2(), function (props) {
  return props.theme.darkMode ? props.theme.eui.euiColorLightShade : props.theme.eui.euiColorLightestShade;
});

var PositiveAreaPath = _eui_styled_components.default.path(_templateObject3(), function (props) {
  return props.theme.darkMode ? props.theme.eui.euiColorMediumShade : props.theme.eui.euiColorLightShade;
});