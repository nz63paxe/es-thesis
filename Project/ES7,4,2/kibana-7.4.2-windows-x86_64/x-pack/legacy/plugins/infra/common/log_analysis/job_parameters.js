"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getDatafeedId = exports.getJobId = exports.getJobIdPrefix = exports.bucketSpan = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const bucketSpan = 900000;
exports.bucketSpan = bucketSpan;

const getJobIdPrefix = (spaceId, sourceId) => `kibana-logs-ui-${spaceId}-${sourceId}-`;

exports.getJobIdPrefix = getJobIdPrefix;

const getJobId = (spaceId, sourceId, jobType) => `${getJobIdPrefix(spaceId, sourceId)}${jobType}`;

exports.getJobId = getJobId;

const getDatafeedId = (spaceId, sourceId, jobType) => `datafeed-${getJobId(spaceId, sourceId, jobType)}`;

exports.getDatafeedId = getDatafeedId;