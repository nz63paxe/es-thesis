"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.throwErrors = exports.createPlainError = void 0;

var _PathReporter = require("io-ts/lib/PathReporter");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const createPlainError = message => new Error(message);

exports.createPlainError = createPlainError;

const throwErrors = createError => errors => {
  throw createError((0, _PathReporter.failure)(errors).join('\n'));
};

exports.throwErrors = throwErrors;