"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createCollectorFetch = createCollectorFetch;
exports.getCloudUsageCollector = getCloudUsageCollector;

var _constants = require("./constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function createCollectorFetch(server) {
  return async function fetchUsageStats() {
    const {
      id
    } = server.config().get(`xpack.cloud`);
    return {
      isCloudEnabled: !!id
    };
  };
}
/*
 * @param {Object} server
 * @return {Object} kibana usage stats type collection object
 */


function getCloudUsageCollector(server) {
  const {
    collectorSet
  } = server.usage;
  return collectorSet.makeUsageCollector({
    type: _constants.KIBANA_CLOUD_STATS_TYPE,
    isReady: () => true,
    fetch: createCollectorFetch(server)
  });
}