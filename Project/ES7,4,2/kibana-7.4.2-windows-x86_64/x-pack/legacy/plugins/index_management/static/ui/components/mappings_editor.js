"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MappingsEditor = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const MappingsEditor = ({
  setGetDataHandler,
  FormattedMessage,
  areErrorsVisible = true,
  defaultValue = {}
}) => {
  const [mappings, setMappings] = (0, _react.useState)(JSON.stringify(defaultValue, null, 2));
  const [error, setError] = (0, _react.useState)(null);

  const getFormData = () => {
    setError(null);

    try {
      const parsed = JSON.parse(mappings);
      return {
        data: parsed,
        isValid: true
      };
    } catch (e) {
      setError(e.message);
      return {
        isValid: false,
        data: {}
      };
    }
  };

  (0, _react.useEffect)(() => {
    setGetDataHandler(getFormData);
  }, [mappings]);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiCodeEditor, {
    mode: "json",
    theme: "textmate",
    width: "100%",
    value: mappings,
    setOptions: {
      showLineNumbers: false,
      tabSize: 2,
      maxLines: Infinity
    },
    editorProps: {
      $blockScrolling: Infinity
    },
    showGutter: false,
    minLines: 6,
    "aria-label": _react.default.createElement(FormattedMessage, {
      id: "xpack.idxMgmt.mappingsEditor.mappingsEditorAriaLabel",
      defaultMessage: "Index mappings editor"
    }),
    onChange: value => {
      setMappings(value);
    },
    "data-test-subj": "mappingsEditor"
  }), areErrorsVisible && error && _react.default.createElement(_react.Fragment, null, _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }), _react.default.createElement(_eui.EuiCallOut, {
    title: _react.default.createElement(FormattedMessage, {
      id: "xpack.idxMgmt.mappingsEditor.formatError",
      defaultMessage: "JSON format error"
    }),
    color: "danger",
    iconType: "alert"
  }, _react.default.createElement("p", {
    "data-test-subj": "errors"
  }, error))));
};

exports.MappingsEditor = MappingsEditor;