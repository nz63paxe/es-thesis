"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mappings_editor = require("./mappings_editor");

Object.keys(_mappings_editor).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _mappings_editor[key];
    }
  });
});