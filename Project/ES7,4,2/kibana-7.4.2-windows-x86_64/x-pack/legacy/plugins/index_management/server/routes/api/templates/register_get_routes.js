"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerGetAllRoute = registerGetAllRoute;
exports.registerGetOneRoute = registerGetOneRoute;

var _lib = require("../../../../common/lib");

var _get_managed_templates = require("../../../lib/get_managed_templates");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
let callWithInternalUser;

const allHandler = async (_req, callWithRequest) => {
  const managedTemplatePrefix = await (0, _get_managed_templates.getManagedTemplatePrefix)(callWithInternalUser);
  const indexTemplatesByName = await callWithRequest('indices.getTemplate');
  return (0, _lib.deserializeTemplateList)(indexTemplatesByName, managedTemplatePrefix);
};

const oneHandler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const managedTemplatePrefix = await (0, _get_managed_templates.getManagedTemplatePrefix)(callWithInternalUser);
  const indexTemplateByName = await callWithRequest('indices.getTemplate', {
    name
  });

  if (indexTemplateByName[name]) {
    return (0, _lib.deserializeTemplate)({ ...indexTemplateByName[name],
      name
    }, managedTemplatePrefix);
  }
};

function registerGetAllRoute(router, server) {
  callWithInternalUser = server.plugins.elasticsearch.getCluster('data').callWithInternalUser;
  router.get('templates', allHandler);
}

function registerGetOneRoute(router, server) {
  callWithInternalUser = server.plugins.elasticsearch.getCluster('data').callWithInternalUser;
  router.get('templates/{name}', oneHandler);
}