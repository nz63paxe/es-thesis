"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerUpdateRoute = registerUpdateRoute;

var _lib = require("../../../../common/lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const handler = async (req, callWithRequest) => {
  const {
    name
  } = req.params;
  const template = req.payload;
  const serializedTemplate = (0, _lib.serializeTemplate)(template);
  const {
    order,
    index_patterns,
    version,
    settings,
    mappings,
    aliases
  } = serializedTemplate; // Verify the template exists (ES will throw 404 if not)

  await callWithRequest('indices.existsTemplate', {
    name
  }); // Next, update index template

  return await callWithRequest('indices.putTemplate', {
    name,
    order,
    body: {
      index_patterns,
      version,
      settings,
      mappings,
      aliases
    }
  });
};

function registerUpdateRoute(router) {
  router.put('templates/{name}', handler);
}