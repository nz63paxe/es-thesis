"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerCreateRoute = registerCreateRoute;

var _i18n = require("@kbn/i18n");

var _create_router = require("../../../../../../server/lib/create_router");

var _lib = require("../../../../common/lib");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const handler = async (req, callWithRequest) => {
  const template = req.payload;
  const serializedTemplate = (0, _lib.serializeTemplate)(template);
  const {
    name,
    order,
    index_patterns,
    version,
    settings,
    mappings,
    aliases
  } = serializedTemplate;
  const conflictError = (0, _create_router.wrapCustomError)(new Error(_i18n.i18n.translate('xpack.idxMgmt.createRoute.duplicateTemplateIdErrorMessage', {
    defaultMessage: "There is already a template with name '{name}'.",
    values: {
      name
    }
  })), 409); // Check that template with the same name doesn't already exist

  try {
    const templateExists = await callWithRequest('indices.existsTemplate', {
      name
    });

    if (templateExists) {
      throw conflictError;
    }
  } catch (e) {
    // Rethrow conflict error but silently swallow all others
    if (e === conflictError) {
      throw e;
    }
  } // Otherwise create new index template


  return await callWithRequest('indices.putTemplate', {
    name,
    order,
    body: {
      index_patterns,
      version,
      settings,
      mappings,
      aliases
    }
  });
};

function registerCreateRoute(router) {
  router.put('templates', handler);
}