"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.registerDeleteRoute = registerDeleteRoute;

var _create_router = require("../../../../../../server/lib/create_router");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const handler = async (req, callWithRequest) => {
  const {
    names
  } = req.params;
  const templateNames = names.split(',');
  const response = {
    templatesDeleted: [],
    errors: []
  };
  await Promise.all(templateNames.map(async name => {
    try {
      await callWithRequest('indices.deleteTemplate', {
        name
      });
      return response.templatesDeleted.push(name);
    } catch (e) {
      return response.errors.push({
        name,
        error: (0, _create_router.wrapEsError)(e)
      });
    }
  }));
  return response;
};

function registerDeleteRoute(router) {
  router.delete('templates/{names}', handler);
}