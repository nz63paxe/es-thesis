"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StepMappings = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _documentation_links = require("../../../lib/documentation_links");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var StepMappings = function StepMappings(_ref) {
  var template = _ref.template,
      updateTemplate = _ref.updateTemplate,
      errors = _ref.errors;
  var mappings = template.mappings;
  var mappingsError = errors.mappings;
  return _react.default.createElement("div", {
    "data-test-subj": "stepMappings"
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h3", {
    "data-test-subj": "stepTitle"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepMappings.stepTitle",
    defaultMessage: "Mappings (optional)"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiText, null, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepMappings.mappingsDescription",
    defaultMessage: "Define how to store and index documents."
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    size: "s",
    flush: "right",
    href: _documentation_links.mappingDocumentationLink,
    target: "_blank",
    iconType: "help"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepMappings.docsButtonLabel",
    defaultMessage: "Mapping docs"
  })))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiFormRow, {
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepMappings.fieldMappingsLabel",
      defaultMessage: "Mappings"
    }),
    helpText: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepMappings.mappingsEditorHelpText",
      defaultMessage: "Use JSON format: {code}",
      values: {
        code: _react.default.createElement(_eui.EuiCode, null, JSON.stringify({
          properties: {
            name: {
              type: 'text'
            }
          }
        }))
      }
    }),
    isInvalid: Boolean(mappingsError),
    error: mappingsError,
    fullWidth: true
  }, _react.default.createElement(_eui.EuiCodeEditor, {
    mode: "json",
    theme: "textmate",
    width: "100%",
    height: "500px",
    setOptions: {
      showLineNumbers: false,
      tabSize: 2
    },
    editorProps: {
      $blockScrolling: Infinity
    },
    showGutter: false,
    minLines: 6,
    "aria-label": _i18n.i18n.translate('xpack.idxMgmt.templateForm.stepMappings.fieldMappingsAriaLabel', {
      defaultMessage: 'Mappings editor'
    }),
    value: mappings,
    onChange: function onChange(newMappings) {
      updateTemplate({
        mappings: newMappings
      });
    },
    "data-test-subj": "mappingsEditor"
  })));
};

exports.StepMappings = StepMappings;