"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StepAliases = void 0;

var _react = _interopRequireDefault(require("react"));

var _i18n = require("@kbn/i18n");

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _documentation_links = require("../../../lib/documentation_links");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var StepAliases = function StepAliases(_ref) {
  var template = _ref.template,
      updateTemplate = _ref.updateTemplate,
      errors = _ref.errors;
  var aliases = template.aliases;
  var aliasesError = errors.aliases;
  return _react.default.createElement("div", {
    "data-test-subj": "stepAliases"
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h3", {
    "data-test-subj": "stepTitle"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepAliases.stepTitle",
    defaultMessage: "Aliases (optional)"
  }))), _react.default.createElement(_eui.EuiSpacer, {
    size: "s"
  }), _react.default.createElement(_eui.EuiText, null, _react.default.createElement("p", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepAliases.aliasesDescription",
    defaultMessage: "Set up aliases to associate with your indices."
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    size: "s",
    flush: "right",
    href: _documentation_links.templatesDocumentationLink,
    target: "_blank",
    iconType: "help"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepAliases.docsButtonLabel",
    defaultMessage: "Index Templates docs"
  })))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiFormRow, {
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepAliases.fieldAliasesLabel",
      defaultMessage: "Aliases"
    }),
    helpText: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepAliases.aliasesEditorHelpText",
      defaultMessage: "Use JSON format: {code}",
      values: {
        code: _react.default.createElement(_eui.EuiCode, null, JSON.stringify({
          my_alias: {}
        }))
      }
    }),
    isInvalid: Boolean(aliasesError),
    error: aliasesError,
    fullWidth: true
  }, _react.default.createElement(_eui.EuiCodeEditor, {
    mode: "json",
    theme: "textmate",
    width: "100%",
    height: "500px",
    setOptions: {
      showLineNumbers: false,
      tabSize: 2
    },
    editorProps: {
      $blockScrolling: Infinity
    },
    showGutter: false,
    minLines: 6,
    "aria-label": _i18n.i18n.translate('xpack.idxMgmt.templateForm.stepAliases.fieldAliasesAriaLabel', {
      defaultMessage: 'Aliases code editor'
    }),
    value: aliases,
    onChange: function onChange(newAliases) {
      updateTemplate({
        aliases: newAliases
      });
    },
    "data-test-subj": "aliasesEditor"
  })));
};

exports.StepAliases = StepAliases;