"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateMappings = void 0;

var _validation_helpers = require("./validation_helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var validateMappings = function validateMappings(template) {
  var mappings = template.mappings;
  var validation = {
    isValid: true,
    errors: {
      mappings: []
    }
  }; // Mappings JSON validation

  if (typeof mappings === 'string' && !(0, _validation_helpers.isStringEmpty)(mappings)) {
    var validationMsg = (0, _validation_helpers.validateJSON)(mappings);

    if (typeof validationMsg === 'string') {
      validation.errors.mappings.push(validationMsg);
    }
  }

  validation.errors = (0, _validation_helpers.removeEmptyErrorFields)(validation.errors);
  validation.isValid = (0, _validation_helpers.isValid)(validation.errors);
  return validation;
};

exports.validateMappings = validateMappings;