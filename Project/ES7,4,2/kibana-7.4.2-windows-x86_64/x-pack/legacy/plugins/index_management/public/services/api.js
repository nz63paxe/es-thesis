"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.loadIndices = loadIndices;
exports.reloadIndices = reloadIndices;
exports.closeIndices = closeIndices;
exports.deleteIndices = deleteIndices;
exports.openIndices = openIndices;
exports.refreshIndices = refreshIndices;
exports.flushIndices = flushIndices;
exports.forcemergeIndices = forcemergeIndices;
exports.clearCacheIndices = clearCacheIndices;
exports.freezeIndices = freezeIndices;
exports.unfreezeIndices = unfreezeIndices;
exports.loadIndexSettings = loadIndexSettings;
exports.updateIndexSettings = updateIndexSettings;
exports.loadIndexStats = loadIndexStats;
exports.loadIndexMapping = loadIndexMapping;
exports.loadIndexData = loadIndexData;
exports.loadIndexTemplates = loadIndexTemplates;
exports.deleteTemplates = deleteTemplates;
exports.loadIndexTemplate = loadIndexTemplate;
exports.saveTemplate = saveTemplate;
exports.updateTemplate = updateTemplate;
exports.loadTemplateToClone = loadTemplateToClone;
exports.getHttpClient = exports.setHttpClient = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _constants = require("../../common/constants");

var _constants2 = require("../constants");

var _track_ui_metric = require("./track_ui_metric");

var _use_request = require("./use_request");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var httpClient;

var setHttpClient = function setHttpClient(client) {
  httpClient = client;
};

exports.setHttpClient = setHttpClient;

var getHttpClient = function getHttpClient() {
  return httpClient;
};

exports.getHttpClient = getHttpClient;

var apiPrefix = _chrome.default.addBasePath('/api/index_management');

function loadIndices() {
  return _loadIndices.apply(this, arguments);
}

function _loadIndices() {
  _loadIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee() {
    var response;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return httpClient.get("".concat(apiPrefix, "/indices"));

          case 2:
            response = _context.sent;
            return _context.abrupt("return", response.data);

          case 4:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _loadIndices.apply(this, arguments);
}

function reloadIndices(_x) {
  return _reloadIndices.apply(this, arguments);
}

function _reloadIndices() {
  _reloadIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(indexNames) {
    var body, response;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            body = {
              indexNames: indexNames
            };
            _context2.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/reload"), body);

          case 3:
            response = _context2.sent;
            return _context2.abrupt("return", response.data);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _reloadIndices.apply(this, arguments);
}

function closeIndices(_x2) {
  return _closeIndices.apply(this, arguments);
}

function _closeIndices() {
  _closeIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee3(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            body = {
              indices: indices
            };
            _context3.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/close"), body);

          case 3:
            response = _context3.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_CLOSE_MANY : _constants.UIM_INDEX_CLOSE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context3.abrupt("return", response.data);

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _closeIndices.apply(this, arguments);
}

function deleteIndices(_x3) {
  return _deleteIndices.apply(this, arguments);
}

function _deleteIndices() {
  _deleteIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee4(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            body = {
              indices: indices
            };
            _context4.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/delete"), body);

          case 3:
            response = _context4.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_DELETE_MANY : _constants.UIM_INDEX_DELETE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context4.abrupt("return", response.data);

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _deleteIndices.apply(this, arguments);
}

function openIndices(_x4) {
  return _openIndices.apply(this, arguments);
}

function _openIndices() {
  _openIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee5(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            body = {
              indices: indices
            };
            _context5.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/open"), body);

          case 3:
            response = _context5.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_OPEN_MANY : _constants.UIM_INDEX_OPEN;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context5.abrupt("return", response.data);

          case 7:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _openIndices.apply(this, arguments);
}

function refreshIndices(_x5) {
  return _refreshIndices.apply(this, arguments);
}

function _refreshIndices() {
  _refreshIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee6(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            body = {
              indices: indices
            };
            _context6.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/refresh"), body);

          case 3:
            response = _context6.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_REFRESH_MANY : _constants.UIM_INDEX_REFRESH;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context6.abrupt("return", response.data);

          case 7:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _refreshIndices.apply(this, arguments);
}

function flushIndices(_x6) {
  return _flushIndices.apply(this, arguments);
}

function _flushIndices() {
  _flushIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee7(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            body = {
              indices: indices
            };
            _context7.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/flush"), body);

          case 3:
            response = _context7.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_FLUSH_MANY : _constants.UIM_INDEX_FLUSH;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context7.abrupt("return", response.data);

          case 7:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _flushIndices.apply(this, arguments);
}

function forcemergeIndices(_x7, _x8) {
  return _forcemergeIndices.apply(this, arguments);
}

function _forcemergeIndices() {
  _forcemergeIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee8(indices, maxNumSegments) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            body = {
              indices: indices,
              maxNumSegments: maxNumSegments
            };
            _context8.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/forcemerge"), body);

          case 3:
            response = _context8.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_FORCE_MERGE_MANY : _constants.UIM_INDEX_FORCE_MERGE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context8.abrupt("return", response.data);

          case 7:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));
  return _forcemergeIndices.apply(this, arguments);
}

function clearCacheIndices(_x9) {
  return _clearCacheIndices.apply(this, arguments);
}

function _clearCacheIndices() {
  _clearCacheIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee9(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            body = {
              indices: indices
            };
            _context9.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/clear_cache"), body);

          case 3:
            response = _context9.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_CLEAR_CACHE_MANY : _constants.UIM_INDEX_CLEAR_CACHE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context9.abrupt("return", response.data);

          case 7:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));
  return _clearCacheIndices.apply(this, arguments);
}

function freezeIndices(_x10) {
  return _freezeIndices.apply(this, arguments);
}

function _freezeIndices() {
  _freezeIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee10(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            body = {
              indices: indices
            };
            _context10.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/freeze"), body);

          case 3:
            response = _context10.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_FREEZE_MANY : _constants.UIM_INDEX_FREEZE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context10.abrupt("return", response.data);

          case 7:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));
  return _freezeIndices.apply(this, arguments);
}

function unfreezeIndices(_x11) {
  return _unfreezeIndices.apply(this, arguments);
}

function _unfreezeIndices() {
  _unfreezeIndices = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee11(indices) {
    var body, response, eventName;
    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            body = {
              indices: indices
            };
            _context11.next = 3;
            return httpClient.post("".concat(apiPrefix, "/indices/unfreeze"), body);

          case 3:
            response = _context11.sent;
            // Only track successful requests.
            eventName = indices.length > 1 ? _constants.UIM_INDEX_UNFREEZE_MANY : _constants.UIM_INDEX_UNFREEZE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, eventName);
            return _context11.abrupt("return", response.data);

          case 7:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));
  return _unfreezeIndices.apply(this, arguments);
}

function loadIndexSettings(_x12) {
  return _loadIndexSettings.apply(this, arguments);
}

function _loadIndexSettings() {
  _loadIndexSettings = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee12(indexName) {
    var response;
    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            _context12.next = 2;
            return httpClient.get("".concat(apiPrefix, "/settings/").concat(indexName));

          case 2:
            response = _context12.sent;
            return _context12.abrupt("return", response.data);

          case 4:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));
  return _loadIndexSettings.apply(this, arguments);
}

function updateIndexSettings(_x13, _x14) {
  return _updateIndexSettings.apply(this, arguments);
}

function _updateIndexSettings() {
  _updateIndexSettings = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee13(indexName, settings) {
    var response;
    return regeneratorRuntime.wrap(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            _context13.next = 2;
            return httpClient.put("".concat(apiPrefix, "/settings/").concat(indexName), settings);

          case 2:
            response = _context13.sent;
            // Only track successful requests.
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, _constants.UIM_UPDATE_SETTINGS);
            return _context13.abrupt("return", response);

          case 5:
          case "end":
            return _context13.stop();
        }
      }
    }, _callee13);
  }));
  return _updateIndexSettings.apply(this, arguments);
}

function loadIndexStats(_x15) {
  return _loadIndexStats.apply(this, arguments);
}

function _loadIndexStats() {
  _loadIndexStats = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee14(indexName) {
    var response;
    return regeneratorRuntime.wrap(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            _context14.next = 2;
            return httpClient.get("".concat(apiPrefix, "/stats/").concat(indexName));

          case 2:
            response = _context14.sent;
            return _context14.abrupt("return", response.data);

          case 4:
          case "end":
            return _context14.stop();
        }
      }
    }, _callee14);
  }));
  return _loadIndexStats.apply(this, arguments);
}

function loadIndexMapping(_x16) {
  return _loadIndexMapping.apply(this, arguments);
}

function _loadIndexMapping() {
  _loadIndexMapping = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee15(indexName) {
    var response;
    return regeneratorRuntime.wrap(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            _context15.next = 2;
            return httpClient.get("".concat(apiPrefix, "/mapping/").concat(indexName));

          case 2:
            response = _context15.sent;
            return _context15.abrupt("return", response.data);

          case 4:
          case "end":
            return _context15.stop();
        }
      }
    }, _callee15);
  }));
  return _loadIndexMapping.apply(this, arguments);
}

function loadIndexData(_x17, _x18) {
  return _loadIndexData.apply(this, arguments);
}

function _loadIndexData() {
  _loadIndexData = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee16(type, indexName) {
    return regeneratorRuntime.wrap(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            _context16.t0 = type;
            _context16.next = _context16.t0 === _constants2.TAB_MAPPING ? 3 : _context16.t0 === _constants2.TAB_SETTINGS ? 4 : _context16.t0 === _constants2.TAB_STATS ? 5 : 6;
            break;

          case 3:
            return _context16.abrupt("return", loadIndexMapping(indexName));

          case 4:
            return _context16.abrupt("return", loadIndexSettings(indexName));

          case 5:
            return _context16.abrupt("return", loadIndexStats(indexName));

          case 6:
          case "end":
            return _context16.stop();
        }
      }
    }, _callee16);
  }));
  return _loadIndexData.apply(this, arguments);
}

function loadIndexTemplates() {
  return (0, _use_request.useRequest)({
    path: "".concat(apiPrefix, "/templates"),
    method: 'get'
  });
}

function deleteTemplates(_x19) {
  return _deleteTemplates.apply(this, arguments);
}

function _deleteTemplates() {
  _deleteTemplates = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee17(names) {
    var result, uimActionType;
    return regeneratorRuntime.wrap(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            result = (0, _use_request.sendRequest)({
              path: "".concat(apiPrefix, "/templates/").concat(names.map(function (name) {
                return encodeURIComponent(name);
              }).join(',')),
              method: 'delete'
            });
            uimActionType = names.length > 1 ? _constants.UIM_TEMPLATE_DELETE_MANY : _constants.UIM_TEMPLATE_DELETE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, uimActionType);
            return _context17.abrupt("return", result);

          case 4:
          case "end":
            return _context17.stop();
        }
      }
    }, _callee17);
  }));
  return _deleteTemplates.apply(this, arguments);
}

function loadIndexTemplate(name) {
  return (0, _use_request.useRequest)({
    path: "".concat(apiPrefix, "/templates/").concat(encodeURIComponent(name)),
    method: 'get'
  });
}

function saveTemplate(_x20, _x21) {
  return _saveTemplate.apply(this, arguments);
}

function _saveTemplate() {
  _saveTemplate = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee18(template, isClone) {
    var result, uimActionType;
    return regeneratorRuntime.wrap(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            result = (0, _use_request.sendRequest)({
              path: "".concat(apiPrefix, "/templates"),
              method: 'put',
              body: template
            });
            uimActionType = isClone ? _constants.UIM_TEMPLATE_CLONE : _constants.UIM_TEMPLATE_CREATE;
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, uimActionType);
            return _context18.abrupt("return", result);

          case 4:
          case "end":
            return _context18.stop();
        }
      }
    }, _callee18);
  }));
  return _saveTemplate.apply(this, arguments);
}

function updateTemplate(_x22) {
  return _updateTemplate.apply(this, arguments);
}

function _updateTemplate() {
  _updateTemplate = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee19(template) {
    var name, result;
    return regeneratorRuntime.wrap(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            name = template.name;
            result = (0, _use_request.sendRequest)({
              path: "".concat(apiPrefix, "/templates/").concat(encodeURIComponent(name)),
              method: 'put',
              body: template
            });
            (0, _track_ui_metric.trackUiMetric)(_track_ui_metric.METRIC_TYPE.COUNT, _constants.UIM_TEMPLATE_UPDATE);
            return _context19.abrupt("return", result);

          case 4:
          case "end":
            return _context19.stop();
        }
      }
    }, _callee19);
  }));
  return _updateTemplate.apply(this, arguments);
}

function loadTemplateToClone(_x23) {
  return _loadTemplateToClone.apply(this, arguments);
}

function _loadTemplateToClone() {
  _loadTemplateToClone = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee20(name) {
    return regeneratorRuntime.wrap(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            return _context20.abrupt("return", (0, _use_request.sendRequest)({
              path: "".concat(apiPrefix, "/templates/").concat(encodeURIComponent(name)),
              method: 'get'
            }));

          case 1:
          case "end":
            return _context20.stop();
        }
      }
    }, _callee20);
  }));
  return _loadTemplateToClone.apply(this, arguments);
}