"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateLogistics = exports.INVALID_NAME_CHARS = void 0;

var _i18n = require("@kbn/i18n");

var _index_patterns = require("ui/index_patterns");

var _validation_helpers = require("./validation_helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var validateIndexPattern = function validateIndexPattern(indexPattern) {
  if (indexPattern) {
    var errors = (0, _index_patterns.validateIndexPattern)(indexPattern);

    if (errors[_index_patterns.ILLEGAL_CHARACTERS]) {
      return _i18n.i18n.translate('xpack.idxMgmt.templateValidation.indexPatternInvalidCharactersError', {
        defaultMessage: "'{indexPattern}' index pattern contains the invalid {characterListLength, plural, one {character} other {characters}} { characterList }.",
        values: {
          characterList: errors[_index_patterns.ILLEGAL_CHARACTERS].join(' '),
          characterListLength: errors[_index_patterns.ILLEGAL_CHARACTERS].length,
          indexPattern: indexPattern
        }
      });
    }

    if (errors[_index_patterns.CONTAINS_SPACES]) {
      return _i18n.i18n.translate('xpack.idxMgmt.templateValidation.indexPatternSpacesError', {
        defaultMessage: "'{indexPattern}' index pattern contains spaces.",
        values: {
          indexPattern: indexPattern
        }
      });
    }
  }
};

var INVALID_NAME_CHARS = ['"', '*', '\\', ',', '?'];
exports.INVALID_NAME_CHARS = INVALID_NAME_CHARS;

var doesStringIncludeChar = function doesStringIncludeChar(string, chars) {
  var invalidChar = chars.find(function (char) {
    return string.includes(char);
  }) || null;
  var containsChar = invalidChar !== null;
  return {
    containsChar: containsChar,
    invalidChar: invalidChar
  };
};

var validateLogistics = function validateLogistics(template) {
  var name = template.name,
      indexPatterns = template.indexPatterns;
  var validation = {
    isValid: true,
    errors: {
      indexPatterns: [],
      name: []
    }
  }; // Name validation

  if (name !== undefined && (0, _validation_helpers.isStringEmpty)(name)) {
    validation.errors.name.push(_i18n.i18n.translate('xpack.idxMgmt.templateValidation.templateNameRequiredError', {
      defaultMessage: 'A template name is required.'
    }));
  } else {
    if (name.includes(' ')) {
      validation.errors.name.push(_i18n.i18n.translate('xpack.idxMgmt.templateValidation.templateNameSpacesError', {
        defaultMessage: 'Spaces are not allowed in a template name.'
      }));
    }

    if (name.startsWith('_')) {
      validation.errors.name.push(_i18n.i18n.translate('xpack.idxMgmt.templateValidation.templateNameUnderscoreError', {
        defaultMessage: 'A template name must not start with an underscore.'
      }));
    }

    if (name.startsWith('.')) {
      validation.errors.name.push(_i18n.i18n.translate('xpack.idxMgmt.templateValidation.templateNamePeriodError', {
        defaultMessage: 'A template name must not start with a period.'
      }));
    }

    var _doesStringIncludeCha = doesStringIncludeChar(name, INVALID_NAME_CHARS),
        containsChar = _doesStringIncludeCha.containsChar,
        invalidChar = _doesStringIncludeCha.invalidChar;

    if (containsChar) {
      validation.errors.name = [_i18n.i18n.translate('xpack.idxMgmt.templateValidation.templateNameInvalidaCharacterError', {
        defaultMessage: 'A template name must not contain the character "{invalidChar}"',
        values: {
          invalidChar: invalidChar
        }
      })];
    }
  } // Index patterns validation


  if (Array.isArray(indexPatterns) && indexPatterns.length === 0) {
    validation.errors.indexPatterns.push(_i18n.i18n.translate('xpack.idxMgmt.templateValidation.indexPatternsRequiredError', {
      defaultMessage: 'At least one index pattern is required.'
    }));
  } else if (Array.isArray(indexPatterns) && indexPatterns.length) {
    indexPatterns.forEach(function (pattern) {
      var errorMsg = validateIndexPattern(pattern);

      if (errorMsg) {
        validation.errors.indexPatterns.push(errorMsg);
      }
    });
  }

  validation.errors = (0, _validation_helpers.removeEmptyErrorFields)(validation.errors);
  validation.isValid = (0, _validation_helpers.isValid)(validation.errors);
  return validation;
};

exports.validateLogistics = validateLogistics;