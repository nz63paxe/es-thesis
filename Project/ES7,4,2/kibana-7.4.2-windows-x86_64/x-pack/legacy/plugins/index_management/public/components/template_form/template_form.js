"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TemplateForm = void 0;

var _react = _interopRequireWildcard(require("react"));

var _react2 = require("@kbn/i18n/react");

var _eui = require("@elastic/eui");

var _template_steps = require("./template_steps");

var _steps = require("./steps");

var _template_validation = require("../../services/template_validation");

var _ = require("..");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var defaultValidation = {
  isValid: true,
  errors: {}
};
var stepComponentMap = {
  1: _steps.StepLogistics,
  2: _steps.StepSettings,
  3: _steps.StepMappings,
  4: _steps.StepAliases,
  5: _steps.StepReview
};
var stepValidationMap = {
  1: _template_validation.validateLogistics,
  2: _template_validation.validateSettings,
  3: _template_validation.validateMappings,
  4: _template_validation.validateAliases
};

var TemplateForm = function TemplateForm(_ref) {
  var initialTemplate = _ref.template,
      onSave = _ref.onSave,
      isSaving = _ref.isSaving,
      saveError = _ref.saveError,
      clearSaveError = _ref.clearSaveError,
      isEditing = _ref.isEditing;

  // hooks
  var _useState = (0, _react.useState)(1),
      _useState2 = _slicedToArray(_useState, 2),
      currentStep = _useState2[0],
      setCurrentStep = _useState2[1];

  var _useState3 = (0, _react.useState)(initialTemplate),
      _useState4 = _slicedToArray(_useState3, 2),
      template = _useState4[0],
      setTemplate = _useState4[1];

  var _useState5 = (0, _react.useState)({
    1: defaultValidation,
    2: defaultValidation,
    3: defaultValidation,
    4: defaultValidation,
    5: defaultValidation
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      validation = _useState6[0],
      setValidation = _useState6[1];

  var lastStep = Object.keys(stepComponentMap).length;
  var CurrentStepComponent = stepComponentMap[currentStep];
  var validateStep = stepValidationMap[currentStep];
  var stepErrors = validation[currentStep].errors;
  var isStepValid = validation[currentStep].isValid;

  var updateValidation = function updateValidation(templateToValidate) {
    var stepValidation = validateStep(templateToValidate);

    var newValidation = _objectSpread({}, validation, {}, _defineProperty({}, currentStep, stepValidation));

    setValidation(newValidation);
  };

  var updateTemplate = function updateTemplate(updatedTemplate) {
    var newTemplate = _objectSpread({}, template, {}, updatedTemplate);

    updateValidation(newTemplate);
    setTemplate(newTemplate);
  };

  var updateCurrentStep = function updateCurrentStep(nextStep) {
    // All steps needs validation, except for the last step
    var shouldValidate = currentStep !== lastStep; // If step is invalid do not let user proceed

    if (shouldValidate && !isStepValid) {
      return;
    }

    setCurrentStep(nextStep);
    clearSaveError();
  };

  var onBack = function onBack() {
    var prevStep = currentStep - 1;
    updateCurrentStep(prevStep);
  };

  var onNext = function onNext() {
    var nextStep = currentStep + 1;
    updateCurrentStep(nextStep);
  };

  var saveButtonLabel = isEditing ? _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.saveButtonLabel",
    defaultMessage: "Save template"
  }) : _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.createButtonLabel",
    defaultMessage: "Create template"
  });
  (0, _react.useEffect)(function () {
    if (!isEditing) {
      updateValidation(template);
    }
  }, []);
  return _react.default.createElement(_react.Fragment, null, _react.default.createElement(_template_steps.TemplateSteps, {
    currentStep: currentStep,
    updateCurrentStep: updateCurrentStep,
    isCurrentStepValid: isStepValid
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), saveError ? _react.default.createElement(_react.Fragment, null, _react.default.createElement(_.SectionError, {
    title: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.saveTemplateError",
      defaultMessage: "Unable to create template"
    }),
    error: saveError,
    "data-test-subj": "saveTemplateError"
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  })) : null, _react.default.createElement(_eui.EuiForm, {
    "data-test-subj": "templateForm"
  }, _react.default.createElement(CurrentStepComponent, {
    template: template,
    updateTemplate: updateTemplate,
    errors: stepErrors,
    updateCurrentStep: updateCurrentStep,
    isEditing: isEditing
  }), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiFlexGroup, null, currentStep > 1 ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    iconType: "arrowLeft",
    onClick: onBack,
    "data-test-subj": "backButton"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.backButtonLabel",
    defaultMessage: "Back"
  }))) : null, currentStep < lastStep ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    iconType: "arrowRight",
    onClick: onNext,
    iconSide: "right",
    disabled: !isStepValid,
    "data-test-subj": "nextButton"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.nextButtonLabel",
    defaultMessage: "Next"
  }))) : null, currentStep === lastStep ? _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButton, {
    fill: true,
    color: "secondary",
    iconType: "check",
    onClick: onSave.bind(null, template),
    "data-test-subj": "submitButton",
    isLoading: isSaving
  }, isSaving ? _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.savingButtonLabel",
    defaultMessage: "Saving..."
  }) : saveButtonLabel)) : null)))), _react.default.createElement(_eui.EuiSpacer, {
    size: "m"
  }));
};

exports.TemplateForm = TemplateForm;