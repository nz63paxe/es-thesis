"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateJSON = exports.isStringEmpty = exports.isValid = exports.removeEmptyErrorFields = void 0;

var _i18n = require("@kbn/i18n");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var removeEmptyErrorFields = function removeEmptyErrorFields(errors) {
  return Object.entries(errors).filter(function (_ref) {
    var _ref2 = _slicedToArray(_ref, 2),
        _key = _ref2[0],
        value = _ref2[1];

    return value.length > 0;
  }).reduce(function (errs, _ref3) {
    var _ref4 = _slicedToArray(_ref3, 2),
        key = _ref4[0],
        value = _ref4[1];

    errs[key] = value;
    return errs;
  }, {});
};

exports.removeEmptyErrorFields = removeEmptyErrorFields;

var isValid = function isValid(errors) {
  return Boolean(Object.keys(errors).length === 0);
};

exports.isValid = isValid;

var isStringEmpty = function isStringEmpty(str) {
  return str ? !Boolean(str.trim()) : true;
};

exports.isStringEmpty = isStringEmpty;

var validateJSON = function validateJSON(jsonString) {
  var invalidJsonMsg = _i18n.i18n.translate('xpack.idxMgmt.templateValidation.invalidJSONError', {
    defaultMessage: 'Invalid JSON format.'
  });

  try {
    var parsedSettingsJson = JSON.parse(jsonString);

    if (parsedSettingsJson && _typeof(parsedSettingsJson) !== 'object') {
      return invalidJsonMsg;
    }

    return;
  } catch (e) {
    return invalidJsonMsg;
  }
};

exports.validateJSON = validateJSON;