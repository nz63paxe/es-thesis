"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.setBreadcrumbs = void 0;

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _management = require("ui/management");

var _i18n = require("@kbn/i18n");

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var homeBreadcrumb = {
  text: _i18n.i18n.translate('xpack.idxMgmt.breadcrumb.homeLabel', {
    defaultMessage: 'Index Management'
  }),
  href: "#".concat(_constants.BASE_PATH)
};
var templatesBreadcrumb = {
  text: _i18n.i18n.translate('xpack.idxMgmt.breadcrumb.templatesLabel', {
    defaultMessage: 'Templates'
  }),
  href: "#".concat(_constants.BASE_PATH, "templates")
};
var breadcrumbsMap = {
  templateCreate: {
    text: _i18n.i18n.translate('xpack.idxMgmt.breadcrumb.createTemplateLabel', {
      defaultMessage: 'Create template'
    })
  },
  templateEdit: {
    text: _i18n.i18n.translate('xpack.idxMgmt.breadcrumb.editTemplateLabel', {
      defaultMessage: 'Edit template'
    })
  },
  templateClone: {
    text: _i18n.i18n.translate('xpack.idxMgmt.breadcrumb.cloneTemplateLabel', {
      defaultMessage: 'Clone template'
    })
  }
};

var setBreadcrumbs = function setBreadcrumbs(type) {
  var breadcrumbs = type ? [_management.MANAGEMENT_BREADCRUMB, homeBreadcrumb, templatesBreadcrumb, breadcrumbsMap[type]] : [_management.MANAGEMENT_BREADCRUMB, homeBreadcrumb];

  _chrome.default.breadcrumbs.set(breadcrumbs);
};

exports.setBreadcrumbs = setBreadcrumbs;