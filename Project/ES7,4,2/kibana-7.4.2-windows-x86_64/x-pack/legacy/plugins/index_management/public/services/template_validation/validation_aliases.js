"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateAliases = void 0;

var _validation_helpers = require("./validation_helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var validateAliases = function validateAliases(template) {
  var aliases = template.aliases;
  var validation = {
    isValid: true,
    errors: {
      aliases: []
    }
  }; // Aliases JSON validation

  if (typeof aliases === 'string' && !(0, _validation_helpers.isStringEmpty)(aliases)) {
    var validationMsg = (0, _validation_helpers.validateJSON)(aliases);

    if (typeof validationMsg === 'string') {
      validation.errors.aliases.push(validationMsg);
    }
  }

  validation.errors = (0, _validation_helpers.removeEmptyErrorFields)(validation.errors);
  validation.isValid = (0, _validation_helpers.isValid)(validation.errors);
  return validation;
};

exports.validateAliases = validateAliases;