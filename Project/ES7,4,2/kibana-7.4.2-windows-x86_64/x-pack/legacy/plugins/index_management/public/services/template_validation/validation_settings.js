"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.validateSettings = void 0;

var _validation_helpers = require("./validation_helpers");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var validateSettings = function validateSettings(template) {
  var settings = template.settings;
  var validation = {
    isValid: true,
    errors: {
      settings: []
    }
  }; // Settings JSON validation

  if (typeof settings === 'string' && !(0, _validation_helpers.isStringEmpty)(settings)) {
    var validationMsg = (0, _validation_helpers.validateJSON)(settings);

    if (typeof validationMsg === 'string') {
      validation.errors.settings.push(validationMsg);
    }
  }

  validation.errors = (0, _validation_helpers.removeEmptyErrorFields)(validation.errors);
  validation.isValid = (0, _validation_helpers.isValid)(validation.errors);
  return validation;
};

exports.validateSettings = validateSettings;