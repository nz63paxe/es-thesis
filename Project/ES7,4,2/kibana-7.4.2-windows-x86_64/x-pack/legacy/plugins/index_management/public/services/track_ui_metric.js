"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "METRIC_TYPE", {
  enumerable: true,
  get: function get() {
    return _public.METRIC_TYPE;
  }
});
exports.trackUiMetric = void 0;

var _public = require("../../../../../../src/legacy/core_plugins/ui_metric/public");

var _constants = require("../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var trackUiMetric = (0, _public.createUiStatsReporter)(_constants.UIM_APP_NAME);
exports.trackUiMetric = trackUiMetric;