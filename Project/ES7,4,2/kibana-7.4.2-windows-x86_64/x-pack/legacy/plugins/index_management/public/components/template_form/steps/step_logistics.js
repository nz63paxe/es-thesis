"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StepLogistics = void 0;

var _react = _interopRequireWildcard(require("react"));

var _eui = require("@elastic/eui");

var _react2 = require("@kbn/i18n/react");

var _constants = require("../../../../common/constants");

var _documentation_links = require("../../../lib/documentation_links");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var indexPatternInvalidCharacters = _constants.INVALID_INDEX_PATTERN_CHARS.join(' ');

var StepLogistics = function StepLogistics(_ref) {
  var template = _ref.template,
      updateTemplate = _ref.updateTemplate,
      errors = _ref.errors,
      isEditing = _ref.isEditing;
  var name = template.name,
      order = template.order,
      version = template.version,
      indexPatterns = template.indexPatterns;
  var nameError = errors.name,
      indexPatternsError = errors.indexPatterns; // hooks

  var _useState = (0, _react.useState)([]),
      _useState2 = _slicedToArray(_useState, 2),
      allIndexPatterns = _useState2[0],
      setAllIndexPatterns = _useState2[1];

  var _useState3 = (0, _react.useState)({
    name: false,
    indexPatterns: false
  }),
      _useState4 = _slicedToArray(_useState3, 2),
      touchedFields = _useState4[0],
      setTouchedFields = _useState4[1];

  var indexPatternOptions = indexPatterns ? indexPatterns.map(function (pattern) {
    return {
      label: pattern,
      value: pattern
    };
  }) : [];
  var isNameTouched = touchedFields.name,
      isIndexPatternsTouched = touchedFields.indexPatterns;
  return _react.default.createElement("div", {
    "data-test-subj": "stepLogistics"
  }, _react.default.createElement(_eui.EuiFlexGroup, {
    justifyContent: "spaceBetween"
  }, _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiTitle, null, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepLogistics.stepTitle",
    defaultMessage: "Logistics"
  })))), _react.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react.default.createElement(_eui.EuiButtonEmpty, {
    size: "s",
    flush: "right",
    href: _documentation_links.templatesDocumentationLink,
    target: "_blank",
    iconType: "help"
  }, _react.default.createElement(_react2.FormattedMessage, {
    id: "xpack.idxMgmt.templateForm.stepLogistics.docsButtonLabel",
    defaultMessage: "Index Templates docs"
  })))), _react.default.createElement(_eui.EuiSpacer, {
    size: "l"
  }), _react.default.createElement(_eui.EuiDescribedFormGroup, {
    title: _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.nameTitle",
      defaultMessage: "Name"
    }))),
    description: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.nameDescription",
      defaultMessage: "A unique identifier for this template."
    }),
    idAria: "stepLogisticsNameDescription",
    fullWidth: true
  }, _react.default.createElement(_eui.EuiFormRow, {
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.fieldNameLabel",
      defaultMessage: "Name"
    }),
    isInvalid: isNameTouched && Boolean(nameError),
    error: nameError,
    fullWidth: true
  }, _react.default.createElement(_eui.EuiFieldText, {
    value: name,
    readOnly: isEditing,
    onBlur: function onBlur() {
      return setTouchedFields(function (prevTouched) {
        return _objectSpread({}, prevTouched, {
          name: true
        });
      });
    },
    "data-test-subj": "nameInput",
    onChange: function onChange(e) {
      updateTemplate({
        name: e.target.value
      });
    },
    fullWidth: true
  }))), _react.default.createElement(_eui.EuiDescribedFormGroup, {
    title: _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.indexPatternsTitle",
      defaultMessage: "Index patterns"
    }))),
    description: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.indexPatternsDescription",
      defaultMessage: "The index patterns to apply to the template."
    }),
    idAria: "stepLogisticsIndexPatternsDescription",
    fullWidth: true
  }, _react.default.createElement(_eui.EuiFormRow, {
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.fieldIndexPatternsLabel",
      defaultMessage: "Index patterns"
    }),
    helpText: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.fieldIndexPatternsHelpText",
      defaultMessage: "Spaces and the characters {invalidCharactersList} are not allowed.",
      values: {
        invalidCharactersList: _react.default.createElement("strong", null, indexPatternInvalidCharacters)
      }
    }),
    isInvalid: isIndexPatternsTouched && Boolean(indexPatternsError),
    error: indexPatternsError,
    fullWidth: true
  }, _react.default.createElement(_eui.EuiComboBox, {
    noSuggestions: true,
    fullWidth: true,
    "data-test-subj": "indexPatternsComboBox",
    selectedOptions: indexPatternOptions,
    onBlur: function onBlur() {
      return setTouchedFields(function (prevTouched) {
        return _objectSpread({}, prevTouched, {
          indexPatterns: true
        });
      });
    },
    onChange: function onChange(selectedPattern) {
      var newIndexPatterns = selectedPattern.map(function (_ref2) {
        var value = _ref2.value;
        return value;
      });
      updateTemplate({
        indexPatterns: newIndexPatterns
      });
    },
    onCreateOption: function onCreateOption(selectedPattern) {
      if (!selectedPattern.trim().length) {
        return;
      }

      var newIndexPatterns = [].concat(_toConsumableArray(indexPatterns), [selectedPattern]);
      setAllIndexPatterns([].concat(_toConsumableArray(allIndexPatterns), [selectedPattern]));
      updateTemplate({
        indexPatterns: newIndexPatterns
      });
    }
  }))), _react.default.createElement(_eui.EuiDescribedFormGroup, {
    title: _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.orderTitle",
      defaultMessage: "Merge order"
    }))),
    description: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.orderDescription",
      defaultMessage: "The merge order when multiple templates match an index."
    }),
    idAria: "stepLogisticsOrderDescription",
    fullWidth: true
  }, _react.default.createElement(_eui.EuiFormRow, {
    fullWidth: true,
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.fieldOrderLabel",
      defaultMessage: "Order (optional)"
    })
  }, _react.default.createElement(_eui.EuiFieldNumber, {
    fullWidth: true,
    value: order,
    onChange: function onChange(e) {
      var value = e.target.value;
      updateTemplate({
        order: value === '' ? value : Number(value)
      });
    },
    "data-test-subj": "orderInput"
  }))), ' ', _react.default.createElement(_eui.EuiDescribedFormGroup, {
    title: _react.default.createElement(_eui.EuiTitle, {
      size: "s"
    }, _react.default.createElement("h3", null, _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.versionTitle",
      defaultMessage: "Version"
    }))),
    description: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.versionDescription",
      defaultMessage: "A number that identifies the template to external management systems."
    }),
    idAria: "stepLogisticsVersionDescription",
    fullWidth: true
  }, _react.default.createElement(_eui.EuiFormRow, {
    fullWidth: true,
    label: _react.default.createElement(_react2.FormattedMessage, {
      id: "xpack.idxMgmt.templateForm.stepLogistics.fieldVersionLabel",
      defaultMessage: "Version (optional)"
    })
  }, _react.default.createElement(_eui.EuiFieldNumber, {
    fullWidth: true,
    value: version,
    onChange: function onChange(e) {
      var value = e.target.value;
      updateTemplate({
        version: value === '' ? value : Number(value)
      });
    },
    "data-test-subj": "versionInput"
  }))));
};

exports.StepLogistics = StepLogistics;