"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.idxMgmtDocumentationLink = exports.templatesDocumentationLink = exports.mappingDocumentationLink = exports.settingsDocumentationLink = void 0;

var _documentation_links = require("ui/documentation_links");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var base = "".concat(_documentation_links.ELASTIC_WEBSITE_URL, "guide/en");
var esBase = "".concat(base, "/elasticsearch/reference/").concat(_documentation_links.DOC_LINK_VERSION);
var kibanaBase = "".concat(base, "/kibana/").concat(_documentation_links.DOC_LINK_VERSION);
var settingsDocumentationLink = "".concat(esBase, "/index-modules.html#index-modules-settings");
exports.settingsDocumentationLink = settingsDocumentationLink;
var mappingDocumentationLink = "".concat(esBase, "/mapping.html");
exports.mappingDocumentationLink = mappingDocumentationLink;
var templatesDocumentationLink = "".concat(esBase, "/indices-templates.html");
exports.templatesDocumentationLink = templatesDocumentationLink;
var idxMgmtDocumentationLink = "".concat(kibanaBase, "/managing-indices.html");
exports.idxMgmtDocumentationLink = idxMgmtDocumentationLink;