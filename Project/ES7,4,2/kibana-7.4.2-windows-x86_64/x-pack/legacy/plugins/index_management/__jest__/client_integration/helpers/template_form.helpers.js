"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.formSetup = void 0;

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const formSetup = async initTestBed => {
  const testBed = await initTestBed(); // User actions

  const clickNextButton = () => {
    testBed.find('nextButton').simulate('click');
  };

  const clickBackButton = () => {
    testBed.find('backButton').simulate('click');
  };

  const clickSubmitButton = () => {
    testBed.find('submitButton').simulate('click');
  };

  const completeStepOne = ({
    name,
    indexPatterns,
    order,
    version
  }) => {
    const {
      form,
      find
    } = testBed;

    if (name) {
      form.setInputValue('nameInput', name);
    }

    if (indexPatterns) {
      const indexPatternsFormatted = indexPatterns.map(pattern => ({
        label: pattern,
        value: pattern
      }));
      find('mockComboBox').simulate('change', indexPatternsFormatted); // Using mocked EuiComboBox
    }

    if (order) {
      form.setInputValue('orderInput', JSON.stringify(order));
    }

    if (version) {
      form.setInputValue('versionInput', JSON.stringify(version));
    }

    clickNextButton();
  };

  const completeStepTwo = ({
    settings
  }) => {
    const {
      find
    } = testBed;

    if (settings) {
      find('mockCodeEditor').simulate('change', {
        jsonString: settings
      }); // Using mocked EuiCodeEditor
    }

    clickNextButton();
  };

  const completeStepThree = ({
    mappings
  }) => {
    const {
      find
    } = testBed;

    if (mappings) {
      find('mockCodeEditor').simulate('change', {
        jsonString: mappings
      }); // Using mocked EuiCodeEditor
    }

    clickNextButton();
  };

  const completeStepFour = ({
    aliases
  }) => {
    const {
      find
    } = testBed;

    if (aliases) {
      find('mockCodeEditor').simulate('change', {
        jsonString: aliases
      }); // Using mocked EuiCodeEditor
    }

    clickNextButton();
  };

  const selectSummaryTab = tab => {
    const tabs = ['summary', 'request'];
    testBed.find('summaryTabContent').find('.euiTab').at(tabs.indexOf(tab)).simulate('click');
  };

  return { ...testBed,
    actions: {
      clickNextButton,
      clickBackButton,
      clickSubmitButton,
      completeStepOne,
      completeStepTwo,
      completeStepThree,
      completeStepFour,
      selectSummaryTab
    }
  };
};

exports.formSetup = formSetup;