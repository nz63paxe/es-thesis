"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PLUGIN = void 0;

var _i18n = require("@kbn/i18n");

var _constants = require("../../../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const PLUGIN = {
  ID: 'index_management',
  NAME: _i18n.i18n.translate('xpack.idxMgmt.appTitle', {
    defaultMessage: 'Index Management'
  }),
  MINIMUM_LICENSE_REQUIRED: _constants.LICENSE_TYPE_BASIC
};
exports.PLUGIN = PLUGIN;