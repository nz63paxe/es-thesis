"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initInternalSpacesApi = initInternalSpacesApi;

var _boom = _interopRequireDefault(require("boom"));

var _errors = require("../../../lib/errors");

var _spaces_url_parser = require("../../../lib/spaces_url_parser");

var _lib = require("../../lib");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initInternalSpacesApi(deps) {
  const {
    http,
    config,
    spacesService,
    savedObjects,
    routePreCheckLicenseFn
  } = deps;
  http.route({
    method: 'POST',
    path: '/api/spaces/v1/space/{id}/select',

    async handler(request) {
      const {
        SavedObjectsClient
      } = savedObjects;
      const spacesClient = await spacesService.scopedClient(request);
      const id = request.params.id;

      try {
        const existingSpace = await (0, _lib.getSpaceById)(spacesClient, id, SavedObjectsClient.errors);

        if (!existingSpace) {
          return _boom.default.notFound();
        }

        return {
          location: (0, _spaces_url_parser.addSpaceIdToPath)(config.get('server.basePath'), existingSpace.id, config.get('server.defaultRoute'))
        };
      } catch (error) {
        return (0, _errors.wrapError)(error);
      }
    },

    options: {
      pre: [routePreCheckLicenseFn]
    }
  });
}