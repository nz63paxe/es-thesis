"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSpaceById = getSpaceById;

var _convert_saved_object_to_space = require("./convert_saved_object_to_space");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getSpaceById(client, spaceId, errors) {
  try {
    const existingSpace = await client.get(spaceId);
    return (0, _convert_saved_object_to_space.convertSavedObjectToSpace)(existingSpace);
  } catch (error) {
    if (errors.isNotFoundError(error)) {
      return null;
    }

    throw error;
  }
}