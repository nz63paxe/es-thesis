"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initExternalSpacesApi = initExternalSpacesApi;

var _route_pre_check_license = require("../../../lib/route_pre_check_license");

var _delete = require("./delete");

var _get = require("./get");

var _post = require("./post");

var _put = require("./put");

var _copy_to_space = require("./copy_to_space");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initExternalSpacesApi({
  xpackMain,
  ...rest
}) {
  const routePreCheckLicenseFn = (0, _route_pre_check_license.routePreCheckLicense)({
    xpackMain
  });
  const deps = { ...rest,
    routePreCheckLicenseFn
  };
  (0, _delete.initDeleteSpacesApi)(deps);
  (0, _get.initGetSpacesApi)(deps);
  (0, _post.initPostSpacesApi)(deps);
  (0, _put.initPutSpacesApi)(deps);
  (0, _copy_to_space.initCopyToSpacesApi)(deps);
}