"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initInternalApis = initInternalApis;

var _route_pre_check_license = require("../../../lib/route_pre_check_license");

var _spaces = require("./spaces");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initInternalApis({
  xpackMain,
  ...rest
}) {
  const routePreCheckLicenseFn = (0, _route_pre_check_license.routePreCheckLicense)({
    xpackMain
  });
  const deps = { ...rest,
    routePreCheckLicenseFn
  };
  (0, _spaces.initInternalSpacesApi)(deps);
}