"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.spaceSchema = exports.SPACE_ID_REGEX = void 0;

var _joi = _interopRequireDefault(require("joi"));

var _constants = require("../../common/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
const SPACE_ID_REGEX = /^[a-z0-9_\-]+$/;
exports.SPACE_ID_REGEX = SPACE_ID_REGEX;

const spaceSchema = _joi.default.object({
  id: _joi.default.string().regex(SPACE_ID_REGEX, `lower case, a-z, 0-9, "_", and "-" are allowed`),
  name: _joi.default.string().required(),
  description: _joi.default.string().allow(''),
  initials: _joi.default.string().max(_constants.MAX_SPACE_INITIALS),
  color: _joi.default.string().regex(/^#[a-zA-Z0-9]{6}$/, `6 digit hex color, starting with a #`),
  disabledFeatures: _joi.default.array().items(_joi.default.string()).default([]),
  _reserved: _joi.default.boolean()
}).default();

exports.spaceSchema = spaceSchema;