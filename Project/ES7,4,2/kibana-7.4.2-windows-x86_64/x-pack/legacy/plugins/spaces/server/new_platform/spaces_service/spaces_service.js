"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SpacesService = void 0;

var _operators = require("rxjs/operators");

var _rxjs = require("rxjs");

var _constants = require("../../../common/constants");

var _spaces_client = require("../../lib/spaces_client");

var _spaces_url_parser = require("../../lib/spaces_url_parser");

var _namespace = require("../../lib/utils/namespace");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class SpacesService {
  constructor(log, serverBasePath) {
    this.log = log;
    this.serverBasePath = serverBasePath;

    _defineProperty(this, "configSubscription$", void 0);
  }

  async setup({
    http,
    elasticsearch,
    savedObjects,
    security,
    config$,
    spacesAuditLogger
  }) {
    const getSpaceId = request => {
      // Currently utilized by reporting
      const isFakeRequest = typeof request.getBasePath === 'function';
      const basePath = isFakeRequest ? request.getBasePath() : http.basePath.get(request);
      const spaceId = (0, _spaces_url_parser.getSpaceIdFromPath)(basePath, this.serverBasePath);
      return spaceId;
    };

    return {
      getSpaceId,
      getBasePath: spaceId => {
        if (!spaceId) {
          throw new TypeError(`spaceId is required to retrieve base path`);
        }

        return (0, _spaces_url_parser.addSpaceIdToPath)(this.serverBasePath, spaceId);
      },
      isInDefaultSpace: request => {
        const spaceId = getSpaceId(request);
        return spaceId === _constants.DEFAULT_SPACE_ID;
      },
      spaceIdToNamespace: _namespace.spaceIdToNamespace,
      namespaceToSpaceId: _namespace.namespaceToSpaceId,
      scopedClient: async request => {
        return (0, _rxjs.combineLatest)(elasticsearch.adminClient$, config$).pipe((0, _operators.map)(([clusterClient, config]) => {
          const internalRepository = savedObjects.getSavedObjectsRepository(clusterClient.callAsInternalUser, ['space']);
          const callCluster = clusterClient.asScoped(request).callAsCurrentUser;
          const callWithRequestRepository = savedObjects.getSavedObjectsRepository(callCluster, ['space']);
          const authorization = security.isEnabled ? security.authorization : null;
          return new _spaces_client.SpacesClient(spacesAuditLogger, message => {
            this.log.debug(message);
          }, authorization, callWithRequestRepository, config, internalRepository, request);
        }), (0, _operators.take)(1)).toPromise();
      }
    };
  }

  async stop() {
    if (this.configSubscription$) {
      this.configSubscription$.unsubscribe();
      this.configSubscription$ = undefined;
    }
  }

}

exports.SpacesService = SpacesService;