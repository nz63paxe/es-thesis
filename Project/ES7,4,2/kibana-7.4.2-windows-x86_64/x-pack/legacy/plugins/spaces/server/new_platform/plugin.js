"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Plugin = void 0;

var _create_default_space = require("../lib/create_default_space");

var _watch_status_and_license_to_initialize = require("../../../../server/lib/watch_status_and_license_to_initialize");

var _check_license = require("../lib/check_license");

var _saved_objects_client_wrapper_factory = require("../lib/saved_objects_client/saved_objects_client_wrapper_factory");

var _audit_logger = require("../lib/audit_logger");

var _spaces_tutorial_context_factory = require("../lib/spaces_tutorial_context_factory");

var _v = require("../routes/api/v1");

var _external = require("../routes/api/external");

var _get_spaces_usage_collector = require("../lib/get_spaces_usage_collector");

var _spaces_service = require("./spaces_service");

var _get_active_space = require("../lib/get_active_space");

var _toggle_ui_capabilities = require("../lib/toggle_ui_capabilities");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Plugin {
  constructor(initializerContext) {
    this.initializerContext = initializerContext;

    _defineProperty(this, "pluginId", 'spaces');

    _defineProperty(this, "config$", void 0);

    _defineProperty(this, "log", void 0);

    this.config$ = initializerContext.config.create();
    this.log = initializerContext.logger.get();
  }

  async setup(core, plugins) {
    const xpackMainPlugin = plugins.xpackMain;
    (0, _watch_status_and_license_to_initialize.watchStatusAndLicenseToInitialize)(xpackMainPlugin, plugins.spaces, async () => {
      await (0, _create_default_space.createDefaultSpace)({
        elasticsearch: core.elasticsearch,
        savedObjects: core.savedObjects
      });
    }); // Register a function that is called whenever the xpack info changes,
    // to re-compute the license check results for this plugin.

    xpackMainPlugin.info.feature(this.pluginId).registerLicenseCheckResultsGenerator(_check_license.checkLicense);
    const spacesAuditLogger = new _audit_logger.SpacesAuditLogger(core.auditLogger.create(this.pluginId));
    const service = new _spaces_service.SpacesService(this.log, this.initializerContext.legacyConfig.get('server.basePath'));
    const spacesService = await service.setup({
      http: core.http,
      elasticsearch: core.elasticsearch,
      savedObjects: core.savedObjects,
      security: plugins.security,
      spacesAuditLogger,
      config$: this.config$
    });
    const {
      addScopedSavedObjectsClientWrapperFactory,
      types
    } = core.savedObjects;
    addScopedSavedObjectsClientWrapperFactory(Number.MIN_SAFE_INTEGER, 'spaces', (0, _saved_objects_client_wrapper_factory.spacesSavedObjectsClientWrapperFactory)(spacesService, types));
    core.tutorial.addScopedTutorialContextFactory((0, _spaces_tutorial_context_factory.createSpacesTutorialContextFactory)(spacesService));
    core.capabilities.registerCapabilitiesModifier(async (request, uiCapabilities) => {
      const spacesClient = await spacesService.scopedClient(request);

      try {
        const activeSpace = await (0, _get_active_space.getActiveSpace)(spacesClient, core.http.basePath.get(request), this.initializerContext.legacyConfig.get('server.basePath'));
        const features = plugins.xpackMain.getFeatures();
        return (0, _toggle_ui_capabilities.toggleUICapabilities)(features, uiCapabilities, activeSpace);
      } catch (e) {
        return uiCapabilities;
      }
    });
    (0, _v.initInternalApis)({
      http: core.http,
      config: this.initializerContext.legacyConfig,
      savedObjects: core.savedObjects,
      spacesService,
      xpackMain: xpackMainPlugin
    });
    (0, _external.initExternalSpacesApi)({
      http: core.http,
      log: this.log,
      savedObjects: core.savedObjects,
      spacesService,
      xpackMain: xpackMainPlugin
    }); // Register a function with server to manage the collection of usage stats

    core.usage.collectorSet.register((0, _get_spaces_usage_collector.getSpacesUsageCollector)({
      config: this.initializerContext.legacyConfig,
      usage: core.usage,
      xpackMain: xpackMainPlugin
    }));
    return {
      spacesService,
      log: this.log
    };
  }

}

exports.Plugin = Plugin;