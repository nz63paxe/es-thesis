"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initGetSpacesApi = initGetSpacesApi;

var _boom = _interopRequireDefault(require("boom"));

var _joi = _interopRequireDefault(require("joi"));

var _errors = require("../../../lib/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initGetSpacesApi(deps) {
  const {
    http,
    log,
    spacesService,
    savedObjects,
    routePreCheckLicenseFn
  } = deps;
  http.route({
    method: 'GET',
    path: '/api/spaces/space',

    async handler(request) {
      log.debug(`Inside GET /api/spaces/space`);
      const purpose = request.query.purpose;
      const spacesClient = await spacesService.scopedClient(request);
      let spaces;

      try {
        log.debug(`Attempting to retrieve all spaces for ${purpose} purpose`);
        spaces = await spacesClient.getAll(purpose);
        log.debug(`Retrieved ${spaces.length} spaces for ${purpose} purpose`);
      } catch (error) {
        log.debug(`Error retrieving spaces for ${purpose} purpose: ${error}`);
        return (0, _errors.wrapError)(error);
      }

      return spaces;
    },

    options: {
      pre: [routePreCheckLicenseFn],
      validate: {
        query: _joi.default.object().keys({
          purpose: _joi.default.string().valid('any', 'copySavedObjectsIntoSpace').default('any')
        })
      }
    }
  });
  http.route({
    method: 'GET',
    path: '/api/spaces/space/{id}',

    async handler(request) {
      const spaceId = request.params.id;
      const {
        SavedObjectsClient
      } = savedObjects;
      const spacesClient = await spacesService.scopedClient(request);

      try {
        return await spacesClient.get(spaceId);
      } catch (error) {
        if (SavedObjectsClient.errors.isNotFoundError(error)) {
          return _boom.default.notFound();
        }

        return (0, _errors.wrapError)(error);
      }
    },

    options: {
      pre: [routePreCheckLicenseFn]
    }
  });
}