"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initCopyToSpacesApi = initCopyToSpacesApi;

var _joi = _interopRequireDefault(require("joi"));

var _copy_to_spaces = require("../../../lib/copy_to_spaces");

var _copy_to_spaces2 = require("../../../lib/copy_to_spaces/copy_to_spaces");

var _space_schema = require("../../../lib/space_schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initCopyToSpacesApi(deps) {
  const {
    http,
    spacesService,
    savedObjects,
    routePreCheckLicenseFn
  } = deps;
  http.route({
    method: 'POST',
    path: '/api/spaces/_copy_saved_objects',

    async handler(request, h) {
      const savedObjectsClient = savedObjects.getScopedSavedObjectsClient(request, _copy_to_spaces2.COPY_TO_SPACES_SAVED_OBJECTS_CLIENT_OPTS);
      const copySavedObjectsToSpaces = (0, _copy_to_spaces.copySavedObjectsToSpacesFactory)(savedObjectsClient, savedObjects);
      const {
        spaces: destinationSpaceIds,
        objects,
        includeReferences,
        overwrite
      } = request.payload;
      const sourceSpaceId = spacesService.getSpaceId(request);
      const copyResponse = await copySavedObjectsToSpaces(sourceSpaceId, destinationSpaceIds, {
        objects,
        includeReferences,
        overwrite
      });
      return h.response(copyResponse);
    },

    options: {
      tags: ['access:copySavedObjectsToSpaces'],
      validate: {
        payload: {
          spaces: _joi.default.array().items(_joi.default.string().regex(_space_schema.SPACE_ID_REGEX, `lower case, a-z, 0-9, "_", and "-" are allowed`)).unique(),
          objects: _joi.default.array().items(_joi.default.object({
            type: _joi.default.string(),
            id: _joi.default.string()
          })).unique(),
          includeReferences: _joi.default.bool().default(false),
          overwrite: _joi.default.bool().default(false)
        }
      },
      pre: [routePreCheckLicenseFn]
    }
  });
  http.route({
    method: 'POST',
    path: '/api/spaces/_resolve_copy_saved_objects_errors',

    async handler(request, h) {
      const savedObjectsClient = savedObjects.getScopedSavedObjectsClient(request, _copy_to_spaces2.COPY_TO_SPACES_SAVED_OBJECTS_CLIENT_OPTS);
      const resolveCopySavedObjectsToSpacesConflicts = (0, _copy_to_spaces.resolveCopySavedObjectsToSpacesConflictsFactory)(savedObjectsClient, savedObjects);
      const {
        objects,
        includeReferences,
        retries
      } = request.payload;
      const sourceSpaceId = spacesService.getSpaceId(request);
      const resolveConflictsResponse = await resolveCopySavedObjectsToSpacesConflicts(sourceSpaceId, {
        objects,
        includeReferences,
        retries
      });
      return h.response(resolveConflictsResponse);
    },

    options: {
      tags: ['access:copySavedObjectsToSpaces'],
      validate: {
        payload: _joi.default.object({
          objects: _joi.default.array().items(_joi.default.object({
            type: _joi.default.string(),
            id: _joi.default.string()
          })).required().unique(),
          includeReferences: _joi.default.bool().default(false),
          retries: _joi.default.object().pattern(_space_schema.SPACE_ID_REGEX, _joi.default.array().items(_joi.default.object({
            type: _joi.default.string().required(),
            id: _joi.default.string().required(),
            overwrite: _joi.default.boolean().default(false)
          }))).required()
        }).default()
      },
      pre: [routePreCheckLicenseFn]
    }
  });
}