"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getActiveSpace = getActiveSpace;

var _errors = require("./errors");

var _spaces_url_parser = require("./spaces_url_parser");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
async function getActiveSpace(spacesClient, requestBasePath, serverBasePath) {
  const spaceId = (0, _spaces_url_parser.getSpaceIdFromPath)(requestBasePath, serverBasePath);

  try {
    return spacesClient.get(spaceId);
  } catch (e) {
    throw (0, _errors.wrapError)(e);
  }
}