"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "convertSavedObjectToSpace", {
  enumerable: true,
  get: function () {
    return _convert_saved_object_to_space.convertSavedObjectToSpace;
  }
});
Object.defineProperty(exports, "getSpaceById", {
  enumerable: true,
  get: function () {
    return _get_space_by_id.getSpaceById;
  }
});

var _convert_saved_object_to_space = require("./convert_saved_object_to_space");

var _get_space_by_id = require("./get_space_by_id");