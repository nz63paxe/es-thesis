"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initDeleteSpacesApi = initDeleteSpacesApi;

var _boom = _interopRequireDefault(require("boom"));

var _errors = require("../../../lib/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initDeleteSpacesApi(deps) {
  const {
    http,
    savedObjects,
    spacesService,
    routePreCheckLicenseFn
  } = deps;
  http.route({
    method: 'DELETE',
    path: '/api/spaces/space/{id}',

    async handler(request, h) {
      const {
        SavedObjectsClient
      } = savedObjects;
      const spacesClient = await spacesService.scopedClient(request);
      const id = request.params.id;
      let result;

      try {
        result = await spacesClient.delete(id);
      } catch (error) {
        if (SavedObjectsClient.errors.isNotFoundError(error)) {
          return _boom.default.notFound();
        }

        return (0, _errors.wrapError)(error);
      }

      return h.response(result).code(204);
    },

    options: {
      pre: [routePreCheckLicenseFn]
    }
  });
}