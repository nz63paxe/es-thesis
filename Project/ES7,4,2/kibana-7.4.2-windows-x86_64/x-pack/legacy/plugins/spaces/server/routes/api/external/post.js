"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.initPostSpacesApi = initPostSpacesApi;

var _boom = _interopRequireDefault(require("boom"));

var _errors = require("../../../lib/errors");

var _space_schema = require("../../../lib/space_schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
function initPostSpacesApi(deps) {
  const {
    http,
    log,
    spacesService,
    savedObjects,
    routePreCheckLicenseFn
  } = deps;
  http.route({
    method: 'POST',
    path: '/api/spaces/space',

    async handler(request) {
      log.debug(`Inside POST /api/spaces/space`);
      const {
        SavedObjectsClient
      } = savedObjects;
      const spacesClient = await spacesService.scopedClient(request);
      const space = request.payload;

      try {
        log.debug(`Attempting to create space`);
        return await spacesClient.create(space);
      } catch (error) {
        if (SavedObjectsClient.errors.isConflictError(error)) {
          return _boom.default.conflict(`A space with the identifier ${space.id} already exists.`);
        }

        log.debug(`Error creating space: ${error}`);
        return (0, _errors.wrapError)(error);
      }
    },

    options: {
      validate: {
        payload: _space_schema.spaceSchema
      },
      pre: [routePreCheckLicenseFn]
    }
  });
}