"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createTestHandler = createTestHandler;
exports.defaultPreCheckLicenseImpl = void 0;

var Rx = _interopRequireWildcard(require("rxjs"));

var _hapi = require("hapi");

var _mocks = require("src/core/server/mocks");

var _server = require("src/core/server");

var _stream = require("stream");

var _streams = require("src/legacy/utils/streams");

var _optional_plugin = require("../../../../../../server/lib/optional_plugin");

var _spaces_client = require("../../../lib/spaces_client");

var _create_spaces = require("./create_spaces");

var _spaces_service = require("../../../new_platform/spaces_service");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// eslint-disable-next-line @kbn/eslint/no-restricted-paths
const defaultPreCheckLicenseImpl = request => '';

exports.defaultPreCheckLicenseImpl = defaultPreCheckLicenseImpl;
const baseConfig = {
  'server.basePath': ''
};

async function readStreamToCompletion(stream) {
  return (0, _streams.createPromiseFromStreams)([stream, (0, _streams.createConcatStream)([])]);
}

function createTestHandler(initApiFn) {
  const teardowns = [];
  const spaces = (0, _create_spaces.createSpaces)();

  const request = async (method, path, options = {}) => {
    const {
      setupFn = () => {
        return;
      },
      testConfig = {},
      payload,
      preCheckLicenseImpl = defaultPreCheckLicenseImpl,
      expectPreCheckLicenseCall = true,
      expectSpacesClientCall = true
    } = options;
    let pre = jest.fn();

    if (preCheckLicenseImpl) {
      pre = pre.mockImplementation(preCheckLicenseImpl);
    }

    const server = new _hapi.Server();
    const config = { ...baseConfig,
      ...testConfig
    };
    await setupFn(server);
    const mockConfig = {
      get: key => config[key]
    };
    server.decorate('server', 'config', jest.fn(() => mockConfig));
    const mockSavedObjectsClientContract = {
      get: jest.fn((type, id) => {
        const result = spaces.filter(s => s.id === id);

        if (!result.length) {
          throw new Error(`not found: [${type}:${id}]`);
        }

        return result[0];
      }),
      find: jest.fn(() => {
        return {
          total: spaces.length,
          saved_objects: spaces
        };
      }),
      create: jest.fn((type, attributes, {
        id
      }) => {
        if (spaces.find(s => s.id === id)) {
          throw new Error('conflict');
        }

        return {};
      }),
      update: jest.fn((type, id) => {
        if (!spaces.find(s => s.id === id)) {
          throw new Error('not found: during update');
        }

        return {};
      }),
      delete: jest.fn((type, id) => {
        return {};
      }),
      deleteByNamespace: jest.fn()
    };
    server.savedObjects = {
      types: ['visualization', 'dashboard', 'index-pattern', 'globalType'],
      schema: new _server.SavedObjectsSchema({
        space: {
          isNamespaceAgnostic: true,
          hidden: true
        },
        globalType: {
          isNamespaceAgnostic: true
        }
      }),
      getScopedSavedObjectsClient: jest.fn().mockResolvedValue(mockSavedObjectsClientContract),
      importExport: {
        getSortedObjectsForExport: jest.fn().mockResolvedValue(new _stream.Readable({
          objectMode: true,

          read() {
            if (Array.isArray(payload.objects)) {
              payload.objects.forEach(o => this.push(o));
            }

            this.push(null);
          }

        })),
        importSavedObjects: jest.fn().mockImplementation(async opts => {
          const objectsToImport = await readStreamToCompletion(opts.readStream);
          return {
            success: true,
            successCount: objectsToImport.length
          };
        }),
        resolveImportErrors: jest.fn().mockImplementation(async opts => {
          const objectsToImport = await readStreamToCompletion(opts.readStream);
          return {
            success: true,
            successCount: objectsToImport.length
          };
        })
      },
      SavedObjectsClient: {
        errors: {
          isNotFoundError: jest.fn(e => e.message.startsWith('not found:')),
          isConflictError: jest.fn(e => e.message.startsWith('conflict'))
        }
      }
    };
    server.plugins.elasticsearch = {
      createCluster: jest.fn(),
      waitUntilReady: jest.fn(),
      getCluster: jest.fn().mockReturnValue({
        callWithRequest: jest.fn(),
        callWithInternalUser: jest.fn()
      })
    };
    const log = {
      log: jest.fn(),
      trace: jest.fn(),
      debug: jest.fn(),
      info: jest.fn(),
      warn: jest.fn(),
      error: jest.fn(),
      fatal: jest.fn()
    };
    const service = new _spaces_service.SpacesService(log, server.config().get('server.basePath'));
    const spacesService = await service.setup({
      http: _mocks.httpServiceMock.createSetupContract(),
      elasticsearch: _mocks.elasticsearchServiceMock.createSetupContract(),
      savedObjects: server.savedObjects,
      security: (0, _optional_plugin.createOptionalPlugin)({
        get: () => null
      }, 'xpack.security', {}, 'security'),
      spacesAuditLogger: {},
      config$: Rx.of({
        maxSpaces: 1000
      })
    });
    spacesService.scopedClient = jest.fn(req => {
      return Promise.resolve(new _spaces_client.SpacesClient(null, () => null, null, mockSavedObjectsClientContract, {
        maxSpaces: 1000
      }, mockSavedObjectsClientContract, req));
    });
    initApiFn({
      http: {
        server,
        route: server.route.bind(server)
      },
      routePreCheckLicenseFn: pre,
      savedObjects: server.savedObjects,
      spacesService,
      log,
      config: mockConfig
    });
    teardowns.push(() => server.stop());
    const headers = {
      authorization: 'foo'
    };

    const testRun = async () => {
      const response = await server.inject({
        method,
        url: path,
        headers,
        payload
      });

      if (preCheckLicenseImpl && expectPreCheckLicenseCall) {
        expect(pre).toHaveBeenCalled();
      } else {
        expect(pre).not.toHaveBeenCalled();
      }

      if (expectSpacesClientCall) {
        expect(spacesService.scopedClient).toHaveBeenCalledWith(expect.objectContaining({
          headers: expect.objectContaining({
            authorization: headers.authorization
          })
        }));
      } else {
        expect(spacesService.scopedClient).not.toHaveBeenCalled();
      }

      return response;
    };

    return {
      server,
      headers,
      mockSavedObjectsRepository: mockSavedObjectsClientContract,
      mockSavedObjectsService: server.savedObjects,
      response: await testRun()
    };
  };

  return {
    request,
    teardowns
  };
}