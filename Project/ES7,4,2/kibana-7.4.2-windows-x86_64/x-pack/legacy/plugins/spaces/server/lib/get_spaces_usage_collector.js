"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getSpacesUsageCollector = getSpacesUsageCollector;

var _lodash = require("lodash");

var _constants = require("../../../monitoring/common/constants");

var _constants2 = require("../../common/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore

/**
 *
 * @param callCluster
 * @param server
 * @param {boolean} spacesAvailable
 * @return {UsageStats}
 */
async function getSpacesUsage(callCluster, kibanaIndex, xpackMainPlugin, spacesAvailable) {
  if (!spacesAvailable) {
    return {};
  }

  const knownFeatureIds = xpackMainPlugin.getFeatures().map(feature => feature.id);
  const resp = await callCluster('search', {
    index: kibanaIndex,
    body: {
      track_total_hits: true,
      query: {
        term: {
          type: {
            value: 'space'
          }
        }
      },
      aggs: {
        disabledFeatures: {
          terms: {
            field: 'space.disabledFeatures',
            include: knownFeatureIds,
            size: knownFeatureIds.length
          }
        }
      },
      size: 0
    }
  });
  const {
    hits,
    aggregations
  } = resp;
  const count = (0, _lodash.get)(hits, 'total.value', 0);
  const disabledFeatureBuckets = (0, _lodash.get)(aggregations, 'disabledFeatures.buckets', []);
  const initialCounts = knownFeatureIds.reduce((acc, featureId) => ({ ...acc,
    [featureId]: 0
  }), {});
  const disabledFeatures = disabledFeatureBuckets.reduce((acc, {
    key,
    doc_count
  }) => {
    return { ...acc,
      [key]: doc_count
    };
  }, initialCounts);
  const usesFeatureControls = Object.values(disabledFeatures).some(disabledSpaceCount => disabledSpaceCount > 0);
  return {
    count,
    usesFeatureControls,
    disabledFeatures
  };
}

/*
 * @param {Object} server
 * @return {Object} kibana usage stats type collection object
 */
function getSpacesUsageCollector(deps) {
  const {
    collectorSet
  } = deps.usage;
  return collectorSet.makeUsageCollector({
    type: _constants2.KIBANA_SPACES_STATS_TYPE,
    isReady: () => true,
    fetch: async callCluster => {
      const xpackInfo = deps.xpackMain.info;
      const available = xpackInfo && xpackInfo.isAvailable(); // some form of spaces is available for all valid licenses

      const enabled = deps.config.get('xpack.spaces.enabled');
      const spacesAvailableAndEnabled = Boolean(available && enabled);
      const usageStats = await getSpacesUsage(callCluster, deps.config.get('kibana.index'), deps.xpackMain, spacesAvailableAndEnabled);
      return {
        available,
        enabled: spacesAvailableAndEnabled,
        // similar behavior as _xpack API in ES
        ...usageStats
      };
    },

    /*
     * Format the response data into a model for internal upload
     * 1. Make this data part of the "kibana_stats" type
     * 2. Organize the payload in the usage.xpack.spaces namespace of the data payload
     */
    formatForBulkUpload: result => {
      return {
        type: _constants.KIBANA_STATS_TYPE_MONITORING,
        payload: {
          usage: {
            spaces: result
          }
        }
      };
    }
  });
}