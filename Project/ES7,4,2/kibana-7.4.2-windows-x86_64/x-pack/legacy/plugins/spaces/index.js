"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.spaces = void 0;

var Rx = _interopRequireWildcard(require("rxjs"));

var _path = require("path");

var _optional_plugin = require("../../server/lib/optional_plugin");

var _audit_logger = require("../../server/lib/audit_logger");

var _mappings = _interopRequireDefault(require("./mappings.json"));

var _errors = require("./server/lib/errors");

var _get_active_space = require("./server/lib/get_active_space");

var _get_space_selector_url = require("./server/lib/get_space_selector_url");

var _migrations = require("./server/lib/migrations");

var _new_platform = require("./server/new_platform");

var _request_interceptors = require("./server/lib/request_interceptors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
const spaces = kibana => new kibana.Plugin({
  id: 'spaces',
  configPrefix: 'xpack.spaces',
  publicDir: (0, _path.resolve)(__dirname, 'public'),
  require: ['kibana', 'elasticsearch', 'xpack_main'],

  config(Joi) {
    return Joi.object({
      enabled: Joi.boolean().default(true),
      maxSpaces: Joi.number().default(1000)
    }).default();
  },

  uiCapabilities() {
    return {
      spaces: {
        manage: true
      },
      management: {
        kibana: {
          spaces: true
        }
      }
    };
  },

  uiExports: {
    chromeNavControls: ['plugins/spaces/views/nav_control'],
    styleSheetPaths: (0, _path.resolve)(__dirname, 'public/index.scss'),
    managementSections: ['plugins/spaces/views/management'],
    apps: [{
      id: 'space_selector',
      title: 'Spaces',
      main: 'plugins/spaces/views/space_selector',
      url: 'space_selector',
      hidden: true
    }],
    hacks: [],
    mappings: _mappings.default,
    migrations: {
      space: {
        '6.6.0': _migrations.migrateToKibana660
      }
    },
    savedObjectSchemas: {
      space: {
        isNamespaceAgnostic: true,
        hidden: true
      }
    },
    home: ['plugins/spaces/register_feature'],

    injectDefaultVars(server) {
      return {
        spaces: [],
        activeSpace: null,
        spaceSelectorURL: (0, _get_space_selector_url.getSpaceSelectorUrl)(server.config())
      };
    },

    async replaceInjectedVars(vars, request, server) {
      const spacesClient = await server.plugins.spaces.getScopedSpacesClient(request);

      try {
        vars.activeSpace = {
          valid: true,
          space: await (0, _get_active_space.getActiveSpace)(spacesClient, request.getBasePath(), server.config().get('server.basePath'))
        };
      } catch (e) {
        vars.activeSpace = {
          valid: false,
          error: (0, _errors.wrapError)(e).output.payload
        };
      }

      return vars;
    }

  },

  async init(server) {
    const kbnServer = server;
    const initializerContext = {
      legacyConfig: server.config(),
      config: {
        create: () => {
          return Rx.of({
            maxSpaces: server.config().get('xpack.spaces.maxSpaces')
          });
        }
      },
      logger: {
        get(...contextParts) {
          return kbnServer.newPlatform.coreContext.logger.get('plugins', 'spaces', ...contextParts);
        }

      }
    };
    const spacesHttpService = { ...kbnServer.newPlatform.setup.core.http,
      route: server.route.bind(server)
    };
    const core = {
      http: spacesHttpService,
      elasticsearch: kbnServer.newPlatform.setup.core.elasticsearch,
      savedObjects: server.savedObjects,
      usage: server.usage,
      tutorial: {
        addScopedTutorialContextFactory: server.addScopedTutorialContextFactory
      },
      capabilities: {
        registerCapabilitiesModifier: server.registerCapabilitiesModifier
      },
      auditLogger: {
        create: pluginId => new _audit_logger.AuditLogger(server, pluginId, server.config(), server.plugins.xpack_main.info)
      }
    };
    const plugins = {
      xpackMain: server.plugins.xpack_main,
      // TODO: Spaces has a circular dependency with Security right now.
      // Security is not yet available when init runs, so this is wrapped in an optional function for the time being.
      security: (0, _optional_plugin.createOptionalPlugin)(server.config(), 'xpack.security', server.plugins, 'security'),
      spaces: this
    };
    const {
      spacesService,
      log
    } = await (0, _new_platform.plugin)(initializerContext).setup(core, plugins);
    (0, _request_interceptors.initSpacesRequestInterceptors)({
      config: initializerContext.legacyConfig,
      http: core.http,
      getHiddenUiAppById: server.getHiddenUiAppById,
      onPostAuth: handler => {
        server.ext('onPostAuth', handler);
      },
      log,
      spacesService,
      xpackMain: plugins.xpackMain
    });
    server.expose('getSpaceId', request => spacesService.getSpaceId(request));
    server.expose('spaceIdToNamespace', spacesService.spaceIdToNamespace);
    server.expose('namespaceToSpaceId', spacesService.namespaceToSpaceId);
    server.expose('getBasePath', spacesService.getBasePath);
    server.expose('getScopedSpacesClient', spacesService.scopedClient);
  }

});

exports.spaces = spaces;