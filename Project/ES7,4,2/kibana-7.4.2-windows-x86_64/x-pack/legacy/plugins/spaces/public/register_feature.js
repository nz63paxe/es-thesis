"use strict";

var _i18n = require("@kbn/i18n");

var _feature_catalogue = require("ui/registry/feature_catalogue");

var _constants = require("./lib/constants");

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
_feature_catalogue.FeatureCatalogueRegistryProvider.register(function () {
  return {
    id: 'spaces',
    title: _i18n.i18n.translate('xpack.spaces.spacesTitle', {
      defaultMessage: 'Spaces'
    }),
    description: (0, _constants.getSpacesFeatureDescription)(),
    icon: 'spacesApp',
    path: '/app/kibana#/management/spaces/list',
    showOnHomePage: true,
    category: _feature_catalogue.FeatureCatalogueCategory.ADMIN
  };
});