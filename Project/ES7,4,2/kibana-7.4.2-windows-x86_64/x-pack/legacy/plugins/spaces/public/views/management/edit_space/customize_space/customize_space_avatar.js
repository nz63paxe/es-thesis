"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomizeSpaceAvatar = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

var _constants = require("../../../../../common/constants");

var _space_attributes = require("../../../../../common/space_attributes");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var CustomizeSpaceAvatarUI =
/*#__PURE__*/
function (_Component) {
  _inherits(CustomizeSpaceAvatarUI, _Component);

  function CustomizeSpaceAvatarUI(props) {
    var _this;

    _classCallCheck(this, CustomizeSpaceAvatarUI);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CustomizeSpaceAvatarUI).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "initialsRef", null);

    _defineProperty(_assertThisInitialized(_this), "initialsInputRef", function (ref) {
      if (ref) {
        _this.initialsRef = ref;

        _this.initialsRef.addEventListener('focus', _this.onInitialsFocus);

        _this.initialsRef.addEventListener('blur', _this.onInitialsBlur);
      } else {
        if (_this.initialsRef) {
          _this.initialsRef.removeEventListener('focus', _this.onInitialsFocus);

          _this.initialsRef.removeEventListener('blur', _this.onInitialsBlur);

          _this.initialsRef = null;
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this), "onInitialsFocus", function () {
      _this.setState({
        initialsHasFocus: true,
        pendingInitials: (0, _space_attributes.getSpaceInitials)(_this.props.space)
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onInitialsBlur", function () {
      _this.setState({
        initialsHasFocus: false,
        pendingInitials: null
      });
    });

    _defineProperty(_assertThisInitialized(_this), "onInitialsChange", function (e) {
      var initials = (e.target.value || '').substring(0, _constants.MAX_SPACE_INITIALS);

      _this.setState({
        pendingInitials: initials
      });

      _this.props.onChange(_objectSpread({}, _this.props.space, {
        initials: initials
      }));
    });

    _defineProperty(_assertThisInitialized(_this), "onColorChange", function (color) {
      _this.props.onChange(_objectSpread({}, _this.props.space, {
        color: color
      }));
    });

    _this.state = {
      initialsHasFocus: false
    };
    return _this;
  }

  _createClass(CustomizeSpaceAvatarUI, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          space = _this$props.space,
          intl = _this$props.intl;
      var _this$state = this.state,
          initialsHasFocus = _this$state.initialsHasFocus,
          pendingInitials = _this$state.pendingInitials;
      var spaceColor = (0, _space_attributes.getSpaceColor)(space);
      var isInvalidSpaceColor = !(0, _eui.isValidHex)(spaceColor) && spaceColor !== '';
      return _react2.default.createElement("form", {
        onSubmit: function onSubmit() {
          return false;
        }
      }, _react2.default.createElement(_eui.EuiFlexItem, {
        grow: false
      }, _react2.default.createElement(_eui.EuiFormRow, {
        label: intl.formatMessage({
          id: 'xpack.spaces.management.customizeSpaceAvatar.initialItemsFormRowLabel',
          defaultMessage: 'Initials (2 max)'
        })
      }, _react2.default.createElement(_eui.EuiFieldText, {
        inputRef: this.initialsInputRef,
        name: "spaceInitials" // allows input to be cleared or otherwise invalidated while user is editing the initials,
        // without defaulting to the derived initials provided by `getSpaceInitials`
        ,
        value: initialsHasFocus ? pendingInitials || '' : (0, _space_attributes.getSpaceInitials)(space),
        onChange: this.onInitialsChange
      }))), _react2.default.createElement(_eui.EuiFlexItem, {
        grow: true
      }, _react2.default.createElement(_eui.EuiFormRow, {
        label: intl.formatMessage({
          id: 'xpack.spaces.management.customizeSpaceAvatar.colorFormRowLabel',
          defaultMessage: 'Color'
        }),
        isInvalid: isInvalidSpaceColor
      }, _react2.default.createElement(_eui.EuiColorPicker, {
        color: spaceColor,
        onChange: this.onColorChange,
        isInvalid: isInvalidSpaceColor
      }))));
    }
  }]);

  return CustomizeSpaceAvatarUI;
}(_react2.Component);

var CustomizeSpaceAvatar = (0, _react.injectI18n)(CustomizeSpaceAvatarUI);
exports.CustomizeSpaceAvatar = CustomizeSpaceAvatar;