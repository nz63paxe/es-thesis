"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getListBreadcrumbs = getListBreadcrumbs;
exports.getCreateBreadcrumbs = getCreateBreadcrumbs;
exports.getEditBreadcrumbs = getEditBreadcrumbs;

var _breadcrumbs = require("ui/management/breadcrumbs");

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function getListBreadcrumbs() {
  return [_breadcrumbs.MANAGEMENT_BREADCRUMB, {
    text: 'Spaces',
    href: '#/management/spaces/list'
  }];
}

function getCreateBreadcrumbs() {
  return [].concat(_toConsumableArray(getListBreadcrumbs()), [{
    text: 'Create'
  }]);
}

function getEditBreadcrumbs(space) {
  return [].concat(_toConsumableArray(getListBreadcrumbs()), [{
    text: space ? space.name : '...'
  }]);
}