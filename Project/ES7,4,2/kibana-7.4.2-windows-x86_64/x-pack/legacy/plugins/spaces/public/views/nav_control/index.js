"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SpacesNavState", {
  enumerable: true,
  get: function get() {
    return _nav_control.SpacesNavState;
  }
});

var _nav_control = require("./nav_control");