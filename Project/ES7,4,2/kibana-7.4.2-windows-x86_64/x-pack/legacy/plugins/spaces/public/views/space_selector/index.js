"use strict";

var _spaces_manager = require("plugins/spaces/lib/spaces_manager");

var _space_selector = _interopRequireDefault(require("plugins/spaces/views/space_selector/space_selector.html"));

require("ui/autoload/styles");

var _chrome = _interopRequireDefault(require("ui/chrome"));

var _i18n = require("ui/i18n");

var _modules = require("ui/modules");

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

var _space_selector2 = require("./space_selector");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
var _module = _modules.uiModules.get('spaces_selector', []);

_module.controller('spacesSelectorController', function ($scope, spaces, spaceSelectorURL) {
  var domNode = document.getElementById('spaceSelectorRoot');
  var spacesManager = new _spaces_manager.SpacesManager(spaceSelectorURL);
  (0, _reactDom.render)(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_space_selector2.SpaceSelector, {
    spaces: spaces,
    spacesManager: spacesManager
  })), domNode); // unmount react on controller destroy

  $scope.$on('$destroy', function () {
    if (domNode) {
      (0, _reactDom.unmountComponentAtNode)(domNode);
    }
  });
});

_chrome.default.setVisible(false).setRootTemplate(_space_selector.default);