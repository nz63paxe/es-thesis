"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AdvancedSettingsTitle = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireDefault(require("react"));

var _components = require("../../../../components");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AdvancedSettingsTitle = function AdvancedSettingsTitle(props) {
  return _react2.default.createElement(_eui.EuiFlexGroup, {
    gutterSize: "s",
    responsive: false,
    alignItems: 'center'
  }, _react2.default.createElement(_eui.EuiFlexItem, {
    grow: false
  }, _react2.default.createElement(_components.SpaceAvatar, {
    space: props.space
  })), _react2.default.createElement(_eui.EuiFlexItem, {
    style: {
      marginLeft: '10px'
    }
  }, _react2.default.createElement(_eui.EuiTitle, {
    size: "m"
  }, _react2.default.createElement("h1", {
    "data-test-subj": "managementSettingsTitle"
  }, _react2.default.createElement(_react.FormattedMessage, {
    id: "xpack.spaces.management.advancedSettingsTitle.settingsTitle",
    defaultMessage: "Settings"
  })))));
};

exports.AdvancedSettingsTitle = AdvancedSettingsTitle;