"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AdvancedSettingsSubtitle = void 0;

var _eui = require("@elastic/eui");

var _react = require("@kbn/i18n/react");

var _react2 = _interopRequireWildcard(require("react"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
var AdvancedSettingsSubtitle = function AdvancedSettingsSubtitle(props) {
  return _react2.default.createElement(_react2.Fragment, null, _react2.default.createElement(_eui.EuiSpacer, {
    size: 'm'
  }), _react2.default.createElement(_eui.EuiCallOut, {
    color: "primary",
    iconType: "spacesApp",
    title: _react2.default.createElement("p", null, _react2.default.createElement(_react.FormattedMessage, {
      id: "xpack.spaces.management.advancedSettingsSubtitle.applyingSettingsOnPageToSpaceDescription",
      defaultMessage: "The settings on this page apply to the {spaceName} space, unless otherwise specified.",
      values: {
        spaceName: _react2.default.createElement("strong", null, props.space.name)
      }
    }))
  }));
};

exports.AdvancedSettingsSubtitle = AdvancedSettingsSubtitle;