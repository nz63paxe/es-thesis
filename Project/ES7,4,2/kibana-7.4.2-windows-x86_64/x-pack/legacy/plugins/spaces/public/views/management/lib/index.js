"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "toSpaceIdentifier", {
  enumerable: true,
  get: function get() {
    return _space_identifier_utils.toSpaceIdentifier;
  }
});
Object.defineProperty(exports, "isValidSpaceIdentifier", {
  enumerable: true,
  get: function get() {
    return _space_identifier_utils.isValidSpaceIdentifier;
  }
});
Object.defineProperty(exports, "SpaceValidator", {
  enumerable: true,
  get: function get() {
    return _validate_space.SpaceValidator;
  }
});
Object.defineProperty(exports, "getCreateBreadcrumbs", {
  enumerable: true,
  get: function get() {
    return _breadcrumbs.getCreateBreadcrumbs;
  }
});
Object.defineProperty(exports, "getEditBreadcrumbs", {
  enumerable: true,
  get: function get() {
    return _breadcrumbs.getEditBreadcrumbs;
  }
});
Object.defineProperty(exports, "getListBreadcrumbs", {
  enumerable: true,
  get: function get() {
    return _breadcrumbs.getListBreadcrumbs;
  }
});

var _space_identifier_utils = require("./space_identifier_utils");

var _validate_space = require("./validate_space");

var _breadcrumbs = require("./breadcrumbs");