"use strict";

var _template = _interopRequireDefault(require("plugins/spaces/views/management/template.html"));

var _react = _interopRequireDefault(require("react"));

var _reactDom = require("react-dom");

require("ui/autoload/styles");

var _i18n = require("ui/i18n");

var _routes = _interopRequireDefault(require("ui/routes"));

var _spaces_manager = require("../../lib/spaces_manager");

var _edit_space = require("./edit_space");

var _lib = require("./lib");

var _spaces_grid = require("./spaces_grid");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var reactRootNodeId = 'manageSpacesReactRoot';

_routes.default.when('/management/spaces/list', {
  template: _template.default,
  k7Breadcrumbs: _lib.getListBreadcrumbs,
  requireUICapability: 'management.kibana.spaces',
  controller: function controller($scope, spacesNavState, spaceSelectorURL) {
    $scope.$$postDigest(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee() {
      var domNode, spacesManager;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              domNode = document.getElementById(reactRootNodeId);
              spacesManager = new _spaces_manager.SpacesManager(spaceSelectorURL);
              (0, _reactDom.render)(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_spaces_grid.SpacesGridPage, {
                spacesManager: spacesManager,
                spacesNavState: spacesNavState
              })), domNode); // unmount react on controller destroy

              $scope.$on('$destroy', function () {
                if (domNode) {
                  (0, _reactDom.unmountComponentAtNode)(domNode);
                }
              });

            case 4:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    })));
  }
});

_routes.default.when('/management/spaces/create', {
  template: _template.default,
  k7Breadcrumbs: _lib.getCreateBreadcrumbs,
  requireUICapability: 'management.kibana.spaces',
  controller: function controller($scope, spacesNavState, spaceSelectorURL) {
    $scope.$$postDigest(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee2() {
      var domNode, spacesManager;
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              domNode = document.getElementById(reactRootNodeId);
              spacesManager = new _spaces_manager.SpacesManager(spaceSelectorURL);
              (0, _reactDom.render)(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_edit_space.ManageSpacePage, {
                spacesManager: spacesManager,
                spacesNavState: spacesNavState
              })), domNode); // unmount react on controller destroy

              $scope.$on('$destroy', function () {
                if (domNode) {
                  (0, _reactDom.unmountComponentAtNode)(domNode);
                }
              });

            case 4:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    })));
  }
});

_routes.default.when('/management/spaces/edit', {
  redirectTo: '/management/spaces/list'
});

_routes.default.when('/management/spaces/edit/:spaceId', {
  template: _template.default,
  k7Breadcrumbs: function k7Breadcrumbs() {
    return (0, _lib.getEditBreadcrumbs)();
  },
  requireUICapability: 'management.kibana.spaces',
  controller: function controller($scope, $route, chrome, spacesNavState, spaceSelectorURL) {
    $scope.$$postDigest(
    /*#__PURE__*/
    _asyncToGenerator(
    /*#__PURE__*/
    regeneratorRuntime.mark(function _callee3() {
      var domNode, spaceId, spacesManager;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              domNode = document.getElementById(reactRootNodeId);
              spaceId = $route.current.params.spaceId;
              spacesManager = new _spaces_manager.SpacesManager(spaceSelectorURL);
              (0, _reactDom.render)(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_edit_space.ManageSpacePage, {
                spaceId: spaceId,
                spacesManager: spacesManager,
                spacesNavState: spacesNavState,
                setBreadcrumbs: function setBreadcrumbs(breadcrumbs) {
                  chrome.breadcrumbs.set(breadcrumbs);
                }
              })), domNode); // unmount react on controller destroy

              $scope.$on('$destroy', function () {
                if (domNode) {
                  (0, _reactDom.unmountComponentAtNode)(domNode);
                }
              });

            case 5:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3);
    })));
  }
});