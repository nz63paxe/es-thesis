"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SpacesManager = void 0;

var _i18n = require("@kbn/i18n");

var _notify = require("ui/notify");

var _events = require("events");

var _kfetch = require("ui/kfetch");

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SpacesManager =
/*#__PURE__*/
function (_EventEmitter) {
  _inherits(SpacesManager, _EventEmitter);

  function SpacesManager(spaceSelectorURL) {
    var _this;

    _classCallCheck(this, SpacesManager);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SpacesManager).call(this));

    _defineProperty(_assertThisInitialized(_this), "spaceSelectorURL", void 0);

    _this.spaceSelectorURL = spaceSelectorURL;
    return _this;
  }

  _createClass(SpacesManager, [{
    key: "getSpaces",
    value: function () {
      var _getSpaces = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee(purpose) {
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: '/api/spaces/space',
                  query: {
                    purpose: purpose
                  }
                });

              case 2:
                return _context.abrupt("return", _context.sent);

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }));

      function getSpaces(_x) {
        return _getSpaces.apply(this, arguments);
      }

      return getSpaces;
    }()
  }, {
    key: "getSpace",
    value: function () {
      var _getSpace = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee2(id) {
        return regeneratorRuntime.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/space/".concat(encodeURIComponent(id))
                });

              case 2:
                return _context2.abrupt("return", _context2.sent);

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }));

      function getSpace(_x2) {
        return _getSpace.apply(this, arguments);
      }

      return getSpace;
    }()
  }, {
    key: "createSpace",
    value: function () {
      var _createSpace = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee3(space) {
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/space",
                  method: 'POST',
                  body: JSON.stringify(space)
                });

              case 2:
                return _context3.abrupt("return", _context3.sent);

              case 3:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }));

      function createSpace(_x3) {
        return _createSpace.apply(this, arguments);
      }

      return createSpace;
    }()
  }, {
    key: "updateSpace",
    value: function () {
      var _updateSpace = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee4(space) {
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/space/".concat(encodeURIComponent(space.id)),
                  query: {
                    overwrite: true
                  },
                  method: 'PUT',
                  body: JSON.stringify(space)
                });

              case 2:
                return _context4.abrupt("return", _context4.sent);

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }));

      function updateSpace(_x4) {
        return _updateSpace.apply(this, arguments);
      }

      return updateSpace;
    }()
  }, {
    key: "deleteSpace",
    value: function () {
      var _deleteSpace = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee5(space) {
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/space/".concat(encodeURIComponent(space.id)),
                  method: 'DELETE'
                });

              case 2:
                return _context5.abrupt("return", _context5.sent);

              case 3:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }));

      function deleteSpace(_x5) {
        return _deleteSpace.apply(this, arguments);
      }

      return deleteSpace;
    }()
  }, {
    key: "copySavedObjects",
    value: function () {
      var _copySavedObjects = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee6(objects, spaces, includeReferences, overwrite) {
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/_copy_saved_objects",
                  method: 'POST',
                  body: JSON.stringify({
                    objects: objects,
                    spaces: spaces,
                    includeReferences: includeReferences,
                    overwrite: overwrite
                  })
                });

              case 2:
                return _context6.abrupt("return", _context6.sent);

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }));

      function copySavedObjects(_x6, _x7, _x8, _x9) {
        return _copySavedObjects.apply(this, arguments);
      }

      return copySavedObjects;
    }()
  }, {
    key: "resolveCopySavedObjectsErrors",
    value: function () {
      var _resolveCopySavedObjectsErrors = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee7(objects, retries, includeReferences) {
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/_resolve_copy_saved_objects_errors",
                  method: 'POST',
                  body: JSON.stringify({
                    objects: objects,
                    includeReferences: includeReferences,
                    retries: retries
                  })
                });

              case 2:
                return _context7.abrupt("return", _context7.sent);

              case 3:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }));

      function resolveCopySavedObjectsErrors(_x10, _x11, _x12) {
        return _resolveCopySavedObjectsErrors.apply(this, arguments);
      }

      return resolveCopySavedObjectsErrors;
    }()
  }, {
    key: "changeSelectedSpace",
    value: function () {
      var _changeSelectedSpace = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee8(space) {
        var _this2 = this;

        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.next = 2;
                return (0, _kfetch.kfetch)({
                  pathname: "/api/spaces/v1/space/".concat(encodeURIComponent(space.id), "/select"),
                  method: 'POST'
                }).then(function (response) {
                  if (response.location) {
                    window.location = response.location;
                  } else {
                    _this2._displayError();
                  }
                }).catch(function () {
                  return _this2._displayError();
                });

              case 2:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }));

      function changeSelectedSpace(_x13) {
        return _changeSelectedSpace.apply(this, arguments);
      }

      return changeSelectedSpace;
    }()
  }, {
    key: "redirectToSpaceSelector",
    value: function redirectToSpaceSelector() {
      window.location.href = this.spaceSelectorURL;
    }
  }, {
    key: "requestRefresh",
    value: function () {
      var _requestRefresh = _asyncToGenerator(
      /*#__PURE__*/
      regeneratorRuntime.mark(function _callee9() {
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                this.emit('request_refresh');

              case 1:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function requestRefresh() {
        return _requestRefresh.apply(this, arguments);
      }

      return requestRefresh;
    }()
  }, {
    key: "_displayError",
    value: function _displayError() {
      _notify.toastNotifications.addDanger({
        title: _i18n.i18n.translate('xpack.spaces.spacesManager.unableToChangeSpaceWarningTitle', {
          defaultMessage: 'Unable to change your Space'
        }),
        text: _i18n.i18n.translate('xpack.spaces.spacesManager.unableToChangeSpaceWarningDescription', {
          defaultMessage: 'please try again later'
        })
      });
    }
  }]);

  return SpacesManager;
}(_events.EventEmitter);

exports.SpacesManager = SpacesManager;