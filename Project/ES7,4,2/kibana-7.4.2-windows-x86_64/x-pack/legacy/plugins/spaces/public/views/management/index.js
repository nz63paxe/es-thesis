"use strict";

var _i18n = require("@kbn/i18n");

require("plugins/spaces/views/management/page_routes");

var _react = _interopRequireDefault(require("react"));

var _management = require("ui/management");

var _saved_objects_management = require("ui/management/saved_objects_management");

var _routes = _interopRequireDefault(require("ui/routes"));

var _lib = require("../../lib");

var _advanced_settings_subtitle = require("./components/advanced_settings_subtitle");

var _advanced_settings_title = require("./components/advanced_settings_title");

var _copy_saved_objects_to_space = require("../../lib/copy_saved_objects_to_space");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
var MANAGE_SPACES_KEY = 'spaces';

_routes.default.defaults(/\/management/, {
  resolve: {
    spacesManagementSection: function spacesManagementSection(activeSpace, spaceSelectorURL) {
      function getKibanaSection() {
        return _management.management.getSection('kibana');
      }

      function deregisterSpaces() {
        getKibanaSection().deregister(MANAGE_SPACES_KEY);
      }

      function ensureSpagesRegistered() {
        var kibanaSection = getKibanaSection();

        if (!kibanaSection.hasItem(MANAGE_SPACES_KEY)) {
          kibanaSection.register(MANAGE_SPACES_KEY, {
            name: 'spacesManagementLink',
            order: 10,
            display: _i18n.i18n.translate('xpack.spaces.displayName', {
              defaultMessage: 'Spaces'
            }),
            url: "#/management/spaces/list"
          });
        } // Customize Saved Objects Management


        var action = new _copy_saved_objects_to_space.CopyToSpaceSavedObjectsManagementAction(new _lib.SpacesManager(spaceSelectorURL), activeSpace.space); // This route resolve function executes any time the management screen is loaded, and we want to ensure
        // that this action is only registered once.

        if (!_saved_objects_management.SavedObjectsManagementActionRegistry.has(action.id)) {
          _saved_objects_management.SavedObjectsManagementActionRegistry.register(action);
        } // Customize Advanced Settings


        var PageTitle = function PageTitle() {
          return _react.default.createElement(_advanced_settings_title.AdvancedSettingsTitle, {
            space: activeSpace.space
          });
        };

        (0, _management.registerSettingsComponent)(_management.PAGE_TITLE_COMPONENT, PageTitle, true);

        var SubTitle = function SubTitle() {
          return _react.default.createElement(_advanced_settings_subtitle.AdvancedSettingsSubtitle, {
            space: activeSpace.space
          });
        };

        (0, _management.registerSettingsComponent)(_management.PAGE_SUBTITLE_COMPONENT, SubTitle, true);
      }

      deregisterSpaces();
      ensureSpagesRegistered();
    }
  }
});