"use strict";

var _spaces_manager = require("plugins/spaces/lib/spaces_manager");

var _nav_control_popover = require("plugins/spaces/views/nav_control/nav_control_popover");

var _path = require("plugins/xpack_main/services/path");

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _i18n = require("ui/i18n");

var _modules = require("ui/modules");

var _chrome_header_nav_controls = require("ui/registry/chrome_header_nav_controls");

var _spaces_header_nav_button = require("./components/spaces_header_nav_button");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
// @ts-ignore
var _module = _modules.uiModules.get('spaces_nav', ['kibana']);

var spacesManager;

_module.service('spacesNavState', function (activeSpace) {
  return {
    getActiveSpace: function getActiveSpace() {
      return activeSpace.space;
    },
    refreshSpacesList: function refreshSpacesList() {
      if (spacesManager) {
        spacesManager.requestRefresh();
      }
    }
  };
});

_chrome_header_nav_controls.chromeHeaderNavControlsRegistry.register(function (chrome, activeSpace) {
  return {
    name: 'spaces',
    order: 1000,
    side: _chrome_header_nav_controls.NavControlSide.Left,
    render: function render(el) {
      if (_path.Path.isUnauthenticated()) {
        return;
      }

      var spaceSelectorURL = chrome.getInjected('spaceSelectorURL');
      spacesManager = new _spaces_manager.SpacesManager(spaceSelectorURL);

      _reactDom.default.render(_react.default.createElement(_i18n.I18nContext, null, _react.default.createElement(_nav_control_popover.NavControlPopover, {
        spacesManager: spacesManager,
        activeSpace: activeSpace,
        anchorPosition: "downLeft",
        buttonClass: _spaces_header_nav_button.SpacesHeaderNavButton
      })), el);
    }
  };
});