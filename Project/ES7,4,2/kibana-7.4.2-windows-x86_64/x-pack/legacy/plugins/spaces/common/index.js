"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "isReservedSpace", {
  enumerable: true,
  get: function () {
    return _is_reserved_space.isReservedSpace;
  }
});
Object.defineProperty(exports, "MAX_SPACE_INITIALS", {
  enumerable: true,
  get: function () {
    return _constants.MAX_SPACE_INITIALS;
  }
});
Object.defineProperty(exports, "getSpaceInitials", {
  enumerable: true,
  get: function () {
    return _space_attributes.getSpaceInitials;
  }
});
Object.defineProperty(exports, "getSpaceColor", {
  enumerable: true,
  get: function () {
    return _space_attributes.getSpaceColor;
  }
});

var _is_reserved_space = require("./is_reserved_space");

var _constants = require("./constants");

var _space_attributes = require("./space_attributes");