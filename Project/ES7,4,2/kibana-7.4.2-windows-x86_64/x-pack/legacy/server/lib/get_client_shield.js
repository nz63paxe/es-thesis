"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getClient = void 0;

var _lodash = require("lodash");

var _esjs_shield_plugin = _interopRequireDefault(require("./esjs_shield_plugin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/*
 * Copyright Elasticsearch B.V. and/or licensed to Elasticsearch B.V. under one
 * or more contributor license agreements. Licensed under the Elastic License;
 * you may not use this file except in compliance with the Elastic License.
 */
// @ts-ignore
const getClient = (0, _lodash.once)(server => {
  return server.plugins.elasticsearch.createCluster('security', {
    plugins: [_esjs_shield_plugin.default]
  });
});
exports.getClient = getClient;